<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS - Keyword search results...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

</head>





<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			


</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<a href='../archive/index.htm'>
			<div class='navBar_item'>
				Back Issues
			</div>
		</a>
		<a href='http://www.schnews.org.uk/schmovies/index.php'>
			<div class='navBar_item'>
				SchMOVIES
			</div>
		</a>
		<a href='http://www.schnews.org.uk/features'>
			<div class='navBar_item'>
				Feature Articles
			</div>
		</a>
		<a href='http://www.schnews.org.uk/links/index.php'>
			<div class='navBar_item'>
				Contacts and Links
			</div>
		</a>
		<a href='http://www.schnews.org.uk/diyguide/index.htm'>
			<div class='navBar_item'>
				DIY Guide
			</div>
		</a>
		<a href='http://www.schnews.org.uk/monopresist/index.htm'>
			<div class='navBar_item'>
				Archive
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/about.php'>
			<div class='navBar_item'>
				About Us
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/subscribe.php'>
			<div class='navBar_item'>
				Subscribe
			</div>
		</a>

			
		<!--	SCROOGLE SEARCh BAR 	-->
		<div class='search_bar'>		<form action="https://duckduckgo.com/" method="get"  target="_blank"> 
		
		 						
				<input type="search" placeholder="Search" name="q" maxlength="300" > 
				&nbsp;&nbsp;
				<input type="hidden" name="sites" value="schnews.org"> 
				    	<input type="hidden" value="1" name="kh">
    					<input type="hidden" value="1" name="kn">
    					<input type="hidden" value="1" name="kac">
						<input type="hidden" value="w" name="kw">
						
			<!--<input name="submit2" type="submit" value="Go">-->
			<!--<button type="submit">Search</button>-->
				
		</form>
		</div>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- DONATE BUTTON -->
		<table width="110" style="border-color:#000000; border-style:solid; border-width:1px" align="center">
			<tr> 
				<td height="2"> 
					<p align="center">
						<font face="Arial, Helvetica, sans-serif" size="1">
							<b>SchNEWS - on the blag for money <BR>since 1994..</b>.<br /> 
						</font>
						<a 	href="../extras/help.htm" 
							onMouseOut="MM_swapImgRestore()" 
							onMouseOver="MM_swapImage('Image45','','../images_main/donate-mo.gif',1)">
								<img 	name="Image45" 
										border="0" 
										src="../images_main/donate.gif" 
										width="84" 
										height="29" 
										alt="Send us a donation via PayPal or cheque" />
						</a>
					</p>
				</td>
			</tr> 
		</table>

		
		<!--	REST OF THE LEFT BAR 
		
				Due to the shitness of IE rendering engine it is better to leave this block of code as it is with no 
				other white space between tags, otherwise IE renders parts of the white spaces and messes up the display. -->
		<hr />

		
		<!-- 	Join Us on Twitter	-->
        <span style='font-size:10px'>
        	Join us on <a href='http://twitter.com/schnews' target="_blank">Twitter</a>
        </span>

        <hr />
        
        
        <!-- 	RSS Feed 	-->
        <a href="../pages_menu/defunct.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','',../'images_main/rss-button-mo.gif',1)">
        	<img name="Image40" border="0" src="../images_main/rss-button.gif" width="110" alt="SchNEWS RSS feed, click here for information on how to subscribe">
        </a>

        <hr />
		
        <div style='text-align:center'>
		
		<!--	Uncertified Link -->
		<a href="../pages_merchandise/merchandise_video.php#uncertified">
    		<img style="border: none" align='center' src='../images_main/uncertified-banner-110.jpg' width="110" border="0" alt="Uncertified" />
    	</a>
		
    	<!-- Paypal By Now Link -->
    	<!-- 	Buy Uncertified Only 	-->
		<form style="padding:0; margin: 0" action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	    <font size="2" face="Arial, Helvetica, sans-serif"> 
	    <input type="hidden" name="cmd" value="_xclick" />
	    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
	    <input type="hidden" name="undefined_quantity" value="1" />
	    <input type="hidden" name="item_name" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection" />
	    <input type="hidden" name="item_number" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection" />
	    <input type="hidden" name="amount" value="6.00" />
	    <input type="hidden" name="no_shipping" value="2" />
	    <input type="hidden" name="return" value="http://www.schnews.org.uk/extras/complete.htm" />
	    <input type="hidden" name="cancel_return" value="http://www.schnews.org.uk/extras/cancel.htm" />
	    <input type="hidden" name="currency_code" value="GBP" />
	    <input type="hidden" name="lc" value="GB" />
	    <input type="hidden" name="bn" value="PP-BuyNowBF" />
	    </font>
	
	    <hr />
	    	
		<font size="2" face="Arial, Helvetica, sans-serif"> 
        <input type="image" src="../images_main/buyNow.gif" class="backgroundColorChange" style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
        </font><br />
		<font style='font-size:8px' face="Arial, Helvetica, sans-serif">
			&pound;6 per copy (inc. P&amp;P) <br> (via Paypal) 
		</font>
		</form>
		
		<!-- 	Buy Uncertified plus last years copy 	-->
		<form style='padding:0; margin: 0' action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	    <font size="2" face="Arial, Helvetica, sans-serif"> 
	    <input type="hidden" name="cmd" value="_xclick" />
	    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
	    <input type="hidden" name="undefined_quantity" value="1" />
	    <input type="hidden" name="item_name" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection + Take Three 2007 Collection" />
	    <input type="hidden" name="item_number" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection + Take Three 2007 Collection" />
	    <input type="hidden" name="amount" value="7.00" />
	    <input type="hidden" name="no_shipping" value="2" />
	    <input type="hidden" name="return" value="http://www.schnews.org.uk/extras/complete.htm" />
	    <input type="hidden" name="cancel_return" value="http://www.schnews.org.uk/extras/cancel.htm" />
	    <input type="hidden" name="currency_code" value="GBP" />
	    <input type="hidden" name="lc" value="GB" />
	    <input type="hidden" name="bn" value="PP-BuyNowBF" />
	    </font>

	    <hr />
	    
	    <font size="2" face="Arial, Helvetica, sans-serif"> 
        <input type="image" src="../images_main/buyNow.gif" class="backgroundColorChange"style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
        </font>
        <br />
		<font style='font-size:8px' face="Arial, Helvetica, sans-serif">
			&pound;7 per copy inc. last years <strong>Take Three 2007 collection</strong>(inc. P&amp;P) <br />(via Paypal) 
		</font>
		</form>

	   	</div>
	   	
	   	
		
		<hr />
		
		<!-- 	Contacts and Links 	-->
		<a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','../images_main/contacts-lhc-button-mo.gif',1)">
			<img name="Image42" border="0" src="../images_main/contacts-lhc-button.gif" alt="Contacts listings">
		</a>
		
		<hr />
		
		
		<!-- 	Satire Link -->
		<A HREF="../satire/index.html" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image48','','../images_main/satire-button-over.png',1)">
			<IMG NAME="Image48" BORDER="0" SRC="../images_main/satire-button.png" WIDTH="110" HEIGHT="40">
		</A>
		
		<hr />
		
		
		<!-- 	Book Reviews Link 	-->
		<a href="../book-reviews/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image43','','../images_main/book-reviews-button-mo.gif',1)">
			<img name="Image43" border="0" src="../images_main/book-reviews-button.gif" width="110" alt="Books (and films) reviewed by SchNEWS hacks"> 
		</a>
		
		<hr />
		
		
		<!-- 	Merchandise Link	-->
		<a href="../pages_merchandise/merchandise.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image44','','../images_main/merchandise-button-mo.gif',1)">
			<img name="Image44" border="0" src="../images_main/merchandise-button.gif" width="110" height="45" alt="DVDs, books, t-shirts and other SchNEWS merchandise">
		</a>
		
		<hr />
		
		
		<!-- 	Merchandise T-Shirts Links	-->
		<a href="../pages_merchandise/merchandise_tshirts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchNEWS T-Shirts','','../images_main/t-shirts-button-over.png',1)">
			<img name="SchNEWS T-Shirts" border="0" src="../images_main/t-shirts-button.png" width="110" height="40"> 
		</a>
		
		<hr />		
		
				
		<!-- 	Radiohead Commendation	-->
		<br />
		<div style='color: black; font-size: 8px; font-family: Arial, Helvetica, sans-serif; font-weight: bold'>
			&quot;Definitely one of the best party and protest sites to come out of the UK. Updated weekly, 
			brilliantly written, bleakly humourous, and essential reading for anyone who gives 
			a shit. And we all should.&quot;
		</div>
		<div style='font-size: 8px; font-weight: bold; font-family: Arial, Helvetica, sans-serif'>
			- Radiohead
			<br /> 
			<A HREF="pages_merchandise/bookreviews.html">
			Other Reviews
			</A>
		</div>

</div>




	
<div class="copyleftBar">
	<img src="../images_main/description-new.png" width="643" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" />
</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main article keyword results -->
		<div id="searchResults">

		</div>
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
		


</div>




<div class='mainBar_right'>

		<h3>Feature Articles that match your keyword...</h3>

		<!-- Feature keyword results -->
		<div id="featureSearchResults">

		</div>
		
</div>






<div class="footer">

		<font face="Arial, Helvetica, sans-serif"> 
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p></font>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>
		
</div>








</div>




</body>
</html>

