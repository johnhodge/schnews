#!/usr/bin/perl
#

#Make sure that the above line is at the very first line, not
#several lines down, and that it is completely flush with 
#the left margin, or your script will not work.

#The first two characters of the first line of your script
#should read #! and should be completely flush with the
#left margin.  If this is not the case, then it is likely that
#your web browser tried to convert this text file to a web page
#and reformatted this page, which will make it unusable, an
#error common with MS Internet Explorer.  Redownload this file
#as a text file, not an HTML file.


#(c)FLATTEXT Database Scripts 1997-2002

use CGI;
$query = new CGI;

#You need to modify this script at all parts of Step B

#Depending on where Perl is on your server, the above location
#may not be correct.  To find where Perl is, telnet to your
#server and at the prompt type: which perl  This will tell you
#the correct path to Perl on your server.  Or, contact your
#server administrator

#Script Description
  #Unique script ID: cc5d-exf6
  #Created on:       7/8/2004
  #Last edited on:   7/21/2004
  #Script class:     D

#You can edit this script on our server any time up to two years
#after it was first created or last edited.  The values that you used
#to create this script will be loaded and you can change the
#configurations as needed.  Any edits you make to this script offline
#should be marked with comments so that you can easily find them and
#transfer them to any upgraded scripts you create

#See http://flattext.com

#STEP A================================
#SCRIPT USAGE
#This is FLATTEXT 7.0, released March 1, 2002

#COPYRIGHT NOTICE
#FLATTEXT License for Regular Scripts

#All FLATTEXT scripts are copyright (c) FLATTEXT Database 
#Scripts, Inc. 1997-2002.  Legally, you must retain the copyright 
#line inside the script.  You are free to remove the printed 
#"Search results by FLATTEXT" text that appears inside Step 
#L of your script (see below), but under no circumstances 
#should the copyright assignment line (above) be removed.

#Each script is licensed for use on a single computer or 
#server.  Running multiple versions of the same script, or 
#continually editing the same script for different uses is 
#not permitted.  Each implementation of the script requires 
#a new license (exceptions to this are those who have 
#purchased a Developer License, or those who have made
#separate and written arrangements with us).  All scripts
#created by Developer License holders are automatically
#licensed.

#By using any of our scripts, you agree not to hold us 
#liable for any lost data, time, or any other costs 
#directly or indirectly associated with the use of our product.

#Many of the common problems are addressed in the help
#files at:

#http://flattext.com/help/

#A1. The following lines get and process data passed 
#through the URL, do not modify
$stringpassed=$ENV{'QUERY_STRING'};

#A2. Replace all plusses with spaces for data passed via URL
$stringpassed=~s/\+/ /g;

#STEP B================================
#You MUST modify each of the variables in this this section

#B1. REQUIRED: The location of the data file on your server.  This must
#be the PATH to your data file, not the URL of your data file!  There
#is extensive treatment of this in the Help Pages, under Data File
#Errors: http://flattext.com/help/
  $data="../contacts/database";

#B1b. REQUIRED: The location of your temporary data file on your server.
#Must be the PATH to your temporary data data file, not the URL.  Use a file
#extension other than .tmp to avoid possible conflicts within script
  $tempdata="../contacts/dbase.temporary";

#B2. REQUIRED: The URL of this script file in your cgi-bin directory.
#You can provide the full URL, beginning with http:// (or, you can
#simply use the filename, i.e. "database.cgi")
  $thisurl="database.cgi";

#B3. OPTIONAL: You can format the opening and closing HTML
#of your results page in a separate file that can be written in
#regular HTML and saved on your server.  If your script can't
#find this file and open it, the default result screen is displayed
#instead.  For ease of configuration, place it in the same directory
#as your data file.  Note: this file must have three plusses +++
#where you want your search results inserted.  See help file
#under Template Problems: http://flattext.com/help/
#Note, you are providing a full or relative server PATH here.  A
#URL will not work.
  $openinghtml="../links/results.htm";

#B4. REQUIRED TO ADD, DELETE, OR MODIFY.  See
#important information in the help files about adding additional
#Security features to your script.

#Change password to any combination of letters (A-Z, a-z) and
#numbers 0-1.  USE ONLY LETTERS AND NUMBERS

  $adminpassword="palm4crav";

#B5. URL to send users to after posting, editing, or getting errors.
#This is usually the main page for this section of your site
 $forwardingURL="../contactsdatabase/test.htm";



#STEP C================================

#C1. Maximum number of matching records to display per page
  $maximumpage=25;

#C2. Maximum total number of records to display per search,
#for stylistic reasons, should be multiple of above number
  $maximum=1000;

@variablenames = $query->param;
foreach $variable (@variablenames){
$value=$query->param($variable);
$tempapprove{$variable}="$value";}

#STEP D================================
#You should not need to modify this section at all

#D1. Check to see if opening html file is on server
if (-e "$openinghtml"){

#D2. If so, open it and write opening and closing text to different strings
#to be used throughout the script
$problem="Can't open template file.  Make sure you are referencing the file and not just a directory.";
open(OPENING, "$openinghtml") || &security;
@wholefile=<OPENING>;
close(OPENING);
$fulltemplate=join("\n",@wholefile);
($templatestart,$templateend)=split(/\+\+\+/,$fulltemplate);}
else{

#D3. If template file not found, use this for now
$templatestart="<HTML><HEAD><TITLE>$templatetitle</TITLE></HEAD><BODY>";
$templateend="</BODY></HTML>";}
  $delimiter="\t";

#D6. Get Password Entered by User
  $checkpassword=$query->param('checkpassword');

#D7. Figure out what action user wants to take.
  $actiontotake=$query->param('actiontotake');

  $linenumberpass=$query->param('linenumberpass');

#D8. If user wants to delete record, and has already
#verified password, then go to the makechange subroutine
if ($actiontotake eq "Delete Record"){
$recordaction="Deleted";
&makechange;
exit;}

#D9. If user wants to edit record, and has already
#verified password, then go to the makechange subroutine
if ($actiontotake eq "Edit Record"){
$recordaction="Edited";
&makechange;
exit;}

#D10. If user wants to edit record, to go subroutine to verify
if ($actiontotake eq "Edit"){
&edit;
exit;}

#D11. If user wants to delete record, to go subroutine to verify
if ($actiontotake eq "Delete"){
&delete;
exit;}

#D12. If user wants to add record, go to add subroutine
if ($actiontotake eq "Add"){
&addrecord;
exit;}

#D13. If user wants to add record to temporary file
if ($actiontotake eq "Addtemp"){
&addtemp;
exit;}

#D14. If owner wants to evaluate records in temp file
if ($actiontotake eq "Scrolltemp"){
&scrolltemp;
exit;}

#D15. If user wants to update temp file
if ($actiontotake eq "Updatetemp"){
&updatetemp;
exit;}

#STEP E================================
#E1. Get the data passed from user
  $Name=$query->param('Name');
  $Namework=lc($Name);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Namework=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Namework eq "select"){
$Namework="";
$Name="";}
  $Namepass="$Name";

#E1. Get the data passed from user
  $Address=$query->param('Address');
  $Addresswork=lc($Address);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Addresswork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Addresswork eq "select"){
$Addresswork="";
$Address="";}
  $Addresspass="$Address";

#E1. Get the data passed from user
  $Telephone=$query->param('Telephone');
  $Telephonework=lc($Telephone);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Telephonework=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Telephonework eq "select"){
$Telephonework="";
$Telephone="";}
  $Telephonepass="$Telephone";

#E1. Get the data passed from user
  $Fax=$query->param('Fax');
  $Faxwork=lc($Fax);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Faxwork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Faxwork eq "select"){
$Faxwork="";
$Fax="";}
  $Faxpass="$Fax";

#E1. Get the data passed from user
  $Web=$query->param('Web');
  $Webwork=lc($Web);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Webwork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Webwork eq "select"){
$Webwork="";
$Web="";}
  $Webpass="$Web";

#E1. Get the data passed from user
  $Email=$query->param('Email');
  $Emailwork=lc($Email);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Emailwork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Emailwork eq "select"){
$Emailwork="";
$Email="";}
  $Emailpass="$Email";

#E1. Get the data passed from user
  $Description=$query->param('Description');
  $Descriptionwork=lc($Description);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Descriptionwork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Descriptionwork eq "select"){
$Descriptionwork="";
$Description="";}
  $Descriptionpass="$Description";

#E1. Get the data passed from user
  @Categoryarray=$query->param('Category');
  $searchelemcount=@Categoryarray;
if ($searchelemcount==1){
  @Categoryarray=split(/; /,$Categoryarray[0]);}
  $Category=join("\; ",@Categoryarray);
  $Category=~s/Select; //g;

  $Categorywork=lc($Category);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Categorywork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Categorywork eq "select"){
$Categorywork="";
$Category="";}
  $Categorypass="$Category";

#E1. Get the data passed from user
  $Book=$query->param('Book');
  $Bookwork=lc($Book);
#E1b. The line below chops characters that cause problems in Perl word searches
  $Bookwork=~tr/[a-zA-Z0-9 \.\,\?\@\-]/ /cd;
if ($Bookwork eq "select"){
$Bookwork="";
$Book="";}
  $Bookpass="$Book";

#E6. Get number of records already displayed
  $startitem=$query->param('startitem');

#E7. Figure the last record to display on this page
  $enditem=$startitem+$maximumpage;

#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Namework=~tr/[��������]/e/;
#$Namework=~tr/[��������������]/a/;
#$Namework=~tr/[��]/c/;
#$Namework=~tr/[��������]/i/;
#$Namework=~tr/[�����������]/o/;
#$Namework=~tr/[��������]/u/;
($Nameone, $Nametwo, $Namethree, $Namefour, $Namefive, $Namesix, $Nameseven)=split(/ /, $Namework);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Addresswork=~tr/[��������]/e/;
#$Addresswork=~tr/[��������������]/a/;
#$Addresswork=~tr/[��]/c/;
#$Addresswork=~tr/[��������]/i/;
#$Addresswork=~tr/[�����������]/o/;
#$Addresswork=~tr/[��������]/u/;
($Addressone, $Addresstwo, $Addressthree, $Addressfour, $Addressfive, $Addresssix, $Addressseven)=split(/ /, $Addresswork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Telephonework=~tr/[��������]/e/;
#$Telephonework=~tr/[��������������]/a/;
#$Telephonework=~tr/[��]/c/;
#$Telephonework=~tr/[��������]/i/;
#$Telephonework=~tr/[�����������]/o/;
#$Telephonework=~tr/[��������]/u/;
($Telephoneone, $Telephonetwo, $Telephonethree, $Telephonefour, $Telephonefive, $Telephonesix, $Telephoneseven)=split(/ /, $Telephonework);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Faxwork=~tr/[��������]/e/;
#$Faxwork=~tr/[��������������]/a/;
#$Faxwork=~tr/[��]/c/;
#$Faxwork=~tr/[��������]/i/;
#$Faxwork=~tr/[�����������]/o/;
#$Faxwork=~tr/[��������]/u/;
($Faxone, $Faxtwo, $Faxthree, $Faxfour, $Faxfive, $Faxsix, $Faxseven)=split(/ /, $Faxwork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Webwork=~tr/[��������]/e/;
#$Webwork=~tr/[��������������]/a/;
#$Webwork=~tr/[��]/c/;
#$Webwork=~tr/[��������]/i/;
#$Webwork=~tr/[�����������]/o/;
#$Webwork=~tr/[��������]/u/;
($Webone, $Webtwo, $Webthree, $Webfour, $Webfive, $Websix, $Webseven)=split(/ /, $Webwork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Emailwork=~tr/[��������]/e/;
#$Emailwork=~tr/[��������������]/a/;
#$Emailwork=~tr/[��]/c/;
#$Emailwork=~tr/[��������]/i/;
#$Emailwork=~tr/[�����������]/o/;
#$Emailwork=~tr/[��������]/u/;
($Emailone, $Emailtwo, $Emailthree, $Emailfour, $Emailfive, $Emailsix, $Emailseven)=split(/ /, $Emailwork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Descriptionwork=~tr/[��������]/e/;
#$Descriptionwork=~tr/[��������������]/a/;
#$Descriptionwork=~tr/[��]/c/;
#$Descriptionwork=~tr/[��������]/i/;
#$Descriptionwork=~tr/[�����������]/o/;
#$Descriptionwork=~tr/[��������]/u/;
($Descriptionone, $Descriptiontwo, $Descriptionthree, $Descriptionfour, $Descriptionfive, $Descriptionsix, $Descriptionseven)=split(/ /, $Descriptionwork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Categorywork=~tr/[��������]/e/;
#$Categorywork=~tr/[��������������]/a/;
#$Categorywork=~tr/[��]/c/;
#$Categorywork=~tr/[��������]/i/;
#$Categorywork=~tr/[�����������]/o/;
#$Categorywork=~tr/[��������]/u/;
($Categoryone, $Categorytwo, $Categorythree, $Categoryfour, $Categoryfive, $Categorysix, $Categoryseven)=split(/ /, $Categorywork);
#F4a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$Bookwork=~tr/[��������]/e/;
#$Bookwork=~tr/[��������������]/a/;
#$Bookwork=~tr/[��]/c/;
#$Bookwork=~tr/[��������]/i/;
#$Bookwork=~tr/[�����������]/o/;
#$Bookwork=~tr/[��������]/u/;
($Bookone, $Booktwo, $Bookthree, $Bookfour, $Bookfive, $Booksix, $Bookseven)=split(/ /, $Bookwork);


#STEP G================================
#Do not modify this section

#G1. Open datafile and write contents to an array, if can't open report the problem at the security subroutine
$problem="You do not have a file to search on the server.  Please ADD test records before trying to search your test data file.";
open (FILE, "$data") || &security;
@all=<FILE>;
close (FILE);

#G2.  The line below is required, do not modify
print "Content-type: text/html\n\n";

#G3. Display HTML Header
print "$templatestart\n";

#G5. Show words user searched for
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>You Searched For: <ul>\n";

if ($Name){
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><li>Name\=$Name</font>\n";}
if ($Address){
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><li>Address\=$Address</font>\n";}
if ($Telephone){
print "<FONT size=\"-1\"><li>Telephone\=$Telephone</font>\n";}
if ($Fax){
print "<FONT size=\"-1\"><li>Fax\=$Fax</font>\n";}
if ($Web){
print "<FONT size=\"-1\"><li>Web\=$Web</font>\n";}
if ($Email){
print "<FONT size=\"-1\"><li>Email\=$Email</font>\n";}
if ($Description){
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><li>Description\=$Description</font>\n";}
if ($Category){
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><li>Category\=$Category</font>\n";}
if ($Book){
print "<FONT size=\"-1\"><li>Book\=$Book</font>\n";}
print "</ul></font>\n";
#STEP H================================
#H1. Read each line of the data file, compare with search words

foreach $line (@all){
$line=~s/\n//g;
$loopsaround++;

$checkleng=length($line);
if ($checkleng<2){next};

$linetemp1=lc($line);

#H1a.  Support for European characters.  Uncomment and replace with your
#character set in brackets for all non-English Characters.  See help files.
#$linetemp1=~tr/[��������]/e/;
#$linetemp1=~tr/[��������������]/a/;
#$linetemp1=~tr/[��]/c/;
#$linetemp1=~tr/[��������]/i/;
#$linetemp1=~tr/[�����������]/o/;
#$linetemp1=~tr/[��������]/u/;

($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$linetemp1);

#H9. This line specifies the fields to sort results by
#See help databases for patches to allow various kinds of sorts
$line="$Name$delimiter$loopsaround$delimiter$line";

#H9.5 This line removes stray leading spaces before sorting your results
$line=~s/^ +//;

$increcount=0;
#H12. Look for matches in field named Name
if (($Name =~/\b$Nameone/ && $Name =~/\b$Nametwo/ && $Name =~/\b$Namethree/ && $Name =~/\b$Namefour/  && $Name =~/\b$Namefive/ && $Name=~/\b$Namesix/ && $Name=~/\b$Nameseven/) || !$Namework) {
$increcount++;}

#H12. Look for matches in field named Address
if (($Address =~/\b$Addressone/ && $Address =~/\b$Addresstwo/ && $Address =~/\b$Addressthree/ && $Address =~/\b$Addressfour/  && $Address =~/\b$Addressfive/ && $Address=~/\b$Addresssix/ && $Address=~/\b$Addressseven/) || !$Addresswork) {
$increcount++;}

#H12. Look for matches in field named Telephone
if (($Telephone =~/\b$Telephoneone/ && $Telephone =~/\b$Telephonetwo/ && $Telephone =~/\b$Telephonethree/ && $Telephone =~/\b$Telephonefour/  && $Telephone =~/\b$Telephonefive/ && $Telephone=~/\b$Telephonesix/ && $Telephone=~/\b$Telephoneseven/) || !$Telephonework) {
$increcount++;}

#H12. Look for matches in field named Fax
if (($Fax =~/\b$Faxone/ && $Fax =~/\b$Faxtwo/ && $Fax =~/\b$Faxthree/ && $Fax =~/\b$Faxfour/  && $Fax =~/\b$Faxfive/ && $Fax=~/\b$Faxsix/ && $Fax=~/\b$Faxseven/) || !$Faxwork) {
$increcount++;}

#H12. Look for matches in field named Web
if (($Web =~/\b$Webone/ && $Web =~/\b$Webtwo/ && $Web =~/\b$Webthree/ && $Web =~/\b$Webfour/  && $Web =~/\b$Webfive/ && $Web=~/\b$Websix/ && $Web=~/\b$Webseven/) || !$Webwork) {
$increcount++;}

#H12. Look for matches in field named Email
if (($Email =~/\b$Emailone/ && $Email =~/\b$Emailtwo/ && $Email =~/\b$Emailthree/ && $Email =~/\b$Emailfour/  && $Email =~/\b$Emailfive/ && $Email=~/\b$Emailsix/ && $Email=~/\b$Emailseven/) || !$Emailwork) {
$increcount++;}

#H12. Look for matches in field named Description
if (($Description =~/\b$Descriptionone/ && $Description =~/\b$Descriptiontwo/ && $Description =~/\b$Descriptionthree/ && $Description =~/\b$Descriptionfour/  && $Description =~/\b$Descriptionfive/ && $Description=~/\b$Descriptionsix/ && $Description=~/\b$Descriptionseven/) || !$Descriptionwork) {
$increcount++;}

#H11. Look for any word entered in this field
if (!$Categorywork){
$increcount++;
}else{
foreach $chunk (@Categoryarray){
$chunk=~s/\(/\\\(/g;
$chunk=~s/\)/\\\)/g;
if ($Category=~/\b$chunk/i){
$increcount++;
last;}}}
#H12. Look for matches in field named Book
if (($Book =~/\b$Bookone/ && $Book =~/\b$Booktwo/ && $Book =~/\b$Bookthree/ && $Book =~/\b$Bookfour/  && $Book =~/\b$Bookfive/ && $Book=~/\b$Booksix/ && $Book=~/\b$Bookseven/) || !$Bookwork) {
$increcount++;}

if ($line=~/markedtoedit/ && $actiontotake eq "markedtoedit"){
$line=~s/markedtoedit//g;
push (@keepers2,$line);}
$line=~s/markedtoedit//g;
if ($increcount==9){
push (@keepers,$line);}}

#STEP J================================
if ($actiontotake eq "markedtoedit"){
@keepers=@keepers2;}
#J1. Sort matches stored in array. 
@keepers=sort(@keepers);

#J2. Get and display number of matches found
$length1=@keepers;

#J3. If the number of matches is less than enditem, adjust
if ($length1<$enditem){
$enditem=$length1;
$displaystat="Y";}

#J4. The first field about to display
$disstart=$startitem+1;

#J5. Show user total number of matches found
if ($length1){
print "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>$length1 Matches Found (displaying $disstart to $enditem)<P>\n";
} else {
print "Your search found zero records, please try again.<P>\n";}

#STEP K================================
#K1. Do some HTML formatting before showing results
print "<table>\n";
#K4. Keep track of results processed on this page
foreach $line (@keepers){

#K5. Delete stray hard returns
$line=~s/\n//g;

#K6. Keep track of records displayed

$countline1++;

#K7. Decide whether or not this record goes on this page
if ($countline1>$startitem && $countline1<=$enditem){

#K8. Open each line of sorted array for displaying

($sortfield,$loopsaround,$Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$line);


#K15. Formatting for field Name.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Name){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Name:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>$Name</strong></td></tr>\n";}

#K15. Formatting for field Address.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Address){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Address:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Address</td></tr>\n";}

#K15. Formatting for field Telephone.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Telephone){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Telephone:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Telephone</td></tr>\n";}

#K15. Formatting for field Fax.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Fax){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Fax:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Fax</td></tr>\n";}

#K15. Formatting for field Web.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Web){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Web:</strong></td><td><a href=\"$Web\"><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Web</a></td></tr></font>\n";}

#K15. Formatting for field Email.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Email){
print "</u><tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Email:</strong></td><td><a href=\"mailto:$Email\"><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Email</a></td></tr></font>\n";}

#K15. Formatting for field Description.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Description){
print "</u><tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong></u>Description:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Description</td></tr>\n";}

#K15. Formatting for field Category.  If you add any HTML, make sure you
#put a backslash in front of all quote marks inside print statements
if ($Category){
print "<tr valign=top><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\"><strong>Category:</strong></td><td><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">$Category<hr></td></tr>\n";}
#K11. Check passwords before showing edit and delete buttons
if ($adminpassword eq $checkpassword){
print "<tr valign=top><td colspan=2><form method=POST action=\"$thisurl\"><input type=hidden name=\"linenumberpass\" value=\"$loopsaround\"><input type=hidden name=\"checkpassword\" value=\"$checkpassword\"><input type=submit name=\"actiontotake\" value=\"Edit\"> <input type=submit name=\"actiontotake\" value=\"Delete\"></td></tr></form>\n";}
print "<tr valign=top><td>&nbsp;</td><td>&nbsp;</td></tr>\n";

#STEP L================================
#L1. If total displayed equals maximum you set, then exit
if ($countline1 == $maximum && $maximum){
$problem2="Your search was terminated at $maximum records, please be more specific in your search";
last;}

#L2. If script just got to last match then exit program
if ($length1 == $countline1){
last;}

#L3. If script is at the end of a page then show NEXT button
if ($countline1 == $enditem && $displaystat ne "Y"  && $maximum>$countline1){
$stopit="Y";
last;
}

}}

print "</table>\n";

#L4. Display NEXT MATCHES button
if ($stopit eq "Y"){
print "<form method=POST action=\"$thisurl\">\n";

#L5. Pass hidden variables so script will know how to display next page
print "<input type=hidden name=\"Name\" value=\"$Namepass\"> \n";
print "<input type=hidden name=\"Address\" value=\"$Addresspass\"> \n";
print "<input type=hidden name=\"Telephone\" value=\"$Telephonepass\"> \n";
print "<input type=hidden name=\"Fax\" value=\"$Faxpass\"> \n";
print "<input type=hidden name=\"Web\" value=\"$Webpass\"> \n";
print "<input type=hidden name=\"Email\" value=\"$Emailpass\"> \n";
print "<input type=hidden name=\"Description\" value=\"$Descriptionpass\"> \n";
print "<input type=hidden name=\"Category\" value=\"$Categorypass\"> \n";
print "<input type=hidden name=\"Book\" value=\"$Bookpass\"> \n";
print "<input type=hidden name=\"checkpassword\" value=\"$checkpassword\"> \n";
print "<input type=hidden name=\"startitem\" value=\"$enditem\"> \n";
print "<input type=submit value=\"Get Next Matches\"></form>\n";
}

#L6. Show problems
print "<P>$problem2\n";


#L8. If opening.htm was found, show its closing html codes
print "$templateend\n";
exit;

#STEP M================================
sub security{
#M1. This is the subroutine that reports all problems
print "Content-type: text/html\n\n";

print "$templatestart\n";
print "<CENTER><FONT size=+2>Data Error</FONT></CENTER><P>\n";
print "<FONT size=\"+1\">Please correct the following error:<blockquote>$problem</blockquote></FONT>\n";
print "$templateend\n";
exit;
}

#STEP N================================
sub edit{
#N1. Open data file and read it
$problem="Can't open data file to read from it at edit subroutine";
open (FILE,"$data") || &security;
@all=<FILE>;
close (FILE);

#N2. Read each line of the data file
foreach $line (@all){
$line=~s/\n//g;
($copyName,$copyAddress,$copyTelephone,$copyFax,$copyWeb,$copyEmail,$copyDescription,$copyCategory,$copyBook,$skipthisfield)=split (/$delimiter/,$line);

$keepcount++;

#N3. Find the line user wants to modify
if ($keepcount==$linenumberpass){
$linetokeep=$line;
$linetokeep=~s/markedtoedit//g;
last;
}
}

#N4. Check password sent via hidden field
if ($adminpassword ne $checkpassword){
$problem="Your password does not match the master password and appears to have been changed since logging onto this record.";
&security;}

#N6. Split matching line into its respective variables
($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$linetokeep);

#Required Header, do not delete
print "Content-type: text/html\n\n";

#N8. If can't find opening html, display default header
print "$templatestart\n";

print "<P><CENTER><FONT size=\"+1\">Edit this Record</FONT></CENTER><P>\n";

print "<BR><FORM ACTION=\"$thisurl\" METHOD=\"POST\" $allowupload><table>\n";
print "<tr valign=top><td>Name:</td><td><input type=text name=\"Name\"  value=\"$Name\" size=35></td></tr>\n";
$Address=~s/<br>/\n/g;
$Address=~s/<BR>/\n/g;
print "<tr valign=top><td>Address:</td><td><textarea name=\"Address\" cols=45 rows=10>$Address</textarea></td></tr>\n";
print "<tr valign=top><td>Telephone:</td><td><input type=text name=\"Telephone\"  value=\"$Telephone\" size=35></td></tr>\n";
print "<tr valign=top><td>Fax:</td><td><input type=text name=\"Fax\"  value=\"$Fax\" size=35></td></tr>\n";
print "<tr valign=top><td>Web:</td><td><input type=text name=\"Web\"  value=\"$Web\" size=35></td></tr>\n";
print "<tr valign=top><td>Email:</td><td><input type=text name=\"Email\"  value=\"$Email\" size=35></td></tr>\n";
$Description=~s/<br>/\n/g;
$Description=~s/<BR>/\n/g;
print "<tr valign=top><td>Description:</td><td><textarea name=\"Description\" cols=45 rows=10>$Description</textarea></td></tr>\n";
@Category=split(/\; /,$Category);
foreach $content (@Category){
$content=~tr/[a-zA-Z0-9]//cd;
$checkCategory{$content}="checked";}
print "<tr valign=top><td>Category:</td><td>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Anarchism'} value=\"Anarchism\"> Anarchism<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'AnimalRights'} value=\"Animal Rights\"> Animal Rights<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'AntiCapitalism'} value=\"Anti-Capitalism\"> Anti-Capitalism<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'AntiRacism'} value=\"Anti-Racism\"> Anti-Racism<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'AntiWar'} value=\"Anti-War\"> Anti-War<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Benefits'} value=\"Benefits\"> Benefits<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Bookshops'} value=\"Bookshops\"> Bookshops<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'CommunityGroups'} value=\"Community Groups\"> Community Groups<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'SocialCommunityCentres'} value=\"Social & Community Centres\"> Social & Community Centres<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'ChildrenParenting'} value=\"Children & Parenting\"> Children & Parenting<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Culture'} value=\"Culture\"> Culture<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'DisabilityRightsAction'} value=\"Disability Rights & Action\"> Disability Rights & Action<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Drugs'} value=\"Drugs\"> Drugs<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Economics'} value=\"Economics\"> Economics<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Education'} value=\"Education\"> Education<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Energy'} value=\"Energy\"> Energy<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Environment'} value=\"Environment\"> Environment<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'FoodFarming'} value=\"Food & Farming\"> Food & Farming<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'ForestsWoodland'} value=\"Forests & Woodland\"> Forests & Woodland<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Gardening'} value=\"Gardening\"> Gardening<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Genetics'} value=\"Genetics\"> Genetics<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Health'} value=\"Health\"> Health<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'HousingHomelessness'} value=\"Housing & Homelessness\"> Housing & Homelessness<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'HumanRights'} value=\"Human Rights\"> Human Rights<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'IndigenousPeoples'} value=\"Indigenous Peoples\"> Indigenous Peoples<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'JusticeTheLaw'} value=\"Justice & The Law\"> Justice & The Law<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'LandRightsPlanning'} value=\"Land Rights & Planning\"> Land Rights & Planning<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Media'} value=\"Media\"> Media<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaDistributionandPublishers'} value=\"Media - Distribution and Publishers\"> Media - Distribution and Publishers<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaFilmVideoTV'} value=\"Media - Film, Video & TV\"> Media - Film, Video & TV<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaInternetNewsServices'} value=\"Media - Internet News Services\"> Media - Internet News Services<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaOverseas'} value=\"Media - Overseas\"> Media - Overseas<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaUKNational'} value=\"Media - UK National\"> Media - UK National<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'MediaUKLocalNews'} value=\"Media - UK Local News\"> Media - UK Local News<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'NetworkingSupport'} value=\"Networking Support\"> Networking Support<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'PrisonerSupport'} value=\"Prisoner Support\"> Prisoner Support<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Privacy'} value=\"Privacy\"> Privacy<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Refugees'} value=\"Refugees\"> Refugees<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Sexuality'} value=\"Sexuality\"> Sexuality<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Transport'} value=\"Transport\"> Transport<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Travellers'} value=\"Travellers\"> Travellers<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'Women'} value=\"Women\"> Women<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'WorkersRights'} value=\"Workers' Rights\"> Workers' Rights<BR>\n";
print "<input type=checkbox name=\"Category\" $checkCategory{'InformationTechnologyInternet'} value=\"Information Technology & Internet\"> Information Technology & Internet<BR>\n";
print "</td></tr>\n";
$Book=~s/\"//g;
print "<tr valign=top><td>Book:</td><td>
<input type=radio name=\"Book\" value=\"$Book\" checked>$Book<BR>

\n";
print "<input type=radio name=\"Book\" value=\"Yes\">Yes<BR>\n";
print "<input type=radio name=\"Book\" value=\"No\">No<BR>\n";
print "</select></td></tr>\n";
#N10. Pass values to next screen
print "</table>\n";
print "<input type=hidden name=\"linenumberpass\" value=\"$linenumberpass\">\n";
print "<input type=hidden name=\"checkpassword\" value=\"$checkpassword\">\n";
print "<input type=submit name=\"actiontotake\" value=\"Edit Record\"></form><P>\n";

print "$templateend\n";
exit;
}

#STEP O================================
sub delete{
#O1. Open data file and read it
$problem="Can't open data file to read from it at delete subroutine";
open (FILE,"$data") || &security;
@all=<FILE>;

close (FILE);
#O2. Read each line of the file
foreach $line (@all){
$line=~s/\n//g;
($copyName,$copyAddress,$copyTelephone,$copyFax,$copyWeb,$copyEmail,$copyDescription,$copyCategory,$copyBook,$skipthisfield)=split (/$delimiter/,$line);

$keepcount++;
#O3. Find line to delete
if ($keepcount==$linenumberpass){
$linetokeep=$line;
$linetokeep=~s/markedtoedit//g;
last;
}
}
($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$linetokeep);

#O4. Check password sent via hidden field
if ($adminpassword ne $checkpassword){
$problem="Your password does not match the master password.";
&security;}

#O6. Requred Header, do not delete
print "Content-type: text/html\n\n";

print "$templatestart\n";

print "<P><CENTER><FONT size=\"+1\">Delete this Record?</FONT></CENTER><P>\n";
($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$linetokeep);

#O7. Show validation HTML
print "<form method=POST action=\"$thisurl\">\n";
print "<table>\n";
print "<tr valign=top><td>Name: </td><td>$Name</td></tr>\n";
print "<tr valign=top><td>Address: </td><td>$Address</td></tr>\n";
print "<tr valign=top><td>Telephone: </td><td>$Telephone</td></tr>\n";
print "<tr valign=top><td>Fax: </td><td>$Fax</td></tr>\n";
print "<tr valign=top><td>Web: </td><td>$Web</td></tr>\n";
print "<tr valign=top><td>Email: </td><td>$Email</td></tr>\n";
print "<tr valign=top><td>Description: </td><td>$Description</td></tr>\n";
print "<tr valign=top><td>Category: </td><td>$Category</td></tr>\n";
print "<tr valign=top><td>Book: </td><td>$Book</td></tr>\n";
print "</table><BR>\n";
print "<input type=hidden name=\"linenumberpass\" value=\"$linenumberpass\">\n";
print "<input type=hidden name=\"checkpassword\" value=\"$checkpassword\">\n";
print "<input type=submit name=\"actiontotake\" value=\"Delete Record\"></form><P>\n";
#If opening.htm was not found, show default closing html codes
print "$templateend\n";
exit;
}
#STEP P================================
sub makechange{
#P1.  For each variable, translate it, remove any delimiters that
#user may have accidentally included, replace hard returns with
#HTML line breaks, and delete all carriage returns
#Go to get variable subroutine and make sure add preferences apply
 if ($recordaction eq "Edited"){
&getvariables;}
#P2. This step either replaces or empties the existing line
if ($recordaction eq "Deleted"){
  $replacementline="";}
else{
  $replacementline="$Name$delimiter$Address$delimiter$Telephone$delimiter$Fax$delimiter$Web$delimiter$Email$delimiter$Description$delimiter$Category$delimiter$Book";}
$problem="Can't open data file to read from it";
open (FILE,"$data") || &security;
@all=<FILE>;

close (FILE);
$linenumberpass--;
$all[$linenumberpass]=$replacementline;
$problem="Can't open temporary file.  You need to chmod 777 the <B>directory</B> your data file is in.  See the help files under Permissions for Class B Scripts.";
#P6. Write the entire changed file to a temporary file
open (FILE2,">$data.tmp") || &security;
foreach $line (@all){
$line=~s/\n//g;
print FILE2 "$line\n";}

close(FILE2);

#P7. Rename the temp file to your master data file
$problem="Can't rename file after making change";
rename("$data.tmp", "$data") || &security;
print "Content-type: text/html\n\n";

#P8. If can't find opening html, display default header
print "$templatestart\n";

print "Your record has been $recordaction.  Please click <A href=\"$forwardingURL\">here</A> to continue.\n";

close (FILE);
#If opening.htm was not found, show default closing html codes
print "$templateend\n";
exit;
}
#STEP Q================================
#This subroutine adds records to your database
sub addrecord{
#Q1. Check password
if ($adminpassword ne $checkpassword && $adminpassword){
$problem="The password you entered does not match your administration password.  Please press BACK on your browser to fix this problem.";
&security;}
&getvariables;

  $replacementline="$Name$delimiter$Address$delimiter$Telephone$delimiter$Fax$delimiter$Web$delimiter$Email$delimiter$Description$delimiter$Category$delimiter$Book";
#Q3. Write the new record to the bottom of the data file
$problem="Can't write to the data file.  Please verify its location and change its permissions to 777.";
open (FILE2,">>$data") || &security;
print FILE2 "$replacementline\n";
close(FILE2);
print "Content-type: text/html\n\n";

#Q4. If can't find opening html, display default header
print "$templatestart\n";

print "Your record has been added.  Please click <A href=\"$forwardingURL\">here</A> to continue.\n";

#If opening.htm was not found, show default closing html codes
print "$templateend\n";
exit;
}
#STEP R================================
sub getvariables{
#R1. This step checks your variables before adding/editing them
  $Name=$query->param('Name');

  $Address=$query->param('Address');

  $Telephone=$query->param('Telephone');

  $Fax=$query->param('Fax');

  $Web=$query->param('Web');

  $Email=$query->param('Email');

  $Description=$query->param('Description');

  @Category=$query->param('Category');

  $Category=join("\; ",@Category);

  $Category=~s/Select; //g;

  $Book=$query->param('Book');

#R3. Replace hard returns with <BR>, cut carriage returns
$Name=~s/\n/<br>/g;
$Name=~s/\r//g;
if ($Name eq "Select"){
$Name="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Address=~s/\n/<br>/g;
$Address=~s/\r//g;
if ($Address eq "Select"){
$Address="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Telephone=~s/\n/<br>/g;
$Telephone=~s/\r//g;
if ($Telephone eq "Select"){
$Telephone="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Fax=~s/\n/<br>/g;
$Fax=~s/\r//g;
if ($Fax eq "Select"){
$Fax="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Web=~s/\n/<br>/g;
$Web=~s/\r//g;
if ($Web eq "Select"){
$Web="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Email=~s/\n/<br>/g;
$Email=~s/\r//g;
if ($Email eq "Select"){
$Email="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Description=~s/\n/<br>/g;
$Description=~s/\r//g;
if ($Description eq "Select"){
$Description="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Category=~s/\n/<br>/g;
$Category=~s/\r//g;
if ($Category eq "Select"){
$Category="";}
#R3. Replace hard returns with <BR>, cut carriage returns
$Book=~s/\n/<br>/g;
$Book=~s/\r//g;
if ($Book eq "Select"){
$Book="";}

#R10. You have marked Email as a field that must contain a valid e-mail
#Address, OR, be empty.  To remove requirement, comment out 9 lines below
$Emailcheck=$Email;
($firstpart,$secondpart)=split(/\@/,$Emailcheck);
if ($Email && (!$firstpart || !$secondpart || $secondpart!~/\./)){
$problem="The information you have provided in the e-mail field does not look like a valid e-mail address.  Please press back on your browser and fix this problem.";
&security;}

#R11. Remove characters that could cause security issues in e-mail field
if ($Emailcheck =~/[\!\|\~\^\'\"]/){
$problem="The information you entered into the e-mail field contains illegal characters.  This field should contain letters, numbers, the \@ symbol, and periods only.  Please press BACK and fix this problem.";
&security;}

}
#STEP S================================
sub addtemp{
#S1. This subroutine adds records to your temporary file for approval
#S2. Check variable sent
&getvariables;

#S3. Randomize in preparation for random generator
srand();
#S4. Get IP address of person posting record
$ipstamp=$ENV{'REMOTE_ADDR'};

#S5. Generate a large random number to serve as key
$randnumb=int(rand(9999999));
  $replacementline="$ipstamp&&temp$randnumb(\+\+)$Name$delimiter$Address$delimiter$Telephone$delimiter$Fax$delimiter$Web$delimiter$Email$delimiter$Description$delimiter$Category$delimiter$Book";
#S6. Write the temp record to the bottom of the 
$problem="Can't write to the data file.  Please verify its location and change its permissions to 777.";
open (FILE2,">>$tempdata") || &security;
print FILE2 "$replacementline\n";
close(FILE2);
print "Content-type: text/html\n\n";

print "$templatestart\n";

#S7. Acknowledge that record has been posted
print "Your record has been sent for evaluation.  Please click <A href=\"/links/\">here</A> to continue.\n";

print "$templateend\n";
exit;
}
#STEP T================================
sub scrolltemp{
#T1. This step is your interface with the temp file
#T2. Check password
if ($adminpassword ne $checkpassword && $adminpassword){
$problem="The password you entered does not match your administration password.  Please press BACK on your browser to fix this problem.";
&security;}
#T3. Check to make sure that the data file can be opened.
$problem="Unable to open your temporary data file.  It either contains no records, or the path to it is incorrect.";
open (FILE, "$tempdata") || &security;
@all=<FILE>;
close (FILE);

print "Content-type: text/html\n\n";

#T4. Start showing contents of data file
print "$templatestart\n";
print "<form method=POST action=\"$thisurl\"><input type=hidden name=\"checkpassword\" value=\"$checkpassword\"><input type=hidden name=\"actiontotake\" value=\"Updatetemp\">\n";
$checktemp=@all;
if (!$checktemp){
print "Your temporary file contains no records for you to evaluate at this time.  Please click <A href=\"$forwardingURL\">here</A> to continue.<P>\n";
print "$templateend\n";
exit;}
print "<FONT size=\"-1\"><B>KEY</B><BR> A=Add to Database<BR> D=Delete from Temp File<BR>E=Add to Database but Mark for Editing<BR>H=Hold in Temp File for Decision Later<BR><BR></FONT>\n";
print "<table>\n";
print "<tr valign=top><td><B>A</B></td><td><B>D</B></td><td><B>E</B></td><td><B>H</B></td><td>Field</td><td>Contents</td>\n";

foreach $line (@all){
$line=~s/\n//g;
$checkleng=length($line);
if ($checkleng<2){next};

($indexvalues,$stringvalues)=split(/\(\+\+\)/,$line);
($ipaddress,$uniqueapproval)=split(/&&/,$indexvalues);
($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$stringvalues);

print "<tr><td><input type=radio name=\"$uniqueapproval\" value=A checked></td><td><input type=radio name=\"$uniqueapproval\" value=D></td><td><input type=radio name=\"$uniqueapproval\" value=E></td><td><input type=radio name=\"$uniqueapproval\" value=H></td><td>IP Address:</td><td>$ipaddress</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Name:</td><td>$Name</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Address:</td><td>$Address</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Telephone:</td><td>$Telephone</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Fax:</td><td>$Fax</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Web:</td><td>$Web</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Email:</td><td>$Email</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Description:</td><td>$Description</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Category:</td><td>$Category</td></tr>\n";

print "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>Book:</td><td>$Book</td></tr>\n";

$keeptrack2++;
if ($keeptrack2>10){
last};
}
print "</table>\n";

if ($keeptrack2>=10){
print "<input type=submit value=\"Update and Get Next Set\"></form>\n";}

else{
print "<input type=submit value=\"Update\"></form>\n";}

print "$templateend\n";
exit;
}

#STEP U================================
sub updatetemp{
#U1. This step makes changes from temp file
#U2. Check password
if ($adminpassword ne $checkpassword && $adminpassword){
$problem="The password you entered does not match your administration password.  Please press BACK on your browser to fix this problem.";
&security;}
$problem="Unable to open your temporary data file.  It either contains no records, or the path to it is incorrect.";
open (FILE, "$tempdata") || &security;
@all=<FILE>;
close (FILE);

foreach $line (@all){
$line=~s/\n//g;
$checkleng=length($line);
if ($checkleng<2){next};

($indexvalues,$stringvalues)=split(/\(\+\+\)/,$line);
($ipaddress,$uniqueapproval)=split(/&&/,$indexvalues);
($Name,$Address,$Telephone,$Fax,$Web,$Email,$Description,$Category,$Book,$skipthisfield)=split (/$delimiter/,$stringvalues);

if ($tempapprove{$uniqueapproval} eq "A"){
push(@recordstoadd,$stringvalues);}
elsif ($tempapprove{$uniqueapproval} eq "D"){
push(@recordstodelete,$stringvalues);}
elsif ($tempapprove{$uniqueapproval} eq "E"){
push(@recordstoedit,$stringvalues);}
else {
push(@recordstohold,$line);}
}
$problem="Unable to open data file to add records.  Check path to it and its permissions.";
open (FILE, ">>$data") || &security;
foreach $line (@recordstoadd){
$line=~s/\n//g;
print FILE "$line\n";}
close(FILE);
$problem="Unable to open data file to records to edit.  Check path to it and its permissions.";
open (FILE, ">>$data") || &security;
foreach $line (@recordstoedit){
$line=~s/\n//g;
print FILE "markedtoedit$line\n";}
close(FILE);
$problem="Unable to open temporary file to refresh data.  Check path to it and its permissions.";
open (FILE, ">$tempdata") || &security;
foreach $line (@recordstohold){
$line=~s/\n//g;
print FILE "$line\n";}
close(FILE);
$checkhold=@recordstohold;
if ($checkhold){
&scrolltemp;}
print "Content-type: text/html\n\n";

print "$templatestart\n";
print "<P>Your actions have been taken.  Please click <A href=\"$forwardingURL\">here</A> to continue.<P>\n";
print "$templateend\n";
exit;
}

#This is the last line of the script


