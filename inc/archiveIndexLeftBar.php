<div align="center">
			<p><font face="Arial, Helvetica, sans-serif"><b><font color="#000000">
				A Complete Archive Of All SchNEWS Issues going back to 1994...
			</font></b></p>

			<?php

				// Links to previous issues
				require "../archive/inc/backIssueLinks.php";

			?>

			<p><b>
				<font color="#000000" size="1" face="Arial, Helvetica, sans-serif">
					Subscribe to our RSS Feed
				</font>
			</b>
			<a href="../pages_menu/defunct.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image30','','../images_main/rss-button-mo.gif',1)">
				<img name="Image30" border="0" src="../images_main/rss-button.gif" width="110" height="25"
				alt="SchNEWS RSS feed, click here for information on how to subscribe">
			</a>
		</div>

		<p><font face="Arial, Helvetica, sans-serif" color="#000000"><b>
			Read about the early days of SchNEWS...
		</b></font></p>

		<p><font face="Arial, Helvetica, sans-serif" color="#000000"><i>
			<a href="news50.htm">CJA Arrestometer</a>
		</i></font></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/round-squatters-estate-agency.htm">Squatters Estate Agency</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news11.htm">Pollok Free State</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news20.htm">Stanworth Valley 'village in the trees'</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news88.htm">Reclaim The Streets</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news57.htm">Newbury <br /> Bypass</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news79.htm">McLibel</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news93.htm">Liverpool Dockers</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news70.htm">Fairmile</a>
		</font></i></p>

		<p><i><font face="Arial, Helvetica, sans-serif" color="#000000">
			<a href="../archive/news92.htm">Exodus Collective</a>
		</font></i></p>