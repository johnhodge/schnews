<!--  Include for the Left Links Bar -->




		<!-- DONATE BUTTON -->
		<table width="110" style="border-color:#000000; border-style:solid; border-width:1px" align="center">
			<tr>
				<td height="2">
					<p align="center">
						<font face="Arial, Helvetica, sans-serif" size="1">
							<b>SchNEWS - on the blag for money <BR>since 1994..</b>.<br />
						</font>
						<a 	href="../extras/help.htm"
							onMouseOut="MM_swapImgRestore()"
							onMouseOver="MM_swapImage('Image45','','../images_main/donate-mo.gif',1)">
								<img 	name="Image45"
										border="0"
										src="../images_main/donate.gif"
										width="84"
										height="29"
										alt="Send us a donation via PayPal or cheque" />
						</a>
					</p>
				</td>
			</tr>
		</table>


		<!--	REST OF THE LEFT BAR

				Due to the shitness of IE rendering engine it is better to leave this block of code as it is with no
				other white space between tags, otherwise IE renders parts of the white spaces and messes up the display. -->
		<?php echo make_horiz_divider(90, '888888'); ?>


		<!-- 	Join Us on Twitter	-->
        <span style='font-size:10px'>
        	Join us on <a href='http://twitter.com/schnews' target="_blank">Twitter</a>
        </span>

        <?php echo make_horiz_divider(90, "888888"); ?>


        <!-- 	RSS Feed 	-->
        <div align='center'>
        <a href="../pages_menu/defunct.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','','../images_main/rss-button-mo.gif',1)">
        	<img name="Image40" border="0" src="../images_main/rss-button.gif" width="110" align='center' alt="SchNEWS RSS feed, click here for information on how to subscribe">
        </a>
        </div>

        <?php echo make_horiz_divider(90, "888888"); ?>

        <!-- Subscribe to Mailing Lists -->
        <div align='center' style='font-color: black'>
        	<div style='font-weight: bold; font-size: 9px'>
				<a href='http://www.schnews.org.uk/pages_menu/subscribe.php'>
					Sign Up to our Mailing List
				</a>
        	</div>
        	<div style='font-size: 9px'>
        		Get SchNEWS in yer inbox every week
        	</div>
        </div>




        <?php echo make_horiz_divider(90, "888888"); ?>


		<!--  Raiders of the Lost Archive Link -->
		<div align='center'>
		<a href="../pages_merchandise/merchandise_video.php#raiders">
    		<img style="border: 1px solid black" align='center' src='../images_main/raider-banner-110.jpg' width="110" border="0" alt="Raider of the Lost Archive" />
    	</a>
    	</div>

		<br />

    	<!-- Paypal By Now Link -->
    	<!-- 	Buy ROTLA Only 	-->
		<form style="padding:0; margin: 0" action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	    <font size="2" face="Arial, Helvetica, sans-serif">
	    <input type="hidden" name="cmd" value="_xclick" />
	    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
	    <input type="hidden" name="undefined_quantity" value="1" />
	    <input type="hidden" name="item_name" value="RAIDERS OF THE LOST ARCHIVE (Vol 1) - SchMOVIES 2009/2010 Anthology" />
	    <input type="hidden" name="item_number" value="RAIDERS OF THE LOST ARCHIVE (Vol 1) - SchMOVIES 2009/2010 Anthology" />
	    <input type="hidden" name="amount" value="6.00" />
	    <input type="hidden" name="no_shipping" value="2" />
	    <input type="hidden" name="return" value="http://www.schnews.org.uk/extras/complete.htm" />
	    <input type="hidden" name="cancel_return" value="http://www.schnews.org.uk/extras/cancel.htm" />
	    <input type="hidden" name="currency_code" value="GBP" />
	    <input type="hidden" name="lc" value="GB" />
	    <input type="hidden" name="bn" value="PP-BuyNowBF" />
	    </font>

	    <font size="2" face="Arial, Helvetica, sans-serif">
        <input type="image" src="../images_main/buyNow.gif" class="backgroundColorChange"style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
        </font>
        <br />
		<font style='font-size:8px' face="Arial, Helvetica, sans-serif">
			&pound;6 per copy (inc. P&amp;P) <br />(via Paypal)
		</font>
		</form>



		<?php echo make_horiz_divider(90, "ff6600", 2); ?>




		<!-- 	Satire Link -->
		<A href="../satire/index.html" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image48','','../images_main/satire-button-over.png',1)">
			<IMG NAME="Image48" BORDER="0" SRC="../images_main/satire-button.png" WIDTH="110" HEIGHT="40">
		</A>

		<?php echo make_horiz_divider(90, "ff6600", 2); ?>


		<!-- 	Book Reviews Link 	-->
		<a href="../book-reviews/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image43','','../images_main/book-reviews-button-mo.gif',1)">
			<img name="Image43" border="0" src="../images_main/book-reviews-button.gif" width="110" alt="Books (and films) reviewed by SchNEWS hacks">
		</a>

		<?php echo make_horiz_divider(90, "ff6600", 2); ?>


		<!-- 	Merchandise Link	-->
		<a href="../pages_merchandise/merchandise.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image44','','../images_main/merchandise-button-mo.gif',1)">
			<img name="Image44" border="0" src="../images_main/merchandise-button.gif" width="110" height="45" alt="DVDs, books, t-shirts and other SchNEWS merchandise">
		</a>

		<?php echo make_horiz_divider(90, "ff6600", 2); ?>


		<!-- 	Merchandise T-Shirts Links	-->
		<a href="../pages_merchandise/merchandise_tshirts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchNEWS T-Shirts','','../images_main/t-shirts-button-over.png',1)">
			<img name="SchNEWS T-Shirts" border="0" src="../images_main/t-shirts-button.png" width="110" height="40">
		</a>

		<?php echo make_horiz_divider(90, "ff6600", 2); ?>


		<?php

			//	Only include the videos if needed...
			if (isset($includeLeftBarVideos) && is_bool($includeLeftBarVideos) && $includeLeftBarVideos)	{

		?>


		<!--	Raiders of the Lost Archive	-->
		<p align='center'><b>
		<a href="../pages_merchandise/merchandise_video.php#raiders">
			<img src="http://www.schnews.org.uk/schmovies/images/raiders-100.jpg" width="100" height="141" border="0" alt="Raiders of the Lost Archive" />
		</a>
		</b></p>

		<!--	Reports from the Verge		-->
		<p align='center'><b>
		<a href="../pages_merchandise/merchandise_video.php#rftv">
			<img src="http://www.schnews.org.uk/schmovies/images/rftv-100.jpg" width="100" height="141" border="0" alt="Reports from the Verge">
		</a>
		</b></p>


		<!--	Uncertified		-->
		<p align='center'><b>
		<a href="../pages_merchandise/merchandise_video.php#uncertified">
			<img src="http://www.schnews.org.uk/schmovies/images/uncertified-100.jpg" width="100" height="141" border="0" alt="Uncertified - SchMOVIES DVD Collection 2008">
		</a>
		</b></p>

		<!--	On the Verge	-->
		<p align='center'>
		<a href="schmovies/index-on-the-verge.htm">
			<img src="http://www.schnews.org.uk/schmovies/images/on-the-verge-100.jpg" alt="On The Verge" border="0">
		</a>
		</p>

		<!--	Take Three	-->
		<p align='center'>
		<a href="pages_merchandise/merchandise_video.php#take3">
			<img src="http://www.schnews.org.uk/schmovies/images/take-three-100.jpg" width="100" height="142" border="0" alt="Take Three - the SchMOVIES DVD collection 2007">
		</a>
		</p>

		<!--	V for Video Activist	-->
		<p align="center">
		<a href="pages_merchandise/merchandise_video.php#v">
			<img src="http://www.schnews.org.uk/schmovies/v-for-video-activist-100.jpg" width="100" border="0" alt="V For Video Activist - SchNEWS DVD Collection 2006"></a></p><p align="center"><b><a href="pages_merchandise/merchandise_books.php#schnewsat10"><img src="http://www.schnews.org.uk/at10/schnewsattensmall.jpg" width="100" border="1" alt="SchNEWS At Ten - our first decade in one handy book" />
		</a>
		</p>

		<p align="center"><a href="pages_merchandise/stickers.html"><img src="images_main/sticker.gif" width="100" height="70" border="0" alt="SchNEWS stickers - sold out but you can download the templates and print your own" />
		</a>
		</p>

		</b>


		<?php

			//	End the videos conditional...
			}
			echo "<br />";

		?>


		<!-- 	Radiohead Commendation	-->
		<span style='color: black; font-size: 9px; font-family: Arial, Helvetica, sans-serif; font-weight: bold'>
			&quot;Definitely one of the best party and protest sites to come out of the UK. Updated weekly,
			brilliantly written, bleakly humourous, and essential reading for anyone who gives
			a shit. And we all should.&quot;
		</span>
		<span style='font-size: 10px; font-weight: bold; font-family: Arial, Helvetica, sans-serif'>
			- <i>Radiohead</i>
			<br />
			<A href="../pages_merchandise/bookreviews.html">
			Other Reviews
			</A>
		</span>