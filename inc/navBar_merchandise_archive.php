<!-- Navigation Bar for Merchandise Pages -->

<?php
//	Init variable...
if (!isset($merch_page))
{
	$merch_page = '';
}


//	Begin output buffer...
$output		=	array();

$output[]	=	"<div class='merchandise_navBar'>";


//	Home
$class		=	'merch_navBarItem';
if ($merch_page == 'home') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='\'>Home</a>";


$output[]	=	"&nbsp;|&nbsp;";


//	Merch Index
$class		=	'merch_navBarItem';
if ($merch_page == 'index') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/merchandise.php'>SchNEWS Merchandise Index</a>";


$output[]	=	"&nbsp;&nbsp;|&nbsp;&nbsp;";


//	Merch Books
$class		=	'merch_navBarItem';
if ($merch_page == 'books') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/merchandise_books.php'>Books</a>";


$output[]	=	"&nbsp;&nbsp;|&nbsp;&nbsp;";


//	Merch Videos
$class		=	'merch_navBarItem';
if ($merch_page == 'video') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/merchandise_video.php'>Videos</a>";


$output[]	=	"&nbsp;&nbsp;|&nbsp;&nbsp;";


//	Merch T-Shirts
$class		=	'merch_navBarItem';
if ($merch_page == 'tshirts') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/merchandise_tshirts.php'>T-Shirts</a>";


$output[]	=	"&nbsp;&nbsp;|&nbsp;&nbsp;";


//	Merch Stickers
$class		=	'merch_navBarItem';
if ($merch_page == 'stickers') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/stickers.php'>Stickers</a>";


$output[]	=	"&nbsp;&nbsp;|&nbsp;&nbsp;";


//	Merch Stickers
$class		=	'merch_navBarItem';
if ($merch_page == 'misc') $class = 'merch_navBarItem_selected';
$output[]	=	"<a class='$class' href='../pages_merchandise/misc.php'>Misc</a>";


$output[]	=	"</div>";


//	Flush Output Buffer
echo implode("\r\n", $output);



?>
