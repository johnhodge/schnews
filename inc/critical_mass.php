<!--  CRITICAL MASS LISTINGS -->


<p>
<font face="Arial, Helvetica, sans-serif">
<b>
<font size="4">
<a name="crit-mass"></a>
</font>

<font size="3">CRITICAL MASS</font>
</b>
</font>

</p>




            <ul>
              <li><font face="Arial, Helvetica, sans-serif"><b>Birmingham:</b>
                meet at 5:30pm on the last Friday of every month in Pigeon Park
                (St Philip&#146;s Cathedral) and set off at about 6pm for a spontaneous
                ride around the city centre <a href="https://lists.riseup.net/www/info/brumcriticalmass" target="_blank">https://lists.riseup.net/www/info/brumcriticalmass</a></font>
              </li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Brighton - </b>
                meet 6pm on the last Friday of every month at the Level (BN2 3FX).
                Come join us! Bring bikes, lights and noise! <a href="http://groups.yahoo.com/group/criticalmassbrighton" target="_blank">http://groups.yahoo.com/group/criticalmassbrighton</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Bristol </b>- meet
                5.30pm at the fountains opposite the Hippidrome. For more
                info see: <a href="http://criticalmass.wikia.com/wiki/Bristol" target="_blank">http://criticalmass.wikia.com/wiki/Bristol</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Cambridge</b> -
                meet 6.30pm on the last Friday of the month at Market Square.
                For more info see <a href="http://criticalmass.cambridgeaction.net" target="_blank">http://criticalmass.cambridgeaction.net</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>London - </b>meets
                6.30pm on the last Friday of every month on the South Bank under
                Waterloo Bridge, by the National Film Theatre. For more see <a href="http://www.criticalmasslondon.org.uk" target="_blank">www.criticalmasslondon.org.uk</a>
                </font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Manchester </b>
                - meets 6pm on the last friday of every month at Central Library,
                Manchester. For more info see: <a href="http://myspace.com/mcrcriticalmass" target="_blank">http://myspace.com/mcrcriticalmass</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Nottingham</b>
                - 5.30pm on the last Friday of every month meet at the meeting
                point in Market Square, 5:30pm. For more info <a href="http://www.criticalmassrides.info/nottingham" target="_blank">www.criticalmassrides.info/nottingham</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Oxford</b> - meet
                5.45pm on the last Friday of every month at the Cornmarket end
                of Broad Street outside The Oxford Story Leaving around 6ish.
                For more see <a href="http://www.myspace.com/oxfordcriticalmass" target="_blank">www.myspace.com/oxfordcriticalmass</a>
                Email <a href="mailto:oxfordcriticalmass@hotmail.co.uk" target="_blank">oxfordcriticalmass@hotmail.co.uk</a></font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>Reading</b> - meets
                5.30pm on the last Friday in each month, starting from infront
                of Reading Town Hall. To check it's on email <a href="mailto:tonyorme@compuserve.com">tonyorme@compuserve.com</a>
                web www.criticalmassreading.info (currently offline)</font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>York</b> - meets
                5.30pm on the last Friday of every month outside the Minster west
                doors. Please remember lights in the winter. Usually have a sound
                system. For more see: <a href="http://york.indymedia.org.uk/critical_mass" target="_blank">http://york.indymedia.org.uk/critical_mass</a>.
                </font></li>
              <li><font face="Arial, Helvetica, sans-serif"><b>For other Critical
                Mass listings</b> see <a href="http://criticalmass.wikia.com/wiki/List_of_rides#Europe" target="_blank">http://criticalmass.wikia.com/wiki/List_of_rides#Europe</a></font></li>
            </ul>