<?php
/*
 * 		LISTS THE CONTACTS ADDED BY THE PUBLIC BUT NOT YET AUTHORISED.
 *
 * 		PERMITS AN ADMIN TO AUTHORISE THEM..
 */





/*
 * 	========================================================================================
 *
 * 							TEST POST AND GET DATA
 *
 * 	========================================================================================
 */


//	==========================================================================
//		DELETE A CONTACT
if (isset($_GET['delete']) && is_numeric($_GET['delete']))	{

		// IF THE CONTACT EXISTS IN THE DB...
		if (contactExists($_GET['delete']))	{

				//	...THEN TRY TO DELETE IT...
				$delete_US	=	deleteContact($_GET['delete']);
				if ($delete_US != false)	{

					//	IF THAT WORKS THEN SHOW A MESSAGE
					$delete		=	cleanForScreenOutput($delete_US);
					echo "<div class='errorMessage'>Your contact <b>'$delete'</b> has been deleted</div>";
					echo "<div style='width:40%' align='right'><a href='?undeleteContact=".$_GET['delete']."'>Undo Delete</a></div>";
					?>
					<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">
						&nbsp;
					</div>
					<?php

				}

		}

}


//	==================================================================
//		UNDO A DELETION
//			OCCURS WHEN THE UNDO LINK IS CLICKED IMMEDIATELY AFTER A DELETION
if (isset($_GET['undeleteContact']) && is_numeric($_GET['undeleteContact']))	{

		// IF THE CONTACT EXISTS I THE DB...
		if (contactExists($_GET['undeleteContact']))	{

				//	...THEN TRY TO UNDELETE IT...
				$delete_US	=	deleteContact($_GET['undeleteContact'], 'u');
				if ($delete_US != false)	{

					//	IF THAT WORKS THEN SHOW A MESSAGE
					$delete		=	cleanForScreenOutput($delete_US);
					echo "<div class='errorMessage'>Your contact <b>'$delete'</b> has been restored to the database</div>";
					?>
					<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">
						&nbsp;
					</div>
					<?php

				}

		}

}




//	==========================================================================
//				AUTHORISE A CONTACT
if (isset($_GET['auth']) && is_numeric($_GET['auth']))	{

		if ($authName_US		=	authoriseContact($_GET['auth']))	{

			$authName		=	cleanForScreenOutput($authName_US);
			echo "<div class='errorMessage'>Your contact <b>'$authName'</b> has been authorised for public viewing</div>";
			?>
			<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">
				&nbsp;
			</div>
			<?php

		}

}



//	==============================================================================
//			EDIT CONTACT DISPLAY
if (isset($_GET['edit']) && is_numeric($_GET['edit']))	{

		$editId		=	$_GET['edit'];
		$_SESSION['editContactID'] = $editId;

}	else	{

		$editId	=	0;
		$_SESSION['editContactID'] = $editId;
}







//	=================================================================
// 	IF A CONTACT EDIT HAS BEEN SUBMITTED
//		THIS WILL OCCUR WHEN THE EDITCONTACT FORM IS SUBMITTED
if (isset($_POST['editContact'])) 	{



	//	STEP 1 :: TEST SUBMITTED DATA FOR VALIDITY
	$errors		=	testSubmittedContactInfo($_POST);

	//	STEP 2 :: ATTEMPT TO ADD THE CONTACT
	//		THE ERROR VAR WILL BE AN ARRAY IF IT CONTAINS ANY ERRORS
	//		IF THIS IS NOT THE CASE, AN ATTEMPT TO ADD THE CONTACT WILL BE MADE.
	//		THE $RESULTS VAR WILL THUS CONTAIN EITHER A TRUE ON SUCCESS OR AN ARRAY ON ERRORS
	if (is_array($errors))	$results	=	false;
		//	THE SESSION VAR WILL HAVE BEEN SET WITH SQL ID WEN THE EDIT FORM WAS LOADED...
		else	$results	=	editContact($_SESSION['editContactID'], $_POST);

	//	STEP 3 :: TEST FOR ERRORS...
	//		IF THERE ARE ANY ERRORS THEY WILL BE IN AN ARRAY AS PART OF THE RESULT OF THE ADD CONTACT FUNCTION.
	//		IF THE ADDCONTACT FUNCTION SUCCEEDED THEN THE VARS WILL BOTH BE TRUE, NOT ARRAYS
	if (is_array($results) || is_array($errors))	{

			echo '<div class="errorMessageTitle">Could Not Update Contact</div>';

			//	CYCLE THROUGH EACH $RESULTS ERROR AND OUTPUT TO SCREEN
			if (is_array($results))	{
				foreach	($results as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error)) 	unset($error);

			//	CYCLE THROUGH EACH $ERROR ERROR AND OUTPUT TO SCREEN
			if (is_array($errors))	{
				foreach	($errors as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error))	unset($error);

			// PREPARE SOME VARS FOR THE REST OF THE PAGE...
			//		CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			//		THE SESSION VAR FORCES THE EDIT WINDOW TO SHOW
			$contactData_US		=	$_POST;
			$_SESSION[SES.'editContactsShowAdd'] = true;

	}	else	{

			//	STEP 4 :: SET SOME VARS FOR LATER USE...

			//	EDITCONTACTID IS THE MYSQL ID OF THE NEW CONTACT. USED FOR DOING FURTHER UPDATES.
			$editContactID 	=	$_SESSION['editContactID'];
			//	CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			$contactData_US		=	getContactData($editContactID);

	}

	//	EDITCONTACTID IS THE MYSQL ID OF THE NEW CONTACT. USED FOR DOING FURTHER UPDATES.
	$editContactID 	=	$_SESSION['editContactID'];

}













/*
 * 	========================================================================================
 *
 * 							DISPLAY	SECTION
 *
 * 	========================================================================================
 */

//	SELECT ALL UNAUTHORISED CONTACTS FROM THE DATABASE...
$conn		=	mysqlConnect();
$sql		=	' SELECT id, added_on, added_ip FROM con_contacts WHERE authorised <> 1 AND deleted <> 1 ORDER BY added_on';
$rs			=	mysqlQuery($sql, $conn);







//	HOW MANY RESULTS ARE THERE...
$numResults	=	mysqlNumRows($rs, $conn);
if (!is_numeric($numResults))	{

	debug('pendingContacts', 'numResults is not a number');
	exit;

}

echo "<h2>Showing All $numResults Contacts Requiring Authorisation</h2>";
echo 	'<div style="width: 50%; border-bottom:1px solid #999999; margin-bottom:8px; padding-top:8px">
				&nbsp;
			</div>';










//	CYCLE THROUGH EACH RESULT AND OUTPUT TO SCREEN...
while ($res	=	mysqlGetRow($rs))	{

	//	MAKE SURE THAT THE ID IS A NUMBER...
	$id		=	$res['id'];
	if (!is_numeric($id))	{

			debug ('pendingContacts', 'contact ID is not numeric - it is '.cleanForScreenOutput($id));
			continue;

	}



	//	PREPARE SOME FORMATTED VARIABLES...
	$formatedContact		=	getFormattedContact($id);
	$date					=	makeDateFromSQL($res['added_on']);
	$time					=	makeTimeFromSQL($res['added_on']);
	$addedIP				=	cleanForScreenOutput($res['added_ip']);



	//	DRAW LINKS
	echo	"\r\n\r\n<div style=\"color:#999999; padding-left:400px; padding-top:10px\">";
	echo	"\r\n<a name='$id'>&nbsp;</a>";
	echo	"\r\n<a href='?page=pendingContacts&edit=$id#$id'>Edit</a>";
	echo	"\r\n&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
	echo	"\r\n<a href='?page=pendingContacts&delete=$id' onClick='return confirmSubmit()'>Delete</a>";
	echo	"\r\n&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
	echo	"\r\n<a href='?page=pendingContacts&auth=$id' onClick='return confirmAuth()'>Authorise</a>";
	echo	"\r\n</div>";

	//	OUTPUT TO SCREEN...
	if ($editId == $id)	{


			echo	"\r\n\r\n<div style='width:500px'>";
			echo	"\r\n<form method='POST' action='?page=pendingContacts&edit=$id#$id'>";
			echo 	"\r\n<p>".editContactToBeAuthed($id).'</p>';
			echo 	"\r\n<p>".drawSelectableCategories($id).'</p>';
			echo	"\r\n<p><br /><input type='submit' name='editContact' value='Save' /><a style='margin-left:150px' href='?page=pendingContacts'>Cancel Edit</a></p>";
			echo	"\r\n</form></div>";

	}	else	{

			echo	$formatedContact;

	}

	echo	"\r\n<div style=\"color:#999999; padding-left:25px; padding-top:10px\">";
	echo		"Added on <b>$date</b> at <b>$time</b><br /><b>Added From IP :</b>$addedIP";
	echo	'</div>';

	echo 	"\r\n\r\n<div style=\"width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px\">
				&nbsp;
			</div>";


}









?>