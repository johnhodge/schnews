<?php
/*
 * 	SchNEWS Contacts Database Administration Centre
 *
 * 	(c) Andrew Winterbottom 2008/2009 support@tendrousbeastie.com
 *
 */

session_start();
require_once 'functions/functions.php';


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>SchNEWS Contacts Database Administration Centre</title>
	<link href="styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<script type="text/javascript" src="delete.js"></script>



	<?php


	/* RESET/INITIALISE THE SESSION VAR FOR LOGIN STATUS.
	 * IF NO LOGIN SESSION VAR IS SET THEN SET IF TO FALSE.
	 *
	 * (I.E. IF WE HAVE NO IDEA WHAT THE USERS STATUS IS THEN THEY SHOULD NOT BE LOGGED IN) */
	if (!isset($_SESSION[SES.'loggedin']) || isset($_POST['logout']))		$_SESSION[SES.'loggedin']	=	false;

	// 	TEST FOR MANUAL VOLENTARY LOGOUTS
	if (isset($_GET['page']) && $_GET['page'] == 'logout') {

		echo "<div class='errorMessage'>You have successfully logged out of the system</div>";
		$_SESSION[SES.'loggedin']	=	false;
		unset($_SESSION[SES.'page']);

	}





	/* =======================================================================
					TEST LOGIN */


	/* POST->LOGIN IS DETECT WHEN AND IF THE USER HAS CLICKED THE LOGIN BUTTON */
	if (isset($_POST['login']))	{

		/* TEST THE USERNAME AND PASSWORD INPUTS. IF THEY ARE ACCEPTABLE, STICK THEM IN VARS.
		ELSE MAKE A ERROR ARRAY ELEMENT FOR THEM */
		if (isset($_POST['username']) && $_POST['username'] != '')		$username		=	$_POST['username'];
			else 		$errors['username']		=	"No Username Set";
		if (isset($_POST['password']) && $_POST['password'] != '')		$password		=	$_POST['password'];
			else 		$errors['password']		=	"No Password Set";

		/* $ERRORS IS ONLY SET IF THE USERNAME OR THE PASSWORD HAD PROBLEMS.
		IF THEY DIDN'T THEN WE CAN TEST THEM TO SET IF THEY ARE THE CORRECT PHRASES */
		if (!isset($errors))	{

				if ($username == USERNAME && $password == PASSWORD)	{


					$_SESSION[SES.'loggedin']	=	true;

				}

				/* IF THEY DONT MATCH UP THEN WE MAKE ANOTHER ERRORS */
				if (!$_SESSION[SES.'loggedin'])	$errors['login']	=	"Either your Username or Password were incorrect";
			}

	}







	/* ==================================================================
				LOGIN FORM */

	/* IF SESSION>LOGGEDIN ISN'T TRUE THEN THE USER ISN'T LOGGEDIN, AND SHOULD BE SHOWN THE LOGIN FORM */
	if (!$_SESSION[SES.'loggedin'])	{

			?>

			<h1>SchNEWS Contacts Database Administration Centre</h1>
			<br /><br />
			<p><b>Need to login first...</b></p>
			<br />

			<?php /* IF A LOGIN ATTEMPT WAS PREVIOUSLY DETECTED, BUT DIDN'T SUCCEED THEN WE MAY HAVE SOME ERRORS
			HERE THAT NEED OUTPUTING */ ?>
			<form action="index.php" method="POST">
			<table>

				<?php	if (isset($errors['login']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['login']."</div></td></tr>"; ?>

				<?php	if (isset($errors['username']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['username']."</div></td></tr>"; ?>
				<tr><td>Username :: </td><td><input type="text" name="username" /></td></tr>

				<?php	if (isset($errors['password']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['password']."</div></td></tr>"; ?>
				<tr><td>Password :: </td><td><input type="password" name="password" /></td></tr>
				<tr><td colspan='2' align="right"><div align="right"><input type="submit" name="login" value="Login" /></div></td></tr>
			</table>

			</form>

	<?php








	/* ==================================================================================
				LOGGED IN - DO STUFF HERE */

	}	else 	{

		?>
		<h1>SchNEWS Contact Database Administration Centre</h1>
		<h2>Logged In</h2>
		<div style="width: 40%; border-bottom:1px solid #888888; margin-bottom:16px; padding-top:8px">
			&nbsp;
		</div>


		<?php
			/*
			 * 	PAGE CONTROLLER LOGIC
			 */
			if (isset($_GET['page']))	$_SESSION[SES.'page'] = $_GET['page'];
			if (!isset($_SESSION[SES.'page']))	$_SESSION[SES.'page']	=	'viewContacts';

			require 'optionsHeader.php';

			switch($_SESSION[SES.'page'])	{

				default:
					// 	DO WE NEED A VIEW CONTACTS PAGE - ISN'T THE EDIT CONTACT PAGE SUFFICIENT?
					break;

				case 'editContacts':
					require 'editContacts.php';
					break;

				case 'editCategories':
					require 'editCategories.php';
					break;

				case 'pendingContacts':
					require 'pendingContacts.php';
					break;

			}



		?>





		<div style="width: 40%; border-bottom:1px solid #888888; margin-bottom:8px; padding-top:8px">
			&nbsp;
		</div>
		<form action="index.php" method="POST">
			<input type="submit" name="logout" value="Logout" />
		</form>

		<?php
	}


?>
</body>
</html>
