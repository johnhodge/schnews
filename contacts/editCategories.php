<?php 
/*
 * 	CATEGORY EDITOR
 */
?>









<?php
/*
 * ========================================
 * 
 * 	PROCESS GET AND POST VARS
 * 
 * ========================================
 */

// MAKE SURE WE'RE LOGGED IN BEFORE WE GO ANY FURTHER.
// 	Some one could call this script directly, rather than relying on it being included through the page handler.
if (!$_SESSION[SES.'loggedin'])	{
	
	echo "<div class='errorMessage'>Sorry, not logged in.<br /><br />Page = editCategories</div>";
	exit;
	
}



if (isset($_POST['categoryNameSubmit']))	{
	
	if (isset($_POST['category']))	editCategories_addCategory($_POST['category']);	
	
}



if (isset($_POST['categoryEdit']))	{
	
	if (isset($_POST['category']) && isset($_GET['id']) && is_numeric($_GET['id']))	editCategories_editCategory($_POST['category'], $_GET['id']);	
	
}



if (isset($_GET['deleteId']) && is_numeric($_GET['deleteId']))	{
	
	if ($name_US = editCategories_deleteCategory($_GET['deleteId']))	{
		
			$name	=	cleanForScreenOutput($name_US);
			echo "<div class='errorMessageTitle'>Deleted Category '<b>$name</b>'</div>";
		
	}	else {

			echo "<div class='errorMessageTitle'>There was a problem deleting the category. It may or may not have worked.</div>";	
		
	}
	
}



?>











<?php
/*
 * ========================================
 * 
 * 	PAGE TITLE
 * 
 * ========================================
 */
?>
<h1>Edit Categories</h1>








<?php 
if (isset($_GET['editId']) && is_numeric($_GET['editId']))	{

	$catId		=	$_GET['editId'];
	$conn		=	mysqlConnect();
	$rs			=	mysqlQuery(" SELECT name FROM con_categories WHERE id = $catId ", $conn);
	$res		=	mysqlGetRow($rs);
	$name		=	cleanForScreenOutput($res['name']);
	
/*
 * ========================================
 * 
 * 	EDIT A CATEGORY
 * 
 * ========================================
 */
?>
<br />
<br />
<h2>Edit a Category</h2>

<?php ?>

<form action="?page=editCategories&id=<?php echo $catId; ?>" method="post">
	Category Name to Edit:: <input type="text" name="category" value="<?php echo $name; ?>" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="categoryEdit" value="Edit Category" />
	&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="index.php">Cancel Edit</a>
</form>	
	
	







<?php 
}	else		{
/*
 * ========================================
 * 
 * 	ADD NEW CATEGORY
 * 
 * ========================================
 */
?>
<br />
<br />
<h2>Add New Category</h2>

<form action="?page=editCategories" method="post">
	Category Name :: <input type="text" name="category" />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<input type="submit" name="categoryNameSubmit" value="Add Category" />
</form>







<?php 
}
/*
 * ========================================
 * 
 * 	LIST CURRENT CATEGORIES
 * 
 * ========================================
 */
?>
<br />
<br />
<h2>Edit Categories</h2>

<table>

<?php 

	$conn	=	mysqlConnect();
	$sql	=	" SELECT id, name FROM con_categories ORDER BY name ASC ";
	$rs		=	mysqlQuery($sql, $conn);
	
	while ($res = mysqlGetRow($rs))	{
		
		$name_US		=		$res['name'];
		$name			=		cleanForScreenOutput($name_US);
		$id				=		$res['id'];
		
		echo "<tr><td class='categoriesTableLeft'>$name</td><td class='categoriesTableRight'>";
		echo "<a href='?editId=$id'>Edit</a></td>";
		echo "<td class='categoriesTableRight'>";
		echo "<a href='?page=editCategories&deleteId=$id' onClick=\"return confirmSubmit()\">Delete</a></td>";
		echo "</tr>";	
		
	}

?>

</table>











<?php 
/*
 * ========================================================================================================================
 * 
 * ========================================================================================================================
 */
?>







<?php 
/*
 * ========================================
 * 
 * 	PAGE SPECIFIC FUNCTIONS
 * 
 * ========================================
 */


function editCategories_addCategory($category_UDUS)	{
	
		if (DEBUG) debug('editCategories_addCategory', 'Beginning');
		
		$category_US		=	cleanForDatabaseInput($category_UDUS);
		$conn				=	mysqlConnect();
		
		
		//	TEST THAT THE CATEGORY DOESN'T ALREADY EXIST :: RETURN FALSE IF SO
		$category_US_lower	=	strtolower($category_US);
		$sql				=	" SELECT id FROM con_categories WHERE LOWER(name) = '$category_US_lower' ";
		$rs					=	mysqlQuery($sql, $conn);
		
		if (mysqlNumRows($rs, $conn) != 0) {
			
			if (DEBUG) debug('editCategories_addCategory', 'Category already exists');
			return false;
			
		}
		
		
		
		// INSERT NEW CATEGORY INTO DB
		$sql		=	" INSERT INTO con_categories (name) VALUES ('$category_US') ";
		$rs			=	mysqlQuery($sql, $conn);

		if (is_numeric(mysqlGetInsertId($conn))) return true;
		else return false;
	
}







function editCategories_editCategory($category_UDUS, $id)	{
	
		debug('editCategories_editCategory', 'Beginning');
		if (!is_numeric($id)) {
			
				debug('editCategories_editCategory', 'Input ID not numeric');
				return false;
		}
		
		
		$category_US		=	cleanForDatabaseInput($category_UDUS);
		$conn				=	mysqlConnect();
		
		$sql				=	" UPDATE con_categories SET name = '$category_US' WHERE id = $id ";
		$rs					=	mysqlQuery($sql, $conn);
		
		return true;	

} 




function editCategories_deleteCategory($id)		{
	
		//	TEST INPUTS
		IF (!is_numeric($id))	{
			
				debug('editCategories_deleteCategory', 'input ID not numeric');
				return false;
			
		}
	
		//	PREPARE SQL...
		$conn 	=	mysqlConnect();
		
		//	TEST THAT THE ID ACTUALLY EXISTS...
		$sql	=	" SELECT name FROM con_categories WHERE id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs, $conn) != 1) {
			
				debug('editCategories_deleteCategory','ID does not exist in db');
				return false;
			
		}	else	{
				
			$res		=	mysqlGetRow($rs);
			$name_US	=	$res['name'];
				
		}
		
		
		//	DELETE IT IF SO...
		$sql	=	" DELETE FROM con_categories WHERE id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		
		// 	TEST THAT IT NO LONGER EXISTS..
		$sql	=	" SELECT name FROM con_categories WHERE id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs, $conn) != 0) {
			
				debug('editCategories_deleteCategory','deletion did not work');
				return false;
			
		}
		
		// ATTEMPT TO DELETE THE NOW ORPHANED CON_CAT MAPPINGS...
		$sql	=	" DELETE FROM con_cat_mappings WHERE cat_id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		
		//	TEST THAT THE CON_CAT DELETION WORKED...
		$sql	=	" SELECT id FROM con_cat_mappings WHERE cat_id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs, $conn) != 0)	{
			
				debug('editCategories_deleteCategory','con_cat mappings deletion did not work');
				return false;
			
		}
			
		return $name_US;
}

?>












