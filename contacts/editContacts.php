<?php
/*
 * 	CONTACTS EDITOR
 */
?>









<?php
/*
 * ========================================
 *
 * 	PROCESS GET AND POST VARS
 *
 * ========================================
 */

// MAKE SURE WE'RE LOGGED IN BEFORE WE GO ANY FURTHER.
// 	Some one could call this script directly, rather than relying on it being included through the page handler.
if (!$_SESSION[SES.'loggedin'])	{

	debug('main script', 'User not logged in');
	exit;

}





//	==================================================================
//	TOGGLES WHETHER OR NOT TO SHOW THE ADD CONTACT FORM
// 		IS SUPERCEEDED BY EDITING
if (!isset($_SESSION[SES.'editContactsShowAdd']))  $_SESSION[SES.'editContactsShowAdd'] = false;
if (isset($_GET['showAddContact'])) {

	if ($_GET['showAddContact'] == '1') 	$_SESSION[SES.'editContactsShowAdd'] = true;
	if ($_GET['showAddContact'] == '0') 	$_SESSION[SES.'editContactsShowAdd'] = false;
	unset($_SESSION['editContactID']);
}







//	==================================================================
//	IF A NEW CONTACT ADDITION HAS BEEN SUBMITTED
//		THIS WILL OCCUR WHEN THE ADDCONTACT FORM IS SUBMITTED.
if (isset($_POST['addContact'])) 	{

	//	STEP 1 :: TEST SUBMITTED DATA FOR VALIDITY
	$errors		=	testSubmittedContactInfo($_POST);

	//	STEP 2 :: ATTEMPT TO ADD THE CONTACT
	//		THE ERROR VAR WILL BE AN ARRAY IF IT CONTAINS ANY ERRORS
	//		IF THIS IS NOT THE CASE, AN ATTEMPT TO ADD THE CONTACT WILL BE MADE.
	//		THE $RESULTS VAR WILL THUS CONTAIN EITHER AN INT (NEW MYSQL ID) ON SUCCESS OR AN ARRAY ON ERRORS
	if (is_array($errors))	$results	=	false;
		else	$results	=	addContact($_POST);

	//	STEP 3 :: TEST FOR ERRORS...
	//		IF THERE ARE ANY ERRORS THEY WILL BE IN AN ARRAY AS PART OF THE RESULT OF THE ADD CONTACT FUNCTION.
	//		IF THE ADDCONTACT FUNCTION SUCCEEDED THEN THE VARS WILL BOTH BE TRUE, NOT ARRAYS
	if (is_array($results) || is_array($errors))	{

			//	CYCLE THROUGH EACH $RESULTS ERROR AND OUTPUT TO SCREEN
			echo '<div class="errorMessageTitle">Could Not Add Contact</div>';
			if (is_array($results))	{
				foreach	($results as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error)) 	unset($error);

			//	CYCLE THROUGH EACH $ERROR ERROR AND OUTPUT TO SCREEN
			if (is_array($errors))	{
				foreach	($errors as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error))	unset($error);

			// PREPARE SOME VARS FOR THE REST OF THE PAGE...
			//		CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			//		THE SESSION VAR FORCES THE EDIT WINDOW TO SHOW
			$contactData_US		=	$_POST;
			$_SESSION[SES.'editContactsShowAdd'] = true;
			?>
			<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px">
				&nbsp;
			</div>
			<?php
	}	else	{

			//	STEP 4 :: DISPLAY SUCCESS REPORT TO SCREEN AND SET SOME VARS FOR LATER USE...
			echo "<div class='errorMessageTitle'>New Contact Added</div>";
			?>
			<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px">
				&nbsp;
			</div>
			<?php

			//	CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			$contactData_US		=	getContactData($results);
			//	EDITCONTACTID IS THE MYSQL ID OF THE NEW CONTACT. USED FOR DOING FURTHER UPDATES.
			$editContactID		=	$results;
			$_SESSION['editContactID']		=	$editContactID;

	}
	//unset($_SESSION['editContactID']);

}







//	=================================================================
// 	IF A CONTACT EDIT HAS BEEN SUBMITTED
//		THIS WILL OCCUR WHEN THE EDITCONTACT FORM IS SUBMITTED
if (isset($_POST['editContact'])) 	{



	//	STEP 1 :: TEST SUBMITTED DATA FOR VALIDITY
	$errors		=	testSubmittedContactInfo($_POST);

	//	STEP 2 :: ATTEMPT TO ADD THE CONTACT
	//		THE ERROR VAR WILL BE AN ARRAY IF IT CONTAINS ANY ERRORS
	//		IF THIS IS NOT THE CASE, AN ATTEMPT TO ADD THE CONTACT WILL BE MADE.
	//		THE $RESULTS VAR WILL THUS CONTAIN EITHER A TRUE ON SUCCESS OR AN ARRAY ON ERRORS
	if (is_array($errors))	$results	=	false;
		//	THE SESSION VAR WILL HAVE BEEN SET WITH SQL ID WEN THE EDIT FORM WAS LOADED...
		else	$results	=	editContact($_SESSION['editContactID'], $_POST);

	//	STEP 3 :: TEST FOR ERRORS...
	//		IF THERE ARE ANY ERRORS THEY WILL BE IN AN ARRAY AS PART OF THE RESULT OF THE ADD CONTACT FUNCTION.
	//		IF THE ADDCONTACT FUNCTION SUCCEEDED THEN THE VARS WILL BOTH BE TRUE, NOT ARRAYS
	if (is_array($results) || is_array($errors))	{

			echo '<div class="errorMessageTitle">Could Not Update Contact</div>';

			//	CYCLE THROUGH EACH $RESULTS ERROR AND OUTPUT TO SCREEN
			if (is_array($results))	{
				foreach	($results as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error)) 	unset($error);

			//	CYCLE THROUGH EACH $ERROR ERROR AND OUTPUT TO SCREEN
			if (is_array($errors))	{
				foreach	($errors as $error)	{

					echo "<div class='errorMessage'>$error</div>";

				}
			}
			if (isset($error))	unset($error);

			// PREPARE SOME VARS FOR THE REST OF THE PAGE...
			//		CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			//		THE SESSION VAR FORCES THE EDIT WINDOW TO SHOW
			$contactData_US		=	$_POST;
			$_SESSION[SES.'editContactsShowAdd'] = true;

	}	else	{

			//	STEP 4 :: SET SOME VARS FOR LATER USE...

			//	EDITCONTACTID IS THE MYSQL ID OF THE NEW CONTACT. USED FOR DOING FURTHER UPDATES.
			$editContactID 	=	$_SESSION['editContactID'];
			//	CONTACTDATA CONTAIN THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
			$contactData_US		=	getContactData($editContactID);

	}

	//	EDITCONTACTID IS THE MYSQL ID OF THE NEW CONTACT. USED FOR DOING FURTHER UPDATES.
	$editContactID 	=	$_SESSION['editContactID'];

}









//	==================================================================
//	IF A CONTACT IS BEING EDITTED
//		THIS WILL OCCUR IF THE EDIT CONTACT LINK IS CLICKED FOR AN INDIVIDUAL CONTACT.
//		CAUSES THE EDIT CONTACT FORM TO BE DISPLAYED...
if (isset($_GET['editContact']) && is_numeric($_GET['editContact']))	{

		//	EDIT CONTACT ID IS THE MYSQL ID OF THE RECORD BEING EDITTED...
		$editContactID		=	$_GET['editContact'];
		//	WE THEN SESSIONISE THE MYSQL ID...
		$_SESSION['editContactID']		=	$editContactID;
		//	THIS CAUSES THE EDIT CONTACT FORM TO BE SHOWN...
		$_SESSION[SES.'editContactsShowAdd'] 	=	true;
		//	CONTACTDATA CONTAINS THE INFO TO DISPLAY THE EDIT FORM ELEMENTS CORRECTLY
		$contactData_US		=	getContactData($editContactID);

}










//	==================================================================
//	DELETE CONTACT
//		OCCURS WHEN THE DELETE CONTACT LINK IS CLICKED FOR AN INDIVIDUAL CONTACT...
if (isset($_GET['deleteContact']) && is_numeric($_GET['deleteContact']))	{

		// IF THE CONTACT EXISTS IN THE DB...
		if (contactExists($_GET['deleteContact']))	{

				//	...THEN TRY TO DELETE IT...
				$delete_US	=	deleteContact($_GET['deleteContact']);
				if ($delete_US != false)	{

					//	IF THAT WORKS THEN SHOW A MESSAGE
					$delete		=	cleanForScreenOutput($delete_US);
					echo "<div class='errorMessage'>Your contact <b>'$delete'</b> has been deleted</div>";
					echo "<div style='width:40%' align='right'><a href='?undeleteContact=".$_GET['deleteContact']."'>Undo Delete</a></div>";
					?>
					<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">
						&nbsp;
					</div>
					<?php
					$_SESSION[SES.'editContactsShowAdd'] = false;

				}

		}

}











//	==================================================================
//		UNDO A DELETION
//			OCCURS WHEN THE UNDO LINK IS CLICKED IMMEDIATELY AFTER A DELETION
if (isset($_GET['undeleteContact']) && is_numeric($_GET['undeleteContact']))	{

		// IF THE CONTACT EXISTS I THE DB...
		if (contactExists($_GET['undeleteContact']))	{

				//	...THEN TRY TO UNDELETE IT...
				$delete_US	=	deleteContact($_GET['undeleteContact'], 'u');
				if ($delete_US != false)	{

					//	IF THAT WORKS THEN SHOW A MESSAGE
					$delete		=	cleanForScreenOutput($delete_US);
					echo "<div class='errorMessage'>Your contact <b>'$delete'</b> has been restored to the database</div>";
					?>
					<div style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">
						&nbsp;
					</div>
					<?php
					$_SESSION[SES.'editContactsShowAdd'] = true;
					$_SESSION['editContactID']	=	$_GET['undeleteContact'];
					$editContactID 				=	$_SESSION['editContactID'];
					$contactData_US				=	getContactData($editContactID);

				}

		}

}







//	===================================================================
//		IF A SEARCH HAS BEEN SUBMITTED
if (isset($_GET['contactSearch']) && $_GET['contactSearch'] != '')	{

		//	GET A CLEAN VERSION OF THE SEARCH KEYWORD...
		$keyword_US				=	cleanForDatabaseInput($_GET['contactSearch']);

		// GET THE NUMBER OF SEARCH RESULTS FOR THE GIVEN KEYWORD...
		$numSearchResults		=	getNumContactSearchResults($keyword_US);

		// IF THERE AREN'T ANY RESULTS THEN WE DON'T HAVE MUCH TO DO...
		if ($numSearchResults == '0')	{

			$searchErrors[]		=	'There were no results to your search';
			unset($numSearchResults);

		//	IF THE NUMRESULTS IS FALSE THEN THERE IS SOMETHING WRONG WITH THE KEYWORD...
		}	elseif ($numSearchResults === false)	{

			$searchErrors[]		=	'There was a problem completing your search';
			unset($numSearchResults);

		// IF THE NUMRESULTS IS A POSITIVE INTEGER THEN WE NEED TO DISABLE ALL OTHER OUTPUT OPTIONS...
		}	else	{

			$_SESSION[SES.'editContactsShowAdd'] 	= false;
			$_SESSION[SES.'showAllContacts'] 		=	false;
			$_SESSION[SES.'showDeletedContacts'] 	=	false;

		}


}	else	{

		// INITIALISE THE KEYWORD VAR AS AN EMPTY STRING...
		$keyword_US = '';

}









//	=====================================================================
// 	IF THE SHOW ALL CONTACTS LINK HAS BEEN CLICKED

//	MAKE SURE THE SESSION VAR FOR THIS IS INITIALISED, TO FALSE BY DEFAULT...
if (!isset($_SESSION[SES.'showAllContacts']))	$_SESSION[SES.'showAllContacts']	=	false;

//	IF THE GET VAR IS SET THEN PROCESS IT....
if (isset($_GET['showAll']) && is_numeric($_GET['showAll']))	{

		//	IF IT IS SET TO TRUE (I.E. 1)...
		if ($_GET['showAll'] == 1)		{

				// SHOW ALL CONTACT IS SET TO TRUE, WHILE WE DISABLE THE SHOWING OF DELETED
				// CONTACTS AS WELL AS THE ADD/EDIT FORM.
				$_SESSION[SES.'showAllContacts'] 	=	true;
				$_SESSION[SES.'showDeletedContacts'] 	=	false;
				$_SESSION[SES.'editContactsShowAdd'] = false;

		}
		//	OTHERWISE WE JUST NEED TO DISABLE THE SHOW ALL OPTION.
		if ($_GET['showAll'] == 0)		$_SESSION[SES.'showAllContacts'] 	=	false;

}








//	======================================================================
// IF THE SHOW DELETED CONTACTS LINK HAS BEEN CLICKED

//	MAKE SURE THAT THE SESSION VAR IS INITIALISED, TO FALSE BY DEFAULT...
if (!isset($_SESSION[SES.'showDeletedContacts']))	$_SESSION[SES.'showDeletedContacts']	=	false;

//	IF THE GET VAR IS SET THEN PROCESS IT...
if (isset($_GET['showDeleted']) && is_numeric($_GET['showDeleted']))	{

		// IF IT IS SET TO 1 (I.E. TRUE)...
		if ($_GET['showDeleted'] == 1)		{

				// ENABLE THE SHOW ALL DELETED OPTION, AND DISABLE THE SHOW ALL OPTION (AS THE
				//	TWO ARE MUTUALLY CONTRADICTARY)
				$_SESSION[SES.'showDeletedContacts'] 	=	true;
				$_SESSION[SES.'showAllContacts'] 	=	false;

		}
		//	OTHERWISE JUST DISABLE THE SHOW ALL DELETED OPTION...
		if ($_GET['showDeleted'] == 0)		$_SESSION[SES.'showDeletedContacts'] 	=	false;

}








//	===================================================================
//	WHAT LETTERS/OPTIONS TO SHOW FROM THE BROWSER TREE......

/*
 * 	HERE WE SET UP A SESSION VAR AS AN ARRAY WHEREBY EACH ELEMENT IS ONE LETTER OF THE ALPHABET.
 * 		THIS ALLOWS EACH LETTER TO BE VIEWED IN THE TREE INDEPENDENTLY AND PERSISTENTLY...
 *
 * 	A SECOND SESSION VAR IS SET UP WHERE EACH ELEMENT IS A CATEGORY, THUS ALLOWING A
 * 	CATEGORY TREE TO BE BROWSED.
 */

//	THIS CONTAINS A LIST OF PERMITTED LETTERS...
$alphabet 		=	array(	'#', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
							'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

// MAKE SURE THE LETTER SESSION VAR IS INITIALISED, ALL DEFAULTING TO FALSE...
if (!isset($_SESSION[SES.'contactsLettersShow']))
		$_SESSION[SES.'contactsLettersShow']	=	array(	'#' => false,
															'A' => false,
															'B' => false,
															'C' => false,
															'D' => false,
															'E' => false,
															'F' => false,
															'G' => false,
															'H' => false,
															'I' => false,
															'J' => false,
															'K' => false,
															'L' => false,
															'M' => false,
															'N' => false,
															'O' => false,
															'P' => false,
															'Q' => false,
															'R' => false,
															'S' => false,
															'T' => false,
															'U' => false,
															'V' => false,
															'W' => false,
															'X' => false,
															'Y' => false,
															'Z' => false);

//	MAKE SURE THE CATEGORY SESSION VAR IS INITIALISED, ALL TO FALSE BY DEFAULT...
if (!isset($_SESSION[SES.'contactsCatIdShow']))	{

	$_SESSION[SES.'contactsCatIdShow']		=	getAllCategories();
	foreach ($_SESSION[SES.'contactsCatIdShow']	as $key => $value )	{

			$_SESSION[SES.'contactsCatIdShow'][$key]	=	false;

	}

}

//	IF THE OPEN LETTER GET VAR IS SET, AND IT IS EITHER A NUMBER OR A VALID LETTER FROM OUT ALPHABET, THEN
//	ENABLE THAT PARTICULAR LETTER IN THE SESSION VARS..
if (isset($_GET['contactOpenLetter']) && (in_array($_GET['contactOpenLetter'], $alphabet) || is_numeric($_GET['contactOpenLetter'])))		{


		$_SESSION[SES.'contactsLettersShow'][$_GET['contactOpenLetter']]	=	true;
		$_SESSION[SES.'contactsCatIdShow'][$_GET['contactOpenLetter']]		=	true;


}

// THE SAME LOGIC APPLIES TO THE CLOSE LETTER GET VAR...
if (isset($_GET['contactCloseLetter']) && (in_array($_GET['contactCloseLetter'], $alphabet) || is_numeric($_GET['contactCloseLetter'])))		{


		$_SESSION[SES.'contactsLettersShow'][$_GET['contactCloseLetter']]	=	false;
		$_SESSION[SES.'contactsCatIdShow'][$_GET['contactCloseLetter']]		=	false;
}





//	==============================================================================
//	ARE WE BROWSING AN ALPHABETIC TREE OR A CATEGORIES TREE

// 	INITIALISE THE SESSION VAR, DEFAULTING TO ALPHABETIC.
if (!isset($_SESSION[SES.'contactsShowMode']))	$_SESSION[SES.'contactsShowMode']	=	'a';

// 	IF THE GET VAR IS SET THEN UPDATE THE SESSION VAR...
if (isset($_GET['contactShowMode']) && strlen($_GET['contactShowMode']) == 1)	{

		$_SESSION[SES.'contactsShowMode']	=	$_GET['contactShowMode'];

}

//	==============================================================================
//	IF WE ARE BROWSING AN ALPHABETIC LIST, THEN BUILD THE ALPHABET ARRAY...
if ($_SESSION[SES.'contactsShowMode'] == 'a')	{
		$alphabet =	array(	'#'	=> '#', 'A' => 'A','B' => 'B','C' => 'C','D' => 'D','E' => 'E', 'F' => 'F',
							'G' => 'G','H'=> 'H','I' => 'I','J' => 'J','K' => 'K','L' => 'L','M' => 'M','N' => 'N',
							'O' => 'O','P' => 'P','Q' => 'Q','R' => 'R','S' => 'S','T' => 'T','U' => 'U',
							'V' => 'V','W' => 'W','X' => 'X','Y' => 'Y','Z' => 'Z');
		$link		=		"<a href='?contactShowMode=c'>Switch to Category View</a>";


//	ELSE WE NEED TO BUILD A CATEGORIES ARRAY...
} else {

	$alphabet	=		getAllCategories();
	$link		=		"<a href='?contactShowMode=a'>Switch to Aplhabet View</a>";

}
?>


















<?php
/*
 *		=====================================================================================================
 *
 * 												MAIN DISPLAY
 *
 * 		=====================================================================================================
 */



/*
 * ========================================
 *
 *	SEARCH CONTACTS
 *
 * ========================================
 */
?>
<table style="width: 40%; border-bottom:1px solid #cccccc; margin-bottom:8px">
<tr>
<td width="75%" align="left">
<div >
	<form method="GET" action="index.php">
		Search :: <input type="input" name="contactSearch" value="<?php echo $keyword_US; ?>" />
		| <input type="submit" name="contactSearchSubmit" value="Search" />
	</form>
	<?php
	if (isset($searchErrors))	{

			foreach($searchErrors as $error)	{

					echo "<div class='errorMessage'>$error</div>";

			}
			echo "<br />";

	}
	?>
</div>

</td>
<td style="25%" align="right">





<?php
//	==================================================================
//		SHOW ALL CONTACTS LINK
?>
<div style="border-bottom:1px solid #cccccc; margin-bottom:8px; text-align:right">

		<?php
		if ($_SESSION[SES.'showAllContacts'])	{

			echo 	'<p style="border:solid 2px red; padding: 5px; font-weight:bold" ><a href="?page=editContacts&showAll=0" >Stop Showing All Contacts</a></p>';

		}	else	{

			echo 	'<p><a href="?page=editContacts&showAll=1">Show All Contacts</a></p>';

		}
		?>
</div>







<?php
//	==================================================================
//		SHOW ALL DELETED CONTACTS LINK
?>
<div>

		<?php
		if ($_SESSION[SES.'showDeletedContacts'])	{

			echo 	'<p style="border:solid 2px red; padding: 5px; font-weight:bold"><a href="?page=editContacts&showDeleted=0">Stop Showing Deleted Contacts</a></p>';

		}	else	{

			echo 	'<p><a href="?page=editContacts&showDeleted=1">Show Deleted Contacts</a></p>';

		}
		?>

</td>
</tr>
</table>








<?php
/*
 * ========================================
 *
 * 	ADD NEW / EDIT CONTACT
 *
 * ========================================
 */

//	IF THE SHOW DELETED CONTACTS OPTION IS ENABLED THEN WE SHOULDN'T SHOW ANY ADD/EDIT FORM...
if (!$_SESSION[SES.'showDeletedContacts'])	{
?>
<br />
<h2>

<?php
/* SHOW THE OPEN OR CLOSE CROSSES AS APPROPRIATE */
if ($_SESSION[SES.'editContactsShowAdd'])	{

	echo "<a href='?showAddContact=0'><img src='gfx/close.gif' /></a>";

}
if (!$_SESSION[SES.'editContactsShowAdd'])	{

	echo "<a href='?showAddContact=1'><img src='gfx/open.gif' /></a>";

}

echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';


//	FORM TITLE
if (isset($editContactID))	echo 'Editting Contact :: '.cleanForScreenOutput($contactData_US['name']);
else	echo 'Add New Contact';
echo '</h2>';

// SHOW A CANCEL LINK IF WE ARE EDITING...
if ($_SESSION[SES.'editContactsShowAdd'])
echo '<div style="padding-left:500px"><a href="?showAddContact=0">Cancel Editting</a></div>';


?>





<?php
/* ONLY SHOW THE ADD CONTACT FORM IF REQUESTED */
if ($_SESSION[SES.'editContactsShowAdd'])	{
?>
<blockquote><blockquote>
<table id="editContacts">
	<form action="?page=editContacts" method="post">
		<tr>
			<td>Name</td>
			<td><input style="width:500px" type="text" name="name"
			<?php if (isset($contactData_US['name']))
					echo " value=\"".htmlentities(cleanForScreenOutput($contactData_US['name']))."\" ";?>
			/></td>
		</tr>

		<tr>
			<td>Address	</td>
			<td><textarea name="address" style="width:500px" rows="6"><?php
			 if (isset($contactData_US['address']))
					echo cleanForScreenOutput($contactData_US['address']);?></textarea>
			</td>
		</tr>

		<tr>
			<td>Phone</td>
			<td><input style="width:500px" type="text" name="phone"
			<?php if (isset($contactData_US['phone']))
					echo " value='".cleanForScreenOutput($contactData_US['phone'])."' ";?>
			/></td>
		</tr>

		<tr>
			<td>Fax</td>
			<td><input style="width:500px" type="text" name="fax"
			<?php if (isset($contactData_US['fax']))
					echo " value='".cleanForScreenOutput($contactData_US['fax'])."' ";?>
			/></td>
		</tr>

		<tr>
			<td>Web</td>
			<td><input style="width:500px" type="text" name="web"
			<?php if (isset($contactData_US['web']))
					echo " value='".cleanForScreenOutput($contactData_US['web'])."' ";?>
			/></td>
		</tr>

		<tr>
			<td>Email</td>
			<td><input style="width:500px" type="text" name="email"
			<?php if (isset($contactData_US['email']))
					echo " value='".cleanForScreenOutput($contactData_US['email'])."' ";?>
			/></td>
		</tr>

		<tr>
			<td>Description</td>
			<td><textarea name="description" style="width:500px" rows="10"><?php
			if (isset($contactData_US['description']))
					echo cleanForScreenOutput($contactData_US['description']);?></textarea>
			</td>
		</tr>

		<tr>
			<td colspan="2">

				<?php
					if (isset($_SESSION['editContactID']) && is_numeric($_SESSION['editContactID']))
						$tempId		=	$_SESSION['editContactID'];
					else	$tempId = 0;

					echo drawSelectableCategories($tempId);
				?>

			</td>
		</tr>

		<tr>
			<td></td>
			<td align="right">
				<?php
					if (isset($editContactID))
						echo	'<input type="submit" name="editContact" value="Save" />';
					else
						echo	'<input type="submit" name="addContact" value="Add Contact" />';
				?>
			</td>
		</tr>
	</form>
</table>
</blockquote></blockquote>
<?php } ?>


<div style="width: 20%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px">
	&nbsp;
</div>

















<?php
}
/*
 * ==========================================================================
 *
 * 						SHOW ALL CONTACTS
 *
 * ==========================================================================
 */


//	===============================================
//	IF WE ARE SHOWING SEARCH RESULTS
if (isset($numSearchResults) && is_numeric($numSearchResults) && $numSearchResults >= 1)	{

	//	DRAW SEARCH TITLE
	echo "<h2 class=\"noticeMessageTitle\">Showing all $numSearchResults Search Results...</h2>";

	// PREPARE SOME SQL...
	$conn		=	mysqlConnect();
	$sql		=	" SELECT id FROM con_contacts
							WHERE 			( name LIKE '%$keyword_US%' OR
											description LIKE '%$keyword_US%' OR
											address LIKE '%$keyword_US%' ) AND
											(deleted = 0 OR deleted IS NULL) AND authorised = 1
					 ORDER BY name asc ";
	$rs			=	mysqlQuery($sql, $conn);

	//CYCLE THROUGH EACH SEARCH RESULTS AND DISPLAY...
	while ($res = mysqlGetRow($rs))	{

				//	DRAW CONTACT
				echo 	'<blockquote><blockquote>';
				echo	'<div id="showContact">' . getFormattedContact($res['id']) . '</div>';

				//	DRAW EDIT/DELETE BUTTONS
				echo 	'<div style="width:500px; text-align:right">';
				echo		'<a href="?editContact='.$res['id'].'">Edit</a>';
				echo			'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;';
				echo		'<a href="?deleteContact='.$res['id'].'" onClick="return confirmSubmit()">Delete</a>';
				echo	'</div>';

				//	DRAW BOTTOM BORDER...
				echo	"<div style=\"width: 20%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px\">
								&nbsp;
							</div>";
				echo 	'</blockquote></blockquote>';

	}









//	====================================================================
}	else	{

//	============================================================
//		IF WE ARE SHOWING THE NORMAL, NON-SEARCHED LIST...



//	SHOW THE LINK TO FLIP BETWEEN ALPHA AND CAT VIEW MODE...
echo "<div>$link</div>";
debug ('main script', 'Session contactShowMode = '. $_SESSION[SES.'contactsShowMode']);



//	WORK OUT IF WE ARE SHOWING DELETED ITEMS OR NOT...
if ($_SESSION[SES.'showDeletedContacts'])	$deleted = '1 OR deleted NOT NULL ) AND authorised = 1 ';
	else $deleted = '0 OR deleted IS NULL ) AND authorised = 1';
debug ('main edit contacts script', "Deleted = $deleted");



//	CYCLE THROUGH EACH LETTER/CATEGORY, SHOWING THE RESULTS FOR EACH ONE.
/*
 *	WE HAVE PRESIOSLY SET THE ALPHABET ARRAY IN THE DATA PROCESSING SECTION.
 * 		IF WE ARE IN ALPHA MODE, THEN THE ARRAY CONTAINS THE LATIN ALPHABET (THE KEY AND VALUES ARE THE SAME).
 * 		IF IN CAT MODE, THEN THE ARRAY IS A LIST OF CATEGORIES WHERE THE KEY IS THE MYSQL ID OF EACH VALUE
 */
foreach ($alphabet as $key => $letter)	{

		$letter		=	cleanForScreenOutput($letter);

		//	IF WE ARE IN ALPHA VIEW MODE THEN MAKE THE CORRESPONDING SQL QUERY...
		//		IF WE ARE LOOKING AT A LETTER AN NOT A NUMBER...
		if ($_SESSION[SES.'contactsShowMode'] == 'a' && $letter != '#')	{
					$sql		=	" SELECT id FROM con_contacts
										WHERE lower(name) LIKE '".strtolower($letter)."%' AND ( deleted = $deleted
										ORDER BY name asc ";

		//		OR IF WE ARE LOOKING AT A NUMBER...
		}	elseif ($_SESSION[SES.'contactsShowMode'] == 'a' && $letter == '#')	{

					for ($n	= 0 ; $n <= 9 ; $n++)	{

							$where[]		=	" name LIKE '$n%' ";

					}
					$where		=	implode(' OR ', $where);
					$sql		=	" SELECT id FROM con_contacts
										WHERE ( $where ) AND deleted = $deleted
										ORDER BY name asc ";


		//	ELSE MAKE A QUERY FOR CAT VIEW MODE...
		}	else	{
					$sql		=		" SELECT DISTINCT con_cat_mappings.contact_id as id FROM con_cat_mappings
											LEFT JOIN con_contacts ON con_cat_mappings.contact_id = con_contacts.id
											WHERE con_cat_mappings.cat_id = $key AND con_contacts.deleted = $deleted
											ORDER BY con_contacts.name ASC";

		}

		//	=================================================================================
		// DRAW THE OPEN AN CLOSE BUTTONS AS APPROPRIATE, SO AS TO DRILL INTO EACH LETTER...

		//	MAKE AN ANCOR FOR QUICK LINKING...
		echo "<a name='$letter'></a><h3>";


		//	WORK OUT WHETHER WE ARE DRILLING INTO THE LETTER/CAT...
		if ((isset($_SESSION[SES.'contactsLettersShow'][$key]) && $_SESSION[SES.'contactsLettersShow'][$key] == true) ||
			(isset($_SESSION[SES.'contactsCatIdShow'][$key]) && $_SESSION[SES.'contactsCatIdShow'][$key] == true) ||
			$_SESSION[SES.'showAllContacts'])
				$showLetter		=	true;
		else
				$showLetter		=	false;


		// SHOW THE OPEN/CLOSE BUTTON FOR THE LETTER AS APPROPRIATE...
		if ($showLetter)	{

			echo "<a href='index.php?contactCloseLetter=".urlencode($key)."#".$letter."'><img src='gfx/close.gif' /></a>";

		}
		else	{

			echo "<a href='index.php?contactOpenLetter=".urlencode($key)."#".$letter."'><img src='gfx/open.gif' /></a>";

		}
		echo "&nbsp;&nbsp;&nbsp;&nbsp;$letter</h3>";


		// IF WE ARE NOT DRILLING INTO THIS LETTER THEN CONTINUE ONTO THE NEXT LETTER
		if (!$showLetter) continue;


		// OTHERWISE SHOW ALL THE CONTACTS FOR THIS LETTER...
		$conn			=	mysqlConnect();
		$rs			=	mysqlQuery($sql, $conn);

		while ($res = mysqlGetRow($rs))	{

				//	DRAW CONTACT
				echo 	'<blockquote><blockquote>';
				echo	'<div id="showContact">' . getFormattedContact($res['id']) . '</div>';

				//	DRAW EDIT/DELETE BUTTONS
				echo 	'<div style="width:500px; text-align:right">';
				echo		'<a href="?editContact='.$res['id'].'">Edit</a>';
				echo			'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;';
				if (isDeleted($res['id']))	{
						echo		'<a href="?undeleteContact='.$res['id'].'" onClick="return confirmUndelete()">UnDelete</a>';
				}	else	{
						echo		'<a href="?deleteContact='.$res['id'].'" onClick="return confirmSubmit()">Delete</a>';
				}
				echo	'</div>';

				echo	"<div style=\"width: 20%; border-bottom:1px solid #cccccc; margin-bottom:8px; padding-top:8px\">
								&nbsp;
							</div>";
				echo 	'</blockquote></blockquote>';

		}

	}

}

?>


