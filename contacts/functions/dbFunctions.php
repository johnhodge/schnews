<?php

/*
 * ESTABLISH A PERSISTENT CONNECTION TO THE MYSQL DB 
*/
function mysqlConnect()	{
	
	$conn	=	mysql_connect(DBHOST, DBUSER, DBPASS, true);
	$db		=	mysql_select_db(DBNAME, $conn);
	if (DEBUG) echo mysql_error();
	
	
	if (DEBUG) echo "<br>New mysql connection made<br>";
	return $conn;
		
}




/*
 * EXECUTE AN SQL QUERY
 * 
 * RETURN THE RESOURCE RESULT
 */
function mysqlQuery($sql, $conn)	{
	
	if (!is_string($sql))	{
		if (DEBUG)	echo '<br>mysqlQuery :: sql not string<br>';
		return false;
	}
	
	$rs		=	mysql_query($sql, $conn);
	if (DEBUG) echo mysql_error();
	if (DEBUG) echo "<br>mysqlQuery :: SQL :: $sql<br>";
	
	return $rs;
}




/*
 * COUNT THE NUMBER OF ROWS IN A GTIVEN RESULT RESOURCE
 * 
 * RETURNS INT
 */
function mysqlNumRows($rs, $conn = '')	{
	
	//if (DEBUG) echo "<p>MySQL Num Rows :: Input type = ".gettype($rs).'</p>';
	
	$num	=	mysql_num_rows($rs);
	return $num;
}




/*
 * 	FETCHES ONE ROW FROM THE GIVEN RESOURCE RESULT
 * 
 * 	RETURNS AN ASSOCIATIVE ARRAY
 */
function mysqlGetRow($rs)	{
	
	return mysql_fetch_array($rs);
	
}




/*
 *	RETURNS THE AUTOINCREMENT ID OF THE MOST RECENTLY WRITTEN INSERT 
 */
function mysqlGetInsertId($conn)	{
	
	if (!is_resource($conn)) {
		
		if (DEBUG)	echo "<br>mysqlGetInsertId :: conn not a resource <br>";
		return false;
	}
	$id		=	mysql_insert_id($conn);
	if (!is_numeric($id)) {
		if (DEBUG)	echo "<br>mysqlGetInsertId :: id not numeric <br>";
		return false;
	}
	return $id;
	
}



?>