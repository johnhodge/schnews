<?php


/*
 * 	INSERTS A NEW CONTACT INTO THE DATABASE
 * 		THIS ASSUMES THAT THE INPUTTED DATA HAS BEEN PREPARED AND IS VALID, INTEGRITY-WISE
 *
 * 	INPUT
 * 		data 		ARRAY(assoc)		The POST array containing the submitted data. This should be pre-prepared.
 *
 * 	OUTPUTS
 * 		true/false	BOOL				True on success, False on any error.
 */
function addContact($data)	{

	//	TEST INPUTS
	if(!is_array($data))	{

			debug ('addContact', 'Submitted data is not array');
			return false;

	}

	//	CLEAN INPUTS
	foreach ($data as $key => $datum)	{

			$data[$key] 		=	cleanForDatabaseInput($datum);

	}

	//	MAKE SURE SUBMITTED ENTRY ISN'T ALREADY IN THERE...
	if(isDuplicateContact($data))	{

			debug ('addContact', 'Submitted data already exists');
			$errors[]		=	'The contact record you submitted already exists';
			return $errors;

	}

	// 	PREPARE THE SQL STATEMENT IN ORDER TO INSER THE CONTACT...
	$sql		=	" INSERT INTO con_contacts
							(
									name,
									address,
									phone,
									fax,
									web,
									email,
									categories_id,
									description,
									added_on,
									added_ip,
									authorised,
									deleted,
									authorised_on
							) VALUES (
									'{$data['name']}',
									'{$data['address']}',
									'{$data['phone']}',
									'{$data['fax']}',
									'{$data['web']}',
									'{$data['email']}',
									0,
									'{$data['description']}',
									'".makeSqlDatetime()."',
									'".$_SERVER['REMOTE_ADDR']."',
									1,
									0,
									'".makeSqlDatetime()."'
							)";

		$conn		=	mysqlConnect();
		$rs			=	mysqlQuery($sql, $conn);

		//	GET THE NEWLY INSERTED MySQL ID
		$newId		=	mysqlGetInsertId($conn);

		// TEST THE NEW ID AND FAIL IF BAD
		if (!is_int($newId)) 	{

			debug('addContact', 'insert of new contact didnt result in a successful insertID');
			return false;

		}

		// GET ALL THE TICKED CATEGORIES FROM THE POST DATA...
		$categoryIds 		=	getCategoryIdsFromPOSTData($_POST);

		// CYCLE THROUGH EACH SELECTED CAT ID AND AD IT TO THE DATABASE...
		foreach($categoryIds as $categoryId)	{

				$sql		=	" INSERT INTO con_cat_mappings
										(contact_id, cat_id)
									VALUES
										($newId, $categoryId)
								";
				debug('addContact', "Adding cat $categoryId to contact record $newId");
				$rs			=	mysqlQuery($sql, $conn);
				if (is_int(mysqlGetInsertId($conn)))	debug ('addContact', "Cat $categoryId added");
					else	debug ('addContact', "Cat $categoryId FAILED");

		}

		return $newId;

}


/*
 * 	TEST THE GIVEN CONTACT ARRAY TO ESTABLISH WHETHER IT ALREADY EXISTS IN THE DATABASE
 *
 * 	INPUT
 * 		contact 		ARRAY(assoc)		THE SUBMITTED CONTACT DATA FOR TESTING WITH
 *
 * 	OUTPUT
 * 		BOOL			TRUE IF CONTACT IS DUPLICATED, FALSE IF NOT.
 */
function isDuplicateContact($contact_USUD)	{

		//	TEST INPUTS...
		// 		MAKE SURE THE INPUT IS AN ARRAY...
		if (!is_array($contact_USUD))	{

				debug('isDuplicateContact', 'input Contact_USUD is not array');

		}

		//	MAKE SURE THAT THE REQUIRED FIELDS ARE SET...
		if (!isset($contact_USUD['name']))	{

				debug('isDuplicateContact', 'input Contact_USUD required elements not set');

		}

		// FEED THE NAME INTO THE DATABASE AND TEST FOR ALREADY EXISTING RECORDS...
		$name		=	cleanForDatabaseInput($contact_USUD['name']);
		$conn		=	mysqlConnect();
		$sql		=	" SELECT id FROM con_contacts WHERE name = '$name' ";
		$rs			=	mysqlQuery($sql, $conn);
		$num		=	mysqlNumRows($rs, $conn);
		if ($num		==	0)	return false;
			else	return true;

}




/*
 * 	UPDATES AND EXISTING CONTACT INTO THE DATABASE
 * 		THIS ASSUMES THAT THE INPUTTED DATA HAS BEEN PREPARED AND IS VALID, INTEGRITY-WISE
 *
 * 	INPUT
 * 		id			INT					The MySQL id of the record to be updated.
 * 		data 		ARRAY(assoc)		The POST array containing the submitted data. This should be pre-prepared.
 *
 * 	OUTPUTS
 * 		true/false	BOOL				True on success, False on any error.
 */
function editContact($id, $data)	{

	//	TEST INPUTS...
	if (!is_numeric($id))	{

			debug('editContact', 'input ID not numeric');
			return false;

	}

	foreach ($data as $key => $datum)	{

			$data[$key] 		=	cleanForDatabaseInput($datum);

	}

	$currentDate		=	makeSqlDatetime();
	$sql		=	" UPDATE con_contacts SET
							name		=		'{$data['name']}',
							address 	=		'{$data['address']}',
							phone		=		'{$data['phone']}',
							fax			=		'{$data['fax']}',
							web			=		'{$data['web']}',
							email		=		'{$data['email']}',
							last_updated =		'$currentDate',
							description	=		'{$data['description']}'
						WHERE id = $id
	";

		$conn		=	mysqlConnect();
		$rs			=	mysqlQuery($sql, $conn);

		//	DELETE ALL THE EXISTING CATEGORIES FROM THE DATABASE BASE BEFORE WE ADD THE NEW ONES...
		debug('editContacts', "Deleting all Cat_mapptings that match contact_id $id");
		$sql		=	" DELETE FROM con_cat_mappings WHERE contact_id = $id ";
		$rs			=	mysqlQuery($sql, $conn);

		// GET ALL THE TICKED CATEGORIES FROM THE POST DATA...
		$categoryIds 		=	getCategoryIdsFromPOSTData($_POST);

		// CYCLE THROUGH EACH SELECTED CAT ID AND AD IT TO THE DATABASE...
		foreach($categoryIds as $categoryId)	{

				$sql		=	" INSERT INTO con_cat_mappings
										(contact_id, cat_id)
									VALUES
										($id, $categoryId)
								";
				debug('addContact', "Adding cat $categoryId to contact record $id");
				$rs			=	mysqlQuery($sql, $conn);
				if (is_int(mysqlGetInsertId($conn)))	debug ('addContact', "Cat $categoryId added");
					else	debug ('addContact', "Cat $categoryId FAILED");

		}

		echo "<div class='errorMessage'>Your record '<b>".cleanForScreenOutput($data['name'])."</b>' has been updated</div><br /><br />";
		return true;

}







/*
 * 	RETURNS AN HTML FORMATTED CONTACT ENTRY.
 *
 * 	INPUTS
 * 		id 	INT	Database ID for the contact entry
 *
 * 	OUTPUTS
 * 		STRING 	html formatted entry, cleaned and ready fro output to screen.
 */
function getFormattedContact($id)	{

		// TEST INPUTS
		if (!is_numeric($id))	{

				debug('getFormattedCOntact', 'id not numeric');
				return false;

		}

		// GRAB FROM DB
		$conn		=	mysqlConnect();
		$sql		=	" SELECT name, address, phone, fax, web, email, description, added_on, deleted
								FROM con_contacts
								WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);
		$contact	=	mysqlGetRow($rs);

		// TEST IF IS DELETED CONTACT AND MAKE RED IF SO...
		if ($contact['deleted'] == 1)		$delStyle		=	" style='color:red' ";
			else $delStyle	=	'';

		// PREPARE THE FORMATED OUTPUT
		$output 	=	"\r\n\r\n<h4 $delStyle>{$contact['name']}</h4>";

		if (trim($contact['address']) != '')
				$output		.=		"\r\n<b>Address</b> : <span $delStyle>".cleanForScreenOutput($contact['address'])."</span><br />";

		if (trim($contact['phone']) != '')
				$output		.=		"\r\n<b>Phone</b> : <span $delStyle>".cleanForScreenOutput($contact['phone'])."</span><br />";

		if (trim($contact['fax']) != '')
				$output		.=		"\r\n<b>Fax</b> : <span $delStyle>".cleanForScreenOutput($contact['fax'])."</span><br />";

		if (trim($contact['web']) != '')
				$output		.=		"\r\n<b>Web</b> : <span $delStyle>".cleanForScreenOutput($contact['web'])."</span><br />";

		if (trim($contact['email']) != '')
				$output		.=		"\r\n<b>Email</b> : <span $delStyle>".cleanForScreenOutput($contact['email'])."</span><br />";
		if (trim($contact['description']) != '')
				$output		.=		"\r\n<b>Description</b> : <span $delStyle>".cleanForScreenOutput($contact['description'])."</span><br />";


		//	GET THE CATEGORIES...
		if (!$categories			=	getCategoriesForContact($id))	{

			debug('getFormattedContact', 'could not get categories');
			return $output;
		}

		if (!is_array($categories) || count($categories) == 0)	return $output;
		$output		.=		"\r\n<b>Categories</b> : ";
		$output		.= 		"<span $delStyle>".implode(', ', $categories)."</span><br />";


		return $output;

}




/*
 * 		RETURNS AN INDEXED ARRAY OF THE CATEGORIES FOR A GIVEN CONTACT, WHERE
 * 		THE KEY IS THE CAT ID AND THE VALUE IS THE CATEGORY NAME
 *
 * 		INPUT
 * 		id		INT			THE MYSQL ID FOR THE CONTACT
 *
 * 		OUTPUT
 * 		ARRAY(index)		THE CATEGORIES FOR THE CONTACT. EMPTY ARRAY IF NO CATEGORIES FOR CONTACT.
 * 		BOOL(false)			ANY ERROR
 */
function getCategoriesForContact($id)	{

	// TEST INPUTS
	if (!is_numeric($id))	{

			debug('getCategoriesForContact', 'input id not numeric ');
			return false;

	}

	//	PREPARE SQL
	$conn		=	mysqlConnect();

	//	GET ALL CATEGORIES, FAIL IF ERROR
	if (!$cateogries	=	getAllCategories())	{

			debug('getCategoriesForCOntact', 'Could not get all categories via external function');
			return false;

	}

	$sql		=	" SELECT cat_id FROM con_cat_mappings WHERE contact_id = $id ";
	$rs			=	mysqlQuery($sql, $conn);

	//	INITITALISE THE OUTPUT ARRAY
	$output		=	array();

	//	CYCLE THROUGH THE CONTACT RESULTS AND ADD AN ENTRY TO THE OUTPUT ARRAY FOR EACH ONE...
	while ($res		=	mysqlGetRow($rs))	{

			$output[$res['cat_id']]			=	$cateogries[$res['cat_id']];

	}

	if (is_array($output))	return $output;
		else return false;

}




/*
 * 	TEST THE DATA SUBMITTED AS A CONTACT FOR VALIDITY.
 *
 * 	INPUTS
 * 		data	ARRAY	the POST data from the calling script.
 *
 * 	OUTPUTS
 * 		EITHER	array	an associative array containing descriptions of all the errors ecountered, where each key is the name of the form element in error
 * 		OR		bool	TRUE is all data is good, FALSE if some problem not relating to data validity
 */
function testSubmittedContactInfo($data)	{

		//	TEST INPUTS
		if (!is_array($data))	{

				debug('testSubmittedContactInfo', 'data input is not an array');
				return false;

		}

		//	CYCLE THROUGH THE REQUIRED ELEMENTS...
		$requireds		=	array('name', 'description');
		foreach	($requireds	as $required)	{

				if (!isset($data[$required]) || trim($data[$required]) == '')	{

						debug ('testSubmittedContactInfo', $required.' is not set');
						$errors[]		=	properCase($required).' is not set or is not valid';
				}

		}


		//	THIS CONTAINS A LIST OF PERMITTED LETTERS...
		$alphabet 		=	array( 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
							'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

		//	MAKE SURE THAT THE FIRST LETTER OF THE NAME IS EITHER A LETTER OR A NUMBER...
		$name			=	$data['name'];
		$initial		=	strtoupper(substr($name, 0, 1));

		if (!is_numeric($initial) & !in_array($initial, $alphabet))		{

						debug ('testSubmittedContactInfo', 'name does not start with an alphanumeric');
						$errors[]		=	'The contact name must start with either a letter or a number';

		}

		if (isset($errors))	return $errors;
		return true;

}




/*
 * 	RETURNS ALL THE ACTIVE CATEGORIES AS AN INDEXED ARRAY, WHERE THE INDEX IS THE MYSQL ID NUMBER
 *
 * 	INPUTS
 * 		none
 *
 * 	OUTPUTS
 * 		array(indexed)		THE LIST OF CATEGORIES (ALPHA A-Z SORT) INDEX BY MYSQL ID NUMBER
 * 		bool (false)		ANY ERROR
 */
function getAllCategories()	{

		//	RUN A MYSQL QUERY TO GET ALL CONTACTS
		$conn		=	mysqlConnect();
		$sql		=	' SELECT id, name FROM con_categories ORDER BY name ASC ';
		$rs			=	mysqlQuery($sql, $conn);


		// 	MAKE SURE THAT THE QUERY IS POPULATED WITH RESULTS
		if (mysqlNumRows($rs, $conn) == 0) 	{

				debug ('getAllCategories', 'No results from MySQL query');
				return false;

		}


		//	DROP THE RESULTS INTO AN INDEXED ARRAY
		while ($res		=	mysqlGetRow($rs))	{

				$output[$res['id']]		=	$res['name'];

		}

		if (is_array($output) && count($output) >= 1)	return $output;
			else	return false;

}




/*
 * 	CREATE AN HTML FORMATTED FORM SECTION OF CHECKBOXS FOR SELECTING THE CATEGORIES
 *
 * 	INPUT
 * 		id 		INT		MySQL ID OF THE CONTACT RECORD BEING UPDATED. IF NO UPDATES THEN THIS SHOULD BE 0 OR FALSE
 *
 * 	OUTPUT
 * 		STRING			FORMATTED HTML STRING CONTAINING FORM ELEMENTS
 * 		BOOL (false)	ANY ERROR
 *
 */
function drawSelectableCategories($id)	{

		//	TEST INPUTS
		if (!is_numeric($id) && !is_bool($id))	{

				debug('drawSelectableCategories','input ID not numeric');
				return false;

		}

		//	GET ALL CATEGORIES
		if (!$categories_US		=	getAllCategories())	{

				debug ('drawSelectableCategories', 'category list not returned properly');
				return false;

		}

		debug ('drawSelectableCategories', "id = $id");

		// 	TEST THE CATEGORIES RETURNED...
		if (!is_array($categories_US) || count($categories_US) == 0)	{

				debug ('drawSelectableCategories', 'category list not an array');
				return false;

		}

		//	===================================================
		//		GET ALL SELECTED CATEGORIES
		$conn		=		mysqlConnect();
		$sql		=		" SELECT cat_id FROM con_cat_mappings WHERE contact_id = $id ";
		$rs			=		mysqlQuery($sql, $conn);
		$selectedCats		=	array();
		while ($res		=	mysqlGetRow($rs))	{

				$selectedCats[]	=	$res['cat_id'];

		}


		//	DRAW THE TITLE
		$output		=	"\r\n<!-- CATEGORIY SELECTOR -->\r\n<table width='100%' align='center'  style='width:100%'>
							\r\n<tr>
								\r\n<td colspan='3'>
									\r\n<b>Categories</b>
								\r\n</td>
							\r\n</tr>";

		//	THIS COUNTER IS TO MARK HOW MANY COLUMNS WE HAVE PER ROW.
		//		ONCE I = X WE WILL START A NEW ROW AND REST I = 0
		$i			=	0;
		$output		.=	"\r\n<tr>";

		//	CYCLE THROUGH EACH CATEGORY
		foreach($categories_US as $index	=> $category_US)	{

				if ($i	==	3)	{

						$output		.=	"\r\n\r\n</tr>\r\n<tr>";
						$i			=	0;

				}

				if (in_array($index, $selectedCats))	$checked = ' checked selected ';
					else $checked	=	'';
				$category		=	cleanForScreenOutput($category_US);
				$output			.=	"\r\n\r\n<td width='33%'>\r\n<table><tr><td>\r\n<input type='checkbox' name='category$index' value='1' $checked />";
				$output			.=	"&nbsp;&nbsp;&nbsp;&nbsp;</td><td>\r\n";
				$output			.=	$category;
				$output			.=	"\r\n</td></tr></table>\r\n</td>\r\n";
				$i++;
		}

		$output		.=	"\r\n</tr>\r\n\r\n</table>";
		return $output;

}





/*
 * 	GET THE UNFORMATTED DATA FOR A CONTACT. RETURN AS ARRAY
 *
 * 	INPUTS
 * 		id 		INT 	the MySQL ID for the contact data
 *
 * 	OUTPUTs
 * 		contact_US	ARRAY(assoc)	the contact data
 * 		false		BOOL			in case of error
 */
function getContactData($id)	{

		// 	TEST THE INPUTS
		if (!is_numeric($id))	{

				debug('getContactData', 'input id not numeric');
				return false;

		}

		$conn		=	mysqlConnect();
		$sql		=	" SELECT * FROM con_contacts WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);

		$contact_US	=	mysqlGetRow($rs);

		if (is_array($contact_US))	return $contact_US;
		else {

			debug ('getContactData', 'mysqlResult not array');
			return false;

		}

}






/*
 * 	TESTS THE INPUT DATA FOR SUBMITTED CHECKBOXES FROM THE drawSelectableCategories FUNCTION
 *
 * 	INPUT
 * 		data		INDEX(assoc)	THE POST ARRAY FROM THE CALLING SCRIPT
 *
 * 	OUTPUT
 * 		ARRAY(INDEX)				AN UNSORTED ARRAY ON INT, EACH ELEMENT BEING A SELECTED CATEGORY ID
 * 		BOOL (FALSE)				ANY ERROR
 */
function getCategoryIdsFromPOSTData($data)	{

		//	TEST INPUTS...
		if (!is_array($data))	{

				debug ('getCategoryIdsFromPOSTData','input data not an array');
				return false;

		}

		//	GET ALL CATEGORIES
		$categories		=	getAllCategories();

		//	INITIALISE THE OUTPUT ARRAY
		$output			=	array();

		//	CYCLE THROUGH EACH DATABASE CATEGORY
		foreach ($categories as $index	=> $category)	{

			if (isset($data['category'.$index]))	{

					$output[]		=	$index;

			}

		}

		return $output;

}




/*
 * 	RETURNS THE NUMBER OF HITS FOR A GIVEN KEYWORD SEARCH ON THE CONTACTS PAGE
 *
 * 	INPUTS
 * 		keyword 		STRING		SEARCH TERM TO BE TESTED FOR NUMBER OF HITS
 *
 * 	OUTPUTS
 * 		INT							THE NUMBER OF HITS. IF 0 SAME AS BOOL FALSE;
 * 		BOOL(false)					ANY ERROR
 */
function getNumContactSearchResults($keyword_USUD)	{

		// TEST INPUTS
		if (!is_string($keyword_USUD))	{

				debug('getNumContactSearchResults', 'input keyword is not a string');
				return false;

		}
		if (strlen($keyword_USUD) === 0)	{

				debug('getNumContactSearchResults', 'input keyword is zero length');
				return false;

		}

		//	CLEAN INPUTS
		$keyword_US		=	cleanForDatabaseInput($keyword_USUD);

		// SET UP AN SQL QUERY...
		$conn		=		mysqlConnect();
		$sql		=		" SELECT id FROM con_contacts
									WHERE 	( 	LOWER(name) LIKE '%$keyword_US%' OR
												LOWER(description) LIKE '%$keyword_US%' OR
												LOWER(address) LIKE '%$keyword_US%' ) AND
											deleted = 0 AND authorised = 1	";
		$rs			=		mysqlQuery($sql, $conn);
		$num		=		mysqlNumRows($rs, $conn);

		if (is_int($num))	return $num;
		else	{

				debug('getNumContactSearchResults', 'num rows not int');
				return false;

		}

}



/*
 * 	TEST FOR THE EXISTENCE OF A CONTACT BASED ON MYSQL ID
 *
 * 	INPUT
 * 		id			INT		MYSQL ID OF THE RECORD FOR TESTING
 *
 * 	OUTPUT
 * 		BOOL				TRUE IS CONTACT DOES EXIST, FALSE IF NOT OR ERROR
 */
function	contactExists($id)	{

	//	TEST INPUTS
	if (!is_numeric($id))	{

		debug ('contactExists', 'input id not numeric');
		return false;

	}
	if ($id < 1)	{

		debug ('contactExists', 'input id less than 1');
		return false;

	}

	// PREPARE SQL
	$conn 		=	mysqlConnect();
	$sql		=	" SELECT id FROM con_contacts WHERE id = $id ";
	$rs			=	mysqlQuery($sql, $conn);

	// TEST NUMBER OF RESULTS...
	$num		=	mysqlNumRows($rs, $conn);
	if (!is_numeric($num))	{

			debug ('contactExists', 'mysqlNumRows not numeric');
			return false;

	}
	if ($num 	==	1)	return true;
	else return false;

}



/*
 * 	SETS DELETE FLAG FOR CONTACT TO TRUE
 *
 * 	INPUT
 * 		id		INT		THE MYSQL ID OF THE CONTACT ENTRY
 *
 * 	OUTPUT
 * 		BOOL			TRUE IF SUCESS, FALSE F FAILURE
 */
function deleteContact($id, $mode = 'd')	{

	//	TEST INPUTS
	if (!is_numeric($id))	{

		debug ('deleteContact', 'input id not numeric');
		return false;

	}
	if ($id < 1)	{

		debug ('deleteContact', 'input id less than 1');
		return false;

	}
	if ($mode != 'd' && $mode != 'u')	{

		debug ('deleteContact', 'mode not an acceptable option');
		$mode	=	'd';

	}

	//	PREPARE SQL...
	if ($mode 	== 'u') 	$deleted = 0;
		else 	$deleted	=	1;
	$conn		=	mysqlConnect();
	$sql		=	" UPDATE con_contacts SET deleted = $deleted WHERE id = $id ";
	$rs			=	mysqlQuery($sql, $conn);


	//	TEST SQL SUCCESS...
	$sql		=	" SELECT deleted, name FROM con_contacts WHERE id = $id ";
	$rs			=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) != 1) {

		debug ('deleteContact', 'while testing results, mysql num rows was not 1');
		return false;

	}
	$res		=	mysqlGetRow($rs);
	if ($res['deleted'] == $deleted) return $res['name'];
		else return false;

}




/*
 * 	TESTS WHETHER A GIVEN MYSQL ID REPRESENT A DELETED CONTACT FROM THE DATABASE
 *
 * 	INPUT
 * 		id			INT		MYSQL ID OF THE CONTACT FOR TESTING
 *
 * 	OUTPUT
 * 		BOOL				TRUE IF IS DELETED, FALSE IF NOT OR ANY ERROR
 */
function isDeleted($id)	{

		//	TEST INPUTS...
		if (!is_numeric($id))	{

				debug('isDeleted', 'input ID is not numeric');
				return false;

		}

		// PREPARE SQL...
		$conn 		=	mysqlConnect();
		$sql		=	" SELECT deleted FROM con_contacts WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);

		// TEST NUMBER OF RESULTS...
		if (mysqlNumRows($rs,$conn) != 1) return false;

		$res		=	mysqlGetRow($rs);

		// TEST RETURNS...
		if ($res['deleted'] == 1) return true;
			else return false;

}








/*
 * 	CREATES AN HTML NOTICE TO SHOW THE CONTACT THAT WAS JUST ADDED.
 *
 * 	INPUT
 * 		id		INT		THE MYSQL ID OF THE RECORD JUST ADDED
 *
 * 	OUTPUT
 * 		STRING			THE HTML BLOCK
 */
function showJustAddedNotice($id)	{

	//	TEST INPUTS
	if (!is_numeric($id))	{

			debug('showJustAddedNotice', 'input ID not numeric');
			return false;

	}

	// GET DATA
	$contactData_US		=		getContactData($id);
	$formattedContact	=		getFormattedContact($id);

	//	PREPARE OUTPUT
	$output				=		"<div style='border-bottom:solid 1px red; border-top: solid 1px red'>";
	$output				.=		"<p style='color:red'>New contact <b>".cleanForScreenOutput($contactData_US['name'])."</b> added</p>";
	$output				.=		"<p style='color:red'>The SchNEWS web admin has been notified of the request, and when he sobers up he'll have a look and authorise the addition</p>";
	$output				.=		"<p>$formattedContact</p></div>";

	return $output;

}












/*
 * 	CHANGES THE AUTH FLAG OF A GIVEN CONTACT
 *
 * 	INPUT
 * 		id 			INT 		MYSQL ID OF THE CONTACT
 * 		authorise	BOOL		TRUE TO SET TO AUTHORISED, FALSE TO SET TO NOT
 *
 * 	OUTPUT
 * 		STRING		NAME OF MODIFIED RECORD
 */
function authoriseContact($id, $authorise	=	true)	{

	//	TEST INPUTS
	if (!is_numeric($id))	{

			debug('authoriseContact', 'input id not numeric');
			return false;

	}
	if (!is_bool($authorise))	{

			debug ('authoriseContact', 'input authorise not bool');
			return false;

	}


	//	INTGERISE THE AUTH FLAG...
	if ($authorise) $auth = '1';
	else	$auth = '0';


	//	PREPARE SQL
	$conn		=	mysqlConnect();
	$sql		=	" UPDATE con_contacts SET authorised = $auth WHERE id = $id ";

	$rs			=	mysqlQuery($sql, $conn);


	//		TEST RESULTS...
	$sql		=	" SELECT authorised, name FROM con_contacts WHERE id = $id ";
	$rs			=	mysqlQuery($sql, $conn);
	$res		=	mysqlGetRow($rs);
	if ($res['authorised'] != $auth)	{

			debug ('authoriseContact', 'DB does not equal auth var, which is '.$auth);
			return false;
	}	else {

			return $res['name'];

	}

}









function editContactToBeAuthed($id)	{

	//	TEST INPUTS
	if (!is_numeric($id))	{

			debug ('editContactToBeAuthed', 'input id not numeric');
			return false;

	}


	//	DO SQL
	$conn		=	mysqlConnect();
	$sql		=	" SELECT * FROM con_contacts WHERE id = $id ";
	$rs			=	mysqlQuery($sql, $conn);

	if (mysqlNumRows($rs, $conn) != 1)	{

			debug ('editContactToBeAuthed', 'numRows not 1');
			return false;

	}

	$contact	=	mysqlGetRow($rs);



	//	PREPARE OUTPUT

	$output		=	"\r\n<table>";

	$output		.=	"\r\n<tr><td>Name : </td><td><input style='width:500px' type='text' name='name' value='{$contact['name']}' /></td></tr>";
	$output		.=	"\r\n<tr><td>Address : </td><td><textarea style='width:500px; height:100px' name='address'>{$contact['address']}</textarea></td></tr>";
	$output		.=	"\r\n<tr><td>Phone : </td><td><input style='width:500px' type='text' name='phone' value='{$contact['phone']}' /></td></tr>";
	$output		.=	"\r\n<tr><td>Fax : </td><td><input style='width:500px' type='text' name='fax' value='{$contact['fax']}' /></td></tr>";
	$output		.=	"\r\n<tr><td>Web : </td><td><input style='width:500px' type='text' name='web' value='{$contact['web']}' /></td></tr>";
	$output		.=	"\r\n<tr><td>Email : </td><td><input style='width:500px' type='text' name='email' value='{$contact['email']}' /></td></tr>";
	$output		.=	"\r\n<tr><td>Description : </td><td><textarea style='width:500px; height:100px' name='description'>{$contact['description']}</textarea></td></tr>";

	$output		.=	"\r\n</table>";



	return $output;


}












?>