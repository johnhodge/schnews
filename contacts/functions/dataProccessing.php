<?php 


/*
 * 	CLEAN THE INPUT STRING FOR MYSQL INSERTION
 */
function cleanForDatabaseInput($string_UDUS)	{
	
	if (!is_string($string_UDUS)) return false;
	return mysql_real_escape_string($string_UDUS, mysqlConnect());
	
}








/*
 * 	CLEAN THE STRING FOR OUTPUT TO SCREEN
 */
function cleanForScreenOutput($string_US)	{
	
	if (!is_string($string_US)) return false;
	$string 	=	 htmlentities(stripslashes($string_US));

	return $string;
	
}




/*
 * 	CONVERT A STRING TO PROPER/TITLE CASE
 * 
 * 	INPUTS
 * 		string		STRING		THE STRING TO BE MADE PROPER CASE
 * 
 * 	OUTPUT
 * 		STRING					PROPER CASE VERSION OF THE INPUT STRING
 * 		BOOL(false)				ANY ERROR 
 */
function properCase($string)	{
	
	// TEST INPUT TYPE
	if (!is_string($string))	{
		
			debug ('properCase', 'input is not string');
			return false;
		
	}
	
	// TEST INPUT LENGTH
	if (strlen($string) == 0) {
		
			debug('properCase', 'input string is zero length');
			return false;
		
	}
	
	// 	IF STRING IS ONLY 1 LETTER LONG THEN NO REAL PROCESSING NEEDS DOING...
	if (strlen($string) == 1)	return strtoupper($string);
	
	
	// IF ALL IS WELL THEN WE CAN BEGIN PROCESSING THE STRING...
	$words		=	explode(' ',$string);
	foreach($words as $key => $word)	{
	
			$words[$key]		=	strtoupper($word{0}).substr($word, 1);
		
	}
	$string		=	implode(' ',$words);
	
	//	MAKE SURE THAT THE FINAL STRING IS REAL...
	if (strlen($string) == 0)	{
		
		debug ('properCase', 'final output string is zero length');
		return false;	
		
	}
	
	return $string;
	
}




/*
 * 	OUTPUT DEBUGGING INFO
 */
function debug($function, $error)	{
	
	if (!DEBUG) return false;
	
	echo "<div class='debug'>$function :: $error </div>";
	return true;
	
}







function sendSMTPEmail($recipient, $subject, $body)
{
	
	
	mail($recipient, $subject, $body);
	return true;
	
	// TEST INPUTS
	if (!is_string($recipient))	{
		
			debug('sentSMPTEmail', 'recipient not string');		return false;
					
	}
	if (!is_string($subject))	{
		
			debug('sentSMPTEmail', 'subject not string');		return false;
					
	}
	if (!is_string($body))	{
		
			debug('sentSMPTEmail', 'body not string');		return false;
					
	}
	
	
	// CLEAN INPUTS...
	$subject			=	cleanForScreenOutput($subject);
	$body				=	cleanForScreenOutput($body);
		
	
	// Instantiate the PEAR::Mail class and pass it the SMTP auth details
	require_once ('Mail.php');
    $mail = Mail::factory("smtp", array('host' => SMTPHOST, 'auth' => TRUE, 'username' => SMTPUSER, 'password' => SMTPPASS));


	// Create the mail headers and send the mail via our SMTP object
	$headers = array("To"=>$recipient, "From" => SMTPSENDER, "Subject"=>$subject);
	$output	=	$mail->send($recipient, $headers, $body);

	// TEST FOR ERRORS...
	if (PEAR::isError($mail))	{
		
				$return 		=	false;
	}	else	{

				$logFile	=	fopen ('../functions/emailLog.txt', 'a+');
				$datetime	=	makeSqlDatetime();
				$date		=	makeDateFromSQL($datetime);
				$time		=	makeTimeFromSQL($datetime);
				$logOutput	=	"email sent, $date, $time, ".$_SERVER['REMOTE_ADDR'].', '.$_SERVER['REQUEST_URI'].", $subject";
				fwrite ($logFile, $logOutput);
				fclose($logFile);
				$return 		=	true;
	}
	
	return $return;
	
}


?>