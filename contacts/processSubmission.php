<?php
/*
 * 	PROCESSES A SUBMITTED CONTACT APPLICATION AND ADDS TO THE DATABASE IF SUCCESSFUL.
 *
 *
 *
 */


// INCLUDE FUNCTION LIBRARY
session_start();
require_once 'functions/functions.php';





/*
 * 	===============================================================================================
 *
 * 								TEST INPUTS AND PROCESS POST VARS
 *
 *	===============================================================================================
 */



// TEST THAT THE FORM WAS SUBMITTED
if (!isset($_POST['submitNewContact']) || $_POST['submitNewContact'] != "Add Record" )	{

		debug	('processSubmission', 'POST submit not set');
		header("Location: http://www.schnews.org.uk/links/");

}



// VALIDATE INPUTS
//		A VAR WILL BE CREATE FOR EACH INPUT NAME, WHICH WILL EQUAL EITHER T OR F DEPENDING ON WHETHER THE INPUT IS SET CORRECTLY OF NOT...
$fields = array('Name', 'Address', 'Telephone', 'Fax', 'Web', 'Email', 'Description');
foreach ($fields as $field)	{

		$$field = false;
		$data_US[$field] 	=	'';
		debug('processSubmit', $field.'<br />'.$_POST[$field].'<br><br>');
		if (isset($_POST[$field]) && trim($_POST[$field]) != '')	{

				$data_US[$field]	=	cleanForDatabaseInput(trim($_POST[$field]));
				$$field = true;

		}

}



// TEST THE T/F STATUS OF EACH INPUT BASED ON THE ABOVE BLOCK
if ($Name == false)
		$errors[]		=		'No <b>Name</b> was set';

if ($Description == false)
		$errors[]		=		'No <b>Description</b> was set';

if ($Address == false && $Telephone == false && $Fax == false && $Web == false && $Email == false)
		$errors[]		=		'No <b>Contact Information</b> was set';





//	PROCESS THE CATEGORIES, ASSUMING THAT THE OTHER INPUTS ARE OK...
$categories		=	getAllCategories();
foreach ($categories as $id => $category)	{

		if (isset($_POST['category'.$id]) && $_POST['category'.$id] == 'on')	$selectedCategories[] 	=	$id;

}





/*
 * 	============================================================================================================
 *
 * 									ON SUCCESS, TRY TO ADD TO THE DB
 *
 * 	============================================================================================================
 */
$conn 		=		mysqlConnect();

//	It it possible that Name and Description could be empty. If they are we'll fill
//	them with random data so we don't get in incorrect hit...
if (trim($data_US['Name']) == '')			$dup_name	=	md5($date['U']);
else		$dup_name	=	$data_US['Name'];
if (trim($data_US['Description']) == '')	$dup_desc	=	md5($date['U']);
else		$dup_desc	=	$data_US['Description'];


//	TEST FOR DUPLICATES
$sql		=		" SELECT id FROM con_contacts WHERE
							name 	=		'$dup_name' OR
							description = 	'$dup_desc'
					";
$rs			=		mysqlQuery($sql, $conn);
if (mysqlNumRows($rs, $conn) != 0)	{

		$errors[]		=	'Your record already exists in the database';

}


// 	IF THERE ARE NO ERRORS THEN WE SHOULD TRY TO ADD THE RECORD TO THE DATABASE
if (!isset($errors))	{

	//	PREPARE ANCILLARY DATA
	$date		=	makeSqlDatetime();
	$clientIp	=	$_SERVER['REMOTE_ADDR'];

	//	PREPARE SQL
	$sql		=		" INSERT INTO con_contacts
								( name, address, phone, fax, web, email, description, added_on, added_ip, authorised, deleted)
							VALUES
								(	'{$data_US['Name']}',
									'{$data_US['Address']}',
									'{$data_US['Telephone']}',
									'{$data_US['Fax']}',
									'{$data_US['Web']}',
									'{$data_US['Email']}',
									'{$data_US['Description']}',
									'$date',
									'$clientIp',
									0, 0
								)
						";
	$rs				=	mysqlQuery($sql, $conn);
	$newId 			=	mysqlGetInsertId($conn);

	if (!is_numeric($newId))	{

			$errors[]		=	'<p>There was a problem adding your record to the database.</p><p>A message has been sent to our website admin, who will, when he wakes up, have a look and try to do some specially wizardry to fix it.';

	} elseif ( isset($selectedCategories) )	{

			foreach ($selectedCategories as $selectedCategory)	{

					$sql		=	" 	INSERT INTO con_cat_mappings
											(contact_id, cat_id)
										VALUES
											( $newId, $selectedCategory )
					";
					$rs			=	mysqlQuery($sql, $conn);

			}

	}

			//	ADD SOME HERE TO PROCESS THE SENDING OF NOTIFICATION EMAIL AND TO NOTIFY THE SCREEN AS TO SUCCESS.
			$categoriesAdded		=	getCategoriesForContact($newId);
			$categoriesAdded		=	implode(', ', $categoriesAdded);
			$body		=	"
A new contact has been added to the Contacts and Links section. I will need to be authorised by an admin before it will be shown on site.

The details are...


=====================================================


Name : {$data_US['Name']}
Description : {$data_US['Address']}
Telephone : {$data_US['Telephone']}
Fax : {$data_US['Fax']}
Web : {$data_US['Web']}
Email : {$data_US['Email']}r/n
Description : {$data_US['Description']}
Categories : $categoriesAdded

Added On : $date
IP : $clientIp
			";

			$subject		=	"New contact has been added : ID = $newId - {$data_US['Name']} ";
			$recipient		=	'contacts@schnews.org.uk';
			//echo "sending email<br>";
			if (!sendSMTPEmail($recipient, $subject, $body))	{

				echo "<div class='errorMessage'> Problem Sending Email </div>";

			}



}



/*
 *
 * 	TODO :: ADD CODE TO HANDLE ERRORS, AND/OR HANDLE THE SUCCESSFULL ADDITION OF A NEW RECORD (FUNCTIONALISE THIS i GUESS)
 *
 *
 */


//	TEST FOR ERRORS AND DIRECT BACK TO THE SUBMISSIONS FORM IF SO...
if (isset($errors) && is_array($errors))		{

		require '../links/submitcontact.php';
		exit;

}		else	{

		$_SESSION[SES.'just_added']		=	$newId;
		header('location: ../links/index.php');

}


?>