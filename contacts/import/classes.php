<?php

class csv_file	{
	
	protected $csv_file;
	protected $lines;
	protected $num_lines;
	protected $contains_headers;
	protected $line_counter;
	protected $errors;
	
	
	function __construct($csv_file, $contains_headers = true)	{
		
		$this->errors =	new fm_errorHandler();
		
		//	Test inputs
		if (!is_string($csv_file) || trim($csv_file) == '')	{
			
			$this->errors->recordError("Input CSV_FILE not valid string");
			return false;	
			
		}
		
		
		if ($contains_headers == true)	{
			
			$this->contains_headers	=	true;
			$this->line_counter 	=	1;
			
		}	else	{
			
			$this->contains_headers	=	false;
			
		}
		
		//	Split the CSV File
		$this->csv_file		=	$csv_file;
		$this->lines		=	$this->split_file_into_lines($this->csv_file);
		$this->num_lines	=	count($this->lines);
		
		$this->reset_line_counter();
		
		return true;
		
	}
	
	
	protected function split_file_into_lines($file)	{
		
		$file		=	nl2br($file);
		$file		=	str_replace(array("\r", "\n"), '', $file);
		return explode('<br />', $file);
		
	}

	
	public function reset_line_counter()	{
		
		if ($this->contains_headers)	{
			
			$this->line_counter		=	1;
			
		}	else	{
		
			$this->line_counter 	= 0;
			
		}		
		return true;
		
	}
	
	
	public function get_line()	{
		
		//	If we have exceeded the number of lines then return so...
		if (!isset($this->lines[$this->line_counter])) return false;
		
		//	Else get the next line and increment the line counter...
		$line		=	$this->lines[$this->line_counter];
		$this->line_counter++;
		return $line;
		
	}

	
	public function get_processed_line()	{
		
		$line		=	$this->get_line();
		$bits		=	$this->split_line($line);
		
		return $bits;
		
	}
	
	
	protected function split_line($line)	{
		
		// Test inputs	
		if (!is_string($line) || trim($line) == '')	{
			
			$this->errors->recordError("Input LINE is not valid string");
			return false;
			
		}		

		// Cycle through the line and prepare the it to be split into pieces...
		$separator	=	"***!!!SEPARATOR!!!***";
		$output		=	'';
		$in_quotes	=	false;
		for ($n = 0 ; $n < strlen($line) ; $n++ )	{

			if ($line{$n} == ',')	{
				
				if ($in_quotes)	{
					
					$output		.=	',';
					
				}	else	{

					$output		.=	$separator;
					
				}
				
			}	elseif ($line{$n} == '"')	{
				
				if ($in_quotes)		{
					
					$in_quotes	=	false;
					
				}	else	{
					
					$in_quotes	=	true;
					
				}
				
			}	else	{
				
				$output	.=	$line{$n};
				
			}
			
		}
		

		//	Split the line into pieces and remove the quote containers...
		$bits		=	explode($separator, $output);
		foreach($bits	as 	$key => $bit)	{
			
			$bits[$key]		-	str_replace('"', '', $bit);
			
		}
		
		return $bits;
		
	}
	
	
	public function get_num_lines()	{
		
		return $this->num_lines;
		
	}
}

?>