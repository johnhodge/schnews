<?php



function get_categories($con_id, $categories)	{

	//	Test inputs
	if (!is_numeric($con_id) || $con_id == 0)	return false;
	if (!is_string($categories) || trim($categories) == '')	return false;

	$ids			=	array();
	$categories		=	explode('; ', $categories);
	$mysql			=	new fm_mysqlHandler();
	foreach ($categories as $category)	{

		$category 	=	mysql_escape_string(trim($category));

		$sql		=	"	SELECT 	id
							FROM	con_categories
							WHERE 	name	=	'$category' ";
		$rs			=	$mysql->query($sql);
		if ($mysql->getNumRows($rs) != 1)	continue;
		$res		=	$mysql->getRow($rs);
		$ids[]		=	$res['id'];

	}

	if (count($ids) > 0) return $ids;
	else	return false;

}