PHP Framework

(C) 2008 Andrew Winterbottom
suport@tendrousbeastie.com
www.tendrousbeastie.com

This framework remain under copyright to myself (Andrew Winterbottom). Youare free to usethis framework in your own code, with the following conditions...

- It is reproduced in full. No copying and pasting little sections!
- This file, and all header copyright notices, are retain in all files.
- No additional entries or credits are added to the file headers.

I will consider making this framework GPL in the fututre, once I consider it finished. If you are using it and want to persuade me to do so then please email me with your arguements.

Also, and obviously, please feel free to email with comments, suggestions, bug fixes, etc.


===============================================================


How to use:

Any page that wants to use this framework should simply include the framework.php file. All other file will be included by this main controller file.

For further instructions refer to the full instructions file.


===============================================================


Change Log:


v0.0.4 - 26-09-08

- fm_datetime->makeSqlDatetime can now take an optional boolean flag, which determines whether we are makig a datetime for the end of the day (i.e. 23:59:59) or the beginning (ie. 00:00:00)

- new method fm_datetime->makeHumanTime. Makes a human readable time out of a MySQL datetime. Second arguement is a string which control the format. Can be '24; = hh:mm, '12' = hh:mm am/pm, or 'words' = mm minutes past hh.

- fm_drawBlock->makeLink now takes an optional 3rd arguement of 'jsFunction', where an arbitrary JS snippet can be passed to the block.




v0.0.3 - 04-09-08

- Added error reporting to datetime classes

- Misc bug fixes.

- Added new config property 'DISPLAY_DEBUGGING' - which controls whether level 0 errors are reported to screen (for a default instantiation of the errorHandler object.



v0.0.2 - 19-08-08





v0.0.1

Framework created.