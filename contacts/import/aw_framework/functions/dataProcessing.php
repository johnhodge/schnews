<?php


/**
 * Creates a Comma Separated list of the values in the array
 * 
 * Quotes discates whether each element is quote encapsulated...
 * 
 * 0 = no quotes
 * 1 = single quote
 * 2 = double quotes
 *
 * @param array $array
 * @param int $quotes
 * @return unknown
 */
function fm_dp_createCSVFromArray($array, $quotes	=	0)	{
			
			if ($quotes	!= 0 && $quotes != 1 && $quotes != 2) 	$quotes	=	0;		//	QUOTES NEEDS TO BE 0 OR 1		
	
			$errorHandler	=	new fm_errorHandler();				//	NEW ERROR HANDLING OBJECT, JUST IN CASE...
	
			if (!is_array($array) || count($array) < 1)	{
					
					$errorHandler->recordError("input wasn't an array", 1, 'returning false', 'fm_dp_createCSVFromArray');
					return false;
					
			}
			
			$outputs		=	array();
			foreach ($array	as $key => $value)	{							//	CYCLE THROUGH EACH ELEMENT IN THE ARRAY
				
					switch ($quotes)	{									//	SWITCH DEPENDING ON WHAT TYPE OF QUOTING WE ARE USING...
						
						case "0":
							$outputs[]		=	"$key = $value";
							break;
							
						case "1":
							$outputs[]		=	"'$key' = '$value'";
							break;
						
						case "2":
							$outputs[]		=	"\"$key\" = \"$value\"";
							break;
					}						
				
			}
			
			switch (count($outputs))	{				//	WE'LL NEED TO RETURN DIFERENT THINGS DEPENDING ON HOW MANY ELEMENTS WE GOT...
				
				case '0':
					return '';
					
				case '1':
					return $outputs[0];
					break;
					
				case '2':
					return implode(',', $outputs);
					break;
				
			}
		
			return false;
			
	}
	
	
	
	
	
	
	
function br2nl($text)	{
	
		if (!is_numeric($text))	return $text;
		$text	=	str_replace('<br>', "\r\n", $text);
		return		str_replace('<br />', "\r\n", $text);
	
}
	
	
	
?>