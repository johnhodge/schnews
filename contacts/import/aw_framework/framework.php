<?php

/*
CONTROLLER FILE FOR DEV FRAMEWORK.

(c) 2008 Andrew Winterbottom
support@tendrousbeastie.com	
www.tendrousbeastie.com

------------------------------------------------------

This script contains the config settings for the framework, and calls the other entites.
All a program needs to do is include/require this script (and preferably set the dirLocation var.

All config settings can be overriden by having them already set as constants, meaning that all config settings
for a given program could reside in one config file.
*/





/*	================================================================================
				
									CONFIG SETTINGS
									
	================================================================================	*/							
$dirname		=	dirname(__FILE__);		//	GRABS THE CURRENT FILE LOCATION OF THIS CONTROLLER SCRIPT


/*
 * Controlles whether the errors sent to errorHandler are actually output to screen, regardless of the MODE setting.
 *
 * TRUE = display the errors, FALSE = don't display them.
 */
if (!defined('DISPLAY_ERRORS')) define ('DISPLAY_ERRORS', 1);


/*
 * Controlles whether the whether level 0 errors are output to screen or not.
 *
 * TRUE = display the errors, FALSE = don't display them.
 */
if (!defined('DISPLAY_DEBUGGING')) define ('DISPLAY_DEBUGGING', 1);



/*
 * Determines whether we are outputing all SQL to screen or not.
 *
 * TRUE = All SQL events and data are output to screen, FALSE = don't display any SQL events.
 */
if (!defined('MYSQL_DEBUGGING'))	define('MYSQL_DEBUGGING', 1);

	
/*
 * Default error log file path. Will be used for the error handler is not log file is explicilty specified.
 */	
if (!defined('ERROR_LOG_FILE'))		define('ERROR_LOG_FILE', $dirname.'/errorLog.log');

	
/*
 * Default error log database. Will be used for the error handler is not log table is explicilty specified.
 */	
if (!defined('ERROR_LOG_DB_TABLE'))		define('ERROR_LOG_DB_TABLE', 'errorLog');




if (!defined('SMTP_HOST'))				define ('SMTP_HOST', 'pilate');
if (!defined('SMTP_USER'))				define ('SMTP_USER', 'php-pear');
if (!defined('SMTP_PASS'))				define ('SMTP_PASS', 'peacefulsolution');



if (!defined('MYSQL_HOST'))				define('MYSQL_HOST', 'localhost');
if (!defined('MYSQL_USER'))				define('MYSQL_USER', $database_login);
if (!defined('MYSQL_PASS'))				define('MYSQL_PASS', $database_password);
if (!defined('MYSQL_NAME'))				define('MYSQL_NAME', $database_name);



/*	================================================================================
				
									FILE SYSTEM
									
	================================================================================	*/

/*
 * 	The location of the main index file for this project
 */
define ('DIR_ROOT', str_replace('aw_framework', '', dirname(__FILE__)));


/*
 * 	The symbol used to separate / delimit file system entries (/ on linux, \ on Windows)
 */
define ('DIR_DELIMITER', DIRECTORY_SEPARATOR);



/*	================================================================================
				
									CONTROLLER
									
	================================================================================	*/	

// BEGIN REQUIRING THE ENTITIES...
require "$dirname/classes/datetime.php";
require "$dirname/classes/errorHandler.php";
require "$dirname/classes/htmlOutput.php";
require "$dirname/classes/mysql.php";
//require "$dirname/classes/smtphandler.php";

require "$dirname/functions/dataProcessing.php";
















