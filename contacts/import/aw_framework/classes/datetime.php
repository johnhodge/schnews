<?php 
/**
 * 		DateTime Handling Class
 * 
 * 		(c) 2008 Andrew Winterbottom
 */


class fm_dateTimeHandler	{
	
		/**
		 * The date/time as a unix timestamp
		 *
		 * @var int
		 */
		protected $timestamp		=	0;
	
		
		
		function __construct($timestamp = false)	{
			
				//	IF THE TIMESTAMP IS EMPTY THEN USE THE CURRENT DATE/TIME
				if (!$timestamp)	{
					
						$this->timestamp	=	date('U');
					
				//	ELSE TRY TO PARSE THE GIVEN TIMESTAMP...
				}	else 	{
					
						//	TRY TO PARSE IT USING THE STRTOTIME FUNCTION
						if ($this->is_mysql_datetime($timestamp) || $this->is_mysql_date($timestamp))	{
							
								if (!$this->timestamp	=	strtotime($timestamp))	{
									
										return true;
									
								}	else {
									
										return false;
									
								}
						
						//	TRY TO PARSE IS AS A INTEGER...
						}	elseif (is_numeric($timestamp))	{
							
								$this->timestamp		=	$timestamp;
							
						}
						
						//	**********************************************************************************
						//		ADD OTHER ELSEIFS HERE TO HANDLE DIFFERENT ATTEMPTS TO PARSE IT AS NEEDED
						//	**********************************************************************************
									
							else 	{
							
								return false;	
							
						}
										
				}
			
		}
		
		
		/**
		 * Tests whether a given string is a mySQL datetime
		 *
		 * @param string $datetime
		 * @return bool
		 */
		protected function is_mysql_datetime($datetime)	{
			
				//	A MYSQL DATETIME SHOULD HAVE 19 CHARS...
				if (strlen($datetime) != 19)	return false;
				
				// 	IT SHOULD HAVE COLONS AND HYPENS
				if (str_replace(array(':', '-'), '', $datetime) == $datetime)	return false;
				
				// EVERYTHING ELSE SHOULD BE A NUMBER
				if (!is_numeric(str_replace(array(':', '-', ' '), '', $datetime) ) )	return false;
				
				// MUST BE AN SQL DATETIME THEN...
				return true;
			
		}
	
		
		/**
		 * Tests whether a given string is a MySQL Date
		 *
		 * @param string $datetime
		 * @return bool
		 */
		protected function is_mysql_date($datetime)	{
			
				//	A MYSQL DATE SHOULD HAVE 10 CHARS...
				if (strlen($datetime) != 10)	return false;
				
				// 	IT SHOULD HAVE HYPENS
				if (str_replace('-', '', $datetime) == $datetime)	return false;
				
				// EVERYTHING ELSE SHOULD BE A NUMBER
				if (!is_numeric(str_replace(array('-', ' '), '', $datetime) ) )	return false;
				
				// MUST BE AN SQL DATE THEN...
				return true;
			
		}
		
		
		
		/**
		 * Returns the current timestsamp in MySQL datetime format
		 *
		 * @return string
		 */
		function make_mysql_datetime()	{
			
					return date('Y-m-d H:i:s', $this->timestamp);
			
		}
	
	
}