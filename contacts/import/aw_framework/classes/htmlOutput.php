<?php
/**
 * 		Class for displaying tables using PHP
 *
 * 		(c) 2008 Andrew Winterbottom
 */


class fm_drawTable {

	//	NUMBER OF ROWS IN THE GIVEN TABLE
	protected $numRows = 0;

	//  AN OUTPUT BUFFER. ALL CONTENT IS ADDED TO THIS AND THEN THE BUFFER IS FLUSHED USING A METHOD.
	protected $output = "";
	
	// 	IF SET TO ONE (DEFAULT) THEN OUTPUT IS BUFFERED AND MUST BE FLUSHED WITH THE FLUCHBUFER FUNCTION
	protected $buffered = true;
	


	/**
	 * Echoes a TABLE tag with appropriate attributes.
	 *
	 * @param int $width
	 * @param string $align
	 * @return bool
	 */
	function __construct($width = '', $buffered = 1, $align = '', $class = '')	{
			if (!is_numeric($width)) return false;
			if ($align != "left" && $align != "right" && $align != "center") $align = "";
				else $align		=	" align=\"$align\" ";
			if ($class != '') $class = " class=\"$class\" ";
				else $class = '';
			if (is_numeric($width))	$width = " width=\"$width%\" "; 
				else $width = '';

			if ($buffered == 0) $this->buffered = false;
			$this->output .= "<table $width $align $class >";
			if (!$this->buffered) return $this->flushBuffer();
			else return true;
	}

	/**
	 * Adds a TR to start a new row
	 *
	 * @return bool
	 */
	function newRow($rowspan = 0, $valign = '')	{

			if ($valign != '') 	$valign		=	" valign = \"$valign\" ";

			if ($rowspan == 0 || $rowspan == 1)  $this->output .= "<tr $valign >";
			if ($rowspan > 1) $this->output .= "<tr rowspan=\"$rowspan>\" $valign >";
			$this->numRows++;
			if (!$this->buffered) return $this->flushBuffer();
			else return true;
	}

	/**
	 * Adds a /TR to close the table row
	 *
	 * @return bool
	 */
	function endRow()	{

			$this->output .= "</tr>";
			if (!$this->buffered) return $this->flushBuffer();
			else return true;
	}

	/**
	 * Draw a standad cell (a TD)
	 *
	 * @param string $content
	 * @param string $class
	 * @param int $width
	 * @param string $align
	 * @param int $colspan
	 * @param int $rowspan
	 * @return bool
	 */
	function drawCell($content, $class = '', $width = '', $halign = 'center', $valign = 'top', $colspan = 0, $rowspan = 0)	{

			if (!is_numeric($width) && $width != "" && $width != NULL) return FALSE;
			if (!is_numeric($colspan) && $colspan != "" && $colspan != NULL) return FALSE;
			if (!is_numeric($rowspan) && $rowspan != "" && $rowspan != NULL) return FALSE;

			//echo $class;

			$width_t 	=	"";
			if ($width !== 0 && $width != "" && $width !== NULL) $width_t = " width=\"$width%\" ";

			$class_t	=	"";
			if ($class !== 0 && $class != "" && $class !== NULL) $class_t = " class=\"$class\" ";

			$halign_t	=	"";
			if ($halign != "" & $halign !== 0 && $halign !== NULL) $halign_t = " align=\"$halign\" ";

			$valign_t	=	"";
			if ($valign != "" & $valign !== 0 && $valign !== NULL) $valign_t = " align=\"$valign\" ";

			$content_t	=	"&nbsp;";
			if ($content != "" && $content !== 0 && $content !== NULL) $content_t = $content;

			$colspan_t 	=	"";
			if ($colspan != 0 && $colspan != "" && $colspan != NULL) $colspan_t = " colspan=\"$colspan\" ";

			$rowspan_t 	=	"";
			if ($rowspan != 0 && $rowspan != "" && $rowspan != NULL) $rowspan_t = " rowspan=\"$rowspan\" ";

			$this->output .= "<td $class_t $width_t $halign_t $valign_t $colspan_t $rowspan_t >$content_t</td>";
			if (!$this->buffered) return $this->flushBuffer();
			else return true;
			
	}

	
	/**
	 * Returns the number of rows in the currenct table at the moment.
	 *
	 * @return int
	 */
	function getNumRows()	{

			return $this->numRows;

	}

	/**
	 * Fluses the output buffer back to the calling script and resets it to empty.
	 *
	 * @return string
	 */
	function flushBuffer(){

		$temp			=	$this->output;
		$this->output	=	"";
		return $temp;

	}

	/**
	 * Adds an end tabl tag to the output buffer
	 *
	 * @return bool
	 */
	function endTable(){

		$this->output .= "</table>";
		if (!$this->buffered) return $this->flushBuffer();
			else return true;

	}
	
	function __destruct()	{
		
		if ($this->buffered) {
				$this->endTable();
				return $this->flushBuffer();		
		}
		else {
			
			return  $this->endTable();
			
		}
		
	}
}



class fm_drawBlock	{

	/**
	 * Used if no Class is specified
	 *
	 * @var string
	 */
	public $defaultClass = "textNormal";
	/**
	 * Used if no align is specified
	 *
	 * @var string
	 */
	public $defaultAlign = "center";

	protected $buffer = "";
	protected $buffering	=	0;

	function __construct($buffering = 0){

		if ($buffering == 1 || $buffering == true) $this->buffering = 1;
		return true;

	}

	/**
	 * Returns a formatted HTML sting based on the inputs
	 *
	 * @param string $content
	 * @param string $type
	 * @param string $class
	 * @param string $align
	 * @param bool $allowTags
	 * @param bool $lineBreaks
	 * @return string
	 */
	function makeHTMLBlock($content, $type = 'span', $class = '', $align = 'center', $allowTags = true, $lineBreaks = false)	{

			if ($type == "" || $type === 0 || $type === NULL) $type = "div";

			$class_t	=	"";
			if ($class !== 0 && $class != "" && $class !== NULL) $class_t = " class=\"$class\" ";

			$align_t	=	"";
			if ($align != "" & $align !== 0 && $align !== NULL) $align_t = " align=\"$align\" ";

			if (!is_bool($allowTags))	$allowTags = true;
			if (!is_bool($lineBreaks))	$lineBreaks = false;
			
			$content_t	=	"&nbsp;";
			if ($lineBreaks)	{		
				if ($content != "" && $content !== 0 && $content !== null) $content_t = stripslashes(nl2br(str_replace("\\r\\n", '<br />', $content)));
			
			}	else	{
				if ($content != "" && $content !== 0 && $content !== null) $content_t = stripslashes(str_replace("\\r\\n", '<br />', $content));
			
			}
			
			if (!$allowTags) $content_t	=	strip_tags($content_t, '<br /><br>');
			
			$temp		= 		"<$type $class_t $align_t > $content_t </$type>";

			if ($this->buffering == 1){
					$this->buffer  .= $temp;
					return true;
			}
			else return $temp;

	}

	/**
	 * Draws a horizontal line, using the tableHorizDivider class. Sort of a newer HR.
	 *
	 * @param int $width
	 * @return bool
	 */
	function drawHorizDivider($width)	{

			if (!is_numeric($width)) return FALSE;
			$output		=	"<table align=\"center\" width=\"$width%\">";
			$output		.=	"<tr><td class=\"horiz_divider\"><br></td></tr></table><br>";

			if ($this->buffering == 1){
					$this->buffer  .= $output;
					return true;
			}
			else return $output;

	}

	function drawLineBreaks($num = 1){

			if (!is_numeric($num)) return false;
			$output		=	"";
			for ($n = 1; $n <= $num; $n++){
					$output .= 	"<br />";
			}
			if ($this->buffering == 1){
					$this->buffer  .= $output;
					return true;
			}
			else return $output;
	}

	function makeMailto($addr){

		$temp = "<a href=\"mailto:$addr\">$addr</a>";
		if ($this->buffering == 1){
					$this->buffer  .= $temp;
					return true;
			}
		else return $temp;
	}

	function makeLink($text, $link, $jsFunction = false){

			$js	=	'';
			if (is_string($jsFunction))	$js	=	$jsFunction;
		

			$temp	=	 "<a href=\"$link\" $js>$text</a>";
			if ($this->buffering == 1){
					$this->buffer  .= $temp;
					return true;
			}
			else return $temp;
	}

	function flushBuffer(){

			$temp		=	$this->buffer;
			$this->buffer = "";
			return $temp;

	}

	function emptyBuffer(){
			$this->buffer = "";
			return true;

	}

	/**
	 * Turns off output buffering for the object
	 *
	 * @param INT $flush 0 = leave buffer intact, 1 = flush buffer, 2 = empty buffer
	 * @return BOOL
	 */
	function disableBuffering($flush	=	0){
			$this->buffering 	=	0;
			switch ($flush){

				case 1:
					$temp	=	$this->buffer;
					$this->buffer	=	"";
					return $temp;
					break;

				case 2:
					$this->buffer	=	"";
					return true;
					break;

			}

			return  true;
	}

	/**
	 * Turns buffering on for the object
	 *
	 * @return BOOL
	 */
	function enableBuffering(){
			$this->buffering	=	1;
			return true;

	}
}



class fm_drawForm{

	protected $openForm;

	/**
	 * Echos a FORM tag with the relevant properties
	 *
	 * @param string $method Can be POST or GET, else the function with return FALSE
	 * @param string $action Where the form send te data to
	 * @param string $enctype Optional (default = urlencode)
	 * @param string $name Optional (default = "")
	 * @return bool
	 */
	function __construct($method, $action, $name = "", $enctype = 'application/x-www-form-urlencoded')	{

				if (strtolower($method) !== "post" && strtolower($method) !== "get") return false;
				if ($action === 0 | $action === null || $action === "") return false;
				if ($name !== null && $name !== "") $name = " name=\"$name\" ";

				$this->openForm = "<form $name method=\"$method\" action=\"$action\" enctype=\"$enctype\">";
				return true;

	}

	function openForm(){

			return $this->openForm;
	}

	function closeForm(){

			return "</form>";
	}

	/**
	 * Closes the FORM tag
	 *
	 * @return bool
	 */
	function __destruct(){
		return true;

	}

	/**
	 * Returns an INPUT tag of either TEXT or PASSWORD type
	 *
	 * @param string $name
	 * @param string $value Optional
	 * @param string $class OPtional
	 * @param string $isPassword Optional
	 * @param string $size Optional
	 * @return string
	 */
	function drawTextInput($name, $value = "", $class = "", $tip = "", $jsType = "", $jsFunction = "", $readonly = 0, $isPassword = 0, $size = ""){

				if ($name === "" || $name === 0 || $name === null) return false;
				if ($value !== "") 	$value 	= 	" value=\"".stripslashes($value)."\" ";
				if ($size !== "" && is_numeric($size) )	$size 	=	" size=\"$size\" ";
				if ($class !== "" )	$class 	=	" class=\"$class\" ";
				if ($tip !== "") $tip		=	"onmouseover=\"Tip('$tip')\"";
				if ($isPassword != 1) $type = " type=\"text\" ";
				else $type = " type=\"password\" ";
				if ($readonly == 1) $readonly	=	" readonly=\"1\" ";
				else 	$readonly = "";


				if ($jsType != "" && is_array($jsFunction) && count($jsFunction) > 0) $js	=	makeJSEvent($jsType, $jsFunction);
				else $js	=	"";


				$input 	=	 "<input $tip $js $type $readonly name=\"$name\" $value $size $class />";
				return $input;

	}

	function drawHidden	($name, $value)	{

				if ($name === "" || $name === 0 || $name === null) return false;
				if ($value !== "") 	$value 	= 	" value=\"".stripslashes($value)."\" ";
				return "<input type=\"hidden\" name=\"$name\" $value >";

	}

	function drawTextArea($name, $value, $class = "", $tip = "", $rows = 5, $cols = ""){

				if ($name === "" || $name === 0 || $name === null) return false;
				if ($value !== "") 	$value 	= 	$value;
				if ($class !== "" )	$class 	=	" class=\"$class\" ";
				if ($tip !== "") $tip		=	"onmouseover=\"Tip('$tip')\"";
				if (is_numeric($rows)) $rows	=	" rows=\"$rows\" ";
				if (is_numeric($cols)) $cols	=	" cols=\"$cols\" ";
		
				return "<textarea name=\"$name\" $tip $class $rows $cols >$value</textarea>";

	}

	function drawSelect($name, $options, $selected = "", $values = array(), $addBlank = false, $tip = "") {

				if ($name === "" || $name === 0 || $name === null) return false;
				if (!is_array($options) || count($options) < 1) return false;
				if (!is_bool($addBlank))	$addBlank = false;
				if (!is_array($values) || count($values) !== count($options)) $values = $options;
				if ($tip !== "") $tip		=	"onmouseover=\"Tip('$tip')\"";

				$output 	=	"<select $tip name=\"$name\">";
				if ($addBlank)	{
					
						$output	.=	"<option></option>";
					
				}
				for($n = 0; $n < count($options) ; $n++)	{
							$output 	.= "<option value=\"".$values[$n]."\"";
							if ($values[$n] == $selected) $output 	.=	" selected ";
							$output		.=	" >".$options[$n]."</option>";
					}
				$output		.=	"</select>";

				return $output;

	}

	function drawSubmit($name, $value, $tip = "", $warn = 0, $class = '')	{

				if ($name === "" || $name === 0 || $name === null) return false;
				if ($value === "" || $value === 0 || $value === null) return false;
				if ($class !== "" )	$class 	=	" class=\"$class\" ";

				if ($tip !== "") $tip		=	"onmouseover=\"Tip('$tip')\"";
				if ($warn === 1) $js = " onClick=\"return confirmSubmit()\" ";
				else $js = "";
				return "<input $tip type=\"submit\" $class name=\"$name\" value=\"$value\" $js>";

	}

	function drawCheckbox($name, $checked = 0, $tip = ""){

			if ($tip !== "") $tip		=	"onmouseover=\"Tip('$tip')\"";
			$output		=	"<input type=\"checkbox\" $tip name=\"$name\" ";
			if ($checked == 1) $output .= " checked ";
			$output 	.=	" > ";
			return $output;
	}

	function drawDateSelector($name, $datetime, $tip = 0){

			if ($name == "" || $name === null) return false;

			$y		=	substr($datetime, 0, 4);
			if (!is_numeric($y)) return false;

			$m		=	substr($datetime, 5, 2);
			if (!is_numeric($m)) return false;

			$d		=	substr($datetime, 8, 2);
			if (!is_numeric($d)) return false;



			// DAY SELECTOR
			if ($tip == 1)		$tipt	=	"Select the day of the month";
			else $tipt = "";

			// Make an array of the 31 days
			for ($n = 0; $n <= 31; $n++)	{

					// Add a preceding zero to single digit values
					if (strlen($n) == 1)	{
							if ($n == 0)	$days[] = "";
							else $days[]		=	"0".$n;
							$values[] = "0".$n;
					}
					else { $days[]	=	$n; $values[] = $n;	}

			}
			$output	= $this->drawSelect($name."_day", $days, $d, $values, $tipt);




			// MONTH SELECTOR
			$months		=	array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
			$values		=	array("00", "01","02","03","04","05","06","07","08","09","10","11","12");

			if ($tip == 1)		$tipt	=	"Select the month";
			else $tipt = "";

			$output	.=	" ".$this->drawSelect($name."_month", $months, $m, $values, $tipt);



			unset ($values);
			// YEAR SELECTER
			$years[]		=	"";
			$values[]		=	"0000";

			for ($n = 2003; $n<=2015; $n++){

					$years[] = $n;
					$values[] = $n;

			}
			if ($tip == 1)		$tipt	=	"Select the year";
			else $tipt = "";

			$output	.=	" ".$this->drawSelect($name."_year", $years, $y, $values, $tipt);

			return $output;

	}

	/**
	 * Draws a link calling a javascript to fill the given date selecter with todays date
	 *
	 * @param string $form (name of the form in question)
	 * @param string $element
	 * @return string
	 */
	function drawNowLink($form, $element)	{

			if ($form == "" || $form === null) 			return false;
			if ($element == "" || $element === null)	return false;
			return "<a onclick=\"makeToday('$form', '$element')\">Now</a>";

	}

}














