<?php
//if (!defined('DISPLAY ERRORS'))	define('DISPLAY_ERRORS', true);			//	JUST IN CASE WE HAVE A PROBLEM ACCESSING OUR CONFIG FILE



class fm_errorHandler	{

	protected $mode = array(0 => '0', 1 => '1', 2 => '12', 3 => '123', 4 => '123', 5 => '1234' );		// Mode governs whether we are outputing to screen, logging to file, dB or email admins.
		/*			An array where index = error level, and element = activity.
					Elements can contains combinations of activities. See default array for examples.
		
					Acitivites
					0	=	Do nothing;
					1 	=	Output to screen;
					2	=	Log to file 		(depends on the file path being set correctly, otherwise will default to 0)	
					3	=	Log to database		(depends on a sane mysqlHandler object being passed, otherwise defaults to 0)
					4 	=	Email to admin		(depends on a sane SMTP object being passed and a sane recipient address, otherwise defaults to 0)
					
					Potential error levels are:
					0	=	None			(Same as notice, but will never get output as a level 1 mode)
					1	=	Notice 			(Not an error as such, but just output that might be useful)
					2	=	Warning			(Something is wrong, but it is not such a problem as to stop the rest of the script running successfully)
					3	=	Error			(Something has gone wrong, and it could well cause the rest of the script to stop running properly)
					4 	= 	Citical Error 	(This error is serious. It will definately result in faulty execution)
					5	=	Fatal Error		(This error is so serious that is could corrupt persistent data - Db or filesystem, etc. This error level will always result in a exit of the script)
		*/	
					
					
	
	//	STATS
	protected	$numErrors	=	0;			// The number of errors this object has processed.
	protected	$highestErrorLevel	=	0;	// The max value of the erro level encountered by this object.
		
	// 	OBJECTS
	protected 	$mysql;						// Contains a MySQL object, if set.
	protected	$smtp;						// Contains an SMTP object, if set.
	
	//	SETTINGS
	protected	$logFile	=	ERROR_LOG_FILE;			//	Location of text log file.
	protected 	$logTable 	= 	ERROR_LOG_DB_TABLE;		//	The MySQL table name for the error log.
	
		
	function __construct($mode = 0, $mysql = false, $logFile = false, $logTable = false, $smtp = false)		{
		
			if (DISPLAY_DEBUGGING) $this->mode[0] = 1;		// 	IF DEBUGGING IS ENABLED THEN OUTPUT LEVEL 0 ERRORS TO SCREEN
		
			// SORT OUT THE MODE ARRAY, IF SET.
			if (is_array($mode) && count($mode) <= 6)		{			//	MAKE SURE MODE IS AN ARRAY AND DOESN'T HAVE MORE THAN THE MAX NUMBER OF ELEMENTS SET.
				
					
					for ($n = 0 ; $n <= 6; $n++)	{					//	CYCLE THROUGH EACH POTENTIAL ELEMENT/ERROR LEVEL
						
						if (isset($mode[$n]) && is_numeric($mode[$n]) && strlen($mode[$n] <= 4)) {
							
								$this->mode[$n] = $mode[$n];
							
						}
						
					}
				
			}
			
			
			// TEST MYSQL INPUT
			if (is_object($mysql))	{
				
					$this->mysql	=	$mysql;
				
			}
			
			
			//	TEST STMP INPUT
			if (is_object($smtp))	{
				
					$this->smtp		=	$smtp;
					
			}
			
			if (file_exists(dirname($logFile)))	{
				
					$this->logFile	=	$logFile;	
				
			}
			
			if (is_string($logTable) && $logTable != '')	{
				
					$this->logTable	=	$logTable;
				
			}
			
			return true;
				
	}
	
	
	/**
	 * Checks if the error log table exists in the current database and creates it if not.
	 *
	 * @return bool
	 */
	protected function checkTableExists()	{
		
		if (!is_object($this->mysql))	return false;		// THIS FUNCTION CAN ONLY WORK IF WE HAVE A VALID MYSQL OBJECT TO PLAY WITH.
		if($this->mysql->tableExists($this->logTable))	return true;	// CHECK WHETHER THE TABLE ALREADY EXISTS.
		
		
		// IF THE TABLE DOESN'T EXIST WE WILL HAVE TO CREATE IT.
		$sql		=	" CREATE TABLE {$this->logTable} (
							id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
							datetime TIMESTAMP (8),
							remoteIP VARCHAR (15),
							URLCalled VARCHAR (255),
							class VARCHAR (255),
							function VARCHAR (255),
							errorMessage VARCHAR (255),
							errorLevel INT (5),
							actionTaken VARCHAR (255),
							post TEXT,
							get TEXT,
							session TEXT
						)	";
		$rs			=	$this->mysql->query($sql);
		
		// IF THIS RETURNS FALSE IT MEANS OUR ATTEMPTS TO CREATE A TABLE HAVE FAILED (MAYBE NO PERMISSION).
		return $this->mysql->tableExists($this->logTable);
		
	}
	
	
	
	/**
	 * Receives an error messages and then calls other funcions to hanles the output, depending on the errorLevel
	 *
	 * @param string $errorMessage
	 * @param int $errorLevel
	 * @param string $actionTaken
	 * @param string $function
	 * @param string $class
	 * @return bool
	 */
	function recordError($errorMessage, $errorLevel = 1, $actionTaken = '', $function = '', $class = '')	{
		
		if ($errorMessage == '') 	return false;											// WE MUST HAVE AN ERROR MESSAGE
		if (!is_numeric($errorLevel) || strlen($errorLevel) != 1)	return false;			// ERROR LEVEL MUST BE A NUMBER.
		
		$action		=	$this->mode[$errorLevel];

		if (strpos(' '.$action, '1') != false)			
			$this->outputErrorScreen($errorMessage, $errorLevel, $actionTaken, $function, $class);
			
		if (strpos(' '.$action, '2') != false)			
			$this->outputErrorTextFile($errorMessage, $errorLevel, $actionTaken, $function, $class);			
		
		if (strpos(' '.$action, '3') != false)			
			$this->outputErrorDatabase($errorMessage, $errorLevel, $actionTaken, $function, $class);	
			
		return true;		
		
	}
	
	/**
	 * Outputs an error message to screen, this handling level 1 activities of this->mode
	 *
	 * @param string $errorMessage
	 * @param int $errorLevel
	 * @param string $actionTaken
	 * @param string $function
	 * @param string $class
	 * @return bool
	 */
	protected function outputErrorScreen($errorMessage, $errorLevel, $actionTaken = '', $function = '', $class = '')	{
		
		if ($errorMessage == '') 	return false;				// WE MUST HAVE AN ERROR MESSAGE
		if (!is_numeric($errorLevel) || strlen($errorLevel) != 1)	return false;			// ERROR LEVEL MUST BE A NUMBER.
	
		$string		=	'';
		if ($errorLevel == 0) 	$string		=	"<br /><b>Debugging</b> ";
		if ($errorLevel == 1) 	$string		=	"<br /><b>Notice</b> ";
		if ($errorLevel == 2) 	$string		=	"<br /><b>Warning</b> ";
		if ($errorLevel == 3) 	$string		=	"<br /><b>Error</b> ";
		if ($errorLevel == 4) 	$string		=	"<br /><b>Critical Error</b> ";
		if ($errorLevel == 5) 	$string		=	"<br /><b>Fatal Error</b> ";

		$string	.=	" :: <i><font color=\"red\"> $errorMessage </font></i> ";		
		if ($class != '')		$string .= " :: <b>Class</b> '$class' ";
		if ($function != '') 	$string	.= " :: <b>Function</b> '$function' ";
		if ($actionTaken != '')	$string	.=	" ;; <b>Action Taken</b> '$actionTaken' ";
		$string	.=	"<br />";
		
		if (DISPLAY_ERRORS)	echo $string;		// ONLY DISPLAY THE ERROR IS THE CONF SETTING IS TRUE.
		
		return true;
		
	}
	
	
	/**
	 * Logs the error to a text file. Creates the text file if it doesn't exist.
	 *
	 * @param string $errorMessage
	 * @param int $errorLevel
	 * @param string $actionTaken
	 * @param string $function
	 * @param string $class
	 * @return bool
	 */
	protected function	outputErrorTextFile ($errorMessage, $errorLevel, $actionTaken = '', $function = '', $class = '')	{
		
		if ($errorMessage == '') 	return false;				// WE MUST HAVE AN ERROR MESSAGE
		if (!is_numeric($errorLevel) || strlen($errorLevel) != 1)	return false;			// ERROR LEVEL MUST BE A NUMBER.
		
		$this->checkLogFileExists();
		
		if (!$file		=	fopen($this->logFile, 'ab'))	{			//	TRY TO OPEN THE FILE, DISPLAY ERROR IF FAIL
			
				$this->outputErrorScreen('failed to open log file', 1, 'returning false', 'outputErrorTextFile', 'errorHandler'); 	//	IF WE CAN'T OPEN THE FILE THEN DISPLAY AN ERROR MESSAGE AND GET OUT.
				return false;	
		}
		
		$date		=	new fm_dateTimeHandler();			//	DATETIME OBJECT REQUIRE TO GET CURRENT TIME.
		
		$string		=	$date->makeSqlDatetime()."\t";	//	GET CURRENT TIME.
		$string		.=	$_SERVER['REMOTE_ADDR']."\t";	//	GET USER'S IP ADDRESS.
		
		$temp		=	'';

		if ($errorLevel == 0) 	$temp		=	"Invisible Notice [0]";
		if ($errorLevel == 1) 	$temp		=	"Notice [1]";
		if ($errorLevel == 2) 	$temp		=	"Warning [2]";
		if ($errorLevel == 3) 	$temp		=	"Error [3]";
		if ($errorLevel == 4) 	$temp		=	"Critical Error [4]";
		if ($errorLevel == 5) 	$temp		=	"Fatal Error [5]";
		
		$string		.=	$temp."\t";						//	TEMP CONTAINS TEXTUAL DESCRIPTION OF THE ERROR LEVEL.
		$string		.=	$errorMessage."\t";
		$string		.=	$class."\t";
		$string		.=	$function."\t";
		$string		.=	$actionTaken."\t";
		$string		.=	"\r\n";		
		
		if (!fwrite	($file, $string))	{				//	ATTEMPT TO WRITE THE DATA TO THE LOG FILE.
			
				$this->outputErrorScreen('failed to write to log file', 1, 'returning false', 'outputErrorTextFile', 'errorHandler');
				$file	=	fclose($file);
				$date	=	null;
				return false;
			
		}
		
		$file	=	fclose($file);
		$date	=	null;
				
		return true;
		
	}
	
	
	
	/**
	 * Test whether the log file exists, and attempts to create it if not.
	 *
	 * @return bool
	 */
	protected function checkLogFileExists()	{
		
		if (!is_string($this->logFile) || $this->logFile == '')	{		//	TEST THAT LOGFILE IS SET.
				$this->outputErrorScreen("log file isn't set", 1, 'returning false', 'checkLogFileExists', 'errorHandler');
				return false;
		}
		
		if (file_exists($this->logFile))	return true;				//	IF THE LOGFILE ALREADY EXISTS THEN WE HAVE NOTHING TO DO HERE.
		if (!$file		=	fopen($this->logFile, 'ab'))	{			//	TRY TO OPEN THE FILE, DISPLAY ERROR IF FAIL
				$this->outputErrorScreen("can't create new file", 1, 'returning false', 'checkLogFileExists', 'errorHandler');
				return false;
			
		}
		
		$string		=	"DateTime\tI.P. Address\tError Level\tError Message\tClass\tFunction\tAction Taken\r\n \r\n"; 	//	MAKE LOG FILE HEADER
		if (!fwrite($file, $string))	{			//	TRY TO ADD HEADERS TO THE NEWLY CREATE FILE
				$this->outputErrorScreen("can't write headers to new log file", 1, 'returning false', 'checkLogFileExists', 'errorHandler');
				return false;
			
		}
		
		$file	=	fclose($file);
		return true;
		
	}
	
	
	protected function outputErrorDatabase($errorMessage, $errorLevel, $actionTaken = '', $function = '', $class = '')	{
		
		if ($errorMessage == '') 	return false;											// WE MUST HAVE AN ERROR MESSAGE
		if (!is_numeric($errorLevel) || strlen($errorLevel) != 1)	return false;			// ERROR LEVEL MUST BE A SINGLE DIGIT NUMBER.
		if (!is_object($this->mysql))		{
				$this->outputErrorScreen("no mysql object set when trying to log to DB", 1, 'returning false', 'outputErrorDatabase', 'errorHandler');
				return false;	
		}
		
		
		// 	TEST AND CLEAN INPUTS
		$errorMessage 	=	mysql_escape_string($errorMessage);	
		
		if (!is_string($class))	$class	=	'';
		$class			=	mysql_escape_string($class);
		
		if (!is_string($function))	$function	=	'';
		$function			=	mysql_escape_string($function);		
		
		if (!is_string($actionTaken))	$actionTaken	=	'';
		$actionTaken			=	mysql_escape_string($actionTaken);
		
		$post		=	mysql_escape_string(fm_dp_createCSVFromArray($_POST));
		$get		=	mysql_escape_string(fm_dp_createCSVFromArray($_GET));
		if (session_id() != '')
			$session	=	mysql_escape_string(fm_dp_createCSVFromArray($_SESSION));
		else 
			$session	=	'';
		
		
		
		//	ENSURE THAT THE DATABASE TABLE EXISTS.
		//		WE'LL HAVE PROBLEMS HERE IF THE MYSQL OBJECT DOESN'T HAVE CREATE PERMISSIONS ON THE MYSQL SERVER.
		if (!$this->checkTableExists())		{
				$this->outputErrorScreen("can't verify or create log table", 1, 'returning false', 'outputErrorDatabase', 'errorHandler');
				return false;	
		}
		
		
		// BEGIN SQL INSERTION...
		$sql	=	" 	INSERT INTO {$this->logTable} (
							remoteIP,URLCalled, class, function, errorMessage, errorLevel, actionTaken, post, get, session )
						VALUES
							( 	'{$_SERVER['REMOTE_ADDR']}',
								'{$_SERVER['REQUEST_URI']}',
								'$class',
								'$function',
								'$errorMessage',
								'$errorLevel',
								'$actionTaken',
								'$post',
								'$get',
								'$session'	) ";
		$rs		=	$this->mysql->query($sql);
		
		if (!$this->mysql->getInsertId())	{
				$this->outputErrorScreen("SQL insertion didn't work", 1, 'returning false', 'outputErrorDatabase', 'errorHandler');
				return false;	
		}
		
		return true;
		
	}
	
	
	function __destruct()	{
		
		$this->mysql	=	null;
		$this->smtp		=	null;
		return true;
		
	}
	
	
	
}

?>