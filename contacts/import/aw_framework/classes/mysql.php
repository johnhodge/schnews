<?php 
/**
 * 		MySQL Handling Class
 * 
 * 		(c) 2008 Andrew Winterbottom
 */



class fm_mysqlHandler	{
	
	protected  $conn;
	protected  $res;
	protected  $error;
	protected  $dbName;	
	
	protected  $errorHandler;
	
	/**
	 * Connects to MySQL Server and selects DB. This connection is then available throughout the rest of the object.
	 *
	 * @param string $dbHost
	 * @param string $dbUser
	 * @param string $dbPass
	 * @param string $dbName
	 * @return bool
	 */
	function __construct($dbHost = MYSQL_HOST , $dbUser = MYSQL_USER, $dbPass = MYSQL_PASS, $dbName = MYSQL_NAME )	{
				
			if ($dbHost == "" || $dbUser == "" || $dbPass == "" || $dbName == "") return FALSE;
			$this->conn 	= 	mysql_connect($dbHost, $dbUser, $dbPass, TRUE);
			$this->dbName	=	$dbName;
			if (MYSQL_DEBUGGING)	echo "<p><b><i>SQL :: New Sql Connection Made</i></b></p>";
			
			$this->errorHandler		=	new fm_errorHandler();
			return true;
			
	}
	
	
	
	/**
	 * Executes an SQL query and returns the resource RS
	 *
	 * @param string $sql
	 * @return bool
	 */
	function query($sql)	{
		
			if (MYSQL_DEBUGGING) echo "<p><b><em>SQL::: $sql </em></b></p>";
			
			if ($sql == "") return false;
			mysql_select_db($this->dbName, $this->conn);
			$this->error	=	mysql_error();
			$this->getError();
			
			$rs 	=	mysql_query($sql, $this->conn);
			$this->error	=	mysql_error();
			$this->getError();
			
			return $rs;
		
	}
	
	/**
	 * Grabs the next row from the result and returns it in a named array
	 *
	 * @return array
	 */
	function getRow($rs)	{
		
			$result	=	mysql_fetch_array($rs);
			if ($result == FALSE) { 
					$this->error 	= 	"No Rows Left";
					return FALSE;
			}
			if (mysql_error() != "")	{
					$this->error	=	mysql_error();
					return FALSE;				
			}
			
			$this->res 	=	$result;
			return $result;
		
	}
	
	/**
	 * Returns the most recent error. Basically a copy of mysql_error
	 *
	 * @return string
	 */
	function getError(){
			
			if (isset($this->error) && $this->error != "")	{
					$error			=	$this->error;
					$this->error	=	'';
					if (MYSQL_DEBUGGING)	echo "<br />SQL :: $error <br />";
					else return '';
			}
			else return FALSE;
		
	}
	
	function getNumRows($rs)	{
		
			return mysql_num_rows($rs);		

	}
	
	function getInsertId(){
		
		$id		=	mysql_insert_id($this->conn);
		if (!is_numeric($id) || $id == 0) return false;
		return $id;
		
	}
	
	/**
	 * Closes the MySQL connection and then the class
	 *
	 * @return bool
	 */
	function __destruct(){
		
		unset($this->errorHandler);
		mysql_close($this->conn);
		return TRUE;
		
	}
	
	
	function tableExists($table)	{
		
		if (!is_string($table))	{
			
			// RAISE A NOTICE (1) LEVEL ERROR
			return false;
			
		}
		
		$table	=	mysql_escape_string($table);
		
		$rs		=	$this->query(" SHOW TABLES LIKE '$table' ");
		if ($this->getNumRows($rs) == 1) return true;
		else return false;
		
	}
	
}