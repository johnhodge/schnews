<?php 
/**
 * 		SMTP Handling Class
 * 
 * 		(c) 2008 Andrew Winterbottom
 */


//	PEAR INCLUDES
include('Mail.php');
include('Mail\mime.php');
    
    
class fm_smtpEmail	{
	
	//	OBJECTS
	protected 	$smtpConn;				//	PEAR OBJECT FOR THE SMTP CONNECTION;
	protected 	$errorHandler;			// 	ERROR HANDLING OBJECT;
	
	
	
function __construct	($smtpHost, $smtpUser, $smtpPass)	{
	
		$this->errorHandler		=	new fm_errorHandler();
		
		
		//	TEST CONFIG CONSTANTS...
		if (!isset($smtpHost) || $smtpHost == '')	{
			
				$this->errorHandler->recordError('constant SMTP_HOST not defined', 1, 'returning false', 'constructor', 'fm_smtpEmail');
				return false;
			
		}
		if (!isset($smtpUser) || $smtpUser == '')	{
			
				$this->errorHandler->recordError('constant SMTP_USER not defined', 1, 'returning false', 'constructor', 'fm_smtpEmail');
				return false;
			
		}
		if (!isset($smtpPass) || $smtpPass == '')	{
			
				$this->errorHandler->recordError('constant SMTP_PASS not defined', 1, 'returning false', 'constructor', 'fm_smtpEmail');
				return false;
			
		}
		
		
		//	CREATE MAIL CONNECTION...
		$this->smtpConn = Mail::factory("smtp", array('host' => $smtpHost, 'auth' => TRUE, 'username' => $smtpUser, 'password' => $smtpPass));
		
		
		if (PEAR::isError($this->smtpConn))	{

    			$this->errorHandler->recordError('Could not create MAIL object', 1, 'returning false', 'constructor', 'fm_smtpEmail');
				return false;
				    	
    	}
		
		
	}
	
	

function sendMimeSmtp($recipient, $sender, $subject, $body)		{
	
		


    if(!$message = new Mail_mime())	{
    	
    		echo "no mail_mime";
    	
    }
    $html = $body;

    if (!$message->setHTMLBody($html))	{
    	
    		echo "no setHTMLBody";
    	
    }
    
    if (!$body = $message->get())	{
    	
    		echo "no mime get";
    	
    }
    $extraheaders = array("From"=>$sender, "Subject"=>$subject);
    
    if (!$headers = $message->headers($extraheaders))	{
    	
    		echo "no mime headers";
    	
    }

    

    //$mail = Mail::factory("mail");
    if (!$this->smtpConn->send($recipient, $headers, $body))	{
    	
    		echo "no mail send";
    	
    }


			if (PEAR::isError($this->smtpConn))
				{
					return  false;
				}
			else
				{
					return 	true;
				}
	
}

}




