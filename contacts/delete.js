
function confirmSubmit()
        {
        var agree=confirm("Are you sure you want to delete this record? It cannot be recovered once it is deleted.");
        if (agree)
            return true ;
        else
            return false ;
        }

function confirmUndelete()
{
var agree=confirm("Are you sure you want to undelete this record? It will be placed immediately back into the public database.");
if (agree)
    return true ;
else
    return false ;
}

function confirmSpam()
{
var agree=confirm("Are you sure you want to mark this as spam? Doing so will remove it from public display.");
if (agree)
    return true ;
else
    return false ;
}

function confirmAuth()
{
var agree=confirm("Are you sure you want to authorise this contact? It will cause it to be visible to the public, so please be careful.");
if (agree)
    return true ;
else
    return false ;
}