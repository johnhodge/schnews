<?php



// Process the POST vars and set some error vars if there are problems or missing 
$mysql	=	new mysql_connection();

if (		isset($_POST['date_from_day']) && $_POST['date_from_day'] != "" &&
			isset($_POST['date_from_month']) && $_POST['date_from_month'] != "" &&
			isset($_POST['date_from_year']) && $_POST['date_from_year'] != ""	)	{

				if (strlen($_POST['date_from_day']) == 1) $d		=	"0".$_POST['date_from_day'];
				else	$d		=	$_POST['date_from_day'];		
				$m				=	$_POST['date_from_month'];
				$y				=	$_POST['date_from_year'];
				
				$date_from		=	"$y-$m-$d 00:00:00";
				
		}	else	{
		
				$errors['date_from']	=	"The Date wasn't specified correctly";
				
		}
		
		
		
		
		

if (isset($_POST['name']) && $_POST['name'] != "")	{

				$name		=	$mysql->clean_string($_POST['name']);
				
		}	else	{
		
				$errors['name']	=	"No event name was specified";
				
		}
		


if (isset($_POST['summary']) && $_POST['summary'] != "")	{

				$summary		=	$mysql->clean_string($_POST['summary']);
				
		}	else	{
		
				$errors['summary']	=	"No description was specified";
				
		}
		



if (isset($_POST['name']) && $_POST['name'] != "")	{

				$name		=	$mysql->clean_string($_POST['name']);
				
		}	else	{
		
				$errors['name']	=	"No event name was specified";
				
		}		
		
		
		
		
		
		
		
		
		
		
		
		
		
if (		isset($_POST['date_to_day']) && $_POST['date_to_day'] != "" &&
			isset($_POST['date_to_month']) && $_POST['date_to_month'] != "" &&
			isset($_POST['date_to_year']) && $_POST['date_to_year'] != ""	)	{

				if (strlen($_POST['date_to_day']) == 1) $d		=	"0".$_POST['date_to_day'];
				else	$d		=	$_POST['date_to_day'];		
				$m				=	$_POST['date_to_month'];
				$y				=	$_POST['date_to_year'];
				
				$date_to		=	"$y-$m-$d 00:00:00";
				
		}	else	{
		
				$date_to		=	"";
					
		}
		
		
if (isset($date_to) && isset($errors['date_from']))		$errors['date_from']	=	"The From Date wasn't specified correctly";







if (isset($_POST['time']) && $_POST['time'] != "")	{

				$time		=	$mysql->clean_string($_POST['time']);
				
		}	else	{
		
				$time		=	"";
				
		}
		
if (isset($_POST['county']) && is_numeric($_POST['county']))	{

				$county		=	$_POST['county'];
				
		}	else	{
		
				$county		=	0;
				
		}
		
		
		
if (isset($_POST['place']) && $_POST['place'] != "")	{

				$place		=	$mysql->clean_string($_POST['place']);
				
		}	else	{
		
				$place		=	"";
				
		}
		
		
if (isset($_POST['postcode']) && $_POST['postcode'] != "")	{

				$postcode		=	$mysql->clean_string($_POST['postcode']);
				
		}	else	{
		
				$postcode		=	"";
				
		}

		
if (isset($_POST['directions']) && $_POST['directions'] != "")	{

				$directions		=	$mysql->clean_string($_POST['directions']);
				
		}	else	{
		
				$directions		=	"";
				
		}
		
		
if (isset($_POST['transport']) && $_POST['transport'] != "")	{

				$transport		=	$mysql->clean_string($_POST['transport']);
				
		}	else	{
		
				$transport		=	"";
				
		}
		
if (isset($_POST['price']) && $_POST['price'] != "")	{

				$price		=	$mysql->clean_string($_POST['price']);
				
		}	else	{
		
				$price		=	"";
				
		}
		
		
if (isset($_POST['phone']) && $_POST['phone'] != "")	{

				$phone		=	$mysql->clean_string($_POST['phone']);
				
		}	else	{
		
				$phone		=	"";
				
		}


if (isset($_POST['email']) && $_POST['email'] != "")	{

				$email		=	$mysql->clean_string($_POST['email']);
				
		}	else	{
		
				$email		=	"";
				
		}
		
		
if (isset($_POST['website']) && $_POST['website'] != "")	{

				$website		=	$mysql->clean_string($_POST['website']);
				
		}	else	{
		
				$website		=	"";
				
		}
		
		

$errors['archived'] 	=	"SchNEWS is archived. We are no longer accepting submissions";



//  If any errors were generated then go back to the form and show them
if (isset($errors))		{
	
			require "inc/form.php";
			exit;
		}		



		
//	SPAM CHECKING
/**
 * MOST OF THE SPAM ON THE FORM HAS THE SAME LINK FOR ALL LOWER FIELDS.
 * 
 * WE CAN SIMPLE CHECK FOR DUPLICATE ENTRY AND REJECT ANYTHING THAT APPEARS TO HAVE DUPLICATED VALUES.
 */

// PUT ALL THE FORM DATA INTO AN ARRAY.
$data 	=	array(
					$name,			
					$summary,
					$date_from,
					$date_to,
					$time,
					$place,
					$price,
					$phone,
					$email,
					$website,
					$directions,
					$transport,
					$postcode
					);

// REMOVE ANY THAT ARE EMPTY 
foreach ($data as $key => $value)	{
	
		if ($value == '') unset($data[$key]);	
	
}

// HOW MANY ARE LEFT...
$numData	=	count($data);					

// REMOVE ANY DUPLICATES...
$data		=	array_unique($data);

// HOW MANY ARE LEFT...
$newNumData	=	count($data);

// THE DIFFERENCE BETWEEN THE TWO COUNTS IS HOW MANY DUPLICATES THERE WERE...
$diffCount	=	$numData - $newNumData;

if ($diffCount > 3)	{
	
		echo "<div align=\"center\">Sorry, but your submission looked like spam to our system, so we can't add it to our database</div>";
		exit;	
	
}









if ($date_to == "") $date_to = "0000-00-00 00:00:00";






$sql		=		"SELECT id FROM pap WHERE name = '$name' AND summary = '$summary'";
$mysql->query( $sql );

if ($mysql->get_num_rows() != 0)	{

	$res	=	$mysql->get_row();
	$id		=	$res[ 'id' ];

}
else
{

$sql		=		"INSERT INTO pap (name, summary, date_from, date_to, time, place, price, phone, email, website, auth, added_on, directions, transport, county, postcode)	VALUES
					(	'$name', '$summary', '$date_from', '$date_to', '$time', '$place', '$price', '$phone', '$email', '$website', 0, '".DATETIME_make_sql_datetime()."', '$directions', '$transport', '$county', '$postcode' )";
$mysql->query( $sql );
$id			=		$mysql->get_insert_id();

$subject	=		"A New Party & Protest Events has been submitted through the SchNEWS.org.uk site.";
$body		=		"
A new Party & Protest event has been submitted to the SchNEWS website.

It was added on " . DATETIME_make_sql_datetime() . " from IP address ".$_SERVER['REMOTE_ADDR'].".

It is in the database and waiting to be authorised or rejected.

-----------

The submission is:



".str_replace("<br />", "\n",$mysql->clean_string(HOMEPAGE_show_pap($id, 0)))."


";

$mail	=	new email( $body, $subject, 'schtalk@lists.aktivix.org' );
$mail->send();

}

require "inc/thanks.php";

?>