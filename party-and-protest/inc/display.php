<!-- BEGIN P&P LIST -->
	<div class='PAP_title_graphic'>
		<img src='pap-banner-10.jpg' align='right' />
	</div>
	
	<div class='PAP_submit_link'>
		<a href='submit.php'>
			Click Here to Submit a Party or Protest
		</a>
	</div>
	
	<div class='PAP_text'>
		Going to a event? Want to look cool and make new friends? 
		
		Then why not hand out a bundle of SchNEWS! 
		
		Get in touch with the office a week or two before and we'll try to arrange to send you a pile. 
		<br /><br />
		Call the office on 01273 685913 or email us at <a href='mailto:mail@schnews.org.uk'>mail@schnews.org.uk</a>. Alternatively get SchNEWS emailed to you each week by PDF and photocopy yerself.
	</div>
	
	<?php
	
		echo HOMEPAGE_include_pap( true, true );
	
	?>
	
	
	<!-- END P&P LIST -->