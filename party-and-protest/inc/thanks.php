<P><strong><font size="4">Thank You</font></strong></P>
            <P><b>Your Party &amp; Protest submission has been accepted. </b></P>
            <P>Our editors will look at it and if they authorise your submission it will be shown on the SchNEWS Party &amp; Protest page.</P>
            <P>&nbsp;</P>
            <P>Back to <a href="../index.htm">SchNEWS Homepage</a> </P>
            <P>&nbsp;</P>
            <P>Your submission will appear as: </P>
            <P>&nbsp;</P>
            <?php echo HOMEPAGE_show_pap($id, 0); ?>