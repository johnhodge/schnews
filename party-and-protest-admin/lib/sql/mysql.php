<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 15-06-11
 *
 */



class mysql_connection
{
	protected $mysql_conn;			//	The mysql connection to the server
	protected $sql;					//	The most recent SQL query
	protected $rs;					//	The most recent query results


	/*
	 * Instantiates the object, connects to the DB and selects the database
	 */
	function __construct($server = false, $db_name = false, $username = false, $password = false)
	{
		//	Are we specifying server and login details or using the config defaults
		if (!$server)		$server 	=	MYSQL_SERVER_ADDRESS;
		if (!$db_name)		$db_name	=	MYSQL_DATABASE_NAME;
		if (!$username)		$username 	=	MYSQL_USERNAME;
		if (!$password)		$password 	=	MYSQL_PASSWORD;

		//	Connect to the mysql server
		if (!$this->mysql_conn = mysql_connect($server, $username, $password, true))
		{
			//	Do some error handling here to indicate a login failure
			return false;
		}

		//	Select the database
		if (!mysql_select_db($db_name, $this->mysql_conn))
		{
			debug ("Database selection failed", __FILE__, __LINE__);
			return false;
		}

		return true;

	}



	function __destruct()
	{
		mysql_close($this->mysql_conn);
		return;
	}



	/*
	 * executes the SQL query
	 */
	public function query($sql)
	{
		//	Test inputs
		if (!is_string($sql) || trim($sql) == '')
		{
			//	ERROR
			debug ('$SQL is either not a string or is empty', __FILE__, __LINE__);
			return false;
		}

		$this->sql		=	$sql;

		//	Run the query
		if (!$this->rs		=	mysql_query($this->sql, $this->mysql_conn))
		{
			//	ERROR
			debug (mysql_error(), __FILE__, __LINE__);
			return false;
		}

		return true;

	}



	public function clean_string($string)
	{

		//	Test inputs
		if (!is_string($string)) return $string;

		return mysql_real_escape_string($string, $this->mysql_conn);

	}



	function get_row()
	{
		//	Ensure that a query has been run
		if (!is_resource($this->rs))
		{
			//	ERROR
			debug ('$RS is not a mysql resource', __FILE__, __LINE__);
			return false;
		}

		if ($result = mysql_fetch_assoc($this->rs))
		{
			return $result;
		}
		else
		{
			return false;
		}
	}



	function get_num_rows()
	{
		if (!is_resource($this->rs)) return false;
		return mysql_num_rows($this->rs);
	}



	function get_insert_id()
	{

			return mysql_insert_id();

	}


}