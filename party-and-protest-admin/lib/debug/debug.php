<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 12-05-11
 *
 */



/*
 * Displays an debugging message to the screen, assuming the DEBUG flag is set in the main CONFIG.PHP file
 *
 * @param string $message		- The error message
 * @param string $file			- The file the message is related to (intended to be the output of the __FILE__ system var)
 * @param int $line				- The line in the file the debug is related to (intend to be the output of the __LINE__ system var)
 *
 * @return bool					- The success of the function
 */
function debug($message, $file = false, $line = false)
{
	//	Test inputs
	if (!is_string($message) || trim($message) == '') return false;

	//	Test whetehr debugging is turned on in the main config file
	if (!DEBUG) return false;

	//	Create string for the file and line vars, depending on whether they are set int he params
	$file_line 	=	'';
	if ($file)
	{
		$file_line 	.=	' :: <b>File</b> = '.$file;
	}
	if ($line)
	{
		$file_line 	.=	' :: <b>Line</b> = '.$line;
	}

	// Create an output buffer
	$output		=	array();
	$output[]	=	"<div class='".DEBUG_DIV_CLASS."'>";
	$output[]	=	"\t<span style='color:blue'><b>DEBUG::</b> ".htmlentities($message).$file_line.'</span>';
	$output[]	=	"</div>";

	// Flush output buffer and end function
	echo implode("\r\n", $output);
	return true;

}