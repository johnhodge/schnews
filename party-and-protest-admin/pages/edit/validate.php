<?php

$errors		=	array( );



/*
 * 	Name
 */
if ( isset( $_POST['name'] ) && !empty( $_POST['name'] ) )
{
	
	$name		=	$_POST['name'];
	
}
else
{
	
	$errors[]	=	"Name not set";
	
}



/*
 * 	Summary
 */
if ( isset( $_POST['summary'] ) && !empty( $_POST['summary'] ) )
{
	
	$summary		=	$_POST['summary'];
	
}
else
{
	
	$errors[]	=	"Summary not set";
	
}



/*
 * 	Date From
 */
if ( isset( $_POST['date_from'] ) && !empty( $_POST['date_from'] ) )
{
	
	if ( !$date_from 	=	DATE_turn_text_to_sql( $_POST['date_from'] ) )
	{
		
		$errors[]	=	"Date From not in correct format";
		
	}
	
}
else
{
	
	$errors[]	=	"Date From not set";
	
}



/*
 * 	Date To
 */
if ( isset( $_POST['date_to'] ) && !empty( $_POST['date_to'] ) )
{
	
	if ( !$date_to 	=	DATE_turn_text_to_sql( $_POST['date_to'] ) )
	{
		
		$errors[]	=	"Date To not in correct format";
		
	}
	
}
else
{
	
	$date_to	=	"0000-00-00 00:00:00";
	
}





/*
 * 	Time
 */
if ( isset( $_POST['time'] ) && !empty( $_POST['time'] ) )
{
	
	$time		=	$_POST['time'];
	
}
else
{
	
	$time		=	'';
	
}





/*
 * 	Place
 */
if ( isset( $_POST['place'] ) && !empty( $_POST['place'] ) )
{
	
	$place		=	$_POST['place'];
	
}
else
{
	
	$place		=	'';
	
}




/*
 * 	Postcode
 */
if ( isset( $_POST['postcode'] ) && !empty( $_POST['postcode'] ) )
{
	
	$postcode		=	$_POST['postcode'];
	
}
else
{
	
	$postcode		=	'';
	
}




/*
 * 	Phone
 */
if ( isset( $_POST['phone'] ) && !empty( $_POST['phone'] ) )
{
	
	$phone		=	$_POST['phone'];
	
}
else
{
	
	$phone		=	'';
	
}





/*
 * 	Email
 */
if ( isset( $_POST['email'] ) && !empty( $_POST['email'] ) )
{
	
	$email		=	$_POST['email'];
	
}
else
{
	
	$email		=	'';
	
}




/*
 * 	website
 */
if ( isset( $_POST['website'] ) && !empty( $_POST['website'] ) )
{
	
	$website		=	$_POST['website'];
	
}
else
{
	
	$website		=	'';
	
}




/*
 * 	Transport
 */
if ( isset( $_POST['transport'] ) && !empty( $_POST['transport'] ) )
{
	
	$transport		=	$_POST['transport'];
	
}
else
{
	
	$transport		=	'';
	
}
