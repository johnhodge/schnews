<?php


/*
 * 	Test inputs
 */
if ( 
		!isset( $_GET['edit'] ) 
		&&
		!isset( $_GET['authorise'] )
		&&
		!isset( $_GET['deauthorise'] ) 
	)
{
	
	require "pages/list_events/index.php";
	
}


if ( isset( $_GET['edit'] ) )			$id	=	$_GET['edit'];
if ( isset( $_GET['authorise'] ) )		$id	=	$_GET['authorise'];
if ( isset( $_GET['deauthorise'] ) ) 	$id	=	$_GET['deauthorise'];


if ( !isset( $id ) || !CORE_is_number( $id ) )
{

	require "pages/list_events/index.php";
	
}


/*
 * 	Make new object
 */
$event	=	new pap_event( $id );




if ( isset( $_POST['submit'] ) )
{
	
	require "pages/edit/validate.php";
	require "pages/edit/update.php";
	
}




/*
 * 	Draw edit form
 */
require "pages/edit/draw_form.php";