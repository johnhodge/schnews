<a href='?page=list_events'>Back to List</a>

<br /><br /><br />

<form method='POST' action='?page=edit&edit=<?php echo $event->getId(); ?>'>

	<div class='event_name'>
	
		Edit P&amp;P Event
		
		<?php 
		
			require "pages/edit/auth_link.php";
		
		?>
			
	</div>
	
	
	<table>
	
		<tr>
			<td class='form_label'>
				Name: 	
				<br />
				<i style='color:#999999'>* Required</i>			
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='name' value='<?php echo $event->getName(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Summary:
				<br />
				<i style='color:#999999'>* Required</i> 				
			</td>
			<td class='form_value'>
				<textarea class='form_input' style='height: 250px' name='summary'><?php echo $event->getSummary(); ?></textarea>			
			</td>
		</tr>	
	
		<tr>
			<td class='form_label'>
				Date From: (dd-mm-yyyy) 
				<br />
				<i style='color:#999999'>* Required</i>				
			</td>
			<td class='form_value'>
				<input type='text' name='date_from' value='<?php echo DATE_turn_sql_to_text ( $event->getDate_from() ); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Date To: (dd-mm-yyyy)
				<br />
				<i style='color:#999999'>( Leave blank if not-applicable )</i> 				
			</td>
			<td class='form_value'>
				<input type='text' name='date_to' value='<?php if ( $event->getDate_to() != '0000-00-00 00:00:00') echo DATE_turn_sql_to_text( $event->getDate_to() ); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Time: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='time' value='<?php echo $event->getTime(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Place: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='place' value='<?php echo $event->getPlace(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Postcode: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='postcode' value='<?php echo $event->getPostcode(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Phone: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='phone' value='<?php echo $event->getPhone(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Email: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='email' value='<?php echo $event->getEmail(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Website: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='website' value='<?php echo $event->getWebsite(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td class='form_label'>
				Transport: 				
			</td>
			<td class='form_value'>
				<input type='text' class='form_input' name='transport' value='<?php echo $event->getTransport(); ?>' />			
			</td>
		</tr>	
	
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<tr>
			<td></td>
			<td>
				<input type='submit' name='submit' value='Update Event' />			
			</td>
		</tr>
		
	</table>

</form>





