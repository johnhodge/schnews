<?php

debug ( "Update form started" );

if ( isset( $errors ) )
{
	
	if ( count( $errors ) > 1 )
	{
		
		foreach ( $errors as $error ) 
		{
			
			echo "<div style='font-0size: 14pt; font-weight: bold; color: red'>" . $error . "</div>";
			
			debug ( "ERRORS" );
			var_dump( $errors );
			
		}
		
	}
	else
	{	
	
		$event->setName( $name );
		$event->setSummary( $summary);
		$event->setDate_from( $date_from );
		$event->setDate_to( $date_to );
		$event->setTime( $time );
		$event->setPlace( $place );
		$event->setPostcode( $postcode );
		$event->setPhone( $phone );
		$event->setEmail( $email );
		$event->setWebsite( $website );
		$event->setTransport( $transport );
		$event->update_db();
		
		echo "<div style='font-0size: 14pt; font-weight: bold; color: blue'>Event Updated</div>";
		
		debug ( "Event '$name' Updated" );
	
	}
	
}