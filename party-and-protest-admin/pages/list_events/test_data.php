<?php


/*
 * 	Make sure there are some session vars set
 */
if ( !isset( $_SESSION['pap_auth'] ) )
{
	
	$_SESSION['pap_auth'] 	=	false;
	
}

if ( !isset( $_SESSION['pap_future'] ) )
{
	
	$_SESSION['pap_future'] 	=	true;
	
}



/*
 * Test for POST vars
 */
if ( isset( $_POST['auth'] ) )
{
	
	if ( $_POST['auth'] == 1 ) $_SESSION['pap_auth'] 	=	true;
	if ( $_POST['auth'] == 0 ) $_SESSION['pap_auth']		=	false;
	
}
if ( isset( $_POST['future'] ) )
{
	
	if ( $_POST['future'] == 1 ) $_SESSION['pap_future'] 	=	true;
	if ( $_POST['future'] == 0 ) $_SESSION['pap_future']		=	false;
	
}