<?php

class	pap_event
{
	
	protected $id;
	
	protected $name;
	protected $summary;
	protected $date_from;
	protected $date_to;
	
	protected $time;
	protected $place;
	protected $price;
	protected $phone;
	
	protected $transport;
	protected $county;
	protected $postcode;
	
	protected $website;
	protected $email;
	
	protected $auth;
	protected $auth_date;
	
	protected $added_on;
	
	function __construct( $id = false )
	{
		
		/*
		 * 	Test inputs
		 */
		if ( !CORE_is_number( $id ) ) $id = false;
		$this->id	=	$id;

		
		/*
		 * Populate data
		 */
		if ( $this->id )
		{
			
			debug ( "Getting Event" );
			$this->get_pap_event();
			
		}
		else 
		{
			
			debug ( "Making new event" );
			$this->make_new_event();
			
		}
		
		return true;
		
	}
	
	
		
	
	
	/*
	 * 	Gets the data for a give PAP event from DB
	 */
	protected function get_pap_event()
	{

		if ( !$this->id ) return false;
		
		
		/*
		 * Query DB
		 */
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT *
						FROM pap
						WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		
		/*
		 * There should be only one result
		 */
		if ( $mysql->get_num_rows() != 1 ) return false;

		
		
		/*
		 * Populate local data
		 */
		$res	=	$mysql->get_row();
		
		$this->name			=	$res['name'];
		$this->summary		=	$res['summary'];
		$this->date_from	=	$res['date_from'];
		$this->date_to		=	$res['date_to'];
		$this->time			=	$res['time'];
		$this->place		=	$res['place'];
		$this->price		=	$res['price'];
		$this->phone		=	$res['phone'];
		$this->transport	=	$res['transport'];
		$this->county		=	$res['county'];
		$this->postcode		=	$res['postcode'];
		$this->website		=	$res['website'];
		$this->email		=	$res['email'];
		$this->auth			=	$res['auth'];
		$this->auth_date	=	$res['auth_date'];
		$this->added_on		=	$res['added_on'];
		
		return true;
		
	}
	
		
	
	
	
	
	/*
	 *	Fills the local data with empty data 
	 */
	protected function make_new_event()
	{

		
		if ( $this->id ) return false;
		
		
		
		/*
		 * Populate local data
		 */
		$this->name			=	'';
		$this->summary		=	'';
		$this->date_from	=	'';
		$this->date_to		=	'';
		$this->time			=	'';
		$this->place		=	'';
		$this->price		=	'';
		$this->phone		=	'';
		$this->transport	=	'';
		$this->county		=	'';
		$this->postcode		=	'';
		$this->website		=	'';
		$this->email		=	'';
		$this->auth			=	0;
		$this->auth_date	=	'0000-00-00 00:00:00';
		$this->added_on		=	'0000-00-00 00:00:00';
		
		return true;
		
	}

	
	
	
	
	
	/*
	 * Public wrapper for DB updates
	 */
	public function update_db()
	{

		debug ( "this->update_db function called" );
	
		if ( $this->id )
		{
			
			$this->update_in_db();
			
		}
		else
		{
			
			$this->add_to_db();
			
		}
		
		
	}
	
	
	
	
	
	
	/*
	 * Updates an existing story in the DB
	 */
	protected function update_in_db()
	{
		
		debug ( "this->update_in_db function called" );
		
		if ( !$this->id ) return false;
		
		
		
		$mysql	=	new mysql_connection();
		
		
		/*
		 * 	General SQL String
		 */
		$fields		=	$this->get_fields();
		$values		=	array();
		foreach ( $fields as $field )
		{
			
			$values[]		=	" $field = '" . $mysql->clean_string( $this->$field ) . "'";
			
		}
		$values		=	implode( ", ", $values );
		$sql		=	" 	UPDATE pap
							SET
								$values
							WHERE 
								id = " . $this->id;
		$mysql->query( $sql );						
		
		debug ( "Update SQL: " . $sql );
		
		return true;
		
	}
	
	
	
	
	
	
	/*
	 * 	Add the event to the DB and updates the local ID
	 */
	protected function add_to_database()
	{
		
		debug ( "this->add_to_database function called" );
		
		if ( $this->id ) return false;
		
		
		$mysql	=	new mysql_connection();
		
		/*
		 *	Generate SQL string
		 */
		$fields			=	$this->get_fields();
		$keys			=	array();
		$values			=	array();
		foreach ( $fields as $field )
		{
			
			$keys[]		=	$field;
			$values[]	=	"'" . $mysql->clean_string( $field ) . "'";	
			
		}
		$keys	=	implode( ", ", $keys );
		$values	=	implode( ", ", $values );
		
		$sql	=	"	INSERT INTO pap
						(
							$keys
						)
						VALUES
						(
							$values
						)";
		
		
			
		/*
		 * 	Update the Database and grab the new ID
		 */
		$mysql->query( $sql );
		
		if ( !$this->id = $mysql->get_insert_id() )
		{
			
			return false;
			
		}
		
		return true;
		
	} 
	
	
	
	
	
	
	
	protected function get_fields()
	{
		
		$fields		=	array(	
		
				'name',
				'summary',
				'date_from',
				'date_to',
				'time',
				'place',
				'price',
				'phone',
				'transport',
				'county',
				'postcode',
				'website',
				'email',
				'auth',
				'auth_date',
				'added_on'
		
		);
		
		return $fields;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * 	==============================================================
	 * 
	 * 			GETTERS AND SETTERS
	 */
	
	
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return the $summary
	 */
	public function getSummary() {
		return $this->summary;
	}

	/**
	 * @return the $date_from
	 */
	public function getDate_from() {
		return $this->date_from;
	}

	/**
	 * @return the $date_to
	 */
	public function getDate_to() {
		return $this->date_to;
	}

	/**
	 * @return the $time
	 */
	public function getTime() {
		return $this->time;
	}

	/**
	 * @return the $place
	 */
	public function getPlace() {
		return $this->place;
	}

	/**
	 * @return the $price
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @return the $phone
	 */
	public function getPhone() {
		return $this->phone;
	}

	/**
	 * @return the $transport
	 */
	public function getTransport() {
		return $this->transport;
	}

	/**
	 * @return the $county
	 */
	public function getCounty() {
		return $this->county;
	}

	/**
	 * @return the $postcode
	 */
	public function getPostcode() {
		return $this->postcode;
	}

	/**
	 * @return the $website
	 */
	public function getWebsite() {
		return $this->website;
	}

	/**
	 * @return the $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return the $auth
	 */
	public function getAuth() {
		return $this->auth;
	}

	/**
	 * @return the $auth_date
	 */
	public function getAuth_date() {
		return $this->auth_date;
	}

	/**
	 * @return the $added_on
	 */
	public function getAdded_on() {
		return $this->added_on;
	}

	/**
	 * @param $name the $name to set
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * @param $summary the $summary to set
	 */
	public function setSummary($summary) {
		$this->summary = $summary;
	}

	/**
	 * @param $date_from the $date_from to set
	 */
	public function setDate_from($date_from) {
		$this->date_from = $date_from;
	}

	/**
	 * @param $date_to the $date_to to set
	 */
	public function setDate_to($date_to) {
		$this->date_to = $date_to;
	}

	/**
	 * @param $time the $time to set
	 */
	public function setTime($time) {
		$this->time = $time;
	}

	/**
	 * @param $place the $place to set
	 */
	public function setPlace($place) {
		$this->place = $place;
	}

	/**
	 * @param $price the $price to set
	 */
	public function setPrice($price) {
		$this->price = $price;
	}

	/**
	 * @param $phone the $phone to set
	 */
	public function setPhone($phone) {
		$this->phone = $phone;
	}

	/**
	 * @param $transport the $transport to set
	 */
	public function setTransport($transport) {
		$this->transport = $transport;
	}

	/**
	 * @param $county the $county to set
	 */
	public function setCounty($county) {
		$this->county = $county;
	}

	/**
	 * @param $postcode the $postcode to set
	 */
	public function setPostcode($postcode) {
		$this->postcode = $postcode;
	}

	/**
	 * @param $website the $website to set
	 */
	public function setWebsite($website) {
		$this->website = $website;
	}

	/**
	 * @param $email the $email to set
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * @param $auth the $auth to set
	 */
	public function setAuth($auth) {
		$this->auth = $auth;
	}

	/**
	 * @param $auth_date the $auth_date to set
	 */
	public function setAuth_date($auth_date) {
		$this->auth_date = $auth_date;
	}

	/**
	 * @param $added_on the $added_on to set
	 */
	public function setAdded_on($added_on) {
		$this->added_on = $added_on;
	}

	
	
	
	
	
	
	
	
	
	
}



