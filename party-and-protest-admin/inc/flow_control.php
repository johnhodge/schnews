<?php


/*
 * 	Make sure a SESSION var exists
 */
if ( !isset( $_SESSION['pap_page'] ) )
{
	
	$_SESSION['pap_page'] 	=	"list";
	
}



/*
 * 	Test GET vars
 */
if ( isset( $_GET['page'] ) )
{
	
	$_SESSION['pap_page'] 	=	$_GET['page'];
	
}

if ( isset ( $_GET['authorise'] ) )
{
	
	$_SESSION['pap_page']	=	'authorise';
	
}

if ( isset( $_GET['deauthorise'] ) )
{
	
	$_SESSION['pap_page'] 	=	'deauthorise';
	
}





switch ( $_SESSION['pap_page'] )
{
	
	default:
		require "pages/list_events/index.php";
		break;
	
	case 'edit':
		require "pages/edit/index.php";
		break;
		
	case 'authorise':
		require "pages/authorise/index.php";
		break;
		
	case 'deauthorise':
		require "pages/deauthorise/index.php";
		break;
	
}