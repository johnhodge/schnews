<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> -->
<html>
	<head>
		<title>P&P Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<link rel="stylesheet" type="text/css" href="css/display.css" />
	</head>
	
	
<body>

<?php

if ( !isset( $_SESSION['pap_logged_in'] ) )
	{
		
		$_SESSION['pap_logged_in'] = false;
		
	}
	
if ( isset( $_GET['logout'] ) ) 
	{
		
		$_SESSION['pap_logged_in'] 	=	false;
		
	}


if ( $_SESSION['pap_logged_in'] )
	{
	
	?>
		
		<div style='text-align: right'>
			<a href='?logout=1'>Logout</a>
		</div>

	<?php 
	
	}

?>

<h1 style='border-bottom: 2px dotted #999999; padding:15px; margin: 15px'>
Party &amp; Protest Admin
</h1>

