<?php


function DISPLAY_show_pap( $id )
{
	
	/*
	 * 	Test input
	 */
	if ( !CORE_is_number( $id ) ) return false;
	
	
	$event		=	new pap_event( $id );
	
	
	$auth		=	"<div style='text-align: right; color: red'>Not Authorised</div>";
	if ( $event->getAuth() )
	{
		
		$auth		=	"<div style='text-align: right; color: blue'>Authorised</div>";
		
	}
	
	
	$output		=	array();
	$output[]	=	"<div class='event_container'>";
	$output[]	=	$auth;
	$output[]	=	"<div class='event_name'>" . $event->getName() . "</div>";
	$output[]	=	"<div class='event_summary'>" . nl2br( strip_tags( $event->getSummary() ) ) . "</div>";
	$output[]	=	"<div class='event_data'><b>Date: </b>" . DISPLAY_make_dates( $event ) . "</div>";
	$output[]	=	"<div class='event_data'><b>Time: </b>" . $event->getTime() . "</div>";
	$output[]	=	"<div class='event_data'><b>Place: </b>" . $event->getPlace() . "</div>";
	$output[]	=	"<div class='event_data'><b>Postcode: </b>" . $event->getPostcode() . "</div>";
	$output[]	=	"<div class='event_data'><b>Price: </b>" . $event->getPrice() . "</div>";
	$output[]	=	"<div class='event_data'><b>Phone: </b>" . $event->getPhone() . "</div>";
	$output[]	=	"<div class='event_data'><b>Email: </b>" . $event->getEmail() . "</div>";
	$output[]	=	"<div class='event_data'><b>Website: </b>" . $event->getWebsite() . "</div>";
	$output[]	=	"<div class='event_data'><b>Transport: </b>" . $event->getTransport() . "</div>";
	
	
	
	
	$output[]	=	"</div>";
	
	return implode( "\r\n", $output );	
	
}



function DISPLAY_make_dates( $event )
{
	
	$date_from		=	new date_time( $event->getDate_from() );
	if ( $event->getDate_to() != '0000-00-00 00:00:00' )
	{
		
		$date_to		=	new date_time( $event->getDate_to() );
		
	}

	$output		=	$date_from->make_human_date();
	if ( isset( $date_to ) ) 
	{
		
		$ouptut	.=	" to " . $date_to->make_human_date();	
		
	}
	
	return $output;
	
}