<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 29-06-11
 *
 */


/*
 * 	This file contains fucntions that replace and/or superceed the built in PHP core functions
 */



/*
 * 	Determine whether the given input is a pure number.
 *
 * 	Will return false if any digit of the input var is not a 0-9 or a .
 *
 * 	The built in is_int() never seems to work properly, and also this function doesn't check type
 *
 * 	@param string/int number
 * 	@param bool clean				//	If true the input will be clean of any non-numerics, if false it will just be evaluated
 * 	@return bool
 */
function CORE_is_number($number, $clean = false)
{

	//	Test inputs
	if ( ( !is_string( $number ) && !is_numeric( $number ) ) || trim( $number ) == '' ) return false;
	if (!is_bool($clean)) $clean = false;

	$number		.=	"*";
	$number		=	str_replace( "*", '', $number );
	
	//	Any chars in the input that is not one of these will be rejected
	$allowed_chars		=	array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.');
	if ($clean)		$output		=	'';
	for ( $n = 0 ; $n < strlen($number) ; $n++ )
	{
		if (in_array($number{$n}, $allowed_chars) && $clean)
		{

			$output		.=		$number{$n};

		}
		if ( !in_array( $number{$n}, $allowed_chars ) && !$clean)
		{

			debug("is_number failed for input '$number'. Failed digit = " . $number{$n}, __FILE__, __LINE__);
			return false;

		}
	}

	if ($clean)
	{

		return $output;

	}
	else
	{

		return true;

	}


}



