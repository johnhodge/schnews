IMPORTANT

Make sure that the contents of this 'conf' directory are not publicly accessible and are not served up by the webserver as part of your web application.

These files will contain username/password combos, plus other site specific info that will present security risks if accessible to the general public.