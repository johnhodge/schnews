<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-08-11
 *
 */



/*
 * This file contains very general configuration settings.
 * More specific settings can be found in the various sub-config files.
 */



/*
 *  What is the location of this file?
 *
 *  In most cases this can be left as dirname(__FILE__).
 *  The only time I can imagine this needing to be changed is when the config files reside on a different server to the main software that is using this library
 */
define ('LIB_CONF_ROOT', dirname(__FILE__));



/*
 * How does the site identify itself. Useful for logs and email notices
 */
define ("SITE_NAME", 'SchNEWS StoryAdmin');



/*
 * 	Include other sub-system config files
 */
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . 'sql' . DIRECTORY_SEPARATOR . 'mysql_conf.php';
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'log_conf.php';
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'email_conf.php';
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . 'debug' . DIRECTORY_SEPARATOR . 'debug_conf.php';
