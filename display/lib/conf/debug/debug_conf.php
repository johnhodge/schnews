<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-08-11
 *
 */



/*
 * Settings related to on-screen debugging.
 *
 * This is not the same as logging (see log/log_conf.php), this controls debugging notices shown on screen
 */


/*
 * Enable onscreen debugging? Should the debug() function display to screen or not.
 *
 * This should absolutely, 100% be set to false for production use. Debugging should only be used on development servers while software is being written.
 */
define ('DEBUG', 0);



/*
 * Which CSS style should the debug() function use. If left blank then debug() notices will just use the default text style.
 */
define ('DEBUG_DIV_CLASS', 'debug_div');