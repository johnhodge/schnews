<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 14-08-11
 *
 */


function DATETIME_make_sql_datetime($unix_timestamp = false)
{

	if (!is_numeric($unix_timestamp)) $unix_timestamp = false;

	if ($unix_timestamp)
	{
		return date("Y-m-d H:i:s", $unix_timestamp);
	}
	else
	{
		return date("Y-m-d H:i:s");
	}

}
