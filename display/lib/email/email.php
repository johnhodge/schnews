<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-08-11
 *
 */



class email
{

	protected	$message;
	protected 	$subject;
	protected	$to;
	protected	$from;

	function __construct( $message, $subject, $to, $from = false )
	{

		//	Test inputs
		if ( !is_string( $message ) || trim( $message ) == '' )		return false;
		if ( !is_string( $subject ) || trim( $subject ) == '' )		return false;
		if ( !URL_is_email( $to ) ) return false;
		if ( !is_string( $from ) || trim( $from ) == '' ) $from = EMAIL_DEFAULT_FROM;

		$this->message 	=	$message;
		$this->subject	=	$subject;
		$this->to		=	$to;
		$this->from		=	$from;

		return true;

	}



	public function send()
	{

		$headers		=	array();
		$headers[]		=	'X-Mailer: PHP/' . phpversion();
		if ( $this->from )
		{

			$headers[]		=	'From: ' . $this->from;
			$headers[]		=	'Reply-To: ' . $this->from;

		}
		$headers		=	implode( "\r\n", $headers );

		mail( $this->to, $this->subject, $this->message, $headers );

	}

}