aw_lib PHP mini framework

(c) Andrew Winterbottom 2011

@auth Andrew Winterbottom
@support support@andrewwinterbottom.com
@license GPL v2
@last_mod 16-06-11




===============
  How to Use
===============


Main classes:


mysql_connection($server, $db_name, $username, $password);
	Establishes a connection to the mysql server and select a database.
	Can either take the 4 params or can accept default from the /conf/sql/mysql.php file

log_entry($log_file_path);
	Allows adding new log entries, aswell as email the specified admin address
	Can either take a param pointing to the desired log file or uses the log file path specified in /conf/log/conf.php

date_time($timestamp)
	Stores and processes a unix timestamp, and converts it into various other formats, as well as manipulating it in various ways
	$timestamp param can be either 	1) emtpy (in which case the current time will be used)
					2) a Unix timestamp
					3) an SQL datetime