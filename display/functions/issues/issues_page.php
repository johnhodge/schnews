<?php



function ISSUES_get_stories_for_issue( $id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $id ) ) return false;
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id, headline, summary
					FROM stories
					WHERE 	issue = $id
					AND		live = 1
					AND		deleted <> 1
					AND 	hide <> 1
					ORDER BY date_live DESC ";
	$mysql->query( $sql );	
	
	$output		=	array();
	while ( $res	=	$mysql->get_row() )
	{

		$output[]	=	"<div class='PAGES_issue_story_headline'>";
		$output[]	=	"<a href='" . WEBSITE_URL . STORIES_make_story_link( $res['headline'] ) ."'>" . $res['headline'] . "</a>";
		$output[]	=	"</div>";
		$output[]	=	"<div class='PAGES_issue_story_summary'>" . strip_tags( $res['summary'] ) . "</div>";
		
	}
	
	echo implode( "\r\n", $output );
	
}




function ISSUES_does_issue_pdf_exist( $issue_num )
{

	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $issue_num ) ) return false;
	
	$path	=	WEBSITE_ROOT . "issues/pdfs/" . $issue_num . ".pdf";
	if ( file_exists( $path ) )
	{
		
		return true;
		
	}
	else
	{

		return false;
		
	}
	
}




function ISSUES_make_pdf_link( $issue_num )
{

	if ( ISSUES_does_issue_pdf_exist( $issue_num ) )
	{
		
		return "<a href='" . WEBSITE_URL . "issues/pdfs/" . $issue_num . ".pdf'><img src='" . WEBSITE_URL . "images/system/pdf.jpg' />Download PDF</a>";
		
	}
	else
	{
	
		return "PDF Not Available";		
		
	}
}