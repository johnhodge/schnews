<?php
function STORIES_make_story_link( $link )
{
	
	/*
	 * 	Test inputs
	 */	
	if( !is_string( $link ) || empty( $link ) ) return false;
	
	
	
	/*
	 * 	Change spaces to hyphens
	 */
	$hyphens	=	array( ' ', '_');
	$link		=	str_replace( $hyphens, '-', $link );
	
	
	
	/*
	 * 	Remove quote marks
	 */
	$removes	=	array( '"', "'", ':', ';', '<', '>', '?' );
	$link		=	str_replace( $removes, '', $link );
	
	

	/*
	 * 	Replace ampersands
	 */
	$link		=	str_replace( "&", "and", $link );
	
	
	
	/*
	 * Create full link
	 */
	$link	=	"stories/" . $link;
	
	
	
	return $link;
	
}
