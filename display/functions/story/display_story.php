<?php


function STORIES_display_story( $id )
{
	
	/*
	 * 	Test Inputs
	 */
	if ( !CORE_is_number( $id ) ) 
	{

		//	DO SOME SERIOUS DEBUGGING HERE
		return false;
		
	}
	
	
	
	$story	=	new story( $id );
	
	
	/*
	 * Test and make graphic
	 */
	$graphic 		=	'';
	if ( $story->get_graphic_lg() != '' )
	{
		
		if ( file_exists( WEBSITE_ROOT . "images/" . $story->get_graphic_lg() ) )
		{
			$link_one	=	'';	$link_two	=	'';
			if ( $story->get_graphic_vlg() && file_exists( WEBSITE_ROOT . "images/" . $story->get_graphic_vlg() ) )
			{
				
				$link_one	=	"<a href='" . WEBSITE_URL . "images/" . $story->get_graphic_vlg() . "' target='_BLANK'>";
				$link_two	=	"</a>";
				
			}
			
			$graphic	=	"<div class='STORIES_story_graphic'>";
			$graphic	.=	$link_one;
			$graphic	.=	"<img src='" . WEBSITE_URL . "images/" . $story->get_graphic_lg() . "' />";
			$graphic	.=	$link_two;
			$graphic	.=	"</div>";
					
		}
		
	}
	
	?>
	
	
	<!-- BEGIN STORY DISPLAY -->
	<div class='STORIES_story_container'>
	
		<div class='STORIES_story_info'>
		
			<?php 
			
				$date	=	new date_time( $story->get_date_live() );
				echo "Published on <b>" . $date->make_human_date() . "</b> | ";
				
				echo "Part of <b>Issue " . STORIES_get_issue_number( $story->get_issue() ) . '</b> | ';
				
				echo "<a href='../print/?id=" . PRINT_encode_id( $story->get_id() ) . "' target='_BLANK'>Print Friendly Version</a>";
			
			?>
		
		</div>
		
		<?php 
			
			echo $graphic;
		
		?>
	
		<h2 class='STORIES_story_title'>
		
			<?php echo htmlentities( $story->get_headline() ); ?>
		
		</h2>
		
		<?php
		if ( $story->get_subheadline() != '' )
		{
		
		?>
		
		<h1 class='STORIES_story_subheadline' style='text-align: left'>
			<?php echo $story->get_subheadline(); ?>
		</h1>
		
		<?php
		
		}
		
		?>
		
		<div class='STORIES_story_content'>
		
			<?php echo $story->get_content();?>
		
		</div>
		
		
		<?php 
		
			echo STORIES_get_additional_graphics( $story );
		
		?>
		
		
		
		<div class='STORIES_story_keywords'>
		
			<?php 
				echo "<b>Keywords:</b> " . KEYWORDS_get_keywords_links( $story->get_id() );
			?>
		
		</div>
		
		
			
		
		<div class='STORIES_social_media_links'>
		
			<?php 
				echo "\r\n\r\n\r\n<table width='100%' align='center'><tr>";
				/*
				 * 	GOOGLE+ REMOVED COS NO-ONE USES IT!
				 * 
				 * echo "\r\n\r\n<td valign='right'>\r\n" . STORIES_get_google_link( $story ) . "\r\n</td>";
				 */ 
				echo "\r\n\r\n<td width='50%' valign='right'>\r\n" . STORIES_get_facebook_button( $story ) . "\r\n</td>";
				echo "\r\n\r\n<td width='50%' valign='left'>\r\n" . STORIES_get_twitter_button( $story ) . "\r\n</td>";
				echo "\r\n\r\n\r\n</tr></table>";
			
			?>
		
		</div>
	
	
	
		<div class='STORIES_previous_next_links'>
		
			<?php 
			
				echo "\r\n\r\n<table width='100%'>";
				echo "\r\n<tr>";
				
				echo "\r\n\r\n<td width='50%' align='left'>";
				echo STORIES_make_previous_next_link( $story, 'p' );
				echo "\r\n</td>";
			
				echo "\r\n\r\n<td width='50%' align='right'>";
				echo STORIES_make_previous_next_link( $story, 'n' );
				echo "\r\n</td>";
			
				echo "\r\n</tr>";
				echo "\r\n</table>\r\n\r\n";
				
			?>	
		
		</div>
	
	
		<?php 
		/*
		 * 	Do comments forsm and processing
		 */
		$_SESSION['comments_page_hash_' . $story->get_id() ]	=	STORIES_make_comment_hash( $story );
		echo COMMENTS_add_comment( $story->get_id() );
		echo COMMENTS_list_comments( $story->get_id() );
		COMMENTS_draw_comments_form( );
		
		?>
		
	</div>
	<!-- END STORY DISPLAY -->

	<?php 

	STORIES_incrememnt_hit_count( $story->get_id() );
	
	return;
		
}






function STORIES_get_issue_number( $id )
{
	
	/*
	 * 	Test Inputs
	 */
	if ( !CORE_is_number( $id ) ) 
	{

		//	DO SOME SERIOUS DEBUGGING HERE
		return false;
		
	}
	
	
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT number 
						FROM issues
						WHERE id = " . $id;
	$mysql->query( $sql );
	
	
	if ( $mysql->get_num_rows() != 1 )
	{

		//	DO SOME SERIOUS DEBUGGING HERE
		return false;
		
	}
	
	$res		=	$mysql->get_row();
	return $res['number'];
	
}



function STORIES_incrememnt_hit_count( $id )
{

	/*
	 * Test inputs
	 */
	if ( !CORE_is_number( $id ) ) return;
	
	
	
	$mysql	=	new mysql_connection();
	$sql	=	"	UPDATE stories
					SET
						hits = hits + 1
					WHERE id = $id ";
	$mysql->query( $sql );
	
	return true;
	
}




function STORIES_get_twitter_button( $story )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !is_object( $story) ) return false;
	
	
	$button 	=	file_get_contents( DISPLAY_SITE_ROOT . 'inc/stories/twitter_link' );
	
	$link		=	"http://www.schnews.org.uk/" . STORIES_make_story_link( $story->get_headline() );
	$headline	=	$story->get_headline();
	
	$button		=	str_replace( "***TITLE***", $headline, $button );
	$button		=	str_replace( "***LINK***", $link, $button );
	
	return $button;
	
}





function STORIES_get_facebook_button( $story )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !is_object( $story) ) return false;
	
	
	$button 	=	file_get_contents( DISPLAY_SITE_ROOT . 'inc/stories/facebook' );
	
	$link		=	"http://www.schnews.org.uk/" . STORIES_make_story_link( $story->get_headline() );
	
	$button		=	str_replace( "***LINK***", $link, $button );
	
	return $button;
	
}



function STORIES_get_google_link( $story )
{
	
	$button 	=	file_get_contents( DISPLAY_SITE_ROOT . 'inc/stories/google' );
	
	return $button;
	
}




function STORIES_make_comment_hash( $story )
{
	
	$headline	=	$story->get_headline();
	$date		=	$story->get_date_added();
	$id			=	$story->get_id();
	$salt		=	'uefuiohsdufhsuiof';
	
	return md5( $headline . $date . $id . $salt );
	
}



function STORIES_make_previous_next_link( $story, $type = 'p' )
{
	
	//	Test inputs
	if ( !is_object( $story ) ) return false;
	if ( $type != 'p' && $type != 'n' ) $type = 'p';

	
	//	Get a list of all the ids in homepage order.
	$ids 	=	HOMEPAGE_get_story_ids();
	
	$key	=	array_search( $story->get_id(), $ids );
	if( $key === false )
	{
		
		
		debug( "Story not found in homepage id list", __FILE__, __LINE__ );
		return '';
		
	}
	
	if ( $type == 'p' && isset( $ids[ $key + 1 ] ) )
	{
		
		$new_story	=	new story( $ids[ $key + 1 ] );
		$text		=	"<span style='font-size: 8pt; font-weight: bold; color: #888888'><< Previous Story</span><br /><b>";
		$text		.=	strtoupper( $new_story->get_headline() ) . "</b>";
		
		$link		=	STORIES_make_story_link( $new_story->get_headline() );
		
		$text		=	"<a href='../../$link'>" . $text . "</a>";
		
		return $text;
		
	}
	if ($type == 'n' && isset( $ids[ $key - 1 ] ) )
	{
		
		$new_story	=	new story( $ids[ $key - 1 ] );
		$text		=	"<span style='font-size: 8pt; font-weight: bold; color: #888888'>Next Story >></span><br />";
		$text		.=	"<b>" . strtoupper( $new_story->get_headline() ) . "</b>";
		
		$link		=	STORIES_make_story_link( $new_story->get_headline() );
		
		$text		=	"<a href='../../$link'>" . $text . "</a>";
		
		return $text;
		
	}
	
}




function STORIES_get_additional_graphics( $story )
{
	
	//	Test inputs
	if ( !is_object( $story ) ) return false;	
	
	$output	=	'';
	
	if ( $story->get_additional_graphic_1() != '' )
	{
	
		$output	.=	"\r\n\r\n\t<img class='STORIES_additional_graphic' src='" . WEBSITE_URL . "images/" . $story->get_additional_graphic_1() . "' />";
		
		if ( $story->get_additional_graphic_caption_1() != '' )
		{

			$output	.=	"\r\n<div class='STORIES_additional_graphic_caption'>" . $story->get_additional_graphic_caption_1() . "</div>";
		}
		
	}
	if ( $story->get_additional_graphic_2() != '' )
	{
	
		$output	.=	"\r\n\r\n\t<img class='STORIES_additional_graphic' src='" . WEBSITE_URL . "images/" . $story->get_additional_graphic_2() . "' />";
		
			
		if ( $story->get_additional_graphic_caption_2() != '' )
		{

			$output	.=	"\r\n<div class='STORIES_additional_graphic_caption'>" . $story->get_additional_graphic_caption_2() . "</div>";
		}
		
	}
	if ( $story->get_additional_graphic_3() != '' )
	{
	
		$output	.=	"\r\n\r\n\t<img class='STORIES_additional_graphic' src='" . WEBSITE_URL . "images/" . $story->get_additional_graphic_3() . "' />";
		
			
		if ( $story->get_additional_graphic_caption_3() != '' )
		{

			$output	.=	"\r\n<div class='STORIES_additional_graphic_caption'>" . $story->get_additional_graphic_caption_3() . "</div>";
		}
		
	}
	if ( $story->get_additional_graphic_4() != '' )
	{
	
		$output	.=	"\r\n\r\n\t<img class='STORIES_additional_graphic' src='" . WEBSITE_URL . "images/" . $story->get_additional_graphic_4() . "' />";
		
			
		if ( $story->get_additional_graphic_caption_4() != '' )
		{

			$output	.=	"\r\n<div class='STORIES_additional_graphic_caption'>" . $story->get_additional_graphic_caption_4() . "</div>";
		}
		
	}
	
	$output		=	"\r\n\r\n\r\n<div class='STORIES_additional_graphics'>" . $output . "\r\n\r\n</div>";
	
	return $output;
	
	
}