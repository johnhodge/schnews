<?php


/*
 * 	Finds the IDs for the keywords associated with a given story.
 * 
 * 	Returns the IDs as a non-indexed array, sorted in alphabetical order by
 * 	keyword name.
 * 
 * 	@param $id int
 * 	@return array
 */
function KEYWORDS_get_ids_for_story( $id )
{
	
	/*
	 *	Test inputs	 
	 */
	if ( !CORE_is_number( $id ) ) return false;
	
	
	
	/*
	 * 	Get data from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT keyword_mappings.keyword	
					FROM keyword_mappings
					LEFT JOIN keywords
						ON keywords.id = keyword_mappings.keyword
					WHERE 
						keyword_mappings.story =	$id
					ORDER BY keywords.keyword ASC	";
	$mysql->query( $sql );

	if ( $mysql->get_num_rows() == 0 )
	{
		
		return array();
		
	}

	
	
	/*
	 * 	Make an array of the results
	 */
	$output		=	array();
	while ( $res	=	$mysql->get_row() )
	{

		$output[]	=	$res['keyword'];
		
	}
	
	return $output;
	
}





/*
 * 	Generate an array of the keyword names, in alphabetical order
 * 
 * @param $id int
 * @return array
 */
function KEYWORDS_get_names_for_story( $id )
{

	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $id ) ) return false;
	
	

	$cache		=	KEYWORDS_get_keyword_cache();
	$ids		=	KEYWORDS_get_ids_for_story( $id );
	
	
	/*
	 * 	Build output array
	 */
	$output		=	array();
	foreach ( $ids as $id )
	{
		
		$output[]	=	$cache[ $id ];
		
	}
	
	return $output;
	
}





function KEYWORDS_get_keywords_csv( $id )
{
	
	/*
	 * Test inputs
	 */
	if ( !CORE_is_number( $id ) ) return false;
	
	$keywords	=	KEYWORDS_get_names_for_story( $id );
	
	return implode( ", ", $keywords );
	
}

function KEYWORDS_get_keywords_links( $id )
{
	
	/*
	 * Test inputs
	 */
	if ( !CORE_is_number( $id ) ) return false;
	
	$cache		=	KEYWORDS_get_keyword_cache();
	$ids		=	KEYWORDS_get_ids_for_story( $id );
	
	$output		=	array();
	foreach ( $ids as $id )
	{
		
		$output[]	=	"<a href='" . WEBSITE_URL . "keywords/?id=$id'>" . $cache[ $id ] . "</a>";
		
	}
	
	return implode( ", ", $output );
	
}




/*
 * 	Generate an associative array of all the keywords
 * 
 * 	@return array
 */
function KEYWORDS_get_keyword_cache()
{
	
	/*
	 * 	Grab from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT * 
					FROM keywords
					ORDER BY keyword ASC ";
	$mysql->query( $sql );

	
	
	/*
	 * 	Build array
	 */
	$output		=	array();
	while ( $res	=	$mysql->get_row() )
	{

		$output[ $res['id'] ] 	=	$res['keyword'];
		
	}
	
	return $output;
	
}





function KEYWORDS_get_name( $id )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $id ) )
	{
		
		return false;
		
	}
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT keyword
					FROM keywords
					WHERE id = $id ";
	$mysql->query( $sql );
	
	if ( $mysql->get_num_rows() != 1 )
	{
		
		return false;
		
	}
	
	$res	=	$mysql->get_row();
	return $res['keyword'];	
	
}





function KEYWORDS_get_story_ids_for_keyword( $id )
{
	
	/*
	 * 	Test Inputs
	 */
	if ( !CORE_is_number( $id ) ) 
	{
		
		//	DO SOME DEGUBBING
		return false;
		
	}
	
	
	/*
	 * 	Grab from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT keyword_mappings.story
					FROM keyword_mappings
					
					LEFT JOIN stories
					ON stories.id = keyword_mappings.story					
					
					WHERE 	keyword_mappings.keyword = $id 
					AND		stories.live = 1
					AND 	stories.deleted <> 1 ";
	$mysql->query( $sql );
	
	$output	=	array();
	while ( $res	=	$mysql->get_row() )
	{
		
		$output[]	=	$res['story'];
		
	}
	
	return $output;	
	
}





function KEYWORDS_list_formatted_stories( $id )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $id ) )
	{
		
		return false;
		
	}
	
	$keyword	=	KEYWORDS_get_name( $id );
	$numbers	=	KEYWORDS_get_number_of_stories();
	
	$output		=	array();

	$output[]	=	"<!-- BEGIN LIST OF STORIES FOR KEYWORD $keyword -->";
	$output[]	=	"<div class='KEYWORDS_list_container'>";
	
	$output[]	=	"<div class='KEYWORDS_list_title'> Showing recent stories tagged with keyword '" . strtoupper( $keyword ) . "'</div>";
	
	$story_ids	=	KEYWORDS_get_story_ids_for_keyword( $id );
	foreach ( $story_ids	as $id )
	{
		
		$output[]	=	HOMEPAGE_format_story( $id );
		
	}	
	$output[]	=	"</div>";
	$output[]	=	"<!-- END LIST OF STORIES FOR KEYWORD $keyword -->";
	
	echo implode( "\r\n", $output );
	
}


function KEYWORDS_list_keyword()
{
	
	echo "LIST ALL KEYWORDS";	
	
}





function KEYWORDS_get_number_of_stories( )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id
					FROM keywords ";
	$mysql->query( $sql );
	
	$output		=	array();
	$mysql_n	=	new mysql_connection();
	
	while ( $res = $mysql->get_row() )
	{
		
		$sql	=	"	SELECT keyword_mappings.id 
						FROM keyword_mappings
						LEFT JOIN stories
						ON keyword_mappings.story = stories.id
						WHERE 	keyword_mappings.keyword = " . $res['id'] . " 
						AND 	stories.live = 1";
		$mysql_n->query( $sql );
		$output[ $res['id'] ] 	=	$mysql_n->get_num_rows();			
		
	}
	
	return $output;
	
}




function KEYWORDS_list_formatted_keywords( $silent = false )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_bool( $silent ) ) $silent = false;
	
	$output		=	array();
	$output[]	=	"<!-- BEGIN SMALL KEYWORD LIST -->";
	$output[]	=	"<div class='KEYWORDS_small_list_container'>";
	if ( !$silent)
	{
		
		$output[]	=	"<div class='KEYWORDS_small_list_title'>Current Keywords...</div>";	
		
		
	}
	
	$cache		=	KEYWORDS_get_keyword_cache();
	$numbers	=	KEYWORDS_get_number_of_stories();
	foreach ( $cache as $id => $keyword )
	{
		
		$output[]	=	"<div class='KEYWORDS_small_list_item'>";
		
		$output[]	=	"<a href='" . WEBSITE_URL . "keywords/?id=$id'>" . strtoupper( $keyword ) . "</a> ( " . $numbers[ $id ] . " )";
		
		$output[]	=	"</div>";
		
	}
	
	$output[]	=	"</div>";
	$output[]	=	"<!-- END SMALL KEYWORD LIST -->";
	
	echo implode( "\r\n", $output );
	
}




function KEYWORDS_get_similar_stories( $story_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) ) return false;
	
	$ids		=	KEYWORDS_get_ids_for_story( $story_id );
	$id_csv		=	implode( ",", $ids );
	
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT DISTINCT keyword_mappings.story
						FROM keyword_mappings
						
						LEFT JOIN stories
						ON keyword_mappings.story = stories.id
						
						WHERE
								keyword_mappings.keyword IN ( $id_csv )
						AND		stories.live	=	1
						AND		stories.deleted <> 1
						AND		keyword_mappings.story <> $story_id
						
						ORDER BY stories.date_live DESC

						LIMIT 0, 8 ";
	$mysql->query( $sql );
	
	
	$output		=	array();
	$output[]	=	"<!-- BEGIN SIMILAR STORIES BY KEYWORD -->";
	$output[]	=	"<div class='STORIES_similar_stories_container'>";
	
	$output[]	=	"<div class='STORIES_similar_stories_title'>";
	$output[]	=	"Stories about similar subjects...";
	$output[]	=	"</div>";
	
	
	while ( $res	=	$mysql->get_row() )
	{
		
		$story		=	new story( $res['story']);
		$output[]	=	"<div class='STORIES_similar_stories_headline'>";
		$output[]	=	"<a href='" . WEBSITE_URL . STORIES_make_story_link( $story->get_headline() ) . "'>";
		$output[]	=	$story->get_headline();
		$output[]	=	"</a></div>";
		$output[]	=	"<div class='STORIES_similar_stories_summary'>";
		$output[]	=	$story->get_summary();
		$output[]	=	"</div>";
		$output[]	=	"<div class='STORIES_similar_stories_keywords'>";
		$output[]	=	KEYWORDS_get_keywords_links( $story->get_id() );
		$output[]	=	"</div>";
		
	}
	
	
	
	$output[]	= "<div class='HOMEPAGE_sib_title' style='font-family: Arial;font-size: 16pt;font-variant: small-caps;font-weight: 700;padding-bottom: 5px;'>SchNEWS in Brief...</div>
		<a class=\"twitter-timeline\"  href=\"https://twitter.com/SchNEWS\" data-widget-id=\"428979152696274944\">Tweets by @SchNEWS</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>

		
		<div class=''>
			<a href='http://twitter.com/#!/SchNEWS' target='_BLANK'>
				Twitter: @SchNEWS
			</a>
		</div>";
	$output[]	=	"\r\n\r\n</div>";
	$output[]	=	"<!-- END SIMILAR STORIES BY KEYWORD -->";
	echo implode( "\r\n", $output );
	
}




