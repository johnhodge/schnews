<?php



/*
 * 	Takes the story SQL id and encodes it in a lossless fashion,
 * 	making it suitable for including in GET links
 */
function PRINT_encode_id( $id )
{
	
	//	Test inputs
	if ( !CORE_is_number( $id ) )
	{
		
		return false;
		
	}
	
	
	$id		=	$id + 1483;
	$id 	=	$id * 378;
	$id		=	$id * 94827;
	
	return $id;
	
}



/*
 * 	Takes an id presiously encoded with PRINT_encode_id and
 * 	converts it back to its native SQL id form
 */
function PRINT_decode_id( $id )
{
	
//	Test inputs
	if ( !CORE_is_number( $id ) )
	{
		
		return false;
		
	}
	
	$id		=	$id / 94827;
	$id		=	$id / 378;
	$id		=	$id - 1483;
	
	return $id;
	
}