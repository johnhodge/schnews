<?php
function HOMEPAGE_show_pap( $id, $cut, $show_month = 0, $show_region = 0 )	{

	/*
	 * 	Test inputs
	 */	
	if ( !is_numeric( $id ) ) return FASLE;
	if (!is_numeric($show_region) && !is_bool($show_region))	$show_region = false;

	
	
	/*	
	 * 	Select record from database
	 */
	$mysql	=	new mysql_connection();
	$sql	=		"SELECT * FROM pap WHERE id = $id";
	$mysql->query( $sql );

	if ( $mysql->get_num_rows( ) != 1 )	return FALSE;
	$res		=	$mysql->get_row( );

	foreach ($res as $key => $datum)	
	{

		$res[$key]	=	stripslashes($datum);

	}


	
	/*
	 * 	Get country
	 */
	if (!is_null($res['county']))	
	{
		$mysql_c	=	new mysql_connection();
		$sql		=	"SELECT county FROM pap_counties WHERE id = ".$res['county'];
		$mysql_c->query( $sql );
		if ( $mysql_c->get_num_rows() == 1 )	
		{
			$resC		=	$mysql_c->get_row();
			$county		=	$resC['county'];
			
		} 
		else 
		{
			$county 	=	'';
		}
	} 
	else	$county 	= 	'';

	
	
	/*
	 * 	Begin Output
	 */	
	$output		=	"\r\n\r\n<!-- BEGIN PAP ITEM -->\r\n\r\n<div class='HOMEPAGE_pap_item'>\r\n";


	/*
	 * Display Dates
	 */
	if ($res['date_to']	==	'0000-00-00 00:00:00')	
	{

		if ($show_month == 0) 
		{
			$datetime	=	new date_time( $res['date_from'] );	
			$output		.=	"\r\n<div class='HOMEPAGE_pap_date'>" . $datetime->make_human_date() . "</div>";
		}
		else	
		{
			$datetime	=	new date_time ( $res['date_from'] );	
			$output		.=	"<b>" . $datetime->make_human_date() . " - </b>";
			
		}

	} 
	else 
	{

		$date_from	=	new date_time( $res['date_from'] );
		$date_to	=	new date_time( $res['date_to'] );
		$output    	.=	"\r\n<div class='HOMEPAGE_pap_date'>" . $date_from->make_human_date() . " to " . $date_to->make_human_date() . "</div> ";
	
	}

	
	/*	
	 * Display Name & Summary
	 */
	$summary	=	$res['summary'];
	//$summary	=	wordwrap( $summary, 40, "\r\n", true );
	$output		.=		"\r\n<div class='HOMEPAGE_pap_name'>".$res['name']."</div>";
	$output		.=		"\r\n<div class='HOMEPAGE_pap_summary'>" . nl2br( strip_tags ( $summary ) ) . "</div>";

	
	/*
	 * Display Location
	 */
	if ($res['place'] != "")	$output	.=	"
	<br><b>Address:</b> ".$res['place'];

	
	/*
	 * Display Region
	 */
	if ($show_region)	
	{
		if ($county != "")	$output	.=	"
		<br><b>Region:</b> ".$county;
	}

	
	/*	
	 * Display other info
	 */
	if ($res['directions'] != "")	$output	.=	"
	<br><b>Directions:</b> ".$res['directions'];
	if ($res['transport'] != "")	$output	.=	"
	<br><b>Nearest Public Transport:</b> ".$res['transport'];
	if ($res['postcode'] != "")	$output	.=	"
	<br><b>Postcode:</b> ".$res['postcode'];
	//if ($mapLink	=	makeMapLink($res['postcode'])) $output .= " | $mapLink ";
	if ($res['time'] != "")		$output	.=	"
	<br><b>Time:</b> ".$res['time'];
	if ($res['price'] != "")	$output	.=	"
	<br><b>Price:</b> ".$res['price'];
	if ($res['phone'] != "")	$output	.=	"
	<br><b>Phone:</b> ".$res['phone'];
	if ($res['email'] != "")	$output	.=	"
	<br><b>Email:</b> " . URL_make_mailto_link( $res['email'] );
	if ($res['website'] != "")	$output	.=	"
	<br><b>Web:</b> " . URL_make_http_link( $res['website'] );

	
	
	return $output."\r\n\r\n</div>\r\n\r\n<!-- END PAP ITEM -->\r\n\r\n\r\n";

}
