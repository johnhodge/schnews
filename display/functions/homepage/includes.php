<?php



function HOMEPAGE_include_pap( $silent = false, $show_all = false )
{
	
	if ( !is_bool( $silent ) ) 		$silent = false;
	if ( !is_bool( $show_all ) ) 	$show_all = false;
	
	$pap	=	"
	
	<!-- BEGIN PARTY AND PROTEST -->
	<div class=\"HOMEPAGE_pap_container\">

	
		";
		
		if ( !$silent )
		{
		
		$pap	.= "
		<!--	Party and Protest Link	-->
		<div style='text-align: center'>
			<a href='party-and-protest/'>
				<img src='../images_main/pap-button-225.png' width=\"225px\" height=\"62\" style='border: none' align='center' />
			</a>
		</div>

		<div style='text-align: left; font-size: 8pt; font-weight: bold; padding-top: 5px'>
			<a href='party-and-protest/submit.php' target='_BLANK'>
				Click Here to Submit a Party or Protest
			</a>		
		</div>
		
		";
		
		}
	
	
		/*
		 * 	Generate list of P&P entries
		 */
		$mysql		=	new mysql_connection();
		$now		=	substr( DATETIME_make_sql_datetime(), 0, 10 ) . ' 00:00:00';

		$i			=		1;
		$now_y		=		date( 'Y' );
		for ($y		=	( $now_y - 2 ); $y <= ( $now_y + 4 ) ; $y++	)	{

			$sql	=	"	SELECT id 
							FROM pap 
							WHERE 
								(	
									date_to >= '$now' 
								OR 	date_from >= '$now'
								) 
								AND DATE_FORMAT(date_from, '%Y') = $y 
								AND auth = 1 
							ORDER BY date_from ASC";
			$mysql->query( $sql );
			debug ( $sql, __FILE__, __LINE__ );
	
			if ( $mysql->get_num_rows() != 0 )	
			{
	
				for ($m		=	1; $m  <= 12 ; $m++)			
				{
	
					$mysql_m		=	new mysql_connection();
					$sql_m			=	"	SELECT id 
											FROM pap 
											WHERE 
												(
													date_to >= '$now' 
												OR 	date_from >= '$now'
												) 
												AND	DATE_FORMAT(date_from, '%Y') = $y 
												AND	DATE_FORMAT(date_from, '%m') = $m 
												AND	auth = 1 
											ORDER BY date_from ASC";
					$mysql_m->query( $sql_m );
	
					if ( $mysql_m->get_num_rows() != 0 )	
					{
	
						if ( $i <= number_pap_items_to_show || $show_all ) 
						{
							$pap .= "<div class='HOMEPAGE_pap_month'>" . date("F", mktime(0, 0, 0, $m, 1, 2000)) . "</div>";
						}
	
						while	( $res		=	$mysql_m->get_row() )	
						{
	
							if ( $i > number_pap_items_to_show && !$show_all ) break;
	
							$pap	.=  HOMEPAGE_show_pap($res['id'], 40, 0);
	
							$i++;
						
						}
	
					}
			
				}
	
			}					

		}
		
		
		if ( !$silent )
		{
		
		$pap .=		"
		
		<!-- Link to Full P&P Listings -->		
		<div class='HOMEPAGE_pap_internal_links'>
			<a href=\"http://www.schnews.org.uk/party-and-protest/\">
				Click here for the whole Party &amp; Protest guide
			</a>
		</div>";
		
		}
		
		
		$pap .= "

</div>

<!--  END PARTY AND PROTEST -->


";
	
	
	
return $pap;
	
}




function HOMEPAGE_include_schmovies()
{
	
	
	/*
	 * 	Grab data from DB
	 */
	$now	=	DATETIME_make_sql_datetime();
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT *
					FROM schmovies
					WHERE date_event > '$now'
					ORDER BY date_event ASC
					LIMIT 0, " . number_schmovies_items_to_show;
	$mysql->query( $sql );
	
	
	
	/*
	 * Begin output
	 */
	$output		=	array();
	$output[]	=	"\r\n\r\n<!-- BEGIN SCHMOVIES ITEMS -->";
	$output[]	=	"<div class='HOMEPAGE_schmovies_container'>";
	
	
	$output[]	=	"<div style='text-align: center'>";
	$output[]	=	"\t<a href='http://www.schnews.org.uk/schmovies/'>";
	$output[]	=	"\t\t<img src='../images_main/schmovies-button-mo-225.png' width='225px' height='60px' />";
	$output[]	=	"\t</a>";
	$output[]	=	"</div>";
	
	
	
	
	while ( $res = $mysql->get_row() )
	{
		
		$output[]	=	"\r\n\r\n<!-- BEGIN SCHMOVIES ITEM -->\r\n";
		$output[]	=	"<div class='HOMEPAGE_schmovies_item'>";
		
		$title		=	stripslashes( $res['title'] );
		if ( trim( $res['link'] ) != '' )
		{
			
			$title		=	URL_make_http_link( $res['link'], $title );
			
		}
		$output[]	=	"<div class='HOMEPAGE_schmovies_title'>$title</div>";
		$output[]	=	"<div class='HOMEPAGE_schmovies_summary'>";
		$output[]	=	stripslashes( $res['summary'] );
		$output[]	=	"</div>";
		
		$output[]	=	"</div>";
		$output[]	=	"\r\n<!-- END SCHMOVIES ITEM -->\r\n\r\n";
		
	}
	
	$output[]	=	"</div>";
	
	
	$output[]	=	"
	<!-- Link to Full SchMOVIES Listings -->		
		<div class='HOMEPAGE_schmovies_internal_links'>
			<a href=\"schmovies/\">
				Click here for full SchMOVIES page
			</a>
	</div>";
		
		
	$output[]	=	"<!-- END SCHMOVIES ITEMS -->\r\n\r\n";
	
	return implode( "\r\n", $output );
	
}




function HOMEPAGE_include_advert_one()
{

	return file_get_contents( DISPLAY_SITE_ROOT . 'inc/homepage/advert_one.php' );

}


function HOMEPAGE_include_advert_two()
{

	return file_get_contents( DISPLAY_SITE_ROOT . 'inc/homepage/advert_two.php' );

}



function HOMEPAGE_include_adhoc_content_one()
{

	return '';

}

function HOMEPAGE_include_adhoc_content_two()
{

	return '';

}




function HOMEPAGE_include_schnews_in_brief()
{

	return "
	
	<!-- INCLUDE TWITTER FEED -->
	
	<div class='HOMEPAGE_sib_container'>

		<div class='HOMEPAGE_sib_title'>SchNEWS in Brief...</div>
		<a class=\"twitter-timeline\"  href=\"https://twitter.com/SchNEWS\" data-widget-id=\"428979152696274944\">Tweets by @SchNEWS</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>

		
		<div class='HOMEPAGE_sib_twitter_link'>
			<a href='http://twitter.com/#!/SchNEWS' target='_BLANK'>
				Twitter: @SchNEWS
			</a>
		</div>
	
		<div class=\"tweet-old\"></div>
	
	</div>
	<!-- END TWITTER FEED -->
	";

}





function HOMEPAGE_include_main_graphic()
{

	return '';

}





function HOMEPAGE_include_merch_links()
{

	$path		=	DISPLAY_SITE_ROOT . 'inc' . DIRECTORY_SEPARATOR . 'homepage' . DIRECTORY_SEPARATOR . 'merch_links.php';
	$output		=	file_get_contents( $path );
			
	return $output;

}




function HOMEPAGE_include_crap_arrest()
{

	/*
	 * 	Grab data from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT title, content
					FROM homepage_crap_arrests
					ORDER BY date DESC
					LIMIT 0, 1";
	$mysql->query( $sql );
	$data	=	$mysql->get_row();
	
	
	
	/*
	 * 	Test whether is is populated
	 */
	if ( $data['title'] == '' || $data['content'] == '' )
	{
		
		return '';
		
	}
	
	
	$output		=	"
	
	
		<!-- BEGIN CRAP ARREST -->
		<div class='HOMEPAGE_crap_arrest_container'>
		
			<div class='HOMEPAGE_crap_arrest_title'>
				" . htmlentities( $data['title'] ) . "
			</div>
			
			<div class='HOMEPAGE_crap_arrest_content'>
				" . $data['content'] . "
			</div>
		
			<div class='HOMEPAGE_crap_arrest_link'>
				<a href='stories/Crap-Arrest-of-the-Week'>More Crap Arrests...</a> 
			</div>
			
		</div>
		<!-- END CRAP ARREST -->


";
		
	return $output;
	
}




function HOMEPAGE_include_and_finally()
{

	/*
	 * 	Grab data from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT title, summary
					FROM homepage_and_finally
					ORDER BY date DESC
					LIMIT 0, 1";
	$mysql->query( $sql );
	$data	=	$mysql->get_row();
	
	
	
	/*
	 * 	Test whether is is populated
	 */
	if ( $data['title'] == '' || $data['summary'] == '' )
	{
		
		return '';
		
	}
	
	
	$output		=	"
	
	
		<!-- BEGIN AND FINALLY -->
		<div class='HOMEPAGE_and_finally_container'>
		
			<div class='HOMEPAGE_and_finally_title'>
				<i>" . htmlentities( $data['title'] ) . "</i>
			</div>
			
			<div class='HOMEPAGE_and_finally_content'>
				" . $data['summary'] . "
			</div>
		
			<div class='HOMEPAGE_and_finally_link'>
				<a href='stories/And-Finally'>Read Full Story...</a> 
			</div>
			
		</div>
		<!-- END AND FINALLY -->


";
		
	return $output;
	
}





function HOMEPAGE_include_disclaimer()
{

	/*
	 * 	Grab data from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT disclaimer
					FROM homepage_misc_fields
					ORDER BY id DESC
					LIMIT 0, 1";
	$mysql->query( $sql );
	$data	=	$mysql->get_row();
	
	
	
	/*
	 * 	Test whether is is populated
	 */
	if ( $data['disclaimer'] == '' )
	{
		
		return '';
		
	}
	
	
	$output		=	"
	
	
		<!-- BEGIN DISCLAIMER -->
		<div class='HOMEPAGE_disclaimer'>
		
			<i>" . htmlentities( $data['disclaimer'] ) . "</i>
		
		</div>
		<!-- END DISCLAIMER -->


";
		
	return $output;
	
}





