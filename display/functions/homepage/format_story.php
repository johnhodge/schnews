<?php



/*
 * 	Generates an HTML div to display the given story in formatted HTML display code.
 */
function HOMEPAGE_format_story( $id, $prime = false )
{
	
	/*
	 * Test inputs
	 */
	if ( !CORE_is_number( $id ) ) return false;
	if ( !is_bool( $prime ) ) $prime 	=	false;	
	
	
	/*
	 * 	Make anew story object
	 */
	$story		=	new story( $id );
	
	
	/*
	 * 	Work out what size CSS styles to use
	 */
	$css_add	=	'';
	switch ( $story->get_size() )
	{
		
		case 1:
			$css_add	=	'_SMALL';
			break;
			
		case 3:
			$css_add	=	'_BIG';
			break;
			
	}
	
	
	/*
	 * 	Test and make graphic
	 */
	$graphic 		=	'';
	$graphic_css	=	'';
	if ( $story->get_graphic_sm() != '' )
	{
		
		if ( file_exists( WEBSITE_ROOT . "images/" . $story->get_graphic_sm() ) )
		{
			
			$link_one	=	'';	$link_two	=	'';
			if ( $story->get_graphic_vlg() && file_exists( WEBSITE_ROOT . "images/" . $story->get_graphic_vlg() ) )
			{
				
				$link_one	=	"<a href='" . WEBSITE_URL . "images/" . $story->get_graphic_vlg() . "' target='_BLANK'>";
				$link_two	=	"</a>";
				
			}
			
			$graphic	=	"<div class='HOMEPAGE_story_graphic'>";
			$graphic	.=	$link_one;
			$graphic	.=	"<img src='" . WEBSITE_URL . "images/" . $story->get_graphic_sm() . "' />";
			$graphic	.=	$link_two;
			$graphic	.=	"</div>";
					
			$graphic_css	=	" style='width: 310px' ";
		}
		
	}
	
	
	/*
	 * 	Test to see if we should be using different css tags
	 */
	if ( $prime ) 
	{

		$css_add	=	'_PRIME';
		
	}
	
	
	/*
	 * 	Begin output buffer
	 */
	$output		=	array();
	
	$date		=	new date_time( $story->get_date_live() );
	
	$link		=	STORIES_make_story_link( $story->get_headline() );
	
	$output[]	=	"\r\n\r\n<!-- BEGIN STORY " . strtoupper( $story->get_headline() ) . " -->";
	$output[]	=	"<div class='HOMEPAGE_story_container" . $css_add . "'>\r\n\r\n";
	
	$output[]	=	$graphic;
	
	$output[]	=	"<div class='HOMEPAGE_story_date" . $css_add . "'>" . $date->make_human_date() . "</div>";	
	
	$output[]	=	"<div class='HOMEPAGE_story_headline" . $css_add . "' $graphic_css ><a href='" . WEBSITE_URL . "$link' class='HOMEPAGE_story_headline_link'>" . $story->get_headline() . "</a></div>";
	if ( $story->get_subheadline() != '' )
	{
		$output[]	=	"<div class='HOMEPAGE_story_subheadline" . $css_add . "'>" . $story->get_subheadline() . "</div>";
	}
	$output[]	=	"<div class='HOMEPAGE_story_summary" . $css_add . "'>" . $story->get_summary() . "<a class='HOMEPAGE_story_read_full' href='" . WEBSITE_URL . "$link'> [ Read Full Story ] </a></div>";
	
	$keywords	=	KEYWORDS_get_keywords_links( $story->get_id() );
	if ( trim( $keywords ) != '' )
	{
	
		$output[]	=	"<div class='HOMEPAGE_story_keywords" . $css_add . "'><b style='color: #666666'>Keywords:</b> " . $keywords . "</div>";
		
	}
	
	$output[]	=	"\r\n\r\n</div>";
	$output[]	=	"<!-- END STORY " . strtoupper( $story->get_headline() ) . " -->\r\n\r\n";
	
	return implode( "\r\n", $output );
	
}








