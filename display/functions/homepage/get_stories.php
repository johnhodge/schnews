<?php



/*
 * 	Generate a list of SQL IDs for the stories that are to be displayed on the homepage main list.
 */
function HOMEPAGE_get_story_ids()
{
	
	$output		=	array();
	$mysql		=	new mysql_connection();
	$count		=	0;
	
	/*
	 * Grab the IDs of sticky stories from the database
	 */
	$sql	=	"	SELECT id 
					FROM stories
					
					WHERE 
						live = 1
					AND	hide <> 1
					AND sticky = 1
					AND deleted <> 1
					
					ORDER BY 
						date_live DESC
					";
	$mysql->query( $sql );
	
		
	/*
	 * 	Add them to an array
	 */
	while( $res		=	$mysql->get_row() )
	{
		if ( $count > number_of_stories_to_display ) break;
		$output[]	=	$res[ 'id' ];
		$count++;
		
	}
	
	
	
	
	/*
	 * 	Grab IDs of non-sticky stories from database
	 */
	$sql	=	"	SELECT id 
					FROM stories
					
					WHERE 
						live = 1
					AND	hide <> 1
					AND sticky = 0
					AND deleted <> 1
					
					ORDER BY 
						date_live DESC
						
					";
	$mysql->query( $sql );
	
	
	/*
	 * 	Add them to an array
	 */
	while( $res		=	$mysql->get_row() )
	{
		
		if ( $count > number_of_stories_to_display ) break;
		$output[]	=	$res[ 'id' ];
		$count++;
	}

	
	return $output;	
	
}



function HOMEPAGE_get_formatted_story_list( $ids = false )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_array( $ids ) || $ids == false )	$ids 	=	HOMEPAGE_get_story_ids();
	
	$output	=	array();
	
	$output[]	=	"\r\n\r\n<!-- BEGIN LIST OF STORIES -->";
	$output[]	=	"<div class='HOMEPAGE_story_list_container'>\r\n\r\n";
	$count		=	0;
	foreach ( $ids as $id )
	{
		
		$count++;
		
		$type 	=	false;
		if ( $count	==	1 )	$type = true;
		
		$output[]	=	HOMEPAGE_format_story( $id, $type );
		
	}
	
	$output[]	=	"\r\n\r\n<div class='HOMEPAGE_previous_stories'>";
	$output[]	=	"<a href='../archived_stories/'>Previous Stories</a>";
	$output[]	=	"</div>\r\n\r\n";
	
	$output[]	=	"\r\n\r\n<div class='HOMEPAGE_archive'>";
	$output[]	=	"<a href='../archive/'>For our entire archive all the way back to 1994, check out our Archived Website</a>";
	$output[]	=	"</div>\r\n\r\n";
	
	$output[]	=	"\r\n\r\n</div>";
	$output[]	=	"<!-- END LIST OF STORIES -->\r\n\r\n";
	
	return implode( "\r\n", $output );
	
}


