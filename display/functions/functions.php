<?php


	$here	=	DISPLAY_SITE_ROOT . 'functions' . DIRECTORY_SEPARATOR;
	
	
	
	/*
	 * 	General Functions
	 */
	$general_here		=	$here . 'general' . DIRECTORY_SEPARATOR;
	require $general_here . 'page_and_template.php';
	require $general_here . 'settings.php';
	require $general_here . 'log.php';
	require $general_here . 'rss.php';
	require $general_here . 'datetime.php';
	
	
	
	/*
	 * 	Homepage functions
	 */
	$homepage_here		=	$here . 'homepage' . DIRECTORY_SEPARATOR;
	require $homepage_here . 'get_stories.php';
	require $homepage_here . 'format_story.php';
	require $homepage_here . 'includes.php';
	require $homepage_here . 'pap.php';
	
	
	
	/*
	 * 	Story Functions
	 */
	$story_here			=	$here . 'story' . DIRECTORY_SEPARATOR;
	require $story_here	. 'display_story.php';
	require $story_here	. 'keywords.php';
	require $story_here	. 'links.php';
	require $story_here . 'print.php';
	
	
	
	/*
	 * 	Issues Functions
	 */
	$story_here			=	$here . 'issues' . DIRECTORY_SEPARATOR;
	require $story_here	. 'issues_page.php';
	
	
	
	/*
	 * 	Pages Functions
	 */
	$story_here			=	$here . 'pages' . DIRECTORY_SEPARATOR;
	require $story_here	. 'pages.php';

	
	
	/*
	 * 	And Finally Functions
	 */
	$af_here			=	$here . 'and_finally' . DIRECTORY_SEPARATOR;
	require $af_here	. 'display_story.php';
	require $af_here	. 'list_stories.php';
	
	
	/*
	 * 	Crap Arrest Functions
	 */
	$ca_here			=	$here . 'crap_arrest' . DIRECTORY_SEPARATOR;
	require $ca_here	. 'list_stories.php';
	
	
	/*
	 * 	Templating functions
	 */
	$template_here		=	$here . 'templates' . DIRECTORY_SEPARATOR;
	require $template_here . 'general.php';