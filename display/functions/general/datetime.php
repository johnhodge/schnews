<?php

function GENERAL_draw_date_selector($date, $name, $split_lines)	{


	if ($date	==	0)	{
			$day		=	0;
			$month		=	0;
			$year		=	0;
		}
	else	{
			$day		=	substr($date, 0, 2);
			$month		=	substr($date, 3, 2);
			$year		=	substr($date, 6, 4);
		}
		
	
	?>
    
		D:
        <select name="<?php echo $name; ?>_day">
           	<option selected></option>
        	<?php
				for ($n	= 1 ; $n<= 31 ; $n ++)	{
					echo "<option";
					if ($day	==	$n) echo " selected ";
					echo ">$n</option>";
				}
			?>
        </select>
        &nbsp;&nbsp;
        M:
        <select name="<?php echo $name; ?>_month">
           	<option selected></option>       
        	<option value="01" <?php if ($month == "01") echo " selected "; ?>>Jan</option> 
        	<option value="02" <?php if ($month == "02") echo " selected "; ?>>Feb</option> 
        	<option value="03" <?php if ($month == "03") echo " selected "; ?>>Mar</option> 
        	<option value="04" <?php if ($month == "04") echo " selected "; ?>>Apr</option> 
        	<option value="05" <?php if ($month == "05") echo " selected "; ?>>May</option> 
        	<option value="06" <?php if ($month == "06") echo " selected "; ?>>Jun</option> 
        	<option value="07" <?php if ($month == "07") echo " selected "; ?>>Jul</option> 
        	<option value="08" <?php if ($month == "08") echo " selected "; ?>>Aug</option> 
        	<option value="09" <?php if ($month == "09") echo " selected "; ?>>Sep</option> 
        	<option value="10" <?php if ($month == "10") echo " selected "; ?>>Oct</option> 
        	<option value="11" <?php if ($month == "11") echo " selected "; ?>>Nov</option> 
        	<option value="12" <?php if ($month == "12") echo " selected "; ?>>Dec</option> 
		</select>
        &nbsp;&nbsp;
        Y:
        <select name="<?php echo $name; ?>_year">
           	<option selected></option>        
        	<?php
        		$l		=	date('Y');
        		$h		=	$l + 4;
				for ($n	=	$l ; $n <= $h ; $n++)	{
					
						echo "<option";
						if ($year == $n) echo " selected ";
						echo ">$n</option>";
					}
			?>
         </select>
         
    <?php     
    
	return TRUE;
	
}