<?php


function RSS_make_feed()
{
	
	
	
	/*
	 *	Load the RSS template 
	 */
	$template_path		=	DISPLAY_SITE_ROOT . 'templates' . DIRECTORY_SEPARATOR . 'rss.xml';
	$template			=	file_get_contents( $template_path );

	
	
	/*
	 * 	Get our SQL data
	 */
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT headline, rss, date_live
						FROM stories
						
						WHERE 	live = 1
						AND 	rss <> ''
						AND 	deleted <> 1
						
						ORDER BY date_live DESC
						LIMIT 0, 100 ";
	$mysql->query( $sql );
	
	
	
	/*
	 * 	Build the item data
	 */
	$items		=	array();
	while ( $res	=	$mysql->get_row() )
	{
		
		$headline	=	$res['headline'];
		$desc		=	$res['rss'];
		$date		=	new date_time( $res['date_live'] );
		$date		=	date( "r", $date->get_timestamp() );
		
		
		
		/*
		 * 	Build item
		 */
		$item		=	array();
		$item[]		=	"<!-- BEGIN ITEM DETAILS -->";
		$item[]		=	"<item>";
		$item[]		=	"\t<title>" . RSS_clean_text( $headline ) . "</title>";
		$item[]		=	"\t<link>http://www.schnews.org.uk/" . STORIES_make_story_link( $headline ) . "</link>";
		$item[]		=	"\t<description>" . RSS_clean_text( $desc ) . "</description>";
		$item[]		=	"</item>";
		$item[]		=	"<!-- END ITEM DETAILS -->";
		
		
		
		/*
		 * 	Add to main list
		 */
		$items[]	=	implode( "\r\n", $item );
	}
	
	
	
	
	/*
	 * 	Update template
	 */
	$template	=	str_replace( "***RSS_ITEMS***", implode( "\r\n\r\n\r\n", $items ), $template);
	
	return $template;
	
}




function RSS_clean_text( $text )
{
	
	$text	=	htmlentities( $text, ENT_QUOTES );
	
	$text	=	str_replace( array( "�", "�" ), "&apos;", $text );
	
	
	return $text;
	
}



