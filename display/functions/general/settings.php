<?php 



/*
 * Loads all the homepage settings from the database and sets them as defined constants
 */
function SETTINGS_load_homepage_settings()
{

	debug ( "Loading homepage_settings" );
	
	/*
	 * 	Get the settings as an array
	 */
	$settings = SETTINGS_get_homepage_settings_as_array();


	/*
	 * 	Cycle through each setting and DEFINE it as a constant
	 */
	$count		=	0;
	foreach ( $settings as $setting => $value )
	{
				if ( $setting == 'id' ) continue;

				if ( !defined( $setting ) )
				{
					define ( $setting, $value );
					$count++;
				}

	}
	
	debug( $count . " settings defined" );

	return true;

}


/*
 * 	Gets the current homepage settings as an array (minus the 'id' field which is stripped out
 *
 * 	@return array
 */
function SETTINGS_get_homepage_settings_as_array()
{

/*
	 * 	Select all story settings from database
	 */
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT * FROM homepage_settings ";
	$mysql->query( $sql );
	/*
	 * 	There should never be anything other than one results. If this is not the case
	 * 	then there is a catastrophic error.
	 */
	if ( $mysql->get_num_rows() != 1 )
	{

		LOG_record_entry( "Couldn't get Homepage Settings from Database. The query did not return 1 result.", 'error', 2, true);
		//require "inc/error.php";
		exit;

	}

	//	Put the settings into an array
	$settings	=	$mysql->get_row();

	/*
	 * 	If the ID field is present (which it will be, given the above SQL query) then remove it.
	 */
	if( isset( $settings[ 'id' ] ) ) unset( $settings[ 'id' ] );

	return $settings;

}



?>