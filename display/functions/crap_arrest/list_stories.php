<?php


function CRAP_ARREST_get_list_of_ids()
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id
					FROM homepage_crap_arrests
					ORDER BY date desc
					LIMIT 0, " . number_crap_arrest_to_show;
	$mysql->query( $sql );
	
	if ( $mysql->get_num_rows() == 0 )
	{
		
		$message	=	"Couldn't get list of ids in CRAP_ARREST_get_list_of_ids()";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 2, 'error' );
		return false;
		
	}
	
	$ids	=	array();
	while ( $res	=	$mysql->get_row() )
	{
		
		$ids[]	=	$res['id'];
		
	}
	
	return $ids;
	
}




function CRAP_ARREST_make_summary_list( $ids )
{

	/*
	 * 	Test inputs
	 */	
	if ( !is_array( $ids ) )
	{
		
		//	DO SOME DEBUGGING HERE
		$message	=	"Input ids is not an array in CRAP_ARREST_make_summary_list()";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 2, 'error' );
		return false;
		
	}	
	
	debug ( "CRAP_ARREST_make_summary_list has " . count( $ids ) . " ids" );
	
	$mysql		=	new mysql_connection();
	
	$output		=	array();
	$output[]	=	"\r\n\r\n<!-- BEGIN CRAP ARREST SUMMARY -->\r\n";
	$output[]	=	"<div class='STORIES_story_container'>";
	
	foreach( $ids as $id )
	{

		$sql	=	"	SELECT id, title, content, date
						FROM homepage_crap_arrests
						WHERE id = $id ";
		$mysql->query( $sql );
		
		if ( $mysql->get_num_rows() != 1 ) continue;
		
		$res	=	$mysql->get_row();
		
		$date	=	new date_time( $res['date'] );
		$output[]	=	"<div class='CRAP_ARREST_item'>";
		
		$output[]	=	"<div class='CRAP_ARREST_title'>" . htmlentities( $res['title'] ) . "</div>";
		$output[]	=	"<div class='CRAP_ARREST_content'>" . $res['content'] . "</div>";
		$output[]	=	"<div class='CRAP_ARREST_date'>" . $date->make_human_date() . "</div>";
		
		$output[]	=	"</div>";
		
	}

	$output[]	=	"</div>";
	$output[]	=	"\r\n<!-- END CRAP ARREST CONTAINER -->\r\n\r\n";

	echo implode( "\r\n", $output );
	
	return true;
	
}




