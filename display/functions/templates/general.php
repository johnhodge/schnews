<?php



function TEMPLATES_get_template( $template )
{
	
	/*
	 * 	Test Inputs
	 */
	if ( !is_string( $template ) || trim( $template ) == '' ) 
	{
		
		debug ( "TEMPLATES_get_template() :: template not a valid string" );
		return false;
		
	}

	
	
	/*	
	 * 	Generate a path to the template file
	 */
	$template	=	$template . ".php";	
	$path 	=	DISPLAY_SITE_ROOT . 'templates' . DIRECTORY_SEPARATOR . $template;
	

	
	/*
	 * 	Load ti if it exists...
	 */
	if ( !file_exists( $path ) ) 
	{
		
		debug ( "TEMPLATES_get_template() :: template file does not exist. path = $path" );
		return false;
		
	}
	return file_get_contents( $path );
	
}