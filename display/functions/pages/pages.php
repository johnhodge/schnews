<?php



function PAGES_display_about_us_text()
{
	
	$text =	file_get_contents( DISPLAY_SITE_ROOT . "templates" . DIRECTORY_SEPARATOR . "about_us.txt" );
	echo $text;
	return;
	
}



function PAGES_diplay_category_list( $silent = false )
{
	
	?>
	<!-- BEGIN KEYWORD CATEGORIES PAGE -->
	<div class='STORIES_story_container'>

		<h2 class='STORIES_story_title'>Story Categories</h2>		

		<div class='STORIES_story_content'>
		
		Stories are categoriesed into different subject areas. You can browse them here...
		
		<br /><br />
		<?php 

			KEYWORDS_list_formatted_keywords( true );
		
		?>
		
		</div>
	
	</div>
	<!-- BEGIN KEYWORD CATEGORIES PAGE -->	
	<?php 
	
}



function PAGES_diplay_previous_stories( $silent = false )
{
	
	?>
	<!-- BEGIN KEYWORD CATEGORIES PAGE -->
	<div class='STORIES_story_container'>

		<h2 class='STORIES_story_title'>Previous Stories From the Past Few Months</h2>		

		<div class='STORIES_story_content'>
		
		To view really old stuff check out our <a href='http://http://www.schnews.org.uk/archive/'>Archived Website</a>
		
		<br /><br />
		<?php 

			$mysql	=	new mysql_connection();
			$sql	=	"	SELECT id 
							FROM stories
							
							WHERE 
								live = 1
							AND	hide <> 1
							AND deleted <> 1
							
							ORDER BY 
								date_live DESC ";
			$mysql->query( $sql );
			
			$output	=	array();
			while ( $res	=	$mysql->get_row() )
			{
				
				$output[]	=	$res['id'];
				
			}
			
			echo HOMEPAGE_get_formatted_story_list( $output );
			
			
		
		?>
		
		</div>
	
	</div>
	<!-- BEGIN KEYWORD CATEGORIES PAGE -->	
	<?php 
	
}





function PAGES_display_issues()
{

	?>
	<!-- BEGIN ISSUES PAGE -->
	<div class='STORIES_story_container'>

		<h2 class='STORIES_story_title'>SchNEWS Issues</h2>		

		<div class='STORIES_story_content'>

		SchNEWS is published as a weekly newsheet, printed around Brighton and distributed around the world.
		PDFs of the printed issues are available here to download...	
			
		
		<br /><br />
		<?php 

			$mysql	=	new mysql_connection();
			$sql	=	"	SELECT id, number
							FROM issues
							WHERE published = 1
							ORDER BY number DESC
							LIMIT 0, 50 ";
			$mysql->query( $sql );
			
			$output	=	array();
			while ( $res	=	$mysql->get_row() )
			{
				
				echo "<div class='PAGES_issue_title'>Issue " . $res['number'];
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				echo ISSUES_make_pdf_link( $res['number'] ) . "</div>";
				echo ISSUES_get_stories_for_issue( $res['id'] );
				
			}
			
			
			
		
		?>
		
		</div>
	
	</div>
	<!-- END ISSUES PAGE -->	
	<?php 
	
}




function PAGES_display_donate()
{
	
	$text =	file_get_contents( DISPLAY_SITE_ROOT . "templates" . DIRECTORY_SEPARATOR . "donate.html" );
	echo $text;
	return;
	
}




