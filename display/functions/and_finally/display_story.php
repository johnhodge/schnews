<?php


function AND_FINALLY_display_story( $id = 0 )
{
	
	/*
	 * 	Test Inputs
	 */
	if ( !CORE_is_number( $id ) ) 
	{

		//	DO SOME SERIOUS DEBUGGING HERE
		$id = 0;
		
	}
	
	if ( isset( $_GET['id'] ) && CORE_is_number( $_GET['id'] ) )
	{
		
		$id	=	$_GET['id'];
		
	}
	
	
	/*
	 * 	Determine which story to query
	 */
	$mysql 	=	new mysql_connection();
	if ( $id == 0 )
	{
		
		$sql	=	"	SELECT * 
						FROM homepage_and_finally
						ORDER BY date DESC
						LIMIT 0, 1 ";
		
	}
	else
	{
		
		$sql	=	"	SELECT *
						FROM homepage_and_finally
						WHERE id = $id ";
		
	}
	
	$mysql->query( $sql );
	
	if ( $mysql->get_num_rows() != 1 )
	{
		
		return;
		
	}
	
	$res	=	$mysql->get_row();
	
	
	
	?>
	
	
	<!-- BEGIN STORY DISPLAY -->
	<div class='STORIES_story_container'>
	
		<div class='STORIES_story_info'>
		
			<?php 
			
				$date	=	new date_time( $res['date'] );
				echo "Added on <b>" . $date->make_human_date() . "</b> ";
			
			?>
		
		</div>
	
		<div class='STORIES_story_title'>
		
			<?php echo htmlentities( $res['title'] ); ?>
		
		</div>
		
		<div class='STORIES_story_content'>
		
			<?php echo $res['content']; ?>
		
		</div>
	
	</div>
	<!-- END STORY DISPLAY -->

	<?php 

	return;
		
}


