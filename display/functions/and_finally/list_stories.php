<?php


function AND_FINALLY_get_list_of_ids()
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id
					FROM homepage_and_finally
					ORDER BY date desc
					LIMIT 0, " . number_and_finally_to_show;
	$mysql->query( $sql );
	
	if ( $mysql->get_num_rows() == 0 )
	{
		
		$message	=	"Couldn't get list of ids in AND_FINALLY_get_list_of_ids()";
		debug( $message, __LINE__, __FILE__ );
		LOG_record_entry( $message, 2, 'error' );
		return false;
		
	}
	
	$ids	=	array();
	while ( $res	=	$mysql->get_row() )
	{
		
		$ids[]	=	$res['id'];
		
	}
	
	return $ids;
	
}




function AND_FINALLY_make_summary_list( $ids )
{

	/*
	 * 	Test inputs
	 */	
	if ( !is_array( $ids ) )
	{
		
		//	DO SOME DEBUGGING HERE
		$message	=	"Input ids is not an array in AND_FINALLY_make_summary_list()";
		debug( $message, __LINE__, __FILE__ );
		LOG_record_entry( $message, 2, 'error' );
		return false;
		
	}	
	
	debug ( "AND_FINALLY_make_summary_list has " . count( $ids ) . " ids" );
	
	$mysql		=	new mysql_connection();
	
	$output		=	array();
	$output[]	=	"\r\n\r\n<!-- BEGIN AND FINALLY SUMMARY -->\r\n";
	$output[]	=	"<div class='AND_FINALLY_container'>";
	
	foreach( $ids as $id )
	{

		$sql	=	"	SELECT id, title, summary, date
						FROM homepage_and_finally
						WHERE id = $id ";
		$mysql->query( $sql );
		
		if ( $mysql->get_num_rows() != 1 ) continue;
		
		$res	=	$mysql->get_row();
		
		if ( isset( $_GET['id'] ) && $res['id'] == $_GET['id'] ) continue;
		
		$date	=	new date_time( $res['date'] );
		$output[]	=	"<div class='AND_FINALLY_item'>";
		
		$output[]	=	"<div class='AND_FINALLY_date'>" . $date->make_human_date() . "</div>";
		$output[]	=	"<div class='AND_FINALLY_title'><a href='?id=" . $res['id'] . "'>" . htmlentities( $res['title'] ) . "</a></div>";
		$output[]	=	"<div class='AND_FINALLY_summary'>" . $res['summary'] . "</div>";
		
		$output[]	=	"</div>";
		
	}

	$output[]	=	"</div>";
	$output[]	=	"\r\n<!-- END AND FINALLY CONTAINER -->\r\n\r\n";

	echo implode( "\r\n", $output );
	
	return true;
	
}




