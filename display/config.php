<?php 



/*
 * 	DIPLSAY SITE URL
 * 
 * 	An absolute URL to this display system's main display.php file
 */
define ( 'DISPLAY_SITE_URL', 'http://www.schnews.org.uk/display/' );





/*
 * 	WEBSITE SITE URL
 * 
 * 	An absolute URL to this website's root index.php file
 */
define ( 'WEBSITE_URL', 'http://www.schnews.org.uk/' );






/*
 * 	WEBSITE ROOT
 * 
 * 	An absolute file system path to the site root that is running this display framework
 */
define ( 'WEBSITE_ROOT', "/home/schnews/www.schnews.org.uk/" );





define ( 'SYSTEM_ADMIN_EMAIL', 'support@andrewwinterbottom.com' );

?>