<?php
/*
 * 	This script creates an HTML header suitable for most pages
 * 
 * 
 * 
 * 	The required vars are:
 * 
 * 		-	$css_to_load [array] 
 * 			
 * 			(Optional) An array listing which CSS dirs to include
 * 
 * 
 * 		-	$page_title [string]
 * 			
 * 			(Optional) The title of the page. If not set then the settings default will be used.	
 *
 * 		-	$page_description [string]
 * 
 * 			(Optional) The description of the page. If not set then the settings default will be used.	
 */

?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!-- BEGIN HTML HEADER -->
<head>

	<?php
		
		/*
		 * 	Page Title
		 */
		if ( !isset( $page_title ) || trim( $page_title ) == '' )
		{
			
			$page_title 	=	default_page_title;
			
		}
	
	?>
	<title><?php echo htmlentities( $page_title ); ?></title>
	
	
	
	<?php
	
		/*
		 * Page Description
		 */
		if ( !isset( $page_description ) || trim( $page_description ) == '' )
		{
			
			$page_description	=	default_page_description;
			
		}
	
	?>
	<meta description="<?php echo htmlentities( $page_description, ENT_QUOTES ); ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	
	<script type='text/javascript' src='<?php echo DISPLAY_SITE_URL . 'js/jquery.js'; ?>' ></script>
	<script type='text/javascript' src='<?php echo DISPLAY_SITE_URL . 'js/jquery.tweet.js'; ?>' ></script>
	<script type='text/javascript' src='<?php echo DISPLAY_SITE_URL . 'js/twitter.js'; ?>' ></script>
	
	<?php 
		/*
		 * 	Load CSS file/s
		 */
	require DISPLAY_SITE_ROOT . 'inc' . DIRECTORY_SEPARATOR . 'load_css.php';
	
	?>
	
</head>
<!-- END HTML HEADER -->


<body>

<?php 

	require DISPLAY_SITE_ROOT . "js/facebook.js";

?>

<div class='GENERAL_page_container'>


