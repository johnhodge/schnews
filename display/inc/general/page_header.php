


<!-- BEGIN PAGE HEADER -->

<div class='GENERAL_page_header'>

	<table width='100%'>
	
	
	
	
	
	
	<!-- BEGIN NAV BAR -->
	<tr>
		<td colspan='2'>
		
			<?php 
			
				require DISPLAY_SITE_ROOT . 'inc' . DIRECTORY_SEPARATOR . 'general' . DIRECTORY_SEPARATOR . 'nav_bar.php';
			
			?>
		
		</td>
		<!-- END NAV BAR -->
	
	
	
	
	
	
	
		<!-- BEGIN MAIN GRAPHIC -->
		<td align='right' rowspan='3' width='10%'>
		
			<div class='GENERAL_main_graphic'>
			
				<?php 
				
					$mysql	=	new mysql_connection();
					$sql	=	"	SELECT graphic_sm, graphic_lg
									FROM homepage_graphics
									ORDER BY date DESC
									LIMIT 0, 1";
					$mysql->query( $sql );
					$data	=	$mysql->get_row();
					$src	=	SITE_IMAGES_URL . $data['graphic_sm'];
					$link	=	SITE_IMAGES_URL . $data['graphic_lg'];
					
				?>
				
				<a href='<?php echo $link; ?>' target='_BLANK'  >
					<img src='<?php echo $src; ?>' />
				</a>
				
			</div>
	
		</td>
		<!-- END MAIN GRAPHIC -->
		
		
		
		
	
	
	
	
	
	<!-- BEGIN SCHNEWS LOGO -->
	<tr>
		<td align='left'>
		
			<div class='GENERAL_wake_up_text'>
			
				<?php
				
					/*
					 * 	Display the Wake Up text if there is any
					 */ 
					$mysql	=	new mysql_connection();
					$sql	=	"	SELECT wake_up_text
									FROM homepage_misc_fields
									ORDER BY id DESC
									LIMIT 0, 1 ";
					$mysql->query( $sql );
					$data	=	$mysql->get_row();
					echo '<i>' . htmlentities( $data['wake_up_text'] ) . '</i>';
				
				?>
			
			</div>
			
			<div class='GENERAL_schnews_logo'>
				<a href='http://www.schnews.org.uk'>
					<img src='../images_main/schnews.gif' width='292px' height='90px' alt='SchNEWS main logo' />
				</a>
			</div>
	
		</td>
		<!-- END SCHNEWS LOGO -->
		
		
		
		
		
		<!-- BEGIN PROMO LINKS -->
		<td valign='bottom'>
			<div class='GENERAL_promo_links'>
				
				<?php 
				
				$text	=	 file_get_contents( DISPLAY_SITE_ROOT . 'inc/general/header_promo_links.html' ); 
				$text	=	str_replace( "***WEBSITE_URL***", WEBSITE_URL, $text );
				echo $text;
				
				?>
								
			</div>
		</td>		
		<!-- END PROMO LINKS -->
		
		
		<!-- BEGIN COPYLEFT BAR -->
		<tr>
			<td colspan='2'>
				<div class='GENERAL_copyleft_bar'>
				
					<?php 
					
						echo file_get_contents( DISPLAY_SITE_ROOT . 'inc/general/copyleft_bar_text.txt' );
					
					?>
				
				</div>			
			</td>	
		</tr>
		<!-- BEGIN COPYLEFT BAR -->
	
		
		
	
	</tr>
	</table>
	
</div>

<!-- END PAGE HEADER -->


