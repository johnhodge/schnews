<?php 

	$nav_bar_items		=	array	(
										'Merchandise' => "http://www.schnews.org.uk/pages_merchandise/merchandise.php",
										'DIY Guide' => "http://www.schnews.org.uk/diyguide/index.htm",
										'Contacts &amp; Links' => "http://www.schnews.org.uk/links/index.php",
										'RSS Feed' => "http://www.schnews.org.uk/feed/rss2html.php"
									);

?>




<!-- BEGIN FOOTER -->

<div class='GENERAL_footer_container'>







	<div class='GENERAL_footer_navbar'>
	<p style="color: #fff; background: #000; padding: 4px; font-weight: 700; font-size: 12px; width: 630px; text-align: center">The free weekly direct action newsheet published in Brighton since 1994 - &copy; Copyleft - Information For Action</p>
	<!--  <img src='../images_main/description-new.png' class='GENERAL_footer_copyleft' />-->
	
	<table class='GENERAL_footer_navbar_table' align='right' >
		<tr>
	
	
			<?php 
					
				foreach ( $nav_bar_items as $label => $link )
				{
							
				?>
								
					<td>
						<a href='<?php echo $link; ?>' class='GENERAL_navbar_link'>
							<div class='GENERAL_navbar_item'>
										
								<span class='GENERAL_navbar_link'>
											
									<?php echo $label; ?>
											
								</span>
										
							</div>
						</a>
					</td>
							
				<?php 
							
				}
				
				?>
	
	
		</tr>
	</table>
	
	
	
	
	
	</div>








	<div class='GENERAL_footer_contacts'>
		SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England<br />
		Phone: +44 (0)1273 685913<br />
		Email: mail@schnews.org.uk<br />
		<br />
		@nti copyright - information for action - copy and distribute!
		
	</div>







</div>
<!-- END FOOTER -->


