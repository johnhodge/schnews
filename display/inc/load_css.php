<?php
/*
 * 	This script checks for each element in the $css_to_load array, and for 
 * 	each element it loads each css file within that directory, located within
 * 	the 'css' path.
 * 
 * 	Obviously, it should be called as part of the page header.
 * 
 */




/*
 * Test that the $css_to_load var exists...
 */
if ( !isset( $css_to_load ) )
{
	
	/*
	 * 	Could add some exception handlers if fussed, otherwise just do nothing...
	 */
	?>
	
	<!-- BEGIN CSS INCLUDE -->	
	
		<!-- NO CSS FILES LOADED -->
	
	<!-- END CSS INCLUDE -->

	<?php 
	
}



/*
 *	Else load the the contents of each directory... 
 */
else
{

	
	/*
	 * 	Cycle through the contents of each folder
	 */
	$output		=	array();		//	Output buffer
	$output[]	=	"\r\n<link rel=\"stylesheet\" type=\"text/css\" href=\"" . DISPLAY_SITE_URL ."css/reset.css\" />";
	foreach ( $css_to_load as $css_file )
	{

			$dir_handle		=	opendir( DISPLAY_SITE_ROOT . 'css' . DIRECTORY_SEPARATOR . $css_file );
			
			$ouptut[]		=	'';
			$output[]		=	"\r\n\r\n<!-- LOADING CSS FOR " . strtoupper( $css_file ) . " -->";
			while ( $filename	=	readdir( $dir_handle ) )
			{
				
				//	Screen for file system entities
				if ( $filename == '.' || $filename == '..' ) continue;
				
				$output[]	=	"<link rel=\"stylesheet\" type=\"text/css\" href=\"" . DISPLAY_SITE_URL ."css/$css_file/$filename\" />";
				
			}
			
			
	}	
	
	
	
	/*
	 * 	Display to screen
	 */
	echo "\r\n\r\n<!-- BEGIN LOAD CSS FILES -->";
	echo implode( "\r\n", $output );
	echo "<!-- END LOAD CSS FILES -->\r\n\r\n";
	
		
}

