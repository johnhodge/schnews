<?php


class comment
{
	
	protected $id;
	protected $story_id;
	
	protected $author;
	protected $content;
	protected $date;
	protected $ip_hash;
	protected $deleted;
	
	protected $spam;
	protected $spam_score;
	protected $spam_reasons;
	
	function __construct( $id = false )
	{
		
		/*
		 * 	Test inputs
		 */
		if ( !CORE_is_number( $id ) ) $id = false;
		
		$this->id	=	$id;
		
		
		/*
		 * Determie if we're adding a new comments or editing an existing one...
		 */
		if ( $this->id == false )
		{
			
			$this->create_new_comment();
			
		}
		else
		{
		
			$this->get_comment_from_db();
		
		}
		
		return true;
		
	}
	

	
	protected function create_new_comment()
	{
		
		$this->story_id		=	false;
		$this->author		=	'';
		$this->content		=	'';
		$this->date			=	DATETIME_make_sql_datetime();
		$this->ip_hash		=	md5( $_SERVER['REMOTE_ADDR'] );
		
		$this->spam			=	false;
		$this->spam_score	=	0;
		$this->spam_reasons	=	'';
		$this->deleted		=	0;
		
		return true;
		
	}
	
	
	
	
	
	
	public function update_db()
	{
		
		if ( $this->id == false )
		{
			
			if ( $this->add_to_database() ) return true;
			
		}
		else
		{
			
			if ( $this->update_in_database() ) return true;
			
		}
		
		return false;
		
	}
	
	
	
	
	
	
	protected function add_to_database()
	{
		
		/*
		 * Test local data
		 */
		if ( $this->story_id == false ) return false;
		if ( $this->content == '' ) return false;
		
		if ( $this->spam_score > spam_points_delete ) $this->deleted = 1;
		
		
		/*
		 * 	Make sure it isn't a duplicate
		 */
		if ( $this->test_for_dupication() )
		{
			
			return true;
			
		}
		
		
		/*
		 * 	Add to database
		 */
		$mysql	=	new mysql_connection();
		$sql	=	" 	INSERT INTO comments
							(
								story,
								comment,
								author,
								date,
								deleted,
								deleted_by,
								spam_score,
								spam_reasons
							)
						VALUES
							(
								" . $this->story_id . ",
								'" . $mysql->clean_string( $this->content ) . "',
								'" . $mysql->clean_string( $this->author ) . "',
								'" . $this->date . "',
								" . $this->deleted . ",
								0,
								" . $this->spam_score . ",
								'" . $this->spam_reasons . "'
							)";
		$mysql->query( $sql );

		
		
		/*
		 * 	Do some messages
		 */
		$message	=	"New comment added to the database, to story " . $this->story_id . ". Comment = " . $this->content;
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'comments' );

		
		
		/*
		 * 	Make sure it worked.
		 */
		if ( !$this->id = $mysql->get_insert_id() )
		{
			
			return false;
			
		}
		
		return true;
		
	}
	
	
	
	
	protected function test_for_dupication()
	{
		
		/*
		 * 	Query DB
		 */
		$mysql	=	new mysql_connection();		
		$sql	=	"	SELECT id
						FROM comments
						WHERE 	comment = '" . $mysql->clean_string( $this->content ) . "'
						AND		story = " . $this->story_id;
		$mysql->query( $sql );
		
		
		
		if ( $mysql->get_num_rows() == 0 )
		{
			
			return false;
			
		}
		if ( $mysql->get_num_rows() == 1 )
		{
			$res		=	$mysql->get_row();
			$this->id	=	$mysql->get_insert_id();
		}
		debug ( "Comment is a duplicate" );
		return true;
		
	}
	
	
	
	
	
	
	/*
	 * 	GETTERS AND SETTERS
	 */
	
	
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $story_id
	 */
	public function getStory_id() {
		return $this->story_id;
	}

	/**
	 * @return the $author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @return the $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @return the $date
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @return the $ip_hash
	 */
	public function getIp_hash() {
		return $this->ip_hash;
	}

	/**
	 * @return the $spam
	 */
	public function getSpam() {
		return $this->spam;
	}

	/**
	 * @return the $spam_score
	 */
	public function getSpam_score() {
		return $this->spam_score;
	}

	/**
	 * @return the $spam_reasons
	 */
	public function getSpam_reasons() {
		return $this->spam_reasons;
	}

	/**
	 * @param $story_id the $story_id to set
	 */
	public function setStory_id($story_id) {
		$this->story_id = $story_id;
	}

	/**
	 * @param $author the $author to set
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * @param $content the $content to set
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * @param $date the $date to set
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * @param $ip_hash the $ip_hash to set
	 */
	public function setIp_hash($ip_hash) {
		$this->ip_hash = $ip_hash;
	}

	/**
	 * @param $spam the $spam to set
	 */
	public function setSpam($spam) {
		$this->spam = $spam;
	}

	/**
	 * @param $spam_score the $spam_score to set
	 */
	public function setSpam_score($spam_score) {
		$this->spam_score = $spam_score;
	}

	/**
	 * @param $spam_reasons the $spam_reasons to set
	 */
	public function setSpam_reasons($spam_reasons) {
		$this->spam_reasons = $spam_reasons;
	}

	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}
	
	
}