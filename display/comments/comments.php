<?php


/*
 * 	COMMENTS MySQL TABLE
 * 
 * 
 * 	(comments)
 * 
 * 	id INT [PK] [AI]
 * 	story INT
 *  comment TEXT
 *  author TEXT
 *  date DATETIME
 *  deleted INT
 *  deleted_by INT
 *  spam_score INT
 *  spam_reasons TEXT
 *  
 * 
 *  
 *  
 *  Also necessary for spam filtering is an IP Hash table, containing the 
 *  last x days worth of spammers IP Hashes
 *  
 *  (spam_ip_hases)
 *  
 *  id INT [PK] [AI]
 *  ip_hash TEXT
 *  date DATETIME
 *  spam_score INT
 */


/*
 * 	Load in the funtions
 */
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "draw_comments_form.php";
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "add_comment.php";
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "spam_filter.php";
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "comment_class.php";
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "settings_functions.php";
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . "list_comments.php";
SETTINGS_load_story_settings();