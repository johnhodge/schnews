<?php

function COMMENTS_add_comment( $story_id )
{
	
	/*
		04-10-14 - Andrew Winterbottom
		SchNEWS is now archived. Comments are no longer accepted.
	*/
	return;
	
	/*
	 * Test inputs
	 */
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"COMMENTS_add_comment() called without a valid story_id. story_id = $story_id";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	/*
	 * 	Only carry on if a comment has been posted
	 */
	if ( !isset( $_POST['content'] ) || !isset( $_POST['author'] ) )
	{
		
		debug ( "No comment detected" );
		return;
		
	}
	
	
	
	
	
	
	
	
	/*
	 * 	Check for Captcha
	 */
	include WEBSITE_ROOT . "display/comments/captcha/securimage.php";
	$securimage = new Securimage();
	if ($securimage->check($_POST['captcha_code']) == false) {
		
		$path		=	DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'comment_captcha.html';
		$output		=	"<a name='comments'></a>\r\n\r\n" . file_get_contents( $path );
		
		$_SESSION['content_n']	=	$_POST['content'];
		$_SESSION['author_n']	=	$_POST['author'];
		
		return $output;
		
	}
	
	
	
	
	
	
	
	
	/*
	 * 	Instantiate and populate a new comment object
	 */
	$comment	=	new comment();
	$comment->setStory_id( $story_id );
	$comment->setContent( $_POST['content'] );
	$comment->setAuthor( $_POST['author'] );
	debug ( "Comment detected. Data set.", __FILE__, __LINE__ );
	if ( $spam_result = COMMENTS_is_spam_comment( $comment ) )
	{
		
		debug ( "Spam comment detected. Score = " . $spam_result['score'] );
		$comment->setSpam( true );
		$comment->setSpam_score( $spam_result['score'] );
		$comment->setSpam_reasons( $spam_result['reason'] );
		COMMENTS_add_ip_hash( $comment );
		debug ( "Comment is spam" );
		
		$email_type	=	'q';
		
		if ( $spam_result['score'] > spam_points_delete )
		{
			
			$comment->setDeleted( 1 );
			$email_type	=	"d";	
			
		}
		
	}
	
	
	
	require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'send_emails.php';
	
	
	
	/*
	 * Add to database
	 */
	if ( $comment->update_db() )
	{
		
		debug ( "Comment added to DB" );
		
	}
	else
	{
		
		debug ( "Failed to add comment to database", __FILE__, __LINE__ );
		
	}
	
	
	
	
	/*
	 * 	Determine its spaminess
	 */
	$quarantine_threshold	=	spam_points_quarantine;
	$delete_threshold		=	spam_points_delete;
	
	
	
	

	
	
	
	if ( 
			$comment->getSpam_score() <  $quarantine_threshold
			
			|| 
			
			$comment->getSpam_score() >  $delete_threshold )
	{
		
		$path		=	DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'comment_added.html';
		$output		=	"<a name='comments'></a>\r\n\r\n" . file_get_contents( $path );
		
	}
	else
	{
		
		$path		=	DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'comment_quarantined.html';
		$output		=	"<a name='comments'></a>\r\n\r\n" . file_get_contents( $path );
		
	}
	
	return $output;
	
}
