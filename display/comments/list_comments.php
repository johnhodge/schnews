<?php

function COMMENTS_list_comments( $story_id )
{
	
	/*
	 * 	Test input
	 */
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message 	=	"COMMENTS_list_comments() called without a valid story_id";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'comments' );
		return false;
		
	}
	
	
	
	
	/*
	 * 	Get data from DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT author, comment, date
					FROM comments
					WHERE
							story	=	$story_id
						AND	spam_score < " . spam_points_quarantine . "
						AND deleted	<> 1
					ORDER BY date ASC ";
	$mysql->query( $sql );
	
						
	
	/*
	 * 	Make sure there are some results
	 */
	if ( $mysql->get_num_rows() == 0 )
	{

		debug ( "This story has no comments" );
		return;
		
	}
	
	
	$output		=	array();
	$count		=	0;
	//$output[]	=	"<a name='comments'></a>";
	
	while ( $res	=	$mysql->get_row() )
	{
		
		$count++;
		$comment 	=	array();
		$comment[]	=	"\r\n\r\n<!-- BEGIN COMMENT $count -->";
		$comment[]	=	"<div class='COMMENTS_display_container'>";
		
		$author		=	htmlentities( $res['author'] );
		if ( trim( $author ) == '' ) $author	=	"<span style='color: #888888'><i>Anonymous</i></span>";

		$date		=	new date_time( $res['date'] );
		$date		=	$date->make_human_date() . ' @ ' . $date->make_human_time();
		
		$comment[]	=	"<div class='COMMENTS_display_author'><b>Added By:</b> $author - $date</div>";
		
		$content	=	nl2br ( htmlentities( $res['comment'] ) );
		$comment[]	=	"<div class='COMMENTS_display_content'>$content</div>";
		$comment[]	=	"</div>";
		$comment[]	=	"<!-- END COMMENT $count -->\r\n\r\n";

		
		$output[]	=	implode( "\r\n", $comment );
		
	}
	
	
	if ( $count == 1 )	
	{
		
		$line	=	" is 1 comment ";
		
	}
	else
	{
		
		$line	=	" are $count comments ";
		
	}
	$header		=	"
	
	<div class='COMMENTS_display_title'>
		There $line on this story...
	</div>";
	
	return $header . implode( "\r\n", $output );
	
}