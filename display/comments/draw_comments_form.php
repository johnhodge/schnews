<?php



function COMMENTS_draw_comments_form( )
{
	
	
	/*
		04-10-14 - Andrew Winterbottom
		
		This is to remove the comments addition function
		
		The SchNEWS site is now archived. No comments are to be accepted.
	?>

	<!-- BEGIN COMMENTS FORM -->	
	<div class='COMMENTS_form_container'>
	
		<div class='COMMENTS_form_title'>
			Add a comment to this story... 
		</div>
	
		<form method='POST' action='#comments' >
		
		
			<span class='COMMENTS_form_valid'>Tick here to confirm message</span><input type='checkbox' name='valid' class='COMMENTS_form_valid' />
		
			<div class='COMMENTS_form_author_label'>
			Your Name: <i>( Optional )</i> 
			<input type='text' name='author' class='COMMENTS_form_author' value='<?php if ( isset( $_SESSION['author_n'] ) ) echo $_SESSION['author_n']; ?>' />
			</div>
			
			<br />
			
			<textarea name='content' class='COMMENTS_form_textarea'><?php if ( isset( $_SESSION['content_n'] ) ) echo $_SESSION['content_n']; ?></textarea>
		
			<br />
			<br />
			
			<div style="font-size: 8pt"><b>Enter the code below to prove you're not a robot</b> (not case-sentitive) :</div>
						
			<img id="captcha" src="<?php echo WEBSITE_URL ?>display/comments/captcha/securimage_show.php" alt="CAPTCHA Image" />
			
			<input type="text" name="captcha_code" size="10" maxlength="6" />
			
			<br />
			<br />
			
			<input type='submit' value='Add Comment' />
		
		</form>	
	
	</div>
	<!-- END COMMENTS FORM -->
	
	<?php 
	*/
	
	?>
	
	<div class='COMMENTS_form_container'>
		<div class='COMMENTS_form_title'>
			SchNEWS no longer accepts comments on stories... 
		</div>
	</div>
	
	<?php
	
	
	unset( $_SESSION['author_n'] );
	unset( $_SESSION['content_n'] );
	
	
}

?>