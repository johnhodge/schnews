<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16 October 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Functions relating to the global system settings
 */




/*
 * Loads all the story settings from the database and sets them as defined constants
 */
function SETTINGS_load_story_settings()
{

	debug ( "Loading story_settings" );
	
	/*
	 * 	Get the settings as an array
	 */
	$settings = SETTINGS_get_story_settings_as_array();


	/*
	 * 	Cycle through each setting and DEFINE it as a constant
	 */
	$count		=	0;
	foreach ( $settings as $setting => $value )
	{
				if ( $setting == 'id' ) continue;

				if ( !defined( $setting ) )
				{
					define ( $setting, $value );
					$count++;
				}

	}
	
	debug( $count . " settings defined" );

	return true;

}



/*
 * 	Gets the current story settings as an array (minus the 'id' field which is stripped out
 *
 * 	@return array
 */
function SETTINGS_get_story_settings_as_array()
{

/*
	 * 	Select all story settings from database
	 */
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT * FROM story_settings ";
	$mysql->query( $sql );
	/*
	 * 	There should never be anything other than one results. If this is not the case
	 * 	then there is a catastrophic error.
	 */
	if ( $mysql->get_num_rows() != 1 )
	{

		LOG_record_entry( "Couldn't get Story Settings from Database. The query did not return 1 result.", 'error', 2, true);
		require "inc/error.php";
		exit;

	}

	//	Put the settings into an array
	$settings	=	$mysql->get_row();

	/*
	 * 	If the ID field is present (which it will be, given the above SQL query) then remove it.
	 */
	if( isset( $settings[ 'id' ] ) ) unset( $settings[ 'id' ] );

	return $settings;

}



/*
 * 	A textual description of what each setting does...
 * 
 * 	@return array
 */
function SETTINGS_get_story_settings_descriptions()
{

	$settings		=	array();
	$settings[ 'internal_mailing_list_address' ] 	= "Address of internal mailing list to which notices such should be posted in the event of new stories, votes required, stories being live, etc.";
	$settings[ 'plain_text_mailing_lisrt' ]		 	= "Address of the plain text mailing list for sending the weekly issue email to.";
	$settings[ 'number_story_votes_to_live' ] 		= "How many votes must a story get from users other than the initial author in order for it to be made live?<br /><br />If changed for a higher value it should not affect existing live stories.";
	$settings[ 'number_admin_votes_to_live' ] 		= "How many admins must vote for a story for it to be live<br /><br />For example, is this was set to 2 and the above setting to 5, the each story must received 5 votes in total, of which at least 2 must be from admins.<br /><br />If changed for a higher value it should not affect existing live stories.";
	$settings[ 'email_author_on_vote' ] 			= "Should the author of a story be emailed to inform them that someone has voted from their story?.";
	$settings[ 'email_list_on_live' ] 				= "Should the author of a story or the mailiing list be notified when a story is voted live?<br /><br />If tick then the mailing list will be emailed, if not ticked then only the author will be emailed.";
	$settings[ 'email_list_on_issue_publish' ] 		= "Should the mailing list be notified that an issue has been published/finished and a new issue started?";
	$settings[ 'secret_story_ballot' ]	 			= "Should the identity of users who have voted for a story be hidden?.";
	$settings[ 'checkout_timeout' ] 				= "After how many minutes of inactivity should a user's edits be automatically checked back in?<br /><br />After this number of minutes of inactivity the story will become editable again by other users. This is used in case a user starts editing a story and then leaves their computer without checking the story back in again.";
	$settings[ 'checkout_timeout_reduction' ] 		= "If another user wants to lock and edit a story already under someone else's control then by how many minutes should the checkout timeout be reduced for that story?<br /><br />If this is greater than or equal to the checkout timeout then it causes any checkout takeover requests to instantly seed control to the new user.";
	$settings[ 'checkout_timeout_save' ] 			= "If a user is inactive for x minutes and theyir story checkout is timed out, then should their changes be saved?";
	$settings[ 'number_keyword_for_votes' ] 		= "How many votes does a proposed / modified keyword need to receive before it is accepted?<br /><br /><b>Note:</b> Once this number of votes are received the keyword will be live. Any further votes against will not matter.";
	$settings[ 'number_keyword_against_votes' ] 	= "How many votes against a proposed / modified keyword are needed to veto it?<br /><br /><b>Note:</b> Once this number of votes are received the keyword will be live. Any further votes against will not matter.";
	$settings[ 'spam_points_quarantine' ]			= "How many points does a public comment need to acrue before it is quarantined?<br /><br />Quarantined spam comments can be check later.";
	$settings[ 'spam_points_delete' ]				= "How many points does a public comment need to acrue before it gets deleted?<br /><br />If deleted they are gone forever.<br /><br /><b>Note:</b> It makes no sense to set this greater than <b>spam_points_quarantine</b>";
	$settings[ 'spam_weight_hidden_fields' ]		= "How many points to assign to a public comment is a hidden field is ticked?<br /><br />The comment form will have two hidden elements, one hidden by CSS and one hidden by Javascript, neither of which a real user using a browser should see, but a robot would see.<br /><br /><b>Note:</b> It is not recommended to set this as greater than <b>spam_points_quarantine</b>";
	$settings[ 'spam_keyword_modifier' ]			= "How much weight to give to the overall keyword hit score? Keyword strength times modifier is added to the spam score.";
	$settings[ 'spam_links_threshold' ]				= "How many links per comments are acceptable, above which they start adding to the spam score?";
	$settings[ 'spam_links_score' ]					= "For each link count above the <b>spam_links_threshold</b> how many points are added to the spam score?";
	$settings[ 'spam_ip_filter_duration' ]			= "If a comment is flagged as spam it IP address is stored as a hash, and future comments are tested against this hash.<br /><br />For how many minutes should an IP hash be stored and compared against future comments.";
	$settings[ 'spam_ip_filter_score' ]				= "How many points should be assigned to comment from a flagged Ip address?";
	$settings[ 'spam_email_on_quarantine' ]			= "Send System Admin a notice that a comment has been quarantined?";
	$settings[ 'spam_email_on_delete' ]				= "Send System Admin a notice that a comment has been deleted?";
	$settings[ 'spam_copy_address' ]				= "Send a copy of all spam notice emails to this address.<br /><br /><b><i>Leave blank if not required.</i></b><br /><br /><b>Note:</b> If this is filled in with a valid address then notice emails will be sent to it even if the settings <b>spam_email_on_quarantine</b> and <b>spam_email_on_delete</b> are set to off.";
	$settings[ 'spam_quarantine_timeout' ]			= "How many days to keep comments in the database before deleting them.<br /><br />Set to 0 to keep forever.<br /><br /><b>Note:</b> this will act retrospectively - change this to 10 and all existing comments 11 days old or more will be deleted, even if they were made before the setting was changed.";

	return $settings;

}



/*
 * 	A textual description of each setting's data-type
 * 
 * 	@return array
 */
function SETTINGS_get_story_settings_types()
{

	$settings		=	array();
	$settings[ 'internal_mailing_list_address' ] = 'text';
	$settings[ 'plain_text_mailing_list' ] = 'text';
	$settings[ 'number_story_votes_to_live' ] = 'int';
	$settings[ 'number_admin_votes_to_live' ] = 'int';
	$settings[ 'email_author_on_vote' ] = 'bool';
	$settings[ 'email_list_on_live' ] = 'bool';
	$settings[ 'email_list_on_issue_publish' ] = 'bool';
	$settings[ 'secret_story_ballot' ]= 'bool';
	$settings[ 'checkout_timeout' ] = 'int';
	$settings[ 'checkout_timeout_reduction' ] = 'int';
	$settings[ 'checkout_timeout_save' ] = 'bool';
	$settings[ 'number_keyword_for_votes' ] = 'int';
	$settings[ 'number_keyword_against_votes' ] = 'int';
	$settings[ 'spam_points_quarantine' ] = 'float';
	$settings[ 'spam_points_delete' ] = 'float';
	$settings[ 'spam_weight_hidden_fields' ] = 'float';
	$settings[ 'spam_keyword_modifier' ] = 'float';
	$settings[ 'spam_links_threshold' ] = 'int';
	$settings[ 'spam_links_score' ] = 'float';
	$settings[ 'spam_ip_filter_duration' ] = 'int';
	$settings[ 'spam_ip_filter_score' ] = 'float';
	$settings[ 'spam_email_on_quarantine' ] = 'bool';
	$settings[ 'spam_email_on_delete' ] = 'bool';
	$settings[ 'spam_copy_address' ] = 'text';
	$settings[ 'spam_quarantine_timeout' ] = 'int';

	return $settings;

}



/*
 *	Generates a string of text describing the current settings, one per line separated 
 *	by new line escape characters.
 *
 * 	@return string
 */
function SETTINGS_get_story_settings_as_text()
{

	//	Get the current settings...
	$settings		=	SETTINGS_get_story_settings_as_array();
	$output			=	array();
	
	//	Cycle through them, adding each key / value pair as an array element
	foreach ( $settings as $setting => $value )
	{

		$output[]		=	"'$setting' = '$value'";

	}

	//	Implode the array back to a string, adding new line chars
	return implode( "\r\n", $output );

}



/*
 * 	Format the settings as a comma separated string
 * 
 * 	@return string
 */
function SETTINGS_get_story_settings_as_csv()
{

	//	Get the settings as a string...
	$settings 	=	SETTINGS_get_story_settings_as_text();

	//	...and replace the new line chars with a comma
	return str_replace( "\r\n", ", ", $settings );

}






