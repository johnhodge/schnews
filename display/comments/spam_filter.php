<?php
function COMMENTS_is_spam_comment( $comment )
{
	
	
	/*
	 * Test inputs
	 */
	if ( !is_object( $comment ) ) 
	{
		
		$message 	=	"COMMENTS_is_spam_comment() called without an object as input";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'comments' );
		return false;
	
	}
	
	$score		=	0;
	$messages	=	array();
	
	
	
	/*
	 * 	Test for keyword matches
	 */
	if ( $keyword_results	=	COMMENTS_test_for_keywords( $comment ) )
	{
		
		$score		=	$score + $keyword_results[0];
		$messages[]	=	$keyword_results[1];
		
	}
	
	
	
	/*
	 * 	Test for IP hash matches
	 */
	if ( $ip_results	=	COMMENTS_is_ip_hash_flagged( $comment ) )
	{
		
		$score		=	$score + $ip_results[0];
		$messages[]	=	$ip_results[1];
		
	}
	
	
	
	/*
	 * 	Test for Hidden Fields
	 */
	if ( $fields	=	COMMENTS_test_for_hidden_fields() )
	{
		
		$score		=	$score + $fields[0];
		$messages[]	=	$fields[1];
		
	}
	
	
	
	/*
	 * 	Test for Links
	 */
	if ( $links		=	COMMENTS_test_for_links( $comment ) )
	{
		
		$score		=	$score + $links[0];
		$messages[]	=	$links[1];
		
	}
	
	
	
	/*
	 * 	test for session cookie
	 */
	if ( $cookie	=	COMMENTS_is_cookie_set( $comment ) )
	{
		
		$score		=	$score + $cookie[0];
		$messages[]	=	$cookie[1];
		
	}
	
	
	
	
	
	/*
	 * 	Test whether it is spam or not
	 */
	if ( $score == 0 )
	{
		
		return false;

	}
	else
	{
		
		return array ( 'score' => $score, 'reason' => implode( ", ", $messages ) );
		
	}
	
}




function COMMENTS_test_for_keywords( $comment )
{

	debug ( "COMMENTS_test_for_keywords() called", __FILE__, __LINE__ );
	
	/*
	 * Test inputs
	 */
	if ( !is_object( $comment ) ) return false;
	
	
	
	/*
	 * 	Break keyword list into separate keywords
	 */
	$path		=	DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'keywords';
	$list		=	file_get_contents( $path );
	$list		=	str_replace( array( "\r", "\n" ), '', nl2br( $list ) );
	$list		=	explode( '<br />', $list );
	debug( "COMMENTS_test_for_keywords() List count = " . count( $list ) );
	
	
	/*
	 * 	Replace punctuation with spaces, and then split appart into seperate words
	 */
	/*
	$spaces		=	array( ".", ",", "'", '"', ";", ":", "?", "!", "(", ")", "<br />" );
	$content	=	str_replace( array( "\r", "\n" ), '', nl2br( $comment->getContent() ) );
	$content	=	str_replace( $spaces, ' ', $content );
	$words		=	explode( " ", $content );
	debug( "COMMENTS_test_for_keywords() Word count = " . count( $words ) );
	*/
	
	$content	=	$comment->getContent();
	
	/*
	 * 	Test to see if any words are in the keyword list
	 */
	$hit_words	=	array();
	$hit_count	=	0;
	/*
	foreach ( $words as $word )
	{

		if ( in_array( $word, $list) )
		{

			$hit_count++;
			$hit_words[]	=	$word;
			
		}
		
	}
	*/
	
	foreach ( $list as $keyword )
	{
	
		if ( str_replace( $keyword, '', $content ) != $content )
		{
		
			$hit_words[]	=	$keyword;
			$hit_count++;
		
		}
	
	}
	
	
	
	$hit_words		=	array_unique( $hit_words );

	
	
	/*
	 * 	Generate an output array, where element 0 is the score and 
	 * 	element 1 is the message.
	 */
	if ( $hit_count == 0 )
	{
		
		debug( "COMMENTS_test_for_keywords() hit count = 0" );
		return false;
		
	}
	$score		=	$hit_count * spam_keyword_modifier;
	$message	=	"$score - Blocked keywords used ( ";
	$message	.=	implode( ", ", $hit_words );
	$message 	.=	")";
	
	return array( $score, $message );
	
}




function COMMENTS_is_ip_hash_flagged( $comment )
{

	debug ( "COMMENTS_is_ip_hash_flagged() called", __FILE__, __LINE__ );
	
	/*
	 * Test inputs
	 */
	if ( !is_object( $comment ) ) return false;	
	
	
	
	/*
	 * 	Work out the how far back we should test the IP hashes
	 */
	$date	=	date( "U" );
	$date	=	$date - ( spam_ip_filter_duration * 60 );
	$date	=	DATETIME_make_sql_datetime( $date );
	
	
	/*
	 * 	Query database
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT date
					FROM comments_ip_hashes
					WHERE 	
							ip_hash = '" . $comment->getIp_hash() . "'
						AND date > '$date'
					ORDER BY date
					LIMIT 0, 1 ";
	$mysql->query( $sql );
	
	
	
	/*
	 * 	If there isn't a result then the IP hash check has failed
	 */
	if ( $mysql->get_num_rows() != 1 )
	{

		debug( "COMMENTS_is_ip_hash_flagged() hit count = 0" );
		return false;
		
	}
	
	
	
	/*
	 * 	Put together an output
	 */
	$score		=	spam_ip_filter_score;
	$date		=	$mysql->get_row();
	$message	=	$score . " - IP hash has sent spam in the past " . spam_ip_filter_duration . " minutes";
	
	return array( $score, $message );
	
}






function COMMENTS_add_ip_hash( $comment )
{
	
	/*
	 * Test inputs
	 */
	if ( !is_object( $comment ) ) return false;

	
	
	/*
	 * 	Add to database
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	INSERT INTO comments_ip_hashes
					(
						ip_hash,
						date,
						spam_score
					)
					VALUES
					(
						'" . $mysql->clean_string( $comment->getIp_hash() ) . "',
						'" . DATETIME_make_sql_datetime() . "',
						'" . $comment->getSpam_score() . "'
					)";
	$mysql->query( $sql );
	
	
	$message	=	"Spam comment IP hash added to database. IP hash = " . $comment->getIp_hash();
	debug ( $message );
	LOG_record_entry( $message, 'comments' );	
	
	
	
	
}






function COMMENTS_test_for_hidden_fields()
{
	
	if ( !isset( $_POST['valid'] ) )	
	{
		
		return false;
		
	}
		
	
	$score		=	spam_weight_hidden_fields;
	$message	=	$score . " - Hidden fields ticked";
	return array( $score, $message );
	
}








function COMMENTS_test_for_links( $comment )
{

	debug ( "COMMENTS_test_for_links() called", __FILE__, __LINE__ );
	
	/*
	 * Test inputs
	 */
	if ( !is_object( $comment ) ) return false;
	
	
	
	/*
	 * 	Replace punctuation with spaces, and then split appart into seperate words
	 */
	$spaces		=	array( ",", "'", '"', ";", "!", "(", ")", "<br />" );
	$content	=	str_replace( array( "\r", "\n" ), '', nl2br( $comment->getContent() ) );
	$content	=	str_replace( $spaces, ' ', $content );
	$words		=	explode( " ", $content );
	debug( "COMMENTS_test_for_links() Word count = " . count( $words ) );
	
	
	
	$hit_words	=	array();
	$hit_count	=	0;
	foreach ( $words as $word )
	{
		
		if ( COMMENTS_is_link( $word ) )
		{
			
			$hit_words[]	=	$word;
			$hit_count++;
			
		}
		
	}

	
	
	/*
	 * 	Generate an output array, where element 0 is the score and 
	 * 	element 1 is the message.
	 */
	if ( $hit_count == 0 )
	{
		
		debug( "COMMENTS_test_for_links() hit count = 0" );
		return false;
		
	}
	
	if ( $hit_count <= spam_links_threshold )
	{
		
		debug ( "COMMENTS_test_for_links() below threshold" );
		return false;
		
	}
	
	$score		=	( $hit_count - spam_links_threshold ) * spam_links_score;
	$message	=	"$score - Too many links used ( ";
	$message	.=	implode( ", ", $hit_words );
	$message 	.=	")";
	
	return array( $score, $message );
	
}







function COMMENTS_is_link( $string )
{
	
	/*
	 * 	Test input
	 */
	if ( !is_string( $string ) || empty( $string ) ) return false;

	
	
	/*
	 * 	Make sure it has at least one .
	 */
	if ( str_replace( '.', '', $string ) == $string ) return false;
	
	
	
	/*
	 * 	Does it contain a 'www.' or 'http://'
	 */
	if ( str_replace( array( 'www.', 'http://' ), '', $string ) == $string ) return false;
	
	
	
	/*
	 * 	Else lets assume it is a link
	 */
	return true;	
	
}





function COMMENTS_is_cookie_set( $comment )
{
	
	$fail		=	array( 100, "100 - Cookie not set, proving that the comment form was not used" );
	$story 		=	new story( $comment->getStory_id() );
	
	
	/*
	 * 	Is the session cokkie even set?
	 */
	if ( !isset( $_SESSION['comments_page_hash_' . $story->get_id() ] ) )
	{
		
		return $fail;	
		
	}
	
	
	
	/*
	 * 	Does it contain the correct value
	 */
	if ( $_SESSION['comments_page_hash_' . $story->get_id()] != STORIES_make_comment_hash( $story ) )
	{
		
		return $fail;
		
	}
	
	return false;	
	
}








