<?php 
class story
{
	//	SQL data
	protected $id;
	protected $story;

	//	Story Content
	protected $issue;
	protected $headline;
	protected $subheadline;
	protected $summary;
	protected $rss;
	protected $twitter;
	protected $author;
	
	protected $graphic_sm;
	protected $graphic_lg;
	protected $graphic_vlg;
	
	protected $additional_graphic_1;
	protected $additional_graphic_2;
	protected $additional_graphic_3;
	protected $additional_graphic_4;
	
	protected $additional_graphic_caption_1;
	protected $additional_graphic_caption_2;
	protected $additional_graphic_caption_3;
	protected $additional_graphic_caption_4;
	
	
	protected $latest_revision; // This should be an object of a revision

	//	Story Meta-Data
	protected $date_added;
	protected $live;
	protected $date_live;
	protected $position;
	protected $size;
	protected $sticky;
	protected $hide;
	protected $hits;

	//	Comments
	protected $hide_comments;
	protected $lock_comments;

	


	/*
	 * 	Populates the object with the initial data.
	 *
	 * 	@param id [INT]			//	The story id in the database.
	 */
	function __construct( $id )
	{

		debug( "New Story Instantiated. ID = $id", __FILE__, __LINE__ );

		
		
		/*
		 * 	Test inputs
		 */ 
		if (!CORE_is_number( $id ) ) 
		{
			
			debug ("Story class :: id is not a number");
			return false;
			
		}

		
		$this->id		=	$id;
		

		/*
		 * 	Load all the data for this story from the database
		 */
		if ( !$this->get_story_data() )
		{

				return false;

		}

		
		
		
		return true;

	}



	
	
	
	/*
	 * 	Grabs the story data from the DB and populates the local properties
	 */
	protected function get_story_data()
	{

		//	Test inputs
		if (!CORE_is_number( $this->id ) )
		{

			/*
			 * If ID is not a number by this stage then we have a serious error
			 */
			$message	=	"story->get_story_data() called without story->id being properly set.";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;

		}

		
		
		/*
		 * 	Do the database query to get the data
		 */
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT * FROM stories WHERE id = " . $this->id;
		$mysql->query( $sql );

		
		
		/*
		 * 	Make sure only 1 result was returned
		 */
		if ( $mysql->get_num_rows() != 1 && $this->id != 0)
		{

			/*
			 * 	If there is not 1 result then we have a pretty serious programming error or a hack attampt
			 */
			$message 	=	"SQL query in story->get_story_data() did not yield 1 result. Query = " . $sql;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;

		}

		$story	=	$mysql->get_row();
		$this->story 	=	$story;

		
		
		//	Populate local data
		$this->issue			=	$story[ 'issue' ];
		$this->headline			=	$story[ 'headline' ];
		$this->subheadline		=	$story[ 'subheadline' ];
		$this->summary			=	$story[ 'summary' ];
		$this->rss				=	$story[ 'rss' ];
		$this->twitter			=	$story[ 'twitter' ];
		$this->author			=	$story[ 'author' ];
		$this->graphic_sm		=	$story[ 'graphic_sm' ];
		$this->graphic_lg		=	$story[ 'graphic_lg' ];
		$this->graphic_vlg		=	$story[ 'graphic_vlg' ];
		$this->date_added		=	$story[ 'date_added' ];
		$this->live				=	$story[ 'live' ];
		$this->date_live		=	$story[ 'date_live' ];
		$this->position			=	$story[ 'position' ];
		$this->size				=	$story[ 'size' ];
		$this->sticky			=	$story[ 'sticky' ];
		$this->hide				=	$story[ 'hide' ];
		$this->hits				=	$story[ 'hits' ];
		$this->hide_comments	=	$story[ 'hide_comments' ];
		$this->lock_comments	=	$story[ 'lock_comments' ];

		$this->additional_graphic_1	=	$story['additional_graphic_1'];
		$this->additional_graphic_2	=	$story['additional_graphic_2'];
		$this->additional_graphic_3	=	$story['additional_graphic_3'];
		$this->additional_graphic_4	=	$story['additional_graphic_4'];

		$this->additional_graphic_caption_1	=	$story['additional_graphic_caption_1'];
		$this->additional_graphic_caption_2	=	$story['additional_graphic_caption_2'];
		$this->additional_graphic_caption_3	=	$story['additional_graphic_caption_3'];
		$this->additional_graphic_caption_4	=	$story['additional_graphic_caption_4'];
		
		/*
		 * Get the latest revision
		 */
		$rev_id		=	$this->get_latest_revision_id();
		$this->latest_revision	=	new revision( $rev_id );

		
		
		return true;

	}
	
	
	
	
	protected function get_latest_revision_id()
	{
		
		$sql		=	"	SELECT id
							FROM revisions
							WHERE story_id = " . $this->id . "
							ORDER BY date_saved DESC
							LIMIT 0, 1";
		$mysql		=	new mysql_connection();
		$mysql->query( $sql );
		
		if ( !$res		=	$mysql->get_row() )
		{
			
			return 0;
			
		}
		else
		{
			
			return $res['id'];
		
		}
		
	}






	
	
	
	
	
	
	/*
	 * 	==================================================
	 * 					     GETTERS
	 */
	public function get_id()
	{
		return $this->id;
	}
	public function get_issue()
	{
		return $this->issue;
	}
	public function get_headline()
	{
		return $this->headline;
	}
	public function get_subheadline()
	{
		return $this->subheadline;
	}
	public function get_summary()
	{
		return $this->summary;
	}
	public function get_rss()
	{
		return $this->rss;
	}
	public function get_twitter()
	{
		return $this->twitter;
	}
	public function get_author()
	{
		return $this->author;
	}
	public function get_date_added()
	{
		return $this->date_added;
	}
	public function get_date_live()
	{
		return $this->date_live;
	}
	public function get_position()
	{
		return $this->position;
	}
	public function get_size()
	{
		return $this->size;
	}
	public function get_sticky()
	{
		return $this->sticky;
	}	
	public function get_hide()
	{
		return $this->hide;
	}
	public function get_live()
	{
		return $this->live;
	}
	public function get_lock_comments()
	{
		return $this->hide_comments;
	}
	public function get_hits()
	{
		return $this->hits;
	}

	public function get_latest_revision()
	{
		return $this->latest_revision;
	}
	
	
	public function get_content()
	{
		
		return $this->latest_revision->get_content();
		
	}
	
	public function get_graphic_sm()
	{
		
		return $this->graphic_sm;
		
	}
	
	public function get_graphic_lg()
	{

		return $this->graphic_lg;
		
	}
	
	public function get_graphic_vlg()
	{

		return $this->graphic_vlg;
		
	}
	
	public function get_additional_graphic_1()
	{

		return $this->additional_graphic_1;
		
	}
	public function get_additional_graphic_2()
	{

		return $this->additional_graphic_2;
		
	}
	public function get_additional_graphic_3()
	{

		return $this->additional_graphic_3;
		
	}
	public function get_additional_graphic_4()
	{

		return $this->additional_graphic_4;
		
	}

	public function get_additional_graphic_caption_1()
	{

		return $this->additional_graphic_caption_1;
		
	}
	public function get_additional_graphic_caption_2()
	{

		return $this->additional_graphic_caption_2;
		
	}
	public function get_additional_graphic_caption_3()
	{

		return $this->additional_graphic_caption_3;
		
	}
	public function get_additional_graphic_caption_4()
	{

		return $this->additional_graphic_caption_4;
		
	}

}