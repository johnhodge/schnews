<?php
class revision
{
	
	//	SQL Data
	protected $id;
	protected $story_id;
	protected $revision;
	
	//	Content
	protected $content;
	
	//	Meta Data
	protected $word_count;
	
	//	Comments
	protected $external_comment;
	
	
	
	
	
	
	function __construct( $id )
	{
		
		debug ( "New Revision object instantiated. ID = $id", __FILE__, __LINE__ );
		
		
		
		/*
		 * 	Test inputs
		 */
		if (!CORE_is_number( $id ) )	return false;
		
		
		$this->id 	=	$id;
		
		/*
		 * 	Load the revison data from the database
		 */
		if ( !$this->get_revision_data() )
		{
				
			return false;
			
		}
		
		
		
		return true;
		
	}
	
	
	
	
	
	
	protected function get_revision_data()
	{
		
		
		
		//	Test assumptions
		if ( !CORE_is_number( $this->id ) )
		{
			
			/*
			 * If ID is not a number by this stage then we have a serious error
			 */
			$message	=	"revision->get_revision_data() called without revision->id being properly set.";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT * FROM revisions WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		
		
		/*
		 * 	Make sure only 1 result was returned
		 */
		if ( $mysql->get_num_rows() != 1 && $this->id != 0)
		{

			/*
			 * 	If there is not 1 result then we have a pretty serious programming error or a hack attampt
			 */
			$message 	=	"SQL query in revision->get_story_data() did not yield 1 result. Query = " . $sql;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		
		/*
		 * 	Populate Local Properties
		 */
		$rev						=	$mysql->get_row();
		$this->revision				=	$rev;
		
		$this->story_id				=	$rev[ 'story_id' ];
		$this->content				=	$rev[ 'content' ];
		$this->word_count			=	$rev[ 'word_count' ];
		$this->external_comment		=	$rev[ 'external_comment' ];
		
		
		
		return true;
		
	}
	
	

	
	
		
	
	
	
	
	
	
	/*
	 * ======================================================
	 * 				GETTERS
	 */
	public function get_id()
	{
		return $this->id;
	}
	public function get_story_id()
	{
		return $this->story_id;
	}
	public function get_content()
	{
		return $this->content;		
	}
	public function get_word_count()
	{
		return $this->word_count;
	}
	public function get_external_comment()
	{
		return $this->external_comment;
	}
	
	
	
	
	
	
	
	
	
	
}







