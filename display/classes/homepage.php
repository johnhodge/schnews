<?php



class homepage
{
	
	protected $template;
	
	protected $list_of_stories;
	
	
	
	function __construct( $template )
	{
	
		/*
		 * Test inputs
		 */
		if ( !is_string( $template ) || trim( $template ) == '' )
		{
			debug( "homepage->__construct :: template not a valid string", __FILE__, __LINE__ );
			return false;
		}
			
		$this->template 	=	$template;
		debug( "homepage_.__construct :: object instantiated", __FILE__, __LINE__ );
		return true;
				
	}
	
	
	
	
	private function update_template()
	{
		
		$this->template 	=	str_replace( '***HOMEPAGE_LIST_OF_STORIES***', $this->list_of_stories, $this->template );		
		$this->template		=	str_replace( '***HOMEPAGE_P&P***', HOMEPAGE_include_pap(), $this->template );	
		$this->template		=	str_replace( '***HOMEPAGE_SCHMOVIES_SCREENINGS***', HOMEPAGE_include_schmovies(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_ADVERT_ONE***', HOMEPAGE_include_advert_one(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_ADVERT_TWO***', HOMEPAGE_include_advert_two(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_ADHOC_CONTENT_ONE***', HOMEPAGE_include_adhoc_content_one(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_ADHOC_CONTENT_TWO***', HOMEPAGE_include_adhoc_content_two(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_SCHNEWS_IN_BRIEF***', HOMEPAGE_include_schnews_in_brief(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_MAIN_GRAPHIC***', HOMEPAGE_include_main_graphic(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_MERCH_LINKS***', HOMEPAGE_include_merch_links(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_CRAP_ARREST***', HOMEPAGE_include_crap_arrest(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_AND_FINALLY***', HOMEPAGE_include_and_finally(), $this->template );
		$this->template		=	str_replace( '***HOMEPAGE_DISCLAIMER***', HOMEPAGE_include_disclaimer(), $this->template );
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 *		GETTERS	 
	 */
	public function get_homepage()
	{

		$this->update_template();
		
		return $this->template;		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*	
	 * 	SETTERS
	 */
	
	public function set_list_of_stories( $list )
	{
		
		$this->list_of_stories 	=	$list;
		
	}
	
	
	
	
}