<?php 

session_start();


/*
 * 	Prepare instance config
 */
if ( isset( $display_config['silent'] ) && $display_config['silent'] == true )
{
	
	define ( 'DISPLAY_SILENT', true );
	
}
else
{
	
	define ( 'DISPLAY_SILENT', false );
	
}



/*
 * Display oc type
 */
if ( !DISPLAY_SILENT )
{
	?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php 
}





/*
 * 		SchNEWS Story Display System
 * 
 * 		Central Controller
 * 
 *		@author: 	Andrew Winterbottom
 *		@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 *		@license:	copyleft / GPL v2 (If you want source code ask the addresses above)
 *		
 *
 *		ABOUT THIS SOFTWARE
 *		-------------------
 *
 *		*	This "Software" is defined as all the php, css, javascript and other files in this directory and all
 *			child directories. By extension it also implies the structure of the database, currently named 'schnews_storyadmin'.
 *
 *			It is considered a separate piece of software to the StoryAdmin software, although they both occupy the same website
 *			and database.
 *
 *		*	Its purpose is to provide all the functions necessary to display stories, issues, summary and homepage entries for the 
 *			www.schnews.org.uk website. While the storyadmin section of the SchNEWS system could be generalised to an all purpose 
 *			CMS with little effort, this display section is specific to the SchNEWS site and there are no plans to 
 *
 *		*	To invoke this software the calling page should include/require this file, and only ever this file.
 *		
 *			It should never call on of the child files on its own.
 *
 *		*	Furthermore, the calling page should only ever call the functions, not the class. The functions act as wrapper for
 *			the classes. Calling the classes will not be supported, so expect things to be unpredictable or buggy.
 *
 *
 *		ABOUT THIS FILE
 *		---------------
 *
 *		*	This file call (require()) all the classes, functions and scripts required to display stories and associated elements
 *			to the browser.
 *
 *		*	This is the only file that a calling page should every interface with. The calling page should never calling the 
 *			child files.
 *
 *		*	This file should be called once and only once per page (or at least per execution path). If called twice it will generate
 *			errors.
 * 
 */



/*
 * 	 	LOCAL CONFIG
 * 
 * 		This small bit of local config is necessary to locate this file on the server. All other files (including the 
 * 		local config.php) can then be called relative to this file.
 * 
 * 		You should not change this in any circumstance
 */

/*	
 * 		DISPLAY_SITE_ROOT
 * 
 * 		Slahs-terminated
 * 
 * 		This should be used as the refernce for all includes for the display system. E.g. when including the config.php file below, it
 * 		should be referenced as: DISPLAY_SITE_ROOT . "config.php"
 */
define ( 'DISPLAY_SITE_ROOT', dirname( __FILE__ ) . DIRECTORY_SEPARATOR );




/*
 *		INCLUDE LOCAL CONFIG
 *
 * 		This sets a few other config setting to do with how the software functions on the server.
 * 
 * 		Settings controlling the appearance of stories and other elements are controlled via the 
 * 		StoryAdmin software.	
 */
require DISPLAY_SITE_ROOT . "config.php";





/*
 * 		SITE IMAGE URL
 * 
 * 		Where to find the main images associated with the homepage and the stories
 */
define ( 'SITE_IMAGES_URL', str_replace( "/display", '', DISPLAY_SITE_URL ) . 'images/' );




	


/*
 * 		DISPLAY BOILDERPLATE INFO TO THE BROWSER
 * 
 * 		A short comment in HTML to show what is happening for debugging purposes
 */
if ( !DISPLAY_SILENT )
{
	require DISPLAY_SITE_ROOT . 'inc' . DIRECTORY_SEPARATOR . 'boilerplate.php';
}







/*
 * 		INCLUDE PHP LIBRARY
 * 
 * 		This contains abstraction wrappers and containers for various common tasks such as SQL, datetime, email,
 * 		logging, etc.
 */
require DISPLAY_SITE_ROOT . 'lib' . DIRECTORY_SEPARATOR . 'aw_lib.php';




/*
 * 		INCLUDE CLASSES
 * 
 * 		Requires a simple file that includes all the classes.
 */
require DISPLAY_SITE_ROOT . 'classes' . DIRECTORY_SEPARATOR . 'classes.php';




/*
 * 		INCLUDE FUNCTIONS
 * 
 * 		Requires a simple file that includes all the functions.
 */
require DISPLAY_SITE_ROOT . 'functions' . DIRECTORY_SEPARATOR . 'functions.php';




/*
 * 		INCLUDE COMMENTS
 * 
 * 		Requires a simple file that includes all the comments handlers.
 */
require DISPLAY_SITE_ROOT . 'comments' . DIRECTORY_SEPARATOR . 'comments.php';





/*
 * 		LOAD THE SETTINGS
 */
SETTINGS_load_homepage_settings();



debug( "Display framework loaded successfully" );


?>