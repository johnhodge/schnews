<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS :: Subscribe</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/archiveIndex.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
	<?php
		//	Include Main Left Bar
		 <!--#include virtual="../inc/leftBarMain.php"-->
	?>
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="main_page_title" id="main_page_title">
		Subscribe
	</div>
	<div style="margin-left: 15px; margin-right: 6px; border-left: 2px solid #e6e6e6; border-right: 2px solid #e6e6e6; padding-left: 18px; padding-right: 18px">
	<p>
	</p><h2 style="margin-bottom: 10px">SchNEWS PDF Files</h2>
	<div style="font-weight: bold">Now you can print yer own SchNEWS!</div>
	
            <p>SchNEWS is now available as a &#39;PDF&#39; file, which looks the same
              as the printed version. </p>
              <p>So you can photocopy it and distribute it in your area... </p>
              <p>You can view and print PDF (Portable Document Format) files using Adobe Acrobat Reader. </p>
              <p>For This week&#39;s SchNEWS in PDF Format go to the latest issue and click on the PDF link near the top. </p>
            <p>When you print it, switch Shrink to Fit OFF in the Print dialogue
              box, so it fills an A4 page.</p>
              <p>Please e-mail <del>webmaster@schnews.org.uk</del><A Href="mailto:schnews@riseup.net"> schnews@riseup.net</A> if you have any problems or suggestions. We&#39;d like to hear from you if you&#39;ve used the PDF file successfully, especially if you&#39;ve made copies for friends! </p>
            <p><a href="http://www.adobe.com/supportservice/custsupport/SOLUTIONS/150d6.htm" target="_blank">Troubleshooting
              Printing Problems in Acrobat for Windows</a><br>
              <a href="http://www.adobe.com/supportservice/custsupport/SOLUTIONS/a9da.htm" target="_blank">Troubleshooting
              Printing Problems in Acrobat for Mac OS </a></p>
              <br><p></p><h3>Where to get Adobe Acrobat Reader</h3>
              The software is free of charge, and can be downloaded from <a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html" target="_blank">Adobe&#39;s
              website</a>. The PDF files on the SchNEWS site can be viewed with
              Acrobat 3 or later. The current version is Acrobat 9...but it
              doesn&#39;t make SchNEWS look any better! 
            <p>You can also use <a href="http://www.cs.wisc.edu/%7Eghost/" target="_blank">Ghostscript</a>
              to view and print PDF files. </p>
              <br><p></p><h3>Notes</h3>There&#39;s no white space around the edges of the text. We&#39;ve &quot;cropped&quot; the document very tightly, because if we didn&#39;t, what with SchNEWS being so tall, the &#39;Shrink to Fit&#39; option would mean the back page would get kinda illegible on anything but a high-end laser printer.
              It doesn&#39;t look great on screen, but at least it&#39;s good on paper. 
              <p>Text can&#39;t easily be copied for use in other programs. The PDF format wasn&#39;t designed with this in mind. If you want to copy text, do it from the normal (HTML) version. </p>
              <p>None of the Web or e-mail addresses are Weblinks. We won&#39;t bother doing this, because users can always get the HTML version from the Web site anyway, and the links in here are more reliable than the ones in the DTP&#39;d version. The Web address at the top of the page is a Weblink, though. </p>
              <p>Some of the &#39;Base 13&#39; fonts are embedded. We&#39;re just playing safe.<br></p>
		</div>
</div>
<div class="mainBar_right">
	<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
