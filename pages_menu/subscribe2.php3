<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<html>
<head>
<title>:: SchNEWS :: Subscribe For Text Only</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel=stylesheet href="/schnews.css" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
</head>

<body bgcolor="#FFFFCC" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" link="#FF6600" vlink="#FF6600" alink="#FF6600" background="../images_main/background.gif">
<a name="begin"></a> 
<table width="760" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#FFFFFF"> 
    <td colspan="3"><a href="/index.htm"><img src="../images_main/schnews.gif" width="292" height="90" border="0"></a></td>
  </tr>
  <tr> 
    <td><img src="../images_main/spacer_header.gif" width="120" height="52"></td>
    <td colspan="2"><img src="../images_menu/subscribe-over.gif" width="90" height="32"><a href="/archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',0)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32"></a><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',0)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32"></a><a href="/links/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',0)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32"></a><a href="/diyguide/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',0)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32"></a><a href="/monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',0)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32"></a><a href="http://www.brightonabc.org.uk/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('prisoners','','../images_menu/prisoners-over.gif',0)"><img name="prisoners" border="0" src="../images_menu/prisoners.gif" width="90" height="32"></a><br>
      <img src="../images_menu/description.gif" width="640" height="20"></td>
  </tr>
  <tr> 
    <td valign="top">
<div align="center"></div>
    </td>
    <td valign="top"> 
      <table width="490" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="../images_main/spacer_border.gif" width="10" height="10"></td>
          <td bgcolor="#FFFFCC"><img src="../images_main/spacer.gif" width="468" height="10"></td>
          <td bgcolor="#FFFFCC"><img src="../images_main/spacer.gif" width="12" height="10"></td>
        </tr>
        <tr> 
          <td valign="bottom" background="../images_main/spacer_border.gif"><img src="../images_main/spacer_black.gif" width="10" height="10"></td>
          <td valign="top" bgcolor="#FFFFCC"><b> 
            <a href="/index.htm">Home</a> | Subscribe For Text Only</b>
            <table width="380" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td>&nbsp;</td>
                <td><img src="../images_main/spacer.gif" width="10" height="10"></td>
                <td><img src="../images_main/spacer.gif" width="306" height="1"></td>
                <td rowspan="3">&nbsp;</td>
              </tr>
                <td colspan="3">
                  <!-- BEGIN CONTENT -->
                  <?
                  error_reporting(E_ERROR+E_WARNING);
                  if (strpos($address, "@")) {
                  	if ($subunsub=="subscribe"){
                  		$result = mail("schnews-l-request@gn.apc.org", "", "subscribe address=$address","From: $address");
                  		?>
                  		<H1>You've <strong>ALMOST</strong> been subscribed to SCHNEWS-L</H1>
                  You'll be sent an email asking you to confirm your subscription
                  in a few minutes. If you follow the instructions in it, which 
                  basically means replying to it, you'll get the next issue on Friday or soon after.
                  <P>If you <I>don't</I> get a message, try again, checking you got
                  your email address right.
                  <P>
                  <a href="/index.htm"><strong>Back to SchNEWS home page</strong></A>
                  	<?
                  	}else if ($subunsub=="unsubscribe"){
                  		$result = mail("schnews-l-request@gn.apc.org", "", "unsubscribe schsquyea $address","From: webmaster@schnews.org.uk");
                  		?>
                  		<H1>You've been unsubscribed from SCHNEWS-L</H1>
                  
                  <P>If you keep on getting messages, try again, checking you got
                  your email address right. If that still fails sent an email to 
                  the address at the bottom of the page with details of your desire.
                  <P>
                  <a href="/index.htm"><strong>Back to SchNEWS home page</strong></A>
                  		<?
                  	} else {
                    ?>
                  <H1>Try Again</H1>
                  <B>We don't think you selected one of the options, do you want to subscribe or unsubscribe ?
                  <form action="/pages_menu/subscribe2.php3" name="frmSubText">
    			        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td><img src="../images_main/txt_dwnld.gif" width="17" height="22"></td>
                      <td>&nbsp;</td>
                      <td><font face="Arial, Helvetica, sans-serif" size="2">Subscribe 
                        / Unsubscribe to <b>text only</b> SchNEWS email:</font></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><font face="Arial, Helvetica, sans-serif" size="2"> 
                        <input type="text" size="36" name="address" value="<? print($address) ?>">
                        </font></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                        <td><font face="Arial, Helvetica, sans-serif" size="2"> 
                          <input type="radio" name="subunsub" value="subscribe" id="sub">
                          <label for="sub">Subscribe</label> <img src="../images_main/spacer.gif" width="10" height="8">
                          <input type="radio" name="subunsub" value="unsubscribe" id="unsub">
                          <label for="unsub">Unsubscribe</label></font></td>
                      <td><a href="javascript:document.frmSubText.submit()"><img src="../images_main/go.gif" width="47" height="31" border="0"></a></td>
                    </tr>
                    <tr> 
                      <td colspan="4"><img src="../images_main/spacer.gif" width="1" height="5"></td>
                    </tr>
                  </table>
    			        </form>
        <P>Give up: <a href="/index.htm">Back to the home page</A>
                    <?}
                  } else {
                  ?>
                  <H1>Try Again</H1>
                  <B>We don't think "<? echo $address; ?>" looks like an email address.
                  Did you type anything in at all?
                  <form action="/pages_menu/subscribe2.php3" name="frmSubText">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
            			  <tr> 
                      <td><img src="../images_main/txt_dwnld.gif" width="17" height="22"></td>
                      <td>&nbsp;</td>
                      <td><font face="Arial, Helvetica, sans-serif" size="2">Subscribe 
                        / Unsubscribe to <b>text only</b> SchNEWS email:</font></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td><font face="Arial, Helvetica, sans-serif" size="2"> 
                        <input type="text" size="36" name="address" value="Your email address here">
                        </font></td>
                    </tr>
                    <tr> 
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                        <td><font face="Arial, Helvetica, sans-serif" size="2"> 
                          <input type="radio" name="subunsub" value="subscribe" id="sub">
                          <label for="sub">Subscribe</label> <img src="../images_main/spacer.gif" width="10" height="8">
                          <input type="radio" name="subunsub" value="unsubscribe" id="unsub">
                          <label for="unsub">Unsubscribe</label></font></td>
                      <td><a href="javascript:document.frmSubText.submit()"><img src="../images_main/go.gif" width="47" height="31" border="0"></a></td>
                    </tr>
                    <tr> 
                      <td colspan="4"><img src="../images_main/spacer.gif" width="1" height="5"></td>
                    </tr>
                    </table>
          			  </form>
                  <P>Give up: <a href="/index.htm"><b>Back to the home page</b></A>
                  <?
                  }
                  ?>
                </td>
              <tr> 
                <td colspan="4"><img src="../images_main/spacer.gif" width="1" height="10"></td>
              </tr>
              <tr> 
                <td colspan="3"><a href="pdf_notes.html"><b>Click here</b></a> 
                  for an explanation of the difference between the <b>text only</b> 
                  and <b>PDF</b> versions of SchNEWS.</td>
                <td>&nbsp;</td>
              </tr>
           </table>
            <p align="left"><img src="../images_main/spacer_black.gif" width="468" height="10"></p>
          </td>
          <td valign="top" bgcolor="#FFFFCC">&nbsp;</td>
        </tr>
        <tr> 
          <td valign="bottom">&nbsp;</td>
          <td valign="top"><b><img src="../images_main/spacer.gif" width="1" height="10"><br>
            </b><font face="Arial, Helvetica, sans-serif" size="2"><font size="1">SchNEWS, 
            PO Box 2600, Brighton, BN2 0EF, England<br>
            Phone: +44 (0)1273 685913<br>
            email: <a href="mailto:schnews@riseup.net">schnews@brighton.co.uk</a></font></font>
<p><font face="Arial, Helvetica, sans-serif" size="2"><font size="1">@nti 
              copyright - information for action - copy and distribute!</font></font></p>
            </td>
          <td valign="top">&nbsp;</td>
        </tr>
      </table>
    </td>
    <td valign="top">
      <table width="150" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><img src="../images_main/spacer.gif" width="10" height="10"></td>
          <td bgcolor="#FFFFCC"><img src="../images_main/spacer.gif" width="140" height="10"></td>
        </tr>
        <tr> 
          <td bgcolor="#FFFFCC">&nbsp;</td>
          <td bgcolor="#FFFFCC">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</body>
</html>