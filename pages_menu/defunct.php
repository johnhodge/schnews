<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS :: About Us</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="SchNEWS, direct action, Brighton, information for action, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, Climate Change, no borders, Simon Jones, protest, privatisation, neo-liberal, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, sustainability, reclaim the streets, RTS, food miles, copyleft, anti-roads, anti-nuclear, anti-war, stopthewar, squatting, subvertise, satire, alternative news, Noborder, Rising Tide, PGA, anti-terrorism, Radical Routes, Palestine occupation, Indymedia, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/archiveIndex.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
		 <!--#include virtual="../inc/leftBarMain.php"-->
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="main_page_title_new" id="main_page_title" style="margin-left: 15px;">Err - whoops - this is a dead link</div>
	<div style="margin-left: 15px; margin-right: 6px;  ">
<!-- GRAPHIC -->
<div style="float:right; margin-left:10px;"><br>
<img alt="Miffy" border="0" src="../images/miffy-250.jpg" width="180">
</div>
<!-- GRAPHIC - END -->
	<p>This is an archive site, not currently being updated. You clicked on something that is now either broken or no longer functioning.</p>

	
    </div>
</div>
<!--
<div class='mainBar_right'>
	<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
</div>
-->
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->
</div>
</div>
</body>
</html>
