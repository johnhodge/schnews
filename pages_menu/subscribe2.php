<?php
	if (isset($_POST['address']))	{
		$address	=	$_POST['address'];
		$subunsub	=	$_POST['subunsub'];
	}	else	{
		header('location: http://www.schnews.org.uk/pages_menu/subscribe.php');
	}
?>
<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS :: Subscribe</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../archiveIndex.css" rel="stylesheet" type="text/css">
<link href="../issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
	<?php
		//	Include Main Left Bar
		 <!--#include virtual="../inc/leftBarMain.php"-->
	?>
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="main_page_title" id="main_page_title">
		Subscribe
	</div>
	<div style="text-align: left; margin-left: 15px; margin-right: 6px; border-left: 2px solid #e6e6e6; border-right: 2px solid #e6e6e6; padding-left: 18px; padding-right: 18px">
	<!-- BEGIN CONTENT -->
	<?
    $settings	=	getSettings();
    error_reporting(E_ERROR+E_WARNING);
    //	Make sure a valid email address was entered...
	if (is_email_address($address))	{
		//	If subscribing...
		if ($subunsub=="subscribe"){
			//	Send email to mailing list manager
 			$result = mail('schnews-txt-request@lists.aktivix.org', "", "subscribe address=$address","From: $address");
 			?>
       		<h1>You&#39;ve <u>ALMOST</u> been subscribed to SCHNEWS-TXT</h1>
            You&#39;ll be sent an email asking you to confirm your subscription in a few minutes.
            If you follow the instructions in it, which basically means replying to it, you&#39;ll get the next issue on Friday or soon after.
            <p>If you <i>don&#39;t</i> get a message, try again, checking you got your email address right.
            <br><br>
            </p><p><a href="/index.htm"><strong>Back to SchNEWS home page</strong></a>
 		<?
 		//	If unsubscribing....
        }else if ($subunsub=="unsubscribe"){
        	//	Send email to mailing list manager
            $result = mail('schnews-txt-request@lists.aktivix.org', "", "unsubscribe utgaipukka $address","From: webmaster@schnews.org.uk");
            ?>
            </p><h1>You&#39;ve been unsubscribed from SCHNEWS-TXT</h1>
             <p>If you keep on getting messages, try again, checking you got your email address right.
             If that still fails sent an email to the address at the bottom of the page with details of your desire.
             <br><br>
             </p><p><a href="/index.htm"><strong>Back to SchNEWS home page</strong></a>
		<?
		//	Otherwise they must have forgotton to tick the sub or unsub radio button...
        } else {
        ?>
        	</p><h1>Try Again</h1>
            <b>We don&#39;t think you selected one of the options, do you want to subscribe or unsubscribe ?</b>
            <br><br>
            <!-- Plain Text Subscription Form -->
			<div id="text_subscribe" style="padding: 20px; border: 2px solid #e6e6e6; margin-top:8px; margin-bottom: 8px">
		    	<h2 style="text-align:right; padding: 0px; margin: 0px">Plain Text</h2>
		    	<form action="/pages_menu/subscribe2.php" method="post" name="frmSubText">
		        	<img alt="plain text mailing list" height="22" src="../images_main/txt_dwnld.gif" width="17">
		            Subscribe / Unsubscribe to <b>text only</b> SchNEWS email: <br>
		            <input maxlength="60" name="address" size="36" style="margin-top: 10px" type="text" value="&lt;?=$address?&gt;">
		 			<br>
		 			<div style="position: relative; top: -8px">
		            <input id="sub" name="subunsub" type="radio" value="subscribe">
		            <label for="sub" style="color:red; font-weight: bold; font-size: 16px">Subscribe</label>
		            <input id="unsub" name="subunsub" type="radio" value="unsubscribe">
		            <label for="unsub" style="color:red; font-weight: bold; font-size: 16px">Unsubscribe</label>
		            <a href="javascript:document.frmSubText.submit()">
		            	<img alt="subscribe to plain test mailing list" border="0" height="31" src="../images_main/go.gif" style="padding-left:140px" width="47">
		            </a>
		            </div>
				</form>
			</div>
			<br><br>
        	<p>Give up: <a href="/index.htm">Back to the home page</a>
        <?
        }
		} 		// Closed the if(is_email_address.... loop
		//	The email address entered didn't look like a valid email address...
		else {
		?>
			</p><h1>Try Again</h1>
			<p><b>We don&#39;t think &quot;<? echo $address; ?>&quot; looks like an email address.</b>
            </p><p>Did you type anything in at all?
			<br><br>
            <!-- Plain Text Subscription Form -->
			</p><div id="text_subscribe" style="padding: 20px; border: 2px solid #e6e6e6; margin-top:8px; margin-bottom: 8px">
		    	<h2 style="text-align:right; padding: 0px; margin: 0px">Plain Text</h2>
		    	<form action="/pages_menu/subscribe2.php" method="post" name="frmSubText">
		        	<img alt="plain text mailing list" height="22" src="../images_main/txt_dwnld.gif" width="17">
		            Subscribe / Unsubscribe to <b>text only</b> SchNEWS email: <br>
		            <input maxlength="60" name="address" size="36" style="margin-top: 10px; border: 3px solid red; padding:3px" type="text" value="&lt;?=$address?&gt;">
		 			<br>
		 			<div style="position: relative; top: -8px">
		            <input id="sub" name="subunsub" type="radio" value="subscribe">
		            <label for="sub">Subscribe</label>
		            <input id="unsub" name="subunsub" type="radio" value="unsubscribe">
		            <label for="unsub">Unsubscribe</label>
		            <a href="javascript:document.frmSubText.submit()">
		            	<img alt="subscribe to plain test mailing list" border="0" height="31" src="../images_main/go.gif" style="padding-left:140px" width="47">
		            </a>
		            </div>
				</form>
			</div>
			<br><br>
			<p>Give up: <a href="/index.htm"><b>Back to the home page</b></a>
		<?
		}
		?>
		</p><div style="margin-top: 50px; padding-bottom: 20px">
        <a href="pdf_notes.html"><b>Click here</b></a>
        for an explanation of the difference between the <b>text only</b> and <b>PDF</b> versions of SchNEWS.
		</div>
	</div>
	<!-- This is a very short page so we need a vertical spacer -->
	<div style="height: 300px">
		 
	</div>
</div>
<div class="mainBar_right">
	<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
