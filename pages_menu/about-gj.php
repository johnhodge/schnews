<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS :: About Us</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/archiveIndex.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
	<?php
		//	Include Main Left Bar
		 <!--#include virtual="../inc/leftBarMain.php"-->
	?>
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="main_page_title_new" id="main_page_title" style="margin-left: 15px;">SchNEWS? Sch...WHO? </div>
	<div style="margin-left: 15px; margin-right: 6px; border-left: 2px solid #e6e6e6; border-right: 2px solid #e6e6e6; padding-left: 18px; padding-right: 18px; ">
	<p><em><strong>&quot;SchNEWS</strong></em> was a free weekly publication from Brighton, England,<a href="https://en.wikipedia.org/wiki/SchNEWS#cite_note-1"></a> which ran from November 1994 until September 2014. The main focus was environmental and social issues/struggles in the UK – but also internationally – with an emphasis on direct action protest, and autonomous political struggles outside formalised political parties&quot;. - From<em> Wikipedia</em> (to read the rest <a href="https://en.wikipedia.org/wiki/SchNEWS" target="_blank"><strong>click here</strong></a>) <br> </p>
	<div class="SCHNEWS_NEW_smaller_font"><strong><em>Not a potted history, just some thoughts and memories from an ex-SchNEWS writer, giving one person&#39;s inside perspective on it... </em></strong></div>
	<h3><strong>Information For Action </strong></h3>
		<p><strong>Information For Action</strong> was the SchNEWS mantra and remit: it was about equipping people with the information and contacts so they could engage with - and get pro-active about -  the issues it covered. It wasn&#39;t just about keeping  right-on readers up-to-date,  so they could go about their lives, but just a bit better informed. It was not intended to be merely some<em> &#39;roughage in a balanced news diet&#39;. </em></p>
	    <p> And likewise, it wasn&#39;t  about burdening readers with  the problems of the world   for the sake of it - the rare species under threat because of the new building development or the spectre of the far-right  - without giving the reader a way to respond or channel their outrage into something constructive or at least connect with other like-minds. SchNEWS articles usually ended with contact details for a campaign group to get involved with or even a date for an upcoming demonstration. That&#39;s as opposed to leaving the reader despondent, disempowered and alone. </p>
	    <p>Remembering this takes us back to the years before people could be placated with the split-second of relief that comes from pressing &#39;like&#39; buttons.</p>
		 <h3>What Else Did  SchNEWS Do? </h3>
      <p>At the absolute centre of its activity was of course the free weekly news-sheet, but there were many other aspects to SchNEWS, plus there were people who were core members, but weren&#39;t  writers. The small crew who put together the <a href="../schmovies/index.php">SchMOVIES </a>videos worked alongside and in parallel with the writers. </p>
      <p>To find out, in a perfunctory way, what SchNEWS was and did, and how it started,  there&#39;s no need to reiterate that again here: to catch some of the back-story and read about a week in the life of SchNEWS at its mid-point - see <a href="about2006.php">About Us</a> from 2006. The Wikipedia entry is a reasonable run-down <a href="https://en.wikipedia.org/wiki/SchNEWS" target="_blank">click here</a>. </p>
      <p>One misconception that often surrounds things that develop something of a public profile is that they have more people and resources than they actually have. SchNEWS achieved an enormous amount, with relatively few people, and a budget relying on donations and small amount of merch. Over 850 issues, probably around 8,000 articles, ten books, ten DVDs, the 20x30 marquee  probably did around 70-80 info-spaces at festivals and protests,  a countless amount  of free-info-stalls with a wobbly trestle table around the country at demos, gigs and events. Countless. Probably over a million news-sheets handed out (you do the maths - whoops sorry about all the trees). Live satirical news shows were how SchNEWS started, which was reprised at later points. Justice - the group who started SchNEWS - were central to two Direct Action conferences in the 90s, then in 2007 SchNEWS hosted a national Radical Media Gathering. </p>
      <p>We can go on if you like... and let&#39;s get to the heart of it: SchNEWS (and Justice) people were often part of or directly involved in the campaigns they wrote about - and for a number of those groups, at various points the SchNEWS office was  a publicity or media centre. Numerous  groups and campaigns were helped by SchNEWS people with creating posters, flyers, websites, press releases, etc.  There was more than a symbiosis between activists and SchNEWS - it was some of the same people! But that&#39;s not disregarding  those in SchNEWS who weren&#39;t involved in direct action, they could perhaps be called &#39;media activists&#39;, where running an independent news publication is an action in itself. </p>
      <p>There were a lot of different side-projects and other activities that happened in the SchNEWS office, including  one-off publications, pamphlets, of which only some are available on this archived site. For instance, going back to the early days, there was an attempt to create an alternative news agency, called Bothered, which was to bring SchNEWS-type news to a larger audience - one of several innovative initiatives which sprung from early Justice/SchNEWS people.</p>
      <p>And - if we can throw it in - another significant effort which came out of the SchNEWS office,  though a different group with some SchNEWS people, was the Brighton-only dirt-digging, muck-raking news-sheet Rough Music in the noughties. </p>
      <p>So - this illustrates the point that SchNEWS had a much broader sphere of activity than the roughly-copied scraps of paper which sat in rows curling up on pamphlet racks in social centres and the like.</p>
      <p>But, lastly, back to the news-sheet: fair play to all the different crews over the years who produced it on a weekly basis. One thing which helped turn SchNEWS into the fixture it was was the fact that it kept coming out, reliably (and it looked the same every time!). For nearly all its years, it barely took weeks off, apart from  around New Year, and during the summers it was busier than ever producing double-issues and handing them out at festivals. And a &#39;big-up&#39;, as they used to say,  to the web, mail-out and printing crews who also worked tirelessly.</p>
      <p>Talking of the crews, SchNEWS was blessed with some very talented people who came through the door. For volunteers the standard was amazingly high. The original crew who started it were obviously a very bright bunch, but once they&#39;d got it up and running,  after a few years some of them  started to move on to other things - but SchNEWS was never short of good new people. And when they left, more came. Some stayed a long time, some just made an impact then left. Many lifelong friendships remain. As the crews rotated and people came and went, you could probably break the twenty years into six or seven &#39;eras&#39; according to certain people who were there at the time - but - it very much kept its consistency. For the regular SchNEWS reader who sat down with a copy on any given week, it would probably have felt very familiar each time. By that I mean it reliably kept up its standard of reporting news not in other publications, kept a consistency in tone and political perspective, and its  slightly jokey tone and ridiculous headlines (I didn&#39;t mean that it just trotted out the same old pap every week!).</p>
      <p>This article is not going into why SchNEWS ended or analysing the changes that have left the media world unrecognisable over the twenty-year period between its first and last issue. This is more a celebration of what it achieved in twenty years with so little.  But to find out about what the SchNEWS writers were saying when they stopped producing it, see <a href="../stories/AND-FINALLY.htm">And Finally</a>. </p>
    </div>
</div>
<!--
<div class='mainBar_right'>
	<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
</div>
-->
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
