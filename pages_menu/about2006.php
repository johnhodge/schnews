<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS :: About Us</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/archiveIndex.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
	<?php
		//	Include Main Left Bar
		 <!--#include virtual="../inc/leftBarMain.php"-->
	?>
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="main_page_title_new" id="main_page_title" style="margin-left: 15px;">
		About Us
	</div>
	<div class="SCHNEWS_NEW_smaller_font" style="margin-left: 15px;"> Last updated August 2006	</div>
	<div style="margin-left: 15px; margin-right: 6px; text-align@ left">
	<p align="left"> SchNEWS was born in a squatted Courthouse in Brighton
              in 1994 as part of Justice? - Brighton&#39;s campaign against the Criminal
              Justice Act. A few bright sparks decided to start reading out the
              news. Some of those bright sparks then decided to put some of it
              on paper - nearly ten years later and we&#39;re still printing! From
              the anti road protests at the <a href="../archive/news22.htm">M11</a>
              in London to the <a href="../archive/news57.htm">Newbury Bypass</a>
              to the big <a href="../archive/news88.htm">Reclaim The Streets</a>
              events of the nineties SchNEWS was there. From worker&#39;s struggles
              such as the <a href="../archive/news93.htm">Liverpool Dockers</a>,
              fights against privatisation of public services to reporting on
              social centres and sustainable futures - week in week out SchNEWS
              reported the news from the direct action frontlines.</p>
            <p>Then in February 1998 some of the crew went to Geneva to the first
              ever <a href="../archive/news156.htm">Peoples&#39;
              Global Action conference</a>. Here we met people involved in grassroots
              movements from across the world, swapped stories, made friendships
              and began to see the bigger picture, and with many others who had
              been involved in localised direct action campaigns, our attention
              now turned to also attacking the corporate carve-up of the entire
              planet while supporting the diverse small-scale alternatives. The
              first signs of this new shift was in May 1998 when mass demonstrations
              were held world-wide simultaneously against the G7 Summit in Birmingham,
              then again on <a href="../archive/news217.htm">June
              18th 1999 (J18)</a>. But it was the mass protests against the WTO
              in <a href="../archive/news239.htm">Seattle
              in November 1999</a> that really brought this &#39;movement&#39; to the
              world&#39;s attention.</p>
            <p>It&#39;s impossible to know how many people read SchNEWS every week,
              particularly now with the internet. Out of the 2,000 that get printed
              (more for big festies and demos) 650 get posted out each week to
              everyone from subscribers to bookshops to around 50 prisoners around
              the world who get it each week for free. Add to that over 11,500
              email subscribers, many of whom get it as a PDF* file (so it looks
              exactly like the real thing). While many photocopy and distribute
              their paper copy locally, PDF files allow people all over the world
              to print out SchNEWS, and distribute it, before it&#39;s even hit the
              streets in Brighton! We often hear from people who print out PDFs
              and distribute each week - at infoshops or wholefood shops, on campuses
              and out on the streets, all over the world. Added to that popular
              sites like <a href="http://www.urban75.com" target="_blank">Urban
              75</a> mirror SchNEWS each week, while <a href="http://www.ainfos.ca/" target="_blank">A-Infos</a>
              news service email it out internationally to subscribers, and the
              odd issue gets translated into other languages - someone even put
              a SchNEWS site out in Russian at one stage! So as well as the sixteen
              thousand or so who visit the SchNEWS site each week, it is hard
              to work out how many we reach altogether, especially after we&#39;ve
              added an <a href="../pages_menu/defunct.php">RSS Feed</a>, allowing people
              to read SchNEWS via news aggregators.</p>
            <p>Our <a href="../pap/index.htm">Party and Protest</a> section is
              the most popular feature on our website, updated every week with
              a mishmash of festival dates, meetings and demos as well as a section
              on where to go if you want to find out radical contact points around
              the country. Our <a href="../diyguide/index.htm">DIY guide</a> has
              useful tips on everything from setting up your own newsletter to
              making your own bio-diesel. We also try to continually update our
              <a href="../links/index.php">contacts list</a> - currently listing
              over 800 useful grassroots organisations.            </p>
            <h3><b>SO WHO FUNDS ALL THIS ?</b></h3>
            <p>SchNEWS is run on a voluntary basis - no one gets paid. Tho&#39; we
              do manage to blag free into gigs and festivals (to spread the word
              of course) and when our treasurer&#39;s not looking, raid the petty
              cash tin for biscuit money. Our stories originate anywhere from
              anarchist literature to the dodgy Financial Times (no, really),
              from conversations in the pub to the internet. While we always try
              to be as accurate as possible and chase people up to verify the
              stories, the idea behind SchNEWS is not to believe the printed word,
              but to get up off yer arse and go and see for yourself. So articles are often first hand accounts
              from trusted sources or ourselves as we storm all over the country
              causing trouble/saving the world/having a laugh.</p>
            <p>As for who keeps us going... Well, since our office rent was introduced
              we reckon to be spending around £24,000 a year on rent, printing,
              stamps, telephone, computers, envelopes, stationary, e-mail accounts,
              (biscuits)... we rely entirely on subscriptions, benefit gigs and
              our readers&#39; generosity to keep us afloat.</p>
            <p>Finally, SchNEWS is an open collective, we are always looking for
              new people to get involved. So if you like reading the news the
              mainstream tends to ignore (or at best turns into a crap lifestyle
              feature, rather than why an action is going on) take a look in the
              next column and see if anything grabs your fancy. 
            </p>
            <h3><b><a name="typical"></a>WAKE UP! WAKE UP! - IT&#39;S YER TYPICAL SchNEWS
              WEEK...</b> </h3>
            <ul>
              <li>Monday/Tuesday evening: We get well over 500 emails a week,
                and a few people have the job of going though all these, answering
                queries and deciding which ones might become stories.</li>
              <li>Wednesday: Start writing and researching SchNEWS, go through
                all the mail, decide roughly what we are going to cover this week.</li>
              <li>Thursday: More writing, except it&#39;s usually a bit more frantic
                cos we&#39;ve got to get it all finished and last minute updates come
                in. Writers go to pub.</li>
              <li>Thursday early evening: Desk Top Publishing crew come in and
                do their stuff, somehow managing to crush a tonne of information into two
                bits of A4. DTPers usually make it to pub.</li>
              <li>Thursday bit later in the evening: Web crew come in, SchNEWS
                gets put on the web and emailed out to thousands across the globe.
                Web crew never get to pub. Honest.</li>
              <li>Friday 9 a.m.: Hot off the press. A bleary-eyed person prints
                up the SchNEWS at the Resource Centre for distribution round Brighton
                and back to the office for...</li>
              <li>Friday afternoons: The mail out crew sends it out to all our
                paper subscribers.</li>
              <li>Weekends: Depending on the weather/time of year you might see
                us at festivals, gigs or conferences or handing out SchNEWS to
                passers by in the street.</li>
            </ul>
            <p>And there&#39;s more: During the week the subscription database is
              constantly being updated, letters answered, book orders posted,
              journalist queries answered and the next SchNEWS book being sorted.
              Still interested? Then give the office a call. We have regular training
              days, but if you can&#39;t wait for them, give the office a ring now
              on 01273 685913. Don&#39;t let the fact that SchNEWS comes out every
              week lull you into a false sense of security! We constantly need
              more people to get involved. We guestimate SchNEWS reaches around 50,000 people
              a week. Not bad for a scrappy no-adverts no-compromise bit of A4, eh?<br>
      </p>
            <p>* <a href="pdf_notes.html">PDF file - Portable Document Format</a>,
              a file format which allows all types of computers to reproduce an
              article in a graphics layout. To view and print these files you
              need Adobe Acrobat (free to get off the internet) or other PDF viewers.
              To subscribe to SchNEWS for free either as &#39;PDF&#39; or normal plain
              text file click here.<br>
            </p>
            <p>** Can you make out a standing order to help with paying our rent
              or just want to bung us a fiver towards this weeks printing costs?
              Find out how by going to: <a href="../extras/help.htm">../extras/help.htm</a></p>
	</div>
</div>
<!--
<div class='mainBar_right'>
	<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
</div>
-->
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
