
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS - Contacts Database</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week&#39;s issue, Party &amp; Protest events listing, free SchMOVIES, archive and more" name="description">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/contacts.css" rel="stylesheet" type="text/css">
<link href="../search.htm" rel="search">
<link href="/favicon.ico" rel="SHORTCUT ICON">
<link href="../feed.xml" rel="alternate" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." type="application/rss+xml">
<meta content="no-cache" http-equiv="Pragma"> <script language="JavaScript" type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
						<!-- Include for Banner Graphic -->
<div class="bannerGraphic">	<a href="../links/index.php"><img alt="SchNEWS Contact &amp; Links" border="0" src="../images_main/contacts-links-banner.jpg"></a> </div>
    <div>  </div>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarMain.php"-->
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="pap_title" id="pap_title">
	Contacts &amp; Links    </div>
            <font face="Arial, Helvetica, sans-serif" size="2">
            <img align="right" src="../images_main/yellow-pages-250h.png" width="250px">
            	<p>SchNEWS maintained a large contacts database, regularly updated (though less so in later years) of the same network of campaign, protest and community groups whose activities featured in the pages of SchNEWS.</p>
				<p>During the very early days of SchNEWS, at the height of the anti-Criminal Justice Protests and the burgeoning anti-roads movement, SchNEWS/Justice made a database which was printed in an A5 booklet called The White Book (or later just The Book) - remember this was still pre-internet. This historic contacts list is available here including the PDF of the full 108-page &#39;The Book&#39;.</p>
				<p><strong><a href="white-book.htm">View &#39;The Book&#39; from June 1995.</a></strong></p>
				<p>Later of course this contacts database became a key section of the SchNEWS website. Several versions of this list from different years will be put up when they become available, but at the moment only one is viewable, from 2002.</p>
				<p><strong><a href="links2002/index.htm">View the online Contacts List from June 2002.</a></strong></p>
         	</font>
            <p align="left">
            <font face="Arial, Helvetica, sans-serif">
            	<img height="10" src="../images_main/spacer_black.gif" width="100%">
          	</font>
          	</p>

</div>
<div class="mainBar_right">
		<!--	PAP Menu	-->
	<div style="text-align: center; margin: auto; width: 230px; font-weight: bold; font-size: 12px">
			<img align="middle" height="60px" name="Party &amp; Protest" src="../images_main/pap-button-225.png" style="border:none" width="225px">
<p align="center" style="border-bottom: 1px solid gray">Find that obscure demo you were on here...</p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/1995.htm">1995</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/1996.htm">1996</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/1997.htm">1997</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/1998.htm">1998</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/1999.htm">1999</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2000.htm">2000</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2001.htm">2001</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2002.htm">2002</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2003.htm">2003</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2004.htm">2004</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2005.htm">2005</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2006.htm">2006</a></p>
<p align="center" style="border-bottom: 1px solid gray"><a href="../pap/2007.htm">2007</a></p>
<p align="center" style="border-bottom: 1px solid gray">2008-2014 <br>
Avaiable Soon!</p>
	</div>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->
</div>
</div>
</body>
</html>
