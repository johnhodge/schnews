<?php
	session_start();
	require_once '../contacts/functions/functions.php';
	 <!--#include virtual="../functions/schnews.php"-->
	 <!--#include virtual="../storyAdmin/formattingFunctions.php"-->
	 <!--#include virtual="../storyAdmin/dataFunctions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS - Contacts Database</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week&#39;s issue, Party &amp; Protest events listing, free SchMOVIES, archive and more" name="description">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../contacts.css" rel="stylesheet" type="text/css">
<link href="../search.htm" rel="search"> <link href="/favicon.ico" rel="SHORTCUT ICON">
<link href="/feed.xml" rel="alternate" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." type="application/rss+xml">
<meta content="no-cache" http-equiv="Pragma"> <script language="JavaScript" type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
    		<?php
    			//	Include the current Banner Graphic
    			include("../inc/bannerGraphic.php");
    		?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
		<?php
			//	Require Left Bar
			 <!--#include virtual="../inc/leftBarMain.php"-->
		?>
</div>
<div class="copyleftBar">
	<?php
		//	Include Copy Left Bar
		 <!--#include virtual="../inc/copyLeftBar.php"-->
	?>
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="pap_title" id="pap_title">
		View Contacts
	</div>
	<?php
              //	ARE WE VIEWING BASED ON CATEGORY OR SEARCH
              $mode 		=	'';			// INITIALISE TO AN EMPTY VARIABLE. IF IT IS STILLE EMPTY AT THE END OF THIS THEN WE WILL SIMPLY SHOW ALL CONTACTS...
              //	IF WE ARE SEARCHING ON CATEGORY...
              if (isset($_GET['cat']) && is_numeric($_GET['cat']))		{
              		$mode		=	'c';
              		$cat_sd		=	cleanForScreenOutput($_GET['cat']);
              		//	GET THE CATEGORY NAME FROM THE DATABASE
              		$conn		=	mysqlConnect();
              		$sql		= 	" SELECT name FROM con_categories WHERE id = $cat_sd ";
              		$rs			=	mysqlQuery($sql, $conn);
              		$res		=	mysqlGetRow($rs);
              		$catName_ss	=	cleanForScreenOutput($res['name']);
              		$conn		=	mysql_close($conn);
              }
              //	IF WE ARE SEARCHING ON KEYWORD...
              if (isset($_GET['sname']) || isset($_GET['saddress']) || isset($_GET['sdescription']) )	{
              		$mode		=	'k';
              		//	NAME KEYWORD
              		if ($_GET['sname'] == '')	{
              			$sName_sd	=	'';	$sName_ss	=	'';
              		} else {
              			$sName_ss	=	cleanForScreenOutput($_GET['sname']);
              			$sName_sd	=	cleanForDatabaseInput($_GET['sname']);
              		}
              		// ADDRESS KEYWORD
              		if ($_GET['saddress'] == '')	{
              			$sAddress_sd	=	'';	$sAddress_ss	=	'';
              		} else {
              			$sAddress_ss	=	cleanForScreenOutput($_GET['saddress']);
              			$sAddress_sd	=	cleanForDatabaseInput($_GET['saddress']);
              		}
              		// DESCRIPTION KEYWORD
              		if ($_GET['sdescription'] == '')	{
              			$sDescription_sd	=	'';	$sDescription_ss	=	'';
              		} else {
              			$sDescription_ss	=	cleanForScreenOutput($_GET['sdescription']);
              			$sDescription_sd	=	cleanForDatabaseInput($_GET['sdescription']);
              		}
              }
              echo "<div style='font-family: arial'>";
              //	DEPENDING ON WHICH MODE WE'RE IN WE WILL NEED A DIFFERENT MAIN SQL QUERY...
              switch ($mode)	{
              		default:
              			$sql	=	" SELECT id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							WHERE 		authorised = 1
              								AND		( deleted <> 1 OR deleted IS NULL )
              							ORDER BY name ASC
              						";
              			echo "<h3>Showing All Contacts</h3>";
              			break;
              		case 'c':
              			$sql	=	" SELECT con_contacts.id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							LEFT JOIN con_cat_mappings
              								ON con_contacts.id = con_cat_mappings.contact_id
              							WHERE		authorised = 1
              							 	AND		( deleted <> 1 OR deleted IS NULL )
              							 	AND		con_cat_mappings.cat_id = $cat_sd
              							ORDER BY name ASC
              						";
              			echo "<h3>Showing result for category '$catName_ss'</h3>";
          				break;
              		case 'k':
              			$wheres		=	array();
              			if ($sName_sd != '')		$wheres[]	=	" name LIKE '%$sName_sd%' ";
              			if ($sAddress_sd != '')		$wheres[]	=	" address LIKE '%$sAddress_sd%' ";
              			if ($sDescription_sd != '')	$wheres[]	=	" description LIKE '%$sDescription_sd%' ";
              			$wheres		=	implode(' AND ', $wheres);
              			$sql	=	" SELECT id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							WHERE		authorised = 1
              								AND		( deleted <> 1 OR deleted IS NULL ) ";
              			if ($wheres != '')	{
              				$sql		.=		" AND		(	$wheres	) ";
              			}
						$sql		.=		" ORDER BY name ASC ";
						echo "<h3>Showing result for search keywords:</h3><blockquote style='font-weight: bold'>";
              			if ($sName_ss != '')		echo "Name :: '<i>$sName_ss</i>'<br /> ";
              			if ($sAddress_ss != '')		echo "Address :: '<i>$sAddress_ss</i>'<br /> ";
              			if ($sDescription_ss != '')	echo "Description :: '<i>$sDescription_ss</i>'<br /> ";
						if ($sName_ss == '' && $sAddress_ss == '' && $sDescription_ss == '')	{
							echo "No Keywords Entered - Showing all results instead...";
						}
              			echo "</blockquote>";
          				break;
              }
              //	GET THE CONTACT RECORDS FROM THE DATABASE...
              $conn			=   mysqlConnect();
              $rs			=	mysqlQuery($sql, $conn);
              $output		=	array();
              while ($res		=	mysqlGetRow($rs))	{
              		$id				=	cleanForDatabaseInput($res['id']);
              		$name_ss		=	cleanForScreenOutput($res['name']);
              		$address_ss		=	cleanForScreenOutput($res['address']);
              		$phone_ss		=	cleanForScreenOutput($res['phone']);
              		$fax_ss			=	cleanForScreenOutput($res['fax']);
              		$web_ss			=	scanForLinks(cleanForScreenOutput($res['web']));
              		$email_ss		=	scanForLinks(cleanForScreenOutput($res['email']));
              		$description_ss	=	nl2br(cleanForScreenOutput($res['description']));
              		$sqlC			=	" SELECT con_categories.name
              									FROM con_categories
              									LEFT JOIN con_cat_mappings
              										ON con_categories.id = con_cat_mappings.cat_id
              									WHERE con_cat_mappings.contact_id = $id
              									ORDER BY con_categories.name ASC
              							";
              		$rsC			=	mysqlQuery($sqlC, $conn);
              		$categories_ss	=	array();
              		while 	($resC			=	mysqlGetRow($rsC))	{
              				$categories_ss[]	=	cleanForScreenOutput($resC['name']);
              		}
              		$categories_ss		=	implode(', ', $categories_ss);
              		$contact		=	'';
              		$contact		.=	"\r\n<tr><td><b>Name:</b></td><td><b>$name_ss</b></td></tr>";
              		if ($address_ss != '') 			$contact		.=	"\r\n<tr><td valign='top'><b>Address:</b></td><td>$address_ss</td></tr>";
              		if ($phone_ss != '') 			$contact		.=	"\r\n<tr><td valign='top'><b>Phone:</b></td><td>$phone_ss</td></tr>";
              		if ($fax_ss != '')	 			$contact		.=	"\r\n<tr><td valign='top'><b>Fax:</b></td><td>$fax_ss</td></tr>";
              		if ($web_ss != '') 				$contact		.=	"\r\n<tr><td valign='top'><b>Web:</b></td><td>$web_ss</td></tr>";
              		if ($email_ss != '') 			$contact		.=	"\r\n<tr><td valign='top'><b>Email:</b></td><td>$email_ss</td></tr>";
              		if ($description_ss != '') 		$contact		.=	"\r\n<tr><td valign='top'><b>Description:</b></td><td>$description_ss</td></tr>";
              		if ($categories_ss != '') 		$contact		.=	"\r\n<tr><td valign='top'><b>Categories:</b></td><td>$categories_ss</td></tr>";
              		$contact		.=	"\r\n</tr>";
              		$output[]		=	$contact;
              }
              $num		=	count($output);
              $glue		=	"\r\n<tr>\r\n<td></td>\r\n<td style='border-bottom: 1px solid black'>&nbsp;</td>\r\n<tr>\r\n<tr><td><br /></td></tr>";
              $output	=	implode($glue, $output);
              $output	.=	"\r\n</table>";
              echo "<div style='font-size: 11px; text-align:right'>Showing $num results</div><br />";
              echo "\r\n<table style='font-size: 14px' width='100%'>\r\n";
              echo $output;
              if (!$num)		echo "<br /><br />They are no results to display";
			  echo "</div>";
			  //	Generate CSV Download Link
			  if ($num > 0)	{
					  $get			=		array();
					  foreach ($_GET as $key => $value)	{
							$get[]		=	$key.'='.$value;
					  }
					  $get		=	implode('&', $get);
					  echo "\r\n<div class='csv_download'>";
					  echo "\r\n<a href='download_csv.php?$get'>";
					  echo "\r\n<img src='../images_main/csv-sm.jpg' align='right' />";
					  echo "\r\nDownload as CSV";
					  echo "\r\n</a>";
					  echo "\r\n</div>";
			  }
              ?>
</div>
<div class="mainBar_right">
	<?php
		//	Include the P&P DB Advert
		 <!--#include virtual="../inc/pap_right_bar_advert.php"-->
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>