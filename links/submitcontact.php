<?php
	session_start();
	require_once '../contacts/functions/functions.php';
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS - Contacts Database</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week&#39;s issue, Party &amp; Protest events listing, free SchMOVIES, archive and more" name="description">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../contacts.css" rel="stylesheet" type="text/css">
<link href="../search.htm" rel="search"> <link href="/favicon.ico" rel="SHORTCUT ICON">
<link href="/feed.xml" rel="alternate" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." type="application/rss+xml">
<meta content="no-cache" http-equiv="Pragma"> <script language="JavaScript" type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
    <div>  </div>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
		<?php
			//	Require Left Bar
			 <!--#include virtual="../inc/leftBarMain.php"-->
		?>
</div>
<div class="copyleftBar">
	<?php
		//	Include Copy Left Bar
		 <!--#include virtual="../inc/copyLeftBar.php"-->
	?>
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="pap_title" id="pap_title">
		Submit New Contact
	</div>
		<div class="add_form_intro">
            Enter the details of your new contact.<br>
            <br>
            Once it is added it will been entered into our database, but it will need one of our very hardworking website people to OK it before it is available to the public...
        </div>
            <?php
            	/*
            	 * 	IT IS POSSIBLE THAT THIS PAGE IS BEING LOADED AS A RESULT AS A FAILED ATTEMPT TO ADD A CONTACT.
            	 *
            	 * 	IF THIS IS THE CASE THEN THERE WILL PROBABLY BE AN $ERRORS ARRAY, WHICH WILL NEED DUMPING TO SCREEN...
            	 */
            	if (isset($errors) && is_array($errors))	{
            			echo "<div style='border-bottom:solid 1px red; border-top: solid 1px red'>";
            			foreach ($errors as $error)		{
            					echo "<p style='color:red; font-family:Arial, Helvetica, sans-serif; text-align:right'>$error</p>";
            			}
            			echo "</div>";
            	}
            	//	PREPARE THE VALUES TO BE FILLED INTO THE SUBSEQUENT FORM...
            	$fields = array('Name', 'Address', 'Telephone', 'Fax', 'Web', 'Email', 'Description');
            	foreach ($fields as $field)	{
            			$value[$field] 		=	'';
            			if (isset($data_US[$field]))	$value[$field]		=	cleanForScreenOutput($data_US[$field]);
            	}
            ?>
            <form action="../contacts/processSubmission.php" method="POST">
              <p>
              </p><table>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Name:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    &lt;input type=text style=&#39;width:350px&#39; name=&quot;Name&quot; <?php echo " value='{$value['Name']}' "; ?> /&gt;
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Address:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    <textarea name="Address" rows="10" style="width:350px"><?php echo "{$value['Address']}"; ?></textarea>
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Telephone:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    &lt;input type=text style=&#39;width:350px&#39; name=&quot;Telephone&quot; <?php echo " value='{$value['Telephone']}' "; ?> /&gt;
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Fax:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    &lt;input type=text style=&#39;width:350px&#39; name=&quot;Fax&quot; <?php echo " value='{$value['Fax']}' "; ?> /&gt;
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Web:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    &lt;input type=text style=&#39;width:350px&#39; name=&quot;Web&quot; <?php echo " value='{$value['Web']}' "; ?> /&gt;
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Email:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    &lt;input type=text style=&#39;width:350px&#39; name=&quot;Email&quot; <?php echo " value='{$value['Email']}' "; ?> /&gt;
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Description:</font></td>
                  <td><font face="Arial, Helvetica, sans-serif" size="2">
                    <textarea cols="40" name="Description" rows="10" style="width:350px"><?php echo "{$value['Description']}"; ?></textarea>
                    </font></td>
                </tr>
                <tr valign="top">
                  <td><font face="Arial, Helvetica, sans-serif" size="2">Category
                    (can be more than one):</font></td>
                  <td> <font face="Arial, Helvetica, sans-serif" size="2">
<?php
//	INCLUDE THE FUNCTION LIBRARY
require_once '../contacts/functions/functions.php';
//	GRAB THE WHOLE CATEGORIES LIST
$categories		=	getAllCategories();
//	CYCLE THROUGH EACH CATEGORY AND LIST A FORM INPUT OPTION FOR EACH
foreach ($categories as $id => $category)	{
		if ( ( isset($errors) && is_array($errors) ) && isset($_POST['category'.$id]) )	$selected 	=	'checked';
		else	$selected	=	'';
		echo "\r\n\r\n<div class='add_form_category'>\r\n<input type='checkbox' name='category$id' id='category$id' $selected /><label for='category$id'>".stripslashes($category)."</label>\r\n</div>";
}
?>
                    </font></td>
                </tr>
                <tr valign="top">
                  <td> </td>
                  <td> <font face="Arial, Helvetica, sans-serif" size="2"> <br>
                    </font></td>
                </tr>
              </table>
              <input name="actiontotake" type="hidden" value="Addtemp"><input name="submitNewContact" type="submit" value="Add Record"></form>
            <p align="left"><font face="Arial, Helvetica, sans-serif"><img height="10" src="../images_main/spacer_black.gif" width="480"></font></p>
</div>
<div class="mainBar_right">
	<?php
		//	Include the P&P DB Advert
		 <!--#include virtual="../inc/pap_right_bar_advert.php"-->
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>