<?php
	session_start();
	require_once '../contacts/functions/functions.php';
	 <!--#include virtual="../storyAdmin/dataFunctions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
	 <!--#include virtual="../functions/csv_classes.php"-->
              //	ARE WE VIEWING BASED ON CATEGORY OR SEARCH
              $mode 		=	'';			// INITIALISE TO AN EMPTY VARIABLE. IF IT IS STILLE EMPTY AT THE END OF THIS THEN WE WILL SIMPLY SHOW ALL CONTACTS...
              //	IF WE ARE SEARCHING ON CATEGORY...
              if (isset($_GET['cat']) && is_numeric($_GET['cat']))		{
              		$mode		=	'c';
              		$cat_sd		=	cleanForScreenOutput($_GET['cat']);
              		//	GET THE CATEGORY NAME FROM THE DATABASE
              		$conn		=	mysqlConnect();
              		$sql		= 	" SELECT name FROM con_categories WHERE id = $cat_sd ";
              		$rs			=	mysqlQuery($sql, $conn);
              		$res		=	mysqlGetRow($rs);
              		$catName_ss	=	cleanForScreenOutput($res['name']);
              		$conn		=	mysql_close($conn);
              }
              //	IF WE ARE SEARCHING ON KEYWORD...
              if (isset($_GET['sname']) || isset($_GET['saddress']) || isset($_GET['sdescription']) )	{
              		$mode		=	'k';
              		//	NAME KEYWORD
              		if ($_GET['sname'] == '')	{
              			$sName_sd	=	'';	$sName_ss	=	'';
              		} else {
              			$sName_ss	=	cleanForScreenOutput($_GET['sname']);
              			$sName_sd	=	cleanForDatabaseInput($_GET['sname']);
              		}
              		// ADDRESS KEYWORD
              		if ($_GET['saddress'] == '')	{
              			$sAddress_sd	=	'';	$sAddress_ss	=	'';
              		} else {
              			$sAddress_ss	=	cleanForScreenOutput($_GET['saddress']);
              			$sAddress_sd	=	cleanForDatabaseInput($_GET['saddress']);
              		}
              		// DESCRIPTION KEYWORD
              		if ($_GET['sdescription'] == '')	{
              			$sDescription_sd	=	'';	$sDescription_ss	=	'';
              		} else {
              			$sDescription_ss	=	cleanForScreenOutput($_GET['sdescription']);
              			$sDescription_sd	=	cleanForDatabaseInput($_GET['sdescription']);
              		}
              }
              //	DEPENDING ON WHICH MODE WE'RE IN WE WILL NEED A DIFFERENT MAIN SQL QUERY...
              switch ($mode)	{
              		default:
              			$sql	=	" SELECT id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							WHERE 		authorised = 1
              								AND		deleted <> 1
              							ORDER BY name ASC
              						";
              			break;
              		case 'c':
              			$sql	=	" SELECT con_contacts.id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							LEFT JOIN con_cat_mappings
              								ON con_contacts.id = con_cat_mappings.contact_id
              							WHERE		authorised = 1
              							 	AND		deleted <> 1
              							 	AND		con_cat_mappings.cat_id = $cat_sd
              							ORDER BY name ASC
              						";
          				break;
              		case 'k':
              			$wheres		=	array();
              			if ($sName_sd != '')		$wheres[]	=	" name LIKE '%$sName_sd%' ";
              			if ($sAddress_sd != '')		$wheres[]	=	" address LIKE '%$sAddress_sd%' ";
              			if ($sDescription_sd != '')	$wheres[]	=	" description LIKE '%$sDescription_sd%' ";
              			$wheres		=	implode(' AND ', $wheres);
              			$sql	=	" SELECT id, name, address, phone, fax, web, email, description
              							FROM con_contacts
              							WHERE		authorised = 1
              								AND		deleted <> 1
              								AND		(	$wheres	)
										ORDER BY name ASC
									";
          				break;
              }
              //	GET THE CONTACT RECORDS FROM THE DATABASE...
              $conn			=   mysqlConnect();
              $rs			=	mysqlQuery($sql, $conn);
              $headers		=	array('Name', 'Address', 'Phone', 'Fax', 'Web', 'Email', 'Description', 'Categories');
              $csv_file		=	new new_csv_file($headers);
              $output		=	array();
              while ($res		=	mysqlGetRow($rs))	{
              		$id				=	cleanForDatabaseInput($res['id']);
              		$name_ss		=	cleanForScreenOutput($res['name']);
              		$address_ss		=	cleanForScreenOutput($res['address']);
              		$phone_ss		=	cleanForScreenOutput($res['phone']);
              		$fax_ss			=	cleanForScreenOutput($res['fax']);
              		$web_ss			=	cleanForScreenOutput($res['web']);
              		$email_ss		=	cleanForScreenOutput($res['email']);
              		$description_ss	=	nl2br(cleanForScreenOutput($res['description']));
              		$sqlC			=	" SELECT con_categories.name
              									FROM con_categories
              									LEFT JOIN con_cat_mappings
              										ON con_categories.id = con_cat_mappings.cat_id
              									WHERE con_cat_mappings.contact_id = $id
              									ORDER BY con_categories.name ASC
              							";
              		$rsC			=	mysqlQuery($sqlC, $conn);
              		$categories_ss	=	array();
              		while 	($resC			=	mysqlGetRow($rsC))	{
              				$categories_ss[]	=	cleanForScreenOutput($resC['name']);
              		}
              		$categories_ss		=	implode(', ', $categories_ss);
					$row	=	array($name_ss, $address_ss, $phone_ss, $fax_ss, $web_ss, $email_ss, $description_ss, $categories_ss);
					$csv_file->add_row($row);
              }
mysql_close($conn);
increment_other_hits('contacts_csv');
//echo $csv_file->make_csv_file();
header("Content-Type: application/octet-stream");
header("Content-Length: ".strlen($csv_file->make_csv_file()));
header('Content-Disposition: attachment;filename="contacts_list.csv"');
echo $csv_file->make_csv_file();
?>
