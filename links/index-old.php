<?php
	session_start();
	require_once '../contacts/functions/functions.php';
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS - Contacts Database</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week&#39;s issue, Party &amp; Protest events listing, free SchMOVIES, archive and more" name="description">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/contacts.css" rel="stylesheet" type="text/css">
<link href="../search.htm" rel="search"> 
<link href="/favicon.ico" rel="SHORTCUT ICON">
<link href="../feed.xml" rel="alternate" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." type="application/rss+xml">
<meta content="no-cache" http-equiv="Pragma"> <script language="JavaScript" type="text/javascript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
    <div>  </div>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
		<?php
			//	Require Left Bar
			 <!--#include virtual="../inc/leftBarMain.php"-->
		?>
</div>
<div class="copyleftBar">
	<?php
		//	Include Copy Left Bar
		 <!--#include virtual="../inc/copyLeftBar.php"-->
	?>
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<div class="pap_title" id="pap_title">
		Contacts &amp; Links Database
	</div>
              <?php
              	//echo $_SESSION[SES.'just_added'];
              	if (isset($_SESSION[SES.'just_added']) && is_numeric($_SESSION[SES.'just_added']))	{
              			echo showJustAddedNotice($_SESSION[SES.'just_added']);
              			unset($_SESSION[SES.'just_added']);
              	}
              ?>
            <p align="left">
            <font face="Arial, Helvetica, sans-serif" size="2">
            <img align="right" height="86" src="../images_main/yellow_pages.jpg" width="100">
            	Welcome to our extensive, brand-new contacts database. This can be searched
              	by category or keyword and contains more than 800 entries. New entries can
              	be added by clicking <a href="submitcontact.php">here</a>.
         	</font>
              <?php
              	/*		NEW CONTACTS DATABASE DISPLAY SYSTEM	*/
              	//	GET ALL CATEGORIES FROM THE DATABASE
              	$conn	=	mysqlConnect();
              	$sql	=	" SELECT * FROM con_categories ORDER BY name ASC ";
              	$rs		=	mysqlQuery($sql, $conn);
              	//	GENERATE AND DISPLAY A LIST OF ALL THE CATEGORIES, EACH ONE PRESENTED
              	//	AS A LINK, IN ORDER TO SHOW THE RESULTS FOR THAT CATEGORY...
              	echo "\r\n<div style='font-family: arial'>\r\n";
              	echo "<br />\r\n";
              	echo "<div class='section_title'>Search by Category</div></p>\r\n";
              	echo "<div class='contacts_category_list'>\r\n";
              	$output		=	array();
              	while ($res		=	mysqlGetRow($rs))	{
              			$id_cs		=	cleanForScreenOutput($res['id']);
              			$name_cs	=	str_replace(' ', "&nbsp;", cleanForScreenOutput($res['name']));
              			$output[]	=	"<a href='showcontacts.php?cat=$id_cs'>$name_cs</a>";
              	}
              	$output		=	implode(" | \r\n", $output);
              	echo $output;
              	$conn		=	mysql_close($conn);
              	echo "</div>\r\n";
              	echo "\r\n<br /><br />";
              	//	GENERATE AND DISPLAY THE FORM FOR SEARCHING CATEGORIES BY KEYWORD/S...
              	echo "<div class='section_title'>Search by Keyword</div><br />\r\n";
              	echo "\r\n\r\n<div class='contacts_search_list'>";
              	echo "\r\n<form method='GET' action='showcontacts.php'>\r\n";
              	echo "\r\n<table width='100%'>\r\n<tr>\r\n";
              	echo "<td>Search in name:</td>\r\n<td><input type='text' name='sname' value='' /><span style='font-size:12px'> e.g. '<i>Schnews</i>'</span></td>\r\n";
              	echo "\r\n</tr><tr>\r\n";
              	echo "<td>Search in address:</td>\r\n<td><input type='text' name='saddress' value='' /><span style='font-size:12px'> e.g. '<i>London</i>'</span></td>\r\n";
              	echo "\r\n</tr><tr>\r\n";
              	echo "<td>Search in description:</td>\r\n<td><input type='text' name='sdescription' value='' /><span style='font-size:12px'> e.g. '<i>Progressive</i>'</span></td>\r\n";
              	echo "\r\n</tr><tr>\r\n";
              	echo "\r\n<td></td>\r\n<td><input type='submit' value='Search' /></td>\r\n";
              	echo "\r\n</tr>\r\n</table>";
              	echo "\r\n</form>";
              	echo "\r\n</div>\r\n\r\n";
              	echo "<br />";
              	echo "\r\n<div style='font-size:10px'><b>Note:</b> This is an 'AND' search - i.e. if you search for the name 'Schnews' and the address 'London' it will only show results that match both those criteria.</div><br />";
              	echo "</div>\r\n";
              ?>
            </p><p align="left">
            <font face="Arial, Helvetica, sans-serif" size="2">
            	<a href="submitcontact.php">Click here to submit a new entry.</a>
            </font>
            </p>
            <p align="left">
            <font face="Arial, Helvetica, sans-serif" size="2">
            	* Some things you may be looking for are on
            	<a href="../pap/index.htm">Party  And Protest</a>.
            	<br><br>
            	Click here if you want: <br>
              	<a href="../pap/regularevents.htm">Regular Events/Actions (weekly and on-going events)</a>
              	<br>
              	<a href="../pap/yourarea.php">Events happening in your area</a>
              	<br>
              	<a href="../pap/bookshops.htm">Radical bookshops and independent info distribution</a>
            </font>
         	</p>
            <p>
            <font face="Arial, Helvetica, sans-serif" size="2">
            	Unlike most links pages ours includes snail mail, telephone and fax details and short description
              	of the groups. We don&#39;t think for a minute it&#39;s complete and it may well be out of date already.
              	If so please get in touch with the <a href="mailto: webmaster@schnews.org.uk">Webmasters</a> and
              	we&#39;ll try to sort it out.
        	</font>
        	</p>
            <p align="left">
            <font face="Arial, Helvetica, sans-serif">
            	<img height="10" src="../images_main/spacer_black.gif" width="480">
          	</font>
          	</p>
		
</div>
<div class="mainBar_right">
	<?php
		//	Include the P&P DB Advert
		 <!--#include virtual="../inc/pap_right_bar_advert.php"-->
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
