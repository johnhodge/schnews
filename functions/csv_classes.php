<?php

class new_csv_file	{

	protected $headers;			//	The column headers/titles
	protected $rows;			//	The rows of data in the file

	protected $num_rows;		//	How many rows do we have
	protected $num_columns;		//	How many columns

	protected $padd_empties;	//	If there are less columns entered for a row than there are headers, should we fill the rest with empties?

	function __construct($headers, $padd = false)	{

		if (!is_array($headers) || count($headers) == 0)	{

			return false;

		}
		if (!is_bool($padd))	$padd = false;

		$this->padd_empties 	=	$padd;

		$this->headers		=	$this->fix_odd_chars(implode(',', $headers));
		$this->num_rows		=	0;
		$this->num_columns	=	count($headers);

		return true;

	}

	public function add_row($row)	{

		if (!is_array($row) || count($row) == 0)	{

			return false;

		}
		if (!$this->padd_empties && count($row) != $this->num_columns)	{

			return false;

		}
		if (count($row) > $this->num_columns)	{

			return false;

		}


		if (count($row) < $this->num_columns)	{

				$diff		=	$this->num_columns - count($row);
				$count		=	0;
				while ($count < $diff)	{

					$row[]		=	'';
					$count++;

				}

		}

		foreach ($row	as $key => $value)	{

			$row[$key] 	=	'"'.trim(str_replace('"', '""', $this->fix_odd_chars($value))).'"';

		}


		$this->rows[]		=	implode(',', $row);
		$this->num_rows++;

		return true;

	}

	public function make_csv_file()	{

		return $this->headers."\r\n".implode("\r\n", $this->rows);


	}

	protected function fix_odd_chars($text)	{

		$find		=	array("�", "�", '�', "�");
		$replace	=	array('"', '"', '-', "'");
		$text		=	str_replace($find, $replace, $text);

		return $text;

	}

}



?>