<?php

if (!function_exists("send_smtp"))	{

function send_smtp($recipient, $subject, $body)
{


	global $send_type;
	
	global $smtp_host;
	global $smtp_user;
	global $smtp_pass;
	global $smtp_sender;
	
	if ($send_type)	{
		
			return mail($recipient, $subject, $body, "From: $smtp_sender");
			
		
	}
	
	//	==================================================================
	//
	//			CONFIG
	//
	//	==================================================================
	
	
	
	
	
	// Clean Inputs
	$subject			=	stripslashes($subject);
	$body				=	stripslashes($body);
	
	
	// Instantiate the PEAR::Mail class and pass it the SMTP auth details
	require_once ('Mail.php');
    $mail = Mail::factory("smtp", array('host' => $smtp_host, 'auth' => TRUE, 'username' => $smtp_user, 'password' => $smtp_pass));


	if (is_array($recipient))
	{
			foreach ($recipient as $to)
			{
				// Create the mail headers and send the mail via our SMTP object
				$headers = array("From" => $smtp_sender, "Subject"=>$subject);
				$output	=	$mail->send($to, $headers, $body);
			
				if (PEAR::isError($mail))
					{
						$return 		=	FALSE;
					}
				else
					{
						$return 		=	TRUE;
					}
			}
		}
		else
		{
			// Create the mail headers and send the mail via our SMTP object
			$headers = array("To"=>$recipient, "From" => $smtp_sender, "Subject"=>$subject);
			$output	=	$mail->send($recipient, $headers, $body);
			
			if (PEAR::isError($mail))
				{
					$return 		=	FALSE;
				}
			else
				{
					$logFile	=	fopen ('../functions/emailLog.txt', 'a+');
					$datetime	=	make_sql_datetime();
					$date		=	make_date_from_sql($datetime);
					$time		=	make_time_from_sql($datetime);
					$logOutput	=	"email sent, $date, $time, ".$_SERVER['REMOTE_ADDR'].', '.$_SERVER['REQUEST_URI'].", $subject";
					fwrite ($logFile, $logOutput);
					fclose($logFile);
					$return 		=	TRUE;
				}
		}	
	
	
	return $return;
	
}

}

?>