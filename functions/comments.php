<?php

function showComments($articleId, $storyNumber)	{

	// 	TEST INPUTS...
	if (!is_numeric($articleId))	return '';
	if (!is_numeric($storyNumber))	$storyNumber = 0;

	$link		=		"<b><a class='issueCommentLink' id='issueCommentLink$storyNumber'>[ Show More or Add Comment ]</a></b>";

	/*	======================== GET DATA FROM DB ============================ */
	$conn 	=	mysqlConnect();
	$sql	=	" SELECT comment, datetime, author FROM story_comments WHERE articleid = $articleId AND spam = 0 AND deleted = 0 ORDER by datetime DESC ";
	$rs		=	mysqlQuery($sql, $conn);
	$num	=	mysqlNumRows($rs, $conn);

	//	IF THERE ARE NO RESULTS THEN RETURN OUT...
	if (DEBUG) echo "Num Results = $num<br />";
	if ($num == 0)	{

		$link		=	"<b><a class='issueCommentLink' id='issueCommentLink$storyNumber'>[ Add a Comment ]</a></b>";
		$style		=	'text-align:right; border-top: 1px solid #ddddbb; padding: 5px; padding-top: 12; margin-top: 7px; color: #999999; font-size:11px; font-weight: normal; width:75%';
		$output		=	"<div align='right'>\r\n\r\n<div style='$style'>There are no comments on this story $link</div>\r\n\r\n</div>";
		$output		.=	"<div class='issueComments' id='issueComments$storyNumber'>".showAddMessage($articleId).'</div>';
		return $output;

	}





	/* 	====================== PROCESS DATA INTO ARRAYS ===================== */
	$count = 0;		//	THIS WILL ACT AS THE KEY FOR THE FOLLOWING ARRAYS...
	$comments		=	array();
	$authors		=	array();
	$datetimes		=	array();

	//	POPULATE THE ARRAYS
	while ($res		=	mysqlGetRow($rs))	{

		$comments[$count]		=	scanForLinks(strip_tags(htmlentities($res['comment'])));
		$authors[$count]		=	strip_tags(htmlentities($res['author']));
		$datetimes[$count]		=	makeHumanDate($res['datetime'], 'words').' at '.makeTimeFromSql($res['datetime']);
		$count++;

	}




	/* 	======================== GENERATE THE OUTPUT =========================== */

	//	SOME LINGUISTIC CONFIG
	$isAre		=	'are';
	$plural		=	's';
	if ($num	==	1)	{

			$isAre	=	'is';
			$plural	=	'';

	}


	// MAKE THE TITLE...
	$style		=	'text-align:right; border-top: 1px solid #ddddbb; padding: 5px; padding-top: 12; margin-top: 7px; color: #666666; font-size:11px; font-weight: normal; width:75%';
	$output		=	"<div align='right'>\r\n\r\n<div style='$style'>There $isAre $num user comment$plural on this story... $link</div>\r\n\r\n";

	// ADD EACH COMMENT...
	$output		.=	"<div class='issueComments' id='issueComments$storyNumber'>";

	if (is_comments_display_disabled($articleId))	{

		$output		.=	"<div style='text-align: center; font-weight: bold; font-size: 18px; COLOR: #999999; padding-top: 25px'>COMMENTS HAVE BEEN DISABLED</div>";

	}	else	{

		foreach ($comments as $key => $comment)	{

				$style		=	'text-align:right; border-top: 1px solid #ccccbb; padding: 5px; margin-top: 7px; color: #666666; font-size:11px; font-weight: bold; width:75%';
				$output 	.=	"<div style='$style'>Added on ".$datetimes[$key]." by ".$authors[$key]."</div>\r\n";
				$style		=	'text-align:left; padding: 5px; color: #666666; font-size:11px; font-weight: normal; width:75%';
				$output		.=	"<div style='$style'>".scanForLinks(nl2br($comment))."</div>";

		}

	}


	$output		.=	"<div style='text-align:left'>".showAddMessage($articleId).'</div>';
	$output		.=	'</div></div>';

	// RETURN THE OUTPUT...
	return $output;

}