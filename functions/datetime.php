<?php


if (!function_exists("make_date_from_sql"))	{
function make_date_from_sql($date)
	{
	
		$day		=		substr($date, 8, 2);
		$month		=		substr($date, 5, 2);

		switch ($month)	{
				
				case "01":
					$m		=	"Jan";
					break;
				case "02":
					$m		=	"Feb";
					break;
				case "03":
					$m		=	"March";
					break;
				case "04":
					$m		=	"Apr";
					break;
				case "05":
					$m		=	"May";
					break;
				case "06":
					$m		=	"June";
					break;
				case "07":
					$m		=	"July";
					break;
				case "08":
					$m		=	"Aug";
					break;
				case "09":
					$m		=	"Sep";
					break;
				case "10":
					$m		=	"Oct";
					break;
				case "11":
					$m		=	"Nov";
					break;
				case "12":
					$m		=	"Dec";
					break;
		}


			
		return		$day."-".$m;
	
	}
}


if (!function_exists("make_day_from_sql"))	{	
function make_day_from_sql($date)
	{
	
		$day		=		substr($date, 8, 2);
		if (substr($day, 0, 1) == "0") $day = substr($day, 1,1);
				
		return		$day;
	
	}
}
	
	

if (!function_exists("make_full_date_from_sql"))	{
function make_full_date_from_sql($date)
	{
	
		$day		=		substr($date, 8, 2);
		$month		=		substr($date, 5, 2);
		$year		=		substr($date, 0, 4);
		
		$output		=		$day."-".$month."-".$year;
		
		return $output;
	
	
	}
}



if (!function_exists("make_time_from_sql"))	{
function make_time_from_sql($date)
	{
	
		$minute		=		substr($date, 14, 2);
		$hour		=		substr($date, 11, 2);
		
		return	$hour.":".$minute;
		
	}
}


	
if (!function_exists("make_sql_datetime"))	{
function make_sql_datetime()
{

		$date 		=	date('Y')."-".date('m')."-".date('d');
		$time		=	date('H').":".date('i').":".date('s');
		
		return	$date." ".$time;
		
}
}



if (!function_exists("draw_date_selector"))	{
function draw_date_selector($date, $name, $split_lines)	{

	$date			=	clean_input($date);
	$split_lines	=	clean_input($split_lines);
	$name			=	clean_input($name);
	if ($date	==	0)	{
			$day		=	0;
			$month		=	0;
			$year		=	0;
		}
	else	{
			$day		=	substr($date, 0, 2);
			$month		=	substr($date, 3, 2);
			$year		=	substr($date, 6, 4);
		}
		
	
	?>
    
		D:
        <select name="<?php echo $name; ?>_day">
           	<option selected></option>
        	<?php
				for ($n	= 1 ; $n<= 31 ; $n ++)	{
					echo "<option";
					if ($day	==	$n) echo " selected ";
					echo ">$n</option>";
				}
			?>
        </select>
        &nbsp;&nbsp;
        M:
        <select name="<?php echo $name; ?>_month">
           	<option selected></option>       
        	<option value="01" <?php if ($month == "01") echo " selected "; ?>>Jan</option> 
        	<option value="02" <?php if ($month == "02") echo " selected "; ?>>Feb</option> 
        	<option value="03" <?php if ($month == "03") echo " selected "; ?>>Mar</option> 
        	<option value="04" <?php if ($month == "04") echo " selected "; ?>>Apr</option> 
        	<option value="05" <?php if ($month == "05") echo " selected "; ?>>May</option> 
        	<option value="06" <?php if ($month == "06") echo " selected "; ?>>Jun</option> 
        	<option value="07" <?php if ($month == "07") echo " selected "; ?>>Jul</option> 
        	<option value="08" <?php if ($month == "08") echo " selected "; ?>>Aug</option> 
        	<option value="09" <?php if ($month == "09") echo " selected "; ?>>Sep</option> 
        	<option value="10" <?php if ($month == "10") echo " selected "; ?>>Oct</option> 
        	<option value="11" <?php if ($month == "11") echo " selected "; ?>>Nov</option> 
        	<option value="12" <?php if ($month == "12") echo " selected "; ?>>Dec</option> 
		</select>
        &nbsp;&nbsp;
        Y:
        <select name="<?php echo $name; ?>_year">
           	<option selected></option>        
        	<?php
        		$l		=	date('Y');
        		$h		=	$l + 4;
				for ($n	=	$l ; $n <= $h ; $n++)	{
					
						echo "<option";
						if ($year == $n) echo " selected ";
						echo ">$n</option>";
					}
			?>
         </select>
         
    <?php     
    
	return TRUE;
	
}
}




if (!function_exists("make_datetime_from_data"))	{
function make_datetime_from_data($d, $m, $y)	{

	if ($d == "") 	return NULL;
	if ($m == "") 	return NULL;
	if ($y == "") return NULL;
	
	$date		=	"$y-$m-$d 00:00:00";
	
	return $date;
	
}
}



if (!function_exists("isValidDatetime"))	{
function isValidDateTime($dateTime)
{
    if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
        if (checkdate($matches[2], $matches[3], $matches[1])) {
            return true;
        }
    }

    return false;
}
}



?>