<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchMOVIES - direct action videos by the video collection of SchNEWS</title> <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="Feature article by SchNEWS the free weekly direct action newsheet from Brighton, UK. Going back to issue 100, some available as PDF" name="description">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/schmovies.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
    <div class="bannerGraphic"> <a href="../schmovies/index.php#raiders2"><img alt="SchMOVIES" border="0" height="90" src="images/schmovies-banner-raiders2.jpg" width="700"></a> </div>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarSchMovies.php"-->
</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
    MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
    <p><font face="Arial, Helvetica, sans-serif" size="2"><b> <a href="/index.htm">Home</a>
      | SchMOVIES </b></font></p>
    <p><font face="Arial, Helvetica, sans-serif"><b><font size="4">SchNEWS
goes to the movies!</font></b></font></p><p><font face="Arial, Helvetica, sans-serif">These
films are &#39;information for action&#39; - to inspire or inform you to get involved
in the issues covered in them, produced in Brighton by SchMOVIES (apart from a
couple). If you are involved in DIY video and have films you&#39;d like to put here,
get in <a href="mailto:schnews@riseup.net">contact</a>. Listed below are
the most recent films - see<b> </b></font><font face="Arial, Helvetica, sans-serif">the
rest in the category list on the lefthand column </font></p>

    <hr>
    <!--  BEGIN RADIERS 2 AD -->
    <table align="right" height="131" width="100">
      <tr>
        <td height="98">
          <div align="center"><font size="4"><b><a href="images/raiders2-1000.jpg" target="_blank"><img alt="Raiders Of The Lost Archive 2008-11 Vol 2" border="0" height="198" src="images/raiders2-140.jpg" style="margin-left: 15px" width="140"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/garden-720.jpg" target="_blank"><br>
            <font size="1">click here for larger image </font></a></font></div>
        </td>
      </tr>
    </table>
    <p><b><font size="4"><b><a name="raiders2"></a></b>NEW DVD OUT NOW - RAIDERS
      OF THE LOST ARCHIVE (Vol 2)</font> </b><br>
      <b>Part two of the SchMOVIES collection 2008-2011</b></p>
    <p>This second volume of the Raiders compilation includes the
      final films and footage which had been confiscated for up to eighteen months
      by police following the raid of a SchMOVIES film-maker in June 2009.</p>
    <p>Presented here, amongst others, is the film that instigated the raid in
      the first place, Batons nBombs, as well as others
      covering the dramatic conclusion to the EDO Decommissioners trial, the rise
      and fall of the much-loved Lewes Road Community Garden in Brighton, several
      recent actions from UK Uncut and more. Once again we have three SchMUSIC
      vids as well - big thanks to all the bands who contributed. </p>
    <p>Available Now - £6 incl P&amp;P</p>
    <p>
	 </p>

    <hr>
    <!-- END RAIDERS 2 AD -->
    <p>
      <!-- SchMOVIES Screenings -->
      <a name="screening"></a> </p>
<table border="0" cellpadding="6" height="194" width="100%">
<tr><td height="96" style="border: 2px solid #333333; padding: 6px" valign="top"><p><font face="Arial, Helvetica, sans-serif" size="4"><b>SchMOVIES SCREENINGS</b></font><br>There will be monthly film nights in 2011 on the last Wednesday of every month at 8pm at the Cowley Club, 12 London Rd, Brighton BN1 4JA - free/donation. For more info see <a href="../pap/index.htm">SchNEWS Party &amp; Protest Listings</a> and <a href="http://www.cowleyclub.org.uk" target="_blank">www.cowleyclub.org.uk</a></p></td></tr>
<!-- SchMOVIES Item - 30th November 2011  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/chumbawambascreeningposter-600.jpg" target="_BLANK"><img align="right" height="198" src="images/chumbawambascreeningposter-140.jpg" width="140"></a>
<b>Wednesday, 30th November 2011, 8pm</b>
</p><p><font size="4"><b>SchMOVIES presents a screening of WELL DONE, NOW SOD OFF! The Chumbawamba documentary.</b></font><br><br>
</p><p>Introduced by special guest speaker and former band member: DUNSTAN BRUCE.
</p><p>This is the potted and often hilarious history of the anarcho-punk-pop-media pranksters that are... Chumbawamba.
</p><p>The documentary includes footage and interviews from the last two decades as they retrace the early days of communal living and involvement in the miners&#39; strike of 84-85, their survival through the Thatcher years and a barrage of bad press, surfacing in the nineties wiser but unrepentant.
</p><p>This is the unlikely tale of a band who spent more time shoplifting than jamming and somehow managed to scrape a living from making radical music in a conservative culture.
</p><p>As A&amp;R men reminisce about the band refusing to accept anything but the money from the record company, Chumbawamba emerge as a groupof anarchist pranksters who are both loved and loathed in equal measures.
</p><p>With an uncanny knack of upsetting people and &#39;shooting themselves in the foot&#39; they surprised themselves and the rest of the world by selling 5 million albums in America. Tagged &#39;One Hit Wonders&#39; they ensured they&#39;d never be invited.
</p><p>Former band member Dunstan Bruce, will be taking questions after...
</p><p><b>Wed 30th November, 8pm. Cowley Club, 12 London Road, Brighton, Donation of \A33</b>
</p>
</td></tr>
<!-- SchMOVIES Item - 5th December 2011  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/scenesfromateenagekilling-600.jpg" target="_BLANK"><img align="right" height="198" src="images/scenesfromateenagekilling-140.jpg" width="140"></a>
<b>Monday, 5th December 2011, 8pm</b>
</p><p><font size="4"><b>SchMOVIES presents SCENES FROM A TEENAGE KILLING...</b></font><br><br>
</p><p>In 2009, 45 teenagers were killed by violence in Britain. Here are their names and some of their stories....
</p><p>Bafta-winning director Morgan Matthews&#39;s landmark film exploring the impact of teenage killings on families and communities across Britain, an emotional journey that chronicles every teenager who died as a result of violence in 2009 in the UK.
</p><p>Harrowing actuality filmed in the immediate aftermath combines with moving testimony from the spectrum of people affected in the wake of violent death. Filmed over eighteen months, this epic documentary is one of the most ambitious films to date about youth violence.
</p><p>The film questions society&#39;s attitudes towards young people while probing the meaning behind terminology such as &#39;gang violence&#39; or &#39;gang-related&#39; often used in connection with teenage killings. It reveals the reality of the teenage murder toll across one year, connecting the viewer with the people behind the headlines and the emotional consequences of violent death. Differing perspectives from families, friends, passers-by and the police are explored with intimacy and depth. Together they reflect the collective impact of a teenage killing on an entire community.
</p><p>Traveling the length and breadth of Britain, the film meets people of different religion, race and class. It tells the story of Shevon Wilson, whose misreported murder divided a community; the teenage girl who discovered she was pregnant to her boyfriend shortly after he was stabbed to death; the nurse who fought to save a dying teenager who was stabbed outside her home; and the outspoken East End twins who lost a mother and daughter in the same attack.
</p><p>Scenes from a Teenage Killing is a poignant and brutal reminder of the needless waste of young potential.
</p><p><b>Monday 5th December, @ The Cowley Club, 12 London Road, Brighton, 8pm, Free</b>
</p><p><b>Dir:</b> Morgan Matthews, Running Time: 2 hrs
</p>
</td></tr>
<!-- SchMOVIES Item - 25th January 2012  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/attitude-600.jpg" target="_BLANK"><img align="right" height="198" src="images/attitude-140.jpg" width="140"></a>
<b>Wednesday, 25th January 2012, 8pm</b>
</p><p><font size="4"><b>SchMOVIES presents PUNK:ATTITUDE The first of this years monthly punk screenings..</b></font><br><br>
</p><p>Punk: Attitude is a documentary on the history of punk rock in the USA and UK. The film traces the different styles of punk from their roots in 60s garage and psychedelic bands (Count Five, the Stooges) through glam-punk (New York Dolls) to the 70s New York and London scenes and into the hardcore present. Interviews with many of the musicians are edited with live clips and historical footage.
</p><p>The film begins showing the roots of punk music with many views on various artists and genres who accentuated the beginning of the genre, like the MC5 and the Velvet Underground. Punk: Attitude then proceeds chronologically to sort through the various artists and alumni who were central to the movement, drawing light on the general idea or &quot;Attitude&quot; of the punk movement, which spoke out for a generation. Bands such as The Ramones, The Stooges, The Clash and The Sex Pistols feature prominently throughout. The movie offers a canvas of praise and respect given from many interviewees as these bands are heralded commonly as the beginning of Punk progressively through the movie. Rare concert footage and personal accounts of gigs and band meetings highlight the aggression and destructive entities with surprising accuracy. The movie wraps up by emphasizing the influence that punk has on modern music.
</p><p>SchMOVIES punk documentary screenings are on the last Wed of each month at the Cowley Club click on www.schnews.org.uk/schmovies for listings.
</p><p><b>Free/Donation</b>
Dir: Don Letts, 2005 (90 mins)
</p>
</td></tr>
<!-- SchMOVIES Item - 13th February 2012  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/fastline-600.jpg" target="_BLANK"><img align="right" height="198" src="images/fastline-140.jpg" width="140"></a>
<b>Monday, 13th February 2012, 8pm</b>
</p><p><font size="4"><b>SchMOVIES presents an archive screening of LIFE IN THE FAST LANE - The No M11 Story</b></font><br><br>
</p><p>This feature length documentary presents the inside story of the No M11 Campaign, recounting 15 months of direct action against one of the most controversial schemes in the history of British road-building.
</p><p>1994. London. On the rooftops of Claremont Road, 300 protesters waited for the start of what was to become the most extraordinary eviction in British history. Nearby, two diggers stood poised to demolish the last remaining houses in the path of the M11 Link Road. By 1:30 pm a convoy of 120 vehicles containing 700 police and 400 private security guards had arrived in the area. By 2 pm Operation Garden Party had begun....
</p><p>Drawing on personal testimony and nearly 200 hours of front-line footage, LIFE IN THE FAST LANE features the battle for Wanstead&#39;s George Green, and the subsequent eviction of its 250 year- old Sweet Chestnut tree. It re-lives the days of the Independent Free Area of Wanstonia, and highlights the celebrated rooftop protests at Westminster and the home of the then Transport Secretary, John MacGreggor. Against a backdrop of growing resistance to the Criminal Justice Act it charts the emergence of CLAREMONT ROAD as an extraordinary symbol of cultural defience, and for the first time tells the story of what became the most expensive eviction in British history.
</p><p>Featuring the music of The Levellers, Zion Train and The Clash, this film is a celebration of dignity in the face of oppression, providing a lasting testament to the strength of people-power and the resilience of community spirit.
</p><p><i>&quot;Excellent...&quot;</i> JOHN PILGER
</p><p><i>&quot;Brilliant. It both inspired me and gave me a much needed boost in energy and commitment.&quot;</i> STEVE PLATT, New Statesman
</p><p><i>&quot;The struggle of people against power, is the struggle of memory against forgetting.&quot;</i> MILAN KUNDERA
</p><p><b>Free/Donation</b>
</p><p>Dir: Mayyasa Al-Malazi &amp; Neil Goodwin, (85mins)
</p>
</td></tr>
<!-- SchMOVIES Item - 29th February 2012  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/hawkwind-600.jpg" target="_BLANK"><img align="right" height="198" src="images/hawkwind-140.jpg" width="140"></a>
<b>Wednesday, 29th February 2012, 8pm</b>
</p><p><font size="4"><b>SchMOVIES presents an evening of psychedelic space rock with THIS IS HAWKWIND DO NOT PANIC - The Documentary...</b></font><br><br>
</p><p>Also with special guest speaker NIK TURNER (vocalist, sax and flute from Hawkwind)
</p><p>Nik will be introducing the film and taking questions after.
</p><p>The inside story of Hawkwind, one of Britain&#39;s wildest acid rock bands. Emerging from the Ladbroke Grove underground at the end of the 60s, the band trailed radicalism and counter-culture in their wake, and have been a direct influence on punk, metal, dance and rave.
</p><p>Includes interviews with some of the band&#39;s enduring legends, including bassist Lemmy, writer Michael Moorcock, founder members Terry Ollis, Nik Turner and Mick Slattery, and former managers Doug Smith and Jeff Dexter.
</p><p>Suggested donation of \A34 to cover travel expenses.
</p><p>BBC4 (60 mins)
</p>
</td></tr>
<!-- SchMOVIES Item - 28th March 2012  -->
<tr><td style="border: 2px solid #333333; padding: 6px"><p><a href="images/nancy-600.jpg" target="_BLANK"><img align="right" height="198" src="images/nancy-140.jpg" width="140"></a>
<b>Wednesday, 28th March 2012, 8pm</b>
</p><p><font size="4"><b>SchMOVIES goes punk conspiracy with this surprising slice of punk investigative journalism...</b></font><br><br>
</p><p><b>The Guardian - 20/1/09
<br><i>&quot;After 30 years, a new take on Sid, Nancy and a punk rock mystery&quot;</i></b>
</p><p>On October 12th 1978 New York Police discovered the lifeless body of a 20 year-old woman, slumped under the bathroom sink in a hotel room. She was dressed in her underwear and had bled to death from a stab wound.
</p><p>The woman was Nancy Spungen, an ex-prostitute, sometimes stripper, heroin addict, and girlfriend of Sex Pistols&#39; bassist Sid Vicious.
</p><p>In a trial by tabloid newspapers Vicious was pronounced guilty before noon the following day. But the case never had the chance to be brought to trial, and a number of New York cops weren&#39;t convinced. Less than six months later in a flat in New York&#39;s Greenwich Village, Sid, himself aged only 21, died of a heroin overdose.
</p><p>For the next 28 years the assumption was that Sid did it - case closed. Over time, the death of Sid and Nancy has passed into rock legend and has only added to the controversial and notorious image of the Sex Pistols and punk music.
</p><p>At the request of Sid&#39;s mother, who committed suicide in 1996, rock author and punk expert Alan Parker has devoted himself to discovering what really happened in room 100. Parker has re-interviewed 182 people, re-examined NYPD evidence, and gone back to his original interviews with Sid&#39;s mother.
</p><p>with interviews including Glen Matlock, Don Letts, Alan Jones, Peter Kodick, Steve &#39;Roadent&#39; Connolly, John &#39;Boogie&#39; Tiberi, Simone Stenfors, Alan Parker, Steve Walsh, Steve Dior, George X, John Holmstrom, Eileen Polk and Howie Pyro.
</p><p><b>Free/Donation</b>
</p><p>Dir Alan.G Parker, 2010 (90mins)
</p>
</td></tr>
<!-- End of SchMOVIES Screenings -->
</table>
    <!--RAIDERS OF THE LOST ARCHIVE Vol 1-->
    <table width="100%">
      <tr>
        <td>
          <table align="right" height="131" width="100">
<tr>
  <td height="98">
    <div align="center"><font size="4"><b><a href="images/raiders-1000.jpg" target="_blank"><img alt="Raiders Of The Lost Archive 2009/10 Vol 1" border="0" height="199" src="images/raiders-140.jpg" style="margin-left: 15px" width="140"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/garden-720.jpg" target="_blank"><br>
      <font size="1">click here for larger image </font></a></font></div>
  </td>
</tr>
          </table>
          <p><b><font size="4"><b><a name="raiders"></a></b>RAIDERS OF THE LOST
ARCHIVE (Vol 1)</font> </b><br>
<b>Part one of the SchMOVIES collection 2009-2010</b></p>
          <p>This DVD features a number of films which were held by Sussex police
for over a year following the raid and confiscation of all SchMOVIES
equipment during an intelligence gathering operation in June 2009
related to the <a href="http://www.smashedo.org.uk" target="_blank">Smash
EDO</a> campaign.</p>
          <p>The films in this collection include demonstrations against the Israeli
bombing of Gaza in early 2009, the <a href="../archive/news729.htm">EDO
Decommissioners trial</a>, Smash EDO protests and more direct action
on the south coast... PLUS music clips of some of the bands who have
performed at recent SchNEWS/SchMOVIES benefit gigs including Brighton&#39;s
finest and perennial festival fave &#39;Tragic Roundabout&#39;.</p>
<p><b>Available Now:</b></p>
			<p> </p>
			</td></tr></table>



    <hr>
<!-- SchNIPPETS - LEWES ROAD COMMUNITY GARDEN -->
<table width="100%"><tr><td>
<table align="right" height="131" width="41"> <tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/garden-720.jpg" target="_blank"><img border="0" height="176" src="images/garden-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/garden-720.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table>
<p><b><font size="4"><b><a name="garden"></a></b>SchNIPPETS - Lewes Rd
Community Garden In Court</font> </b><br><b>Brighton, June 2010, 5.06 mins</b></p><p>The
much loved Lewes Rd Community Garden in Brighton finally get it&#39;s day in court.
After a year this guerilla garden is under threat from developers who want to
turn the land into another TESCOs and a betting shop. This was stage one of their
master plan.... <br> </p><p>Download hi-res Mpeg (72 meg)<a href="http://schnews.clearerchannel.org/schnippetgardencourtavi_mpeg2video.mpg" target="_blank">
click here</a><br> Download low-res FLV (21 meg) <a href="http://schnews.clearerchannel.org/schnippetgardencourtavi.flv" target="_blank">click
here</a></p><p>For more coverage on the occupied garden see <a href="../archive/news6774.php">SchNEWS
677</a>, <a href="../archive/news6807.php">680</a><br>
See also <a href="http://lewesroadcommunitygarden.webs.com" target="_blank">http://lewesroadcommunitygarden.webs.com</a></p>
</td></tr></table>
<hr>
<!-- SchNIPPETS - THE DECOMMISSONERS TRIAL -->
<table width="100%"><tr><td>
<table align="right" height="131" width="41"> <tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/decomcourt-720.jpg" target="_blank"><img border="0" height="176" src="images/decomcourt-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/decomcourt-720.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table>
<p><b><font size="4"><b><a name="garden"></a></b>SchNIPPETS - The Decommissoners
Trial <br> </font> Day One outside the court Brighton, June 2010, 3.16min<br>
</b></p><p>Short reportage of day one of the Decommisonners trial, where nine
activists are on trial for smashing up the Brighton based bomb component factoty,
EDO/ITT in Januray 2009. The trial is not so much if they are guilty but if EDO/ITT
are guilty of illegally arming the Israeli military...</p><p>Download hi-res Mpeg
(46 meg)<a href="http://schnews.clearerchannel.org/snippet-decomsdayone_mpeg2video.mpg" target="_blank">
click here</a><br> Download low-res FLV (320K) <a href="http://schnews.clearerchannel.org/snippet-decomsdayone.flv" target="_blank">click
here</a></p><p>See also <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>
<a href="http://decommissioners.co.uk/" target="_blank">http://decommissioners.co.uk</a>
</p>
</td></tr></table>
<hr>
<!-- Madness (English Defence League) 25 April 2010          -->
<table width="100%"><tr><td><table align="right"><tr><td><div align="center"><font size="4"><b><a href="images/madness-720.jpg" target="_blank"><img border="0" height="175" src="images/madness-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/madness-720.jpg" target="_blank"><br><font size="1">click here for larger image </font></a></font></div></td></tr></table><p><b><font size="4"><b><a name="madness"></a></b>
 Madness (English Defence League)</font></b><br>
 <b>Brighton, 25th April 2010, 10.35 mins</b></p>
 <p>The frankly barmy March For England returns to the streets of Brighton to celebrate St Georges Day but this year is infiltrated by the English Defence League.<br>
The footage from this film was shot by those attending the first SchMOVIES Video Activist Course and considering some had never been in a situation like this with a camera before they got some good results...
</p>
<br>
 <p>Download hi-res Mpeg
 (? meg)<a href="http://schnews.clearerchannel.org/madness.mpg" target="_blank">click here</a><br>Download low-res FLV
 (? meg) <a href="http://schnews.clearerchannel.org/madness.flv" target="_blank">click here</a></p>
</td></tr></table>
<hr>
<!-- EVERY FUCKING WEEK! -->
<table width="100%"><tr><td>
<table align="right" height="131" width="41"> <tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/week-lg.jpg" target="_blank"><img border="0" height="176" src="images/week-sm.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/week-lg.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table>
<p><b><font size="4"><b><a name="garden"></a></b>Every Fucking Week!<br>
</font>Footage of Smash EDO demo, Brighton, April 2010, 7.43 min<br> </b></p><p>Short
reportage of a recent incident at the bomb component making factory EDO/ITT in
Brighton during one of the regular weekly noise demos. Just goes to show how much
a few bods and a camera can pile on the pressure... <br> </p><p>Download hi-res
Mpeg (108 meg)<a href="http://schnews.clearerchannel.org/every-fucking-week.mpg" target="_blank">
click here</a><br> Download low-res FLV (33 meg) <a href="http://schnews.clearerchannel.org/every-fucking-week.flv" target="_blank">click
here</a></p><p>See also <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>
</p><p><br> </p>
</td></tr></table>
<hr>
<!-- IMAGINE A GARDEN -->
<table width="100%"><tr><td>
<table align="right" height="131" width="41"> <tr> <td height="189">
<div align="center"><font size="4"><b><a href="images/imagine-a-garden-lg.jpg" target="_blank"><img alt="Shut ITT - Smash EDO, October 08" border="0" height="176" src="images/imagine-a-garden-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/imagine-a-garden-lg.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table>
<p><b><font size="4"><b><a name="garden"></a></b>Imagine A Garden<br>
</font>The Story Of The Lewes Rd Community Garden, Brighton, June 2009, 15:04
min<br> </b></p><p>Documenting the creation of the Lewes Rd Community Garden in
Brighton which was squatted on the site of a disused ESSO garage by local residents
in June 2009. The occupation is ongoing despite TESCOs claiming the land and wanting
to build another store there... !!<br> </p><p>Download hi-res Mpeg1 (157 meg)<a href="http://schnews.clearerchannel.org/imagine-a-garden.mpg" target="_blank">
click here</a><br> Download low-res FLV (65 meg) <a href="http://schnews.clearerchannel.org/imagine-a-garden.flv" target="_blank">click
here</a></p><p>For more coverage on the occupied garden see <a href="../archive/news6774.php">SchNEWS
677</a>, <a href="../archive/news6807.php">680</a><br>
See also <a href="http://lewesroadcommunitygarden.webs.com" target="_blank">http://lewesroadcommunitygarden.webs.com</a></p>
</td></tr></table>
<hr>
<!-- May The Fourth Be With You (Smash EDO)  May Day 2009    -->
<table width="100%"><tr><td><table align="right"><tr><td><div align="center"><font size="4"><b><a href="images/mayday-720.jpg" target="_blank"><img border="0" height="147" src="images/mayday-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/mayday-720.jpg" target="_blank"><br><font size="1">click here for larger image </font></a></font></div></td></tr></table><p><b><font size="4"><b><a name="garden"></a></b>
 May The Fourth Be With You (Smash EDO)</font></b><br>
 <b>Brighton, May Day 2009, 12.27 mins</b></p>
 <p>The campaign to shut down the Brighton based bomb component factory takes to the streets in this whistle stop march of the companies and institutions that support ITT/EDO.
</p>
<br>
 <p>Download hi-res Mpeg
 (? meg)<a href="http://schnews.clearerchannel.org/mayday.mpg" target="_blank">click here</a><br>Download low-res FLV
 (? meg) <a href="http://schnews.clearerchannel.org/mayday.flv" target="_blank">click here</a></p>
<p>For more coverage on the Smash EDO campaign see <a href="http://www.smashedo.org">www.smashedo.org</a></p>
</td></tr></table>
<hr>
<div style="height: auto"> <table align="right" height="93" width="51"> <tr> <td height="187">
<div align="center"><font size="4"><b><a href="images/rftv-1000.jpg" target="_blank"><img alt="Reports From The Verge" border="0" height="198" src="images/rftv-140.jpg" style="margin-left: 15px" width="140"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table>
      <p><font size="4"><b><font size="4"><b><a name="rftv"></a></b></font>REPORTS
        FROM THE VERGE<br>
        </b></font><b>Smash EDO/ITT Anthology 2005-2009</b></p>
      <p>A new collection of twelve SchMOVIES covering
the Smash EDO/ITT&#39;s campaign efforts to shut down the Brighton based bomb factory
since the company sought its draconian injunction against protesters in 2005.
This series of films illustrate what lengths EDO and Sussex Police will go to
stop the campaign and how Smash EDO hit back.</p><p><b>Available now</b> </p><p> </p>Or send cheques for the above amount made to &#39;Justice&#39; to the
address at the bottom of this page... <p><b>For more details <a href="../pages_merchandise/merchandise_video.php#rftv">click
here</a></b></p><hr> <b><font size="4"></font></b> <table align="right" height="131" width="41">
<tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/gaza-london-lg.jpg" target="_blank"><img alt="Gaza protests in London , Jan 09" border="0" height="176" src="images/gaza-london-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><b><font size="4"><b><a name="conflict"></a></b>Conflict Alert</font><br>
Demo in support of Gaza, January 2009, 6:37 min</b></p><p>Reportage from one of
the big London marches and the civil disobedience and outrage that followed when
Israel bombed the Gaza strip massacring hundreds during late 2008-Jan 2009.</p><p>
Download hi-res Mpeg1 (68 meg) <a href="http://schnews.clearerchannel.org/conflict-alert.mpg" target="_blank">click
here</a><br> Download low-res FLV (28 meg) <a href="http://schnews.clearerchannel.org/conflict-alert.flv" target="_blank">click
here</a></p><p>For more coverage on the Israeli attacks on Gaza in January 2009
see <a href="../archive/news661.htm">SchNEWS 661</a>, <a href="../archive/news662.htm">662</a>,
<a href="../archive/news663.htm">663</a> <br> See also
<a href="http://talestotell.wordpress.com" target="_blank">http://talestotell.wordpress.com</a></p><hr>
<b><font size="4"></font></b> <table align="right" height="131" width="41"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/humanity-gaza-lg.jpg" target="_blank"><img alt="Gaza protests in Brighton, Jan 09" border="0" height="176" src="images/humanity-gaza-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><b><font size="4"><b><a name="humanity"></a></b>Humanity</font><br>
Brighton Gaza Demo, January 2009, 6:41 min</b></p><p>Another film the cops took
in the raid (see <a href="../archive/news680.htm">SchNEWS
680</a>). The Brighton leg of solidarity marches for the Gaza people being bombed
to hell by Israel at the start of 2009.<br> WARNING! This film does contain some
graphic Gaza casualty footage!</p><p>Download hi-res Mpeg1 (60 meg) <a href="http://schnews.clearerchannel.org/humanity.mpg" target="_blank">click
here</a><br> Download low-res FLV (29 meg) <a href="http://schnews.clearerchannel.org/humanity.flv" target="_blank">click
here</a></p><p>For more coverage on the Israeli attacks on Gaza in January 2009
see <a href="../archive/news661.htm">SchNEWS 661</a>, <a href="../archive/news662.htm">662</a>,
<a href="../archive/news663.htm">663</a> <br> See also
<a href="http://talestotell.wordpress.com" target="_blank">http://talestotell.wordpress.com</a></p><hr>
<b><font size="4"></font></b> <table align="right" height="131" width="41"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/shut-itt-smash-edo-lg.jpg" target="_blank"><img alt="Shut ITT - Smash EDO, October 08" border="0" height="176" src="images/shut-itt-smash-edo-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><b><font size="4"><b><a name="shut-itt"></a></b>Watch ITT!<br> </font>Brighton
Shut ITT! Demo, October 15th 2008, 13:23 min<br> </b></p><p>One of the SchMOVIES
that Sussex Police took away in the raid (see <a href="../archive/news680.htm">SchNEWS
680</a>). Reportage from the second mass nationwide demo against the bomb component
making EDO/ITT factory in Brighton. The film is also meant to act as a kind of
caution for those that put political direct action films up on the web without
checking the content through first...SchMOVIES are routinely downloaded by the
Police...for our own protection of course... so Watch ITT!!!<br> </p><p>Download
hi-res Mpeg1 (136 meg) <a href="http://schnews.clearerchannel.org/watch-itt.mpg" target="_blank">click
here</a><br> Download low-res FLV (57 meg) <a href="http://schnews.clearerchannel.org/watch-itt.flv" target="_blank">click
here</a></p><p>For more coverage on the Shut ITT demo on October 15th 2008 <a href="../archive/news651.htm">SchNEWS
651</a> <br> See also <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a></p></div><hr>
<div style="height: auto"> <table align="right" height="131" width="41"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/uncertified-600.jpg" target="_blank"><img alt="Uncertified - SchMOVIES collection 2008" border="0" height="197" src="images/uncertified-140.jpg" style="margin-left: 15px" width="140"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><a name=""></a>UNCERTIFIED<br> </b></font><b> SchMOVIES
2008 DVD Collection &#39;Uncertified&#39;</b></p><p>Films on this DVD include... The saga
of On The verge  the film they tried to ban, the Newhaven anti-incinerator
campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton
church, Titnore Woods update, protests against BNP festival and more...</p><p>
</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/uncertified-trailer.mpg" target="_blank">click
here to download</a></b> - 39 sec, mpg format, 4.9 meg, Jan 2009.<br> </font><font face="Arial, Helvetica, sans-serif" size="2"><b>To
download &#39;Uncertified&#39; on bit torrent</b> (900meg) <a href="http://onebigtorrent.org/torrents/5221/UNCERTIFIED" target="_blank">click
here</a></font><font face="Arial, Helvetica, sans-serif"> </font></p></div><hr>
<div style="height: 220px"> <table align="right" height="93" width="160"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/time-out-at-titnore-lg.jpg" target="_blank"><img alt="Time Out At Titnore" border="0" height="176" src="images/time-out-at-titnore-sm.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><a name="titnore"></a>Time Out At Titnore<br> </b></font><b>Titnore
Protest Camp, West Sussex</b></p><p>Update at Titnore Woods Protest Camp in Worthing
now in it&#39;s third year where the forces of darkness want to trash ancient woodland
to build a new housing development and another bloody Tescos.</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/time-out-at-titnore.mpg" target="_blank">click
here to download</a></b> - 4:29 min, mpg1 format, 34 meg, September 2008.</font></p><p><b>Campaign
website</b> -<a href="http://www.protectourwoodland.co.uk" target="_blank"> www.protectourwoodland.co.uk</a><br>
See also<i> Rough Music #20</i> - <b>1,000 Days And Counting</b> -<a href="http://www.roughmusic.org.uk/rm20.html#3" target="_blank">
www.roughmusic.org.uk/rm20.html#3</a></p></div><hr> <div style="height: 220px">
<table align="right" height="93" width="160"> <tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/little-britain-lg.jpg" target="_blank"><img alt="Wasters" border="0" height="176" src="images/little-britain.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><a name="little-britain"></a><font size="4"></font>Little
Britain<br> </b></font><b> BNP &#39;Festival&#39; protests</b></p><p>The far-right British
National Party held their second festival (yes, that&#39;s right the BNP
hold festivals now) in Derbyshire in August 2008. Only one person turned up last
year to protest against this fascist festival - this year, however, was a different
story.</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/little-britain.mpg" target="_blank">click
here to download</a></b> - 8:07 min, mpg1 format, 61 meg, August 2008.</font></p><p>See
<b>Red, Blue &amp; Whitey </b>in <a href="../archive/news6433.htm">SchNEWS
643</a><br> <b>Antifa </b>- <a href="http://www.antifa.org.uk" target="_blank">www.antifa.org.uk</a></p></div><hr>
<div style="height: 220px"> <table align="right" height="93" width="160"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/wasters-lg.jpg" target="_blank"><img alt="Wasters" border="0" height="176" src="images/wasters-sm.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><a name="wasters"></a>Wasters! <br> </b></font><b>Newhaven
anti-incinerator campaign</b></p><p>With democratic resistance thwarted and the
go ahead given for the Newhaven Incinerator to be built, the campaign to stop
this monstrous toxic facility is stepped up.</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/wasters.mpg" target="_blank">click
here to download</a></b> - 8:40 min, mpg1 format, 66 meg, July 2008.</font></p><p><b>Campaign
website</b> -<a href="http://stopincinerationnownetwork.wordpress.com" target="_blank">
http://stopincinerationnownetwork.wordpress.com</a></p></div><hr> <div style="height: 220px">
<table align="right" height="93" width="160"> <tr> <td height="98"> <div align="center"><font size="4"><b><a href="images/trespasses-lg.jpg" target="_blank"><img alt="Forgive Us Our Trespasses" border="0" height="176" src="images/trespasses.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><font size="4"></font><a name="trespasses"></a>Forgive
Us Our Trespasses<br> </b></font><b>London Rd, Brighton church squat</b></p><p>Christian
charity is put to the test when squatters occupy a disused church on London Rd,
Brighton as part of a global days of action for squats and autonomous spaces.<br>
</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/forgive-us-our-trespasses.mpg" target="_blank">click
here to download</a></b> - 9:43 min, mpg1 format, 61 meg, May/July 2008.</font></p><p>See
<b>Pew Crew</b> in <a href="../archive/news6384.htm">SchNEWS
638</a><br> <b>Advisory Service For Squatters</b> - <a href="http://www.squatter.org.uk" target="_blank">www.squatter.org.uk</a></p></div><hr>
<div style="height: 220px"> <table align="right" height="93" width="160"> <tr>
<td height="98"> <div align="center"><font size="4"><b><a href="images/batonsnbombs-900.jpg" target="_blank"><img alt="Bombs&#39;n&#39;Batons - Smash EDO&#39;s Red Wednesday, June 4th 2008" border="0" height="161" src="images/batonsnbombs-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><font size="4"><b><a></a>Batons &#39;n&#39; Bombs<br> </b></font><b> Carnival
Against The Arms Trade 4th June 2008</b></p><p>Coverage of &#39;Red Wednesday&#39;, Smash
EDO&#39;s mass nationwide mobilisation against the Brighton bomb making factory, recently
taken over by nazi scumbags ITT. With a large turnout and a minimal police presence
no one was sure what to expect.... and it turns out nor did the police.</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/batons-n-bombs-123meg.mpg" target="_blank">click
here to download</a></b> - 16.20 min, mpg1 format, 123 meg, July 2008.</font></p><p>For
report on &#39;Red Wednesday&#39;, see <a href="../archive/news6342.htm">SchNEWS 634</a></p><p>For
more about Smash EDO see <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a></p></div>
<hr>
<div style="height: 220px"> <table align="right"> <tr> <td height="123"> <div align="center"><font size="4"><b><a href="images/verging-1200.jpg" target="_blank"><img alt="Cinema Told To Pull Film - The Argus, 18th March 2008" border="0" height="146" src="images/verging-220.jpg" style="margin-left: 15px" width="220"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
<font size="1">click here for larger image </font></a></font></div></td></tr>
</table><p><b><font size="4">Verging On The Ridiculous<br> </font>SchMOVIE about
the Smash EDO campaign gets suppressed.</b></p><p> The story of the nationwide
police suppression of On The Verge, firstly at its Brighton premiere
and then as it toured around the country. </p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/verging-on-the-ridiculous.mpg" target="_blank">click
here to download</a></b> </font> - 8:22min, mpg format, 65 meg, March 2008. </p><p>See
<b>Showstoppers</b> in <a href="../archive/news625.htm">SchNEWS 625</a>, also
<a href="../schmovies/index-on-the-verge.htm" target="_blank">www.schnews.org.uk/schmovies/index-on-the-verge.htm</a>
</p></div><hr> <h3>On The Verge - OUT NOW</h3><p><font face="Arial, Helvetica, sans-serif" size="2"><b>Order
the DVD</b>. £6 incl. postage, with profits going to the Smash EDO Campaign.
Send a cheque written to &#39;Justice&#39; to the address below, and paypal will be set
up shortly - promise. </font></p><font face="Arial, Helvetica, sans-serif" size="2">
<p><font face="Arial, Helvetica, sans-serif" size="2"><b>Bit Torrent</b>
- Download the whole 90 minute film in fairly good quality (900meg mpg). To start
the download <a href="on-the-verge-schmovies-2008.mpg.torrent">click here</a>
and it will open in your bit torrent program. If you haven&#39;t got one, get the
free open source <a href="http://azureus.sourceforge.net" target="_blank">Azureus</a>.</font></p><hr>
<div style="height: 220px"> <p><font size="4"><b><img align="right" alt="On The Verge - The trailer" border="1" src="smash-edo-images/trailer-on-the-verge.jpg" style="margin-left:15px">On
The Verge  Trailer<br> </b></font><b> for the full-length Smash EDO campaign
film.</b></p><p><b>On The Verge </b> tells the story of one of the most persistent
and imaginative direct action campaigns to emerge out of the UK&#39;s anti-war movement.
</p><p><font face="Arial, Helvetica, sans-serif"><b><a href="http://enr.clearerchannel.org/media/schnews/on-the-verge-trailer.mpg" target="_blank">click
here to download trailer</a></b> - 1.30 min, MPEG1 format, 37.8meg, Dec 2007.
</font></p><p><font face="Arial, Helvetica, sans-serif">* For updates about <b>On
The Verge</b> and release information <a href="index-on-the-verge.htm">click here</a></font></p></div><table border="1" bordercolor="#000000" cellpadding="6" width="100%">
<tr> <td height="56"> <h3>Smash EDO Tour March-June 2008 </h3><p>The<b> Smash
EDO</b> campaign has begun a tour of the UK and screening <b>On The Verge</b>
- the new full length Smash EDO SchMOVIES film - at every venue. The tour will
give those involved in the campaign the chance to discuss tactics, get feedback,
and organise for the future. <br> <b>* Screenings are being closed by the police</b>,
including the premiere at the Duke Of Yorks Cinema in Brighton. Several venues
around the country due to host the tour have already been intimidated by police.
To read about it <a href="../archive/news625.htm">click here</a><br> * For current
dates and other tour info <a href="index-on-the-verge.htm#tour">click here</a></p></td></tr>
</table><hr> <table align="right" height="209" width="196"> <tr> <td height="262">
<div align="center"><a href="../schmovies/images/take-three-800.jpg" target="_blank"><img alt="Take Three - SchMOVIES DVD Collection 2007" border="0" src="../schmovies/take-three-200.jpg" style="margin-left:15px"><br>
<font size="1">click here for larger image</font> </a></div></td></tr> </table><p><font size="4"><b><a name="take-three"></a>TAKE
THREE  SchMOVIES Collection DVD 2007</b></font><br> <b>*** OUT NOW ***</b></p><p>This
is the third SchMOVIES DVD collection featuring short direct action films produced
by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash EDO, Naked Bike Ride,
The No Borders Camp at Gatwick, Class War plus many others.</p><b> <p>Get a copy
now... </p></b> <br> or, send a cheque written out to &#39;Justice&#39; to the address at the
bottom of the page...<br> <br> Please make sure to enter your shipping address
on paypal, as it doesn&#39;t give us this information automatically. <p><a href="http://enr.clearerchannel.org/media/schnews/take-three-trailer.mpg" target="_blank"><b>Click
here</b></a><b> to download the trailer</b> - (1.30mins, MPEG1 format, 16.2meg,
Dec 2007)</p><hr> <table align="right" height="77" width="173"> <tr> <td> <div align="center"><a href="v-for-video-activist-700.jpg" target="_blank"><img alt="V For Video Activist" height="198" src="v-for-video-activist-200.jpg" style="margin-left:15px" width="200"><br>
<font size="1">click here for larger image</font></a></div></td></tr> </table><p><font size="4"><b>V
For Video Activist<br> </b></font><b>SchMOVIES DVD Collection 2006</b></p><p>The
pick of the SchMOVIES produced in 2006, covering events across the UK such as
the Smash EDO campaign, protest camps at Titnore Woods and Shepton Mallet, the
Camp For Climate Action, anti-war demos at Parliament Square in the face of SOPCA
laws, community resistance to the M74 motorway in Glasgow, and more... One theme
that runs through these films is the blatant use of a &#39;political police force&#39;
to protect corporations and war criminals rather than uphold the law. But despite
this, people are prepared to take direct action and not let a police state goosestep
in. Remember... People should not be afraid of their governments. Governments
should be afraid of their people.</p><p><b>Temporarily out of stock - damn this
market based resource allocation system! Check again soon.</b></p><hr width="100%">
<div style="height: 200px"> <p><font face="Arial, Helvetica, sans-serif" size="4"><b><img align="right" alt="Crap Arrest Of The Week" border="1" height="176" src="craparrestcopper.jpg" style="margin-left:15px" width="220">Crap
Arrest Copper</b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2">Here
it is - that &#39;crap arrest&#39; moment we&#39;ve been searching for for ten years - courtesy
of a friendly copper at the 2005 Big Green Gathering...</font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="craparrestcopper.mov" target="_blank">Click
Here To Download</a> </b>(approx 30 secs, 1.3meg, Quicktime format, August 2005)</font><font face="Arial, Helvetica, sans-serif"><br>
</font> </p><br> </div><hr> <div style="height: 240px"> <table align="right" height="135" width="183">
<tr> <td> <div align="center"><a href="../schmovies/images/schmovies-dvd-2005-800.png" target="_blank"><img alt="SchMOVIES DVD Collection 2005" border="0" height="201" src="../schmovies/images/schmovies-dvd-2005-200.png" style="margin-left:15px" width="200"><br>
<font size="1">click here for larger image</font></a></div></td></tr> </table><p class="style1"><font face="Arial, Helvetica, sans-serif"><b><font size="4"><a name="schmoviedvd"></a>SchMOVIES
DVD - 2005</font></b></font></p><p><font face="Arial, Helvetica, sans-serif"><strong>AVAILABLE
NOW<br> </strong>PLACE YOUR ORDER NOW - £6 (including P&amp;P) Contact the
SchNEWS Office 01273 685913 or <a href="mailto:schnews@riseup.net">email</a></font></p><p><font face="Arial, Helvetica, sans-serif">An
eclectic collection spanning the G8 protests in Scotland this year, the SMASH
EDO campaign (with Mark Thomas), the CRE8 Summit and MAD PRIDE to name a few.
These are high resolution screening copies of some of the best SchMOVIES of 2005
(including some which not on this website) There are plenty of extras which also
include a SchNEWS at DSEI special. </font></p><p><font face="Arial, Helvetica, sans-serif"><strong>Buy
this and help keep SchNEWS free. </strong></font></p></div><hr> <p><font face="Arial, Helvetica, sans-serif" size="2"><b>If
you have footage of recent demos or positive projects on the go get in touch with
us. We can&#39;t promise to give all of them a SchNEWS film treatment but we&#39;ll have
a go. Contact us on the link below.</b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2">For
comments about these films please contact <a href="mailto:schnews@riseup.net">SchNEWS</a></font></p><p><img height="10" src="../images_main/spacer_black.gif" width="100%"></p></font>
</div>
  <div class="rightBar">
    <table bgcolor="#999999" border="0" height="197" width="140">
      <tr>
        <td height="217">
          <div align="center">
            <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="../pages_merchandise/merchandise_video.php#raiders"><img alt="Raiders Of The Lost Archive Vol 2" border="0" height="198" src="images/raiders2-140.jpg" width="140"></a><br>
              <b><font color="#000000">Raiders Of The Lost Archive Vol 2 - </font></b><font color="#000000">Part
              two of the SchMOVIES collection 2009-11<b> <br>
              OUT NOW<br>
              </b>* For details</font> <a href="../pages_merchandise/merchandise_video.php#raiders2">click
              here</a></font></p>
          </div>
        </td>
      </tr>
    </table>
    <hr>
    <table bgcolor="#999999" border="0" height="197" width="140">
      <tr>
        <td height="217">
          <div align="center">
<p><font face="Arial, Helvetica, sans-serif" size="2"><a href="../pages_merchandise/merchandise_video.php#raiders"><img alt="Raiders Of The Lost Archive Vol 1" border="0" height="199" src="images/raiders-140.jpg" width="140"></a><br>
  <b><font color="#000000">Raiders Of The Lost Archive Vol 1 - </font></b><font color="#000000">Part
  one of the SchMOVIES collection 2009/10<b> <br>
  OUT NOW<br>
  </b>* For details</font> <a href="../pages_merchandise/merchandise_video.php#raiders">click
  here</a></font></p>
          </div>
        </td>
      </tr>
    </table>
    <hr>
<br> <table bgcolor="#000000" border="0" height="71" width="140">
<tr>
        <td height="237">
          <div align="center"> <p><font face="Arial, Helvetica, sans-serif" size="2"><img alt="Reports From The Verge - Smash EDO/ITT Anthology 2005-2009" border="0" height="198" src="images/rftv-140.jpg" width="140"><br>
  <b><font color="#996600">Reports From The Verge - </font></b><font color="#996600">The
  Smash EDO/ITT Anthology 2005-2009 </font><b><font color="#996600"><br>
</font></b><font color="#996600">* For details</font> <a href="../pages_merchandise/merchandise_video.php#rftv">click
here</a></font></p></div></td></tr> </table><hr> <table bgcolor="#FFFFFF" border="0" width="140">
<tr>
        <td height="272">
          <div align="center"> <p><a href="../pages_merchandise/merchandise_video.php#uncertified" target="_blank"><img alt="Uncertified - SchMOVIES DVD Collection 2009" border="0" height="197" src="images/uncertified-140.jpg" width="140"></a><br>
  <b>Uncertified - SchMOVIES DVD Collection 2008 - <br>
</b>* For details<a href="../pages_merchandise/merchandise_video.php#uncertified" target="_blank">
click here<br> </a>* To see trailer<a href="http://enr.clearerchannel.org/media/schnews/uncertified-trailer.mpg" target="_blank">
click here<br> </a><font face="Arial, Helvetica, sans-serif" size="2">* For torrent
(900meg) <a href="http://onebigtorrent.org/torrents/5221/UNCERTIFIED" target="_blank">click
here</a></font><font face="Arial, Helvetica, sans-serif"> </font></p></div></td></tr>
</table><hr> <table align="center" bgcolor="#FFFFFF" border="0" height="268" width="140">
<tr> <td bgcolor="#003399" height="41"><a href="../schmovies/images/on-the-verge-800.jpg" target="_blank"><img alt="On The Verge" border="0" height="197" src="images/on-the-verge-140.jpg" width="140"></a></td></tr>
<tr> <td style="background-color:#0066CC"> <div align="center"><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif" size="3">On
The Verge</font><font color="#FFFFFF" face="Arial, Helvetica, sans-serif"> <br>
- the Smash EDO campaign film</font></b></div></td></tr> <tr> <td height="2">
<div align="center">
<p><font color="#003399" face="Arial, Helvetica, sans-serif">* For
  details </font><font color="#0000CC" face="Arial, Helvetica, sans-serif"><a href="index-on-the-verge.htm"><font color="#000099"><font color="#FF6600">click
  here</font></font></a> <br>
  * To see the trailer <a href="http://enr.clearerchannel.org/media/schnews/on-the-verge.wmv" target="_blank">click
  here<br>
  </a>* To order DVD <a href="../pages_merchandise/merchandise_video.php">click
  here</a><br>
  * Torrent file to download full film (900meg, mpg1) <a href="../schmovies/on-the-verge-schmovies-2008.mpg.torrent">click
  here</a></font></p>
          </div></td></tr> </table><hr> <p><a href="../schmovies/images/take-three-800.jpg" target="_blank"><img alt="Take Three - The SchMOVIES DVD Collection 2007" border="0" height="209" src="take-three-140.jpg" width="140"></a></p><p><b><font size="4">Take
Three</font> - The SchMOVIES DVD Collection 2007</b>. To order a copy <a href="index.html#take-three">click
here</a>... To view trailer <a href="http://enr.clearerchannel.org/media/schnews/take-three-trailer.mpg">click
here</a></p><hr> <h4 align="center"><a href="../schmovies/v-for-video-activist-700.jpg" target="_blank"><img alt="V For Video Activist" border="0" height="139" src="images/v-for-video-activist-140.jpg" width="140"></a></h4><h4 align="center"><b><font face="Arial, Helvetica, sans-serif">V
For Video Activist - SchMOVIES DVD Collection 2006</font></b></h4><p align="center"><a href="../pages_merchandise/merchandise_video.php"><b>For
more click here</b></a></p><p align="center"><font face="Arial, Helvetica, sans-serif"><b>OUT
NOW - </b>£6 (including P&amp;P). Contact the SchNEWS Office 01273 685913
or <a href="mailto:schnews@riseup.net">email</a> </font></p><hr> <h4 align="center"><a href="../schmovies/images/schmovies-dvd-2005-800.png" target="_blank"><img alt="SchMOVIES DVD Collection 2005 " border="0" height="143" src="schmovies-2005-dvd-140.gif" width="140"></a><b>
</b></h4><h4 align="center"><b><font size="4">SchMOVIES DVD - 2005</font></b></h4><p align="center"><strong><a href="../pages_merchandise/merchandise_video.php">Click
here for more info</a></strong></p><p align="center"><strong>AVAILABLE NOW<br>
</strong> </p><p align="center"> £6 (including P&amp;P) Contact the SchNEWS
Office 01273 685913 or <a href="mailto:schnews@riseup.net">email</a> </p><hr> <h4 align="center"><font face="Arial, Helvetica, sans-serif" size="3"><b><a href="../schmovies/images/schnews-at-ten-800.jpg" target="_blank"><img align="middle" alt="SchNEWS At Ten - the movie" border="0" height="197" src="images/schnews-at-ten-140.jpg" width="140"></a></b></font></h4><p align="center"><font face="Arial, Helvetica, sans-serif" size="3"><b>SchNEWS
At Ten - The Movie</b></font></p><p align="left"><font face="Arial, Helvetica, sans-serif">A
rollocking 74 minute journey through ten years of party and protest. </font></p><p align="left"><font face="Arial, Helvetica, sans-serif"><b><a href="sch-at-10-movie.htm">For
more info click here</a></b></font></p><p align="left"><font face="Arial, Helvetica, sans-serif"><b><a href="schnews-at-ten-trailer.mov">Click
here to view the trailer</a> </b>(1.20min, 4meg, quicktime format)</font></p><p align="left"><font face="Arial, Helvetica, sans-serif"><b>Available
now </b>for a fiver including p&amp;p <a href="../pages_merchandise/merchandise_video.php#at10">click
here</a>.</font></p>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
