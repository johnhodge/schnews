<?php 
session_start();
/*
 * 
 * 	SchMOVIES Admin System
 * 
 * 	The purpose of this software is to all editors to add and manipulate SchMOVIES
 * 	events and advert on the www.schnews.org.uk/schmovies page.
 * 
 * 	It relies on the existing MySQL database in use by the SchNEWS StoryAdmin software.
 * 
 * 
 * 	@author: Andrew Winterbottom
 * 	@support: webmaster@schnews.org.uk / support@andrewwinterbottom.com
 * 	@license: copyleft GPL v2
 * 	@owner: SchNEWS ( www.schnews.org.uk mail@schnews.org.uk )
 * 
 */
/*
 *	=================================================================== 
 *							PAGE SETUP
 *
 * 				Load any required files and libraries
 */
/*
 * 	Grab our config files
 */
 <!--#include virtual="conf/conf.php"-->
/*
 *	Load the function library
 */
 <!--#include virtual="lib/aw_lib.php"-->
 <!--#include virtual="inc/php/classes/classes.php"-->
 <!--#include virtual="inc/php/functions/functions.php"-->
/*
 * 	Pre-processing
 * 	
 * 	Load anything here that absiolutely has to happen before the page headers are displayed
 */
 <!--#include virtual="preprocessors/test_logout.php"-->
 <!--#include virtual="preprocessors/test_login.php"-->
/*
 *	=================================================================== 
 *							MAIN EXECUTION
 */
//	HTML HEADER
 <!--#include virtual="inc/html/html_header.php"-->
//	PAGE HEADER
 <!--#include virtual="inc/html/page_header.php"-->
//	TEST LOGIN
 <!--#include virtual="pages/login/test_for_login.php"-->
/*
 * 	MAIN FLOW CONTROL
 */
$page	=	"list";
if ( isset( $_GET['page'] ) )
{
	$page	=	$_GET['page'];
}
switch ( $page )
{
	default:
		 <!--#include virtual="pages/list/index.php"-->
		break;
	case "edit":
		 <!--#include virtual="pages/edit/index.php"-->
		break;
	case "delete":
		 <!--#include virtual="pages/delete/index.php"-->
		break;
}
//	HTML FOOTER
 <!--#include virtual="inc/html/html_footer.php"-->
?>