function show_delete_item_confirm()
{
	
	return confirm(
			"WARNING\r\n\r\nThis will remove the item from the system entirely.\r\n\r\nIf you do this you will not be able to recover the item.\r\n\r\n\r\nAre you sure?"
	)
	
}