<?php

function does_image_exist( $image_name )
{
	
	//	Test input
	if ( !is_string( $image_name ) || trim( $image_name ) == '' ) return false;
	
	
	$full_path		=	SCHMOVIES_IMAGE_LOC . $image_name;
	
	
	if ( file_exists( $full_path ) )
	{
		
		return true;
		
	}

	return false;
	
}