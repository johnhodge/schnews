<?php


function display_item( $id, $display = true )
{
	
	//	Test input
	if ( !CORE_is_number( $id ) || $id == 0 )
	{
		
		$message 	=	"display_item() input id not a positive integer";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !is_bool( $display ) ) $display = true;
	
	
	
	
	$item	=	new schmovies_item( $id );
	
	$style	=	'';
	if ( $item->get_hide() == 1 )
	{
		
		$style	=	" style='border: 3px solid red' ";
		
	}
	
	
	$class	=	'display_item_container';
	if ( $item->get_no_border() == 1 )
	{
		
		$class	=	'display_item_container_no_border';
		
	}
	
	
	
	/*
	 * Construct HTML
	 */
	$output		=	"	
	
	<!-- BEGIN ITEM DISPLAY: ITEM " . $id . " -->
	<div class='$class' $style >
	
	";
	
	
	if 	(
			$item->get_date_event() != ''
			&&
			$item->get_date_event() != '0000-00-00 00:00:00'
		)
		{
			
			$date	=	new date_time( $item->get_date_event() );
			
			$output	.=	"			
				<div class='display_item_date_event'>
					
						" . $date->make_human_date() . " @ " . $date->make_human_time() . "
				
				</div>
			";			
		}
	
	
	if ( file_exists( SCHMOVIES_IMAGE_LOC . $item->get_image() ) )
	{
		
		$output	.=	"
		
			<div class='display_item_image_div'>
				";
		if ( does_image_exist( $item->get_image_large() ) )
		{
			$output 	.=	 "<a href='" . SCHMOVIES_IMAGE_URL . $item->get_image_large() . "' target='_BLANK'>";
		}
		$output	.=	"		<img src='" . SCHMOVIES_IMAGE_URL . $item->get_image() . "' class='display_item_image' />";
		if ( does_image_exist( $item->get_image_large() ) )
		{
			$output	.=	'</a>';
		}
		$output	.=	"</div>	";
		
	}
	
	$output		.=	"
		<div class='display_item_title'>
			" . $item->get_title() . "
		</div>

		<div class='display_item_summary'>
			" . $item->get_text() . "
		</div>
		
	";
	
	
	if ( trim( $item->get_flowplayer() ) != '' )
	{
		
		$output	.=	"
		
			<a 
				href=\"" . $item->get_flowplayer() . "\"
				style=\"display:block;width:300px;height:200px; border: 2px solid black\" 
				id=\"player" . $id . "\">
			</a>
								
			<script language=\"JavaScript\">
				flowplayer(\"player" . $id . "\", \"" . SCHMOVIES_FLOWPLAYER_LOC . "flowplayer-3.2.7.swf\", {
				    clip:  {
				        autoPlay: false,
				        autoBuffering: true
				    }
				});
			</script>
		
		";
		
	}
	
	
	$output	.=	display_item_paypal_link( $item );
	
	
	
	
	$output	.=	display_item_added_date( $item );
	
	
	
	$output	.=	"
	
	</div>
	<!-- END ITEM DISPLAY -->
	";
	
	
	
	/*
	 * 	Output and return
	 */
	if ( $display )
	{
		
		echo $output;
		return true;
		
	}
	else
	{
		
		return $output;
		
	}

}








function display_item_paypal_link( $item )
{
	
	//	Test input
	if ( !is_object( $item ) ) return false;
	
	
	if 	( 
		
			trim( $item->get_paypal_price() ) == ''
			||
			trim( $item->get_paypal_desc() ) == ''
	
		)
		{
			
			return '';
			
		}
	
	
	$output		=	"
	<br /><br />
	<form action=\"https://www.paypal.com/cgi-bin/webscr\" target=\"_blank\" method=\"post\">

			<input type=\"hidden\" name=\"cmd\" value=\"_xclick\" />
			<input type=\"hidden\" name=\"business\" value=\"paypal@schnews.org.uk\" />
			<input type=\"hidden\" name=\"undefined_quantity\" value=\"1\" />
			<input type=\"hidden\" name=\"item_name\" value=\"" . $item->get_paypal_desc() . "\" />
			<input type=\"hidden\" name=\"item_number\" value=\"" . $item->get_paypal_desc() . "\" />
			<input type=\"hidden\" name=\"amount\" value=\"" . $item->get_paypal_price() . "\" /> 
			<input type=\"hidden\" name=\"no_shipping\" value=\"2\" />
			<input type=\"hidden\" name=\"return\" value=\"http://www.schnews.org.uk/extras/complete.htm\" />
			<input type=\"hidden\" name=\"cancel_return\" value=\"http://www.schnews.org.uk/extras/cancel.htm\" />

			<input type=\"hidden\" name=\"currency_code\" value=\"GBP\" />
			<input type=\"hidden\" name=\"lc\" value=\"GB\" />
			<input type=\"hidden\" name=\"bn\" value=\"PP-BuyNowBF\" />
			<input type=\"image\" src=\"https://www.paypal.com/en_US/i/btn/x-click-but23.gif\" border=\"0\" name=\"submit2\" alt=\"Make payments with PayPal - it's fast, free and secure!\" />
			<b>" . $item->get_paypal_desc() . "</b>

			</form>
	";
	
	return $output;
	
}








function display_item_edit_links( $id )
{
	
	//	Test input
	if ( !CORE_is_number( $id ) || $id == 0 )
	{
		
		$message 	=	"display_item_edit_links() input id not a positive integer";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	
	$item	=	new schmovies_item( $id );
	
	$style	=	'';
	if ( $item->get_hide() == 1 )
	{
		
		$style	=	" style='border: 3px solid red' ";
		
	}
	
	?>
	
	
	
	<!-- BEGIN DISPLAY ITEM EDIT LINKS <?php echo $id; ?> -->
	<div class='display_item_edit_container' <?php echo $style; ?> >
	
		<table width='100%'>

			<tr>
			
				<td align='left'>
				
					<a href='?page=delete&item=<?php echo $id; ?>'>
						<b> 
							[ DELETE ITEM ]
							<?php 
							
							if ( $item->get_hide() == 1 )
							{

								echo "<span style='font-size: 10pt; font-variant:small-caps'> ( Or stop the item being hidden ) </span>";
								
							}
							
							?> 
						</b>
					</a>
				
				</td>
			
				<td align='right'>
				
					<a href='?page=edit&item=<?php echo $id; ?>'><b> [ EDIT ITEM ] </b></a>
				
				</td>
				
			</tr>		
		
		</table>
	
	</div>
	<!-- END DISPLAY ITEM EDIT LINKS -->	
	
	<?php 
	
	return true;
	
}








function display_item_divider()
{
	
	?>
	
		<div class='display_item_divider'>
		
			&nbsp;
		
		</div>
	
	<?php 
	
}





function display_item_added_date( $item )
{
	
	//	Test inputs
	if ( !is_object( $item ) ) return false;

	
	$date	=	new date_time( $item->get_date_added() );
	
	$output		=	array();
	$output[]	=	"<div class='display_date_added'>";
	$output[]	=	"Added on " . $date->make_human_date();
	$output[]	=	"</div>";
	
	return implode( "\r\n", $output );
	
}








