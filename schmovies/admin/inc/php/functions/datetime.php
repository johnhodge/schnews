<?php

function convert_sql_datetime_to_text_date_and_time( $datetime )
{
	
	//	Test inputs
	if ( !is_string( $datetime) ) return false;
	$date_time	=	new date_time( $datetime );
	if ( !$date_time->is_sql_datetime( $datetime ) ) return false;

	$date	=	date( "d-m-Y", $date_time->get_timestamp() );
	$time 	=	date( "H-i", $date_time->get_timestamp() );
	
	return array( "date" => $date, "time" => $time );
	
}


function convert_text_date_and_time_to_sql_datetime( $date, $time )
{
	
	//	Test inputs
	if ( !is_string( $date ) || !is_string( $time ) ) return false;
	
	$date		=	explode( "-", $date );
	$time 		=	explode( "-", $time );
	
	$datetime	=	$date[2] . '-' . $date[1] . '-' . $date[0];
	$datetime	.=	' ' . $time[0] . ':' . $time[1];
	
	return $datetime;
	
}


function is_text_date_correct( $date )
{

	//	Test input
	if ( !is_string( $date ) ) return false;

	$date	=	explode( "-", $date );
	
	if ( count( $date ) != 3 ) return false;
	
	if ( !CORE_is_number( $date[0] ) || strlen( $date[0] ) != 2 ) return false;
	if ( !CORE_is_number( $date[1] ) || strlen( $date[1] ) != 2 ) return false;
	if ( !CORE_is_number( $date[2] ) || strlen( $date[2] ) != 4 ) return false;
	
	if ( $date[1] < 1 || $date[1] > 12 ) return false;
	if ( $date[0] > 31 ) return false;
	
	return true;
	
}


function is_text_time_correct( $time )
{
	
	//	Test input
	if ( !is_string( $time ) ) return false;
	
	$time 	=	explode( "-", $time );
	
	if ( count( $time ) != 2 ) return false;
	
	if ( !CORE_is_number( $time[0] ) || strlen( $time[0] ) != 2 ) return false;
	if ( !CORE_is_number( $time[1] ) || strlen( $time[1] ) != 2 ) return false;
	
	if ( $time[0] < 1 || $time[0] > 24 ) return false;
	if ( $time[1] < 0 || $time[1] > 59 ) return false;
	
	return true;
	
}






