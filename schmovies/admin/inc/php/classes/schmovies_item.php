<?php



class schmovies_item
{
	
	
	
	
	
	
	
	
	protected $id;
	
	protected $title;
	protected $text;
	
	protected $date_event;
	protected $date_added;
	
	protected $image;
	protected $image_large;
	protected $flowplayer;
	
	protected $paypal_desc;
	protected $paypal_price;
	
	protected $added_by;
	
	protected $position;
	protected $no_border;
	protected $hide;
	protected $sticky;
	
	
	
	
	
	
	
	
	function __construct( $id = false )
	{
		
		/*
		 * 	Test inputs
		 */
		if ( !CORE_is_number( $id ) || $id == 0 )	$id = false;
		
		
		
		$this->id 	=	$id;
		
		
		
		if ( $this->id )
		{
			
			$this->select_item_from_db();			
			
		}
		
		
		
		return true;
		
		
		
	}
	
	
	
	
	
	
	
	
	protected function select_item_from_db()
	{
		
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT 
								*
								
							FROM
								schmovies_items
								
							WHERE 
								id = " . $this->id;
		$mysql->query( $sql );
		
		
		
		/*
		 * 	There should be only 1 result
		 */
		if ( $mysql->get_num_rows() != 1 )
		{
			
			$message	=	"SQL query to select schmovies item did not yield 1 result";
			debug( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			
			debug ( $sql ); 
			
			return false;
			
		}

		
		
		/*
		 * 	Populate local properties
		 */
		$res	=	$mysql->get_row();
		
		$this->title		=	$res['title'];
		$this->text			=	$res['text'];
		$this->date_event	=	$res['date_event'];
		$this->date_added	=	$res['date_added'];
		$this->image		=	$res['image'];
		$this->image_large	=	$res['image_large'];
		$this->flowplayer	=	$res['flowplayer'];
		$this->paypal_desc	=	$res['paypal_desc'];
		$this->paypal_price	=	$res['paypal_price'];
		$this->added_by		=	$res['added_by'];
		$this->position		=	$res['position'];
		$this->no_border	=	$res['no_border'];		
		$this->hide			=	$res['hide'];
		$this->sticky		=	$res['sticky'];

		
		return true;
		
		
	}
	
	
	
	
	
	
	
	
	public function update_database()
	{
		
		if ( $this->id )
		{
			
			$this->update_item_in_database();
			
		}
		else
		{
			
			$this->add_item_to_database();
			
		}
		
		
	}
	
	
	
	
	
	
	
	protected function update_item_in_database()
	{
		
		$mysql		=	new mysql_connection();
		
		$sql		=	"	UPDATE
								schmovies_items

							SET
								title		=	'" . $mysql->clean_string( $this->title ) . "',
								text		=	'" . $mysql->clean_string( $this->text ) . "',
								date_event	=	'" . $mysql->clean_string( $this->date_event ) . "',
								image		=	'" . $mysql->clean_string( $this->image ) . "',
								image_large	=	'" . $mysql->clean_string( $this->image_large ) . "',
								flowplayer	=	'" . $mysql->clean_string( $this->flowplayer ) . "',
								position	=	'" . $mysql->clean_string( $this->position ) . "',
								no_border	=	'" . $mysql->clean_string( $this->no_border ) . "',
								hide		=	'" . $mysql->clean_string( $this->hide ) . "',
								sticky		=	'" . $mysql->clean_string( $this->sticky ) . "'
								
							WHERE
								id = " . $this->id;

		$mysql->query( $sql );
		
		return true;
		
	}
	
	
	
	
	
	
	
	protected function add_item_to_database()
	{
		
		/*
		 * 	Make sure it doesn't already exist
		 */
		if ( $this->does_item_exist_in_database() )
		{
			
			return false;
			
		}
		
		
		
		
		/*
		 * 	Add to database
		 */
		$mysql		=	new mysql_connection();
		
		$sql		=	" 	INSERT INTO
								schmovies_items
							
							(
								title,
								text,
								date_event,
								date_added,
								image,
								image_large,
								flowplayer,
								added_by,
								position,
								no_border,
								hide,
								sticky
							)
							
							VALUES
							
							(
								'" . $mysql->clean_string( $this->title ) . "',
								'" . $mysql->clean_string( $this->text ) . "',
								'" . $mysql->clean_string( $this->date_event ) . "',
								'" . DATETIME_make_sql_datetime() . "',
								'" . $mysql->clean_string( $this->image ) . "',
								'" . $mysql->clean_string( $this->image_large ) . "',
								'" . $mysql->clean_string( $this->flowplayer ) . "',
								'" . $_SESSION['SCHMOVIES_logged_in'] . "',
								'" . $mysql->clean_string( $this->position ) . "',
								'" . $mysql->clean_string( $this->no_border ) . "',
								'" . $mysql->clean_string( $this->hide ) . "',
								'" . $mysql->clean_string( $this->sticky ) . "'
							)
		
						";
		
		$mysql->query( $sql );
		
		
		/*
		 * 	Get a new SQL id
		 */
		$this->id		=	$mysql->get_insert_id();
		
		
		return true;
		
		
	}
	
	
	
	
	
	
	
	protected function does_item_exist_in_database()
	{
		
		$mysql		=	new mysql_connection();
		
		$sql		=	"	SELECT
								id
								
							FROM 
								schmovies_items

							WHERE
								title		=	'" . $mysql->clean_string( $this->title ) . "'
								AND
								text		=	'" . $mysql->clean_string( $this->text ) . "'
							
						";
		
		$mysql->query( $sql );
		
		
		if ( $mysql->get_num_rows() != 0 )
		{
			
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	/**
	 * @return the $id
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * @return the $title
	 */
	public function get_title() {
		return $this->title;
	}

	/**
	 * @return the $text
	 */
	public function get_text() {
		return $this->text;
	}

	/**
	 * @return the $date_event
	 */
	public function get_date_event() {
		return $this->date_event;
	}

	/**
	 * @return the $date_added
	 */
	public function get_date_added() {
		return $this->date_added;
	}

	/**
	 * @return the $image
	 */
	public function get_image() {
		return $this->image;
	}
	public function get_image_large()
	{
		
		return $this->image_large;
		
	}

	/**
	 * @return the $flowplayer
	 */
	public function get_flowplayer() {
		return $this->flowplayer;
	}
	
	public function get_paypal_desc()
	{
		return $this->paypal_desc;
	}
	
	public function get_paypal_price()
	{
		return $this->paypal_price;		
	}

	/**
	 * @return the $added_by
	 */
	public function get_added_by() {
		return $this->added_by;
	}
	public function get_position()
	{
		return $this->position;
	}
	public function get_no_border()
	{
		return $this->no_border;
	}
	/**
	 * @return the $hide
	 */
	public function get_hide() {
		return $this->hide;
	}

	/**
	 * @return the $sticky
	 */
	public function get_sticky() {
		return $this->sticky;
	}

	/**
	 * @param $title the $title to set
	 */
	public function set_title($title) {
		$this->title = $title;
	}

	/**
	 * @param $text the $text to set
	 */
	public function set_text($text) {
		$this->text = $text;
	}

	/**
	 * @param $date_event the $date_event to set
	 */
	public function set_date_event($date_event) {
		$this->date_event = $date_event;
	}

	/**
	 * @param $date_added the $date_added to set
	 */
	public function set_date_added($date_added) {
		$this->date_added = $date_added;
	}

	/**
	 * @param $image the $image to set
	 */
	public function set_image($image) {
		$this->image = $image;
	}

	/**
	 * @param $flowplayer the $flowplayer to set
	 */
	public function set_flowplayer($flowplayer) {
		$this->flowplayer = $flowplayer;
	}

	/**
	 * @param $added_by the $added_by to set
	 */
	public function set_added_by($added_by) {
		$this->added_by = $added_by;
	}

	public function set_position( $position )
	{
		
		$this->position = $position;
		
	}
	
	
	/**
	 * @param $hide the $hide to set
	 */
	public function set_hide($hide) {
		$this->hide = $hide;
	}

	/**
	 * @param $sticky the $sticky to set
	 */
	public function set_sticky($sticky) {
		$this->sticky = $sticky;
	}

	
	
	
	
	
	
	
	
	
	
	
	
}









