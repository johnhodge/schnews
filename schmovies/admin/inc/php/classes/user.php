<?php



class user
{
	
	protected $id;
	protected $name;
	protected $username;
	protected $email;
	protected $status;
	
	
	
	function __construct( $id )
	{
		
		/*
		 * 	Test inputs
		 */
		if ( !CORE_is_number( $id ) || $id == 0 )
		{
			
			$message	=	"Attempt to instantiate user class with no id parameter";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		$this->id	=	$id;
		
		
		
		/*
		 * 	Get user info from database
		 */
		if ( !$this->select_user_from_db() )
		{
			
			$message	=	"Could not retrieve user info from database";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		return true;
		
	}
	
	
	
	
	protected function select_user_from_db()
	{
		
		if ( !CORE_is_number( $this->id ) )
		{
			
			debug ( "user->select_user_from_db called without valid id property", __FILE__, __LINE__ );
			return false;
			
		}
		
		
		
		/*
		 * 	SQl Query
		 */
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT
							name,
							username, 
							email,
							status
							
						FROM
							users
							
						WHERE
							id = " . $this->id;
		$mysql->query( $sql );

		
		
		/*
		 * 	There should be only 1 result
		 */
		if ( $mysql->get_num_rows() != 1 )
		{
			
			$message	=	"Not 1 result returned from user db query";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		
		/*
		 * 	Populate local properties
		 */
		$res	=	$mysql->get_row();
		
		$this->name			=		$res['name'];
		$this->username		=		$res['username'];
		$this->email		=		$res['email'];
		$this->status		=		$res['status'];		
		
		
		
		return true;
		
		
		
		
		
		
	}
	/**
	 * @return the $id
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * @return the $name
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * @return the $username
	 */
	public function get_username() {
		return $this->username;
	}

	/**
	 * @return the $email
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * @return the $status
	 */
	public function get_status() {
		return $this->status;
	}

	
	
	
	
	
	
	
	
	
}




