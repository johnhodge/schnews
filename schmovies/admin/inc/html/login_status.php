<?php



/*
 *	Is the current session logged in? 
 */
if ( 
		isset( $_SESSION['SCHMOVIES_logged_in'] ) 
		&&
		CORE_is_number( $_SESSION['SCHMOVIES_logged_in'] )
		&&
		$_SESSION['SCHMOVIES_logged_in'] 	!=	0	
	)
	
{
	
	
	
	$user		=	new user( $_SESSION['SCHMOVIES_logged_in'] );
	
	
	?>
	
		<div style='text-align: right'>
			Logged In: <b><?php echo $user->get_name(); ?></b>
			<br /><br />
			<a href='?logout=1'> [ LOGOUT ] </a>
		</div>
	
	<?php 	
	
}	
	