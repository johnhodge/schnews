<?php

	//	OPTIONS BAR - Filters, search and Add New button
	require "pages/list/options_bar.php";
	
	
	
	/*
	 * 	DRAW LEFT ITEMS
	 */
	//	CREATE SQL ID LIST
	require "pages/list/get_left_sql_ids.php";
	
	//	LIST EVENTS
	require "pages/list/left_list.php";


	
	/*
	 * 	DRAW RIGHT ITEMS
	 */
	//	CREATE SQL ID LIST
	require "pages/list/get_right_sql_ids.php";
	
	//	LIST EVENTS
	require "pages/list/right_list.php";
	