



<!-- BEGIN ADD NEW ITEM -->
<div class='edit_container'>

	<div class='edit_title'>
		<?php echo $title; ?>
	</div>

	<?php 
	
		debug ( "Editing Item " . $item->get_id(), __FILE__, __LINE__ );
	
	?>

	<?php 
	
		if ( isset( $errors ) )
		{
			
			foreach ( $errors as $error )
			{
				
				?>
				
				<div class='edit_error'>
					<?php echo $error; ?>				
				</div>
				<?php 
				
			}
			
		}
	
	
	?>

	<form method='POST' enctype="multipart/form-data" action="<?php echo $target; ?>">
	
		<table class='edit_form_table'>
		
		
		
			<!-- TITLE -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Item Title</b> <span style='font-size: 8pt; color: red'>( Required )</span> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<input type='text' name='title' value='<?php echo $item->get_title(); ?>' style='width: 600px' />								
				</td>
			</tr>
			
			
			
			
			<!-- TEXT -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Text Summary</b> <span style='font-size: 8pt; color: red'>( Required )</span> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<textarea name='text' class='ckeditor_1' id='ckeditor_1' ><?php echo $item->get_text(); ?></textarea>						
				</td>
			</tr>
			
			<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ckeditor_1',
						    {
						        toolbar : 	[
												['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
											],
						        uiColor : '#999999',
						        height: '300px',
						        width: '600px'
						    });
					
					</script>
			<!-- END JS CALL TO CKEDITOR -->
			
			
			
			
			<!-- DATE & TIME -->
			<?php 
			
				$datetime	=	convert_sql_datetime_to_text_date_and_time( $item->get_date_event() );
			
			?>
			
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Date and Time of Event</b> <span style='font-size: 8pt; color: blue'>( Optional )</span> :								
				</td>
				<td align='right' class='edit_form_cell'>
					<span style='font-size: 8pt; color: black'>Date ( dd-mm-yyyy ) </span> <input name='date_event_date' type='text' value='<?php echo $datetime['date']; ?>' />
					<br /><br />
					<span style='font-size: 8pt; color: black'>Time ( hh-mm ) <i>[ 24hr time format ]</i> </span> <input name='date_event_time' type='text' value='<?php echo $datetime['time']; ?>' />
					
					<br /><br />
					<?php 
					
						$datetime	=	convert_sql_datetime_to_text_date_and_time( DATETIME_make_sql_datetime() );
					
					?>
					<span style='font-size: 8pt; color: black'>
					
						e.g. for right now the values would be
						
						( GMT <?php echo date( "O" ); ?> )
						
						: 
						
						Date = <b>"<?php echo $datetime['date']; ?>"</b>
						
						and
						
						Time = <b>"<?php echo $datetime['time']; ?>"</b>
					
					</span>
												
				</td>
			</tr>
			
			
			
			
			<!-- IMAGE -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Image</b> <span style='font-size: 8pt; color: blue'>( Optional )</span> :								
				</td>
				<td align='left' class='edit_form_cell'>
				
					<table width='100%'>
						
						<tr>
						
							<td align='left'>
						
								<input type='file' name='image' value='' style='width: 600px' />
								<br /><br />
								<span style='font-size: 8pt; color: black'>
									<?php echo $image_text; ?>
									<br /><br />
									Please make it as close as possible to XX px high and YY px wide.
								</span>
							
							</td>
							
							<td align='right'>
							
								<!-- BEGIN IMAGE PREVIEW -->
								<?php 
								
									if ( does_image_exist( $item->get_image() ) )
									{
										
										$path	=	SCHMOVIES_IMAGE_URL . $item->get_image();
										echo "<b>Current Image</b><br />";
										if ( does_image_exist( $item->get_image_large() ) )
										{
											echo "<a href='" . SCHMOVIES_IMAGE_URL . $item->get_image_large() . "' target='_BLANK'>";
										}
										echo "<img src='$path' style='border: 2px solid black' />";
										if ( does_image_exist( $item->get_image_large() ) )
										{
											echo "</a>";
										}										
										
									}
								
								?>
								<!-- END IMAGE PREVIEW -->
							
							</td>
							
						</tr>
						
					</table>
					
				</td>
			</tr>
			
			
			
			
			<!-- IMAGE LARGE -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Image Large</b> <span style='font-size: 8pt; color: blue'>( Optional )</span> :								
				</td>
				<td align='left' class='edit_form_cell'>
				
					<table width='100%'>
						
						<tr>
						
							<td align='left'>
						
								<input type='file' name='image_large' value='' style='width: 600px' />
								<br /><br />
								<span style='font-size: 8pt; color: black'>
									This image can be as big as you want.									
								</span>
							
							</td>
									
						</tr>
						
					</table>
					
				</td>
			</tr>
			
			
			
			
			
			
			
			<!-- FLOWPLAYER LINK -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Video Link</b> <span style='font-size: 8pt; color: blue'>( Optional )</span> :									
				</td>
				<td align='left' class='edit_form_cell'>
				
					<table width='100%'>
					
						<tr>
						
							<td align='left'>
							
								<input type='text' name='flowplayer' value='<?php echo $item->get_flowplayer(); ?>' style='width: 600px' />		
								<br /><br />
								<span style='font-size: 8pt; color: black'>
									Enter the URL pointing to the Flash file (.flv) of the video. The video will then be embedded in the SchMOVIE Item.
								</span>	
								
							</td>
							
							<?php 
							
							if ( trim( $item->get_flowplayer() ) != '' )
							{
							
							?>
							<td align='right' style='padding-left: 25px'>
							
								<!-- BEGIN FLOWPLAYER PREVIEW -->
								<div style='font-weight: bold; font-variant: small-caps; color: red'>
								
									Please check that this works, this system cannot check the link automatically, so you need to make sure this plays.
									<br /><br />
									If it doesn't play here it won't play on the live site.
								
								</div>
								
								<a 
									href="<?php echo $item->get_flowplayer(); ?>" 
									style="display:block;width:300px;height:200px; border: 2px solid black" 
									id="player">
								</a>
								
								<script language="JavaScript">
									flowplayer("player", "<?php echo SCHMOVIES_FLOWPLAYER_LOC; ?>flowplayer-3.2.7.swf");
								</script>
								<!-- END FLOWPLAYER PREVIEW -->
							
							</td>
							<?php 
							}
							?>
							
						</tr>
						
					</table>
										
				</td>
			</tr>
			
			
			
			
			<!-- PAYPAL LINKS -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>PayPal Link</b> <span style='font-size: 8pt; color: blue'>( Optional )</span> :									
				</td>
				<td align='left' class='edit_form_cell'>
					<div style='font-size: 8pt; color: black; font-variant: small-caps'>
					
						<b>Price ( &pound; ): </b><input type='text' name='paypal_price' value='<?php echo $item->get_paypal_price(); ?>' />
						
						<br /><br />
						
						<b>Description: </b><input type='text' name='paypal_desc' value='<?php echo $item->get_paypal_desc(); ?>' style='width: 600px' />
					
					</div>
				</td>
			</tr>
				
			


			
			<!-- POSITION -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Position on Page:</b> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<select name='position'>
						<option value='left' <?php if ( $item->get_position() == 'left' ) echo " selected "; ?> >Left</option>
						<option value='right' <?php if ( $item->get_position() == 'right' ) echo " selected "; ?> >Right</option>
					</select>
				</td>
			</tr>
						
			
			
			
			
			<!-- NO BORDER -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>No Border</b> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<input type='checkbox' name='no_border' <?php if ( $item->get_no_border() == 1 ) echo " checked "; ?> />								
				</td>
			</tr>
				
			
			
			<!-- STICKY -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Make Sticky</b> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<input type='checkbox' name='sticky' <?php if ( $item->get_sticky() == 1 ) echo " checked "; ?> />								
				</td>
			</tr>
			
			
			
			<!-- HIDE -->
			<tr>
				<td align='right' class='edit_form_cell'>
					<b>Hide Item</b> :								
				</td>
				<td align='left' class='edit_form_cell'>
					<input type='checkbox' name='hide' <?php if ( $item->get_hide() == 1 ) echo " checked "; ?> />								
				</td>
			</tr>
			
			
			<!-- SUBMIT BUTTON -->
			<tr>
				<td align='right' class='edit_form_cell'>
					&nbsp;								
				</td>
				<td align='left' class='edit_form_cell'>
					<input type='submit' value='<?php echo $submit; ?>' />								
				</td>
			</tr>
			
		
		
		</table>
	
	
	</form>



</div>
<!-- END ADD NEW ITEM -->