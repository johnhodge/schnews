<?php


//	OPTIONS
require "pages/edit/options_bar.php";


//	PREPARE DATA
require "pages/edit/prepare_data.php";


//	TEST FOR SUBMISSION
require "pages/edit/test_submission.php";


//	DRAW FORM
require "pages/edit/draw_form.php";