<?php

$mysql		=	new mysql_connection();


$fields		=	array( 	'title', 
						'text',
						'date_added',
						'added_by',
						'position',
						'no_border',
						'sticky',
						'hide'
					 );
$values		=	array( 	$mysql->clean_string( $title ), 
						$mysql->clean_string( $text ),
						DATETIME_make_sql_datetime(),
						$_SESSION['SCHMOVIES_logged_in'],
						$position,
						$no_border,
						$sticky,
						$hide
					);

if ( isset( $date_event ) )
{
	
	$fields[]	=	'date_event';
	$values[]	=	$mysql->clean_string( $date_event );
	
}

if ( isset( $image ) )
{
	
	$fields[]	=	"image";
	$values[]	=	$mysql->clean_string( $image );
	
}

if ( isset( $image_large ) )
{
	
	$fields[]	=	"image_large";
	$values[]	=	$mysql->clean_string( $image_large );
	
}

if ( isset( $flowplayer ) )
{
	
	$fields[]	=	'flowplayer';
	$values[]	=	$mysql->clean_string( $flowplayer );
	
}

if ( isset( $paypal_price ) )
{
	
	$fields[]	=	"paypal_price";
	$values[]	=	$mysql->clean_string( $paypal_price );
	
}

if ( isset( $paypal_desc ) )
{
	
	$fields[]	=	"paypal_desc";
	$values[]	=	$mysql->clean_string( $paypal_desc );
	
}


foreach ( $values as $key => $value )
{
	
	$values[ $key ] 	=	"'" . $value . "'";
	
}



$fields		=	implode( ", ", $fields );
$values		=	implode( ", ", $values );


$sql		=	"	INSERT INTO
						schmovies_items
						
					( $fields )
					VALUES
					( $values ) ";
$mysql->query( $sql );

$id			=	$mysql->get_insert_id();