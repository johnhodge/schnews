<?php 

if	( 
		isset( $_GET['item'] )
		&&
		CORE_is_number( $_GET['item'] )
		&&
		$_GET['item'] != 0
	)
	
{
	
	$item		=	new schmovies_item( $_GET['item'] );
	$title		=	"Editing SchMOVIES Item...";
	$submit		=	"Update Item";
	$image_text	=	"If there is an existing image you can replace it by uploading a new one.";
	$target		=	"?page=edit&item=" . $item->get_id();
	
}
else
{
	
	$item		=	new schmovies_item();
	$title		=	"Adding New SchMOVIES Item...";
	$submit		=	"Add New Item";
	$image_text	=	"You can upload an image to attach to the item.";
	$target		=	"?page=edit&item=0";
	
}

?>