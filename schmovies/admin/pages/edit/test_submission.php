<?php

if 	( 
		isset( $_POST['title'] )
		&&
		isset( $_POST['text'] )
		&&
		isset( $_GET['item'] )
	)
{
	
	debug ( "Testing submitted form data" );
	
	
	$id		=	$item->get_id();
	$errors	=	array();
	
	
	/*
	 * 	Test title
	 */
	if ( $_POST['title'] == '' )
	{
		
		$errors[]	=	"Title not set";
		
	}
	else
	{
		
		$title	=	$_POST['title'];
		
	}
	
	
	
	
	/*
	 * 	Test text
	 */
	if ( $_POST['text'] == '' )
	{
		
		$errors[]	=	"Summary Text not set";
		
	}
	else
	{
		
		$text	=	$_POST['text'];
		
	}
	
	
	
	
	/*
	 * 	Test date
	 */
	if 	(
			isset( $_POST['date_event_date'] )
			&&
			isset( $_POST['date_event_time'] )
			&&
			$_POST['date_event_date'] != ''
			&&
			$_POST['date_event_time'] != ''
				
		)
		{
			
			if ( !is_text_date_correct( $_POST['date_event_date'] ) )
			{
				
				$errors[]	=	"Event Date not set correctly";
				
			}		
			if ( !is_text_time_correct( $_POST['date_event_time'] ) )
			{
				
				$errors[]	=	"Event Time not set correctly";
				
			}

			$date_event		=	convert_text_date_and_time_to_sql_datetime( $_POST['date_event_date'], $_POST['date_event_time'] );
			
		}
	
	
	
	
	/*
	 * 	Test flowplayer
	 */
	if ( isset( $_POST['flowplayer'] ) && $_POST['flowplayer'] != '' )
	{
		
		$flowplayer		=	$_POST['flowplayer'];	
		
	}	
	
	
	
	
	/*
	 * 	Test image upload
	 */
	if 	( 
			isset( $_FILES['image'] )
			&&
			$_FILES['image']['name'] != ''
		)
	{

		if ( $_FILES["image"]["error"] > 0 )
		{
			
			$errors[]	=	"There was a problem uploading your image file";
			
		}	
		else
		{
			
			
			if ( move_uploaded_file( $_FILES["image"]["tmp_name"], SCHMOVIES_IMAGE_LOC . $_FILES["image"]["name"] ) )
			{

				$image	=	$_FILES['image']["name"];
				
			}
			else
			{
				
				$errors[]	=	"Couldn't save uploaded image file";
				
			}
			
		}
	}
	

	
	/*
	 * 	Test image large upload
	 */
	if 	( 
			isset( $_FILES['image_large'] )
			&&
			$_FILES['image_large']['name'] != ''
		)
	{

		if ( $_FILES["image_large"]["error"] > 0 )
		{
			
			$errors[]	=	"There was a problem uploading your large image file";
			
		}	
		else
		{
			
			
			if ( move_uploaded_file( $_FILES["image_large"]["tmp_name"], SCHMOVIES_IMAGE_LOC . $_FILES["image_large"]["name"] ) )
			{

				$image_large	=	$_FILES['image_large']["name"];
				
			}
			else
			{
				
				$errors[]	=	"Couldn't save uploaded large image file";
				
			}
			
		}
	}
	
	
	
	
	
	/*
	 * 	Test position
	 */
	if 	( 
			!isset( $_POST['position'] )
			||
			(
				$_POST['position'] != 'left'
				&&
				$_POST['position'] != 'right'
			) 
			
		)
		{
			
			$errors[]	=	"Position is incorrectly set";
			
		}
		else
		{
			
			$position	=	$_POST['position'];
			
		}
	
		
		
	
	/*
	 * 	Test no border
	 */
	if ( isset( $_POST['no_border'] ) )
	{
		
		$no_border		=	1;
		
	}
	else
	{
		
		$no_border		=	0;
		
	}
	
	
	
	
	/*
	 * 	Test sticky
	 */
	if ( isset( $_POST['sticky'] ) )
	{
		
		$sticky		=	1;
		
	}
	else
	{
		
		$sticky 	=	0;
		
	}
	
	
	
	/*
	 * 	Test hide
	 */
	if ( isset( $_POST['hide'] ) )
	{
		
		$hide	=	1;
		
	}
	else
	{
		
		$hide	=	0;
		
	}
	
	
	
	/*
	 * 	Test PayPal
	 */
	if ( isset( $_POST['paypal_price'] ) && ( $_POST['paypal_price'] ) != '' )
	{
		
		if ( CORE_is_number( $_POST['paypal_price'] ) )
		{
			
			$paypal_price 	=	$_POST['paypal_price'];
			
		}
		else
		{
			
			$errors[]	=	"PayPal Price is not a number";
			
		}		
		
	}
	if ( isset( $_POST['paypal_desc'] ) && trim( $_POST['paypal_desc'] ) != '' )
	{
		
		$paypal_desc	=	$_POST['paypal_desc'];
		
	}
	
	
	
	
	
	
	/*
	 * 	Do SQL stuff
	 */
	if ( count( $errors ) == 0 )
	{

		if ( $id == 0 )
		{
			
			if ( !does_item_exist( $title, $text ) )
			{
				
				require "pages/edit/do_insert_sql.php";

			}
			else
			{
				
				debug ( "Item already exists. Refresh conflict?", __FILE__, __LINE__ );
				$id		=	does_item_exist( $title, $text );
				
			}
			
		}
		else
		{
			
			require "pages/edit/do_update_sql.php";
			
		}
		
		require "pages/edit/reload_data.php";
		
	}
	
}