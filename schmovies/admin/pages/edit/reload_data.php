<?php

$item		=	new schmovies_item( $id );
$title		=	"Editing SchMOVIES Item...";
$submit		=	"Update Item";
$image_text	=	"If there is an existing image you can replace it by uploading a new one.";
$target		=	"?page=edit&item=" . $item->get_id();