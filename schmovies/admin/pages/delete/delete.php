<?php


/*
 * 	Update Database
 */
$mysql	=	new mysql_connection();
$sql	=	"	DELETE FROM
					schmovies_items

				WHERE
					id 		=	" . $item->get_id();
$mysql->query( $sql );





/*
 * Display Notice
 */
?>


<!-- BEGIN DELETE NOTICE -->
<div class='delete_container'>

	<div class='delete_title' style='color: red'>
	
		Item "<?php echo $item->get_title(); ?>" Deleted
	
	</div>
	
	<div class='delete_text'>

		The item is now removed from the system, and cannot be recovered.
		
		<br /><br />
		
		You cannot undo this. If you want to recover this item then tough, you should have thought things through better.
		
		<br /><br />
		<br /><br />
		
		<table align='center' width='100%'>
		
			<tr>
			
				<td align='center'>
				
					<a href='?page=list'>
						<div class='delete_notice_cell' >
							<b> [ BACK TO MAIN LIST ] </b>
						</div>
					</a>
					
				</td>
				
			</tr>
			
		</table>
	
	</div>
	
	
	
	

</div>
<!-- END DELETE NOTICE -->


