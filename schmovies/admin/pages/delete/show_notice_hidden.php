


<!-- BEGIN DELETE NOTICE -->
<div class='delete_container'>


	<a href='?page=list'>
		<div class='delete_notice_cell' style='text-align: right'>
			<b> [ CANCEL, BACK TO LIST ] </b>
		</div>
	</a>


	<br /><br />
	

	<div class='delete_title'>
	
		Item "<?php echo $item->get_title(); ?>" is Already Hidden
	
	</div>
	
	<div class='delete_text'>

		The item is already hidden, which means that it is not being shown to the public.
		
		<br /><br />
		
		You can choose to <b>delete</b> it, or you can stop it being hidden, which means that it will be visible and live to the public again.
		
		<br /><br />

		If you <b>delete</b> it, then it will be removed from the database entirely, and will not be accessible to anyone. It will not be possible to recover the item once it is deleted.
		
		<br /><br />
		<br /><br />
		
		<table align='center' width='100%'>
		
			<tr>
			
				<td align='left'>
				
					<a href='?page=delete&item=<?php echo $item->get_id(); ?>&action=delete'>
						<div class='delete_notice_cell' >
							<b> [ DELETE ITEM ] </b>
						</div>
					</a>
					
				</td>
				
				<td>&nbsp;</td>
				
				<td align='right'>
				
					<a href='?page=delete&item=<?php echo $item->get_id(); ?>&action=unhide'>
						<div class='delete_notice_cell' >
							<b> [ UN-HIDE ITEM ] </b>
						</div>
					</a>
					
				</td>
				
			</tr>
			
		</table>
	
	</div>
	
	
	<?php 
	
		display_item_divider();
		display_item( $item->get_id() );
	
	?>
	

</div>
<!-- END DELETE NOTICE -->



