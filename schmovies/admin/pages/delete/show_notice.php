


<!-- BEGIN DELETE NOTICE -->
<div class='delete_container'>


	<a href='?page=list'>
		<div class='delete_notice_cell' style='text-align: right'>
			<b> [ CANCEL, BACK TO LIST ] </b>
		</div>
	</a>


	<br /><br />
	

	<div class='delete_title'>
	
		Delete Item - "<?php echo $item->get_title(); ?>"
	
	</div>
	
	<div class='delete_text'>

		Are you sure you want to delete the item?
		
		<br /><br />
		
		You can choose to <b>delete</b> it, or instead you can <b>hide</b> it.
		
		<br /><br />

		If you <b>delete</b> it, then it will be removed from the database entirely, and will not be accessible to anyone. It will not be possible to recover the item once it is deleted.
		
		<br /><br />

		If you <b>hide</b> it then it will still exist in the system, but it will be hidden from the public. A hidden item can later be un-hidden.
		
		<br /><br />
		<br /><br />
		
		<table align='center' width='100%'>
		
			<tr>
			
				<td align='left'>
				
					<a href='?page=delete&item=<?php echo $item->get_id(); ?>&action=delete' onclick="return show_delete_item_confirm()" >
						<div class='delete_notice_cell' >
							<b> [ DELETE ITEM ] </b>
						</div>
					</a>
					
				</td>
				
				<td>&nbsp;</td>
				
				<td align='right'>
				
					<a href='?page=delete&item=<?php echo $item->get_id(); ?>&action=hide'>
						<div class='delete_notice_cell' >
							<b> [ HIDE ITEM ] </b>
						</div>
					</a>
					
				</td>
				
			</tr>
			
		</table>
	
	</div>
	
	
	<?php 
	
		display_item_divider();
		display_item( $item->get_id() );
	
	?>
	

</div>
<!-- END DELETE NOTICE -->



