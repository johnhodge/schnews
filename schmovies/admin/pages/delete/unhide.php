<?php


/*
 * 	Update Database
 */
$mysql	=	new mysql_connection();
$sql	=	"	UPDATE
					schmovies_items

				SET
					hide	=	0
					
				WHERE
					id 		=	" . $item->get_id();
$mysql->query( $sql );





/*
 * Display Notice
 */
?>


<!-- BEGIN DELETE NOTICE -->
<div class='delete_container'>

	<div class='delete_title'>
	
		Item "<?php echo $item->get_title(); ?>" Is No Longer Hidden
	
	</div>
	
	<div class='delete_text'>

		The item is no longer hidden, it is now once again visible to the public.
		
		<br /><br />
		<br /><br />
		
		<table align='center' width='100%'>
		
			<tr>
			
				<td align='center'>
				
					<a href='?page=list'>
						<div class='delete_notice_cell' >
							<b> [ BACK TO MAIN LIST ] </b>
						</div>
					</a>
					
				</td>
				
			</tr>
			
		</table>
	
	</div>
	
	
	<?php 
	
		display_item_divider();
		display_item( $item->get_id() );
	
	?>
	

</div>
<!-- END DELETE NOTICE -->


