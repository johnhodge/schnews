<?php



/*
 * 	Test for Item ID
 */
if 	( 

		!isset( $_GET['item'] ) 
		||
		$_GET['item'] == 0
		||
		!CORE_is_number( $_GET['item'] )
	
	)
	{
		
		$message		=	"Attempt to delete item without valie GET item id";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		
		require "pages/list/index.php";
		require "inc/html/html_footer.php";
		exit;		
		
	}
$item	=	new schmovies_item( $_GET['item'] );





/*
 * 	Test GET vars
 */
$action		=	"show_notice";
if ( isset( $_GET['action'] ) )
{
	
	$action		=	$_GET['action'];
	
}






/*
 * 	Flow control
 */
switch ( $action )
{

	default:
		if ( $item->get_hide() == 1 )
		{

			require "pages/delete/show_notice_hidden.php";
			
		}
		else
		{
			
			require "pages/delete/show_notice.php";
			
		}
		break;
		
	case 'delete':
		require "pages/delete/delete.php";
		break;
		
	case 'hide':
		require "pages/delete/hide.php";
		break;	
		
	case 'unhide':
		require "pages/delete/unhide.php";
		break;
	
}


