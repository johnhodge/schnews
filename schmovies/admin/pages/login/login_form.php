





<!-- BEGIN LOGIN FORM -->
<div class='login_form_container'>
	
	<div class='login_form_title'>
		You must login to access this software...
	</div>
	
	<form method='POST'>

		<table width='100%' align='center'>
		
		
		<!-- BEGIN LOGIN ERRORS -->
		<?php 
		
		if ( isset( $login_errors ) )
		{
			
			foreach ( $login_errors as $error )
			{
				
				?>
				
				<tr>
					<td colspan='2'>
						<div class='login_form_error'>
							<?php echo $error; ?>						
						</div>					
					</td>
				</tr>
				
				<?php 
				
			}
			
		}
		
		?>
		<!-- END LOGIN ERRORS -->
		
		
			
		<tr>
		
			<!-- USERNAME -->
			<td align='right' style='padding: 10px'>
				Username:
			</td>
			<td align='left' style='padding: 10px'>
				<input type='text' name='username' />
			</td>
		
		</tr>
		<tr>
			
			<!-- PASSWORD -->
			<td align='right' style='padding: 10px'>
				Password:
			</td>
			<td align='left' style='padding: 10px'>
				<input type='password' name='password' />
			</td>
			
		</tr>
		<tr>
		
			<!-- SUBMIT BUTTON -->
			<td>&nbsp;</td>
			<td align='left' style='padding: 10px'>
				<input type='submit' value='Login' />
			</td>
			
		</tr>
		
		</table>	
	
	</form>

</div>
<!-- END LOGIN FORM -->



<?php 


/*
 * 	End the HTML page
 */
require "inc/html/html_footer.php";
exit;

?>