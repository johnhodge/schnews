<?php


/*
 *	Is the current session logged in? 
 */
if ( 
		!isset( $_SESSION['SCHMOVIES_logged_in'] ) 
		||
		!CORE_is_number( $_SESSION['SCHMOVIES_logged_in'] )
		||
		$_SESSION['SCHMOVIES_logged_in'] 	==	0	
	)
{

	
	/*
	 * 	The current session is NOT logged in, so we need to present the loggin form...
	 */
	require "pages/login/login_form.php";
	exit;

	
}
else
{
	
	
	/*
	 * 	The current session IS logged in, so we will generate a user object...
	 */
	$logged_in_user		=	new user( $_SESSION['SCHMOVIES_logged_in'] );
	
	
}