&lt;?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 14-08-11
 *
 */
function DATETIME_make_sql_datetime($unix_timestamp = false)
{
	if (!is_numeric($unix_timestamp)) $unix_timestamp = false;
	if ($unix_timestamp)
	{
		return date(&quot;Y-m-d H:i:s&quot;, $unix_timestamp);
	}
	else
	{
		return date(&quot;Y-m-d H:i:s&quot;);
	}
}
