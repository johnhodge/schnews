<?php $this- $timestamp; ($timestamp="==" (c) * 16-06-11 2011 @auth @last_mod @license @support __construct($timestamp="false)" andrew basic class date_time false) framework function gpl if protected support@andrewwinterbottom.com v2 winterbottom {></?php>timestamp 	=	date(&#39;U&#39;);
			return true;
		}
		if ($this-&gt;is_sql_datetime($timestamp))
		{
			$this-&gt;timestamp = $this-&gt;get_timestamp_from_datetime($timestamp);
		}
	//	End of constructor
	}
	/*
	 * 	Test whether a string is an SQL datetime
	 *
	 * 	@param datetime			string
	 *
	 * 	@return bool
	 */
	public function is_sql_datetime($datetime)
	{
		//	Test inputs
		if (!is_string($datetime) || trim($datetime) == &#39;&#39;) return false;
		//	Test that there are 2 hyphens
		$datetime_processed 	=	str_replace(&quot;-&quot;, &#39;&#39;, $datetime);
		if (strlen($datetime) != (strlen($datetime_processed) + 2) )
		{
			debug (&quot;Datetime does not contain 2 hyphens. Datetime = &#39;&quot;.$datetime.&quot;&#39;&quot;, __FILE__, __LINE__);
			return false;
		}
		//	Test that there are 2 colons
		$datetime_processed 	=	str_replace(&quot;:&quot;, &#39;&#39;, $datetime);
		if (strlen($datetime) != (strlen($datetime_processed) + 2) )
		{
			debug (&quot;Datetime does not contain 2 colons. Datetime = &#39;&quot;.$datetime.&quot;&#39;&quot;, __FILE__, __LINE__);
			return false;
		}
		// Test that there are 14 numbers
		$datetime_processed 	=	str_replace(array(&#39;-&#39;, &#39;:&#39;, &#39; &#39;), &#39;&#39;, $datetime);
		if (strlen($datetime_processed) != 14)
		{
			debug(&quot;There are not 14 numeric digits in the datetime. Datetime = &#39;&quot;.$datetime.&quot;&#39;&quot;, __FILE__, __LINE__);
			return false;
		}
		return true;
	//	End of is_sql_datetime function
	}
	/*
	 * 	Converts an SQL datetime to a Unix timestamp
	 *
	 * 	@param datetime string
	 *
	 * 	@return int
	 */
	public function get_timestamp_from_datetime($datetime)
	{
		//	Test inputs
		if (!is_string($datetime) || trim($datetime) == &#39;&#39; || !$this-&gt;is_sql_datetime($datetime)) return false;
		//	Split the datetime into date and time components
		$bits		=	explode(&quot; &quot;, $datetime);
		$date		=	$bits[0];
		$time		=	$bits[1];
		//	Process Date
		$bits		=	explode(&quot;-&quot;, $date);
		$year		=	$bits[0];
		$month		=	$bits[1];
		$day		=	$bits[2];
		//	Process Time
		$bits		=	explode(&#39;:&#39;, $time);
		$hour		=	$bits[0];
		$minute		=	$bits[1];
		$seconds	=	$bits[2];
		//	Make timstamp
		$timestamp	=	mktime($hour, $minute, $seconds, $month, $day, $year);
		return $timestamp;
	//	End of get_timestamp_from_datetime
	}
	/*
	 * 	Create a human readable date string
	 *
	 * 	@param mode string				:: Options are &#39;words&#39; = 1st January 2011, &#39;numbers&#39; = 01-01-2011, &#39;words_short&#39; = 1 Jan 2011
	 * 	@param format string			:: Options are &#39;uk&#39; = 21-05-2011, &#39;us&#39; = 05-21-2011
	 */
	public function make_human_date($mode = &#39;words&#39;, $format = &#39;uk&#39;)
	{
		switch ($format)
		{
			//	UK Format
			default:
				switch ($mode)
				{
					//	Full text - i.e. 1st January 2011
					default:
						return date(&quot;jS F Y&quot;, $this-&gt;timestamp);
						break;
					//	Numbers only - i.e. 01-01-2011
					case &#39;numbers&#39;:
						return date(&quot;d-m-Y&quot;, $this-&gt;timestamp);
						break;
					//	Short textual - i.e. 1 Jan 2011
					case &#39;words_short&#39;:
						return date(&quot;j M Y&quot;, $this-&gt;timestamp);
						break;
				}
			//	US Format
			case &#39;us&#39;:
				switch ($mode)
				{
					//	Full text - i.e. 1st January 2011
					default:
						return date(&quot;jS F Y&quot;, $this-&gt;timestamp);
						break;
					//	Numbers only - i.e. 01-01-2011
					case &#39;numbers&#39;:
						return date(&quot;m-d-Y&quot;, $this-&gt;timestamp);
						break;
					//	Short textual - i.e. 1 Jan 2011
					case &#39;words_short&#39;:
						return date(&quot;j M Y&quot;, $this-&gt;timestamp);
						break;
				}
		}
	//	End of make_human_date function
	}
	/*
	 * 	Create a human readable time string
	 *
	 * 	@param format string			:: Options are &#39;24&#39; = 14:33, &#39;12&#39; = 2:33 PM
	 */
	public function make_human_time($format = &#39;12&#39;)
	{
		switch ($format)
		{
			//	12 Hour format
			default:
				return date(&quot;g:i A&quot;, $this-&gt;timestamp);
				break;
			//	24 Hour format
			case &#39;24&#39;:
				return date(&quot;H:i&quot;, $this-&gt;timestamp);
				break;
		}
	//	End of make_human_time function
	}
	/*
	 * ========================================================
	 * 						GETTERS
	 */
	public function get_timestamp()
	{
		return $this-&gt;timestamp;
	}
// End of Class
}
