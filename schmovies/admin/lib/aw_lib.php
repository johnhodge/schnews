&lt;?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 17-06-11
 *
 */
// What is the location of this file?
define (&#39;LIB_ROOT&#39;, dirname(__FILE__).&#39;/&#39;);
require LIB_ROOT.&quot;conf/config.php&quot;;
require LIB_ROOT.&quot;functions/functions.php&quot;;
require LIB_ROOT.&quot;debug/debug.php&quot;;
require LIB_ROOT.&quot;sql/functions.php&quot;;
require LIB_ROOT.&quot;datetime/functions.php&quot;;
require LIB_ROOT.&quot;datetime/datetime.php&quot;;
require LIB_ROOT.&quot;log/logging.php&quot;;
require LIB_ROOT.&quot;email/email.php&quot;;
require LIB_ROOT.&quot;sql/mysql.php&quot;;
