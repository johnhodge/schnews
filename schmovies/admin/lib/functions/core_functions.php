&lt;?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 29-06-11
 *
 */
/*
 * 	This file contains fucntions that replace and/or superceed the built in PHP core functions
 */
/*
 * 	Determine whether the given input is a pure number.
 *
 * 	Will return false if any digit of the input var is not a 0-9 or a .
 *
 * 	The built in is_int() never seems to work properly, and also this function doesn&#39;t check type
 *
 * 	@param string/int number
 * 	@param bool clean				//	If true the input will be clean of any non-numerics, if false it will just be evaluated
 * 	@return bool
 */
function CORE_is_number($number, $clean = false)
{
	//	Test inputs
	if ( ( !is_string( $number ) &amp;&amp; !is_numeric( $number ) ) || trim( $number ) == &#39;&#39; ) return false;
	if (!is_bool($clean)) $clean = false;
	$number		.=	&quot;*&quot;;
	$number		=	str_replace( &quot;*&quot;, &#39;&#39;, $number );
	//	Any chars in the input that is not one of these will be rejected
	$allowed_chars		=	array(&#39;0&#39;, &#39;1&#39;, &#39;2&#39;, &#39;3&#39;, &#39;4&#39;, &#39;5&#39;, &#39;6&#39;, &#39;7&#39;, &#39;8&#39;, &#39;9&#39;, &#39;.&#39;);
	if ($clean)		$output		=	&#39;&#39;;
	for ( $n = 0 ; $n &lt; strlen($number) ; $n++ )
	{
		if (in_array($number{$n}, $allowed_chars) &amp;&amp; $clean)
		{
			$output		.=		$number{$n};
		}
		if ( !in_array( $number{$n}, $allowed_chars ) &amp;&amp; !$clean)
		{
			debug(&quot;is_number failed for input &#39;$number&#39;. Failed digit = &quot; . $number{$n}, __FILE__, __LINE__);
			return false;
		}
	}
	if ($clean)
	{
		return $output;
	}
	else
	{
		return true;
	}
}
