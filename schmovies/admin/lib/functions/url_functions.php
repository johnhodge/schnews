<?php $_get $key="" $output="array();" ($_get (c) * 17-06-11 2011 @auth @last_mod @license @return @support a and andrew array as basic contains converts file for foreach framework function functions gpl interpreting into links mailto normal processing superglobal support@andrewwinterbottom.com the this url_get_get_vars() urls v2 winterbottom {></?php> $value)
	{
		$output[$key]	=	$value;
	}
	return $output;
}
/*
 * Turns the $_GET superglobal (or any other assicative array into a URL string - i.e. ?key1=value1&amp;key2=value2
 *
 * @param $get array
 * @return string
 */
function URL_make_url_from_get($get)
{
	//	Test inputs
	if (!is_array($get) || count($get) == 0)	return false;
	$pairs	=	array();
	foreach ($get as $key =&gt; $value)
	{
		$pairs[]	=	$key.&#39;=&#39;.$value;
	}
	return &#39;?&#39;.implode(&#39;&amp;&#39;, $pairs);
}
/*
 * 	Tets whether the input is a valid email addy
 *
 * 	@param email string
 * 	@return bool
 */
function URL_is_email($email)
{
	//	Test inputs
	if (!is_string($email) || trim($email) == &#39;&#39;) return false;
	//	Does it contain only on @ glyph
	$temp		=	str_replace(&#39;@&#39;, &#39;&#39;, $email);
	if (strlen($temp) != (strlen($email) - 1))
	{
		debug (&quot;Email &#39;$email&#39; does not contain only 1 @ glyph&quot;, __FILE__ , __LINE__);
		return false;
	}
	//	Does it contain at least one . full stop glypgh
	$temp		=	str_replace(&#39;.&#39;, &#39;&#39;, $email);
	if (strlen($temp) == strlen($email))
	{
		debug (&quot;Email &#39;$email&#39; does not contain any . glyphs&quot;, __FILE__ , __LINE__);
		return false;
	}
	return true;
}
/*
 * 	Converts the input string to an HTTP link, with an optional TARGET parameter
 * 
 * @param $url string
 * @param $display_text string 
 * @param $target string
 * @return string
 */
function URL_make_http_link( $url, $display_text = &#39;&#39;, $target = &#39;&#39; )
{
	/*
	 * Test inputs
	 */
	if ( !is_string( $url ) || trim( $url ) == &#39;&#39; ) return false;
	if ( $target != &quot;_BLANK&quot; ) $target = &#39;&#39;;	
	if ( !is_string( $display_text ) ) $display_text = &#39;&#39;;
	if ( $target != &#39;&#39; )
	{
		$target	=	&quot; target=\&quot;$target\&quot; &quot;;	
	}
	if ( $display_text == &#39;&#39; )
	{
		$display_text	=	$url;
	}
	$link		=	&quot;<a $target href="$url">$display_text</a>&quot;;	
	return $link;
}
/*
 * 	Converts the input address to a mailto link
 * 
 * 	@param $addr string
 * 	@param $display_text string
 * 	@return string
 */
function URL_make_mailto_link( $addr, $display_text = &#39;&#39; )
{
	/*
	 * 	Test inputs
	 */
	if ( !is_string( $addr ) || trim( $addr ) == &#39;&#39; ) return false;
	if ( !is_string( $display_text ) ) $display_text = &#39;&#39;;
	if ( $display_text == &#39;&#39; )
	{
		$display_text 	=	$addr;	
	}
	$link		=	&quot;<a href="mailto:$addr">$display_text</a>&quot;;
	return $link;
}