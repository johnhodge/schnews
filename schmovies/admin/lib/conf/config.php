&lt;?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-08-11
 *
 */
/*
 * This file contains very general configuration settings.
 * More specific settings can be found in the various sub-config files.
 */
/*
 *  What is the location of this file?
 *
 *  In most cases this can be left as dirname(__FILE__).
 *  The only time I can imagine this needing to be changed is when the config files reside on a different server to the main software that is using this library
 */
define (&#39;LIB_CONF_ROOT&#39;, dirname(__FILE__));
/*
 * How does the site identify itself. Useful for logs and email notices
 */
define (&quot;SITE_NAME&quot;, &#39;SchNEWS StoryAdmin&#39;);
/*
 * 	Include other sub-system config files
 */
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . &#39;sql&#39; . DIRECTORY_SEPARATOR . &#39;mysql_conf.php&#39;;
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . &#39;log&#39; . DIRECTORY_SEPARATOR . &#39;log_conf.php&#39;;
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . &#39;email&#39; . DIRECTORY_SEPARATOR . &#39;email_conf.php&#39;;
require LIB_CONF_ROOT . DIRECTORY_SEPARATOR . &#39;debug&#39; . DIRECTORY_SEPARATOR . &#39;debug_conf.php&#39;;
