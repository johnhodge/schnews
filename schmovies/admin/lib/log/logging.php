<?php !="" $log_file_handle; $log_file_path; $most_recent_log_entry; $this- && (c) (is_string($log_file_path) ) * 13-05-11 2011 @auth @last_mod @license @support __construct($log_file_path="false)" andrew basic class file framework function gpl if is.... log log_entry out path protected support@andrewwinterbottom.com the to trim($log_file_path) v2 what winterbottom work {></?php>log_file_path		=	$log_file_path;
		}
		elseif (LOGFILE_PATH != false &amp;&amp; trim(LOGFILE_PATH) != &#39;&#39;)
		{
			$this-&gt;log_file_path 		= 	LOGFILE_PATH;
		}
		else
		{
			$this-&gt;log_file_path 		=	LIB_ROOT.DIRECTORY_SEPARATOR.&#39;log&#39;.DIRECTORY_SEPARATOR.&#39;log.log&#39;;
		}
		/*
		 * Open the log file
		 *
		 * 	This is abstracted to a separate method to aid inheretence for oterh class. This class can then be used as a parent template
		 */
		$this-&gt;open_log_file();
	}
	/*
	 * Simply opens a handle to the log file
	 *
	 * 	Abstracted to a separate method to aid inheretence of future classes
	 */
	protected function open_log_file()
	{
		$this-&gt;log_file_handle 	=	fopen($this-&gt;log_file_path, &#39;a&#39;);
	}
	/*
	 * Adds an entry to the log file.
	 *
	 * @param string $message			- The log entry to add
	 * @param int $status				- 0 = information / 1 = security / 2 = error
	 * @param bool $email				- Whether to email the message to the designated admin (if specified in the LOG/CONF file)
	 */
	public function add_log_entry($message, $status = 0, $email = false)
	{
		// Test inputs
		if (!is_string($message) || trim($message) == &#39;&#39;) return false;
		if ($status != 0 &amp;&amp; $status != 1 &amp;&amp; $status != 2) $status = 0;
		if (!is_bool($email)) $email = false;
		$this-&gt;most_recent_log_entry	=	$this-&gt;make_log_entry($message, $status);
		if (!fwrite($this-&gt;log_file_handle, $this-&gt;most_recent_log_entry))
		{
			debug(&quot;Couldn&#39;t write log entry \&quot;$message\&quot;&quot;, __FILE__, __LINE__);
			return false;
		}
		if ($email)
		{
			if (!$this-&gt;send_email())
			{
				debug(&quot;Couldn&#39;t send loggin email&quot;, __FILE__, __LINE__);
			}
		}
	}
	/*
	 * Generates the error message string
	 *
	 * 	Abstracted to a separate method to aid inheretence
	 *
	 * @param string $message			- The log entry to add
	 * @param int $status				- 0 = information / 1 = security / 2 = error
	 */
	protected function make_log_entry($message, $status)
	{
		$date	= 	DATETIME_make_sql_datetime();
		$ip 	=	$_SERVER[&#39;REMOTE_ADDR&#39;];
		if (LOGFILE_HASH_IP)
		{
			$ip 	=	md5($ip);
		}
		$uri 	=	$_SERVER[&#39;REQUEST_URI&#39;];
		$message 	=	str_replace(array(&quot;\r&quot;, &quot;\n&quot;, &quot;;&quot;), &quot; &quot;, htmlentities($message));
		switch ($status)
		{
			default:
				$status = &#39;Information&#39;;
				break;
			case 1:
				$status = &#39;Security&#39;;
				break;
			case 2:
				$status = &#39;Error&#39;;
				break;
		}
		return $date.&quot;;&quot;.$ip.&quot;;&quot;.$status.&quot;;&quot;.$uri.&quot;;&quot;.$message.&quot;\r\n&quot;;
	}
	protected function send_email()
	{
		if (LOGFILE_EMAIL_ADDRESS == &#39;&#39; || !LOGFILE_EMAIL_ADDRESS) return false;
		$lines	=	explode(&quot;\t;\t&quot;, $this-&gt;most_recent_log_entry);
		$message = implode(&quot;\r\n\r\n&quot;, $lines);
		$headers = 		&#39;From: &#39;.LOGFILE_SEND_ADDRESS . &quot;\r\n&quot; .
    					&#39;Reply-To: &#39;.LOGFILE_SEND_ADDRESS . &quot;\r\n&quot; .
    					&#39;X-Mailer: PHP/&#39; . phpversion();
		if (!mail(LOGFILE_EMAIL_ADDRESS, SITE_NAME.&#39; Log Entry Notification&#39;, $message, $headers))
		{
			return false;
		}
		return true;
	}
}
