<?php
/*
 * 	LOCAL CONFIG
 * 
 * 	Because we can't be sure where this file is being called from we have to hard
 * 	code the location of this file.
 * 
 * 	This should be fine to do automatically.
 */
define ( "SCHMOVIES_DISPLAY_ROOT", dirname( __FILE__ ) . DIRECTORY_SEPARATOR );
/*
 * 	Load out function library
 */
require SCHMOVIES_DISPLAY_ROOT . "lib/aw_lib.php";
/*
 * 	Load only the necessary config files
 */
require SCHMOVIES_DISPLAY_ROOT . "conf/file_system/conf.php";
/*
 * 	Load only the necessary functions and classes
 */
require SCHMOVIES_DISPLAY_ROOT . "inc/php/classes/schmovies_item.php";
require SCHMOVIES_DISPLAY_ROOT . "inc/php/functions/display_items.php";
require SCHMOVIES_DISPLAY_ROOT . "inc/php/functions/items.php";
require SCHMOVIES_DISPLAY_ROOT . "inc/php/functions/images.php";
require SCHMOVIES_DISPLAY_ROOT . "display/functions/display_items.php";
require SCHMOVIES_DISPLAY_ROOT . "display/functions/general.php";
?>
<!-- DISPLAY FRAMEWORK LOADED - <?php echo DATETIME_make_sql_datetime(); ?> -->
<!-- UNIQUE SESSION ID - <?php echo md5( DATETIME_make_sql_datetime() . $_SERVER['REMOTE_ADDR'] ); ?> -->
