#!/usr/bin/perl
use strict;
use warnings;
use utf8;
use 5.10.0;
use Data::Dumper;
use Mojo::DOM;
use File::Find;
use File::Copy;

## Configure these
my @html_folders = ('../features');
my @unwanted_includes = ('inc/navBar_merchandise.php','bannerGraphic.php','inc/contacts_right_bar_advert.php','inc/currentIssueSummary.php');

## End config, don't change anything below here unless you know what you're doing.

find(\&each_file, @html_folders);

# This function is called on each file found by find.
# If the file ends .php or .html then create a backup of the old content
# Process the content and overwrite the original
# Note that File::Find chdirs you to the directory containing the file on which you're currently working
sub each_file {
  return unless($_ =~ m{(php|html?)$}i );
  my $filename = $File::Find::name;
  my $local_filename = $_;
  say "Processing $filename";
  # we are an html or php file, should be processed.
  copy($local_filename,"$local_filename.bak") or die "Couldn't copy $filename to $filename.bak. $!"; # create a backup copy
  my $new_content = cleanup_file($local_filename);
  to_file($local_filename,$new_content);
}

# Given a file, run all the cleanup operations on its contents and return the amended contents.
sub cleanup_file {
  my $filename = shift;
  my $contents = file_contents($filename);
  my $dom = Mojo::DOM->new($contents);

  ## Change file encoding tag to UTF-8 Nb. if this was HTML5 we could just use meta charset=
  if($dom->at("meta['content=\"text/html; charset=iso-8859-1\"']")) {
    $dom->at("meta['content=\"text/html; charset=iso-8859-1\"']")->replace('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
  }

  ## Remove any paypal forms
  if($dom->at('form[action=https://www.paypal.com/cgi-bin/webscr]')) {
    $dom->find('form[action=https://www.paypal.com/cgi-bin/webscr]')->map('remove');
  }

  ## Remove search bar
  ### Assume: only one search bar present
  if($dom->at('div[class=search_bar]')) {
    $dom->at('div[class=search_bar]')->remove;
  }

  $dom = rewrite_includes(strip_unwanted_includes($dom));

  return $dom;
}

# Translate php includes or requires into SSI syntax
## Assume: The includes don't go over 2 lines.
sub rewrite_includes {
  my $html = shift;
  $html =~ s{(.*)(include|require)\s+["']([^"']+)["']\s*;}{$1 <!--#include virtual="$3"-->}gi;
  return $html;
}


# Strip any php includes or requires matching in unwanted_includes array
## Assume: The includes don't go over 2 lines.
sub strip_unwanted_includes {
  my $html = shift;
  for my $u (@unwanted_includes) {
    $html =~ s{^\s*(require|include).*["']?$u.*["']?;}{}g
  }
  return $html;
}

# Given a filename and a string of content, print content to file
# will overwrite existing files
# Note: we push out UTF-8
sub to_file {
  my $filename = shift;
  my $content = shift;
  open(my $fh, '>:encoding(UTF-8)', $filename)
    or die "Could not open file for writing '$filename' $!";
  print $fh $content;
  close $fh;
}

# Given a filename, return the contents of the file as a string
# Note: We don't specify UTF-8 when opening
sub file_contents {
  my $filename = shift;
  say "File contents called with '$filename'";
  open(my $fh, '<', $filename)
    or die "Could not open file '$filename' $!";
  my $html = '';
  while (my $row = <$fh>) {
    next if($row =~ /^\s*$/);
    $html .= $row;
  }
  return $html;
}


__END__
=head1 NAME

cleanup.pl: sort out schnews archive

=head2 VERSION

0.1

=head1 SYNOPSIS

perl cleanup.pl

=head1 DESCRIPTION

See README file

=head1 REQUIREMENTS

Perl 5.10.0 (not tested on earlier versions)
Data::Dumper
Mojo::DOM
File::Find
File::Copy

=head1 COPYRIGHT AND LICENCE

           Copyright (C)2020 Charlie Harvey

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version
 2 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public
 License along with this program; if not, write to the Free
 Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA  02111-1307, USA.
 Also available on line: http://www.gnu.org/copyleft/gpl.html

=head1 SEE ALSO

____________________________

=cut
