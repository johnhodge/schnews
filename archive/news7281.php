<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 728 - 25th June 2010 - Cutting 'N' Running</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, g20, g8, financial crisis, canada, greece, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 728 Articles: </b>
<b>
<p><a href="../archive/news7281.php">Cutting 'n' Running</a></p>
</b>

<p><a href="../archive/news7282.php">Up Shit Greek</a></p>

<p><a href="../archive/news7283.php">Edo Trial Update</a></p>

<p><a href="../archive/news7284.php">Green Village Preservation Society</a></p>

<p><a href="../archive/news7285.php">Tome Raiders</a></p>

<p><a href="../archive/news7286.php">Razed Beds</a></p>

<p><a href="../archive/news7287.php">Sticky Wiki</a></p>

<p><a href="../archive/news7288.php">Israel In The Docks</a></p>

<p><a href="../archive/news7289.php">...and Finally...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 25th June 2010 | Issue 728</b></p>

<p><a href="news728.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CUTTING 'N' RUNNING</h3>

<p>
<p>
	<strong>WHO WILL TORONTO THE LINE AT THE G20 MEETINGS AS AUSTERITY TOPS THE AGENDA..?</strong> </p>
<p>
	So &lsquo;austerity&rsquo; took shape this week as George &lsquo;where were you hiding during the election campaign&rsquo; Osborne went for broke on Tuesday with his hard-hitting &lsquo;fuck the poor&rsquo; budget. </p>
<p>
	Yes, taxes &amp; VAT are going up and almost every type of benefit is being slashed (and tied into being available for whatever work they deem you fit for), but hey! it&rsquo;s not all bad news - corporation tax is being reduced! </p>
<p>
	Well someone&rsquo;s gotta pay for all those huge profits, bonuses and debt screw-ups &ndash; and it might as well be the less well off. After all there&rsquo;s so many more of them. Easier to take say &pound;100 a year off 30 million relatively powerless people (bringing in &pound;3bn) than try and take half a million each off the richest 60,000 (and just think &ndash; the deficit is hundreds of billions!). </p>
<p>
	Events in Greece (see below) have lead to a few tight sphincters around the European elites &ndash; how best to avoid similar scenes in their own countries? A fashion for economic hair shirts has swept Europe with Germany, Spain, France (a bit) and many others joining in to see who can self harm the most. But it is a big gamble. Opinion amongst leading economists is split as to whether excessive blood letting is actually more or less likely to lead to another longer, more severe depression. And many in the US, included Obama&rsquo;s most trusted advisers, err on the side of less austerity now. </p>
<p>
	Not that we are flag waving one way or the other for methods to prop up a failing, structurally flawed exploitative system &ndash; but it all means that this week&rsquo;s cosy get-togethers of world leaders at the latest G8 and G20 meetings might be the scene of more disagreements than usual. Don&rsquo;t worry, we&rsquo;re sure they&rsquo;ll cordially agree to differ here and there and come out for the usual photo-opportunities afterwards. </p>
<p>
	Both summits are being held in Canada (G20 in the centre of Toronto, G8 in a more remote luxury &lsquo;resort&rsquo; in Huntsville, Ontario) &ndash; where the conservative Government is currently introducing its own programme of heavy public cuts. But somehow they were still keen to pitch for the chance to host the &lsquo;Austerity&rsquo; round of meetings &ndash; and demonstrate their own commitment to penny pinching by spending a jaw-dropping $1.1bn on wining and dining the World&rsquo;s most powerful for a week. Well it&rsquo;s not cheap to get 20,000 police to shut down, clear out and create a impenetrable &lsquo;ring of steel&rsquo; in the middle of your capital city. </p>
<p>
	Not that all the cash has gone on a huge overblown security operation - despite the media being full of footage of uber-riot gear cops sporting serious weapons up to and including a &lsquo;sonic cannon&rsquo;. A spokesman for the Canadian secret service reported no particular intelligence of very large scale protests or extremist threat &ndash; but don&rsquo;t worry though, there is an officially-sanctioned &lsquo;protest zone&rsquo; miles away from the action. No, nearly $200m dollars has been spent on creating a artificial lake out by the media centre, presumably to provide a more picturesque backdrop to those handshaking photo calls. Well Canada is the country with the highest number of natural lakes in the World, so it obviously badly needed a new fake one. </p>
<p>
	Such utter wastefulness did not go completely unnoticed by the Canadian public and media, and it will be interesting to see if that swells numbers as a week of whatever-protest-is-still-possible is set to begin with the largest likely to be Saturday (26th)&rsquo;s &lsquo;People First!&rsquo; union / socialist alliance type shindig. </p>
<p>
	Yesterday saw a protest in the financial district &ndash; up against the giant security fence erected across downtown Toronto - where oil-dowsed protesters spilt through the streets decrying the many shortcomings of a black gold worshipping World, whilst others handed out &lsquo;$1bn&rsquo; bills to passers-by. </p>
<p>
	There was also a 1000-strong march in support of the First Nation peoples, who of course have been treated like shit since the European invaders first nicked their country. And not only historically &ndash; as well as numerous unanswered land claims, they claim that hundreds of their women have been murdered or disappeared in the past decades as part of a deliberate strategy to dwindle their numbers down to extinction. see <a href="http://www.defendersoftheland.org" target="_blank">www.defendersoftheland.org</a> </p>
<p>
	* A convergence and full program of resistance events is planned for the whole week &ndash; for details see <a href="http://www.g20.torontomobilize.org" target="_blank">www.g20.torontomobilize.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(831), 97); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(831);

				if (SHOWMESSAGES)	{

						addMessage(831);
						echo getMessages(831);
						echo showAddMessage(831);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


