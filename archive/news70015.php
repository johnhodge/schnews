<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 700 - 20th November 2009 - ...and Finally... Milestone Or Millstone?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, alternative media, criminal justice act" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 700 Articles: </b>
<p><a href="../archive/news7001.php">Good Cop Bad Cop</a></p>

<p><a href="../archive/news7002.php">Tamil Torture</a></p>

<p><a href="../archive/news7003.php">Not A Leg To Stand On</a></p>

<p><a href="../archive/news7004.php">Walls Of Steel</a></p>

<p><a href="../archive/news7005.php">Nae To Nato</a></p>

<p><a href="../archive/news7006.php">Blackboard Rumble</a></p>

<p><a href="../archive/news7007.php">Crasbo Selecta</a></p>

<p><a href="../archive/news7008.php">Schnews In Brief</a></p>

<p><a href="../archive/news7009.php">Truck Or Treat</a></p>

<p><a href="../archive/news70010.php">Taking The Cissbury</a></p>

<p><a href="../archive/news70011.php">Out Of Their League</a></p>

<p><a href="../archive/news70012.php">French Letter</a></p>

<p><a href="../archive/news70013.php">Atomic Acquitten</a></p>

<p><a href="../archive/news70014.php">R . I . P Ivan Khutorskoy</a></p>

<b>
<p><a href="../archive/news70015.php">...and Finally... Milestone Or Millstone?</a></p>
</b>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th November 2009 | Issue 700</b></p>

<p><a href="news700.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>...AND FINALLY... MILESTONE OR MILLSTONE?</h3>

<p>
So happy 15th birthday SchNEWS! - and issue 700 falls on the same week. A big thank you to everyone who has been involved in SchNEWS, past and present. And a big thanks to all our readers, many who have helped by contributing information and donating money. Their continued support has kept us going through the leanest times and justified our existence.  <br />  <br /> In 1994, when the Justice? campaign were campaigning against the Criminal Justice Act in Brighton and squatting the old Courthouse, they were urged by a visiting group of women who were part of &lsquo;Women Against Pit Closures&rsquo; - whose legacy went back to the 80s miners&rsquo; strikes - to start a news letter. Justice? responded to this and began producing something called SchNEWS on a weekly basis. Most newsletters come and go but the resourceful crew who started SchNEWS (none still on the writing staff), moved on from covering the CJA and quickly found a niche as an info-hub for the UKs direct action movement. <br />  <br /> The road protest movement was just starting to gain momentum then, with the urban M11 campaign at full throttle. Schnews was a cruder, more direct beast in those days with headlines such as &ldquo;You&rsquo;re Nicked&rdquo; and &ldquo;Mother-Fucked&rdquo;. (although dedicated SchNEWSologists have found early proto-puns such as &lsquo;Silence is Olden&rsquo; within the first fifty issues). Although there were other independent media around, SchNEWS&rsquo; frequency and focus on current events made it a must-read. Remember (if you still can) that this was all before the internet or even mobile phones were in general use. <br />  <br /> The nineties is now talked about like a golden era for ecological direct action and D.I.Y culture (alright, mostly by the likes of us). Obviously the summers were longer and the drugs were better (remember purple ohms?) but in retrospect it looks like quite a politically fluffy decade for us in the West. The major class battles of the eighties were over. The attacks on travellers, ravers and squatters were a by-product of Thatcher&rsquo;s victory over the organised elements of the British working class. The CJA seemed hideously draconian at the time and sparked major resistance but most of its clauses would pass without mention in any of the more recent Crime/Police/Justice Acts.  <br />  <br /> For many of that generation, particularly the ones who eventually grew up, cut their dreads off and got a proper job, the nineties is looked back on as the baby boomers look back at the 1960s - a radical time, which ended (and ended roughly the same time they went straight). A window of craziness and change that opened, but then closed. <br />  <br /> But the need for direct action and grass-roots community mobilisation never went away. Despite notable successes &ndash; road protests, reclaim the streets , anti-GM struggles, animal rights, summit hopping etc etc we&rsquo;ve still never made the breakthroughs we need to. Yes, meanwhile we&rsquo;ve championed issues that are now mainstream media fodder (witness the recent outpouring of liberal ire around police behaviour at protests, something regular SchNEWS readers wouldn&rsquo;t have found much of a surprise) but, despite the anti-Iraq war march of a million-plus, there hasn&rsquo;t been a real popular uprising since the anti-poll tax movement. <br />   <br /> Even last year&rsquo;s economic collapse seems to have caused nothing more than increased ratings for X-factor and Strictly Come Dancing. Global elites have gambled by mortgaging a huge slice of government tax income for decades, and given it to themselves to continue with business as usual. Maybe the shock of the sheer audacity of what&rsquo;s taken place has hit home yet and will need the effects of the recession to really bite before the penny drops.  <br />  <br /> And if the bailout doesn&rsquo;t hold then we&rsquo;ll really be in uncharted waters.&nbsp; With some kind of new crises, economic or environmental, almost inevitable sooner or later, there will be less and less room to manoeuvre for those in power &ndash; and a dangerous likelihood of totalitarian tendencies coming to the fore. <br />  <br /> It&rsquo;s in precisely these times that we need to be pushing for real democratic social change and rejecting the narrow nationalist non-solutions that are inevitably gonna arise. <br />  <br /> Here&rsquo;s to the next fifteen years...
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=alternative+media&source=700">alternative media</a>, <span style='color:#777777; font-size:10px'>criminal justice act</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(583);
			
				if (SHOWMESSAGES)	{
				
						addMessage(583);
						echo getMessages(583);
						echo showAddMessage(583);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>