<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(43);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	43;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 676 - 22nd May 2009 - AS WE BOLDLY TAKE A SCHNEWS CRUISE TO CALAIS</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, colombia, farc, democratic security, minga, quilcue, alvaro uribe, human rights, immigration, no borders, deportation, calais, detention centre, colnbrook, tamil tigers, sri lanka, war, human rights, rajapaksa, white phosphorous, refugee camp, prabhakaran, rolls royce, rayensway, nuclear weapons, trident missiles, protest, derby" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.london.noborders.org.uk/calais2009" target="_blank"><img
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news675.htm">SchNEWS 675, 8th May 2009</a></b><br />
<b>Brighton Rocked</b> - As Smash EDO Mayday mayhem hits the streets of Brighton we report from the mass street party and demonstration against the arms trade, war and capitalism... plus, the bike-powered Coal Caravan concludes its three week tour of the North Of England, a US un-manned Predator drone accidently bombs the village of Bala Baluk, killing up to 200 innocent civilians, RavensAit, the squatted island on the Thames near Kingston, is finally evicted this week, the Nine Ladies protest camp, having won its ten-year battle to stop the quarrying of Stanton Moor in Derbyshire, has finally tatted down and finished, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news674.htm">SchNEWS 674, 1st May 2009</a></b><br />
<b>May the Fourth be With You</b> - With Smash EDO's Mayday Mayhem about to hit Brighton, here's some useful information covering the day.... plus, RBS&nbsp;are seeking &pound;40,000 in compensation from a seventeen year old girl who damaged one computer screen and keyboard at the G20 demo last month, the Metropolitan Police are forced to admit they assaulted and wrongfully arrested protesters during a 2006 demo at the Mexican Embassy in London, the IMF/World Bank meeting in Washington DC is met with three days of protests,&nbsp;one London policeman is sacked for admitting that the police killed someone while another is merely disciplined for saying that he 'can't wait to bash up some long haired hippys', and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news673.htm">SchNEWS 673, 24th April 2009</a></b><br />
<b>Quick Fix</b> - The twelve students arrested this month as alleged 'terrorists' have been released without charge - like many 'anti-terrorist operations' in Britain, this one becomes a joint operation between the police and the media... plus,&nbsp;a protester in the West Bank town of Bil'in is killed by the Israel military while protesting against the 'Apartheid Wall',&nbsp;Birmingham squatters prevent two separate community squats in the city from being evicted on the same day, the bloodshed continues in Sri Lanka,&nbsp;a coalition hasformed called The United Campaign Against Police Violence, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news671.htm">SchNEWS 671, 3rd April 2009</a></b><br />
<b>G20 SUMMIT EYEWITNESS REPORT</b> - As the G20 Summit takes place, we look at what the leaders are discussing, along with an eyewitness account of the G20 protests around the Bank Of England on Wednesday April 1st.... plus, a report from day two of the protests, on the day when the G20 Summit began at the ExCel entre in the East London Docklands, the squatted island at Raven's Ait, on the Thames near Surbiton, is under threat from eviction and calling for help, groups of sacked workers who are feeling the direct consequences of the financial crash are protesting, and re-occupying their work places, peace campaigner Lindis Percy causes traffic chaos outside the USAF airbase at Lakenheath in Suffolk, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news670.htm">SchNEWS 670, 27th March 2009</a></b><br />
<b>G20 SUMMIT SPECIAL</b> - SchNEWS looks how deep the financial problems are for the banks and the British Govt, and how they won't learn from their errors. We ask the question: the crash which looked inevitable in 1999 has happened, so we look to how we can survive this.... plus, ex-Royal Bank Of Scotland boss Fred 'The Shred' Goodwin, has his home and car attacked by demonstrators, Horfield Prison in Bristol is surrounded by a noise demo in solidarity with Elija Smith, one of the EDO Decommissioners, Climate change campaigners stop work at Muir Dean open-cast coal mine in Scotland, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news669.htm">SchNEWS 669, 20th March 2009</a></b><br />
<b>Stand Up To Detention</b> - As the British Home Secretary opens another detention centre for 'failed' asylum seekers, No Borders protests around the country highlighting the plight of those victims of the UK asylum system.... plus, the sixth anniversary of the murder in Gaza of US activist Rachel Corrie is tragically marked by the shooting of another US activist, the largest Gypsy and Traveller community in Britain is seriously under threat of being evicted this year, Smash EDO hold an all night demo outside the Brighton factory of EDO-ITT, and more.....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news668.htm">SchNEWS 668, 13th March 2009</a></b><br />
<b>Taking It All In</b> - A giant independent UK aid convoy involving 110 vehicles travels 6000 miles to deliver medical and other supplies to war-torn Gaza.... plus, Smash EDO protester is arrested for criminal damage after banging a metal fence with a piece of plastic, two stories about squatted social centres in London and Brighton resisting eviction, Shell To Sea campaigner gaoled for four weeks in an action against a Shell oil refinery and offshore pipeline, student activist Hicham Yezza is sentenced to nine months imprisonment under an immigration technicality, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news667.htm">SchNEWS 667, 6th March 2009</a></b><br />
<b>Fools Rush In</b> - As the financial leaders preapre to gather, plans for mass G20 actions in London take shape, plus.... the Coroners and Justice Bill is the latest Big Brother law trying to snea its way through parliament, Tintore protesters up the fight against Tesco developers, EDO activists get charges increased to conspiracy, Hunger striker makes point at 10 Downing St., and more...</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/676-startTrek-lg.jpg" target="_blank">
											<img src="../images/676-startTrek-sm.jpg" alt="They're forcing you to go back to Vulcan Spock, but why?!" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 22nd May 2009 | Issue 676</b></p>
<p><b><i>WAKE UP!! IT'S YER AT YOUR OWN EXPENSE...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news676.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news676.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6761.php">Final Frontiers</a>  |  <a href="../archive/news6762.php">Not A Blind Brit Of Difference</a>  |  <a href="../archive/news6763.php">War That Broke The Tamils' Back</a>  |  <a href="../archive/news6764.php">Now We're Rolling</a>  |  <a href="../archive/news6765.php">And Finally</a> 
</p>


<div id="article">

<h3>FINAL FRONTIERS</h3>

<p>
<p><strong>AS WE BOLDLY TAKE A SCHNEWS CRUISE TO CALAIS</strong> <br />  <br /> &ldquo;<em>Please tell your government and your media that we are here, then I&rsquo;m sure they will help us. There is a family; a young Kurdish girl only five-years-old in the Jungle. The police came and gassed the family, she is very weak. I am OK, but I am worried for her, please go back and tell them that we are here</em>.&rdquo; - <strong>Iraqi Kurd in Calais.&nbsp; <br /> </strong> <br /> A humanitarian crisis caused by British immigration policy is taking place just over the channel in Calais. Thousands of people in search of a new life in the area are risking their lives to enter the UK and are only surviving with the &lsquo;illegal&rsquo; solidarity of local people. The situation has worsened dramatically since the forced closure of the Red Cross&rsquo;s Sangatte centre. <br />  <br /> A team from the No Borders network recently visited Calais &ndash; in advance of the No Borders Action camp (23-30th June). The following are edited extracts from their report: <br />  <br /> &ldquo;<em>Following a scare campaign by the right wing press, in 2002 an agreement between UK and French governments shut the Sangatte Red Cross centre. The centre provided shelter for 2000 people and its closure pushed the situation for migrants in Calais to crisis point. Large groups from Afghanistan, Eritrea, Iraq, Sudan and Palestine gather there before attempting to cross the channel, by clinging on to the underside of lorries or attempting to walk the length of the tunnel. <br />  <br /> &nbsp;Speaking to some of the migrants we learned how up to 1000 people are living in woods near  the ferry port known as &lsquo;The Jungle&rsquo;, all waiting for a chance to travel to the UK. Police regularly destroy or even burn their temporary structures and  tear gas their tents. More repression seems imminent with French immigration minister Eric Besson stating he wants an &lsquo;exclusion zone&rsquo; for migrants in the region</em>.&rdquo; - <strong>NBN <br /> </strong> <br /> Across the globe millions are on the move - responding to war, climate change or economic needs. The rich world&rsquo;s response is to throw up a network of fences, cameras and checkpoints to &lsquo;manage&rsquo; this movement. One bottleneck for migrants is Calais &ndash; the final hurdle before entering Blightly. The UKs borders now extend to this portion of France (for the first time since 1558 for you history buffs). &nbsp; <br />  <br /> So why are migrants heading here? Nobody&rsquo;s coming here looking for a &lsquo;soft touch&rsquo; no matter what you read in the Sun. The fact is that thanks to our globe-spanning imperialist past we have had an impact on countries all around the world. English is the global language while many British polices and investments abroad are responsible for forced migration. Waging war in the Middle East, exporting weapons to military regimes and supporting large infrastructure projects, such as dams, oil and gas pipelines and mines have all provoked a mass exodus of people. Dams alone, many backed by Britain cash, have displaced an estimated 80 million since the Second World War.&nbsp; <br />  <br /> The tightening of border controls across  Europe and in the UK has created a de-facto underclass across the Union &ndash; people without states or rights or access to vital infrastructure such as health care. While genuinely free movement is feared by our dark masters, &lsquo;managed&rsquo; migration can serve their interests very nicely &ndash; cheap black market labour undermining legal workers status. <br />  <br /> This process of &lsquo;managed migration&rsquo; involves a network of detention centres which incarcerates refused asylum seekers and refugees, picks up people on entry to the UK or those who&rsquo;ve overstayed their visas. These people are detained for weeks, months and even years. Some are currently being held in detention indefinitely. At any one time there are around 3,000 people detained under immigration powers in this country. With the detention centres run privately this means there are plenty of fat contracts out there for companies wanting to get in on the refugee detention game. <br />  <br /> <strong>BORDERLINE INSANITY</strong> <br />  <br /> The suffering inflicted on migrants in Calais is just one example of a brutal system whose apparent purpose is to deter others from coming but which also creates the conditions for an illegal workforce that undermines the rights of all workers. Calais is just one example of the hi-tech EU borders regime that has resulted in the death (many at sea) of at least 14,000 migrants in the last twenty years, according to a recent press review.&nbsp; <br /> The EU policy of the &ldquo;free movement of persons&rdquo; within its borders has gone hand in hand with an attempt to build &lsquo;Fortress Europe&rsquo;; externalising EU borders into Africa and Asia with EU border guards patrolling the Mediterranean, Libya and off the West Coast of Africa, courtesy of the Frontex borders agency. Also via the European Neighbourhood Policy where countries from the Ukraine all the way round the Mediterranean to Morocco are now paid by the EU to do its migration prevention work for it.&nbsp; <br />  <br /> Although most internal European borders were removed by the 1990 Schengen Convention. But while the Schengen agreement was a mechanism for allowing the &lsquo;free movement&rsquo; of certain people within certain EU states, it has always gone hand in hand with an increase in internal borders and social controls, such as SIS (Schengen Information System). Another recent manifestation of Schengen policy has been the creation of Frontex, an armed transnational border police that works almost exclusively outside EU territory.&nbsp; <br />  <br /> And meanwhile the UK has continued to ferociously police its borders, nowhere more so than in trying to stop people coming in from Calais, leaving people there between the proverbial rock and a hard place. As NBN report: <br />  <br /> "<em>French law specifically forbids aiding &lsquo;illegal immigrants&rsquo;, punishable by up to five years in prison and a &pound;25,000 fine. Despite this, we met with two local humanitarian groups who distribute free food to migrants - these volunteers risk arrest daily simply by feeding people. <br />  <br /> One friend who has lived with the migrants in Calais for several months explained to us how the night-time routine involves numerous midnight escape attempts. Nearby truck stops are patrolled by gangsters, who wander the night with knives out, wearing Balaclavas and demanding ransom from any migrant wishing to pass. Just like on the Greek, Italian and Turkish borders the non-existence of any legal entry for asylum-seekers has produced a market of illegal transport facilities, a prospering business at the EU-borders. Border controls have produced this market while criminalising the migrants. Draconian penalties against &ldquo;people trafficking&rdquo; drives up transportation prices making the market more attractive to criminal elements, as well as making a secure arrival at the aimed destination impossible.</em>&rdquo;&nbsp; <br />  <br /> No Borders is a transnational network of autonomous groups who advocate freedom of movement and equality for all, but more than that it is a political position; seeing borders as systemic to a world order that is supremely unjust and is structured towards massive inequality, acting as a useful control to divide a social hierarchy of legal/illegal, documented/undocumented, citizens/non-citizens. No Borders means rejecting this categorisation and calls for unity between exploited people against the rich and powerful. <br />  <br />  <br />  <br /> No Borders are holding a transnational demo for freedom of movement and the abolition of migration controls in Calais on the 27th of June. <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> <br />  <br />  <br />  <br /> *<strong>Activists from the Stop Deportation Network blockaded Colnbrook detention centre</strong> on 12th May in a last-ditch attempt to prevent the mass deportation of around 45 people to Iraqi Kurdistan. The six activists efwfectively blocked the entrance to the detention centre for four hours after which time they were cut from their lock-ons and arrested for obstruction of the highway. Tinsley House, a detention centre near Gatwick has also been targeted in recent months (see <a href='../archive/news669.htm'>SchNEWS 669</a>).</p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=calais&source=676">calais</a>, <span style='color:#777777; font-size:10px'>colnbrook</span>, <a href="../keywordSearch/?keyword=deportation&source=676">deportation</a>, <span style='color:#777777; font-size:10px'>detention centre</span>, <a href="../keywordSearch/?keyword=immigration&source=676">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=676">no borders</a></div>
<?php echo showComments(329, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>NOT A BLIND BRIT OF DIFFERENCE</h3>

<p>
With an uncharacteristic display of conscience the British government has finally stopped military aid to Colombia. So what provoked this moral U-turn from what was the second biggest financial backer of the Colombian military (which still receives billions of dollars from the US under the controversial &lsquo;Plan Colombia&rsquo;). And will it make any difference? <br />  <br /> Since coming to power in 2002 Colombian president Alvaro Uribe has vigorously pursued his policy of &lsquo;Democratic Security&rsquo; - which involved strengthening the military and going on the offensive against the FARC, Colombia&rsquo;s dominant guerilla force. While the FARC have been retreating, the policy has also seen the Colombian military overtake both the FARC and Colombia&rsquo;s numerous right-wing paramilitary groups to become the leading perpetrators of human rights abuses. In 2002 the state was responsible for 17% of recorded violence. By 2006 that number had jumped to 56%. <br />  <br /> In recent months it has become increasingly obvious that recorded human rights violations only represent a small proportion of the atrocities committed in Colombia. Both the military and the police have a long history of colluding with paramilitary organisations committing numerous massacres throughout the country. Recent weeks have seen allegations that they have also been conspiring with the militias to burn the bodies of massacre victims in an effort to conceal the number of people killed. <br />  <br /> In addition to this, investigators are looking into the 1,296 cases of extra-judicial executions that have occurred since 2002. The murders were so called &lsquo;false positives&rsquo; &ndash; a practice where soldiers murder civilians in rural areas before dressing them up in combat fatigues and labelling them guerillas. In the last few weeks 67 soldiers have been convicted for murdering civilians while more than 400 have been arrested. Uribe, meanwhile, has stated that he believes there are many false accusations and he wants the state to take up the legal defence of the accused military personnel. <br />  <br /> While much of the violence appears to be determined by little more than location &ndash; most victims were from strategically or economically important areas &ndash; some of it is far more targeted. More union leaders are assassinated each year in Colombia than in the rest of the world combined. The most recent, Edgar Martinez of the farmers and miners union in the department of Bolivar, was killed earlier this month shortly after being turned back from a police road block. &nbsp; <br />  <br /> The indigenous communities of Colombia have also been repeatedly targeted. Following the &lsquo;Minga&rsquo; protest movement of last November (see <a href='../archive/news656.htm'>SchNEWS 656</a>), the husband of  Aida Quilcue, Chief Council of CRIC (Indigenous Regional Council of Cauca) - the architects of the uprising - was assassinated at a military roadblock in what appeared to be an attempt on Quilcue&rsquo;s life. The assassinations have continued since, last week Roberth Guachet&aacute;, another community leader and a protected person by the Inter-American Court of Human Rights, was found beaten to death. Indigenous communities throughout rural Colombia have also been targeted by both the army and the guerillas with military attacks, occupation of villages and disappearances all routine. <br />  <br /> Journalists critical of Uribe also continue to be subjected to death threats, forcing many into exile. Two weeks ago veteran Colombian journalist Jos&eacute; Everardo Aguilar, a noted critic of official corruption, was shot dead on his doorstep.&nbsp; <br />  <br /> Throughout his time in power Uribe has consistently accused trade unionists, indigenous activists and journalists of collaborating with the guerillas, singling them out as targets for paramilitary and military attacks. <br />  <br /> While it makes a welcome change for the British government to take notice of a state complicit in the murder and abuse of its citizens, Britain will continue to send Uribe and his military force secret and unconditional counter-narcotics assistance. With a government quite happy to label dissenters in any way that suits their purpose and a military quite happy to do the state&rsquo;s dirty work it seems unlikely that this latest development will make much difference to the hundreds of thousands of Colombians caught up in the conflict.&nbsp; <br />  <br /> See <a href="http://www.colombiajournal.org/index.htm" target="_blank">www.colombiajournal.org/index.htm</a> and <a href="http://mamaradio.blogspot.com" target="_blank">http://mamaradio.blogspot.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>alvaro uribe</span>, <a href="../keywordSearch/?keyword=colombia&source=676">colombia</a>, <span style='color:#777777; font-size:10px'>democratic security</span>, <span style='color:#777777; font-size:10px'>farc</span>, <a href="../keywordSearch/?keyword=human+rights&source=676">human rights</a>, <span style='color:#777777; font-size:10px'>minga</span>, <span style='color:#777777; font-size:10px'>quilcue</span></div>
<?php echo showComments(330, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>WAR THAT BROKE THE TAMILS' BACK</h3>

<p>
Following battles which have raged since late last year (see <a href='../archive/news672.htm'>SchNEWS 672</a>, <a href='../archive/news665.htm'>665</a>), the Sri Lankan government declared victory in its 26 year war against the separatist Liberation Tigers of Tamil Eelam &ndash; the Tamil Tigers - this Tuesday (19th). After squeezing the remaining rebels into an ever smaller area the declaration came after the death of the Tigers&rsquo; leader Velupillai Prabhakaran whose corpse was paraded on TV the same day. <br />  <br /> The announcement followed weeks of intensifying fighting with rapidly escalating casualty figures. The area designated by the Sri Lankan army as a &lsquo;safety zone&rsquo; for escaping civilians has been shelled repeatedly in attacks which included the repeated shelling of several hospitals, killing hundreds of wounded civilians. The Sri Lankan army denies attacking the area despite massive evidence that they have repeatedly used heavy artillery, air strikes, cluster bombs and white phosphorous on Tamil civilians. Health workers in the area estimate that up to 15,000 people, including 2000 children, have died in the last few months of fighting. <br />  <br /> Tamils fleeing the conflict area have come come under fire from both the Sri Lankan army and LTTE fighters. Around a quarter of a million of those that have made it out have been shepherded into heavily policed government internment camps. On Wednesday the Sri Lankan government announced that the majority of the refugees will remain in the camps, where conditions are said to be desperate, for up to two years. Thousands of former LTTE fighters, some as young as fourteen, are being held in &lsquo;rehabilitation centres&rsquo; while the &lsquo;hardcore&rsquo; rebels are being interrogated in a secure camp in the South. In the Sinhalese  areas of the country ethnic Tamils are now also being detained with increasing regularity. <br />  <br /> The Tamil diaspora meanwhile has continued to express its outrage at the actions of the Sri Lankan army and the silence of the international community, with protests from South Africa to the US. In London, demonstrations have continued throughout the week with thousands of Tamils protesting in and around Parliament Square. With the protests barely acknowledged in the national press the police have been free to respond in an unrestrained fashion. Last week a blockade of Westminster Bridge was ended when police were said to have seized the front-line protesters &ndash; children in pushchairs &ndash; and removed them to nearby police vans, forcing the parents to follow. <br />  <br /> In his victory address to parliament Sri Lankan president Mahinda Rajapaksa stated the country had been  &ldquo;liberated from terrorism&rdquo; and declared a national holiday. &ldquo;<em>Our intention was to save the Tamil people from the cruel grip of the LTTE</em>.&rdquo; he said. &ldquo;<em>We all must now live as equals in this free country</em>,&rdquo;. With hundreds of thousands of suffering and bereaved Tamils trapped in razor wired refugee camps and the Tamil diaspora still demanding justice, Rajapaksa&rsquo;s triumphalism seems as misplaced as Bush&rsquo;s &ldquo;<strong>Mission Accomplished!</strong>&rdquo; moment...&nbsp; <br />  <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=human+rights&source=676">human rights</a>, <span style='color:#777777; font-size:10px'>prabhakaran</span>, <span style='color:#777777; font-size:10px'>rajapaksa</span>, <span style='color:#777777; font-size:10px'>refugee camp</span>, <a href="../keywordSearch/?keyword=sri+lanka&source=676">sri lanka</a>, <a href="../keywordSearch/?keyword=tamil+tigers&source=676">tamil tigers</a>, <span style='color:#777777; font-size:10px'>war</span>, <span style='color:#777777; font-size:10px'>white phosphorous</span></div>
<?php echo showComments(331, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>NOW WE'RE ROLLING</h3>

<p>
Anti-Trident protesters have occupied a new site at the Rolls Royce Rayensway hi-tech manufacturing facility in Derby, in reaction to RR&rsquo;s recently acquired contract to build nuclear reactors for power stations. Already on the site RR have elements enriched with uranium and zircaloy as well as a nuclear test reactor for submarine engines equipped with Trident missiles. &nbsp; <br />  <br /> Protesters are also demanding an evacuation plan for local inhabitants and workers within a two-mile radius of the factory, which should be open to evaluation from the local council and emergency services. <br />  <br /> The disarmament camp, now a week old, is located on an old training ground for the Derby football team and has plenty of space for tents, tree-houses and the like.  Police intervention has so far been minimal.  It is easy to get to on foot along the river footpaths signed for Alvaston from the city centre.   <br />  <br /> Map links and details at: <a href="http://www.indymedia.org.uk/en/2009/05/429776" target="_blank">www.indymedia.org.uk/en/2009/05/429776</a> see also <a href="http://www.tridentploughshares" target="_blank">www.tridentploughshares</a> for more general info&nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>derby</span>, <a href="../keywordSearch/?keyword=nuclear+weapons&source=676">nuclear weapons</a>, <a href="../keywordSearch/?keyword=protest&source=676">protest</a>, <span style='color:#777777; font-size:10px'>rayensway</span>, <span style='color:#777777; font-size:10px'>rolls royce</span>, <a href="../keywordSearch/?keyword=trident+missiles&source=676">trident missiles</a></div>
<?php echo showComments(332, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY</h3>

<p>
A group of friends from Brighton off for a weekend camping trip in a hired minibus had their plans disrupted at the last minute when their driver turned up to present his driving license. The hire company told him that he could not have the van as he was a &ldquo;dangerous criminal&rdquo; and the company was worried about what the van could be used for. <br />  <br /> &nbsp;The hire company in Brighton told him they had been visited by police from Nottingham who warned the company about him &ndash; arising from when he&rsquo;d previously hired a minibus sometime  before being one of the 114 arrested when gathering at a school under suspicion of conspiracy to close down a power station (<a href='../archive/news672.htm'>SchNEWS 672</a>).&nbsp; <br />  <br /> SchNEWS has heard of a similar story relating to last year&rsquo;s Climate Camp &ndash; a hire company in Hastings was told by police not to rent vans to a climate activist &ndash; but the hire company told the police where to go saying they knew the man and he was one of their best customers. <br />  <br /> After all where would minibus rental companies be without their bread and butter patrons? In police state Britain we guess the protest market is thriving at the moment... <br />  <br />
</p>

</div>
<?php echo showComments(333, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS warns all readers - pay for your own bloody moat. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

