<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(149);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	149;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 780 - 22nd July 2011 - As Famine Strikes Somalia, The US Just Serves Up More Bombs </title>
<meta name="description" content="Somalia Civil war and famine and now Predators are circling, Flotilla and Flytilla for Palestine up against the world according to Israel?, Food charities pack up for hols leaving who to take care of migrants?, and more..." />
<meta name="keywords" content="SchNEWS, direct action, Brighton, africa, somalia, greece, mexico, cartel, drugs, narco wars, brazil, rainforest, latin america, coal, energy, eviction, direct action, riots, uk uncut, no borders, migrants, refugees, asylum seekers, greece, anarchists, financial crisis, ratcliffe-on-soar, climate camp, edl, anti-fascist, portsmouth, media, parliament, uk uncut, freedom flotilla, israel, palestine, greece, calais, no borders, migrants" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news779.htm">SchNEWS 779, 15th July 2011</a></b><br />
<b>Rolling Out the Unwelcome Mat</b> -
	The closure of the Immigration Advisory Service on Monday (11th), following new legislation denying legal aid for all immigration cases, has left hundreds of migrants and asylum seekers without legal support. The service had been running for 35 years from London and regional offices, with 300 employees. At the time of the shock closure - which staff discovered via notices on doors when they turned up for work - there were 650 active cases. Claimants are now being told to ask courts for their hearings to be delayed and to find themselves alternative legal representation.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news778.htm">SchNEWS 778, 8th July 2011</a></b><br />
<b>It's All Over</b> -
	Yes the world of alternative jounalism was rocked to it&rsquo;s core this week as SchNEWS International&rsquo;s mogul boss Rupert Makepeace announced the sudden closure of the paper. Britain&rsquo;s most well-loved anarcho-newsheet has been serving up it&rsquo;s unpopular brand of poorly researched facts, corporate and political sleaze, eco-terror propaganda and non-gender specific titillation for 168 months, and commentators are still in shock that it will be no more.

	&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news777.htm">SchNEWS 777, 1st July 2011</a></b><br />
<b>Libya: Anti-Nato Classes</b> -
	The bombs are still falling on Tripoli in yet another British intervention on behalf of Arab human rights in the Middle East (the 46th since 1945). What started out as a U.N backed campaign&nbsp; supposedly to protect anti-Gaddafi elements from a massacre has turned into reckless use of air-power to oust Gadaffi from power. And yet the UK peace movement is virtually nowhere to be seen.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news776.htm">SchNEWS 776, 24th June 2011</a></b><br />
<b>Squatting on Heaven's Door</b> -
	As expected: Following the opportunistic yelps of a jumped-up tory media-whore, against the background of a right-wing ideological crusade, the criminalisation of squatting is now on parliament&rsquo;s agenda. This week the government announced a brief consultation period was under-way on the issue of occupation without authorisation. The sights of the Tory legislative blunderbuss are slowly being zeroed in, battle lines are being drawn and arguments rehearsed. As every day goes by, the corporate press prejudice people further against one of the few remaining laws that empowers the many against the few.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news775.htm">SchNEWS 775, 17th June 2011</a></b><br />
<b>Flaming June?</b> -
	Is the big fight on? Union responses to the Tory cuts have so far been fairly muted - the M26 outing, resembling a cross between a family picnic and a Labour Party rally. However on June 30th the largest public sector strikes since the 80s are planned. With the Daily Mail claiming that &lsquo;Union Barons&rsquo; plan to &lsquo;unleash hell&rsquo;, what&rsquo;s actually going to happen?
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news774.htm">SchNEWS 774, 10th June 2011</a></b><br />
<b>Going to Hell-as</b> -
	Over a thousand migrants have been arrested and made homeless over the last two months after the Greek government launched a sweeping crackdown in the port city of Igoumenitsa. Last month the Greek government announced the scorched earth policy at Igoumenitsa alongside plans to begin the construction of 14 new detention and deportation centres.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news773.htm">SchNEWS 773, 27th May 2011</a></b><br />
<b>No Spain, No Gain</b> -
	We&#39;ve got the Arab Spring &ndash; what about a European summer? It&#39;s a week since thousands of pro-democracy demonstrators pitched up in central Madrid - and revolution fever is spreading across Europe like nits in a playground. With the Spanish sit-in still going strong, street demonstrations have also hit Greece, Georgia, and, er, Bristol. Protests are spreading to Italy, France, Portugal, Austria even German - could it be that last year&#39;s initial protests against austerity measures are maturing, one year on, into a broader demand for political reform?
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news772.htm">SchNEWS 772, 20th May 2011</a></b><br />
<b>A Bit of Hows Yer Intifada?</b> -
	A mass non-violent (or at least unarmed) resistance movement is on the move in Palestine. Inspired by the events in neighbouring Arab countries but drawing on decades of resistance - the Third Intifada may be here. The waves of the Arab movement are beginning to lap at the Israeli shore.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news771.htm">SchNEWS 771, 13th May 2011</a></b><br />
<b>Fracking Hell</b> -
	A few months since Fukishima, nearly a year since Deepwater Horizon but the global elite aren&rsquo;t resting on their laurels and it looks like we won&rsquo;t have to wait too long for the fossil fuel industry to cause the next environmental apocalypse. New kid on the block is the appropriately named &lsquo;fracking&rsquo; &ndash; or, more explicitly: hydraulic fracturing, a method of extracting natural gas from shale rock layers thousands of feet deep &ndash; and its coming here soon.
</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">

<div style='font-weight: bold; color: #888888; text-align: right'>
	Tuesday 26th July 2011
</div>

<div style='font-weight: bold; font-size: 16pt; text-align: center; border-top: 1px solid #999999; border-bottom: 1px solid #999999; margin: 15px; padding: 10px'>

	The Rise of the Nutters<br />
	This is Norway to Go..?

</div>

<p>
<b><i>"The lunatic is all idee fixe, and whatever he comes across confirms his lunacy. You can tell him by the liberties he takes with common sense, by his flashes of inspiration, and by the fact that sooner or later he brings up the Templars."</i> - Umberto ECO, Foucault's Pendulum</b>

<p>
The Norway attacks have led to frenzied speculation around the nature of the crazed ideology that could lead to such horrific acts. Handily Andre Brievik (or Andrew Berwick) not only allowed himself to be captured but posted a 1500 page manifesto online - Grandly entitled 2083 - A European Declaration of Independence (<a href='http://www.slideshare.net/darkandgreen/2083-a-european-declaration-of-independence-by-andrew-berwick' target='_BLANK'>www.slideshare.net/darkandgreen/...</a>) , apart from a load of weird stuff about body-armour, the Knights Templar and the appropriate use of steroids, at the heart is a vision eerily familiar to readers of such fringe esoteric publications as say the Daily Express, Daily Mail or Sun.

<p>
The fact that before the smoke had cleared from the first detonation in Oslo the mainstream media immediately pointed the finger at Al-Qaeda or one of its offshoots shows that their underlying assumptions aren't so different from the self-proclaimed Knight Templar Justiciar with the 'piercing blue eyes' - (copyright all newspapers.).

<p>
Experts and pundits queued up to explain why peaceful, civilised Norway had been attacked by the jihadist hordes. The Sun called it Norway's 9/11 and even name-checked Al-Qaeda. Even when the Norwegian press revealed that the killer was an 'ethnic Norwegian' pundits quickly raised the prospect of a brainwashed convert to Islam. A spectacular amount of back-pedalling ensued when the actual facts surfaced (not that this of course stopped the very same pundits becoming sudden experts on the far-right)

<p>
The idea that we are at war with the entire Muslim World and that a 'politically correct' elite has manipulated immigration to the detriment of the ethnic majority is now a mainstream belief in Western Europe. There's no need to go trawling through SpearofOdin88s lunatic ramblings on Stormfront when you can go straight to the Daily Mail and read Melanie Philipps - author of Londonistan, the EDLs bible, who recently demanded to know if "Whooaa! Is Britain finally about to go over the cliff into official Islamisation?" ( Philipps has the honour of having her parts of her work reproduced in their entirety in Breivik's manifesto, but rather side-stepped the issue this week by focussing instead on the tragic but perhaps less significant demise of Amy Winehouse.

<p>
The Daily Mail has now denounced Breivik as neo-Nazi, despite his explicit rejection of Nazism - in fact politically he wasn't much more right-wing than them. There is now a concerted attempt to divorce Breivik's ideas from his actions - to suggest that he was just 'insane' or 'sick', even calls to not allow his monstrous actions to 'shut down the debate on immigration'. When jihadists commit an outrage there isn't usually such a rush to let their ideology off the hook.

<p>
It's the Desmond papers that lead the pack though - In any given week the Express is rarely without an anti-immigrant front-page splash and generally one that suggests EU compulsion. such as 'Eurocrats will force British families to have an asylum-seeker in spare bedroom'. OK, we made that one up but in fact the headline BRITAIN 'MUST TAKE MORE MIGRANTS' was last Friday's offering, claiming the EU bureaucrats are forcing the pace on migration The idea that the left is forcing 'political correctness' down the the throats of the 'silent majority' of the population is continually represented as a given. Of course it was the youth wing of the socialist (centre-left) party that Breivik attacked - not any immigrants.

<p>
A big part of this ideology is an old racist canard - the demographic time-bomb. The fear is that 'They' will outbreed us and force us to adapt to their culture. Of course the fact that all this was being said in the 70s about the Afro-Caribbean population and before that the Irish and the Jews doesn't stop the idea. "We have only a few decades to consolidate a sufficient level of resistance before our major cities are completely demographically overwhelmed by Muslims" - Breivik " Will the white British population be in a minority in 2066?" - headline Daily Mail December 2010

<p>
From the outside, it seems insane to fear that the U.K could become subject to anything like sharia law. Apart from a few headcases, many of them zealous converts, nobody Muslim wants there to be sharia law here either. Yet resistance to this is the main platform of the EDL, who at the same time have no idea about what the culture is that they are defending - is it British, English or white European? Breivik is sure that the foundation of Western Civilisation should be Catholicism -some of the EDL make vague noises about Christianity but don't really seem that sure who they're in league with or what they're defending.

<p>
In fact it's fair to say that a large part of the population exists in an echo-chamber, where these are received truths and amount to 'common sense' . The online world, rather than a gateway to a variety of points of view provides a fertile ground for reinforcement of ideas (something the left should be as wary of as the right). Backing up the overt statements in the mainstream press are a host of circular e-mails, with a racist agenda doing the rounds as well as blogs, facebook postings etc. The EDL are the physical manifestation of this belief system. It's because their beliefs are reinforced from all sides that they've been able to grow so quickly.

<p>
So in a sense - it doesn't matter whether Breivik was in direct contact with the EDL, whether he regarded them as 'naive' or an inspiration- their ideas spring from the same soil. However evidence is emerging that he admires them, did attend several of their demos and was active on their forums and Facebook. He is positive about their provocation/reaction strategy, baiting Muslim youth into a reaction. The EDL is very much an online phenomenon - they don't do public meetings or paper sales, their membership exists in the right wing 'echo-chamber'. Who or what else is going to bubble up from the EDL cauldron?

<p>
<b><i>"It is highly advisable to structure any street protest organisation after the English Defence League (EDL) model as it is the only way to avoid paralyzing scrutiny and persecution"</i> - Breivik.</b>

<p>
The EDL are also part of a new-wave of European nationalism, one that claims to defend a 'pan-European' culture. The U.K is in a strange situation with the far-right. Despite a certain overlap in membership there is no degree of co-operation between the EDL and the BNP. The EDL articulate a modern racism, based on Englishness rather than Britishness. In fact attempts to set up Welsh and Scottish Defence leagues have proved a failure. The BNP are still too firmly tied to old-school 'It was the Elders of Zion wot dunnit' white supremacist fascism. The EDL has side-stepped these issues and by representing the conflict with Islam as cultural rather than racial, have out-boxed their flatter footed leftwing opposition

<p>
The EDL are closer to the likes of Geert Wilders, leader of the Netherlands third largest political party - the Freedom Party, whose main platform is anti-Islamic. He travelled to the UK in March 2010, after a ban on his presence was overturned. This event was greeted with great enthuisiam by the EDL who organised a rally for him. Other European politicians have latched onto the fear of Islam and they too are fascinated by the EDL as a potential model for a street army. The EDL leadership in turn have shown an interest in more electoral conventional politics.

<p>
In a time of economic crisis, ideas like these are dangerous. As Mark Twain put it "History doesn't repeat itself, but it does rhyme". If one good thing comes out of Breivik's murder spree it will be a re-examination of the anti-Islamic agenda. The worst thing to do in these circumstances would be to dismiss his actions as those of a lone nutter. The EDLs next major outing is September 3rd in Tower Hamlets - anti-fascists will be counter-mobilising.


</div>

<div style='border-bottom: 1px solid black'>&nbsp;</div>

			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A href="../schmovies/index.php" TARGET="_blank">RAIDERS OF THE LOST ARCHIVE</A></B>
			-<B> Part one of the SchMOVIES collection 2009-2010 </B>
			- This DVD features a number of films which were held by Sussex police for over a year following the raid and confiscation of all SchMOVIES equipment during an intelligence gathering operation in June 2009 related to the <a href='http://www.smashedo.org.uk/' target='_BLANK'>Smash EDO</a> campaign.</p>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

