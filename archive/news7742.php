<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 774 - 3rd June 2011 - Hinkley: Gone Frission</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, nuclear energy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 774 Articles: </b>
<p><a href="../archive/news7741.php">Going To Hell-as</a></p>

<b>
<p><a href="../archive/news7742.php">Hinkley: Gone Frission</a></p>
</b>

<p><a href="../archive/news7743.php">Not Playing Workfare</a></p>

<p><a href="../archive/news7744.php">Basildon Brush Off</a></p>

<p><a href="../archive/news7745.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7746.php">Hasta La Democracia</a></p>

<p><a href="../archive/news7747.php">Smooth Operators</a></p>

<p><a href="../archive/news7748.php">Gaza Freedom Flotilla Ii</a></p>

<p><a href="../archive/news7749.php">Ukba-rmy</a></p>

<p><a href="../archive/news77410.php">Schnews In Graphic Detail Out Now</a></p>

<p><a href="../archive/news77411.php">Gm: Sooner Or Tater</a></p>

<p><a href="../archive/news77412.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd June 2011 | Issue 774</b></p>

<p><a href="news774.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HINKLEY: GONE FRISSION</h3>

<p>
<p>  
  	New nuclear rumblings are afoot as EDF put in a planning application to clear a site for Hinkley C, the first new nuclear development in the UK since Sizewell B in 1988.  </p>  
   <p>  
  	The proposed works would clear more than 400 acres of grassland, woodland and hedgerows around the two existing nuclear sites at Hinkley in Somerset. This would involve diverting footpaths; laying drainage infrastructure; constructing car parks, haulage and access roads; the undertaking of massive earthworks, concrete reinforcing and putting in provision for the operation of plant and machinery. And all this before formal permission to actually build the power station has been granted.  </p>  
   <p>  
  	Hinkley A was decommissioned and went out of service in 2000, with Hinkley B due to be retired in 2016. After the 2002 UK government moratorium on building any new nuclear facilities, it looked like the same fate would befall all operating nuclear power stations in the UK. However, in 2008 the government announced eight sites they thought suitable for new nuclear. Hinkley is the first to get works underway (see <a href='../archive/news763.htm'>SchNEWS 763</a>).  </p>  
   <p>  
  	And it&rsquo;s not just brand-new sites that are going to gobble the uranium. EDF have stated that they&rsquo;re going to extend the lifespan of two of its UK-based nuclear stations by five years, Heysham 1 in Lancashire and Hartlepool power station in County Durham. Both plants were due to shut in 2014, but will now continue until 2019. The company is eyeing up the other five reactors it operates, with Sizewell B in Suffolk potentially being kept open for an additional 20 years.  </p>  
   <p>  
  	In response to the application at Hinkley, Stop New Nuclear (an alliance of Campaign for Nuclear Disarmament, Stop Nuclear Power Network, Kick Nuclear, South West Against Nuclear, Trident Ploughshares, Stop Hinkley, Shutdown Sizewell and Sizewell Blockaders) have announced a non-violent blockade of the site on 3rd October. Campaigners are heralding the action as the a definitive moment in the future of nuclear in the UK, stating &ldquo;<em>If [the industry] fails at Hinkley, it is unlikely the &ldquo;nuclear renaissance&rdquo; will have the momentum to continue</em>.&rdquo;  </p>  
   <p>  
  	* See <a href="http://www.stopnewnuclear.org.uk" target="_blank">www.stopnewnuclear.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1236), 143); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1236);

				if (SHOWMESSAGES)	{

						addMessage(1236);
						echo getMessages(1236);
						echo showAddMessage(1236);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


