<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 669 - 20th March 2009 - Caravanguard</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dale farm, romany, gypsy, essex, constant & co, basildon, caravan sites act, direct action, eviction" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 669 Articles: </b>
<p><a href="../archive/news6691.php">Stand Up To Detention</a></p>

<p><a href="../archive/news6692.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6693.php">U S Activist Shot In Palestine</a></p>

<b>
<p><a href="../archive/news6694.php">Caravanguard</a></p>
</b>

<p><a href="../archive/news6695.php">Doing Whirly-bird</a></p>

<p><a href="../archive/news6696.php">Big Touble</a></p>

<p><a href="../archive/news6697.php">Pieces Of Ait</a></p>

<p><a href="../archive/news6698.php">Hoi-dee-hoi</a></p>

<p><a href="../archive/news6699.php">--- Stop Press ---</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th March 2009 | Issue 669</b></p>

<p><a href="news669.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CARAVANGUARD</h3>

<p>
The Dale Farm Romany Gypsy site near Billericay in Essex is threatened with eviction this year, but got a temporary reprieve after yesterday having lodged an appeal to the House Of Lords which  stalls proceedings. If an eviction does happen &#8211; most likely by the violent &#8216;gypsy eviction specialist&#8217; bailiffs Constant & Co (See <a href='../archive/news439.htm'>SchNEWS 439</a>) - legal monitors as well as those prepared to take direct action will be needed to protect this community. They are also calling out for tents and marquees to provide temporary accommodation for the 90 families (500 people) affected. <br />
 <br />
After losing a High Court appeal earlier this year (<a href='../archive/news659.htm'>SchNEWS 659</a>), which gave Basildon Council the green light to begin eviction proceedings against the largest Gypsy and Traveller community in Britain, the residents of Dale Farm now wait for the &#8211; fortunately slow &#8211; process of the Lords. They&#8217;ll take 6-8 weeks to decide if they hear the application with a decision in 6-12 months, which could buy some time. But if the Lords refuse to even hear the application, sometime in May or June the community could receive a 28 day notice to quit and the battle to defend Dale Farm will commence. <br />
 <br />
Dale Farm has been home to Gypsies since the 1960s, when planning permission was granted for 40 families to live on the former scrapyard. Those threatened are the many more families who have arrived since, bought adjacent greenbelt land, and are living on it without planning permission. In May 2005 Basildon Council decided to evict those without planning permission, then in May 2008 this was put on hold when the High Court ruled the council was not offering an alternative site. They also urged the council to not use Constant & Co, after being appalled by a video of a previous violent eviction (See <a href='../archive/news632.htm'>SchNEWS 632</a>). In January this year, the Court Of Appeals overturned this ruling, paving the way for an eviction. <br />
 <br />
Romany and Gypsy communities across Europe are being violently ethnically cleansed. In the UK the infamous 1994 Criminal Justice Act repealed the Caravan Sites Act, freeing local authorities from the obligation to provide travellers with park-up space. Many communities bought land to live on &#8211; but 96% of the applications for permission to settle have been refused. These itinerant communities have been forced to buy land, yet are denied the right to stay there and are subject to extreme racism and prejudice where ever they go. Basildon Council have so far spent more than �750,000 on legal fees, and are prepared to put up to �2million of taxpayers money into breaking up Dale Farm. <br />
 <br />
Currently there are plans to set up a tent city on adjacent land, owned by some residents and used as pasture for ponies. The Red Cross has donated a large tent and so far they also have several marquees and portaloos left over from the 2007 Heathrow climate camp. Three local churches have offered to open church halls, with plans to move mothers with children and the elderly to these should an eviction start. <br />
 <br />
Fortifications are in place including two scaff-pole towers connected by a bridge over Oak Lane, the private road which is the only entry to the site.  Other preparations to defend the site continue. Donations of tat welcome, including: tents, scaffolding, safety fencing, barbed-wire, planks, rope, wire etc. <br />
 <br />
Soon there will be meeting for Human Rights Monitors at the House Of Commons &#8211; or if Whitehall is a bit off your manor email/telephone the contacts below. <br />
 <br />
* Info: 01206 523528, <a href="mailto:dale.farm@btinternet.com">dale.farm@btinternet.com</a> <br />
 <br />
* Snapshot on current plight of Romany Gypsies in Europe read &#8216;Coordinator Information on Evictions November 2008.pdf&#8217; at <a href="http://www.coe.int/t/dg3/romatravellers/Coordinator/default_en.asp" target="_blank">www.coe.int/t/dg3/romatravellers/Coordinator/default_en.asp</a> <br />
 <br />
* See <a href="http://www.humanrightstv.com/siege-of-dale-farm" target="_blank">www.humanrightstv.com/siege-of-dale-farm</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>basildon</span>, <span style='color:#777777; font-size:10px'>caravan sites act</span>, <span style='color:#777777; font-size:10px'>constant & co</span>, <span style='color:#777777; font-size:10px'>dale farm</span>, <a href="../keywordSearch/?keyword=direct+action&source=669">direct action</a>, <span style='color:#777777; font-size:10px'>essex</span>, <a href="../keywordSearch/?keyword=eviction&source=669">eviction</a>, <span style='color:#777777; font-size:10px'>gypsy</span>, <span style='color:#777777; font-size:10px'>romany</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(251);
			
				if (SHOWMESSAGES)	{
				
						addMessage(251);
						echo getMessages(251);
						echo showAddMessage(251);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>