<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(44);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	44;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 677 - 29th May 2009 - AS A SWARM OF M�skitoS TAKES OVER THE COAST OF NICARAGUA</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, m�skito, nicaragua, daniel ortega, moskitia, sandinistas, latin america, crap arrest of the week, queer, scotland, surrey, oil, europa oil and gas, community garden, brighton, squatting, gaza, egypt, palestine, international aid, hunger strike, london, eco-village, hammersmith, squatting, wandsworth, the land is ours, ronnie easterbrook, hunger strike, political prisoners, mojuk, huntingdon life sciences, barclays, vivisection, animal testing, animal rights, vanguard group, highbridge capital management, shac, vikki waterhouse-taylor, animal rights, vivisection, national extremism tactical co-ordinating unit, netcu, socpa, direct action, shac, stop sequani animal torture, hicham yezza, immigration, bnp, british national party, far-right, neo-nazis, racism, postal workers, bnp, british national party, far-right, neo-nazis, racism, cyber-attack, nick griffin" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.london.noborders.org.uk/calais2009" target="_blank"><img
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news676.htm">SchNEWS 676, 22nd May 2009</a></b><br />
<b>Final Frontiers</b> - No Borders special as SchNEWS reports on the humanitarian crisis in Calais caused by British immigration policy... plus, will the belated withdrawal of British backing affect the Colombian military and their reign of violence, the Sri Lankan government claims victory over the Tamil Tigers but we question at what price, anti-Trident protesters have occupied a new site at the Rolls Royce Rayensway hi-tech manufacturing facility in Derby, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news675.htm">SchNEWS 675, 8th May 2009</a></b><br />
<b>Brighton Rocked</b> - As Smash EDO Mayday mayhem hits the streets of Brighton we report from the mass street party and demonstration against the arms trade, war and capitalism... plus, the bike-powered Coal Caravan concludes its three week tour of the North Of England, a US un-manned Predator drone accidently bombs the village of Bala Baluk, killing up to 200 innocent civilians, RavensAit, the squatted island on the Thames near Kingston, is finally evicted this week, the Nine Ladies protest camp, having won its ten-year battle to stop the quarrying of Stanton Moor in Derbyshire, has finally tatted down and finished, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news674.htm">SchNEWS 674, 1st May 2009</a></b><br />
<b>May the Fourth be With You</b> - With Smash EDO's Mayday Mayhem about to hit Brighton, here's some useful information covering the day.... plus, RBS&nbsp;are seeking &pound;40,000 in compensation from a seventeen year old girl who damaged one computer screen and keyboard at the G20 demo last month, the Metropolitan Police are forced to admit they assaulted and wrongfully arrested protesters during a 2006 demo at the Mexican Embassy in London, the IMF/World Bank meeting in Washington DC is met with three days of protests,&nbsp;one London policeman is sacked for admitting that the police killed someone while another is merely disciplined for saying that he 'can't wait to bash up some long haired hippys', and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news673.htm">SchNEWS 673, 24th April 2009</a></b><br />
<b>Quick Fix</b> - The twelve students arrested this month as alleged 'terrorists' have been released without charge - like many 'anti-terrorist operations' in Britain, this one becomes a joint operation between the police and the media... plus,&nbsp;a protester in the West Bank town of Bil'in is killed by the Israel military while protesting against the 'Apartheid Wall',&nbsp;Birmingham squatters prevent two separate community squats in the city from being evicted on the same day, the bloodshed continues in Sri Lanka,&nbsp;a coalition hasformed called The United Campaign Against Police Violence, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news671.htm">SchNEWS 671, 3rd April 2009</a></b><br />
<b>G20 SUMMIT EYEWITNESS REPORT</b> - As the G20 Summit takes place, we look at what the leaders are discussing, along with an eyewitness account of the G20 protests around the Bank Of England on Wednesday April 1st.... plus, a report from day two of the protests, on the day when the G20 Summit began at the ExCel entre in the East London Docklands, the squatted island at Raven's Ait, on the Thames near Surbiton, is under threat from eviction and calling for help, groups of sacked workers who are feeling the direct consequences of the financial crash are protesting, and re-occupying their work places, peace campaigner Lindis Percy causes traffic chaos outside the USAF airbase at Lakenheath in Suffolk, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news670.htm">SchNEWS 670, 27th March 2009</a></b><br />
<b>G20 SUMMIT SPECIAL</b> - SchNEWS looks how deep the financial problems are for the banks and the British Govt, and how they won't learn from their errors. We ask the question: the crash which looked inevitable in 1999 has happened, so we look to how we can survive this.... plus, ex-Royal Bank Of Scotland boss Fred 'The Shred' Goodwin, has his home and car attacked by demonstrators, Horfield Prison in Bristol is surrounded by a noise demo in solidarity with Elija Smith, one of the EDO Decommissioners, Climate change campaigners stop work at Muir Dean open-cast coal mine in Scotland, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news669.htm">SchNEWS 669, 20th March 2009</a></b><br />
<b>Stand Up To Detention</b> - As the British Home Secretary opens another detention centre for 'failed' asylum seekers, No Borders protests around the country highlighting the plight of those victims of the UK asylum system.... plus, the sixth anniversary of the murder in Gaza of US activist Rachel Corrie is tragically marked by the shooting of another US activist, the largest Gypsy and Traveller community in Britain is seriously under threat of being evicted this year, Smash EDO hold an all night demo outside the Brighton factory of EDO-ITT, and more.....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news668.htm">SchNEWS 668, 13th March 2009</a></b><br />
<b>Taking It All In</b> - A giant independent UK aid convoy involving 110 vehicles travels 6000 miles to deliver medical and other supplies to war-torn Gaza.... plus, Smash EDO protester is arrested for criminal damage after banging a metal fence with a piece of plastic, two stories about squatted social centres in London and Brighton resisting eviction, Shell To Sea campaigner gaoled for four weeks in an action against a Shell oil refinery and offshore pipeline, student activist Hicham Yezza is sentenced to nine months imprisonment under an immigration technicality, and more...</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/677-ortega-mosquito-lg.jpg" target="_blank">
											<img src="../images/677-ortega-mosquito-sm.jpg" alt="" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 29th May 2009 | Issue 677</b></p>
<p><b><i>WAKE UP!! IT'S YER ONCE BITTEN, TWICE FORTNIGHTLY...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news677.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news677.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6771.php">Mosquito Bite</a>  |  <a href="../archive/news6772.php">Crap Arrest Of The Week</a>  |  <a href="../archive/news6773.php">No Country For Old Men</a>  |  <a href="../archive/news6774.php">Guerillas In Our Midst   </a>  |  <a href="../archive/news6775.php">Medical Alert</a>  |  <a href="../archive/news6776.php">Eco-village People</a>  |  <a href="../archive/news6777.php">Ronnie Easterbrook Rip</a>  |  <a href="../archive/news6778.php">Withdrawal Method</a>  |  <a href="../archive/news6779.php">Slipping The Net</a>  |  <a href="../archive/news67710.php">Inside Schnews</a>  |  <a href="../archive/news67711.php">Gone Postal</a>  |  <a href="../archive/news67712.php">And Finally</a> 
</p>


<div id="article">

<h3>MOSQUITO BITE</h3>

<p>
<p><strong>AS A SWARM OF MISKITOS TAKES OVER THE COAST OF NICARAGUA</strong> <br />  <br /> On April 19th, representatives from 360 M&iacute;skito communities declared the secession of the entire Caribbean coast of Nicaragua, also known as the Mosquito Coast. They announced that the area, which accounts for 46% of Nicaragua&rsquo;s territory and an estimated 11% of the population, would form the independent Nation of Moskitia. <br />  <br /> Met with barely a mention from the world press, a stony silence from Nicaraguan President Daniel Ortega and scorn from local government, the indigenous Council of Elders annulled all land contracts with the Nicaraguan government pending renegotiation, cancelled government elections, called for businesses to begin paying tax to them instead of the government authorities and gave the police and military six months to organise an orderly and peaceful withdrawal. Not only have the new &lsquo;government&rsquo; already begun work on a currency, a flag and a national anthem they are also backed by the &lsquo;<strong>Indigenous Army of Moskitia</strong>&rsquo;, comprised of up to four hundred conflict veterans. <br />  <br /> One of the first acts of the &lsquo;<strong>Indigenous Army</strong>&rsquo; was the unarmed takeover of the party headquarters of the political party Yatama &ndash; the party which represents the indigenous population in central government and is an ally of the ruling Sandinista party, the FSLN. The party, like the members of the &lsquo;Indigenous Army&rsquo;, has its origins in the region&rsquo;s struggle against the Sandinista government in the 1980&rsquo;s. Following the overthrow of dictator Anastasio Somoza in 1979 the M&iacute;skito people, the region&rsquo;s majority indigenous group, found themselves caught up in the conflict between the new revolutionary Sandinista government and US backed militias, the Contras. Those in the firing line faced forced relocation into government camps, arbitrary imprisonment and even disappearances and torture. In response many took up arms, some later to join with the Contras, others independently, in a rebellion that lasted until 1987. <br />  <br /> The treatment of the Misqito people was always one noticeable stain on the Sandinista&rsquo;s less than impressive human rights record, which as well as accusations of arbitary imprisonment and disapearences also included the curtailing of freedom of speech and assembly and the freedom of the press. Most of the Sandinista abuses of the time could at least be put into the context of a conflict with well-armed, foreign-backed and completely ruthless paramilitary organisations which habitually employed the worst kind of terror tactics. The M&iacute;skito people, however, just lived in a strategically important location.&nbsp; <br />  <br /> The legislation which eventually brought an end to hostilities was the &lsquo;Autonomy of the Caribbean Coast&rsquo; law, passed by the Sandinista government in 1987. The law divided the region into two autonomous regions, one in the North &ndash; RAAN, and one in the South - RAAS which, in theory, had control of natural resources and a degree of self-determination. In practice any semblance of autonomy quickly came to an end following the 1990 electoral defeat of the Sandinistas and the accession of the conservative Violeta Barrios de Chamarro who had little time for autonomy but a keen interest in the region&rsquo;s natural resources. <br />  <br /> Since then the M&iacute;skitos have suffered in poverty. Struggling against an increasingly unpredictable climate and hit hard by natural disasters, the M&iacute;skitos have also witnessed the destruction of around 50% of their rainforest in the last 50 years from logging and the pollution of their water supplies with mercury and cyanide by gold mines. The issue of secession first came to attention in 2002 when the Council of Elders declared independence in theory but took no concrete steps to achieve it in practice. <br />  <br /> In 2006, Daniel Ortega, leader of the Sandinista party that first fought the M&iacute;skitos then granted them autonomy in the 80s, returned to power after 16 years in opposition. Back in the 80s Ortega and the Sandinistas gained world-wide renown following their ousting of the vicious Somoza. While they were demonised by the Americans, with Ronald Reagan labelling Sandinista Nicaragua a &ldquo;totalitarian dungeon&rdquo;, they were idolised by many on the left for being a uniquely multi-class, multi-ethnic, multi-doctrinal, and politically pluralistic movement. <br />  <br /> In its latest incarnation the Sandinistas have proved to be equally divisive, although for very different reasons. Throughout their years in opposition, Ortega was accused of accumulating personal power by strengthening his grip on the party machinery. This led to a rift in the party with dissidents breaking away to form their own Sandinista party, the Sandinista Renovation Movement (MRS). Unperturbed, Ortega began forging alliances with what had previously been regarded as Sandinista enemies.&nbsp; <br />  <br /> The process began in 1999 when Ortega entered into an agreement with Arnoldo Alem&aacute;n, an official during the Somoza regime who had been elected president in 1997. Amongst other things, the agreement was used to attempt to prevent the prosecution of Alem&aacute;n on corruption charges and Ortega on charges that he had sexually abused his stepdaughter, Zoilamerica Narvaez.&nbsp; <br />  <br /> With the 2006 elections approaching this was followed by Ortega converting to Catholicism &ndash; the church had been fierce opponents of the Sandinistas, seeing them as a threat to their traditionally privileged position. He then announced a former contra, Jaime Morales, as his running mate. <br />  <br /> Since regaining power Ortega has been attacked for clamping down on democracy. Things reached a head in the November municipal elections when candidates from MRS and the traditional Conservative party were banned from running altogether. International observers were barred from the elections while the tens of thousands of observers from the independent Nicargauan group, Ethics and Transparency, were reduced to trying to observe from outside the polling stations. Even from that distance they estimated that there were irregularities in a third of polling stations and questioned the results which saw a comprehensive win for the FSLN that flew in the face of independent surveys conducted before the election. The perception that the election was fraudulant led to mass street protests which were violently put down.&nbsp; <br />  <br /> Ortega has also faced criticism from disillusioned social movements, especially for his stance on women's rights. In the 80's the Sandinista commitment to gender equality saw women play a major role in the movement and in its leadership. Following the 1990 electoral defeat these gains were quickly reversed and by the end of 1991 around 16,000 working women had lost their jobs. Since returning to power, instead of  restoring the pre-eminence of women&rsquo;s struggle for equality, Ortega has pandered to his chauvinistic foes by not only supporting a blanket ban on abortion bill also by persecuting the Autonomous Women&rsquo;s Movement (MAM) for opposing it.&nbsp; <br />  <br /> Nevertheless Ortega's return has a seen the reintroduction of a number of progressive policies; literacy rates are approaching 100%, free health clinics are once again staffed and stocked and the small and medium farming sector has been revived.&nbsp; <br />  <br /> The current Ortega government is clearly considerably different in character to the one that battled the M&iacute;skito in the early 80s and to the one that signed a peace deal years later. So far it has yet to actively respond to the declaration, perhaps embarrassed by the fact that last year Ortega was the only world leader to recognize the breakaway Russian-backed republics of South Ossetia and Abkhazia. Or maybe they are biding their time waiting to see if the movement finds wider resonance in the region. <br />  <br /> The proposed Nation of Moskitia would be divided into seven autonomous regions, reflecting the ethnic diversity of the region which is home to Spanish speaking Mestizos (people of mixed Spanish and indigenous heritage), English speaking Creoles, Garifuna (a Caribbean population of mixed indigenous and African slave heritage) as well as a number of other indigenous groups. This diversity is already presenting the separatists with one of their biggest challenges. While the M&iacute;skito are the majority in the Northern region, in the South they form a minority. So far, although Southern M&iacute;skito leaders have declared their support for independence, the  rest of the regions population has remained detached and silent.&nbsp; <br />  <br /> While the national government has yet to comment the regional powers have been considerably more vocal. The Governor of the RAAN, Reynaldo Francis, stated that it is merely a &ldquo;poorly organised&rdquo; effort by a &ldquo;group of old men&rdquo;. This charge was strongly denied by separatist leader Rev. Hector Williams who has been been elected as Wihta Tara, or 'Great Judge' of Moskitia by the M&iacute;skito communities. In response to Francis' comments Williams said, "We are not puppets. We are men. And now we have the weight of a nation on our shoulders." <br />  <br /> Whether it proves successful or not, the independence movement of the Mosquito coast is further proof of the increasing rebelliousness of Latin America's indigenous population after 500 years of abuse and exploitation. With an indigenous president in power in Bolivia, the continuing Zapatista autonomous zones in Mexico and mass movements from the Minga in Colombia (see <a href='../archive/news656.htm'>SchNEWS 656</a>) to the continuing protests in Peruvian Amazonia (see <a href='../archive/news675.htm'>SchNEWS 675</a>), Latin America's indigenous peoples are becoming increasingly vocal in demanding  not just recognition and resources but also to live in a society organised on different principles, their own principles.</p> <br /> 
 <br />      <br />    <br /> 
<p></p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>daniel ortega</span>, <a href="../keywordSearch/?keyword=latin+america&source=677">latin america</a>, <span style='color:#777777; font-size:10px'>moskitia</span>, <span style='color:#777777; font-size:10px'>m�skito</span>, <span style='color:#777777; font-size:10px'>nicaragua</span>, <span style='color:#777777; font-size:10px'>sandinistas</span></div>
<?php echo showComments(334, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>CRAP ARREST OF THE WEEK</h3>

<p>
<p><strong>For sod all...</strong> <br />  <br /> One queer activist in Scotland copped the wrong side of PCs gone mad after being arrested for having &lsquo;homophobic slogans&rsquo; on a placard. She was sat quietly at a table in a cafe when the cops pounced, having just returned from a pro-gay counter demo against hardline god-botherers, the Westboro Baptist Church. Who were, in turn, protesting against the church of Scotland General Assembly. Confused?.. well it certainly seems the forces of law and order were. The placard actually said "<strong>Sinful Sodomite Seeks Similar For Sin + Sodomy. Westboro Baptists Need Not Apply</strong>". Well ...durrh <br /> </p> <br /> 
 <br />    <br />    <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=crap+arrest+of+the+week&source=677">crap arrest of the week</a>, <span style='color:#777777; font-size:10px'>queer</span>, <a href="../keywordSearch/?keyword=scotland&source=677">scotland</a></div>
<?php echo showComments(335, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>NO COUNTRY FOR OLD MEN</h3>

<p>
The thirst for fossil fuels has seen an increase in willingness to search for black gold in the most provincial of places. The latest area of outstanding natural beauty under threat is Bury Hill Wood, part of the Abinger Forest, Surrey.&nbsp; <br />  <br /> A firm called Europa Oil and Gas has applied to drill there for oil, proposing a 120ft high drilling rig in a prominent position, visible from miles around including the North Downs. In case you miss it, it will be lit at night &ndash; as will the whole compound for health and safety reasons &ndash; and it will be capped by a flashing aircraft warning light. There will be four gas and oil flare units, generators and buildings including storerooms and staff accommodation covering most of the two acre site, currently pristine ancient woodland. And if they actually find oil, well...! <br />  <br /> Ahead of the council planning hearing, locals and potential activists are going through the motions of registering objections &ndash; but as their website notes, sometimes these proposals get the nod despite all the objections. Well fancy!  <br />  <br /> See <a href="http://www.thevirtualvillage.com/oilwell.cfm?CFID=2332556&amp;CFTOKEN=36851118" target="_blank">www.thevirtualvillage.com/oilwell.cfm?CFID=2332556&amp;CFTOKEN=36851118</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>europa oil and gas</span>, <a href="../keywordSearch/?keyword=oil&source=677">oil</a>, <span style='color:#777777; font-size:10px'>surrey</span></div>
<?php echo showComments(336, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>GUERILLAS IN OUR MIDST   </h3>

<p>
Dissident factions in Brighton have seized control of a derelict Esso petrol station on Lewes Rd. Raising the black flag they declared it a community garden. The first day of clearing, planting and painting took place on Sunday 10th May. The team cleared broken glass, rubbish and debris, painted concrete tubes (to-be plant pots), created a circular lawn in the centre of the ground and planted 11 giant pots of flowers and plants.  <br />  <br /> Regular days of further planting and maintenance are planned - check out <a href="http://lewesroadcommunitygarden.webs.com" target="_blank">http://lewesroadcommunitygarden.webs.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=brighton&source=677">brighton</a>, <a href="../keywordSearch/?keyword=community+garden&source=677">community garden</a>, <a href="../keywordSearch/?keyword=squatting&source=677">squatting</a></div>
<?php echo showComments(337, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>MEDICAL ALERT</h3>

<p>
A group of European medics are now into the second week of a hunger strike in protest at being denied permission by Egyptian authorities to cross the Rafah border into Gaza in order to provide desperately needed medical support.  <br />  <br /> &ldquo;<em>We are aid workers here for humanitarian work. We are not politicians, even our demonstration here is not a political one, it is a desperate act for people who have come for humanitarian purposes and have been denied entry</em>,&rdquo; said British doctor Omar Al-Mankoosh. Some of the medics have been at the border since early May while others have now been there for more than 50 days. <br />  <br /> Meanwhile the &lsquo;Hope&rsquo; aid convoy, consisting of around 40 trucks of medical supplies finally gained admission to Gaza after initially being refused permission. After being stranded at the border for several days the Egyptian authorities finally relented, albeit only admitting 22 of the 100 strong party to accompany the trucks. <br />  <br /> * <a href="http://www.assedbaig.blogspot.com" target="_blank">http://www.assedbaig.blogspot.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=egypt&source=677">egypt</a>, <a href="../keywordSearch/?keyword=gaza&source=677">gaza</a>, <a href="../keywordSearch/?keyword=hunger+strike&source=677">hunger strike</a>, <span style='color:#777777; font-size:10px'>international aid</span>, <a href="../keywordSearch/?keyword=palestine&source=677">palestine</a></div>
<?php echo showComments(338, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>ECO-VILLAGE PEOPLE</h3>

<p>
Plans are afoot to squat a derelict site somewhere near Hammersmith in London - with a view to establishing an eco-village, somewhat in the vein of the Pure Genius community in 1996 where This Land Is Ours (<a href="http://www.tlio.org.uk" target="_blank">www.tlio.org.uk</a>) established a thriving camp on a large site in Wandsworth for nearly six months (see <a href='../archive/news95.htm'>SchNEWS 95</a>).  <br />  <br /> Anyone looking to join the gang in helping establish the camp, vegetable plots, compost toilets etc. should meet up on June 6th at Waterloo Station (under the clock in the middle of the station) at 10am sharp. The exact location of the site will be revealed on the day. <br />   <br /> Email <a href="mailto:diggers360@yahoo.co.uk">diggers360@yahoo.co.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=eco-village&source=677">eco-village</a>, <span style='color:#777777; font-size:10px'>hammersmith</span>, <a href="../keywordSearch/?keyword=london&source=677">london</a>, <a href="../keywordSearch/?keyword=squatting&source=677">squatting</a>, <span style='color:#777777; font-size:10px'>the land is ours</span>, <span style='color:#777777; font-size:10px'>wandsworth</span></div>
<?php echo showComments(339, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>RONNIE EASTERBROOK RIP</h3>

<p>
Britain&rsquo;s oldest political prisoner, Ronnie Easterbrook, has died at 78 in a hunger strike (See <a href='../archive/news661.htm'>SchNEWS 661</a>). After being fitted up in 1987 in a set-up armed robbery that ended with the death of another supposed robber, he was sentenced to life.  <br />  <br /> Ronnie spent the remainder of his life inside, refusing to apply for parole because he did not recognise the legality of his sentence, and maintaining to the end that unless he got a new trial justice would not be done.  <br />  <br /> He was an agitator in prison who held protests and hunger strikes fighting for justice, supported other prisoners&rsquo; struggles, and was key in the formation of MOJUK, the miscarriages of justice campaign.  <br />  <br /> The funeral will be held 2pm on June 2nd at Eltham Crematorium, Crown Woods Way, London, SE9 2AZ. <br />  <br /> * See <a href="http://www.mojuk.org.uk" target="_blank">www.mojuk.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=hunger+strike&source=677">hunger strike</a>, <span style='color:#777777; font-size:10px'>mojuk</span>, <a href="../keywordSearch/?keyword=political+prisoners&source=677">political prisoners</a>, <span style='color:#777777; font-size:10px'>ronnie easterbrook</span></div>
<?php echo showComments(340, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>WITHDRAWAL METHOD</h3>

<p>
Stop Huntingdon Animal Cruelty (SHAC) have scored a major victory over Huntingdon Life Sciences as major shareholders Barclays have pulled out. Barclays Global Investors sold their 398,187 shares while Barclays Plc sold 26,945.  <br />  <br /> HLS are &pound;72 million in debt, their 1st quarter revenue for 2009 is down 24% from 2008. As vivisector in chief Brian Cass put it &ldquo;<em>revenue levels are down from last year and the outlook remains uncertain</em>&rdquo;.&nbsp; <br />  <br /> This makes Vanguard Group and new shareholder Highbridge Capital Management the top two  shareholders. SHAC said &ldquo;<em>Barclays have seen sense and sold their shares after protests in London, Europe and across the US. Thanks to all campaigners that have protested, emailed and contacted these companies over their investments</em>.&rdquo; <br />  <br /> Despite police repression the AR movement continues at a frenetic pace - check the SHAC website for a torrent of actions/demos <a href="http://www.shac.net&nbsp;" target="_blank">www.shac.net&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=677">animal rights</a>, <a href="../keywordSearch/?keyword=animal+testing&source=677">animal testing</a>, <a href="../keywordSearch/?keyword=barclays&source=677">barclays</a>, <span style='color:#777777; font-size:10px'>highbridge capital management</span>, <a href="../keywordSearch/?keyword=huntingdon+life+sciences&source=677">huntingdon life sciences</a>, <a href="../keywordSearch/?keyword=shac&source=677">shac</a>, <span style='color:#777777; font-size:10px'>vanguard group</span>, <a href="../keywordSearch/?keyword=vivisection&source=677">vivisection</a></div>
<?php echo showComments(341, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>SLIPPING THE NET</h3>

<p>
The National Extremism Tactical Co-ordinating Unit (NETCU) - the anti-protestor political police &ndash; have suffered an embarrassing setback with the acquittal of Vikki Waterhouse-Taylor.  <br />  <br /> Vikki, along with three others, was charged under SOCPA 145 &lsquo;<strong>conspiracy to interfere with the contractual relations of an animal research organisation</strong>&rsquo; after being caught trespassing near to Highgate Rabbit Farm in Lincolnshire. Despite huge police expenditure  &ndash; with one source estimating nearly &pound;1 million - the case against Vikki was thrown out due to lack of evidence. In a comment on Indymedia UK, Vikki said the reason she pleaded not guilty and went through with the trial was &ldquo;<em>mostly because I was/am too stubborn to listen to anyone</em>.&rdquo; That stubbornness paid off. <br />  <br /> During the prosecution it emerged that Highgate Rabbit farm had been induced to stay open by the state. Following an ALF raid in Janiary 2008 the farmer had been about to call it a day. He sold his remaining stock of rabbits to HLS and set about winding the business down. At this point, the farmer said, he was visited by the police &ldquo;and others&rdquo; and was persuaded to keep the business open. <br />  <br /> <strong>* NETCU</strong> was essentially set up to protect corporations from increasingly effective animal rights campaigns such as SHAC. It monitors the policing of animal rights activists and other political movements; follows prosecutions through the courts and cultivates informants. Animal rights campaigners have been targeted with increasingly repressive new legislation in a series of show trials against the SHAC campaign and Stop Sequani Animal Torture (SSAT).&nbsp; <br />  <br /> * <a href="http://netcu.wordpress.com" target="_blank">http://netcu.wordpress.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=677">animal rights</a>, <a href="../keywordSearch/?keyword=direct+action&source=677">direct action</a>, <span style='color:#777777; font-size:10px'>national extremism tactical co-ordinating unit</span>, <a href="../keywordSearch/?keyword=netcu&source=677">netcu</a>, <a href="../keywordSearch/?keyword=shac&source=677">shac</a>, <a href="../keywordSearch/?keyword=socpa&source=677">socpa</a>, <span style='color:#777777; font-size:10px'>stop sequani animal torture</span>, <span style='color:#777777; font-size:10px'>vikki waterhouse-taylor</span>, <a href="../keywordSearch/?keyword=vivisection&source=677">vivisection</a></div>
<?php echo showComments(342, 9); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>INSIDE SCHNEWS</h3>

<p>
Student political prisoner Hicham Yezza has been moved to Canterbury prison. Sentenced in March to nine months for immigration irregularities, Hicham was originally arrested on bogus terrorist charges in June 2008 after he and another student had downloaded Al Qaeda manuals from a public domain US govt website, for research purposes (See <a href='../archive/news668.htm'>SchNEWS 668</a>).  <br />  <br /> After that nonsense fell through, Algerian-born Hicham was re-arrested for immigration problems. Letters to Hicham Yezza XP9266, HMP Canterbury, 46 Longport, Canterbury, Kent, CT1 1PJ <br />  <br /> * See also <a href="http://freehicham.co.uk" target="_blank">http://freehicham.co.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=hicham+yezza&source=677">hicham yezza</a>, <a href="../keywordSearch/?keyword=immigration&source=677">immigration</a></div>
<?php echo showComments(343, 10); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>GONE POSTAL</h3>

<p>
Posties have been up in arms as the BNP have been using the Royal Mail to distribute their racist leaflets in the run-up to the European elections. Too few in number and too scared to be seen in public delivering their brand of hate propaganda with a PR spin, they&rsquo;ve used their privileges as a &lsquo;legitimate&rsquo; political party to spend some &pound;270,000 sending them door to door.&nbsp; <br />  <br /> Although many posties have been told that they must deliver these leaflets or risk disciplinary action, a deal thrashed out in 2007 with the Communication Workers&rsquo; Union does allow posties to refuse on conscience grounds. However, they can only refuse individually, not collectively and it means they face stick from their bosses and lose wages (they&rsquo;re paid 3p for any leaflet that they deliver).&nbsp; <br />  <br /> One refusenik postie even wrote to us in disgust, explaing why he won&rsquo;t be helping spread the bad word and speaking out against the &ldquo;racist (and) barmy&rdquo; BNP, making an erudite case for their racist founding premises.&nbsp; <br />  <br /> As always, casual workers get it hardest; brought in to deliver the BNP leaflets when post people refuse, they don&rsquo;t have any sort of uniform and there have been cases of them being ranted at when people think they are the BNP actually delivering them! <br />  <br /> Let&rsquo;s hope it&rsquo;s not the BNP going first past the post in next week&rsquo;s EU elections, and that they get stamped out soon... <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bnp&source=677">bnp</a>, <a href="../keywordSearch/?keyword=british+national+party&source=677">british national party</a>, <a href="../keywordSearch/?keyword=far-right&source=677">far-right</a>, <a href="../keywordSearch/?keyword=neo-nazis&source=677">neo-nazis</a>, <span style='color:#777777; font-size:10px'>postal workers</span>, <a href="../keywordSearch/?keyword=racism&source=677">racism</a></div>
<?php echo showComments(344, 11); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY</h3>

<p>
The BNP website was brought crashing down last week in what the far-right group themselves dramatically described as the &lsquo;largest cyber attack in recorded history&rsquo;. The denial of service attack began on Saturday night, at one point flooding servers with 28 million hits (allegedly) emanating from Eastern Europe and Russia. It continued for a number of days. BNP Leader Nick Griffin is calling them &lsquo;Marxist cyber criminals&rsquo;. <br />  <br /> Also last Friday Clear Channel, the huge media conglomerate who provides billboards for the BNP (see <a href='../archive/news541.htm'>SchNEWS 541</a>, <a href="http://www.clearchannelsucks.org" target="_blank">www.clearchannelsucks.org</a>), suffered a similar cyber-attack.&nbsp; <br />  <br /> In a urgent communique to &ldquo;fellow patriots&rdquo;, Nick Griffin is foaming at the mouth and knows exactly who to blame, thundering, &ldquo;<em>The rising popularity of the BNP&rsquo;s website ...was obviously simply too much for the alliance of Communists, Tories, Labour, Lib-Dem and UKIP parties to bear, hence the decision to try and close it down by illegal means</em>.&rdquo; Well, that narrows it down Nick &ndash; at least there&rsquo;s a united kingdom opposing you. <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bnp&source=677">bnp</a>, <a href="../keywordSearch/?keyword=british+national+party&source=677">british national party</a>, <span style='color:#777777; font-size:10px'>cyber-attack</span>, <a href="../keywordSearch/?keyword=far-right&source=677">far-right</a>, <a href="../keywordSearch/?keyword=neo-nazis&source=677">neo-nazis</a>, <a href="../keywordSearch/?keyword=nick+griffin&source=677">nick griffin</a>, <a href="../keywordSearch/?keyword=racism&source=677">racism</a></div>
<?php echo showComments(345, 12); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS warns all readers - by all means break away from your government, but do try and replace it with a horizontally ground-up, decentralised anarchist utopia. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

