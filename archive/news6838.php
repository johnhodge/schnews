<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 683 - 10th July 2009 - Climate Campaigns Run Hot</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, camp for climate action, environmentalism, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 683 Articles: </b>
<p><a href="../archive/news6831.php">Bobbies On The Bleat</a></p>

<p><a href="../archive/news6832.php">Hans Off!</a></p>

<p><a href="../archive/news6833.php">Won&#8217;t Take It Zelaya-n Down  </a></p>

<p><a href="../archive/news6834.php">Oh, The Humanity</a></p>

<p><a href="../archive/news6835.php">Inside Schnews</a></p>

<p><a href="../archive/news6836.php">Nice Tosia, To  See You Nice...  </a></p>

<p><a href="../archive/news6837.php">Call Out! Keen To Squat!   </a></p>

<b>
<p><a href="../archive/news6838.php">Climate Campaigns Run Hot</a></p>
</b>

<p><a href="../archive/news6839.php">Digging In  </a></p>

<p><a href="../archive/news68310.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th July 2009 | Issue 683</b></p>

<p><a href="news683.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CLIMATE CAMPAIGNS RUN HOT</h3>

<p>
<strong>With three court cases this week, plans for three Climate Camps, and other international demonstrations later in the year, it&rsquo;s a busy summer for climate campaigners across the country...</strong> <br />  <br /> <strong>THE DRAX 22</strong> <br />  <br /> This week in Leeds Crown Court, guilty verdicts were handed down in a jury trial for the Drax 22 who were arrested in June 2008 for stopping a coal train destined for the Drax Power Station in North Yorkshire, the UK&rsquo;s most polluting power plant (See <a href='../archive/news636.htm'>SchNEWS 636</a>). Dressed in overalls, they used safety signals to have the train stopped, before climbing on board and dumping coal from it onto the tracks. Others were suspended from ropes across the track to prevent it moving. 29 were arrested &ndash;  this trial involved 22.&nbsp; <br />  <br /> The defendants were refused the right to claim in defence that they were acting against the &ldquo;imminent threat&rdquo; of climate change, citing that &ldquo;<em>UN statistics show that the amount of carbon produced by Drax was responsible for 180 deaths a year. Every minute we were on that train, we were stopping carbon emissions</em>.&rdquo; The judge instructed the jury to &lsquo;stick to the facts&rsquo;, refusing to allow this argument as a defence, but assured the 22 that custodial sentences were not on the cards. Along with the inevitable fines and costs, they are all likely to get community service, as meted out recently to the Plane Stupid Stansted Airport activists (see <a href='../archive/news659.htm'>SchNEWS 659</a>). As one of the Plane Stupid lot (a youthful D-lock fanatic) put it: &ldquo;Death by charity shop old ladies&rdquo;.&nbsp; <br />  <br /> * See <a href="http://www.facebook.com/group.php?gid=123128030015" target="_blank">www.facebook.com/group.php?gid=123128030015</a> <br />  <br /> <strong>THE RATCLIFFE 114</strong> <br />  <br /> This week 67 of the Ratcliffe 114 (See <a href='../archive/news672.htm'>SchNEWS 672</a>), had their charges dropped due to &lsquo;insufficient evidence&rsquo;. The 114 were pre-emptively arrested over Easter in a huge media-splashed bust at a school in Nottingham where the group was staying, before any alleged demo could happen. The climate change campaigners found themselves on charges of conspiracy to cause criminal damage to the Eon-owned Ratcliffe-on-Soar coal-fired power station, in a mass-arrest costing the taxpayer &pound;777,000. The remaining 47 will find out what&rsquo;s next over the following weeks as the police continue their futile search for &lsquo;ringleaders&rsquo;.&nbsp; <br />  <br /> * See <a href="http://www.indymedia.org.uk/en/regions/nottinghamshire" target="_blank">www.indymedia.org.uk/en/regions/nottinghamshire</a> <br />  <br /> <strong>THE CLIMATE RUSH er... 4</strong> <br />  <br /> Also up in court this week were four Climate Rush activists. The group who model themselves on the Suffragettes attempted to blockade a coal conference at Chatham House on 1st June by chaining themselves to a sculpture made of bicycle bits. When that failed they got out the superglue. Their case was dismissed in court this week owing to lack of evidence from the prosecution.&nbsp; <br />  <br /> &ldquo;<em>We believe the police acted more forcefully than was absolutely necessary: namely arresting anybody!</em>&rdquo; sniffed a spokeswoman. <br />  <br /> * See <a href="http://www.facebook.com/group.php?gid=37252577758" target="_blank">www.facebook.com/group.php?gid=37252577758</a> <br />  <br /> <strong>CLIMATE CAMPS</strong> <br />  <br /> The Camp For Climate Action is back this summer with three camps across the UK. Firstly, one at The Firth Of Forth, Scotland, August 3rd-10th (See <a href="http://climatecampscotland.org.uk" target="_blank">http://climatecampscotland.org.uk</a>), then in Pembrokeshire, Wales, August 13th-16th, (See <a href="http://climatecampcymru.org" target="_blank">http://climatecampcymru.org</a>) then onto the London area, August 27th-September 2nd, to take on the Met &ldquo;somewhere within the M25 area&rdquo;. The call is out for everyone to be ready and waiting in or around London to swoop on the site on 27th August. Details will posted on the web and texted for those who sign up (See <a href="http://climatecamp.org.uk" target="_blank">http://climatecamp.org.uk</a>). In the lead-up to all this there will be a Climate Camp National Gathering in London, July 18th-19th. For more info call 07534 598 733 and see <a href="http://www.climatecamp.org.uk/node/471" target="_blank">www.climatecamp.org.uk/node/471</a> <br />  <br /> Dedicated campers have already been sharing their skills with a series of workshops at last weekend&rsquo;s Glastonbury festival. The training involved tripod climbing and blockading techniques. Graduates of the Climate Camp School of BLOCK got to try out their new skills by closing down the Greenpeace bar which was appropriately fashioned as an airport terminal with departure lounge. <br />  <br /> Planning meetings are also underway for a mass international action on October 24th-25th, when a range of groups will come together in Britain in an attempt to close down a power station. Climate Camp, Climate Rush and Greenpeace have formed a coalition and are inviting others to join for a  day of global action shutting down climate criminals. For info see the climate camp site, plus <a href="http://www.climaterush.co.uk" target="_blank">www.climaterush.co.uk</a> <br />  <br /> <strong>COPENHAGEN COP OUT</strong> <br />  <br /> The next UN Climate Conference, COP-15 (the biggest yet), will be in Copenhagen on December 7th -18th. While the leaders rearrange the deckchairs on the Titanic one more time there will be an international convergence of protesters there to make an impact. One activist told SchNEWS that plans mooted include &ldquo;<em>keeping the lobbyists out, locking the delegates in until they come up with a respectable solution to climate change &ndash; and - create a space for proper talks to happen between delegates and activists, though probably still with the lobbyists locked in their hotels!</em>&rdquo;  <br />  <br /> See <a href="http://www.climatecamp.org.uk/copenhagen" target="_blank">www.climatecamp.org.uk/copenhagen</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=683">camp for climate action</a>, <a href="../keywordSearch/?keyword=direct+action&source=683">direct action</a>, <a href="../keywordSearch/?keyword=environmentalism&source=683">environmentalism</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(400);
			
				if (SHOWMESSAGES)	{
				
						addMessage(400);
						echo getMessages(400);
						echo showAddMessage(400);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>