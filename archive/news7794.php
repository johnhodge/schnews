<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 779 - 15th July 2011 - Not the Full English</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, unite against fascism, edl" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 779 Articles: </b>
<p><a href="../archive/news7791.php">Rolling Out The Unwelcome Mat</a></p>

<p><a href="../archive/news7792.php">Gaza Flotilla: Seige You Later</a></p>

<p><a href="../archive/news7793.php">Don't Mansion It</a></p>

<b>
<p><a href="../archive/news7794.php">Not The Full English</a></p>
</b>

<p><a href="../archive/news7795.php">Inside Schnews</a></p>

<p><a href="../archive/news7796.php">Like It Or Lumpur It</a></p>

<p><a href="../archive/news7797.php">Rotten Fruit</a></p>

<p><a href="../archive/news7798.php">Lyons Led By Donkeys</a></p>

<p><a href="../archive/news7799.php">C'mon Bunny Light My Fire</a></p>

<p><a href="../archive/news77910.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th July 2011 | Issue 779</b></p>

<p><a href="news779.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOT THE FULL ENGLISH</h3>

<p>
<p>  
  	Last Saturday (9th) saw far-right knuckledraggers, the English Defence League, stage four regional demos in Middlesbrough, Halifax, Cambridge and Plymouth. There had also been one planned for Derby, but the fash bottled it. Generally counter-demos were better attended and more lightly policed than their far-right counterparts, perhaps partly due to the EDL&rsquo;s new policy of not cooperating with police (see <a href='../archive/news776.htm'>SchNEWS 776</a>).  </p>  
   <p>  
  	The Middlesbrough and Halifax ED actions drew around 300-400 each. Cambridge managed a couple of hundred (though this was dwarfed by a counter-demo more than four times the size), but it was Plymouth that won the prize for most farcical protest of the week.  </p>  
   <p>  
  	It didn&rsquo;t look good in the run-up, with EDL infighting meaning other South West divisions were actually telling members not to go. A breakdown in communication meant two different meeting points split the already small turnout, with around 60 waiting at the official start and end pub (Wild Coyote, Exeter St &ndash; in case you were wondering) and others kettled on the Hoe. With help from the police, EDL stewards managed to round up supporters, swelling their numbers to almost 200.  </p>  
   <p>  
  	Meanwhile, up to 500 hundred counter-demonstrators gathered in Jigsaw Gardens (the opposite side of the city). With the Unite Against Fascism event seemingly more focused on Labour politicians and union leaders giving speeches about &ldquo;smashing the EDL&rdquo; and the EDL themselves surrounded by twice the number of police &ndash; the prospect of actually smashing the EDL was never particularly high.  </p>  
   <p>  
  	Portsmouth will see the next regional EDL demo on Saturday (16th). Not having to compete with other divisions, and the sell-out crowd expected for the Chelsea &ndash; Portsmouth friendly the same day, the turnout is expected to be higher. However, with groups of autonomous antifascists and unaligned locals determined to defend their town, it could be an interesting situation.  </p>  
   <p>  
  	Portsmouth - Antiracists will assemble at 12 noon in the Guildhall Square on Saturday 16th July.  </p>  
   <p>  
  	As SchNEWS goes to print the EDL website is still offline having been targeted by hacktivists last week, a fact they seem unwilling to admit, saying only that it &ldquo;is undergoing redevelopment&rdquo;. When it finally returns, we can expect more bragging about how they are going to invade Tower Hamlets on 3rd September. Whether this actually happens remains to be seen &ndash; the EDL cancelled Tower Hamlets last year fearing &ldquo;a suicide mission&rdquo;. If they actually make it this time around, antifascists will be keen to prove them right.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1279), 148); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1279);

				if (SHOWMESSAGES)	{

						addMessage(1279);
						echo getMessages(1279);
						echo showAddMessage(1279);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


