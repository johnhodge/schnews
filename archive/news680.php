<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(47);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	47;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 680 - 19th June 2009 - AS SUSSEX POLICE RAID BRIGHTON FILM MAKER</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, school of school of oriental and african studies, immigration, justice for cleaners, anti-war, direct action, ofog, loyal arrow, sweden, direct action, guerilla garden, brighton, esso, schmovies, sussex police, police harassment, civil liberties, smash edo, immigration, yarl's wood, serco, deportation, human rights, free trade, peru, indigenous people, anti-war, direct action, aldermaston, trident ploughshares, trident missiles" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.london.noborders.org.uk/calais2009" target="_blank"><img
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news679.htm">SchNEWS 679, 12th June 2009</a></b><br />
<b>Cop That!</b> - As SchNEWS takes a wild swing in the direction of the latest SMASH EDO trial... plus, the London Met Police round up children for their DNA, indigenous blockade in Amazonian region turns into a police massacre, another peacful protestors murdered by Israeli forces in occupied Palestine, Brighton campaign against crap coffee chain gets a double shot in the arm, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news678.htm">SchNEWS 678, 5th June 2009</a></b><br />
<b>Convoy Polloi</b> - Twenty four years have passed since the defining moment of the Thatcher government&rsquo;s assault on the traveller movement - the Battle of the Beanfield - SchNEWS revisits it all... plus, the G8 returns to Italy, to the site of the recent earthquake, fourteen road protesters say goodbye to court as their cases are dismissed, a Texas court dishes out draconian punishment to aid charity, a legal victory challenges police methods of storing protest pictures, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news677.htm">SchNEWS 677, 29th May 2009</a></b><br />
<b>Mosquito Bite</b> - The M&iacute;skito people on the coast of Nicaragua have broken away and declared themselves a new nation, in defiance of Daniel Ortega's government... plus, Bury Hill Wood, part of the Abinger Forest, Surrey is under threat from oil exploration by Europa Oil and Gas, plans are afoot to squat land in the Hammersmith area of London and turn it into an eco-village, things are getting harder for vivisection lab Huntingdon Life Sciences, as major shareholder Barclays bank pulls out investment, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news676.htm">SchNEWS 676, 22nd May 2009</a></b><br />
<b>Final Frontiers</b> - No Borders special as SchNEWS reports on the humanitarian crisis in Calais caused by British immigration policy... plus, will the belated withdrawal of British backing affect the Colombian military and their reign of violence, the Sri Lankan government claims victory over the Tamil Tigers but we question at what price, anti-Trident protesters have occupied a new site at the Rolls Royce Rayensway hi-tech manufacturing facility in Derby, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news675.htm">SchNEWS 675, 8th May 2009</a></b><br />
<b>Brighton Rocked</b> - As Smash EDO Mayday mayhem hits the streets of Brighton we report from the mass street party and demonstration against the arms trade, war and capitalism... plus, the bike-powered Coal Caravan concludes its three week tour of the North Of England, a US un-manned Predator drone accidently bombs the village of Bala Baluk, killing up to 200 innocent civilians, RavensAit, the squatted island on the Thames near Kingston, is finally evicted this week, the Nine Ladies protest camp, having won its ten-year battle to stop the quarrying of Stanton Moor in Derbyshire, has finally tatted down and finished, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news674.htm">SchNEWS 674, 1st May 2009</a></b><br />
<b>May the Fourth be With You</b> - With Smash EDO's Mayday Mayhem about to hit Brighton, here's some useful information covering the day.... plus, RBS&nbsp;are seeking &pound;40,000 in compensation from a seventeen year old girl who damaged one computer screen and keyboard at the G20 demo last month, the Metropolitan Police are forced to admit they assaulted and wrongfully arrested protesters during a 2006 demo at the Mexican Embassy in London, the IMF/World Bank meeting in Washington DC is met with three days of protests,&nbsp;one London policeman is sacked for admitting that the police killed someone while another is merely disciplined for saying that he 'can't wait to bash up some long haired hippys', and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news673.htm">SchNEWS 673, 24th April 2009</a></b><br />
<b>Quick Fix</b> - The twelve students arrested this month as alleged 'terrorists' have been released without charge - like many 'anti-terrorist operations' in Britain, this one becomes a joint operation between the police and the media... plus,&nbsp;a protester in the West Bank town of Bil'in is killed by the Israel military while protesting against the 'Apartheid Wall',&nbsp;Birmingham squatters prevent two separate community squats in the city from being evicted on the same day, the bloodshed continues in Sri Lanka,&nbsp;a coalition hasformed called The United Campaign Against Police Violence, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news671.htm">SchNEWS 671, 3rd April 2009</a></b><br />
<b>G20 SUMMIT EYEWITNESS REPORT</b> - As the G20 Summit takes place, we look at what the leaders are discussing, along with an eyewitness account of the G20 protests around the Bank Of England on Wednesday April 1st.... plus, a report from day two of the protests, on the day when the G20 Summit began at the ExCel entre in the East London Docklands, the squatted island at Raven's Ait, on the Thames near Surbiton, is under threat from eviction and calling for help, groups of sacked workers who are feeling the direct consequences of the financial crash are protesting, and re-occupying their work places, peace campaigner Lindis Percy causes traffic chaos outside the USAF airbase at Lakenheath in Suffolk, and more....</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/680-b-b-lg.jpg" target="_blank">
											<img src="../images/680-b-b-sm.jpg" alt="...The next category is 'most police repression on an independent film'" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 19th June 2009 | Issue 680</b></p>
<p><b><i>WAKE UP!! IT&#8217;S YER CINEMA V�RIT� ...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news680.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news680.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6801.php">Cops, Lies And Videotape</a>  |  <a href="../archive/news6802.php">Yarl&#8217;s Wood Hunger Strikers Attacked  </a>  |  <a href="../archive/news6803.php">Clean Sweep</a>  |  <a href="../archive/news6804.php">Ofog Of War</a>  |  <a href="../archive/news6805.php">Darkest Peru</a>  |  <a href="../archive/news6806.php">Aldermast-off</a>  |  <a href="../archive/news6807.php">Guerilla Garden To Stay</a>  |  <a href="../archive/news6808.php">And Finally</a> 
</p>


<div id="article">

<h3>COPS, LIES AND VIDEOTAPE</h3>

<p>
<strong>AS SUSSEX POLICE RAID BRIGHTON FILM MAKER</strong> <br />  <br /> Given the amount of aggro handed out to those brave enough to document Babylon&rsquo;s excesses we wondered how long it was before we started getting our collars felt. Of course they&rsquo;d need a task force to batter down the steel doors of the SchNEWS bunker - let alone take us alive - so Sussex Police took a more indirect route. <br />  <br /> At approximately 8.30am on Tuesday 2nd June, Sussex cops arrived at the home of Paul Light, now revealed as one of the SchMOVIES collective. (For anyone thinking that SchNEWS was still stuck in the Dark Ages with just a scrappy sheet of A4, we&rsquo;ve moved right into the twentieth century and have our own slightly estranged film-making department.) Paul&rsquo;s arrest has wide ranging implications for anyone who reports on controversial issues.&nbsp; <br />  <br /> When the cops came round Paul was getting his ten-year old son ready for school. He was immediately arrested on suspicion of being involved in the &lsquo;decommissioning&rsquo; of the EDO ITT weapons factory in January (see <a href='../archive/news663.htm'>SchNEWS 663</a>). The actual charge was &lsquo;<strong>conspiracy to commit criminal damage</strong>&rsquo;. <br />  <br /> This vague charge was enough for the police to launch into a full-blown fishing expedition.&ldquo;<em>The only evidence they [Sussex police] have is that someone phoned me, asking if I wanted to film the police response</em>,&rdquo; Paul told SchNEWS, &ldquo;<em>I said I couldn&rsquo;t because I had my son and his friend sleeping over that night. That was the end of it as far as I was concerned. As a film maker I frequently get phone calls about possible incidents to film &ndash; this was one shoot I couldn&rsquo;t and didn&rsquo;t make</em>.&rdquo; <br />  <br /> Paul was held and questioned for eight hours and his home was raided. The police took Paul&rsquo;s son to school in a police car, where he burst into tears due to the stress. His father was released on bail and is awaiting the outcome of the arrest. <br />  <br /> Police took every item that could possibly be used for data storage as evidence, including cameras, computers, external hard drives, software, mobile phones, personal accounts, diaries and even his music collection.&ldquo;<em>Put bluntly they have concocted this arrest to see what material I have</em>,&rdquo; said Paul, who regularly films at protests and demonstrations. <br />  <br /> Police have recently attempted to use a film credited to SchMOVIES as evidence in court. The film &lsquo;Batons and Bombs&rsquo; documents events at last year&rsquo;s Carnival Against the Arms Trade (See <a href='../archive/news634.htm'>SchNEWS 634</a>) and was downloaded from the web by cops. They were refused its use as evidence in court on the grounds that the film was edited and they did not possess the original footage. The Crown Prosecution Service are currently appealing that decision via judicial review. The discovery of the original footage would greatly strengthen their hand. <br />  <br /> &ldquo;<em>The raid has effectively shut me down and I am no longer able to make films</em>,&rdquo; said Paul. &ldquo;<em>Film work is my livelihood, so effectively they have put me on the dole</em>.&rdquo; Paul makes films about many different campaigns and issues, and the arrest and raid has been a major setback in his ability to earn a living.&ldquo;<em>They have left me with nothing</em>,&rdquo; he said, &ldquo;<em>I have three commissions in production at the moment and I have had to phone people up and tell them the news &ndash; that their films will have to be postponed indefinitely!</em>&rdquo; <br />  <br /> * See <a href="../schmovies" target="_blank">www.schnews.org.uk/schmovies</a> <br />  <br /> * <strong>SchMOVIES is not new to controversy</strong>. The release of the 2008 film &ldquo;<strong>On The Verge</strong>&rdquo; (<a href="../schmovies/index-on-the-verge.htm" target="_blank">www.schnews.org.uk/schmovies/index-on-the-verge.htm</a>), a documentary about the Smash EDO campaign was met with bans across the UK by various police forces, claiming the film would need certification to be publicly viewed (See <a href='../archive/news626.htm'>SchNEWS 626</a>). <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=civil+liberties&source=680">civil liberties</a>, <span style='color:#777777; font-size:10px'>police harassment</span>, <span style='color:#777777; font-size:10px'>schmovies</span>, <a href="../keywordSearch/?keyword=smash+edo&source=680">smash edo</a>, <a href="../keywordSearch/?keyword=sussex+police&source=680">sussex police</a></div>
<?php echo showComments(365, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>YARL&#8217;S WOOD HUNGER STRIKERS ATTACKED  </h3>

<p>
A mass hunger strike by families detained at Yarl&rsquo;s Wood refugee detention centre in Bedfordshire has been met with violent assaults on men, women and children by security guards working for Serco - who manage the prison on behalf of the UK Border Agency. The detainees started the hunger strike on Monday and since Tuesday prison managers have stopped detainees from speaking to visitors and reporters, and denied them internet access. <br />  <br /> Having collectively prevented the deportation of a family on Tuesday, all the detained families left their rooms to stage a sit-in in the corridors on Wednesday. At about 4pm, between 30 and 40 Serco goons waded in, using excessive force, to remove all men involved in the protest, separating them from the women and children. Two women, who had their clothes ripped off, were also violently removed. In the struggle, one of the women&rsquo;s child, aged 19 months, fell from her back and was stepped on by a guard, and her husband was also injured. A guard was reportedly seen filming one of the semi-naked women. Another detainee who had spoken to the press and put out a statement calling for support for the hunger strike was also violently assaulted. Doctors have not been allowed access to examine the casualties, some of which are reportedly severely injured. <br />  <br /> One of the women detainees, who had been snatched from Newcastle on Friday, reported &ldquo;<em>I have never ever seen such violence. They were beating the men like they were animals. They say if we dare to go back into the corridor they will spray us all over [with pepper spray]. We need your help from outside. We don&rsquo;t have any rights in here. We need your support from outside</em>.&rdquo; <br />  <br /> By the end of Wednesday, at least two men, two women and two children had been removed from the wing and at the time of writing their whereabouts is still unknown.&nbsp; <br />  <br /> Shamelessly, the UK Border Agency have issued a press release saying &ldquo;<em>Officers have separated a small number of detainees from the general population at Yarl&rsquo;s Wood Immigration Removal Centre. This was conducted by staff trained in conflict resolution... This separation was conducted with the utmost sensitivity and there have been no injuries to detainees</em>.&rdquo; These lies were echoed by mainstream media reports. <br />  <br /> Many Yarl&rsquo;s Wood detainees, including pregnant women, have medical conditions but don&rsquo;t receive proper medical attention, whilst many children are ill and suffer anxiety, crying and screaming all night. <br />  <br /> A lively demo with street theatre in solidarity with the hunger strikers and against immigration prisons was held outside outside Government Offices North East in Newcastle on Wednesday with another one planned for Saturday. A solidarity protest called for by No Borders London will be held today (19th) outside Serco&rsquo;s London offices at 22 Hand Court, Holborn, WC1V 6JF. <br />  <br /> * See <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a> <br />  <br /> * Also last Friday (12th), detainees rioted and lit a fire at the new Brook House detention centre at Gatwick Airport (See <a href='../archive/news669.htm'>SchNEWS 669</a>). <br />  <br /> <strong>No Border Camp</strong> <br /> <em>Calais, June 23rd-29th</em> <br />  <br /> This week of protests calls for the freedom of movement for all, an end to borders and to all migration controls - as well as highlighting the realities of the situation in Calais and Northern France for migrants. It will build links between migrants support groups, challenge the authorities on the ground, and protest against the increased repression of migrants and local activists alike.  <br />  <br /> There will be a big demo on Saturday 27th. <br /> &nbsp; <br /> * See <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=deportation&source=680">deportation</a>, <a href="../keywordSearch/?keyword=human+rights&source=680">human rights</a>, <a href="../keywordSearch/?keyword=immigration&source=680">immigration</a>, <span style='color:#777777; font-size:10px'>serco</span>, <a href="../keywordSearch/?keyword=yarl%5C%27s+wood&source=680">yarl's wood</a></div>
<?php echo showComments(366, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>CLEAN SWEEP</h3>

<p>
<strong>Immigration Trap Set for University Cleaners</strong> <br />  <br /> Early last Friday (12th) morning nine sub-contracted cleaners at the University of London&rsquo;s School of School of Oriental and African Studies (SOAS) were summoned to an &lsquo;<strong>emergency staff meeting</strong>&rsquo;. They were met by twenty riot-gear-clad immigration officers who detained the cleaners and took them into detention. The following Monday morning, at an emergency demo, around 40 university activists took matters into their own hands and occupied the university director&rsquo;s office. <br />  <br /> The cleaners meanwhile, who were all from Latin America, were being interrogated by immigration police without legal representation, union support and possibly even interpreters. One suffered a heart attack while in detention and was denied medical assistance and even water. Another was six months pregnant. <br />  <br /> Six of the cleaners have already been deported and one who is still in detention is due to be deported in the coming days. The two that were released have gone into hiding. <br />  <br /> The timing of the raid comes after a recent victory for the &lsquo;<strong>Justice for Cleaners</strong>&rsquo; campaign which ensured the SOAS cleaners would receive the London &lsquo;Living Wage&rsquo; of &pound;7.20 an hour, improved working conditions and the right to union representation. The campaign was established in 2007 by activists working with the University and College Union (UCU), the Student Union and the trade union Unison, when it came to light that a group of SOAS cleaners had not been paid for over three months by the company contracted to clean the university, ISS. The campaign has spread to other campuses including Birkbeck and the London School of Tropical Medicine. <br />  <br /> ISS, however, weren&rsquo;t so keen on having properly paid workers with rights and representation. Shortly after the victory one worker and leading activist in the campaign, Jose Bermudez - known as &lsquo;Stalin&rsquo; (not, we are assured, because of his fondness for gulags and rewriting history) - was sacked.&nbsp; <br />  <br /> Angry not only about the raid, but also the complicity of the SOAS management in permitting the raid to take place and not forewarning the cleaners, the occupiers issued a list of demands. As well as calling for the release of the detainees and the prevention of their deportation, the return of the deportees, as well as the re-instatement of &lsquo;Stalin&rsquo; , they also demanded, amongst other things, that all contract staff be brought in-house; that no further immigration raids take place and that the SOAS management complicit in facilitating the raid be brought to account. <br />  <br /> The university authorities responded with threats and intimidation, preventing anyone from entering the building and threatening the occupiers with the police. They also attempted to serve them with an injunction, although it wouldn&rsquo;t have come into action until the 22nd. The activists countered the aggressive response and maintained pressure on the management with a live blog, garnering support and publicising exactly what they - and the SOAS management - were doing. <br />  <br /> An agreement was finally struck on Wednesday afternoon after what the activists described as &ldquo;intense and complicated&rdquo; negotiations. In the final agreement the SOAS management made a number of concessions: They agreed to petition the Home Secretary for exceptional leave to remain for the cleaner who is still being detained and the return of those deported. They also agreed to hold discussions with ISS, UCU, UNISON and the Students&rsquo; Union to review the immigration raids, for the issue of outsourced cleaning to be raised at the next Governing Body meeting and for an amnesty for all those involved. In addition to this the management agreed to hold talks with other heads of universities about the wider implications of the government&rsquo;s policy on immigration. <br />  <br /> Campaigner Clare Solomon who was involved in the occupation told SchNEWS that aside from providing critical support to the cleaners the outcome was important because it had sparked academic debate on the issue which could be used to pressure the Governing Body. She said that the next meeting would raise crucial questions as to &ldquo;who (the university) is run by and for whose benefit&rdquo;. <br />  <br /> * See <a href="http://freesoascleaners.blogspot.com" target="_blank">http://freesoascleaners.blogspot.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=immigration&source=680">immigration</a>, <span style='color:#777777; font-size:10px'>justice for cleaners</span>, <span style='color:#777777; font-size:10px'>school of school of oriental and african studies</span></div>
<?php echo showComments(367, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>OFOG OF WAR</h3>

<p>
Last Friday Swedish anti-militarist direct action campaigners from Ofog narrowly escaped being bombed, as they spent 36 hours occupying restricted military land near Vidsel, Lapland, being used by NATO for a live aerial bombing exercise called &lsquo;Loyal Arrow&rsquo;. <br />   <br /> Ofog alerted those in charge of the range that peace activists had breached the fence, but the wargames went ahead anyway. Three large explosions went off near the protesters, but fortunately they were able to hike across the harsh terrain and get away from the danger zone. The RFN missile range is the largest in Western Europe. This was one of a series of actions by Ofog against the Loyal Arrow operation.&nbsp; <br />  <br /> * See <a href="http://www.ofog.org&nbsp;" target="_blank">www.ofog.org&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-war&source=680">anti-war</a>, <a href="../keywordSearch/?keyword=direct+action&source=680">direct action</a>, <span style='color:#777777; font-size:10px'>loyal arrow</span>, <span style='color:#777777; font-size:10px'>ofog</span>, <a href="../keywordSearch/?keyword=sweden&source=680">sweden</a></div>
<?php echo showComments(368, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>DARKEST PERU</h3>

<p>
Following the massacre of up to a hundred indigenous protesters by police in Peru earlier this month (See <a href='../archive/news679.htm'>SchNEWS 679</a>) solidarity demonstrations have been taking place across the world with protests outside Peruvian embassies in Bonn, Milan, Madrid, Barcelona, Rome, Paris, Washington DC, Brussels, Quito, Houston and Denver. Last Monday (14th) a lively demo was staged in London with more set to follow. <br />  <br /> Meanwhile in Peru protests have spread throughout the country and a general strike called last Thursday (11th) saw thousands of protesters on the streets, not only in the Amazonian region but also in the capital, Lima, where riot police were called in to break up the demonstration. <br />  <br /> The government of president Alan Garcia is now looking increasingly shaky. The Minister for Women, Carmen Vilodoso resigned last Thursday while Prime Minister, Yehude Simon, has now also announced his intention to step down in protest against the actions of his government. <br />  <br /> In an attempt to calm the situation down the Peruvian congress last week voted to suspend two of the contentious laws which open up the region to exploitation (See <a href='../archive/news675.htm'>SchNEWS 675</a>). The protesters however, are refusing to be pacified, holding out for the full repeal of all 10 of the laws that Garcia passed as part of a Free Trade agreement with the US. <br />  <br /> * See also <a href="http://www.amazonwatch.org" target="_blank">www.amazonwatch.org</a> and <a href="http://www.aidesep.org.pe" target="_blank">www.aidesep.org.pe</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>free trade</span>, <a href="../keywordSearch/?keyword=indigenous+people&source=680">indigenous people</a>, <a href="../keywordSearch/?keyword=peru&source=680">peru</a></div>
<?php echo showComments(369, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>ALDERMAST-OFF</h3>

<p>
Four gates at nuclear weapon facility Aldermaston were blockaded on Monday in protest at the building of new nuclear weapons facilities and the lack of democratic accountability. The surprise blockades, using several cars and including someone in a wheelchair, caused massive road blockages and prevented construction vehicles getting into the site. Although the locks at one gate were broken quickly enabling traffic in, the other gates were blocked for 2-3 hours. The eleven blockaders were arrested, charged and bailed away from the site. <br />  <br /> Angie Zelter of Trident Ploughshares said, &ldquo;<em>When our Government refuses to comply with the fundamental principles of law and undermines the whole Non-Proliferation process then it is up to us, ordinary people, to prevent &lsquo;business as usual&rsquo; at Aldermaston. The blockades today are responsible non-violent attempts to prevent nuclear state terrorism</em>.&rdquo; <br />  <br /> * See <a href="http://www.tridentploughshares.org" target="_blank">www.tridentploughshares.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=aldermaston&source=680">aldermaston</a>, <a href="../keywordSearch/?keyword=anti-war&source=680">anti-war</a>, <a href="../keywordSearch/?keyword=direct+action&source=680">direct action</a>, <a href="../keywordSearch/?keyword=trident+missiles&source=680">trident missiles</a>, <a href="../keywordSearch/?keyword=trident+ploughshares&source=680">trident ploughshares</a></div>
<?php echo showComments(370, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>GUERILLA GARDEN TO STAY</h3>

<p>
In the last month locals in Brighton have been transforming a derelict former ESSO garage into a community garden. The concrete Lewes Rd site, empty for five years, was cleared of rubble before being laid with flower beds and lawn. Benches, flower pots, sculptures and other decorations have made the space an oasis of calm on a busy, heavy-traffic, high street. <br />  <br /> Last week site owners tried to spoil the garden party by chaining the access gate, but undeterred, the gardeners ensured access to the site by removing the gate. The owners have now given consent for the continued use of the garden until &lsquo;development&rsquo; begins. <br />  <br /> Volunteers are still needed to help with the garden and run workshops as well as garden materials.&nbsp; <br />  <br /> * See <a href="http://lewesroadcommunitygarden.webs.com" target="_blank">http://lewesroadcommunitygarden.webs.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=brighton&source=680">brighton</a>, <a href="../keywordSearch/?keyword=direct+action&source=680">direct action</a>, <span style='color:#777777; font-size:10px'>esso</span>, <span style='color:#777777; font-size:10px'>guerilla garden</span></div>
<?php echo showComments(371, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY</h3>

<p>
SchNEWS wishes to issue a humble apology to all the police we&rsquo;ve unfairly maligned over the years. Apparently the boys in blue&rsquo;s over-zealous friskiness with the boots, truncheons and tasers isn&rsquo;t &lsquo;cos they&rsquo;re the front-line of state repression after all. It&rsquo;s because the poor dears are getting their brains fried by microwave energy when they&rsquo;re sat in their paddy-wagons. <br />  <br /> According to an article in Jane&rsquo;s Police Review - the &lsquo;UK&rsquo;s best-selling policing title&rsquo;, &ldquo;&lsquo;<em>Police behaviour at the G20 protests in London could have been caused by the frequency used by the officers&rsquo; Airwave radios interfering with their brainwaves&rsquo;, one expert has said</em>.&rdquo; <br />  <br /> So what turns yer average easy-going liberal bobby from helping old ladies across the road into an armoured truncheon wielding stormtrooper? &ldquo;<em>Officers were waiting in metal vans for hours, and  their Airwave radios effectively turned the vehicles into microwaves. Airwave radios send out microwaves at a greater rate than the brain&rsquo;s natural rhythm, which controls decision making in emergency situations. If you put waves through the brain, you end up with entrainment, which makes you do something you are not programmed to do.</em>&rdquo; <br />  <br /> Next up we expect to see police violence excused on grounds of poor Feng-shui (insert gag here excess Gamma Rays etc) inside New Scotland Yard. Stay tuned! <br />  <br />
</p>

</div>
<?php echo showComments(372, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS warns all film makers - don&#8217;t film anything political - stick to romantic comedies. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

