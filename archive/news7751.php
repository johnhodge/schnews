<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 775 - 17th June 2011 - Flaming June?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, strike, cuts, tory, unions" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 775 Articles: </b>
<b>
<p><a href="../archive/news7751.php">Flaming June?</a></p>
</b>

<p><a href="../archive/news7752.php">Greece: No Cash In The Attica</a></p>

<p><a href="../archive/news7753.php">Mexico In A Narcho-chy: Eyewitness Report</a></p>

<p><a href="../archive/news7754.php">Into Valley Of Darkness</a></p>

<p><a href="../archive/news7755.php">Soas I Was Saying</a></p>

<p><a href="../archive/news7756.php">Keepin It Montreal</a></p>

<p><a href="../archive/news7757.php">Thin End Of The Wedges</a></p>

<p><a href="../archive/news7758.php">Schnews In Brief</a></p>

<p><a href="../archive/news7759.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th June 2011 | Issue 775</b></p>

<p><a href="news775.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FLAMING JUNE?</h3>

<p>
<p>  
  	<strong>AS LARGEST MASS UNION WALKOUT SINCE THE 1980S SET FOR JUNE 30TH</strong>  </p>  
   <p>  
  	Is the big fight on? Union responses to the Tory cuts have so far been fairly muted - the M26 outing (see <a href='../archive/news765.htm'>SchNEWS 765</a>), resembling a cross between a family picnic and a Labour Party rally. However on June 30th the largest public sector strikes since the 80s are planned. With the Daily Mail claiming that &lsquo;Union Barons&rsquo; plan to &lsquo;unleash hell&rsquo;, what&rsquo;s actually going to happen?  </p>  
   <p>  
  	Well who&rsquo;s on board? On J30, up to 750,000 public sector workers, including many members of the UCU, NUT, PCS and ATL unions, including teachers, health workers, air traffic controllers, job centre wallahs, police support staff etc, etc. will go on strike.&nbsp; It remains to be seen whether the strike will unfurl as another generalised &lsquo;day of action&rsquo;, but marches are planned across the country and activist groups are planning to support picket lines and hold solidarity actions.  </p>  
   <p>  
  	Ostensibly about public sector pension cuts (unions legally have to ballot to strike over a specific issue), the strikes are the first wave of mass industrial action against the proposed austerity measures. To keep hammering home the point - these austerity measures are only necessary because of the ruling classes determination to ensure that the pain caused by the banking crisis is borne by anyone except the people who caused it.  </p>  
   <p>  
  	More strikes are planned for the autumn. The biggest unions, Unite and Unison, have not balloted this time for reasons best known to their compromising selves, but could be on the bandwagon come September. The RMT are leading the way with four days of industrial action planned in June.  </p>  
   <p>  
  	This has immediately led to Tory calls for harsher anti-strike legislation, despite the UK already having the most draconian anti-union laws in Europe. Notwithstanding the already&nbsp; restrictive regime, the government has now said that any trades union action that it deems &lsquo;disruptive&rsquo; might cause a further clamp-down.&nbsp; This could include a minimum turn-out requirement. Currently, strike ballots are successful if the majority of voters are in favour, whatever the turn out. Also on the cards is a &lsquo;minimum services agreement&rsquo;, which would mean unions would have to ensure services continue throughout strikes.  </p>  
   <p>  
  	With the ConDems not budging, will the union movement finally see the need to move beyond legalism and put up a real fight? Here&rsquo;s hoping.  </p>  
   <p>  
  	* See Generalise the Strike on <a href="http://www.facebook.com/event.php?eid=100787720014939" target="_blank">www.facebook.com/event.php?eid=100787720014939</a> <br />  
  	plus <a href="http://www.anticutsnetwork.blogspot.com" target="_blank">www.anticutsnetwork.blogspot.com</a> and <a href="http://www.ukuncut.org.uk" target="_blank">www.ukuncut.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1247), 144); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1247);

				if (SHOWMESSAGES)	{

						addMessage(1247);
						echo getMessages(1247);
						echo showAddMessage(1247);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


