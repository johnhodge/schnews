<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 736 - 27th August 2010 - Royal BS</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate camp, climate change, environment, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 736 Articles: </b>
<b>
<p><a href="../archive/news7361.php">Royal Bs</a></p>
</b>

<p><a href="../archive/news7362.php">Hovefields Eviction Threat</a></p>

<p><a href="../archive/news7363.php">The Fash Show</a></p>

<p><a href="../archive/news7364.php">War Is Over (killing Continues)</a></p>

<p><a href="../archive/news7365.php">Grow Heathrow Eviction Alert</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/736-climate-camp-derek-lg.jpg" target="_blank">
													<img src="../images/736-climate-camp-derek-sm.jpg" alt="Climate Camp Scotland, August 2010" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 27th August 2010 | Issue 736</b></p>

<p><a href="news736.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROYAL BS</h3>

<p>
<p>
	Camp for Climate Action 2010 finished this week having shut down operations at the RBS Global Headquarters, disrupting works at their administration building and closing numerous branches around Edinburgh&#39;s city centre. Activists also targeted Cairn Energy and Forth Energy, companies that had received huge wads of cash from the bank for not-so-environmentally-friendly projects. </p>
<p>
	Climate Campers outfoxed police by swooping fifteen hours earlier than announced and taking a site rather closer to their target than expected at around 9pm on Wednesday 18th August. The first campers arrived at the RBS Global Headquarters at Gogarburn in Edinburgh and erected tents right in their back garden, giving a clear &quot;fingers up&quot; signal to the bankers and cops. </p>
<p>
	RBS, dubbed the &quot;Oil Bank of Scotland&quot;, was chosen as a target for its involvement in various climate crimes, including financing tar sands extraction from an area of pristine wilderness in Canada, and being the biggest lender out of all the banks to the fossil fuels industry. Banners set up to be clearly visible to the 3,300 workers employed at the site included &quot;Nature Doesn&#39;t Do Bail-Outs&quot;. Campers repeatedly targeted the Gogarburn building (set in acres of woodland and meadows with its own moat, tennis courts and &#39;education centre&#39;) throughout the week, leading up to the day of mass action on Monday (23rd), when RBS told workers to stay at home and left the building empty. </p>
<p>
	Friday saw campers warming up with an &quot;RBS Rousing&quot;. A mobile rave danced around the building, with police apparently seeing no reason to stop them until the ravers entered the building and one woman superglued herself to the reception desk. The woman has changed her name to &quot;Dongaria Kondh&quot; after an indigenous community whose land has been occupied by Vedanta. Vedanta is mining bauxite from an Indian mountain area rich with wildlife, including an elephant corridor, has an atrocious human rights record, and they have been doing it all with money loaned from RBS. The Treasury had to defend its part in the mining project in the High Court last October, when campaign groups challenged why they had allowed RBS to invest taxpayers&#39; money in this destruction. Dongaria sent a report to Andrew Cage, the Sustainability Officer for RBS and says she will not change her name back until Vedanta pulls out of the project and gives the tribe their land back. Dongaria was arrested and charged with Breach of the Peace. </p>
<p>
	Other mischief continued throughout the week. On Saturday Greenwash Guerillas in biohazard suits marched around the Royal Mile with feather dusters detecting and denouncing RBS greenwash. They did the same at the Gogarburn site, an odd choice of action considering no workers were in the building nor were there any press on site. Police watched with mild amusement and after about 15 minutes the guerillas returned to camp. No trouble was had from the police in town either, even when three women twice invaded RBS sponsored stages at the Edinburgh Fringe Festival and sang Lady Gaga rewrites about tar sands. </p>
<p>
	On Sunday afternoon, 100 people in biohazard suits and masks stormed the small foot-bridge separating the camp from the RBS offices with a sound system, pushed back police lines and gave them a run around the building, smashing windows but getting stopped short of entering the building. There were some successful de-arrests, but police caught two people and charged them with Breach of the Peace. The window-smashers are as yet unidentified. Two people were injured in the resulting turmoil, but both were back onsite the following day. A camp-wide discussion was prompted by the day&#39;s events. The workshop by representatives from the Indigenous People&#39;s Network campaigning against the tar sands oil extraction was interrupted by the action that resulted in the damage. They addressed the camp in the evening, voicing concerns that the ties of solidarity between the two campaign groups may now suffer due to a clash of tactics, as the Indigenous People&#39;s Network did not advocate damage to private property. Camp-wide consensus was that as the Camp for Climate Action is made up of autonomous individuals, a diverse range of tactics are employed to attain a common goal, and property damage was an action that most people in the camp saw little problem with. News also spread around camp in the evening that RBS workers wouldn&#39;t be coming in the next day, meaning actions had to be re-planned. </p>
<p>
	During the night, RBS branch doors were superglued together, and RBS slogans were graffitied around Edinburgh city centre. </p>
<p>
	Mass-Action Monday got off to an early start with campers waking up to triple the number of police around the site, access roads in and out of the Gogarburn site blocked by police and the building looking decidedly empty. Affinity groups quickly re-organised and a plethora of small actions started to take place all over the city. </p>
<p>
	Seven people made themselves into a human-superglue-arm lock-chain in the RBS executive car park and a banner was dropped outside the headquarters reading &quot;RBS: Using Our Money to Fuck the Planet&quot;. No mean feat considering the vast numbers of police that had been set to work guarding the bridge over the road outside the entrance. </p>
<p>
	Another affinity group got in on the action straight away by targeting Forth Energy, a company that is pushing biomass solutions that don&#39;t meet Climate Camp&#39;s stringent eco-principles. Two got on top of the building, dropping banners saying &#39;BIO MASS HEALTH HAZARD&#39; and &#39;BIO MASS = CLIMATE CHANGE&#39;, two chained themselves to the front of the building and three activists occupied the inside. </p>
<p>
	Cairn Energy was the next to come under fire as campaigners smashed a giant piggy bank of molasses in front of their offices, in protest of the &pound;117 million in RBS loans the company has used to start drilling for oil off the coast of Greenland &ndash; only now possible as the ice caps are melting. </p>
<p>
	In Edinburgh, most RBS branches had already been shut down by the police before any campers had even stepped foot in the town centre, but protests continued with groups demoing outside shut branches and talking to the public about why they were there. Several performances of &#39;Oily Gaga&#39; spontaneously sprang up at different locations. </p>
<p>
	Other activists managed to shut down one of the few branches still open by supergluing themselves across the front entrance and handing out leaflets while another two scaled some scaffolding in the city centre and did a banner drop. </p>
<p>
	Four campaigners acting in direct solidarity with the tar sands campaign staged a sit-in inside a branch and then outside the bank while covered in molasses, shutting it down for an hour. The group then recreated a toxic tailings pond, complete with dead ducks, on the Royal Mile, drawing a large crowd of Edinburgh fringe-goers to listen to a representative from the Indigenous People&#39;s Network&#39;s explanation of why RBS&#39;s investments are so devastating. </p>
<p>
	Meanwhile, back on site, a &quot;Molassapult&quot; was thrown at RBS from across the stream, leaving large dirty oily patches clearly visible and a Rhino Siege Tower kept police occupied for several hours as it slowly descended the hill to the main gate. Police, presumably frenziedly thumbing through medieval law books for advice on how to deal with such an incident, ran themselves in circles, eventually deciding to erect a metal fence at the main gate which flummoxed the Rhino&#39;s attack. This incident ended with a standoff between police and Rhino, which continued &lsquo;til the end of camp. </p>
<p>
	By Monday evening 12 arrests had been made, taking the grand total up to 18. Nearly all the arrests made were as result of superglue blockades and most were charged with the Scottish police&#39;s failing-all-else charge: Breach of the Peace. </p>
<p>
	Allegations have been made that Climate Campers were involved with one more event that occurred on the Monday. According to police, unknown persons poured an &#39;oil like substance that resembled vegetable oil&#39; over the busy dual carriageway outside the headquarters, causing delays and traffic jams while a clean-up operation ensued. SchNEWS spoke to one South Coaster about the accusation; &ldquo;<em>I don&#39;t believe anyone at the camp would have done this. Nobody there would do something that would so recklessly endanger people&#39;s lives, that totally goes against the ethos of the camp. And anyway, what&#39;s the point of this action? When Climate Camp does &#39;oil&#39; actions we use something visual like treacle or molasses. It sounds more like an attempt to damage the reputation of Climate Camp to me</em>.&rdquo; </p>
<p>
	Did the campers succeed in their stated attempt to &quot;shut down RBS Global Headquarters&quot; then? Well it appears so. Workers were told to stay at home on Monday after the window-smashing and rebel-rousing incident allegedly sparked concern for office workers&#39; safety and the offices were shut down for the day. However, the building was shut down, but were the operations of the business? Employees were on their laptops in their houses working from home, and although the disruption caused to RBS is obviously something that Climate Camp can count as victory, whether this has any real impact on the way the 84% publicly-owned bank spends taxpayers&#39; money is less certain. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(914), 105); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(914);

				if (SHOWMESSAGES)	{

						addMessage(914);
						echo getMessages(914);
						echo showAddMessage(914);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


