<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 744 - 22nd October 2010 - Crude but Refined</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, climate camp, plane stupid, direct action, environment" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.anarchistbookfair.org.uk" target="_blank"><img
						src="../images_main/an-bkfr-2010-banner.jpg"
						alt="Anarchist Bookfair 2010, London, October 23rd"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 744 Articles: </b>
<b>
<p><a href="../archive/news7441.php">Crude But Refined</a></p>
</b>

<p><a href="../archive/news7442.php">Huntington Eviction Alert</a></p>

<p><a href="../archive/news7443.php">The Big Libcon Cuts: To The Manor Osbourne</a></p>

<p><a href="../archive/news7444.php">France: Will You Still Need Me, Feed Me, When I'm 62?</a></p>

<p><a href="../archive/news7445.php">Chev It Up Ya Prs</a></p>

<p><a href="../archive/news7446.php">Who Ya Gonna Coal?</a></p>

<p><a href="../archive/news7447.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd October 2010 | Issue 744</b></p>

<p><a href="news744.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CRUDE BUT REFINED</h3>

<p>
<p>
	<strong>AS CLIMATE ACTIVISTS HIT ESSEX REFINERY WITH PRECISION ACTION</strong> </p>
<p>
	Utmost secrecy, activist goodie bags and sheer determination shut down the the UK&rsquo;s busiest oil refinery last week in one of the most well-planned actions the climate justice movement has seen so far. </p>
<p>
	Stilt walkers, a samba band and around 500 activists blockaded the road to Corydon oil refinery in Essex on Saturday (16th), stopping an estimated 400,000 gallons of oil getting to London&rsquo;s petrol stations. Organised by a coalition of action groups including Camp for Climate Action, Plane Stupid and UK Tar Sands Network, the 9-hour blockade was part of a global week of action against the fossil fuel industry that saw protests in 22 other countries. </p>
<p>
	With very few details released before the day, three blocs of protesters gathered at Euston, Victoria and Waterloo stations that morning with a clear mission - to disrupt the flow of oil - but no knowledge of who the target was, where or how. The crowds gathered with an obligatory but small police presence. At 10am the signal was given for each group to head down to the tube, where they navigated their way through London, following flags and listening to whispered directions. </p>
<p>
	The Dirty Money Bloc ended up at Fenchurch station first, and boarded a train, when it became clear for the first time that the action wasn&rsquo;t in the city. They were soon joined by the Building Bloc. Twenty minutes into their suburban journey, pieces of paper were handed down the carriages of squashed-in protesters. The target and plan was finally revealed: to blockade Corydon oil refinery in Stanford-le-Hope, Essex. Corydon is the UK&rsquo;s busiest refinery and largest supplier to the major oil companies&rsquo; petrol stations in London. </p>
<p>
	Next to appear were huge laundry bags of &lsquo;protest packs&rsquo;, which were handed out to all the demonstrators on the train (leaving the two cops who came along for the ride looking a bit left out, bless&rsquo;em). Inside was an impressive direct action toolkit: white boiler suit, mask, map to the location, rope, carabiners and a tube to lock-on at the blockade. </p>
<p>
	<strong>SUITED AND BOOTED</strong> </p>
<p>
	Suited, masked and ready to go, around 300 activists arrived at the very normal town of Stanford-le-Hope, 30 miles east of London. With sound systems blaring and flags waving, the blocs made their way through the streets. Given the strange looks given by passers-by, you can bet Stanford-le-Hope doesn&rsquo;t get this sort of visit very often. They weren&rsquo;t the only ones surprised by the action - with the Met busy &lsquo;protecting&rsquo; the oil financiers&rsquo; HQs in London from a never-to-appear anarchist mob, the protest was met with only a very small and very local police presence. </p>
<p>
	The blocs split to take different routes to the refinery, one road and one cross-country. Each heard the announcement: an all-women affinity group had already started the blockade. They were locked-on underneath two vans they had driven onto the road. Now they just needed the protest to catch up with them. </p>
<p>
	Unfortunately the third bloc had been waylayed at Fenchurch, where police had blocked platform exits and ordered the 200-strong group off the train to search them under a Section 1 order. The party-pooping tactics didn&rsquo;t work for long - the group caught up with the protest later in the day, swelling the already strong numbers. </p>
<p>
	Missioning across the fields to the target, the Dirty Money Bloc managed to break through no less than five police lines, linking arms and pushing through each time the coppers tried to get in the way. FIT were kept on their toes as people used umbrellas and banners to block the camera. At the sight of the gaggle of protesters rounding the corner to the refinery, around 15 empty tankers parked in a adjacent car park roared into life and shot down an unblocked back road, obviously terrified of getting caught in the middle of it all. </p>
<p>
	As people saw the tankers making their getaway, a group huddle formed, questioning whether to go and join the affinity group already at the blockade or to set up a second blockade at the back exit. In one of the quickest consensus decisions ever made, people decided to join the affinity group. The Building Bloc and the newly arriving Body Bloc were to set up the second blockade. </p>
<p>
	Just as the first blockade came into view, two meat wagons screeched up from behind with sirens blaring, attempting to stop the group from reaching their mates by getting in front of them. Protesters fanned out, blocking the road and preventing the cops from getting ahead - a move that topped the list of the day&rsquo;s victories by protesters vs. police. At around 1.30pm, the Dirty Money Bloc got to the blockade with much cheering and celebration. They immediately locked-on in lines, supporting the women who had been holding strong since 11am. </p>
<p>
	Over at the second blockade, the Building Bloc had reached their destination, complete with bamboo tripods. The Met made their presence known by trying to rush the group as they set up the tripods. The lines held strong, however. In total six tripods were put up between lines of locked-on demonstrators straddling both sides of a dual carriageway. The shut down of the refinery was complete. Hard work done, the Body bloc arrived with more fun in the form of sound systems, a samba band and stilt walkers, handy for stringing up banners. </p>
<p>
	<strong>CORY-DONE FOR THE DAY</strong> </p>
<p>
	Both blockades got visitors from locals during the day, mainly groups of curious kids on their bikes. Several mini direct action workshops took place, with the kids being taught how to lock on, given boiler suits and taught all the chants. One group particularly got into the spirit, shouting &lsquo;fight the power&rsquo; back at the lines of cops and joining in with chalking anti-oil messages over the road. The press were kept away however, as police refused to let anyone with a camera or notebook through to the protest. </p>
<p>
	At about 5pm, the first blockade made the decision to pack up and go down the road to join the party. After perhaps watching one too many dance routines by the stilt walkers, a meeting was soon called and it was decided to end the action by &lsquo;tactful withdrawal&rsquo;, point made, and all without a single arrest. Just as swiftly as it had struck, the 500-strong crew dismantled itself and headed for the station and the after-party. </p>
<p>
	As one protester put it: &ldquo;<em>Today&rsquo;s action showed what can be done by a group of people determined to show the oil industry that what they are doing needs to be stopped, and how well it can be achieved with collective action and co-operation</em>.&rdquo; </p>
<p>
	The -A- team: We love it when a plan comes together. </p>
<p>
	*See <a href="http://www.crudeawakening.org.uk" target="_blank">www.crudeawakening.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(980), 113); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(980);

				if (SHOWMESSAGES)	{

						addMessage(980);
						echo getMessages(980);
						echo showAddMessage(980);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


