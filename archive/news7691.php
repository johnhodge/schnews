<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 769 - 29th April 2011 - Stoking the Fires</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tesco, bristol, squatting, riots" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 769 Articles: </b>
<b>
<p><a href="../archive/news7691.php">Stoking The Fires</a></p>
</b>

<p><a href="../archive/news7692.php">Tes-cop-oly</a></p>

<p><a href="../archive/news7693.php">Hove For The Best</a></p>

<p><a href="../archive/news7694.php">Sticking To The Task</a></p>

<p><a href="../archive/news7695.php">Georgian Facade</a></p>

<p><a href="../archive/news7696.php">More Benefit Cuts</a></p>

<p><a href="../archive/news7697.php">Climate Camp: Lewes</a></p>

<p><a href="../archive/news7698.php">Ahava Got News For You</a></p>

<p><a href="../archive/news7699.php">Manning The Barricade</a></p>

<p><a href="../archive/news76910.php">I Can't Believe It's Not Mayday</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th April 2011 | Issue 769</b></p>

<p><a href="news769.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STOKING THE FIRES</h3>

<p>
<p>  
  	<strong>AS INNER BRISTOL RIOTS AFTER SQUAT RAID AND ANTI-TESCO PROTESTS</strong>  </p>  
   <p>  
  	Last Thursday (21st), the Stokes Croft area of Bristol was the scene of violent clashes between police and protesters of&nbsp; he kind not seen in the city since 1980.  </p>  
   <p>  
  	The hours of rioting that lasted from 9pm until five or six the following morning were prompted firstly by the eviction of the Telepathic Heights squat, and then tapped into deep anger in the community against the opening of another unwanted Tesco store, which was conveniently trashed and looted in the process. &nbsp;  </p>  
   <p>  
  	The Tesco Express, which campaigners say 96% of the local community is opposed to, was built on the site of a former comedy club, Jesters. When the gags had dried up and the club closed, squatters moved in to protest the Council&rsquo;s granting of planning permission for the character- and profit-sucking leech of the multi-national grosser.  </p>  
   <p>  
  	Since being evicted last March during a large police operation (see <a href='../archive/news714.htm'>SchNEWS 714</a>), cross-community campaigning has continued the fight to protect the independent businesses of Stokes Croft and its unique character. Tesco opened on April 15th to immediate daily pickets and few customers.  </p>  
   <p>  
  	<strong>REACHING THE HEIGHTS</strong>  </p>  
   <p>  
  	As is the way of the government, as one hand gives power to the corporations, the other tries to take autonomy away from the people. The government&rsquo;s recent clampdown on squatting and attempts to criminalise all dissident behaviour makes the radical hub of Stokes Croft an obvious target. The area has a squatted art gallery, info space, workshops, a social centre and an on/off event space. Telepathic Heights was a long-established squat opposite the Tesco.  </p>  
   <p>  
  	The storming of the squat by 160 cops in riot gear on Thursday night was, supposedly, prompted by suspicion that petrol bombs were being made inside. Police claimed, in their usual priority-muddling way, that the squat posed &ldquo;a real threat to the local community&rdquo;, which the squatters deny. The council had not applied for an eviction order for the residents, most of whom were in the process of leaving voluntarily.  </p>  
   <p>  
  	By 9pm, along with the squat defenders, a crowd had gathered from the local Stokes Croft area. There was a mixture of people, drawn from the varied users of the busy street at that time on a Friday. The cops tried to herd people onto the pavement.  </p>  
   <p>  
  	SchNEWS spoke to someone who witnessed the events: &ldquo;<em>At some point there was some kerfuffle at the corner of the road, and the police took the horses there. They were trying to form a blockade but people weren&rsquo;t complying. This one guy was standing his ground&nbsp; but was pushed up against a police van by one of the horses &ndash; it caused a bit of commotion. When the police reinforcements arrived he was grabbed and arrested</em>.&rdquo;  </p>  
   <p>  
  	 <br />  
  	By 9.45 increasing amounts of people were turning up, including a large number of passers-by, and the crowd had swelled to above a hundred. The mood at this point was still good; people were milling around, some chatting to the cops &ndash; including the Welsh forces who had already hopped the border at the prospect of some action. A sound system on a trolley arriving from down Ashley Road prompted a cheer from the crowd.  </p>  
   <p>  
  	Some protesters then began to make barricades in the road with communal bins and palettes, and a polyprop web. Behind the road block, people danced and there was the occasional shout of a slogan.  </p>  
   <p>  
  	When police began blocking off Cheltenham Road, which leads into the city, people began to throw bottles towards the lines.  </p>  
   <p>  
  	The police then made their initial charge at the barricades. As the police line pushed down Ashley Road, the truncheons came out and battering began. &ldquo;<em>Some people witnessed blatantly outrageous stuff: a man whose legs were damaged as he was hit by a police van, a woman who got dragged through broken glass, indiscriminate batoning of bystanders</em>.&rdquo;  </p>  
   <p>  
  	It wasn&rsquo;t until around 11pm that the situation could really be called a riot, when hundreds more people had joined the crowds.  </p>  
   <p>  
  	&ldquo;<em>They made a complete screw-up by pushing people down Ashley Road. The fact they pushed the crowd into a notoriously anti-police residential area meant it became a logistical nightmare for them</em>.&rdquo;  </p>  
   <p>  
  	<strong>WHACKY BACCY</strong>  </p>  
   <p>  
  	Down Ashley Road, people could escape down side streets and the crowds dispersed. The police had exited the area round Telepathic and Tesco, leaving a police van (intentionally?) to be smashed in and the siren and lights to be used as a mobile disco. The shop also had an impromptu late night opening and people helped themselves to 100% discounted tobacco. &nbsp;  </p>  
   <p>  
  	Despite the numbers of fresh people arriving later on, there still appeared to be an clear understanding throughout the crowd that the riot was about the squat eviction and/or Tesco. &ldquo;<em>They are very much joined in peoples minds</em>,&rdquo; says our Bristolian, &ldquo;<em>The feeling in the air is impossible to put into words. People stood in their front yards beaming, others watched from caf&eacute;s just looking bemused</em>.&rdquo; &nbsp;  </p>  
   <p>  
  	The police finally peed off at about 5 or 6 in the morning, and the riot calmed down. St Paul&rsquo;s residents started to come out with brooms and sweep up broken glass as a neighbourhood. <br />  
  	And the mood since? &ldquo;<em>There isn&rsquo;t a sense of anything won as such &ndash; we didn&rsquo;t start or sustain it. It was off the back of the community but at least there&rsquo;s reassurance that there&rsquo;s awareness of the Tesco issue and anti-establishment sentiment</em>.&rdquo;  </p>  
   <p>  
  	Nine people were arrested for offences ranging from public affray to violent disorder, with at least two being held on remand. At the time of writing, a planned film screening in Mina Park of footage of the riot has been stopped by police, who swarmed in and confiscated the project, bizarrely justifying their action citing anti-rave legislation.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1194), 138); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1194);

				if (SHOWMESSAGES)	{

						addMessage(1194);
						echo getMessages(1194);
						echo showAddMessage(1194);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


