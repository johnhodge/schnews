<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - Raving Madness</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, free party, festivals, autonomous spaces" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>
</b>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<p><a href="../archive/news7275.php">Turkey Shoot</a></p>

<p><a href="../archive/news7276.php">The Itt Crowd</a></p>

<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>

<p><a href="../archive/news7278.php">Hoto Finish</a></p>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/727-avatar-lg.jpg" target="_blank">
													<img src="../images/727-avatar-sm.jpg" alt="Avan It" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RAVING MADNESS</h3>

<p>
<p>
	<strong>AS UK TEKNIVAL &lsquo;RAVE SIX&rsquo; FACE COURT ACTION AFTER BUST...</strong> </p>
<p>
	Around 2,500 partygoers descended on Dale Aerodrome in Wales last May bank holiday for the 2010 UK Teknival, only to be met with a massive police response. Police broke up the party on the first day, arresting 17 people in the process. Four remain on police bail and six have been charged. </p>
<p>
	Automatic number plate recognition, a police photographer, hand-held camcorders, helicopters and even a plane were used by police in a sophisticated surveillance operation which resulted in hundreds of thousands of pounds&rsquo; worth of equipment and vehicles being seized (not to mention a similar amount spent on the police operation no doubt). </p>
<p>
	The annual UK Teknival has emerged out of a long tradition of free festivals, its roots stretching back to the Avon Free Festival, one of a circuit of free festivals which emerged as part of the alternative and traveller scene in the 1970s. These gatherings were largely tolerated before the Criminal Justice Act, passed in 1994 (see SchNEWS 1-50), rendered them illegal. </p>
<p>
	Avon Free Festival took place each year on May bank holiday weekend, and culminated in the infamous 1992 Castlemorton party. Every year on the anniversary of Castlemorton, a teknival or large free party is held somewhere in the UK. The most notable of these was the 2002 Steart Beach party in Somerset, held on the tenth anniversary of Castlemorton, which coincided with the Golden Jubilee weekend and attracted forty soundsystems and over ten thousand people (see <a href='../archive/news363.htm'>SchNEWS 363</a>). Teknivals are now a global phenomenon, with an international circuit you can follow all summer, in the same way people used to be able to follow the free festival circuit around the UK. The French government actually permits two teknivals a year to take place unhindered. </p>
<p>
	This year as hundreds of vehicles congregated near the small village of Dale on the coast of southwest Wales, four policemen attempted to block the road leading to the disused aerodrome site, causing a massive tailback which brought traffic to a standstill for three hours. One witness reports they were stuck at least five miles behind the front of the jam. Eventually, after someone brought out a 12 volt rig and people started dancing in the road, the policemen moved aside and actually directed everyone onto to the site, negotiating with a landowner to get a gate opened. </p>
<p>
	As a result of the blockade, soundsystems didn&rsquo;t begin setting up until the early hours of Sunday morning. By about midday the next day, police, the local council and the BBC were all on the scene. Fairly positively-slanted BBC interviews with partygoers were broadcast nationally and posted online, although the second has since been removed from the BBC website. </p>
<p>
	Mid-afternoon Sunday a helicopter flew overhead, broadcasting something that might have been the words of Section 63 of the Criminal Justice and Public Order Act 1994 over a loudspeaker. The message was inaudible due to loud music being played on the ground; even those straining their ears to hear only caught snatches of it, and witness accounts vary. It was apparently a warning to leave within between one, four, or twenty-four hours. </p>
<p>
	<img style="border:2px solid black;width: 300px; height: 206px; margin-right: 10px"  align="left"  alt="Heddlu Police"  src="http://www.schnews.org.uk/images/727-teknival-sm.jpg"  /> Whichever it was, at this stage the majority of soundsystems started packing their rigs into their vehicles as ordered by the police. It became clear then that the three day mega-rave everyone was expecting had been thwarted. The atmosphere of unease and fear generated by the authorities caused a mass exodus of ravers who would otherwise have stayed to help to clean up the site after the party. </p>
<p>
	Most people left the site in a hurry, although some efforts were made to clear rubbish. As each soundsystem drove off site their driver was stopped and arrested, their equipment was seized and their vehicles were impounded. Only the luckiest got away. Confiscated items include work tools, vinyl collections, several vehicles without sound equipment in them, a hire van, and hired and borrowed music equipment. Police deliberately kept the hire van for two weeks, making the total cost &pound;950. </p>
<p>
	Along with one other soundsystem that left early on Monday morning, a well-known deep house music soundsystem stayed behind and continued playing music and partying until mid-afternoon on Monday, when more than twenty police, including the Chief of Dyfed-Powys Constabulary, came over and physically handed out a Section 63 notice, telling people to leave within one hour. They explained that they had drunk too much to drive and asked if they could stay until the next morning. The officers agreed that they could stay on site and drive home in the morning on condition that they packed their equipment into the van immediately. </p>
<p>
	Whilst negotiations were taking place, a disabled traveller started to play punk music on his car stereo, which police then confiscated from his live-in vehicle. &ldquo;<em>He wasn&rsquo;t even playing repetitive beats</em>,&rdquo; recalls one partygoer, &ldquo;<em>he was a disabled man playing music in his own home and the police seemed to illegally enter his home and steal his stereo</em>.&rdquo; </p>
<p>
	Police then left the site, but an hour later, a low-loader recovery vehicle arrived to tow the van containing the soundsystem, followed by four riot vans and about fifteen police cars. There were less than fifty people left on site at this point. A woman whose partner was detained overnight was forced to sleep outside the police station as she awaited his release because their van had been impounded leaving her nowhere to sleep and no way of returning home. Despite this, the police refused to let her stay inside. </p>
<p>
	Four people were released on police bail pending further investigation and the &lsquo;Rave Six&rsquo;, as the mainstream media has dubbed them, have been charged under Section 136 of the Licensing Act for carrying out unlicensed licensable activity. The six have now been released on unconditional bail and are due to return to Haverfordwest Magistrates Court on 24th June. </p>
<p>
	Four of the six arrested were merely friends from the last soundsystem to leave the party and had nothing to do with the overall organisation of the event. (It&rsquo;s highly probable that the other two didn&rsquo;t either). Offenders under Section 136 are liable for up to six months in jail and/or a fine of up to &pound;20,000. A Facebook group called &lsquo;Drop the Charges Over UKTek&rsquo; has been created and has so far attracted nearly 3000 members. There is video evidence being uploaded all the time, including a clip of police leading the convoy to the party site, which would suggest that they actually allowed the party to take place. </p>
<p>
	* If you can help the &lsquo;Rave Six&rsquo; in the form of legal or financial support or if you witnessed events at the UK Tek then please join the Facebook group &lsquo;Drop the Charges Over UKTek&rsquo; and contact the administrators, or if you&rsquo;re not on Facebook, please get in touch via by emailing <a href="mailto:rave_six@yahoo.com">rave_six@yahoo.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(817), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(817);

				if (SHOWMESSAGES)	{

						addMessage(817);
						echo getMessages(817);
						echo showAddMessage(817);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


