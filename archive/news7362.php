<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 736 - 27th August 2010 - Hovefields Eviction Threat</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dale farm, hovefields, gypsy, travellers" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 736 Articles: </b>
<p><a href="../archive/news7361.php">Royal Bs</a></p>

<b>
<p><a href="../archive/news7362.php">Hovefields Eviction Threat</a></p>
</b>

<p><a href="../archive/news7363.php">The Fash Show</a></p>

<p><a href="../archive/news7364.php">War Is Over (killing Continues)</a></p>

<p><a href="../archive/news7365.php">Grow Heathrow Eviction Alert</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 27th August 2010 | Issue 736</b></p>

<p><a href="news736.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HOVEFIELDS EVICTION THREAT</h3>

<p>
<p>
	Gypsy/travellers in Essex are in serious trouble after receiving a 28-day notice of eviction to vacate their site at Hovefields at the beginning of August. This site is near Dale Farm - the largest Gypsy site in the country &ndash; which is also under imminent threat (see <a href='../archive/news702.htm'>SchNEWS 702</a>). Twenty families face a violent eviction by bailiffs Constant &amp; Co if they don&#39;t leave by next Tuesday (31st). This group includes two children with learning disabilities, plus several elderly residents, and no alternative site is being offered by Basildon Council. It is a breach of the Human Rights Act. </p>
<p>
	This comes after six families were evicted from Hovefields on June 29th, when Constant &amp; Co bailiffs arrived &ndash; with Essex Police &ndash; without forewarning in the early hours of the morning and gave them one hour to go. A bulldozer was used to flatten pitches and knock down a building being used as a toilet. Six pitches were empty due to the residents being on the road at the time &ndash; they are not allowed to return due to an injunction. No compensation or alternative site was on offer. </p>
<p>
	Yesterday there was a training day at Dale Farm to help prepare people to defend the eviction, and will surely need reinforcements as Constant &amp; Co, the self proclaimed &#39;gypsy eviction specialists&#39; &ndash; and currently under investigation for breaches of safety law - have a long history of wanton violence (see <a href='../archive/news439.htm'>SchNEWS 439</a>), as they play their part in the ethnic cleansing of traditional travelling peoples. </p>
<p>
	Dale Farm &ndash; which houses around 1000 people &ndash; is on land owned by the travellers, but only part of it has planning permission &ndash; and the Basildon Council is choosing to evict rather than settle the whole matter by allowing permission for the rest. It is also under direct threat, and some consider Hovefields to be a warm-up for a massive eviction of Dale Farm. </p>
<p>
	Tory cutbacks have seen the cancellation of funding for new caravan parks for Gypsies and Travellers, but in reality none of this is about saving money &ndash; in fact quite the opposite. No expense is being spared to ethnically cleanse those living a travellers&#39; life. So far Basildon Council have spent &pound;2.5 million on legal fees, and will spend &pound;3.5 million trying to evict these sites in Essex. Their offer to provide housing for the Essex families would cost &pound;35-40 million, and it costs tens of millions to keep evicting travellers around the country. The cheaper option of spending &pound;4 million to provide sites for travellers doesn&#39;t come into it. </p>
<p>
	<strong>* Hovefield location</strong>: off A127, take Gardiners Lane North to Oak Road, Wickford CM11 2YH. </p>
<p>
	<strong>* To help protect Gypsies</strong> and their way of life, email <a href="mailto:dale.farm@btinternet.com">dale.farm@btinternet.com</a> or the Essex Human Rights Clinic <a href="mailto:losori@essex.ac.uk">losori@essex.ac.uk</a> See also <a href="http://www.gypsy-traveller.org" target="_blank">www.gypsy-traveller.org</a> </p>
<p>
	<strong>* Roma and Gypsy travelling communities</strong> are constantly suffering from brutal evictions across Europe, and at the moment in France there is a campaign to deport 850 Roma people to Romania by the end of August. In the past fortnight there&#39;s been several large expulsions with hundreds who the French Government say have outstayed the three-month limit (even though these are EU citizens) being flown to Romania. The French Government are giving a paltry compensation of Eur300 per adult, Eur100 per child for the loss of vehicles and property. See <a href="http://www.errc.org" target="_blank">www.errc.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(915), 105); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(915);

				if (SHOWMESSAGES)	{

						addMessage(915);
						echo getMessages(915);
						echo showAddMessage(915);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


