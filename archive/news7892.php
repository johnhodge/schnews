<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 789 - 23rd September 2011 - Egypt: Sheikhing the System</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, egypt, mubarak" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 789 Articles: </b>
<p><a href="../archive/news7891.php">Flying The Flag</a></p>

<b>
<p><a href="../archive/news7892.php">Egypt: Sheikhing The System</a></p>
</b>

<p><a href="../archive/news7893.php">Too Late To Dale Out</a></p>

<p><a href="../archive/news7894.php">Glue-me Retail Outlook</a></p>

<p><a href="../archive/news7895.php">Dogs Of War</a></p>

<p><a href="../archive/news7896.php">Sparks Fly</a></p>

<p><a href="../archive/news7897.php">Rip: Troy Anthony Davis</a></p>

<p><a href="../archive/news7898.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd September 2011 | Issue 789</b></p>

<p><a href="news789.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EGYPT: SHEIKHING THE SYSTEM</h3>

<p>
<p>  
  	AS PROTESTERS PUSH BACK AGAINST THE SCAF  </p>  
   <p>  
  	Egyptian workers are staging the biggest wave of strikes in years, challenging a military regime that&rsquo;s finding it hard to retire gracefully.&#8239; Since the beginning of September,&#8239; more than half a million have defied an anti-strike law passed in April by the Supreme Council of the Armed Forces (SCAF).&#8239; Postal workers were among the first to strike this month, soon joined by weavers, teachers, and nurses; then train drivers, doctors, oil workers, civil servants and even some of the Sheikhs affliated with Egypt&rsquo;s historic Al-Azhar, centre of Sunni Islam, who perform the call to prayer at mosques. Demanding a decent minimum wage &ndash; promised but not delivered by SCAF &ndash; the strikes are being staged by many of the 120 or so independent unions that have sprung up since January&rsquo;s unfinished revolution.  </p>  
   <p>  
  	In getting better at organising themselves, Egyptian workers are also challenging the corrupt economy that&rsquo;s developed since the 1970s. Under Presidents Sadat and then Mubarak, the military dished out land, contracts, commercial licences and privatised businesses to their mates, while the police kept workers quiet with threats of violence. Companies, and the workers&rsquo; official unions, were filled with regime-allied placemen, on swish salaries.  </p>  
   <p>  
  	Getting rid of those corrupt officials - the &ldquo;mini-Mubaraks&rdquo; in each workplace - is now one of the key aims of the striking workers. Teachers have been striking across Egypt since Saturday [17th], demanding the Education Minister &ndash; regime crony Ahmed Gamal Eddin Moussa &ndash; be sacked.&#8239; Police tried and failed that day to break up strikes at a number of schools.  </p>  
   <p>  
  	Since February, over 7,000 civilians have been summarily convicted in military trials, which the regime justifies by the question-begging presumption that they&rsquo;re dealing only with &ldquo;thugs&rdquo;.&#8239; Meanwhile, they&rsquo;re getting paranoid about independent media, clamping down on Al Jazeera and other satellite stations.&#8239;  </p>  
   <p>  
  	It remains illegal to criticize the military, which owns a third of Egypt&rsquo;s economy, with its budget a state secret.&#8239; In February, the military promised to revoke the Emergency Law, which for thirty years has let the police bang up and torture whom they please.. &#8239;Now, they&rsquo;re using the recent outbreak of exuberance at the Israeli embassy [see <a href='../archive/news788.htm'>SchNEWS 788</a>] &ndash; which it&rsquo;s widely speculated the SCAF deliberately let happen - as a pretext to make the law more severe.  </p>  
   <p>  
  	Meanwhile, many in the revolutionary groups see a looming &ldquo;new establishment&rdquo; born of a tacit alliance between the military and the Muslim Brotherhood&rsquo;s new Freedom and Justice Party, which many expect to do well in the upcoming elections. &#8239;The Brothers opposed the regime mainly where it&rsquo;s suited them strategically &ndash; e.g., over the timing of the elections &ndash; while refusing to take part in Tahrir protests.  </p>  
   <p>  
  	But it&rsquo;s not all going SCAF&rsquo;s way. To coincide with a big protest in Cairo&rsquo;s Tahrir Square on the 9th, they bussed in thousands of workers to Cairo stadium for a diversionary &ldquo;workers&rsquo; day&rdquo; celebration &ndash; &lsquo;Exactly as Mubarak used to do&rsquo;, as one activist observed. &#8239;But the gambit backfired, as hundreds of farmers gathered outside the stadium to demand that corrupt officials be sacked and state-run agricultural co-ops, dissolved.  </p>  
   <p>  
  	The current strikes have seen workers abandon the official trade union which the regime used to prevent strikes, and are building on the unprecedented strike wave of 2007. &ldquo;Before [the revolution], the only events where we had tens of thousands of people protesting were workers&rsquo; protests&rdquo;, says Nadeem Mansur, director of the Egyptian Centre for Economic and Social Rights, which has been helping facilitate rank-and-file union organizing. &#8239;&lsquo;Tahrir took inspiration from the workers&hellip; And [now] the other way around.&rdquo;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1365), 158); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1365);

				if (SHOWMESSAGES)	{

						addMessage(1365);
						echo getMessages(1365);
						echo showAddMessage(1365);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


