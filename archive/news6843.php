<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 684 - 17th July 2009 - Call Off The Hounds!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, fox hunting, blood sports, crawley & horsham, hunt monitors, hunt sabs, protection from harassment act, countryside alliance" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 684 Articles: </b>
<p><a href="../archive/news6841.php">Good Plan, 'stan</a></p>

<p><a href="../archive/news6842.php">Crap Search Of The Week</a></p>

<b>
<p><a href="../archive/news6843.php">Call Off The Hounds!</a></p>
</b>

<p><a href="../archive/news6844.php">Dow And Nowt</a></p>

<p><a href="../archive/news6845.php">Va-va-boom!</a></p>

<p><a href="../archive/news6846.php">Coup Stark</a></p>

<p><a href="../archive/news6847.php">Calais Bruise Crews</a></p>

<p><a href="../archive/news6848.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th July 2009 | Issue 684</b></p>

<p><a href="news684.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CALL OFF THE HOUNDS!</h3>

<p>
The Crawley and Horsham (C&amp;H) foxhunt&rsquo;s attempt to crush hunt monitors under the weight of an injunction has collapsed ignominiously! As we reported back in <a href='../archive/news637.htm'>SchNEWS 637</a>, the C&amp;H, backed by the Countryside Alliance, had engaged the services of Timothy Lawson-Cruttenden - pioneer of liberty-busting lawsuits under the Protection from Harassment Act (See <a href='../archive/news581.htm'>SchNEWS 581</a>) to prevent monitoring of their activities. On Tuesday (14th), hunt monitors received notice that the case was being dropped. <br />  <br /> The injunction would&rsquo;ve seen huge areas of Sussex closed off to monitors &ndash; who could have been arrested regardless of what the hunt was doing. Since the ban came into effect in 2005, hunts have tried every trick in the book to ensure that they are still able to set dogs on wildlife. With senior cops making it clear that they weren&rsquo;t interested in enforcing a ban, it&rsquo;s been left up to hunt monitors to procure the evidence to ensure that the blood-junkies end up in the dock. Monitors and sabs are the last obstacle to the resumption of hunting. Having solicited donations from across the world to fight for blood-sports, it&rsquo;s clear that the Countryside Alliance saw this as a flagship case and were hoping to roll out similar injunctions across the country.&nbsp; <br />  <br /> Chichester based monitors Simon and Jane Wilde have long been at the forefront of getting evidence of hunt cruelty. This year, using an undercover infiltrator, they were able to get hold of conclusive evidence that the plaintiffs - who said that they were being harassed by the Wilde&rsquo;s activities - were engaged in illegal hunting. The principle plaintiff, Simon Greenwood, was filmed using his hounds to chase a fox to ground and then call in terrier-men to dig it out and throw it to the hounds.&nbsp; <br />  <br /> Jane&rsquo;s solicitor Victoria von Strandmann told SchNEWS that these proceedings were an oppressive attempt by a powerful group seeking to use the courts to stop individuals concerned about the welfare of animals, in order to flout a law that they have unstintingly opposed. In the process, they were seeking to stifle legitimate opposition and rights to free speech, and prevent individuals from going about their everyday lives. <br />  <br /> Simon Wilde told us: &ldquo;<em>The Crawley and Horsham hunt has been forced to withdraw from an action that the Countryside Alliance had originally engineered as a master plan designed to defeat the Hunting Act by preventing monitoring. If they had achieved this, it would have spread nationwide and been game, set and match for hunting. Now we have turned the tables, and the hunt will have to write out cheques for hundreds of thousands of pounds for legal fees, having achieved nothing</em>.&rdquo; <br />  <br /> Twos-up on yer new Land Rover then Simon! <br /> &nbsp; <br /> * To contact West Sussex Wildlife Protection  call 07990522712&nbsp; <br />  <br /> * See also <a href="http://www.hsa.enviroweb.org" target="_blank">www.hsa.enviroweb.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>blood sports</span>, <span style='color:#777777; font-size:10px'>countryside alliance</span>, <span style='color:#777777; font-size:10px'>crawley & horsham</span>, <a href="../keywordSearch/?keyword=fox+hunting&source=684">fox hunting</a>, <a href="../keywordSearch/?keyword=hunt+monitors&source=684">hunt monitors</a>, <span style='color:#777777; font-size:10px'>hunt sabs</span>, <span style='color:#777777; font-size:10px'>protection from harassment act</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(405);
			
				if (SHOWMESSAGES)	{
				
						addMessage(405);
						echo getMessages(405);
						echo showAddMessage(405);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>