<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 765 - 1st April 2011 - M26: Ritz Spirit</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, unions, riots, direct action, black bloc, anarchists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 765 Articles: </b>
<b>
<p><a href="../archive/news7651.php">M26: Ritz Spirit</a></p>
</b>

<p><a href="../archive/news7652.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7653.php">Shimmon Then If You Think You're Hard Enough</a></p>

<p><a href="../archive/news7654.php">Carlo Giuliani Verdict</a></p>

<p><a href="../archive/news7655.php">Loose About Aughouse</a></p>

<p><a href="../archive/news7656.php">Bhopal Selecta</a></p>

<p><a href="../archive/news7657.php">Tomlinson Inquest</a></p>

<p><a href="../archive/news7658.php">Lucky Lukashenko</a></p>

<p><a href="../archive/news7659.php">Nuking On Heavens Door</a></p>

<p><a href="../archive/news76510.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st April 2011 | Issue 765</b></p>

<p><a href="news765.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>M26: RITZ SPIRIT</h3>

<p>
<p>  
  	<strong>SCHNEWS LOOKS FOR A REAL ALTERNATIVE AT LONDON MARCH</strong>  </p>  
   <p>  
  	<strong>&ldquo;<em>HUNDREDS of anti-royal anarchists ran riot across central London yesterday, turning a peaceful demonstration about spending cuts into a class war</em>&rdquo; - Sunday Express</strong>  </p>  
   <p>  
  	Well - somebody had to do it. The TUC&rsquo;s glad-hand fest was never going to change the ConDEMS course by even one degree. It was hijacked by anarchists and it deserved to be. The TUC mobilised its masses and did everything they could to make sure that genuine dissent didn&rsquo;t rear its ugly head. The emphasis was to be on a family-friendly day out without frightening the horses. Quite who they think is going to hand out prizes for niceness in the shark tank that is global capitalism remains a mystery.  </p>  
   <p>  
  	The TUC&rsquo;s magnolia-tinted plan culminated in the endpoint rally in Hyde Park, where even moderate voices like Bob Crow of the RMT and Caroline Lucas MP of the Green Party were not allowed to speak. They were left out in favour of Ed Millaround the gormless puppet who&rsquo;s now trying to pass off the Labour Party as a revitalised radical alternative to the Tories - less than eleven months after they were run out of office. Is this then the TUCs strategy? Wait four years, and then vote in a business friendly Labour government - and they wonder why union membership is at an all time low.  </p>  
   <p>  
  	While dressing himself up as the re-incarnation of Emily Pankhurst and Martin Luther King bearing Mandela&rsquo;s torch, Milliband junior was emphatic about Saturday&rsquo;s disorder, condemning it roundly while ignoring the fact that all the struggles he was paying homage to involved breaking more than a few windows.  </p>  
   <p>  
  	<strong>SPLINTERS OF DISCONTENT</strong>  </p>  
   <p>  
  	Earlier that morning, the feeder marches, all condemned as unauthorised by the TUC, made their way largely unhindered to greet the masses gathered at Embankment. A half-hearted attempt by cops to put a line across Westminster bridge in front of the Radical Workers block that had left from Kennington Park was shrugged aside.  </p>  
   <p>  
  	The anarchist block split from the main march past Embankment Station, as a voice over a megaphone beckoned marchers to come with them for a sight-seeing tour around London. The break off bloc poured past Trafalgar Square, past the front of the TUC demo where hundreds of disenchanted activists decided to sack off the A to B marching exercise and join the black bloc. By the time the bloc had made its way through Covent Garden to Oxford Street it numbered several thousand.  </p>  
   <p>  
  	Trudging along with the masses, other anarchists filtered through the crowd to UK Uncut&rsquo;s pre-arranged meet up on Oxford St at 2pm. UK Uncut had called for occupations of corporate tax dodgers. The police tactic for the day was clearly the protection of known UK Uncut targets, with lines of police in front of branches of Boots, Vodafone and Topshop. The cops even seemed willing to protect Topshop at the expense of more traditional anti-capitalist targets leaving the way open for unfortified McDonalds and Starbucks to get done over anyway, showing that this was far from a single issue demo.  </p>  
   <p>  
  	And that was where the fun really started - Topshop, one of UK Uncut&rsquo;s main targets, was the first to get it and then, for over hour and a half a rag-tag black block, numbering around a thousand, ran cops ragged around Soho and Piccadilly Circus. Selected corporate targets were attacked with smoke flares, paint and street furniture. Banks bore the brunt of the anger with branches of HSBC, Santander and Lloyds getting the paint and broken glass treatment. The HSBC in Cambridge Circus got given some special attention. As &lsquo;Smash the banks&rsquo; was daubed across the window, the crowd began to move on, until, bearing in mind the complete absence of Old Bill, people decided to take the advice. Windows were smashed and a green wheelie bin was used to batter open the doors. The inside of the bank was turned over before activists did a runner as the plod turned. The F.I.T team were chased off in Cambridge Circus.  </p>  
   <p>  
  	&nbsp;Even London&rsquo;s Boris bikes scheme wasn&rsquo;t ignored, with activists peeling off the Barclay&rsquo;s stickers while leaving the machines intact. It might have been a touch violent but it wasn&rsquo;t mindless.  </p>  
   <p>  
  	<strong>BRITISH SUMMERS TIME</strong>  </p>  
   <p>  
  	At the Ann Summers shop, militant queer action emerged from the crowd, spray-painting Ann Summers shiny shop front and apparently taking a stand against &ldquo;mainstream heteronormatively constructed sexuality&rdquo;, and finally putting a bin right through the window. - for some top post-situationist drivel - check out this communique <a href="http://indymedia.org.uk/en/2011/03/476879.html" target="_blank">http://indymedia.org.uk/en/2011/03/476879.html</a>  </p>  
   <p>  
  	(SchNEWS note - you&rsquo;re not actually allowed to issue a communique unless you&rsquo;ve got an armed wing or have at least kidnapped the finance minister - otherwise it&rsquo;s just a press release)  </p>  
   <p>  
  	Police coming into the area were harassed, with street signs and wheelie bins hurled under vehicles. Riot vans were liberally re-decorated. Some incredibly loud bangers or thunderflashes caused cops to flinch. Plumes of coloured smoke could be seen. Often groups of cops were rushing to protect targets that had been left by the fluid and swift-moving crowd. Uncontained, the block, waving red and black flags, moved along Piccadilly towards the Ritz.  </p>  
   <p>  
  	The chant went up - &ldquo;The Ritz, the Ritz - we&rsquo;re gonna get rid of the Ritz&rdquo; as tables smashed through the windows.  </p>  
   <p>  
  	The block then re-united with the main march, giving many a chance to de-mask and slip away.  </p>  
   <p>  
  	Nearing Hyde Park, no doubt fearing an outburst of sentimental centre-left rhetoric, they split off back into Mayfair. Symbols of wealth, including a Porsche dealership and several overpriced sports car were done, along with a couple of shops selling overwhelmingly priced status antiques. Every corner turned in Mayfair showed just how much grotesque wealth is still out there waiting to be redistributed.  </p>  
   <p>  
  	<strong>UNCUT UNLEASHED</strong>  </p>  
   <p>  
  	Meanwhile at 3.30pm on Oxford Circus , UK Uncut&rsquo;s second pre-arranged meet up was underway. At the convergence demonstrators were slipped a card that told them which colour flag to follow, with the aim of separating into branches then joining forces for the surprise occupation.  </p>  
   <p>  
  	The target &ndash; Fortnum &amp; Mason, was well chosen, grocers to the Queen and corporate tax dodgers. Unfortunately the sheer size of the protests meant the different crowds didn&rsquo;t arrive simultaneously and there was a stand-off with protestors inside the building and police lines forming outside. A push and shove ensued but due to the pavement barriers not enough of the crowd could get behind the push and the cops remained unbudged, blockading the entrances.  </p>  
   <p>  
  	The two to three hundred protesters who were already inside were left to soak up the atmosphere, and eye up bargains like the twenty quid jam and &pound;65 napkin rings. Others took a more direct route and climbed up a street lamp onto the first floor. This was were the bulk of the day&rsquo;s arrests happened - 148 in total, even after cops promised to let them go. <a href="http://www.youtube.com/watch?v=Tfk0O1SeH6A" target="_blank">http://www.youtube.com/watch?v=Tfk0O1SeH6A</a>  </p>  
   <p>  
  	Unfortunately here, as during the rest of the day, there seemed to be more people filming than actually getting involved in any action. Anyone who wanted to get stuck in had to push their way through a line of happy snappers first. Where does all this footage end up? - Think before you get yer zoom lens out kids, &lsquo;cos careless shots cost lives.  </p>  
   <p>  
  	The rest of the day was a mix of stand-offs and running skirmishes. The Peace Horse, a fabulous construction of hessian and bamboo was torched outside Oxford St tube. By this time the block was being pushed back onto Piccadilly circus as, freed of the obligation to police the TUC demo, that was safely in Hyde Park, more cops began arriving. Sealing off Fortnum &amp; Mason they made numerous arrests.  </p>  
   <p>  
  	A number of the crowd gathered in Trafalgar Sq, where some had suggested a 24 hr occupation. Described to SchNEWS by one eyewitness as &ldquo;just a party, really&rdquo;, the cops seized their opportunity to wade in. A kettle was put in place and despite a fight being put up the crowd was dispersed or arrested by 1am.  </p>  
   <p>  
  	According to Green&amp;Black Cross legal support there were over 200 arrests - and that&rsquo;s just those who&rsquo;ve contacted the help-line. Most had their clothes and phones seized. Interestingly, they are receiving bail conditions to stay away from central London on the day of the next TUC mobilisation 1st of May and on the 29th April, the date of Kate&amp;Wills happy nuptials.  </p>  
   <p>  
  	If you were arrested then there&rsquo;s a defendants&rsquo; meeting - Saturday 2nd April at 2pm - University of London Student Union, Malet Street, WC1E 7HY  </p>  
   <p>  
  	Otherwise - see you on the streets! <br />  
  	 <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1162), 134); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1162);

				if (SHOWMESSAGES)	{

						addMessage(1162);
						echo getMessages(1162);
						echo showAddMessage(1162);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


