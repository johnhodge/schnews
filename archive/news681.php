<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(48);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	48;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 681 - 26th June 2009 - 100,000 march in London to remind the world that the fighting in Sri Lanka may be over, but the suffering of the Tamil people continues...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tamil, tamil tigers, sri lanka, liberation tigers of tamil eelam, cricket, no borders, immigration, refugees, asylum seekers, calais, france, nick griffin, bnp, neo-nazis, fascism, blackpool, lancashire, red white and blue festival, lewisham, squatting, schools, privatisation, solitaire, direct action, ireland, rossport, county mayo, shell, broadhaven bay, gardai, tarnac 9, france, eco-community, direct action, julien coupat, israel, palestine, boycott israeli goods, tesco, london, shoreditch, south lanarkshire, coal, climate change, direct action, protest camp, scotland, camp for climate action, mainshill woo, deer, deer culling, animal rights, stonehenge, summer solstice, fitwatch, police, wiltshire, english heritage" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.london.noborders.org.uk/calais2009" target="_blank"><img
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news680.htm">SchNEWS 680, 19th June 2009</a></b><br />
<b>Cops, Lies and Videotape</b> - As SchMOVIES film-maker is raided by Sussex Police... plus, Serco security guards serve up violent assault for detention centre hunger strikers, immigration trap is set for university cleaners, Swedish anti-war actvists occupy land being bombed in training exercise, anti-free trade protests continue and threaten the Peruvian government, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news679.htm">SchNEWS 679, 12th June 2009</a></b><br />
<b>Cop That!</b> - As SchNEWS takes a wild swing in the direction of the latest SMASH EDO trial... plus, the London Met Police round up children for their DNA, indigenous blockade in Amazonian region turns into a police massacre, another peacful protestors murdered by Israeli forces in occupied Palestine, Brighton campaign against crap coffee chain gets a double shot in the arm, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news678.htm">SchNEWS 678, 5th June 2009</a></b><br />
<b>Convoy Polloi</b> - Twenty four years have passed since the defining moment of the Thatcher government&rsquo;s assault on the traveller movement - the Battle of the Beanfield - SchNEWS revisits it all... plus, the G8 returns to Italy, to the site of the recent earthquake, fourteen road protesters say goodbye to court as their cases are dismissed, a Texas court dishes out draconian punishment to aid charity, a legal victory challenges police methods of storing protest pictures, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news677.htm">SchNEWS 677, 29th May 2009</a></b><br />
<b>Mosquito Bite</b> - The M&iacute;skito people on the coast of Nicaragua have broken away and declared themselves a new nation, in defiance of Daniel Ortega's government... plus, Bury Hill Wood, part of the Abinger Forest, Surrey is under threat from oil exploration by Europa Oil and Gas, plans are afoot to squat land in the Hammersmith area of London and turn it into an eco-village, things are getting harder for vivisection lab Huntingdon Life Sciences, as major shareholder Barclays bank pulls out investment, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news676.htm">SchNEWS 676, 22nd May 2009</a></b><br />
<b>Final Frontiers</b> - No Borders special as SchNEWS reports on the humanitarian crisis in Calais caused by British immigration policy... plus, will the belated withdrawal of British backing affect the Colombian military and their reign of violence, the Sri Lankan government claims victory over the Tamil Tigers but we question at what price, anti-Trident protesters have occupied a new site at the Rolls Royce Rayensway hi-tech manufacturing facility in Derby, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news675.htm">SchNEWS 675, 8th May 2009</a></b><br />
<b>Brighton Rocked</b> - As Smash EDO Mayday mayhem hits the streets of Brighton we report from the mass street party and demonstration against the arms trade, war and capitalism... plus, the bike-powered Coal Caravan concludes its three week tour of the North Of England, a US un-manned Predator drone accidently bombs the village of Bala Baluk, killing up to 200 innocent civilians, RavensAit, the squatted island on the Thames near Kingston, is finally evicted this week, the Nine Ladies protest camp, having won its ten-year battle to stop the quarrying of Stanton Moor in Derbyshire, has finally tatted down and finished, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news674.htm">SchNEWS 674, 1st May 2009</a></b><br />
<b>May the Fourth be With You</b> - With Smash EDO's Mayday Mayhem about to hit Brighton, here's some useful information covering the day.... plus, RBS&nbsp;are seeking &pound;40,000 in compensation from a seventeen year old girl who damaged one computer screen and keyboard at the G20 demo last month, the Metropolitan Police are forced to admit they assaulted and wrongfully arrested protesters during a 2006 demo at the Mexican Embassy in London, the IMF/World Bank meeting in Washington DC is met with three days of protests,&nbsp;one London policeman is sacked for admitting that the police killed someone while another is merely disciplined for saying that he 'can't wait to bash up some long haired hippys', and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news673.htm">SchNEWS 673, 24th April 2009</a></b><br />
<b>Quick Fix</b> - The twelve students arrested this month as alleged 'terrorists' have been released without charge - like many 'anti-terrorist operations' in Britain, this one becomes a joint operation between the police and the media... plus,&nbsp;a protester in the West Bank town of Bil'in is killed by the Israel military while protesting against the 'Apartheid Wall',&nbsp;Birmingham squatters prevent two separate community squats in the city from being evicted on the same day, the bloodshed continues in Sri Lanka,&nbsp;a coalition hasformed called The United Campaign Against Police Violence, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/681-stonehenge-drones-lg.jpg" target="_blank">
											<img src="../images/681-stonehenge-drones-sm.jpg" alt="Police use unmanned surveillance drone at Stone Henge" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 26th June 2009 | Issue 681</b></p>
<p><b><i>WAKE UP!! IT&#8217;S YER EVER BOWLING GOOGLYS...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news681.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news681.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6811.php">It&#8217;s Just Not Cricket</a>  |  <a href="../archive/news6812.php">Borderline Insanity</a>  |  <a href="../archive/news6813.php">You&#8217;re Nicked Sunshine!</a>  |  <a href="../archive/news6814.php">A Turd In The Pool  </a>  |  <a href="../archive/news6815.php">Schools Not Out</a>  |  <a href="../archive/news6816.php">Playing Solitaire  </a>  |  <a href="../archive/news6817.php">Tarred With A Brush  </a>  |  <a href="../archive/news6818.php">Big Issue  </a>  |  <a href="../archive/news6819.php">Kilt In Action</a>  |  <a href="../archive/news68110.php">Bleeding Harts  </a> 
</p>


<div id="article">

<h3>IT&#8217;S JUST NOT CRICKET</h3>

<p>
<strong>100,000 MARCH IN LONDON TO REMIND THE WORLD THAT THE FIGHTING IN SRI LANKA MAY BE OVER, BUT THE SUFFERING OF THE TAMIL PEOPLE CONTINUES...</strong> <br />  <br /> After 26 years of bloodshed, violence and terror from the Sri Lankan military on one side and the Liberation Tigers of Tamil Eelam (LTTE or the Tamil Tigers) on the other, the civil war in Sri Lanka supposedly came to an end last month as the army took control of the last slither of Tiger controlled territory and paraded the dead leadership of the LTTE on television (See <a href='../archive/news676.htm'>SchNEWS 676</a>). According to SchNEWS&rsquo; sources on the ground, the Sinhalese areas of Sri Lanka are now being bombarded with the propaganda of victory as billboards line the streets proclaiming the great victory and forthcoming peace while &ldquo;<em>returning soldiers are given flowers in the streets</em>&rdquo;. Meanwhile, around 250,000 Tamils in the north have been confined to what are essentially prison camps, where it is claimed they face daily torture, rape and disappearances. <br />  <br /> The massive Tamil diaspora, in Britain and around the world, are determined that the continuing plight of their people does not drop out of view following the end of the fighting. While the rolling protests of marches, blockades and hunger strikes staged in Parliament Square came to an end last Wednesday (17th), the following Saturday (20th) saw around 100,000 march through Central London. The protest was led by a mocked-up concentration camp consisting of Tamils dressed in blood-stained clothing and bandages surrounded by barbed wire, followed by a procession of people carrying photos of lost loved ones. The last month has also seen a number of protests outside cricket grounds as the Sri Lankan team competed in the 20/20 cricket world cup. <br />  <br /> The British authorities, having completely failed to bring the Sri Lankan government to account while it was committing atrocities, has moved from indifference to irritation. While Westminster Council complained that the protests would damage the grass, which was going through &ldquo;urgent reseeding&rdquo;, the Deputy Mayor of London likened the Parliament Square protests to a &ldquo;shanty town&rdquo;. Conservative MP Gerald Howarth raised a point of order in parliament saying &ldquo;<em>It is completely outrageous that members of this House have been subjected to this inconvenience... The situation in Sri Lanka is nothing to do with this House</em>&rdquo;. <br />  <br /> The Tamils, aware of Britain&rsquo;s historical role in the conflict (See <a href='../archive/news665.htm'>SchNEWS 665</a>), take a different view. One campaigner was quoted as saying &ldquo;<em>Britain is to blame for this; like Palestine, like Zimbabwe, your history has a hand in the death of innocents in 2009, and the British government should stand up and take ownership of it</em>&rdquo;. <br />  <br /> * See <a href="http://www.tamilsforum.com" target="_blank">www.tamilsforum.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>cricket</span>, <span style='color:#777777; font-size:10px'>liberation tigers of tamil eelam</span>, <a href="../keywordSearch/?keyword=sri+lanka&source=681">sri lanka</a>, <a href="../keywordSearch/?keyword=tamil&source=681">tamil</a>, <a href="../keywordSearch/?keyword=tamil+tigers&source=681">tamil tigers</a></div>
<?php echo showComments(373, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BORDERLINE INSANITY</h3>

<p>
<strong>The No Borders Camp in Calais is now in full swing</strong> despite scare-scaremongering from both the press and the authorities who the camp have condemned for attempting to &ldquo;<em>paint the camp participants as violent criminals and (refusing) to engage in the real issues we campaign on</em>&rdquo;. Sam Davis, a UK No Borders activist commented that &ldquo;<em>The only violence we see is the daily and constant attacks on individuals by the French police. The rest comes from the vivid imagination of journalists keen to silence any debate on these issues</em>.&rdquo; <br />  <br /> Four hundred activists from around Europe have already arrived and have invited local residents and migrants alike to join them - in a relaxed atmosphere - to attend workshops, share experiences and construct the camp which will remain until this Sunday. <br />  <br /> Actions have already begun. Activists from Lille blockaded a detention centre this morning to prevent the removal of migrants by the French authorities. Twenty nine were arrested after locking the gates of the Lesquin detention centre using arm tubes. <br />  <br /> The Camp continues to insist that the demonstrations planned for Saturday (27th) are intended to be peaceful and have agreed to alter the route of the march in order to limit the inconvenience to local people. <br />  <br /> Migrants attending the camp report that they suffer from regular attacks from police who destroy their camps and arbitrarily arrest and beat them. In recent days there have been reports of the eviction and beating of Eriteans living in a squatted building in Calais town centre and the  destruction of migrant camps along the coast. <br />  <br /> As the press continue to blame the influx of migrants to Calais on the UK benefit system, it is clear from those there - many of whom have spent their life savings to make the journey - that the reality is very different: &ldquo;<em>I just want to be free to go where I choose. I don&rsquo;t care about money, I just want to be safe</em>,&rdquo; one Iraqi refugee told us. <br />  <br /> &ldquo;<em>Some of the migrants we have met here are as young as twelve years old. To suggest that such young people take such a dangerous journey because they want money is not only ridiculous, it&rsquo;s insulting</em>&rdquo; said another activist. <br />  <br /> * See <a href="http://calaisnoborder.eu.org" target="_blank">http://calaisnoborder.eu.org</a> <br />  <br /> * Read the Twitters from South Wales No Borders at <a href="http://www.lasthours.org.uk" target="_blank">www.lasthours.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=681">asylum seekers</a>, <a href="../keywordSearch/?keyword=calais&source=681">calais</a>, <a href="../keywordSearch/?keyword=france&source=681">france</a>, <a href="../keywordSearch/?keyword=immigration&source=681">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=681">no borders</a>, <a href="../keywordSearch/?keyword=refugees&source=681">refugees</a></div>
<?php echo showComments(374, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>YOU&#8217;RE NICKED SUNSHINE!</h3>

<p>
<strong>AS POLICE CLAMP DOWN ON STONEHENGE SOLSTICE CELEBRATIONS</strong> <br />  <br /> Revellers attending this year&rsquo;s solstice celebrations at Stonehenge last weekend reported a massive police presence. Bolstered by tooled-up private security (presumably employed by English Heritage), police took control of the gathering but, in the words of one attendee, &ldquo;<em>If the Babylon are going to start bringing their shit to Stonehenge again, and act in an intimidating, intrusive manner as they did this year, we need to treat it as a serious threat to our right to gather peacefully</em>.&rdquo; <br />  <br /> &ldquo;<em>[There were] Hundreds of coppers in total; at the entrance to the stones, the exit from the car park and patrolling all over the site supported by considerable numbers of private security contractors with stab vests, handcuffs and sniffer dogs at the entrance to the stones</em>&rdquo;. <br />  <br /> To totally destroy any remnants of sacred energy, Wiltshire police continually buzzed the site with an unmanned drone copter, which filmed the crowd below. Also a police helicopter hovered over the car park at low altitude for about an hour from around 10am. There were 37 arrests and three hundred searches with sniffer dogs. <br />  <br /> As one told us &ldquo;<em>There was nothing free about it</em>&rdquo;. Another commented on Indymedia &ldquo;<em>Let&rsquo;s bring Fitwatch tactics to the party next year, give out some flyers encouraging people to challenge the policing of the event, and, if possible, find a way to knock that fucking drone out of the sky. If the pigs are going to police Stonehenge as if it was a demo, it&rsquo;s because they rightly see such free and anarchic gatherings as a threat to their authority. To preserve the spirit of the summer solstice, I really think it&rsquo;s time for us to organise against them</em>.&rdquo; <br />  <br /> <strong>* SchNEWS recommends Andy Worthington&rsquo;s book Stonehenge: Celebration and Subversion</strong>, featuring the history of spiritual and counter-cultural activity around the Stones going from 19th century pagan and Druidic gatherings through to the Stonehenge free festivals, the Battle Of The Beanfield, and more recently.  <br />  <br /> See <a href="http://www.andyworthington.co.uk/2009/06/20/its-25-years-since-the-last-stonehenge-free-festival&nbsp;" target="_blank">www.andyworthington.co.uk/2009/06/20/its-25-years-since-the-last-stonehenge-free-festival&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>english heritage</span>, <a href="../keywordSearch/?keyword=fitwatch&source=681">fitwatch</a>, <a href="../keywordSearch/?keyword=police&source=681">police</a>, <a href="../keywordSearch/?keyword=stonehenge&source=681">stonehenge</a>, <span style='color:#777777; font-size:10px'>summer solstice</span>, <span style='color:#777777; font-size:10px'>wiltshire</span></div>
<?php echo showComments(375, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>A TURD IN THE POOL  </h3>

<p>
A group of Nazi thugs were arrested last weekend outside the BNP Gathering in Blackpool for taunting an anti-BNP demonstration. A range of North West anti-fascist groups were there to show that Nick Griffin&rsquo;s recent victory in the European elections didn&rsquo;t mean that the far-right in any way represented the people of the region. <br />  <br /> The argy bargy happened when a group of skins approached the demo and started sieg-heiling the crowd, and chanting names of concentration camps. After a further tirade of racial abuse, the police finally intervened and arrested four. Other far-right thugs were waiting along the promenade to pick off individuals leaving the demonstration, so the protesters stuck together as a group as they moved off.&nbsp; <br />  <br /> * See <a href="http://lancasteruaf.blogspot.com/2009/06/four-nazis-arrested-at-anti-bnp-protest.html" target="_blank">http://lancasteruaf.blogspot.com/2009/06/four-nazis-arrested-at-anti-bnp-protest.html</a> <br />  <br /> * Anti-fascists are gearing up to spoil the party at the BNP&rsquo;s Red, White &amp; Blue Festival in August in Derbyshire. Meet 9am on the 15th at Market Pl Codnor, Ripley, DE5, followed by a National Demo at 11am. <a href="http://nobnpfestival.wordpress.com" target="_blank">http://nobnpfestival.wordpress.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>blackpool</span>, <a href="../keywordSearch/?keyword=bnp&source=681">bnp</a>, <a href="../keywordSearch/?keyword=fascism&source=681">fascism</a>, <span style='color:#777777; font-size:10px'>lancashire</span>, <a href="../keywordSearch/?keyword=neo-nazis&source=681">neo-nazis</a>, <a href="../keywordSearch/?keyword=nick+griffin&source=681">nick griffin</a>, <a href="../keywordSearch/?keyword=red+white+and+blue+festival&source=681">red white and blue festival</a></div>
<?php echo showComments(376, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>SCHOOLS NOT OUT</h3>

<p>
The Lewisham Bridge School squatters are still occupying the roof and part of the grounds of the school after surviving the scheduled eviction this Wednesday morning. A small number of bailiffs &ndash; and four police vans and a helicopter &ndash; arrived, but no attempt was made to evict the 20-30 people on the roof. Another 50 odd were also there on the ground for support. The squatters are still on alert and are calling out for more to get involved. <br />  <br /> The squatters and the local community are there to stop the listed school building being knocked down for a privatised mega-school on the site (See <a href='../archive/news679.htm'>SchNEWS 679</a>). <br />  <br /> Yesterday the protesters called for a &lsquo;stay away day&rsquo; for the local kids who would be at this school,  but are temporarily at the Mornington Centre. Parents and teachers put on art, music and sport activities as an alternative for the kids. Today (26th) they are holding a BBQ at the school from 7pm, with all invited. Call the site on 07946 541331. The school is in Elmira Street, off Loampit Vale SE13 7BN, near Lewisham rail station and DLR. <br />  <br /> * See <a href="http://www.facebook.com/group.php?gid=94554226367&amp;ref=mf" target="_blank">www.facebook.com/group.php?gid=94554226367&amp;ref=mf</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>lewisham</span>, <a href="../keywordSearch/?keyword=privatisation&source=681">privatisation</a>, <a href="../keywordSearch/?keyword=schools&source=681">schools</a>, <a href="../keywordSearch/?keyword=squatting&source=681">squatting</a></div>
<?php echo showComments(377, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>PLAYING SOLITAIRE  </h3>

<p>
Anti-Shell protesters in Rossport, County Mayo, Ireland are gearing up for renewed action as their shore-to-ship monitoring shows that Shell&rsquo;s contracted pipe laying ship, the Solitaire, is steaming  their way.&nbsp; <br />  <br /> The Solitaire, the largest ship of it&rsquo;s kind in the world, is coming back for its second go at entering Broadhaven Bay to lay the offshore part of the gas pipeline - last year it was repelled from the area by a combination of the weather and a flotilla of gutsy activists in rubber dinghies (See <a href='../archive/news647.htm'>SchNEWS 647</a>). <br />  <br /> This time, 300 extra Gardai have been called in to provide extra muscle for the regular private security thugs and Shell have declared a 500m exclusion zone on apparently imaginary legal grounds.&nbsp; <br />  <br /> All are encouraged to get out there and join the David and Goliath struggle to protect the west coast of Ireland &ndash; see <a href="http://www.corribsos.com" target="_blank">www.corribsos.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>broadhaven bay</span>, <span style='color:#777777; font-size:10px'>county mayo</span>, <a href="../keywordSearch/?keyword=direct+action&source=681">direct action</a>, <a href="../keywordSearch/?keyword=gardai&source=681">gardai</a>, <a href="../keywordSearch/?keyword=ireland&source=681">ireland</a>, <a href="../keywordSearch/?keyword=rossport&source=681">rossport</a>, <a href="../keywordSearch/?keyword=shell&source=681">shell</a>, <span style='color:#777777; font-size:10px'>solitaire</span></div>
<?php echo showComments(378, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>TARRED WITH A BRUSH  </h3>

<p>
Last November 150 balaclava clad police descended in force on the isolated village of Tarnac in rural France, home to just 350 people. Twenty people were arrested and nine were later charged with &lsquo;criminal association for the purposes of terrorist activity&rsquo;. Amongst the detainees were a Swiss sitcom actor, a distinguished clarinettist, a student nurse and an Edinburgh University graduate who ran the grocer&rsquo;s shop and its adjoining bar-restaurant. <br />  <br /> The French government claimed that Tarnac had been the location of a cell of dangerous anarchist terrorists, who locals had been too simple to detect. The arrests followed months of surveillance and were supposedly in connection with incidents of sabotage of high speed train lines that passed near the village. While a few trains were delayed in the actions, no one was hurt.&nbsp; <br />  <br /> The nine, like a number of other young people in the village, had moved to Tarnac to escape the consumer society and to seek an alternative lifestyle based on community values and ecological principals. They came to the attention of the authorities after attending protests and demonstrations around Europe and the US. <br />  <br /> What paltry evidence the police had strongly focused on linking the nine with the publication of the anonymously written and rather optimistically titled The Coming Insurrection. <br />  <br /> Despite a complete lack of material evidence the authorities branded the nine as a &ldquo;<em>hard core cell that had armed struggle as its purpose</em>&rdquo; and described their home as a &ldquo;<em>a meeting place, for indoctrination, (and) a headquarters for violent action</em>&rdquo;. Residents scoffed at the claims: one neighbour, a sheep farmer and the independent mayor of the neighbouring village, Thierry Letellier, was quoted as saying &ldquo;<em>They were my neighbours, helping me on the farm and selling my meat at the shop. They were kind, intelligent and spoke several languages. They were politicised, on the left and clearly anti-capitalist like lots of people here, but they were people active in community life who wanted to change society at a local level first</em>.&rdquo; <br />  <br /> In spite of the heady claims of the police that these were dangerous terrorists, eight of the nine were rapidly released on bail. Only the supposed &lsquo;ringleader&rsquo;  Julien Coupat, remained in incarceration. The arrests sparked a worldwide solidarity campaign which has kept up pressure on the government throughout the last six months. Last Thursday Coupat was finally released, although under the terms of his release, he will have to stay in the Paris region and surrender his passport and identity papers. Even though they have completely failed to bring any prosecutions against the nine, they remain under investigation.&nbsp; <br />  <br /> Coupat has dismsissed the ridiculousness of the behaviour of the authorities in his persecution saying such &ldquo;<em>pathetic allegation(s) can only be the work of a regime that is on the point of tipping over into nothingness</em>&rdquo;. <br />  <br /> * See <a href="http://tarnac9.wordpress.com" target="_blank">http://tarnac9.wordpress.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=681">direct action</a>, <span style='color:#777777; font-size:10px'>eco-community</span>, <a href="../keywordSearch/?keyword=france&source=681">france</a>, <span style='color:#777777; font-size:10px'>julien coupat</span>, <span style='color:#777777; font-size:10px'>tarnac 9</span></div>
<?php echo showComments(379, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BIG ISSUE  </h3>

<p>
Last Sunday (21st), Boycott Israeli Goods activists held actions at Tesco&rsquo;s. Two branches in the Shoreditch area were targeted as protesters filled up a trolley with goods produced in illegally occupied land in the West Bank and sat in front of it while leaflets were handed out. Before police were called, they packed up and moved to another nearby Tesco&rsquo;s and did the same.  <br />  <br /> For more see <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=boycott+israeli+goods&source=681">boycott israeli goods</a>, <a href="../keywordSearch/?keyword=israel&source=681">israel</a>, <a href="../keywordSearch/?keyword=london&source=681">london</a>, <a href="../keywordSearch/?keyword=palestine&source=681">palestine</a>, <span style='color:#777777; font-size:10px'>shoreditch</span>, <a href="../keywordSearch/?keyword=tesco&source=681">tesco</a></div>
<?php echo showComments(380, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>KILT IN ACTION</h3>

<p>
Climate camping has, of course, established itself as the package holiday of choice for the more conscientious classes. But why wait till August 27th to get this year&rsquo;s fix*? Start the summer by getting up to Scotland where a new protest camp has, quite literally, got off the ground at Mainshill in South Lanarkshire, after Scottish Coal were given permission to mine 1.7 million tonnes of the black stuff, despite a long 9-year campaign by enraged locals. In fact over 700 of the 1000 odd population were moved to object to the county authorities &ndash; meaning, again, the go-ahead is completely without community consent.&nbsp; <br />  <br /> It&rsquo;s not that locals are merely being a bit nimbyish - their green and pleasant land is already despoiled by four other mines, making it one of the most heavily mined areas in the whole of Europe. And then there is the catalogue of climate chaos reasons not to be open-cast mining coal at the moment (see <a href="http://www.leaveitintheground.org.uk/?page_id=9" target="_blank">www.leaveitintheground.org.uk/?page_id=9</a>). The Mainshill mine is just one of 20 such projects recently given the go-ahead in Scotland. <br />  <br /> Last week a small number of activists secured a site in Mainshill Wood, declaring, &ldquo;<em>We have taken this autonomous and free space for those who wish to create positive, creative and egalitarian solutions to ecological collapse, climate change and environmental injustice. Profiteering companies, land owners and governments will not mine for new coal here!</em>&rdquo; Others quickly joined and tree-houses and other defences were quickly established.&nbsp; <br />  <br /> The cops have now got their arses into gear and have served papers for a June 29th hearing, after which the camp may be under threat of imminent eviction. Numbers are needed now to defend the land and enjoy the good weather.  <br />  <br /> For details on how to get there and more call 07806 926 040 and see <a href="http://www.coalactionedinburgh.noflag.org.uk/?page_id=415" target="_blank">www.coalactionedinburgh.noflag.org.uk/?page_id=415</a> <br />  <br /> * This year&rsquo;s Camp For Climate Action (August 27th &ndash; September 2nd) is also set to make excursions into Scotland (Aug 3-6th), and Wales (Aug 13-16th), before heading back to London to &ldquo;<em>prepare for an autumn of mass action in the run up to the United Nations climate talks in Copenhagen this December</em>&rdquo;.  <br />  <br /> See <a href="http://www.climatecamp.org.uk" target="_blank">www.climatecamp.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=681">camp for climate action</a>, <a href="../keywordSearch/?keyword=climate+change&source=681">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=681">coal</a>, <a href="../keywordSearch/?keyword=direct+action&source=681">direct action</a>, <span style='color:#777777; font-size:10px'>mainshill woo</span>, <a href="../keywordSearch/?keyword=protest+camp&source=681">protest camp</a>, <a href="../keywordSearch/?keyword=scotland&source=681">scotland</a>, <span style='color:#777777; font-size:10px'>south lanarkshire</span></div>
<?php echo showComments(381, 9); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BLEEDING HARTS  </h3>

<p>
Wildlife campaigner Trevor Weeks has hit out at government plans to cull deer. He told SchNEWS &ldquo;<em>There are currently increasing calls for a cull of Deer due to a rise in road collisions and damage to crops. Deer culling, like the culling of any animal or birds, is only short term and is never that affective</em>.&rdquo; He&rsquo;s calling for farmers to undertake more humane measures to prevent deer damage such as fencing.&nbsp; <br />  <br /> &ldquo;<em>All too often people look to the easy and convenient ways of shooting an animal rather than looking at the long term cheaper solutions which do not cause the unnecessary death of animals</em>.&rdquo; <br />  <br /> * Visit the Number 10 website and sign a petition against the cull at  <a href="http://petitions.number10.gov.uk/StoptheDeerCull" target="_blank">http://petitions.number10.gov.uk/StoptheDeerCull</a> <br />  <br /> * Or if you&rsquo;re not yet involved with yer local fox hunt sabs see <a href="http://hsa.enviroweb.org" target="_blank">http://hsa.enviroweb.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=681">animal rights</a>, <span style='color:#777777; font-size:10px'>deer</span>, <span style='color:#777777; font-size:10px'>deer culling</span></div>
<?php echo showComments(382, 10); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS advises all readers - don&#8217;t sit inside on  your computer reading SchNEWS, get out and enjoy  the sunshine. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

