<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 789 - 23rd September 2011 - Dogs of War</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, palestine" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 789 Articles: </b>
<p><a href="../archive/news7891.php">Flying The Flag</a></p>

<p><a href="../archive/news7892.php">Egypt: Sheikhing The System</a></p>

<p><a href="../archive/news7893.php">Too Late To Dale Out</a></p>

<p><a href="../archive/news7894.php">Glue-me Retail Outlook</a></p>

<b>
<p><a href="../archive/news7895.php">Dogs Of War</a></p>
</b>

<p><a href="../archive/news7896.php">Sparks Fly</a></p>

<p><a href="../archive/news7897.php">Rip: Troy Anthony Davis</a></p>

<p><a href="../archive/news7898.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd September 2011 | Issue 789</b></p>

<p><a href="news789.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DOGS OF WAR</h3>

<p>
<p>  
  	Despite being slipped a cache of tear gas and stun grenades by the Israeli military ahead of Palestine&#39;s September application for UN recognition (see <a href='../archive/news786.htm'>SchNEWS 786</a>), some of the more insane sections of Israel&#39;s settler movement are also training dogs to attack Palestinians.  </p>  
   <p>  
  	The Civilian Attack Dog Unit (CADU) is the brain child of New York-born settlement leader Yekutiel &ldquo;Mike&rdquo; Guzofsky, a former sidekick of the deceased Rabbi Meir Kahana &ndash; a man branded a fascist by the Israeli Supreme Court. Such an affiliation has harmed Guzosky&#39;s public image. In a rare moment of clarity the Home Office labelled him a terrorist and banned him from entering the UK in 2009 due to his affiliations with the Jewish Defense League (JDF) founded by Kahana.  </p>  
   <p>  
  	The&nbsp; CADU (or K9 Jewish Legion) is an offshoot of Israel&#39;s Best Friend (IBF), a registered NGO and not-for-profit started by Guzosky &ndash; still its CEO. (That sentence was expecially created for acronym fans).  </p>  
   <p>  
  	In a promotional video posted on YouTube Guzosky claims that the dogs are &ldquo;really friendly&rdquo; to their &ldquo;Jewish masters&rdquo; but &ldquo;they&#39;re very, very dangerous dogs if you are an Arab terrorist trying to penetrate into a Jewish town in Judea and Samaria [ie. the West Bank].&rdquo; Unsurprisingly he doggedly neglected to divulge that these so-called &ldquo;Jewish towns&rdquo; are in fact colonies illegal under international law and only established and perpetuated by the state terrorism of the Israeli Defense Force (IDF).  </p>  
   <p>  
  	The racist nature of the unit is barely hidden, with the text accompanying the video stating that as Muslim Terrorists consider dogs unclean, they &ldquo;fear our Malinois and Dobermans more than they fear firearms and airstrikes.&rdquo; Its main dog training centre is housed in the West Bank settlement of Kfar Tapauch - where Guzofsky lives &ndash; known for it&#39;s affiliations with the banned Kahane Chai movement. Kfar Tapauch served as an unofficial base of the extremist group during the last decade, and was where IDF corporal Eden Natan Zada spent much of his time in the eighteen months before he gunned down four Palestinian-Israeli&#39;s on a bus near Haifa in 2005.  </p>  
   <p>  
  	As the Palestinian Authority gets ready to go before the UN Security Council, Jewish settlers across the West Bank have embarked on a wave of attacks aiming to &#39;show Palestinian&#39;s whose land it is&#39;. Violence has been particularly brutal in villages around Nablus, with Awarta, Iraq Burin and Asira al-Qibliya all targeted. In Asira settlers from the colony of Yitzhar stormed the village&nbsp; on Tuesday 20th September, damaging property and assaulting Palestinians. Unsurprisingly when the IDF arrived on the scene it was the Palestinians defending their village who felt the brunt of Israeli soldiers. A 13-year-old boy was hit in the back at close range by a high-velocity tear gas canister, and at least three people were injured by rubber bullets.  </p>  
   <p>  
  	In response a coalition of Palestinian and international activists this week launched &#39;Refusing to Die in Silence&#39;, a group dedicated to recording and monitoring settler violence in the Occupied Territories. The attacks came as Barack Obama readied himself for a UN speech in which he reiterated the right of Israel to defend itself and the need for direct negotiations between the right-wing government of Benjamin Netanyahu and the puppet-regime of Mahmoud Abbas.  </p>  
   <p>  
  	Obama made no mention of settlements - and the fact that Israel&#39;s foreign minister lives in one - or the barbaric treatment of Palestinians.  </p>  
   <p>  
  	But never mind the Baracks... despite the high chances that today&#39;s application to the UN Security <br />  
  	Council is likely to be vetoed by the US, there&#39;s nothing they&#39;ll be able to do to stop an overwhelming majority granting some recognition of a Palestinian state if they apply for &ldquo;Observer&rdquo; status with the&nbsp;&nbsp; But if they then go to the UN General Assembly, which seems likely sooner or later, the Palestinians will win an overwhelming majority. <br />  
  	 <br />  
  	Link to Civilian Attack Dog Unit video: <a href="http://www.youtube.com/watch?feature=player_embedded&amp;v=JUJqDfft1U0" target="_blank">http://www.youtube.com/watch?feature=player_embedded&amp;v=JUJqDfft1U0</a> <br />  
  	 <br />  
  <a href="	http://www.alternativenews.org/english/index.php/topics/settlers-violence/3829-settler-attacks-countered-by-new-refusing-to-die-in-silence-group" target="_blank">	http://www.alternativenews.org/english/index.php/topics/settlers-violence/3829-settler-attacks-countered-by-new-refusing-to-die-in-silence-group</a> <br />  
  	 <br />  
  <a href="	http://www.alternativenews.org/english/index.php/topics/settlers-violence/3827-settlers-escalating-west-bank-attacks-in-protest-of-palestinian-un-bid" target="_blank">	http://www.alternativenews.org/english/index.php/topics/settlers-violence/3827-settlers-escalating-west-bank-attacks-in-protest-of-palestinian-un-bid</a> <br />  
  	 <br />  
  <a href="	http://www.cdi.org/terrorism/kach.cfm" target="_blank">	http://www.cdi.org/terrorism/kach.cfm</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1368), 158); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1368);

				if (SHOWMESSAGES)	{

						addMessage(1368);
						echo getMessages(1368);
						echo showAddMessage(1368);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


