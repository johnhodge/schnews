<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 685 - 27th July 2009 - Breaking Wind</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, workers struggles, renewable energy, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 685 Articles: </b>
<p><a href="../archive/news6851.php">The Hippy, Hippy Shakedown</a></p>

<p><a href="../archive/news6852.php">Still Camping It Up</a></p>

<p><a href="../archive/news6853.php">Still Kemping It Up</a></p>

<b>
<p><a href="../archive/news6854.php">Breaking Wind</a></p>
</b>

<p><a href="../archive/news6855.php">Testing Times</a></p>

<p><a href="../archive/news6856.php">Calais Abouts</a></p>

<p><a href="../archive/news6857.php">La Lutte Continue</a></p>

<p><a href="../archive/news6858.php">Twisty Tierney   </a></p>

<p><a href="../archive/news6859.php">Republic Gone Bananas</a></p>

<p><a href="../archive/news68510.php">Saving It For An Iran-y Day</a></p>

<p><a href="../archive/news68511.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Monday 27th July 2009 | Issue 685</b></p>

<p><a href="news685.htm">Back to the Full Issue</a></p>

<div id="article">

<div id='update' style='padding: 20px; border: 2px solid black; margin: 20px'>

<div id='date' style='text-align: right; font-size:11px; color: #888888; padding-bottom: 10px'>
Friday, 31st July 2009
</div>

<div id='title' style="font-family: 'Courier New', Courier, mono; font-weight: bold; font-size: 16px; padding: 10px; margin: auto; border-left: 2px solid black; border-right: 2px solid black" >
Special update from our reporter on the scene...
</div>


<h3>BALLS TO VESTAS</h3>

Just got back from the Isle of Wight. Hilariously the reason the company (Vestas) didn't get a possession order is that they tried to get it against 'persons unknown'. Now that might work when yer property's been taken over by random K-punks but when you've employed people for years it's a bit harder to argue that they're unknown.<br />
<br />
Vestas are still trying to control the food supply to the factory with security guards present the whole time. But undeterred,people are sneaking round and throwing tennis balls packed with vital resupplies up to he balcony where the occupiers are standing. Every once in a while a mass food rush happens. the fences are bypassed, the car park invaded and food passed up on a rope.<br />
<br />
The next court hearing is on Tuesday 4th August with a possible eviction after. The workers are calling out for solidarity. Currently there's a makeshift camp on a roundabout opposite the plant, where supporters are gathering. A bicycle soundsystem turned up yesterday evening and numbers are swelling generally.<br />

</div>

<br /><br /><br /><div style='font-weight:bold; font-size:13px; clear:all; float:none'><div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 Special Extended Report on Vestas Occupation 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div></div>

<h3>BREAKING WIND</h3>

<p>
After sticking their heads out of the Westminster windows for a quick climatic analysis, Britain&rsquo;s politicians have earmarked wind power as our best option in the race to prevent climate catastrophe. Yet, in April, when the worlds largest manufacturer of wind turbines, Vestas, announced plans to close its sites in the Isle of Wight and Southampton, the government remained silent - about both the loss of Britain&rsquo;s most significant turbine manufacturing facilities and the loss of hundreds of jobs. Faced with a wall of silence, apathy and hypocrisy, the non-unionised Vestas workers on the Isle of Wight had little option but to take action and, last week, they occupied their factory in protest. <br />  <br /> The occupation began on Monday night (20th) when 30 workers took over the management offices in the factory. The Vestas management immediately locked down the site and called in the police. Workers arriving for their last few days of work the next day were turned away from the gates, but when they realised why, many stayed on and established a picket outside the factory, which has since been manned by up to 150 people. <br />  <br /> Vestas took a hard line in response to the protest. Riot police who turned up on the first day threatened those inside with arrest for aggravated trespass and huffed and puffed but failed to force their way into the offices. They also prevented people from joining the protest or providing food to the occupiers. Although the riot shields have since been put away, police have continued obstructing attempts to deliver food. On Tuesday two people were arrested for breach of the peace after protesters staged a mass walk-in, in an attempt to resupply the workers inside.&nbsp; <br />  <br /> Hoping to starve the protesters out, Vestas imposed a food ban after the failure of their initial cunning plan of offering the workers food as long as they came out to get it. Following the success of the walk-in however, they changed their stance and are currently offering to provide the workers with food, as long as they accept no more from supporters.&nbsp; <br />  <br /> The company has so far refused to negotiate - preferring threats instead. They&rsquo;ve told the occupiers that they faced arrest and being fired without redundancy pay (a measly three weeks), if they didn&rsquo;t leave the premises immediately. Two workers have left the occupation but the others remain strong despite having been given four ultimatums. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 250" width="250px"><a href="../images/685-vestas-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/685-vestas-sm.jpg"  alt='Vesta Occupation Supporters'  /></a></td></tr></table>The factory on the Isle of Wight had been making turbines for the US market, but production is moving to China and the US. Vestas, who recently saw a 70% rise in quarterly profits, have failed to adapt production for European wind farms, blaming a &ldquo;lack of political initiatives&rdquo; and chronic nimbyism, with a &lsquo;not in my backyard&rsquo; attitude rife in Britain. <br />  <br /> Energy and climate change secretary, Ed Miliband, recently promised the creation of 400,000 new &lsquo;green&rsquo; jobs as part of his plans to meet his hot air target of &ldquo;over 30%&rdquo; of Britain&rsquo;s energy being produced by renewable sources, most notably wind power, by 2020. Passing wind on the Vestas situation, Miliband, without a trace of irony, blamed insufficient orders for the turbines and the difficulty in obtaining planning permission for wind farms. <br />  <br /> A similar situation arose in Scotland earlier in the year when Vestas closed a plant in Machrihanish,  Kyntyre, for similar reasons. However, the Scottish government negotiated a takeover by another company, (the hilariously named) Skykon, who, with the help of government subsidies, are planning to adapt the plant to purpose before expanding production as part of a push to meet the Scottish government&rsquo;s renewable energy targets. <br />  <br /> While the government has been happy to give the &lsquo;green&rsquo; light to airport expansion plans and the construction of new nuclear and coal powered power stations on the premise that they will create desperately needed jobs, not to mention pouring rivers of cash into the failing banks and propping up the car industry, it seems that stepping in to save 525 jobs in a hard hit island economy is out of the question. Up to their necks in hypocrisy, Miliband&rsquo;s feeble protestations have shown once again that the government&rsquo;s action on climate change amounts to little more than pissing in the wind turbine. <br />  <br /> * See <a href="http://www.savevestas.wordpress.com" target="_blank">www.savevestas.wordpress.com</a> <br />  <br /> * Send messages of support to: <a href="mailto:savevestas@googlemail.com">savevestas@googlemail.com</a> <br />  <br /> * The workers are calling for solidarity actions &ndash; specifically at Vestas UK head office in Warrington at 302 Bridgewater Place, Birchwood Park, WA3 6XG <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=685">direct action</a>, <span style='color:#777777; font-size:10px'>renewable energy</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=685">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(413);
			
				if (SHOWMESSAGES)	{
				
						addMessage(413);
						echo getMessages(413);
						echo showAddMessage(413);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>