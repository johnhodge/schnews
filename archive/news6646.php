<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 664 - 30th January 2009 - Big It Up  </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, israel, palestine, boycott israeli goods, carmel agrexco, tescos, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 664 Articles: </b>
<p><a href="../archive/news6641.php">Mner Mner...</a></p>

<p><a href="../archive/news6642.php">Iceland Meltdown</a></p>

<p><a href="../archive/news6643.php">Bolivian It Up</a></p>

<p><a href="../archive/news6644.php">Inside Guantanamo</a></p>

<p><a href="../archive/news6645.php">Exclusive: Omar Deghayes Speaks To Schnews</a></p>

<b>
<p><a href="../archive/news6646.php">Big It Up  </a></p>
</b>

<p><a href="../archive/news6647.php">Outfoxed</a></p>

<p><a href="../archive/news6648.php">Upping The Auntie  </a></p>

<p><a href="../archive/news6649.php">Bayer 'eck</a></p>

<p><a href="../archive/news66410.php">Tas-mania</a></p>

<p><a href="../archive/news66411.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th January 2009 | Issue 664</b></p>

<p><a href="news664.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>BIG IT UP  </H3>

<p>
The boycott on Israeli goods and Carmel Agrexco (See <a href="../archive/news649.htm">SchNEWS 649</a>) seems to be working.  Whilst the boycott has been going for some time, the latest Israeli military outrage has swelled the numbers of consumers voting with their trolleys and their action is starting to bite.  <br />
 <br />
According to this week&#8217;s guest publication, the ever-riveting International Supermarket News, Israeli farmers have been telling of unsold food going rotten in warehouses as supermarkets can&#8217;t shift the stuff. Israeli goods include potatoes, avocados, chillies, sweet potatoes, tomatoes, peppers, dates, melons, sharon fruit, oranges, pomegranates, sweetcorn, radishes and fresh herbs. <br />
 <br />
To help keep the pressure up, the BIG (Boycott Israeli Goods &#8211; geddit!) Campaign are holding a demo on Feb 7th at the Carmel Agrexco HQ at Swallowfield Way, Hayes, Middlesex.  <br />
 <br />
They&#8217;ll be there to highlight the fact that a vast number of flowers - grown in Israel and the Occupied Territories but often packaged and labelled as though from Holland &#8211; are flogged in the name of Valentine&#8217;s Day each year. Lorries will be leaving the depot stuffed with blooms for ill-informed lovers around the country &#8211; if you can&#8217;t make the demo why not take the campaign to your local supermarket and let shoppers know what it means to say it with flowers... <br />
 <br />
* For demo details see <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a> <br />
 <br />
** Nationwide protests continue....! On 24th Jan till queues in a Leeds M & S store were brought to a standstill as activists held up, leafleted customers, and sparked debate about the issues surrounding profiteering from the occupation of the West Bank. The protesters were stopped and searched by the police but no arrests were made. <br />
 <br />
On 27th Jan, three protestors were arrested in a Tesco in Swansea for filling a shopping trolley with Israeli goods and covering it in goats blood. Ms Murphy, a grandmother-of-four, has been charged with theft and will have to appear in court.  An undercurrents video report of the protest is now available on <a href="http://undercurrentsvideo.blogspot.com/2009/01/israeli-goods-seized-in-tescos-and.html" target="_blank">http://undercurrentsvideo.blogspot.com/2009/01/israeli-goods-seized-in-tescos-and.html</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=664">gaza</a>, <a href="../keywordSearch/?keyword=israel&source=664">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=664">palestine</a>, <a href="../keywordSearch/?keyword=carmel+agrexco&source=664">carmel agrexco</a>, <a href="../keywordSearch/?keyword=tescos&source=664">tescos</a>, <a href="../keywordSearch/?keyword=direct+action&source=664">direct action</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(209);
			
				if (SHOWMESSAGES)	{
				
						addMessage(209);
						echo getMessages(209);
						echo showAddMessage(209);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>