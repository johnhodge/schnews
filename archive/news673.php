<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(40);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	40;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 673 - 24th April 2009 - As terror suspects are released without charge</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, apartheid wall, palestine, israel, basem abu rahme, tristan anderson, ewa  jasiewicz, idf, birmingham, squatting, justice not crisis, tamil tigers, sri lanka, parliament square, demonstration, parameswaran  subramaniya, yorkshire evening pest, leeds, spoof newspaper, ian thomlinson, police, g20 summit, wille corduff, rossport, ireland, shell, glengad, shell to sea, terrorists, bob quick, hicham yezza, students, muslims, newspapers, police, war on terror, titnore, crap arrest of the week, supermarkets, co-op, tesco, barack obama, summit of the americas, trinidad, chavez, evo morales, fidel castro, cuba, nafta, plan colombia, plan mexico, zapatistas, latin america, barack obama, war on terror, overseas contingency operation, war on terror" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/mayday-09.htm" target="_blank"><img
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news671.htm">SchNEWS 671, 3rd April 2009</a></b><br />
<b>G20 SUMMIT EYEWITNESS REPORT</b> - As the G20 Summit takes place, we look at what the leaders are discussing, along with an eyewitness account of the G20 protests around the Bank Of England on Wednesday April 1st.... plus, a report from day two of the protests, on the day when the G20 Summit began at the ExCel entre in the East London Docklands, the squatted island at Raven's Ait, on the Thames near Surbiton, is under threat from eviction and calling for help, groups of sacked workers who are feeling the direct consequences of the financial crash are protesting, and re-occupying their work places, peace campaigner Lindis Percy causes traffic chaos outside the USAF airbase at Lakenheath in Suffolk, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news670.htm">SchNEWS 670, 27th March 2009</a></b><br />
<b>G20 SUMMIT SPECIAL</b> - SchNEWS looks how deep the financial problems are for the banks and the British Govt, and how they won't learn from their errors. We ask the question: the crash which looked inevitable in 1999 has happened, so we look to how we can survive this.... plus, ex-Royal Bank Of Scotland boss Fred 'The Shred' Goodwin, has his home and car attacked by demonstrators, Horfield Prison in Bristol is surrounded by a noise demo in solidarity with Elija Smith, one of the EDO Decommissioners, Climate change campaigners stop work at Muir Dean open-cast coal mine in Scotland, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news669.htm">SchNEWS 669, 20th March 2009</a></b><br />
<b>Stand Up To Detention</b> - As the British Home Secretary opens another detention centre for 'failed' asylum seekers, No Borders protests around the country highlighting the plight of those victims of the UK asylum system.... plus, the sixth anniversary of the murder in Gaza of US activist Rachel Corrie is tragically marked by the shooting of another US activist, the largest Gypsy and Traveller community in Britain is seriously under threat of being evicted this year, Smash EDO hold an all night demo outside the Brighton factory of EDO-ITT, and more.....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news668.htm">SchNEWS 668, 13th March 2009</a></b><br />
<b>Taking It All In</b> - A giant independent UK aid convoy involving 110 vehicles travels 6000 miles to deliver medical and other supplies to war-torn Gaza.... plus, Smash EDO protester is arrested for criminal damage after banging a metal fence with a piece of plastic, two stories about squatted social centres in London and Brighton resisting eviction, Shell To Sea campaigner gaoled for four weeks in an action against a Shell oil refinery and offshore pipeline, student activist Hicham Yezza is sentenced to nine months imprisonment under an immigration technicality, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news667.htm">SchNEWS 667, 6th March 2009</a></b><br />
<b>Fools Rush In</b> - As the financial leaders preapre to gather, plans for mass G20 actions in London take shape, plus.... the Coroners and Justice Bill is the latest Big Brother law trying to snea its way through parliament, Tintore protesters up the fight against Tesco developers, EDO activists get charges increased to conspiracy, Hunger striker makes point at 10 Downing St., and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news666.htm">SchNEWS 666, 13th February 2009</a></b><br />
<b>The Watchtower</b> - SchNEWS has a round up of the latest attacks against our civil liberties, and asks whose liberties are being protected, plus... a new undercover investigation in Huntingdon Life Sciense shows that the abuse continues, protests continue against Israeli exporter Carmel Agrexco, illegal growers of produce on Palestinian soil, the Sea Sheperd vessal, the Steve Irwin, returns after a 27 day hot pursuit of Japanese whaling vessel, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news665.htm">SchNEWS 665, 6th February 2009</a></b><br />
<b>Sheik Battle & Rule</b> - In Somalia, the US's African front in the war on terror has collapsed as the UN-installed Government has fled.... plus, Binyam Mohammed, a British resident who's been tortured and wrongly imprisoned at Guantanamo since his arrest in Pakistan in 2002, is still fighting for justice and currently on hunger strike, new laws come in this month which make it criminal to photograph or get information about anyone in the armed or police forces,  a look at this weeks wildcat strikes around the subcontractor employing Italian and Portuguese workers, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news664.htm">SchNEWS 664, 30th January 2009</a></b><br />
<b>Guantanamo Special</b> - SchNEWS looks and what Obama's said he will do about Guantanamo and interviews Guantanamo ex-detainee Omar Deghayes to guage his reaction - plus Mner Mner.... Workers of a bust printing company in Buenos Aires have re-occupied  their factory and are running it as a co-op, one of many similar workplaces in Argentina since the collapse in 2001. As the UK slips into a financial black hole, could this be a portend to a post-collapse future?.... plus, the Iceland government has collapsed, SchNEWS looks and what Obama's said he will do about Guantanamo and interviews Guantanamo ex-detainee Omar Deghayes to guauge his reaction, despite the ban, fox hunting continues, and last weekend fifty hunt saboteurs came up against the particularly abusive New Forest hunters, and police, demonstration continued this week against companies doing business with vivisection lab Huntingdon Life Sciences, and more...</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/673-obama-chavez-lg.jpg" target="_blank">
											<img src="../images/673-obama-chavez-sm.jpg" alt="" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 24th April 2009 | Issue 673</b></p>
<p><b><i>WAKE UP!! WAKE UP!! IT'S YER 'CLEAN SKINNED'...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news673.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news673.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6731.php">Quick Fix</a>  |  <a href="../archive/news6732.php">Crap Arrest Of The Week</a>  |  <a href="../archive/news6733.php">Summit Of The Un-americas</a>  |  <a href="../archive/news6734.php">And Then We Take... Bil&#8217;in</a>  |  <a href="../archive/news6735.php">Brum Punch</a>  |  <a href="../archive/news6736.php">Tiger Tiger  Burning Bright</a>  |  <a href="../archive/news6737.php">First Past The Post</a>  |  <a href="../archive/news6738.php">Passing The Baton</a>  |  <a href="../archive/news6739.php">Rossport In A Storm</a>  |  <a href="../archive/news67310.php">And Finally</a> 
</p>


<div id="article">

<h3>QUICK FIX</h3>

<p>
<p><strong>AS TERROR SUSPECTS ARE RELEASED WITHOUT CHARGE.. <br /> 
      </strong> <br /> 
      Unsurprisingly enough, the twelve terror suspects arrested and detained without charge over Easter have been released. Never charged, these innocent men have been freed from the clutches of the anti-terror plod into the unwelcoming arms of the UK Borders Agency where Jacqui Smith&rsquo;s Home Office plots and schemes to have them deported back to Pakistan on the grounds that they are a &lsquo;<strong>security threat</strong>.&rsquo; <br /> 
         <br /> 
        The lawyer for the twelve, Mohammed Ayubsaid said &ldquo;<em>Our clients have no criminal history, they were here lawfully on student visas and all were pursuing their studies and working part-time. They are neither extremists nor terrorists. Their arrest and detention has been a serious breach of their human rights. As a minimum they are entitled to an unreserved apology</em>.&rdquo; <br /> 
         <br /> 
        Having been arrested slightly earlier than planned due to the &lsquo;balls up&rsquo; of Assistant Commissioner for Groundless Scaremongering, Bob Quick (the guy snapped whilst waving his &lsquo;top secret&rsquo; papers), the press worked themselves into a frenzy over a politician&rsquo;s incompetence putting British lives at risk (as if they ever would). Headlines such as &lsquo;Scramble to find the Easter bomb factory&rsquo; (The Times) gleefully leapt onto the front pages with a familiar absence of fact. Bob Quick&rsquo;s just retired on a fat pension and the ensuing hoo-ha knocked Ian Tomlinson&rsquo;s death off the front pages &ndash; nice work!! <br /> 
         <br /> 
        These &lsquo;anti terrorist operations&rsquo; are really joint exercises by the police and the media. The fact that the men had no previous convictions and didn&rsquo;t appear on any terror/crime/subversive watchlists anywhere was spun as to make them appear even more dangerous - &lsquo;clean skins&rsquo; in the spooks&rsquo; vocabulary - &ldquo;<em>highly trained, professional killers whose blameless backgrounds provide not the slightest clue as to their true, evil intent</em>&rdquo; (to quote the Terrorgraph). So lack of evidence is used as proof of wrongdoing. Looks like the people who spun the Iraq War are still getting paid. <br /> 
         <br /> 
        The twelve innocent men - who&rsquo;ve been threatened at gunpoint, imprisoned without charge and are now facing deportation - are just the latest in a long litany of repression justified as security. From the Forrest Gate shooting (no man shot by cops, no charges made), the Ricin plot (no ricin, no plotting ever happened) and the arrest of Nottingham student Hicham Yezza (downloading public domain documents - see <a href='../archive/news668.htm'>SchNEWS 668</a>), the pattern repeats time and time again - high profile arrests, persecution of immigrant communities, &lsquo;<strong>Britain under attack</strong>&rsquo; headlines. <br /> 
         <br /> 
        Using the flimsy excuse that the men were in the UK on student visas, the government wants universities to vet foreign nationals against state watch-lists and act as part of the state surveillance network against (Muslim) foreigners. Fortunately, British Universities have stood up to attempts to co-opt them Syrian-style into the state repression machinery, for now. <br /> 
         <br /> 
        The supposed &lsquo;<strong>threat to national security</strong>&rsquo; pretext for their deportation forms part of the government&rsquo;s latest strategy, &lsquo;Contest 2&rsquo; , as elaborated in The United Kingdom&rsquo;s Strategy for Countering International Terrorism. This emphasises threats from domestic terrorist threats, due to &lsquo;an extremist violent ideology associated with Al Qaeda&rsquo;. <br /> 
         <br /> 
        The policy emphasises sources of terrorism in political views. The prevention strategy will challenge &lsquo;views which fall short of supporting violence and are within the law, but which reject and undermine our shared values and jeopardise community cohesion.&rsquo; To counter that threat, Contest 2 justifies greater use of &lsquo;non-prosecution actions&rsquo;, hence various special powers to impose punishment without trial, such as house arrest, asset-freezing and deportation. <br /> 
         <br /> 
        First they came for the Muslims..... <br /> 
         <br /> 
        </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>bob quick</span>, <a href="../keywordSearch/?keyword=hicham+yezza&source=673">hicham yezza</a>, <span style='color:#777777; font-size:10px'>muslims</span>, <a href="../keywordSearch/?keyword=newspapers&source=673">newspapers</a>, <a href="../keywordSearch/?keyword=police&source=673">police</a>, <a href="../keywordSearch/?keyword=students&source=673">students</a>, <span style='color:#777777; font-size:10px'>terrorists</span>, <a href="../keywordSearch/?keyword=war+on+terror&source=673">war on terror</a></div>
<?php echo showComments(295, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>CRAP ARREST OF THE WEEK</h3>

<p>
<p><strong>For leaving a bin laden with food...</strong> <br /> 
     <br /> 
    One of the anti-Tesco campaigners from Titnore woods (See <a href='../archive/news672.htm'>SchNEWS 672</a>) made the mistake of assuming food tossed in a skip outside a Co-op supermarket was in some way unwanted and proceeded to liberate seven carriers bags of it to feed hungry masses at a Bristol Squat. He was swiftly nicked and held in (out of date) custody. <br /> 
     <br /> 
    Instead of being grateful for sparing them the cost of sending it all to landfill, the &lsquo;caring sharing&rsquo; Co-op are instead looking to prosecute. As if the Co-op think stopping skipping would force these scroungers to get jobs so they had the money to buy the same food before it was chucked. <br /> 
  </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>co-op</span>, <a href="../keywordSearch/?keyword=crap+arrest+of+the+week&source=673">crap arrest of the week</a>, <a href="../keywordSearch/?keyword=supermarkets&source=673">supermarkets</a>, <a href="../keywordSearch/?keyword=tesco&source=673">tesco</a>, <a href="../keywordSearch/?keyword=titnore&source=673">titnore</a></div>
<?php echo showComments(304, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>SUMMIT OF THE UN-AMERICAS</h3>

<p>
<p>For Barack Obama the G20 Summit was something of a festival of sycophancy as world leaders rubbed up against him like cats in heat, eager for some of that shiny, hopeful magic to stick to them and play well to the audiences at home. Two weeks later though, at the Summit of the Americas, Obama was facing a different proposition altogether.  <br /> 
     <br /> 
    Gone were the gushing platitudes and hearty backslapping, replaced instead with 50 minute diatribes on &lsquo;Yankee Imperialism&rsquo;, homie handshakes with American bogeymen, and allegations that America was meddling and plotting assassinations in other countries. <br /> 
     <br /> 
    The summit was held in Port of Spain, Trinidad, where it has been deeply controversial. Problems began months before the summit when Beetham Gardens, a run down area of government housing, was walled off from the rest of the capital. <br /> 
     <br /> 
    Beetham came into existence 35 years ago when a large squatters community was transformed by government financed construction of concrete homes with running water, indoor plumbing, electricity and even mail delivery. But neglected thereafter, it has since been plagued by poverty and crime. In what the government declared to be nothing more than a touch of &lsquo;beautification&rsquo;, the 5ft wall completely separated the community from the rest of the city. <br /> 
     <br /> 
    Squatters living on land near a railway line due to transport the arriving heads of state were other casualties of the tarting up process. While some had lived in the area for over thirty years, they were handed eviction notices by the Land Settlement Agency - despite many of them holding &lsquo;Certificates of Comfort&rsquo; which should have protected them from eviction. Even after angry protests the residents say they were offered no serious alternatives despite promises. <br /> 
     <br /> 
    As the summit approached, the Trinidad and Tobagan government took further steps to protect the incoming leaders from reality. With the summit costing billions of dollars to put on, a number of local unions and social movements were planning large demonstrations to highlight the hypocrisy of such extravagant spending in the face of far more pressing issues like high food prices, inflation, crime, corruption and health. However, all applications for demos were rejected, essentially banning all protest. The Truth Drummers &ndash; a coalition of several farming, civil rights and environmental groups - were told they had permission to stage a drumming demo but as soon as they began were ordered to disperse under threat of arrest, facing off with armed riot police. Another member of the group was arrested the day before the demo on charges of &lsquo;placarding&rsquo;, with bail set at $17,000. <br /> 
     <br /> 
    The alternative &lsquo;Peoples Summit&rsquo; was also repressed. All attempts to gain permission to hold the event in Port of Spain were turned down flat and the counter-summit was finally staged at the University of the West Indies, some distance outside of the city. Although this meant it didn't have the highly visible presence that the organisers had been hoping for, the three day event was still attended by hundreds of locals and internationals. <br /> 
     <br /> 
    The mainstream summit began with a now famous handshake as Venezualan president Hugo Chavez told Obama &ldquo;I want to be your friend&rdquo; and later slipped him a copy of Eduardo Galeanos famous history of imperial exploitation of Latin America, The Open Veins of Latin America. After the conference Chavez hailed a &lsquo;new era&rsquo; and announced the restoration of US-Venezuelan ambassadors. Diplomatic relations had been broken off in September in solidarity with Bolivian president Evo Morales who&rsquo;d expelled a top US diplomat for aiding opposition groups in inciting violence (Morales, subject of a recent thwarted murder attempt, was reported to have confronted Obama about American meddling in Bolvian politics and covert plots). But despite the thaw in relations Chavez still refused to back the final declaration of the summit. <br /> 
     <br /> 
    This was mostly in protest at the continued exclusion of Cuba from the gathering &ndash; a subject several other leaders raised with Obama. Despite the improvement in US-Cuban relations after the US relaxed travel restrictions and Cuban leader Raul Castro declared he was willing to talk about &lsquo;everything&rsquo;, the Big Brother of the Castro family &ndash; Fidel - declared the summit to be nothing more than &lsquo;delirious dreams&rsquo;. <br /> 
     <br /> 
    Chavez was not alone in refusing to sign the final declaration. In fact, Trinidad Prime Minister Patrick Manning was the only one that did. Aside from Cuba, absentees also cited issues that were off the agenda - such as a serious discussion of the economic crisis and more local issues like the growth of the biofuels industry. Also of concern was the Obama&rsquo;s refusal to renegotiate NAFTA (North American Free Trade Area) or other free trade agreements. <br /> 
     <br /> 
    The rise of the current generation of leftist Latin American leaders has been paralleled by a rise in social movements at the grassroots level. But for those movements there were a number of pressing issues which barely got a mention. One such was the increasing militarisation of Colombia and Mexico in the name of the &lsquo;War on Drugs&rsquo;. <br /> 
     <br /> 
    While Obama has won plaudits for stalling a Free Trade deal with Colombia until it improves its horrendous human rights record, he has also backed the continuation of &lsquo;Plan Colombia&rsquo; which supplies billions of dollars of military equipment and support to the Colombian army. The plan has completely failed in its stated intention of ridding Colombia of the scourge of cocaine with coca production actually on the increase since its inception. While the American policy of spraying coca crops with industrial Roundup has caused untold environmental damage, the coca growers simply moved deeper into the cover of the jungle. Meanwhile the militarisation has reduced violence in major population centres (without tackling the underlying causes) but has pushed an ever expanding conflict deeper into rural areas, with accompanying human rights abuses. <br /> 
     <br /> 
    Mexico, with its exploding drug war, looks set to follow a similar path. A similar agreement, &lsquo;Plan Mexico&rsquo; - also known as the &lsquo;Merida Initative&rsquo; - began in 2008, a year in which over 6000 people were killed in the drug war. In a pre-summit visit to Mexico, Obama backed the plan and asserted his commitment to help fight the &lsquo;War on Drugs&rsquo;. <br /> 
     <br /> 
    For the indigenous rights movements across the continent, which continue to grow regardless of this latest summit circus, the main struggles are around recognition and land rights but also against environmental degradation, foreign exploitation of resources and the militarisation of their territories. As well as the continuation of the Zapatistas in Mexico there have been large-scale movements in Ecuador and Colombia (<a href='../archive/news656.htm'>SchNEWS 656</a>) and smaller protest movements wherever there is a sizeable indigenous population. <br /> 
     <br /> 
    Obama&rsquo;s opening statement that &ldquo;I have a lot to learn and I&rsquo;m very much looking forward to listening&rdquo; may be welcome relief after the Bush years. But with so many key issues off the table, and a number of Latin American countries refusing to cave in to US pressure, it might just be that Hilary Clinton&rsquo;s commentary on proceedings that she &ldquo;...thought the cultural performance was fascinating,&rdquo; may be more revealing. <br /> 
     <br /> 
    * See also <a href="http://mamaradio.blogspot.com" target="_blank">http://mamaradio.blogspot.com</a> <br /> 
    </p> <br /> 
 <br /> 
   <br /> 
     <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=barack+obama&source=673">barack obama</a>, <a href="../keywordSearch/?keyword=chavez&source=673">chavez</a>, <span style='color:#777777; font-size:10px'>cuba</span>, <a href="../keywordSearch/?keyword=evo+morales&source=673">evo morales</a>, <span style='color:#777777; font-size:10px'>fidel castro</span>, <a href="../keywordSearch/?keyword=latin+america&source=673">latin america</a>, <span style='color:#777777; font-size:10px'>nafta</span>, <span style='color:#777777; font-size:10px'>plan colombia</span>, <span style='color:#777777; font-size:10px'>plan mexico</span>, <span style='color:#777777; font-size:10px'>summit of the americas</span>, <span style='color:#777777; font-size:10px'>trinidad</span>, <span style='color:#777777; font-size:10px'>zapatistas</span></div>
<?php echo showComments(296, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND THEN WE TAKE... BIL&#8217;IN</h3>

<p>
<p><strong>Basem Abu Rahme</strong>, a resident of Bil&rsquo;in village in The West Bank, has become the eighteenth person to be killed by Israeli forces while protesting the construction of Israel&rsquo;s &lsquo;Apartheid Wall&rsquo; (See <a href='../archive/news544.htm'>SchNEWS 544</a>). Abu Rahme was shot in the chest last Friday (17th) with a high-velocity tear gas projectile by soldiers 40 metres away from the demonstration, later dying of his wounds in hospital. The tear gas projectile, designed to be used at long range, was the same as that which critically injured American activist Tristan Anderson last month (See <a href='../archive/news669.htm'>SchNEWS 669</a>). <br /> 
   <br /> 
  * Human rights worker, Free Gaza activist and journalist <strong>Ewa Jasiewicz</strong>, who was in Gaza during the recent military assault, will be in Brighton talking about her experiences and what you can do to help the people of Gaza. 7pm, Friends Meeting House, Ship Street, Brighton on April 28 &ndash; see <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> <br /> 
  </p> <br /> 
 <br /> 
   <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>apartheid wall</span>, <span style='color:#777777; font-size:10px'>basem abu rahme</span>, <span style='color:#777777; font-size:10px'>ewa  jasiewicz</span>, <a href="../keywordSearch/?keyword=idf&source=673">idf</a>, <a href="../keywordSearch/?keyword=israel&source=673">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=673">palestine</a>, <span style='color:#777777; font-size:10px'>tristan anderson</span></div>
<?php echo showComments(297, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BRUM PUNCH</h3>

<p>
To defend one eviction may be considered fortunate &ndash; but two in one morning is pretty darn impressive, so congrats to Justice Not Crisis (JNC) who repelled simultaneous attempted evictions at squatted social centres in Birmingham on Tuesday (21st). Getting people barricaded on the roofs proved insurmountable to bailiffs and police on the day and the centres &ndash; one being the Beechwood Hotel, which houses homeless people as well as the Birmingham People&rsquo;s Library project, and the other running permaculture training in the previously run-down nature reserve in the grounds &ndash; remain open but are still under imminent eviction alert and need support to get mobilised. <br /> 
   <br /> 
  * See <a href="http://justicenotcrisis.wordpress.com" target="_blank">http://justicenotcrisis.wordpress.com</a> <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=birmingham&source=673">birmingham</a>, <span style='color:#777777; font-size:10px'>justice not crisis</span>, <a href="../keywordSearch/?keyword=squatting&source=673">squatting</a></div>
<?php echo showComments(298, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>TIGER TIGER  BURNING BRIGHT</h3>

<p>
Recent Tamil protests against the continuing humanitarian disaster in Sri Lanka (See <a href='../archive/news.htm'>SchNEWS </a> <a href='../archive/news672.htm'>672</a>, <a href='../archive/news665.htm'>665</a>) continued this week when at least 3,500 Tamils blocked off Parliament Square, London, this Monday (20th).  <br /> 
   <br /> 
  The protesters assembled around the makeshift tent of hunger striker Parameswaran Subramaniyan, now into his second week. The UN estimates 4,500 civilians have been killed in the conflict in the last three months and that there are currently up to 110,000 refugees fleeing the fighting. <br /> 
   <br /> 
  * See <a href="http://london.indymedia.org.uk/articles/1228" target="_blank">http://london.indymedia.org.uk/articles/1228</a> <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>demonstration</span>, <span style='color:#777777; font-size:10px'>parameswaran  subramaniya</span>, <a href="../keywordSearch/?keyword=parliament+square&source=673">parliament square</a>, <a href="../keywordSearch/?keyword=sri+lanka&source=673">sri lanka</a>, <a href="../keywordSearch/?keyword=tamil+tigers&source=673">tamil tigers</a></div>
<?php echo showComments(299, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>FIRST PAST THE POST</h3>

<p>
One of the best spoof papers seen in recent times was published earlier this month in Leeds, apeing the local rag, the Yorkshire Evening Post, with a focus on the local affects of corrupt and failing monetarism. One journactivist told SchNEWS, &ldquo;Leeds has undergone crazy changes over the last 15 years. It&rsquo;s like the city has been gripped by a delirium. <br /> 
    <br /> 
  Buildings being ripped down and new ones springing up and all with no one able to even ask if this is a good thing. Huge amounts of money have been spent with no regard to the needs of the people of Leeds. The whole point of the paper is that unless people want to remain patsies in someone else&rsquo;s plans then we have to begin collectively getting to grips with this crisis and act on both it&rsquo;s effects and the &lsquo;solutions&rsquo; being put forward.&rdquo; <br /> 
   <br /> 
  * Read it at <a href="http://www.yorkshireeveningpest.co.uk" target="_blank">www.yorkshireeveningpest.co.uk</a> <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=leeds&source=673">leeds</a>, <span style='color:#777777; font-size:10px'>spoof newspaper</span>, <span style='color:#777777; font-size:10px'>yorkshire evening pest</span></div>
<?php echo showComments(300, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>PASSING THE BATON</h3>

<p>
<p>In response to police violence meted out at the G20, a broad coalition of victims of police violence, together with their representatives, has founded The United Campaign Against Police Violence (UCAPV). <br /> 
   <br /> 
  They say &ldquo;<em>A second post-mortem has revealed that Ian Tomlinson died of internal bleeding, not a heart attack as was initially claimed. The extent of the police lies and cover-up is sickening. One tragedy of Ian&rsquo;s death is that it is not the first time someone has died at the hands of the police. Our campaign must also take up the fight that many others are waging for justice for family members killed in police custody. And this comes at a time when wider questions about police tactics on and around demonstrations are coming thick and fast from all angles</em>.&rdquo; <br /> 
   <br /> 
  * April 30th - Picket of public meeting of the Metropolitan Police Authority meeting on G20 policing, City Hall. <br /> 
   <br /> 
  * May 5th - public rally at Friends Meeting House, Euston Road, London at 7pm. This will be the public launch of the United Campaign Against Police Violence. <br /> 
   <br /> 
  * May 23rd - National demonstration, 3pm marching from Trafalgar Square to Scotland Yard via Downing Street. <br /> 
   <br /> 
  * See <a href="http://www.againstpoliceviolence.org.uk" target="_blank">www.againstpoliceviolence.org.uk</a> <br /> 
  </p> <br /> 
 <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=g20+summit&source=673">g20 summit</a>, <a href="../keywordSearch/?keyword=ian+thomlinson&source=673">ian thomlinson</a>, <a href="../keywordSearch/?keyword=police&source=673">police</a></div>
<?php echo showComments(301, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>ROSSPORT IN A STORM</h3>

<p>
<p>Veteran Rossport protester Willie Corduff was attacked and hospitalised this week by a gang of masked security guards as he blocked the movement of a truck during protests at Shell&rsquo;s Glengad facility on the west coast of Ireland. Glengad is the site of the planned onshore part of their facility. Earlier  in the protest, which took place during the night of April 22nd-23rd, Shell&rsquo;s fences were torn down and the site invaded.  <br /> 
   <br /> 
  Shell to Sea Spokesperson Terence Conway said &ldquo;<em>This was a vicious and cowardly attack by this group of balaclava-wearing Shell mercenaries in the middle of the night, on a brave member of our community, Willie Corduff, who was peacefully protesting the illegality of this project. The only violence that happened in Glengad last night was carried out by Shell security</em>&rdquo;. <br /> 
   <br /> 
  Willie is one of the original Rossport Five  and won the Goldman Environment award (see <a href='../archive/news585.htm'>SchNEWS 585</a>). <br /> 
   <br /> 
  In the same week, one of the alumni of Shell's security operation in Rossport was gunned down as he attempted to assassinate Bolivia&rsquo;s Evo Morales. Not just yer average hired goons then? <br /> 
   <br /> 
  * See <a href="http://www.corribsos.com" target="_blank">www.corribsos.com</a> <br /> 
  </p> <br /> 
 <br /> 
   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>glengad</span>, <a href="../keywordSearch/?keyword=ireland&source=673">ireland</a>, <a href="../keywordSearch/?keyword=rossport&source=673">rossport</a>, <a href="../keywordSearch/?keyword=shell&source=673">shell</a>, <a href="../keywordSearch/?keyword=shell+to+sea&source=673">shell to sea</a>, <span style='color:#777777; font-size:10px'>wille corduff</span></div>
<?php echo showComments(302, 9); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY</h3>

<p>
Now that Dubya&rsquo;s a distant memory and clean livin&rsquo; paragon of virtue Barack Obama is in the White House &ndash; some of the neo-con rhetoric is obviously starting to feel a little stale in the mouth. The War on Terror is out.  <br /> 
     <br /> 
    A recent leaked memo circulating around the White House explained that the War on Terror has been officially re-branded as the &lsquo;Overseas Contingency Operation.&rsquo; Which is great, because from now on whenever an Afghan wedding party or a group of Pakistani shepherds is wiped out by a US Predator drone they didn&rsquo;t die as incidental casualties in the war on evil but as part of some &lsquo;overseas contingency,&rsquo; as if international mass murder was now just some corporate business plan that didn&rsquo;t quite hit its target.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=barack+obama&source=673">barack obama</a>, <span style='color:#777777; font-size:10px'>overseas contingency operation</span>, <a href="../keywordSearch/?keyword=war+on+terror&source=673">war on terror</a>, <a href="../keywordSearch/?keyword=war+on+terror&source=673">war on terror</a></div>
<?php echo showComments(303, 10); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS warns all readers - don&#8217;t act normal, you&#8217;ll only look suspicious. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

