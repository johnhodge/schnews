<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
require_once '../functions/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 781 - 29th July 2011 - And Finally</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="../feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

	<?php require "../inc/leftBarMain.php"; ?>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

<div style='text-align: right; width: 75%; margin-left: 25%; padding-bottom: 5px; font-weight: bold; border-bottom: 1px solid #bbbbbb' >
	<a href='news786.php'>Issue 786</a> - 
	<span style='font-size: 15px'>Special Update</span> - 
	5th September 2011</div>

<h3>Tower to the People</h3>

<p>It was going to be the 'Big One', the EDL's grand day out in the capital city. They proudly proclaimed that they were going into the "Lion's Den" - "Islamic hell-hole", and what they no doubt had in mind when this hare-brained scheme was forged was something like an Orange march through London's biggest Muslim community, with drums beating and the chant of 'E.E.EDL' echoing off the walls of the East London Mosque as terrified Muslims fled. What actually happened was a half-arsed piss-up miles from Tower Hamlets. They predicted thousands would attend but only attracted hundreds. EDL Facebook posters have been reduced to claiming that Aldgate is by some spurious definition 'in Tower Hamlets' which is the equivalent of trying to pass off a Calais booze cruise as a seaborne invasion of the European mainland.

<p>The Home Secretary intervened early in proceedings and banned marches for thirty days in the six surrounding boroughs. This is old hat to the EDL, who until recently always had their marches banned and have resorted to 'static demonstrations' in car parks around the country. These static demos basically revolve around drunken EDLers trying with varying degrees of success to break out of their police cordon and go on the rampage.

<p>The ban seemed to come as bit of a surprise to the Unite against Fascism lot though, who'd planned a march from Weaver's Field around Tower Hamlets. To their credit, despite calls from the authorities for anti-fascists to stay at home now that a ban had been implemented, they re-located their protest to within 200 yards of the East London Mosque (the EDLs stated target), meaning that there was at least a strong presence on Whitechapel High St.

<p><b style='font-size: 15px'>EDL.O.L</b>

<p>In the run-up to the event it was clear that the EDL leadership were getting panicky about numbers, posting on their official Facebook page, "All the talking has been done leading up to today. Lets now see who walks the walk into tower hamlets for the ones who have been on the walls for weeks now saying cant wait for this one. well if you dont show you should hang your heads in shame." [sic]. This seems to be a perennial problem for the League; they have thousands of wifi-warriors and facebook friends, and for a demo in a safe zone like Luton they can pull in maybe three thousand on the streets - however anything more adventurous than that and their numbers start dropping into the hundreds. In fact Saturday's demo was the same sort of size as their last 'Big One' in Bradford (see <a href='../archive/news737.php'>SchNEWS 737</a>)

<p>From the start of the day it was clear the cops had mobilised huge numbers to keep the two sides apart. The recent riots have left the Met with no wish to take chances. Their plan was to corral the EDL at Liverpool St Station and march them to within shouting distance of Tower Hamlets. But it was the Railways Union (RMT) who pulled off the biggest game-changer of the day. The rail workers refused to allow the EDL to travel by train, forcing them to change their plans repeatedly at short notice. Eventually they were forced to gather at King's Cross on totally the wrong side of the city. A representative of the RMT had this to say: "The fascist EDL will be in London tomorrow morning. I have informed management that if they appear on any of our stations or trains, then there will be refusals to work on safety grounds of serious and imminent danger, and stations will close. The RMT will not allow staff and the public to be put in danger by these thugs, who assaulted people on their last demo."

<p>When smaller groups of EDLers tried to get around on trains, some resourceful fellow travellers stopped them in their tracks by pushing the emergency stop buttons on the trains. Eventually cops somehow got one train running and by 3pm had got the EDL to Moorgate tube. Spotters reported the crowd to be around the eight hundred mark. For reasons best known to himself, EDL leader Tommy Robinson aka Stephen Yaxley-Lennon turned up to rally the drunken overweight patriots dressed as an Orthodox Rabbi (sadly not a rabbit as reported on Twitter). The EDL leader broke his bail to be there, so a disguise made sense, but mocking the religion that they've been trying to court is the sort of boneheaded stupidity that marks the nationalist mob.

<p>Meanwhile the anti-fascist presence in the East End was building up. With police concentrating on creating a sterile zone around the EDLs protest site, numbers were gathering at the junction of Commercial Rd and Whitechapel High St from midday onwards (for geography buffs this is actually the boundary of Tower Hamlets). The way ahead was totally blocked and small groups that went scouting for routes into the EDL pen were soon turned back. Eventually the bulk of the UAF counter demo made its way down to the junction to stand with the local youth and the non-aligned anti-fascists, numbering over 2,000. An hour passed scuffling with the police (with a few de-arrests) and waiting for an EDL break-out that never came. Most only saw a few George Cross flags fluttering in the distance. Eventually even the flags retreated and the day seemed to be over - a slightly anti-climactic no score draw. Some groups of non-aligned anti-fash set off in the hope of encountering the league but were turned back by cops.

<p><b style='font-size: 15px'>DOUBLE-DECKED</b>

<p>However by blind-chance (or possibly an over-reliance on Sat-Nav) locals were given the chance to go 1-0 in injury time. One EDL coach tried to make it past the East London Mosque, with predictable results. Check out this thoroughly amusing video <a href='http://www.youtube.com/watch?v=AP9DTdIDn5A' target='_BLANK'>http://www.youtube.com/watch?v=AP9DTdIDn5A</a>. The coach driver eventually sped off with hundreds of local youth and anti-fascists giving chase. Police vehicles raced alongside trying to head off the crowds and prevent a lynching. The coach was ambushed again in Mile End with rubble being hurled off a footbridge. Abandoning ship, the leaguers turned to the police for protection and were promptly nicked for violent disorder. Unconfirmed reports of attacks on other coaches are circulating.

<p>Where now for the EDL? A dismal trudge around the country defending regional car-parks from the Islamic Caliphate? There are now splits aplenty as Tommy Robinson sets himself up as an unelected Fuhrer - with others being chased out of the movement or leaving to set up rival 'Infidels' groups (North West, North East and counting). For more excellent analysis albeit with a slightly contrived Scottish accent check out <a href='http://malatesta32.wordpress.com/' target='_BLANK'>http://malatesta32.wordpress.com/</a>. With the war in Afghanistan drawing to a close and Al-Qaeda on the back foot all over the Arab world their whole 'raison d'etre' seems to be withering away.

<p>Meanwhile, judging by numbers on Saturday, the autonomous anti-fascist movement does seem to be finally waking from its slumber and is beginning to make its presence felt on the streets.

<p><a href='brightonantifascists.wordpress.com' target='_BLANK'>brightonantifascists.wordpress.com</a>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


