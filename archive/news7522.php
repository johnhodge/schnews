<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 752 - 17th December 2010 - Soar Point</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, environment, ratcliffe-on-soar" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 752 Articles: </b>
<p><a href="../archive/news7521.php">The Well Unfair State</a></p>

<b>
<p><a href="../archive/news7522.php">Soar Point</a></p>
</b>

<p><a href="../archive/news7523.php">No White Xmas After All</a></p>

<p><a href="../archive/news7524.php">Another Smashing Week</a></p>

<p><a href="../archive/news7525.php">Wiki-keeper Bailed</a></p>

<p><a href="../archive/news7526.php">Gaza Vigil</a></p>

<p><a href="../archive/news7527.php">A Cocktail Of Two Cities</a></p>

<p><a href="../archive/news7528.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th December 2010 | Issue 752</b></p>

<p><a href="news752.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SOAR POINT</h3>

<p>
<p>
	<strong>RATCLIFFE POWER STATION GUILTY VERDICT</strong> </p>
<p>
	<span style="font-family:times new roman,times,serif;">&ldquo;<em>Taking action on climate change is not an act of moral righteousness, but of self-defence. History is full of ordinary people who have acted to protect their fundamental rights and we need a strong movement of people doing just that. We want to reiterate our support for everyone fighting for climate justice</em>.&rdquo; - <strong>Ratcliffe Defendants statement.</strong></span> </p>
<p>
	This week, as world leaders predictably failed to come up with a global carbon emissions policy in Cancứn there was a disappointing result for twenty climate campaigners in Nottingham Crown Court. The twenty defendants were charged with &lsquo;conspiracy to aggravated tresspass&rsquo; after a mass arrest in April 2009 (see <a href='../archive/news672.htm'>SchNEWS 672</a>) prevented an action taking place against Ratcliffe-on-Soar power station. After three days of deliberation the jury in their case returned a unanimous guilty verdict. </p>
<p>
	Altogether 114 activists were arrested in the raid on the school. Most later had their charges dropped. Of the twenty six who went to trial, twenty decided to run a &lsquo;necessity&rsquo; defence, admitting that they planned the action but maintaining that their main purpose was to prevent the emission of 150,000 tonnes of CO2 during the week that Ratcliffe was to be shut down. One team of protesters would have pressed the emergency stop buttons on the coal conveyors which feed the boilers, while another team would have climbed the inside of the chimney before abseiling into the flues to prevent the plant re-opening for a week. Six others who are due to stand trial in January are maintaining that they were not part of the plot. </p>
<p>
	The occupation was meticulously planned and was intended to last for a week. However, during the prosecution case it was revealed that police knew about the plan at least five days in advance. In the end the prosecution only produced one witness - Raymond Smith, the power station manager. Apparently, according to a source close to the trial, two senior police were due to give evidence but were pulled at the last minute. Its unknown whether this was to avoid awkward questions about police intelligence gathering. (Undercover cop Mark Stone / Kennedy &ndash; see <a href='../archive/news745.htm'>SchNEWS 745</a> - was present when the school was raided.) </p>
<p>
	Ratcliffe-on-Soar, owned by German energy giant E.ON, has been the repeated target of environmental activists. In 2007, activists from Eastside Climate Action succeeded in infiltrating the plant (see <a href='../archive/news583.htm'>SchNEWS 583</a>). In October 2009 , a thousand activists converged on the site in the Great Cliamte Swoop (see <a href='../archive/news696.htm'>SchNEWS 696</a>). As a coal-burning power station it kicks around ten million tonnes of CO2 into the atmosphere every year. While he was on the stand the station manager admitted that the station burned coal rather than cleaner gas solely to make profit. </p>
<p>
	As both the defence and the prosecution were in agreement that a conspiracy to shut the power station down existed, the defence case rested entirely on the necessity of preventing emissions to save lives. The defence were allowed to present an impressive array of witnesses, including James Hansen (a NASA climate scientist) and Caroline Lucas (Green M.P for Brighton) When commenting on the motives for the defendants, James Hansen said , &ldquo;It doesn&rsquo;t surprise me that young people are angry when they know that politicians are lying to them. </p>
<p>
	The prosecution countered these arguments with a variety of bizarre celebrity-fixated schemes. Wouldn&rsquo;t it be better, their arguments went, that the 15 grand action budget been better spent on buying Cheryl Cole a wardrobe of second-hand clothes or flying David Beckham to the Cancun Climate Summit? (What dizzying logic). The prosecutor even felt the need to tell the jury that she had a compost toilet herself, before being informed by the judge that her personal life really didn&rsquo;t have any bearing on the case. </p>
<p>
	In fact given that the case went so swimmingly well until the verdict - what went wrong? </p>
<p>
	SchNEWS spoke to one defendant who said, &ldquo;<em>The whole thing has been an amazing experience, the plan went wrong but it was worth a shot. Of course there&rsquo;s no way of looking into the mind of the jury but I wish one of us at least had done without a professional defence. If we&rsquo;d run the defence in person then we&rsquo;d have been able to probe harder and get away with more than the barristers could. But people are learning from it.</em>&rdquo; </p>
<p>
	Sentencing has been adjourned until January 6th. The trial of the six who deny their involvement begins on the 10th. </p>
<p>
	* For a full run-down on everything that happened see <a href="http://www.ratcliffeontrial.org" target="_blank">www.ratcliffeontrial.org</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1041), 121); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1041);

				if (SHOWMESSAGES)	{

						addMessage(1041);
						echo getMessages(1041);
						echo showAddMessage(1041);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


