<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 730 - 9th July 2010 - The Hole Truth</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, oil, direct action, protest camp, ireland, rossport" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 730 Articles: </b>
<b>
<p><a href="../archive/news7301.php">The Hole Truth</a></p>
</b>

<p><a href="../archive/news7302.php">Decommissioners All Out</a></p>

<p><a href="../archive/news7303.php">Bog Roll Of Honour</a></p>

<p><a href="../archive/news7304.php">Village Fate</a></p>

<p><a href="../archive/news7305.php">Uzbeks Against The Wall</a></p>

<p><a href="../archive/news7306.php">Besetting A President</a></p>

<p><a href="../archive/news7307.php">Raven's Law</a></p>

<p><a href="../archive/news7308.php">Gettin On Their Wiki</a></p>

<p><a href="../archive/news7309.php">The Paper Trail</a></p>

<p><a href="../archive/news73010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th July 2010 | Issue 730</b></p>

<p><a href="news730.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE HOLE TRUTH</h3>

<p>
<p>
	<strong>AS SCHNEWS REPORTS FROM THE NEW ROSSPORT FRONTLINE..</strong> </p>
<p>
	There is still plenty of ire left in Ireland as campaigners ready themselves for another summer of action against Shell and their plans to despoil the coast of County Mayo with a new gas pipeline (see <a href='../archive/news719.htm'>SchNEWS 719</a>). The project is already a decade late and three times over budget; pretty impressive for a small community fighting one of the biggest multinationals in the world. <br /> 
	 <br /> 
	Previous years&rsquo; resistance and obstruction &ndash; whilst met with intimidation, violence, police and legal harassment - have finally led to a rethink at Shell HQ. Their cunning plan is now to avoid going through the troublesome Rossport area and instead build underneath the nearby Sruth Fhada Chonn estuary instead. <br /> 
	 <br /> 
	Preparation for this involves drilling up to 80 boreholes to assess suitability, with work due to start this month. The site is a Specially Protected Area (and what self-respecting energy corporation would build anywhere else?), part of the Broadhaven Bay Special Area of Conservation, and the operation will damage parts of the estuary, disturbing the local wildlife, particularly Atlantic salmon and birds found on the intertidal areas. <br /> 
	 <br /> 
	But, as they have every step of the way, local campaigners and Shell to Sea activists are planning to stop them (without any extra disturbing of wildlife, presumably). They are calling for a &ldquo;continuous mass act of civil disobedience&rdquo; and want groups or individuals to pledge to stop a borehole at some time this summer. <br /> 
	 <br /> 
	The aim is to assemble a rag-tag bag of &lsquo;Shell&rsquo;s angels&rsquo; and assign each borehole it&rsquo;s own &lsquo;Beat the Borehole&rsquo; group. Actions could range from merely walking out on the sands for picnic to full-on boarding of drilling rigs. </p>
<p>
	Due to the tides, and seasonal nature of the job, any disruption that slows the process down could prevent them getting an adequate survey done this year. Work has to stop in mid-Oct when the protected Brent Geese arrive for the winter. </p>
<p>
	The Solidarity Camp is now located on land right next to the estuary. Everyone is welcome to stay there and equipment / training can be provided. There is also a camp house and local hostel nearby if camping isn&rsquo;t for your (sleeping) bag. </p>
<p>
	* To pledge to Beat a Borehole email/ring the camp Ph: +353 851141170 Email - <a href="mailto:rossportsolidaritycamp@gmail.com">rossportsolidaritycamp@gmail.com</a> </p>
<p>
	** See also <a href="http://www.rossportsolidaritycamp.org" target="_blank">www.rossportsolidaritycamp.org</a> ww.shelltosea.com </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(849), 99); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(849);

				if (SHOWMESSAGES)	{

						addMessage(849);
						echo getMessages(849);
						echo showAddMessage(849);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


