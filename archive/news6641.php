<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 664 - 30th January 2009 - Mner Mner...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, argentina, buenos aires, indugraf, workers struggles, squatting, financial crisis, workers co-operative" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 664 Articles: </b>
<b>
<p><a href="../archive/news6641.php">Mner Mner...</a></p>
</b>

<p><a href="../archive/news6642.php">Iceland Meltdown</a></p>

<p><a href="../archive/news6643.php">Bolivian It Up</a></p>

<p><a href="../archive/news6644.php">Inside Guantanamo</a></p>

<p><a href="../archive/news6645.php">Exclusive: Omar Deghayes Speaks To Schnews</a></p>

<p><a href="../archive/news6646.php">Big It Up  </a></p>

<p><a href="../archive/news6647.php">Outfoxed</a></p>

<p><a href="../archive/news6648.php">Upping The Auntie  </a></p>

<p><a href="../archive/news6649.php">Bayer 'eck</a></p>

<p><a href="../archive/news66410.php">Tas-mania</a></p>

<p><a href="../archive/news66411.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/664-wham-obama-lg.jpg" target="_blank">
													<img src="../images/664-wham-obama-sm.jpg" alt="What me, bomb anybody..?!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 30th January 2009 | Issue 664</b></p>

<p><a href="news664.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>MNER MNER...</H3>

<p>
Last November, the 88 workers of Indugraf - a graphics printing company in Buenos Aires, Argentina - turned up to work only to find the workshop gates locked. A small sign informed them that they would receive telegrams with further information. The management arrived soon after to inform them that they&#8217;d been fired. <br />
 <br />
The workers reconvened outside the gates on December 9th, having been told to expect more news on this date, with many showing up in their work clothes expecting to resume their positions. With the gates still locked and no sign of the owners, the workers held an assembly to decide their next move. The next day (10th) they took over the factory. <br />
 <br />
In the time leading up to the seizure the workers had been paid irregularly if at all and were owed four months pay. What&#8217;s more they had seen a considerable decline in their working conditions. With the closure it also became apparent that the company had not been paying the workers&#8217; pension, union and social security contributions for two years, despite subtracting them from wages. The owners - Carlos Martinez - meanwhile had a list of creditors that they&#8217;d been avoiding for some time. Despite this, the workers say that there had been no appreciable downturn in production and they&#8217;d been as busy as ever. <br />
 <br />
Under the name of the 10th of December cooperative, the group set up in the workshop and, aware of their precarious legal position, went straight to the Ministry of Work to lodge their claim. The Ministry recognised both their complaints and their claim but has yet to take any action at all. <br />
Not so the Justice Department however. By December 30th they had already issued an eviction order giving the workers until the 25th of January to clear out. The workers however, were already camped inside the premises to prevent any action to reclaim it by the owners or the police.  <br />
 <br />
Accompanied by more than 50 supporters, mostly neighbourhood youths, and hooked up to water and electricity by a neighbour, they spent Christmas and New Year in the workshop. Since then they&#8217;ve been holding regular marches, assemblies and cultural events to whip up support. <br />
 <br />
After instigating eviction proceedings, the owners reported that the cooperative was looting the building and the group soon started suffering police harassment. An assembly of 400 people in front of the building following a march was met by 200 cops and then, on January 22nd, the police raided the premises. They issued a notice that only the seven people they found inside would be permitted to enter the building and posted a guard on the door. Since then the remaining workers have been camped outside. <br />
 <br />
Fortunately the 10th of December are not on their own in this struggle. Following the economic crisis of 2001, a huge workers-co-op movement sprang up in Argentina (see <a href="../archive/news350.htm">SchNEWS 350</a> &#8211; Taking The Peso) and, despite facing enormous problems, it is still going strong. Employing some 13,000 people, there are currently around 150 re-occupied workplaces, from balloon factories to health clinics. <br />
 <br />
Many, like the iconic BAUEN Hotel which reopened with 30 workers and now employs more than 160, have grown into profitable businesses. Although some have slid into more traditional hierarchical management structures, most still operate on their original principles - equal pay for all and all-important decisions taken by one-person-one-vote assemblies. More than that, the co-ops have been reinvesting in the community. A number of them use their buildings as community and cultural centres while co-ops have also built a health clinics in neighbouring barrios. Two of the largest, Zanon and BAUEN, put on regular community rock concerts and theatre productions. <br />
 <br />
Since their formation the co-ops have formed a broad solidarity network based on mutual aid and cooperation among recuperated businesses and other worker organisations. They have organised under the name MNER - the Movimiento Nacional de Empresas Recuperadas (national movement of recuperated businesses), an organisation which is providing support and legal advice to the 10th of December co-op. MNER has also established a fund to help starting co-ops. <br />
 <br />
The two-sided response to the 10th of December from the authorities &#8211; considering their claim on one hand while issuing eviction orders on the other &#8211; is typical of the deep ambivalence the government has shown towards the movement since its inception. While refusing to provide any sort of practical help, like low-interest loans or subsidies, they have, for the most part, not been shutting the co-ops down. Instead they&#8217;ve left most of them in a precarious legal limbo. <br />
 <br />
Despite a campaign from MNER there has been no &#8216;national law of expropriation&#8217; and each co-op has its legal status examined individually. Some have been granted permanent rights to the business &#8211; full expropriation - while some have no legal status at all. The majority however have temporary rights, which usually last for between two and five years but can be changed at any moment. A number of businesses, including the BAUEN, have faced down eviction notices. <br />
 <br />
So far the 10th of December have avoided eviction, despite the passing of the notice deadline. The new co-op has a lot going against it; they are being delayed legally as Martinez has still not held a creditors meeting or filed for bankruptcy, while on the ground they are being prevented from getting the company up and running by problems with the machines, none of which work after apparently being sabotaged by the owners. But, on the other hand they have community backing and the support and experience of 150 worker-run businesses behind them. <br />
 <br />
With banks and businesses failing left right and centre back here in Britain - with more to come as the Credit Crunch becomes the Free Market Freefall, perhaps the Argentinians are offering an instructive model for exploited workers here to seize the reigns of power and take the con out of the economy... <br />
 <br />
See <a href="http://argentina.indymedia.org" target="_blank">http://argentina.indymedia.org</a> <br />
 <a href="<br />
http://www.zmag.org/znet/viewArticle/4145" target="_blank"><br />
http://www.zmag.org/znet/viewArticle/4145</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=squatting&source=664">squatting</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(204);
			
				if (SHOWMESSAGES)	{
				
						addMessage(204);
						echo getMessages(204);
						echo showAddMessage(204);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>