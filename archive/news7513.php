<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 751 - 10th December 2010 - A Hellas of a Time</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, riots, police, general strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 751 Articles: </b>
<p><a href="../archive/news7511.php">For Whom The Fee Tolls</a></p>

<p><a href="../archive/news7512.php">Anti-tax Dodgers High Street Bonanza</a></p>

<b>
<p><a href="../archive/news7513.php">A Hellas Of A Time</a></p>
</b>

<p><a href="../archive/news7514.php">Wikileaks: Hacktion Stations</a></p>

<p><a href="../archive/news7515.php">Schews In Brief</a></p>

<p><a href="../archive/news7516.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th December 2010 | Issue 751</b></p>

<p><a href="news751.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A HELLAS OF A TIME</h3>

<p>
<p>
	Two years since the death of Alexis, Athens is once again in flames. While demonstrations and actions kick off in 17 cities across Greece, solidarity demonstrations take place around Europe. </p>
<p>
	15-year-old Alexandros Grigoropoulos was shot dead by police in the Exarcheia district of Athens on 6th December 2008, igniting months of riots and a popular uprising. After another uprising last year, a new wave of riots looks likely. (see <a href='../archive/news659.htm'>SchNEWS 659</a>). </p>
<p>
	On the anniversary of the assasination, clashes with police began even before the main demonstration - which police effectively kick-started by charging and hurling teargas at the amassing crowd. Following an earlier demonstration of around 3,000 students and teachers, several thousand people took to the streets for several hours, many armed with sticks, molotovs and gas masks. Those without relied on scarves or paint masks; scant protection against the clouds of teargas that followed the crowd. Paving slabs were ripped up, broken and thrown at windows of banks and shops as well as police. Rubbish bins, already overflowing after a series of collection strikes, were set alight. Many a cop was seen running around on fire. </p>
<p>
	At around 10pm, police launched incendiary attacks on the congregation taking place on the spot were Alexis was killed, shoving people away from the shrine. In the end police had to use tear gas and stun grenades to disperse the crowd. Over 96 people were detained in Athens leading to 42 arrests. The next anarchist demonstration in central Athens has been called for December 11th, while the next general strike is on the 15th. </p>
<p>
	This past week has also seen a huge crackdown by the authorities in all major Greek cities with searches and arrests. Anti-terrorist units have been ruthless on squats and the &lsquo;Nadir&rsquo; in Thessaloniki was raided on Saturday (4th). Eye witnesses report that riot police moved in after the initial terror unit had driven off, forcibly dragging people out and arresting eight . The 150 onlookers outside became a solidarity vigil. At least three houses were raided in Athens during the weekend and another one in the western city of Agrinio. Ten people have been detained and of these at least seven people will be arrested and charged. On Sunday (5th), the night before the demonstration, there was an unconventional 21-hour traffic ban in central Athens and a colossal police presence in Exarcheia (Athens&rsquo; anti-authoritarian district) where four people were allegedly detained. </p>
<p>
	And facing time in Greek jail is not pleasant. As of Monday (6th) more than 1,000 prisoners across the country have gone on hunger strike, with more than 9,000 others - three-quarters of the entire prison population &ndash; also abstaining from prison meals (relying on privately supplied food in prisons with no food shops: an effective hunger strike). Demands include the de-congestion of prison buildings, overall improvement of detention conditions, true justice and shorter imprisonment times. </p>
<p>
	Monday, December 13th has been called as a day of action and demonstrations will take place across the country. </p>
<p>
	* Info fresh from the mean Greek Streets at <a href="http://www.occupiedlondon.org/blog" target="_blank">www.occupiedlondon.org/blog</a> and <a href="http://athens.indymedia.org/?lang=en" target="_blank">http://athens.indymedia.org/?lang=en</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1036), 120); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1036);

				if (SHOWMESSAGES)	{

						addMessage(1036);
						echo getMessages(1036);
						echo showAddMessage(1036);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


