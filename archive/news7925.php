<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 792 - 14th October 2011 - A Healthy Turnout</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 792 Articles: </b>
<p><a href="../archive/news7921.php">False Profits</a></p>

<p><a href="../archive/news7922.php">Greece Tightening</a></p>

<p><a href="../archive/news7923.php">Honduran-ce Test</a></p>

<p><a href="../archive/news7924.php">Far Right Angels</a></p>

<b>
<p><a href="../archive/news7925.php">A Healthy Turnout</a></p>
</b>

<p><a href="../archive/news7926.php">Dale Farm Update</a></p>

<p><a href="../archive/news7927.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th October 2011 | Issue 792</b></p>

<p><a href="news792.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A HEALTHY TURNOUT</h3>

<p>
<p>  
  	<strong><em>&ldquo;The NHS will last as long as there are folk left with the faith to fight for it.&rdquo;</em> - Nye Bevan.</strong>  </p>  
   <p>  
  	Somewhere between 3,000-5,000 people came to &ldquo;Block the Bridge, Block the Bill&rdquo; &ndash; the UK Uncut-organised demo to defend the NHS from axe-wielding Tory maniacs (see <a href='../archive/news791.htm'>SchNEWS 791</a>). Billed as an up-fer-it direct action blockade of Westminster Bridge (in the very heart of the city for those who&rsquo;s knowledge of central London&rsquo;s geography is lacking), the location couldn&rsquo;t have been better chosen. On one side of the bridge is Parliament, on the other St Thomas&rsquo; Hospital. Protesters symbolically placed themselves in the way of the hospital to protect it from the harm that the passage of the Health and Social Care Bill will do to it and the rest of the NHS.  </p>  
   <p>  
  	By 1pm the entire bridge was closed to traffic and full of demonstrators of many persuasions; health workers, disabled people, students, trades unionists and more gathered to defend Britain&rsquo;s most popular state institution. Some of the more enthusiastic attendees took &ldquo;Block the Bridge&rdquo; a little bit too seriously and linked arms, preventing protesters and non-protesters alike from crossing the bridge to get to the demo because, err, the bridge was blocked. They eventually stopped when the rest of the crowd nearby started taunting them for being idiots.  </p>  
   <p>  
  	The rest of the demo was full of creative chaos; including a die-in by people dressed in medical gear, street theatre of all shapes and flavours and mobile sound-systems a plenty. Block the Bridge bore witness to the first &lsquo;Occupy London&rsquo; General Assembly. Directly inspired by Occupy Wall Street, the Assembly was a classic example of Brits almost, but not quite, copying the American way of doing things. In place of the exuberant &lsquo;can do&rsquo; attitude of our comrades across the pond there were hesitant speeches and suggestions that people might like to get into groups to organise for the future.  </p>  
   <p>  
  	Although the event was about as fluffy as direct action ever is, the police still couldn&rsquo;t stop their authoritarian urges from getting the better of them. After stopping anyone who looked vaguely protester-ish from leaving via the north side of the bridge, they then kettled groups of protesters on Lambeth Bridge, eventually letting people go after a round of pointless stop and searches.  </p>  
   <p>  
  	With the Wall St protests continuing, and an Arab Spring still in people&rsquo;s steps, occupation as a tactic looks like the way to go these days. Flush from the success of the Westminster Bridge demo it looks like next week&rsquo;s Occupy the Stock Exchange should be a biggie.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1391), 161); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1391);

				if (SHOWMESSAGES)	{

						addMessage(1391);
						echo getMessages(1391);
						echo showAddMessage(1391);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


