<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 665 - 6th February 2009 - Tank In Your Tiger</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tamil tigers, lasantha wickramatunga, ethnic cleansing, sri lanka" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 665 Articles: </b>
<p><a href="../archive/news6651.php">Sheik Battle & Rule</a></p>

<p><a href="../archive/news6652.php">Bye-byelaw!  </a></p>

<p><a href="../archive/news6653.php">Dirty Tricks</a></p>

<p><a href="../archive/news6654.php">Fitted Up</a></p>

<b>
<p><a href="../archive/news6655.php">Tank In Your Tiger</a></p>
</b>

<p><a href="../archive/news6656.php">Wildcat Flap</a></p>

<p><a href="../archive/news6657.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 6th February 2009 | Issue 665</b></p>

<p><a href="news665.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>TANK IN YOUR TIGER</H3>

<p>
Early last January, as the Sri Lankan army pushed into the remaining enclaves of territory held by separatist rebels the Liberation Tigers of Tamil Eelam, AKA the Tamil Tigers, two gunmen on a motorbike shot dead Lasantha Wickramatunga, editor of anti-establishment Sri Lankan newspaper The Sunday Leader. The following Sunday the Leader published his posthumous piece &#8216;And then they came for me.&#8217; He blamed the government for his murder and condemned them for abuses and corruption. He also bleakly stated that while the Sri Lankan authorities might win a military victory against the Tigers it would be ultimately self-defeating. He warned that &#8220;<i>&#8230;a military occupation of the country&#8217;s north and east will require the Tamil people of those regions to live eternally as second-class citizens, deprived of all self respect&#8230; The wounds of war will scar them forever, and you will also have an even more bitter and hateful Diaspora to contend with. A problem amenable to a political solution will thus become a festering wound that will yield strife for all eternity.</i>&#8221; <br />
 <br />
That military victory looks increasingly likely. In the last two months the Tigers have been driven out of most of their former territory, including the de facto capital, Kilinochchi. They&#8217;ve been pushed back into a 115-square-mile area on the coast, surrounded by army and navy. <br />
 <br />
Also trapped are around 250,000 civillians. The Tigers have stopped many from fleeing, forcing them deeper into their remaining territory. Those that have escaped, including whole families, have been held in miltary detainment camps. The government has said it wants them to live in &#8216;model villages&#8217;- heavily policed camps, instead of returning home. The government &#8216;saftey zone&#8217; inside the territory  has been shelled regularly. <br />
 <br />
It is nearly impossible to get accurate reports on the number of causalities from the area. Journalists and aid agencies have been banned while back in the Capital, Colombo, the media has been mostly cowed into silence by the Wickramatunga&#8217;s  assassination and a spate of other recent attacks. There have been estimates of around 400 deaths, based on health authorities&#8217; and UN reports. <br />
 <br />
It&#8217;s a familiar position for Tamil civilians. Since 1983 when the Tigers began their campaign in reaction to anti-Tamil discrimination, civilians have suffered constant abuse from both sides. Thousands of Tamils have &#8216;disappeared&#8217; over the years while even more have been incarcerated under the draconian Prevention of Terrorism act. The Tigers meanwhile ruled through fear, denying basic freedoms of expression, association, assembly and movement, assassinating Tamil opponents and forcing children to join their ranks. <br />
 <br />
Wickramatunga&#8217;s comments regarding the diaspora are also already proving prescient. Last Saturday (31st) between 50,000 and 100,000 Tamils, maybe half of the British Tamil population,  turned out in London, to protest the killings and abuses and demand a Tamil homeland, Tamil Eelam. With the Sri Lankan army continuing to kill and mistreat Tamils, more and more will see the Tigers as the only alternative to the violence and discrimination of the authorities.  <br />
 <br />
Despite the difficulty in obtaining reliable reports from the area it does seem clear that the Tigers days as a major military force are numbered. But the pioneers of suicide bombing seem unlikely to just give up and go away. As one Sri Lankan journalist who was too scared to reveal his name put it, &#8220;<i>The LTTE are a guerrilla outfit, they&#8217;ll melt away and will come back with terrorism. It&#8217;s not over. It&#8217;s probably just begun.</i>&#8221;
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777'>tamil tigers</span>, <span style='color:#777777'>lasantha wickramatunga</span>, <span style='color:#777777'>ethnic cleansing</span>, <span style='color:#777777'>sri lanka</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(219);
			
				if (SHOWMESSAGES)	{
				
						addMessage(219);
						echo getMessages(219);
						echo showAddMessage(219);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>