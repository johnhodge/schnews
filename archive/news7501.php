<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 750 - 2nd December 2010 - Fee Foes' Fine Fun</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, students, cuts, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 750 Articles: </b>
<b>
<p><a href="../archive/news7501.php">Fee Foes' Fine Fun</a></p>
</b>

<p><a href="../archive/news7502.php">Schnews 750: State Of The Indignation</a></p>

<p><a href="../archive/news7503.php">It's A Zad Day When...</a></p>

<p><a href="../archive/news7504.php">The Italian Jobless</a></p>

<p><a href="../archive/news7505.php">Ratcliffe 2o Trial Begins</a></p>

<p><a href="../archive/news7506.php">Town Hall Meeting</a></p>

<p><a href="../archive/news7507.php">Phil Yer Boots</a></p>

<p><a href="../archive/news7508.php">Edl Double Bill</a></p>

<p><a href="../archive/news7509.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 2nd December 2010 | Issue 750</b></p>

<p><a href="news750.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FEE FOES' FINE FUN</h3>

<p>
<p>
	<strong>STUDENT REBELLION CONTINUES WITH DAY OF ACTION AND OCCUPATIONS</strong> </p>
<p>
	Day X2, the second national day of action against education reforms, saw a whirlwind of protest around the country. From teach-ins to storming the town hall, walk-outs to battling police; students, school children and anti-cuts activists ensured the momentum of the student rebellion continues to build. </p>
<p>
	Crowds of over a thousand marched through London, Manchester, Bristol and Brighton while hundreds more took to the streets everywhere from Oxford to Leeds. In all, over 40 places were hit by protests. Targets were varied - in Sheffield students tried to take their protest to Nick Clegg&rsquo;s suspiciously empty constituency office, in Birmingham demonstrators stormed the council chambers, while across the country symbols of casino capitalism and privateering such as Vodafone bore the brunt of student anger. The message however, was clear. These kids know what sort of country the coalition is trying to build and they aren&rsquo;t going to stand idly by while it happens. </p>
<p>
	The protests weren&rsquo;t just confined to University students. Thousands of school children walked out of class to join the demonstrations in defiance of school threats to punish them. </p>
<p>
	<strong>OCCUPY, RESIST, PRODUCE</strong> </p>
<p>
	The last week has also seen the wave of student occupations gather pace, with the count currently standing at 19. Some are already into their second week, while the latest came yesterday (2nd) as students at the University of East Anglia rushed the university council building. With it too cold to occupy on their arses, students have been busy using their reclaimed spaces for an alternative education programme of workshops, talks, public meetings and planning actions. Many of the occupations retain enough control of the buildings to allow people to come and go, with supporters arriving all the time to boost numbers. A number have also ensured that normal lectures can continue throughout the occupations to ensure minimal disruption to those who still see universities as places to get an education. Even simpering wannabe Prime Minister NUS president Aaron Porter finally threw his weight behind the campaign, not that many cared. </p>
<p>
	Most attempts at negotiation with the educational powers-that-be have failed after the authorities either refused to meet or met only to dismiss the students as a rowdy minority. These claims that the occupations are a radical fringe without widespread support are becoming ever harder to sustain as support for the occupiers has poured in from all quarters, ranging from letters of support signed by UK academics to international solidarity demos in Greece. </p>
<p>
	With the frantic university establishment watching its legitimacy seep away, the retaliation has begun. Three of the occupations, Slade School of Art, Nottingham and the University of Central London (UCL) have received eviction notices. At Nottingham and UCL the students have declared they will resist any attempts to turf them out. </p>
<p>
	<strong>MET STICK THE KETTLE ON</strong> </p>
<p>
	After being caught with their pants down at Milbank, the cops have aggressively policed the protests. With the G20 furore fading in the public&rsquo;s memory the kettle is this winter&rsquo;s must-use police tactic while in protests around the country, riot police have been quick to whip out the batons and charge. Youtube has been flooded with police video nasties showing more punches than a Manny Pacquiao fight and more horse charging than the Light Brigade. The most heavy handed policing was at the London demo, where the futility of negotiating a demo with the police was helpfully highlighted by the Met, who tried to kettle protesters at the starting line of the agreed route from Trafalgar Square to parliament. The students scattered and the next few hours were spent in a game of cats and mice as packs of cops chased the groups of protesters through the city streets. There were several clashes, many of them coming as police lashed out at students attempting to evade kettles. After most of the marchers had returned to Trafalgar Square having found Parliament Square decked out like the Green Zone, police again moved in, kettling the protest and merrily clubbing anyone who tried to break out. There also were reports of snatch squads seizing protesters inside the kettles. </p>
<p>
	Determined to get a grip on the situation the police have also stepped up their intelligence gathering campaign, employing new and ever more repressive techniques. As the students in Trafalgar Square were released from the kettle, they were interviewed on camera by FIT officers who told them they were under arrest for breach of the peace and had to give their details. After doing so, some were bundled into vans while others were released. Over 150 arrests were made. </p>
<p>
	Despite the police aggression, crowds have generally refused to be passive and controlled and have frequently stood up to the police assault. In Bristol a crowd of around 1000 lit flares and hurled mustard at police while in Brighton, attempts to kettle the main march close to its target - Hove Town Hall - were short lived as the crowd hurled missiles and pushed back against the lines. Police vans and cars were also attacked during the protest. </p>
<p>
	The movement shows no sign of abating and the next nationwide day of all out action is scheduled for Thursday the 8th of December - the day before the parliamentary vote on fees. A rally is also scheduled for the day of the vote itself. A weekend of action aiming to united the student rebellion with the wider anti-cuts movement has been called for this weekend (4th and 5th). Protests against cuts, tax dodgers and the decimation of education have been planned across the country. See <a href="http://indymedia.org.uk/en/2010/11/469072.html" target="_blank">http://indymedia.org.uk/en/2010/11/469072.html</a> for details. </p>
<p>
	*For a list of occupations and links to their websites, see: <a href="http://edinunianticuts.wordpress.com/other-occupations/who" target="_blank">http://edinunianticuts.wordpress.com/other-occupations/who</a> tried to break out. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1025), 119); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1025);

				if (SHOWMESSAGES)	{

						addMessage(1025);
						echo getMessages(1025);
						echo showAddMessage(1025);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


