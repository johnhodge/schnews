<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 675 - 8th May 2009 - Brighton Rocked</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, brighton, may day, arms trade, anti-capitalism, direct action, fitwatch, barclays, mc donalds, thales, royal bank of scotland, anti-military gathering" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 675 Articles: </b>
<b>
<p><a href="../archive/news6751.php">Brighton Rocked</a></p>
</b>

<p><a href="../archive/news6752.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6753.php">May Day Media Watch</a></p>

<p><a href="../archive/news6754.php">Sick Note</a></p>

<p><a href="../archive/news6755.php">Going Cheap On Amazon</a></p>

<p><a href="../archive/news6756.php">In Beds With The Enemy</a></p>

<p><a href="../archive/news6757.php">Coal Caravan  </a></p>

<p><a href="../archive/news6758.php">Droning On</a></p>

<p><a href="../archive/news6759.php">Raven'saint No More</a></p>

<p><a href="../archive/news67510.php">Shells Bells</a></p>

<p><a href="../archive/news67511.php">Nine Lives</a></p>

<p><a href="../archive/news67512.php">Roller Balls</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/675-riot-porn-monthly-lg.jpg" target="_blank">
													<img src="../images/675-riot-porn-monthly-sm.jpg" alt="OUT NOW - Riot Porn Monthly" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 8th May 2009 | Issue 675</b></p>

<p><a href="news675.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRIGHTON ROCKED</h3>

<p>
<p><strong>As Smash EDO May Day Mayhem Hits The Streets Of Brighton...</strong> <br />  <br /> Brighton Festival got off to an unorthodox but flying start start on Monday as over 1,000 people turned out to take a stand against War, Capitalism and Greed. At 11am cyclists started to gather at Brighton Station and half an hour later the 50-strong critical mass bike ride with two sound systems headed for the sea accompanied by two police horses, Evidence Gathering camera teams and a number of vans. <br />  <br /> Organisers of the Mayday!Mayday! Protest - <strong>Smash EDO</strong> - took a leaf out of Reclaim the Streets book keeping the party&rsquo;s location secret &lsquo;til the last minute. The signal to gather at Palace Pier roundabout was given online and via Smash EDO FM at 11:45am. By 12.30 a crowd of approximately 500 had gathered around a sound-system and were dancing. They soon saw off the assembled FIT (Forward Intelligence Team) and Evidence Gatherers. A Mayday flag was run up the lamp-post in the centre of the roundabout. On high alert following the recent discovery of the existence of police brutality, the world&rsquo;s media were also there in numbers - so police behaviour at this point was notably hands-off. <br />  <br /> At 12.45, headed by two reinforced banners carried by hard-hat and mask wearing chaoto-nihilists, the march moved off and streamed up North St. Bringing up the rear in true carnival spirit was the kids bloc in buggies, on scooters and on foot, faces painted and papier mached flowers held aloft. <br />  <br /> The first target selected from the Anti-militarist Map of Brighton(1) was Barclays. <br />  <br /> Masked figures swiftly scaled scaffolding outside the premises and unfurled a banner saying &ldquo;Arms Dealers out of Brighton&rdquo;(2). The branch was also redecorated with red paint as the crowd made a safe space for the climbers to get down without having their collars felt.&nbsp; <br />  <br /> The march was being headed by police horses and as the crowd reached the Clocktower, police thought the march was going straight on. But the march turned right, and after realising their mistake, the police horses were cantered round and raced to the front of the march sending bemused tourists and shoppers fleeing for safety.&nbsp; <br />  <br /> On Queens Rd towards Brighton Station is the Army recruitment centre. Metal shutters across the front meant that a quick re-spray was the only option, before marching on to the train station. The crowd was visibly more up for it by now - clearly angry not just about the weapons factory but by the beating and kettling handed out at the G20. <br />  <br /> For a real 90s-retro feel the crowd paused outside McDonald&rsquo;s before surging forward behind the reinforced banners. Brew cans rattled ineffectually against the windows of the golden arches, as a line of riot cops formed along the store-front. One cop-van was rocked as the masked crowd turned on it. Soon a smoke grenade was billowing red clouds from under the vehicle. Short shield police backed up by horses moved into the crowd splitting it into two. <br />  <br /> Fleeing down side-streets and arming themselves with wheelie-bins full of bottles, the crowd swiftly outwitted the police and regrouped - before a pink-fibreglass car (out of Wind in the Willows) joined in the uphill stretch along Ditchling Rd in the vague direction of EDO&rsquo;s weapons factory. The uphill struggle came to end as lines&nbsp;of riot police backed with horses started batoning peoples&rsquo; legs.&nbsp; <br />  <br /> <strong></strong><table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/675-mounted-police-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/675-mounted-police-sm.jpg"  alt='Mounted Police'  /></a></td></tr></table><strong>BREW - WHAT A SCORCHER!</strong> <br />  <br /> The crowd, having picked up a number of enthusiastic festival goers, turned downhill through the side-streets towards Preston Park (apparently the intended end-point of the demo). A heavy police presence was deployed outside RBS and the Thales arms company offices opposite the park. <br />  <br /> As the police pursued the demonstrators through the park the black clad mob encountered a group of elderly folk in their best bowling whites enjoying a quiet game of bowls on the green. This being English Anarchy, the protesters kept off the grass whilst they tussled with the police, who - British bulldog-style - tried to stop anyone crossing the park. &ldquo;<em>We&rsquo;re not playing this game any more</em>&rdquo;, one cop was overheard saying. <br />  <br /> A couple of hundred stayed in the park for a bit of a party but fears of a kettle persuaded most to keep moving. Another thrust in the direction of the factory was sidestepped on Preston Drove and following scuffles people began moving back into town. By now the march had fragmented with many deciding the sensible thing to do would be to disappear into side streets after having made a forceful show against the targets of war profiteers. <br />  <br /> What had started out as a full-on protest against war and profit had by this time turned into an impromptu drunken street party, with local youth joining various anti-authoritarian elements. It&rsquo;s a  familiar story that just as the bulk of the initial protesters are beginning to tire and disappear, further waves of people respond to the forces of law&rsquo;n&rsquo;order breaking down as an opportunity to take out their frustrations on a society they feel neglected or criminalized by. It was after this that some of the more random chaos took place: the rooftop occupation and graffitiing of the Palace Pier ice cream shop wasn&rsquo;t claimed as a bit of vegan direct action but more of a case of youthful high spirits. Still, having a load of people who&rsquo;ve never engaged in any kind of politics chanting &ldquo;Who&rsquo;s streets? Our Streets!&rdquo; while taunting the police can&rsquo;t be all bad. <br />  <br /> May Day was another successful day of action and Smash EDO goes from strength to strength despite the vindictiveness of the authorities with arrests and failed prosecutions. <br />  <br /> It spearheads a growing national anti-militarist network, whose groups have been inspired by the five-year Brighton campaign. This network was consolidated - and future actions planned - at the Anti-Militarist Gathering in Brighton the weekend before the big demo.&nbsp; <br />  <br /> (1) - Map - <a href="http://www.indymedia.org.uk/media/2009/04//427416.pdf" target="_blank">www.indymedia.org.uk/media/2009/04//427416.pdf</a> <br />  <br /> (2) -  Not &ldquo;Arms dealers out of our communities&rdquo; as incorrectly stated on yer &lsquo;make it up as we go along - day out by the sea-side&rsquo;  Indymedia &nbsp; <br />  <br /> <strong>Night Actions <br /> </strong> <br /> * In the night cheeky pixies came out into the streets of Brighton and smashed an HSBC Bank, sprayed up three Barclays cashpoints and disappeared as quickly as they came... <br />  <br />  <br /> </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-capitalism&source=675">anti-capitalism</a>, <a href="../keywordSearch/?keyword=anti-military+gathering&source=675">anti-military gathering</a>, <a href="../keywordSearch/?keyword=arms+trade&source=675">arms trade</a>, <a href="../keywordSearch/?keyword=barclays&source=675">barclays</a>, <a href="../keywordSearch/?keyword=brighton&source=675">brighton</a>, <a href="../keywordSearch/?keyword=direct+action&source=675">direct action</a>, <a href="../keywordSearch/?keyword=fitwatch&source=675">fitwatch</a>, <a href="../keywordSearch/?keyword=may+day&source=675">may day</a>, <span style='color:#777777; font-size:10px'>mc donalds</span>, <a href="../keywordSearch/?keyword=royal+bank+of+scotland&source=675">royal bank of scotland</a>, <a href="../keywordSearch/?keyword=smash+edo&source=675">smash edo</a>, <span style='color:#777777; font-size:10px'>thales</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(317);
			
				if (SHOWMESSAGES)	{
				
						addMessage(317);
						echo getMessages(317);
						echo showAddMessage(317);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>