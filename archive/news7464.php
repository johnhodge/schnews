<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 746 - 4th November 2010 - Coal-Ition Politics</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, coal, open cast mine, protest camps, climate change, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 746 Articles: </b>
<p><a href="../archive/news7461.php">The Vodafone-y War</a></p>

<p><a href="../archive/news7462.php">Students Do It Dub-style</a></p>

<p><a href="../archive/news7463.php">Need Scumbody To Love</a></p>

<b>
<p><a href="../archive/news7464.php">Coal-ition Politics</a></p>
</b>

<p><a href="../archive/news7465.php">Above Their Station</a></p>

<p><a href="../archive/news7466.php">G4s Deportation Death</a></p>

<p><a href="../archive/news7467.php">3-pronged Attack</a></p>

<p><a href="../archive/news7468.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th November 2010 | Issue 746</b></p>

<p><a href="news746.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COAL-ITION POLITICS</h3>

<p>
<p>
	<strong>Happendon Woods</strong> are holding their Autumn Gathering 5th&ndash;10th November with info- and skill-sharing, action workshops and opportunities to get involved in the campaign and camp. The Happendon Wood Action Camp (THWAC) was occupied on September 12th to resist the destruction of the Douglas Valley by Scottish Coal and SRG Estates (see <a href='../archive/news739.htm'>SchNEWS 739</a>). </p>
<p>
	The camp are saying &ldquo;<em>The eviction at Mainshill was not an end but just a beginning. We&rsquo;re back to finish what we started. If Scottish Coal want to obliterate what&rsquo;s left of the Scottish countryside, we will obliterate them. We&rsquo;re calling for affinity groups to come to the site with energy and ideas for action to destroy Scottish Coal&rsquo;s plans</em>.&rdquo; </p>
<p>
	Last week (28th) an anonymous overnight action in solidarity with THWAC caused extensive damage to borehole drilling machinery at the nearby planned Glentaggart East open cast mine. <br /> 
	&nbsp;Meanwhile, as part of what they&rsquo;re calling their &lsquo;forward strategy&rsquo;, Scottish Coal have begun the process of closing the circle of open cast mines around the communities of the Douglas Valley by announcing three new open cast applications. </p>
<p>
	<strong>Get there:</strong> The camp is near junction 12 of the M74, between Carlisle and Glasgow. The nearest train stations are Lanark and Hamilton and there are frequent direct buses to near the site - call if you need a lift from station. Site mob 07806 926040 <a href="mailto:contact@coalactionscotland.org.uk">contact@coalactionscotland.org.uk</a> <a href="http://coalactionscotland.org.uk/?page_id=1974" target="_blank">http://coalactionscotland.org.uk/?page_id=1974</a> </p>
<p>
	<strong>Not to be outdone</strong> and still holding out strong, campers at the Huntington Lane anti-open cast site (see <a href='../archive/news744.htm'>SchNEWS 744</a>) are urging all to join them for a weekend of action on November 12th-15th. </p>
<p>
	One camper told SchNEWS &ldquo;<em>There&rsquo;s still thousands of acres of beautiful deciduous woodland around the site. We&rsquo;re busy building defences and as UK Coal have set up next door it&rsquo;s what you might call a target rich environment</em>&rdquo;. There&rsquo;ll be free vegan food, fireside music and dogs are welcome. </p>
<p>
	<strong>Get there:</strong> The entry point is on New Works Lane, New Works, Telford, Shropshire. Nearest train station is Wellington (Telford West). Site mob 07503 583419 <a href="mailto:defendhuntingtonlane@hush.com">defendhuntingtonlane@hush.com</a> <a href="http://www.defendhuntingtonlane.wordpress.com" target="_blank">www.defendhuntingtonlane.wordpress.com</a> </p>
<p>
	<strong>Meanwhile at Titnore Woods</strong> (see <a href='../archive/news714.htm'>SchNEWS 714</a>) controversy has reared its ugly head with landowner Clem Somerset, no doubt frustrated at the utter collapse of his plans to tarmac the woods, threatening to personally chop down the trees if the victorious campers don&rsquo;t leave forthwith. Does the axe-man cometh? There&rsquo;s an emergency meeting to discuss the response at the camp starting at 1pm, this Sunday 7th November. Site mob 07804 245324 <a href="mailto:info@protectourwoodland.co.uk">info@protectourwoodland.co.uk</a> <a href="http://www.protectourwoodland.co.uk" target="_blank">www.protectourwoodland.co.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(999), 115); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(999);

				if (SHOWMESSAGES)	{

						addMessage(999);
						echo getMessages(999);
						echo showAddMessage(999);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


