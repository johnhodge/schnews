<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 750 - 2nd December 2010 - It's a ZAD day when...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 750 Articles: </b>
<p><a href="../archive/news7501.php">Fee Foes' Fine Fun</a></p>

<p><a href="../archive/news7502.php">Schnews 750: State Of The Indignation</a></p>

<b>
<p><a href="../archive/news7503.php">It's A Zad Day When...</a></p>
</b>

<p><a href="../archive/news7504.php">The Italian Jobless</a></p>

<p><a href="../archive/news7505.php">Ratcliffe 2o Trial Begins</a></p>

<p><a href="../archive/news7506.php">Town Hall Meeting</a></p>

<p><a href="../archive/news7507.php">Phil Yer Boots</a></p>

<p><a href="../archive/news7508.php">Edl Double Bill</a></p>

<p><a href="../archive/news7509.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 2nd December 2010 | Issue 750</b></p>

<p><a href="news750.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IT'S A ZAD DAY WHEN...</h3>

<p>
<p>
	The struggle between campaigners and developers in ZAD (le Zone d&rsquo;Am&eacute;nagement Differ&eacute;) protest site near Nantes, France, is heating up this month. </p>
<p>
	The site sprung up last August after the first French Camp Action Climate, but there has been a 40 year campaign to prevent an airport being built in the countryside area. The plans for the space now, are for a so-called &ldquo;high quality environmental project&rdquo; will also include a high speed TGV railway, four-lane highway and a tram train. </p>
<p>
	The 1860 hectare site houses a total of 17 squats including fields, houses, forests, a farm and a guesthouse. The group is in the process of occupying a further two forest areas. The occupiers have been busy, organising two libraries, two free shops, a bike workshop, a welding workshop, a herb pharmacy and many collective gardens. </p>
<p>
	But in the last month surveyors have been clearing areas of land for the highway and drilling for water samples. Aided, of course, by heavy police protection, in this case 150 military police. </p>
<p>
	The sabotage of a machine &ldquo;in defiance of the private security guards&rdquo; who patrol the site to protect the interests of the developers has lead to an escalation of police presence and of direct action from the camp. </p>
<p>
	To stop the work going ahead, groups have engaged in tree-sits, blockades and the first public demo outside a public meeting in the town hall of nearby Notre Dame des Landes. On one occasion, local organisationd surrounded the police and made them retreat in a solidarity action. </p>
<p>
	The police, however, have also upped the ante. The morning after the town hall demonstration, the protesters awoke to find four police riot vans and several buses of military and local police. The police pepper sprayed activists as they &ldquo;chanted and milled about&rdquo; with older people and children present. </p>
<p>
	Campers at ZAD are eager for more support and this critical time, and welcome visitors to stay at the camp. Info and directions available from <a href="mailto:zad@riseup.net.">zad@riseup.net.</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1027), 119); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1027);

				if (SHOWMESSAGES)	{

						addMessage(1027);
						echo getMessages(1027);
						echo showAddMessage(1027);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


