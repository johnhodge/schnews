<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - Greece: The Conspiracy of Fire Nuclei - Flamin' Eck</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, anarchists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>

<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>

<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>

<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<b>
<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>
</b>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<p><a href="../archive/news75511.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREECE: THE CONSPIRACY OF FIRE NUCLEI - FLAMIN' ECK</h3>

<p>
<p>
	The Conspiracy of Fire Nuclei (SPF) trial has started in Greece. </p>
<p>
	The bombastically named anarchist group, SPF, are on trial for a series of attacks using incendiary devices in the form of gas canister bombs and other improvised explosive devices. The SPF are being held accountable for over 200 bombs against symbols of wealth, power and state since 2008. A mail bomb campaign aimed at political institutions such as Embassies and Europol (the European Law Enforcement Organisation) had unnerved much of Europe in 2010. Last month the group claimed responsibility for an Athens courthouse bombing. </p>
<p>
	There are 13 defendants on trial, accused of being members of the group. Nine of the thirteen defendants have appeared in court, the rest will be tried in absentia. If the court condemns them of intending to commit murder they face life imprisonment. The defendants are being tried by a three member tribunal rather than a jury under a new terrorism law that supposedly minimises the possibility of intimidation. An online posting warned court officials that &ldquo;<em>for every year in prison given to our brothers, we will plant a kilogram of explosives in your front yards, cars and offices</em>&rdquo;. </p>
<p>
	On the first day of business, Monday (17th), time inside the court room was dedicated mainly to procedural details. It was made known that nobody would be allowed to enter without ID and subsequently the ID card would be kept for the duration of the trial. Never mind the above edict is illegal since the police only have the right to keep your ID card if you are a witness. The defendants refused to speak and exited from the courtroom upon hearing this decision. An hour later they re-entered the arena with demands of no personal data to be recorded from identity cards, nothing to be held on record and demands that the plain clothes policemen leave the courtroom. </p>
<p>
	The defendants declared that if their demands were not met they would refuse to cooperate and would not participate in the trial. As the presiding judge failed to meet their demands they left the courtroom to the sound of shouts &ldquo;The passion for freedom is stronger than the prison&rdquo;.The trial will continue at 9am on Monday 24th January. </p>
<p>
	Solidarity action with the conspiracy of cells of fire group took place in Istanbul, Turkey, in the late hours of Sunday(16th) where the largest shopping centre was submitted to an attack with fireworks creating panic, no injuries were sustained. In Bristol in the early hours of Monday(17th) two telecommunications vehicles of British Telecom were set on fire in support also. </p>
<p>
	Elsewhere this week the trial of the Thessaloniki 4 began on Friday (14th). The four defendants have been in limbo since the EU summit held in Greece in 2003. Police repression at the summit led to over a hundred arrests. Most had their charges dropped in light of overwhelming evidence. Since 2003 the defendants have had their charges dropped, only to be reinstated, there has been a first trial and then a second trial was adjourned until this month. </p>
<p>
	*To follow proceedings check: <a href="http://www.salonikisolidarity.org.uk" target="_blank">www.salonikisolidarity.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1072), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1072);

				if (SHOWMESSAGES)	{

						addMessage(1072);
						echo getMessages(1072);
						echo showAddMessage(1072);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


