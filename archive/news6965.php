<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 696 - 23rd October 2009 - Bright Sparks</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, workers struggles, manchester" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 696 Articles: </b>
<p><a href="../archive/news6961.php">Sooty & Swoop</a></p>

<p><a href="../archive/news6962.php">Edo Aquittals  </a></p>

<p><a href="../archive/news6963.php">It&#8217;s Brim Up North</a></p>

<p><a href="../archive/news6964.php">Cymru Have A Go If You Think Yer Hard Enuff</a></p>

<b>
<p><a href="../archive/news6965.php">Bright Sparks</a></p>
</b>

<p><a href="../archive/news6966.php">Schnews In Brief</a></p>

<p><a href="../archive/news6967.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd October 2009 | Issue 696</b></p>

<p><a href="news696.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRIGHT SPARKS</h3>

<p>
A judge has dismissed a power company&rsquo;s attempts to prevent a one-man picket with an injunction under the Terrorism Act as &ldquo;fanciful and bordering on paranoia&rdquo;. Scottish and Southern Energy (SSE) attempted to suggest that the picket represented a &ldquo;threat to national security&rdquo;. <br />  <br /> Electrician Steve Acheson was dismissed from from his job at the Fiddlers Ferry power station project in December 2008. Although he was technically made redundant, at the time of his dismissal the firm took on another 300 workers. Since then he&rsquo;s maintained a regular picket outside the construction site with a banner saying &ldquo;<strong>STOP VICTIMISING UNION WORKERS - END THE BLACKLISTING NOW</strong>&rdquo; <br />  <br /> Initially SSE had attempted to secure a hearing without notice one month ago that would have left Steve unable to defend himself. Steve told SchNEWS, &ldquo;<em>All I want to do is highlight this abhorrent abuse of people&rsquo;s rights - I&rsquo;m not linked to any terrorist or protest organisations</em>&rdquo;. The evidence submitted by SSE suggested that he was inciting workers to &lsquo;direct action&rsquo; and was imperilling the nation&rsquo;s energy supplies. <br />   <br /> Having been involved in an earlier industrial dispute at the Manchester Royal Infirmary he strongly suspected he had been blacklisted within the construction industry. Confirmation of this came when the Information Commissioners Office launched a prosecution against Ian Kerr, head of the Consulting Agency. The Consulting Agency had maintained a database of over 3,000 construction workers. Companies who accessed the data were a virtual who&rsquo;s who of the UK construction giants, Balfour Beatty, Laing O&rsquo;Rourke, McAlpine etc etc. <br />   <br /> According to one worker who featured on the database, David Smith of the Blacklist Support Group: &ldquo;<em>It&rsquo;s a database of trouble-makers, people who&rsquo;ve stood up for their rights, complained about site safety, about having to remove asbestos without dustmasks. My dossier has even got information about my political affiliations and my wife. This is a human rights abuse, a complete conspiracy by the major companies paying thousands and thousands to organise this database</em>.&rdquo; <br />  <br /> In his case Ian Kerr was fined a mere &pound;5,000, which, given that his operation had been running since the eighties, is hardly a disincentive. Blacklisting is particularly useful to companies in the building trade as workers are employed on short-term contracts for different projects. Union activity is therefore that much easier to stamp out. <br />   <br /> A spokesperson for Blacklist Support Group said, &ldquo;<em>The files are blatant evidence of systematic victimisation - but this is not about being a victim. This is about ensuring justice for those honest building workers who raised genuine concerns about safety or complained if they were not paid. This is about multi-national companies not being above the law This is about equality and fairness: Human Rights for trade union members.</em>&rdquo; <br />   <br /> * <a href="mailto:blacklistsg@googlemail.com">blacklistsg@googlemail.com</a>  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=manchester&source=696">manchester</a>, <a href="../keywordSearch/?keyword=workers+struggles&source=696">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(539);
			
				if (SHOWMESSAGES)	{
				
						addMessage(539);
						echo getMessages(539);
						echo showAddMessage(539);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>