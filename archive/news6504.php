<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 650 - 10th October 2008 - Tata For Now</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, india, tata, nano, mamata banerjee, anti-corporate, resistance, singur, bengal" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../schmovies/index-on-the-verge.htm"><img 
						src="../images_main/on-the-verge-banner-tor.jpg" 
						width="465px" 
						height="90px" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 650 Articles: </b>
<p><a href="../archive/news6501.php">End Of World Is Nigh</a></p>

<p><a href="../archive/news6502.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6503.php">Navy Blues</a></p>

<b>
<p><a href="../archive/news6504.php">Tata For Now</a></p>
</b>

<p><a href="../archive/news6505.php">It Doesn't Add Up</a></p>

<p><a href="../archive/news6506.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th October 2008 | Issue 650</b></p>

<p><a href="news650.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>TATA FOR NOW</H3>

<p>
Earlier this year there was much media fanfare over the unveiling of the world�s cheapest car � the  Nano, to be made by Indian car manufacturers TATA corporation. And if plans to sell bucketloads of them to less wealthy Indians succeed, �Tata� is what they�ll be saying to hopes of reducing the impacts of climate change.  Due to roll out in October and set to retail at less than �1500 quid, the idea is to make it affordable to the many millions of people who currently buy motorbikes, which outsell cars by seven to one in India. But not for long - car sales are projected to more than quadruple to �90bn by 2016.  <br />
<br />
Apart from the fact that the cars are made largely from oil-intensive plastics and other environmentally unfriendly natural resources, their proliferation will also mean a massive upscaling of India�s infrastructure � huge road building programmes and petrol stations etc etc.  Claims from TATA that they aim to make less polluting cars in the future, with electric Nano�s being touted, look pretty hollow when you consider that India largely generates its power from coal and TATA made headlines earlier in the year by buying the not-so-eco Land Rover and Jaguar brands from Ford.<br />
<br />
But a huge grassroots movement is delaying the launch. The land for the factory in Singur, West Bengal � some of the most fertile in the area, where most of the 150,000 population make their living from agriculture - was forcibly taken off local farmers by the regional government, keen to lay out the red carpet for the industrialists � who promise to create just a thousand jobs, most of which will go to outsiders. <br />
<br />
Just under a thousand acres was annexed for the plant leading to a mass outcry and the start of a campaign of direct action by many of the dispossessed,  inspired by a local opposition politician, Mamata Banerjee, who even went on hunger strike to support them in December 2006. Protests at the fenced off site that month � heavily guarded by police - saw the rape and murder of a teenage activist who was burned to death. <br />
<br />
Intermittent attacks on the site have continued in the intervening period, with all the usual bullying tactics wheeled out to stop them.  Demanding the return of at least 400 acres of the land stolen, since Aug 24th, protesters have laid siege to the factory, blockading it, harassing workers and stopping TATA from completing the construction of the site, which they have pumped $350 million quid into so far.  They also have blockaded the main highway between Delhi and nearby city Kolkata, hitting supplies as trucks are left stranded on the road. <br />
<br />
And the protests have proved so effective that last week, TATA were forced to completely abandon the plant. This is despite head honcho Ratan Tata having once claimed he will not pull out of Singur even �at gun point�. In the end guns weren�t needed and TATA have had to walk away from their $340 million investment in the facility. They are now looking for new land elsewhere to start all over again, delaying the launch of the car for a long time to come. Meanwhile, protests are continuing to get land given back and more compensation for those displaced. The message from West Bengal is clear � Just say Nano... 
</p>

</div>


<br /><br />

		</div>
		
		
		
		
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>