<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 686 - 7th August 2009 - Blowing In The Wind</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, vestas, direct action, isle of wight, workers struggles" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 686 Articles: </b>
<b>
<p><a href="../archive/news6861.php">Blowing In The Wind</a></p>
</b>

<p><a href="../archive/news6862.php">Plugging Into The Mainshill</a></p>

<p><a href="../archive/news6863.php">Rabbiting On</a></p>

<p><a href="../archive/news6864.php">Rotten To The Caucasus</a></p>

<p><a href="../archive/news6865.php">The Garda They Come</a></p>

<p><a href="../archive/news6866.php">Down Under Fire</a></p>

<p><a href="../archive/news6867.php">Jungle Missive</a></p>

<p><a href="../archive/news6868.php">Yeah Yeah Yeah</a></p>

<p><a href="../archive/news6869.php">Afghan Hounding</a></p>

<p><a href="../archive/news68610.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/686-vestas-lg.jpg" target="_blank">
													<img src="../images/686-vestas-sm.jpg" alt="They can't call me a roofless extremist..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 7th August 2009 | Issue 686</b></p>

<p><a href="news686.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BLOWING IN THE WIND</h3>

<p>
<strong>DESPITE COURT ORDER, VESTAS ACTION GOES NATIONWIDE...</strong> <br />  <br /> The campaign against the closure of the wind turbine manufacturer Vestas&rsquo; site in the Isle of Wight, instigated by sacked workers occupying the factory two weeks ago (see <a href='../archive/news685.htm'>SchNEWS 685</a>), has continued this week with a broad coalition of activists and locals assembling to show solidarity with the workers.&nbsp; <br />  <br /> The six workers that currently remain in the factory are being supported by a multitude of campaigners from climate action groups, trade unions and socialist networks, as well as concerned locals, who have all gathered (accompanied by a thumping bicycle sound system) to oppose the loss of hundreds of jobs, damage to the local economy and the loss of an opportunity to develop a key green industry. <br />  <br /> Vestas successfully applied for a re-possession order on Tuesday (4th) and, despite saying that they were in no hurry to turf out the barricaded workers, served them with an eviction notice which will be carried out with the help of bailiffs at some time after 12pm today (7th). However, campers on the roundabout outside the factory have stressed that no matter what the outcome, the protest will continue and are calling for people to grab &lsquo;a bag of tricks&rsquo; and camping gear and get over to the protest site by midday today.&nbsp; <br />  <br /> Along with demonstrators on the &lsquo;Magic Roundabout&rsquo; on the St Cross industrial estate, a second site has been established in Cowes, much to the delight of the amazingly supportive locals. Protesters scaled the 30m walls of Vestas-owned workshops on Tuesday 4th, and have been on the roof ever since. They have been displaying pro-worker solidarity banners to the passing yachters attending the Cowes Regatta and anyone entering the busy port. Police have, with no sense of irony, been barricading these campaigners in with anti-vandal paint and massive fences, although with solar showers being set up and a huge stock of supplies, the protesters have no intention of going anywhere. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/686-swan-vestas-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/686-swan-vestas-sm.jpg"  alt='Strike a light, it's... Swine Vestas'  /></a></td></tr></table>Representatives from the transport union RMT, who are supporting the ex-employees, have met with government officials who have agreed to work towards reinstating the redundancy packages previously taken away from the demonstrators inside the factory. These same officials have also reported that they have been in talks with Vestas to try and either encourage them to sell the facility or re-open, ideas which have been flatly rejected by the company. However, Vestas&rsquo; compliance is not required if the business were to be nationalised, and this is now the primary focus of all campaign efforts. <br />  <br /> Two of the ex-Vestas operatives who left the stand-off earlier this week have since embarked on a tour, meeting with interested parties and giving talks. Those still at the camp remain upbeat and determined to continue in their campaign, retaining at its heart the plight of hundreds of workers who now find themselves unemployed in an island economy devastated by the economic crisis. <br />  <br /> * A rally is to be held in Newport town centre on Saturday 8th, all available support is needed. A national day of solidarity is also scheduled for next Wednesday (12th), for details see <a href="http://savevestas.wordpress.com" target="_blank">http://savevestas.wordpress.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=686">direct action</a>, <span style='color:#777777; font-size:10px'>isle of wight</span>, <span style='color:#777777; font-size:10px'>vestas</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=686">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(422);
			
				if (SHOWMESSAGES)	{
				
						addMessage(422);
						echo getMessages(422);
						echo showAddMessage(422);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>