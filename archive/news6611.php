<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 661 - 9th January 2009 - Eyewitness : Rafah</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, israel, idf, genocide, rafah" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 661 Articles: </b>
<b>
<p><a href="../archive/news6611.php">Eyewitness : Rafah</a></p>
</b>

<p><a href="../archive/news6612.php">Eyewitness : Gaza</a></p>

<p><a href="../archive/news6613.php">Global Outrage</a></p>

<p><a href="../archive/news6614.php">Gaza Relief Boat Rammed</a></p>

<p><a href="../archive/news6615.php">Ceasefire? What Ceasefire?</a></p>

<p><a href="../archive/news6616.php">Gaza Gameplan</a></p>

<p><a href="../archive/news6617.php">Media War</a></p>

<p><a href="../archive/news6618.php">Elementary My Dear Watson</a></p>

<p><a href="../archive/news6619.php">Inside Schnews</a></p>

<p><a href="../archive/news66110.php">Outside Schnews</a></p>

<p><a href="../archive/news66111.php">Shac Verdict</a></p>

<p><a href="../archive/news66112.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/661-01-gaza-airstrike-lg.jpg" target="_blank">
													<img src="../images/661-01-gaza-airstrike-sm.jpg" alt="Gaza Explodes" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 9th January 2009 | Issue 661</b></p>

<p><a href="news661.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>EYEWITNESS : RAFAH</H3>

<p>
"<i>With the Israeli ban on international journalists, the Gazan voice has been further muted. Communicating the reality on the ground with the external world is essential to highlight the illegality of Israel's attacks. We recently started accompanying ambulances to document the attacks on medical personnel, which is a violation of the Geneva Convention. I have seen and felt the suffering of families and cannot leave them, all the civilians are vulnerable to Israel's attacks. We intend to stay and continue exposing the nature of Israel's attacks on the Gazan people.</i> " - <b>Jenny Linnel - International Solidarity Movement.</b> <br />
 <br />
Day 14 of <b>Operation Cast Lead</b> - Israel's attack on the Gaza strip - and the scale of the damage inflicted is becoming apparent. has been in contact with British peace activist Jenny Linell in Rafah. She's in no doubt that the unleashing of the world's fourth most powerful army on a civilian population has little to do with suppressing Hamas' rocket fire. It's about intensifying the siege and collective punishment of the whole population for daring not to knuckle under.  <br />
 <br />
Jenny told SchNEWS: <br />
 <br />
"<i>It's not about targeting tunnels on the border - it's about wiping out half of Rafah. That's what it's about. In the north people left their homes as they were warned to by the Israelis - but where are they supposed to stay? Many families began staying in UN schools - now they've bombed them! That's crossing the limit. It means they'll do anything - anything! Two other families are coming to stay with our family tonight. That's the situation all over Rafah - people are just trying to put up families who've left as best they can. And now leaflets have been dropped saying... <br />
 <br />
<blockquote>"Citizens of Rafah: Due to Hamas using your houses to smuggle and store ammunition, the Israeli Defence Force will attack your homes from Sea Street to the Egyptian border. To the people who live in these areas: Block O, Al Brazil camp, Al Shora area and Qishta area, all homes beyond Sea Street must be evacuated. You have from the time you receive this leaflet until 7am the following morning. For you and your children's safety follow what this leaflet says. - </i><b>The leadership of the Israeli Defence Force"</b><i></blockquote> <br />
 <br />
From the borderline to Sea Street - that's thousands and thousands of homes. They've all been threatened - many families directly on the border had already fled - now there's just this mass exodus and goodness knows what's going to happen. Some people have had automated messages [from the IDF] saying if you live in such and such an area, if you have tunnels , if you have weapons in your house you will be targeted. But they've been hitting anyone and everything whether or not you've had a message.</i>" <br />
 <br />
Effectively the whole of Rafah has been declared a free-fire zone for F-16s and helicopter gunships. During the siege the tunnels across to Egyptian Rafah were a vital lifeline (see <a href="../archive/news617.htm">SchNEWS 617</a>). The Israeli airforce is now attempting to smash the network using giant U.S supplied bunker-busters. The locals are used to a level of constant violence and intimidation - years of IDF operations with armoured bulldozers have created a no-mans land of smashed houses along the border strip. <br />
 <br />
"<i>There's no bomb shelters or anything like that. You just sit in your house as the bombs drop around you. Bombs that were going off on the border 2km away and it feels like they're just at the end of the street. We have to keep the windows open all the time because they'd shatter otherwise from the force of the explosion. People are not hysterical - they're Palestinians which means they've experienced so much of this all their lives. But this is above and beyond - the level of aggression - like nothing that's ever been seen before. There's nowhere to run to.</i>"
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=661">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=661">palestine</a>, <a href="../keywordSearch/?keyword=israel&source=661">israel</a>, <a href="../keywordSearch/?keyword=idf&source=661">idf</a>, <a href="../keywordSearch/?keyword=genocide&source=661">genocide</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(180);
			
				if (SHOWMESSAGES)	{
				
						addMessage(180);
						echo getMessages(180);
						echo showAddMessage(180);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>