<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 669 - 20th March 2009 - Stand Up To Detention</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, asylum seekers, refugees, immigration, detention centres, jacqui smith, brook house, phil woolas, no borders, congo, kurdistan, iraq, tinsley house, gatwick, campsfield, serco, sodexo, harmondsworth, darfur, yarls wood" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 669 Articles: </b>
<b>
<p><a href="../archive/news6691.php">Stand Up To Detention</a></p>
</b>

<p><a href="../archive/news6692.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6693.php">U S Activist Shot In Palestine</a></p>

<p><a href="../archive/news6694.php">Caravanguard</a></p>

<p><a href="../archive/news6695.php">Doing Whirly-bird</a></p>

<p><a href="../archive/news6696.php">Big Touble</a></p>

<p><a href="../archive/news6697.php">Pieces Of Ait</a></p>

<p><a href="../archive/news6698.php">Hoi-dee-hoi</a></p>

<p><a href="../archive/news6699.php">--- Stop Press ---</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/669-deporture-screen-lg.jpg" target="_blank">
													<img src="../images/669-deporture-screen-sm.jpg" alt="Deportations - Expect delays on some flights due to demonstrations..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 20th March 2009 | Issue 669</b></p>

<p><a href="news669.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STAND UP TO DETENTION</h3>

<p>
<span style="font-size:13px; font-weight:bold">AND BE COUNTED IN THE FIGHT AGAINST UK IMMIGRATION POLICY</span> <br />
 <br />
On Wednesday, Home Secretary Jacqui Smith opened the latest addition to UK plc&#8217;s capacity for locking people up, Brook House - the largest immigration detention centre yet - with space for 426 prisoners. 2,500 people are currently locked up in 13 British immigration prisons and the government want to be able lock up another 1,500. Next up is a &#8216;mega-detention centre&#8217; near Bullington in Oxfordshire and one in Bedfordshire. <br />
 <br />
Last Friday, immigration minister Phil Woolas was treated to a dose of his own detention when two dozen local residents occupied his Oldham and Saddleworth constituency office. The group of migrant rights advocates and anarchists took over for 30 minutes to demand the abolition of all immigration prisons, and specifically Pennine House at Manchester Airport. Opened earlier this year by the Wool-ass, Pennine House can hold 32 detainees for up to a week, until they are either deported or transferred to other detention centres.  <br />
 <br />
Previously, Manchester No Borders had pied Woolas at a debate and targeted council leader Richard Leese over the same issues. They dumped 100 jumpers in front of the panellists of an event entitled &#8216;The Right to the City&#8217; in a protest against the Pennine House prison. <br />
 <br />
Early morning raids by the Immigration Police (See <a href='../archive/news615.htm'>SchNEWS 615</a>) continue to target families fleeing persecution, with some groups being targeted for deportation despite the well-documented political violence in their home countries. Such places  as Democratic Republic of Congo and Iraqi Kurdistan are classed as &#8216;safe&#8217; to deport asylum seekers to, yet the foreign office simultaneously advises against all but essential travel to these areas for everyone else. Over 350 people have been deported by charter flights to Iraqi Kurdistan in the last six months. Recent deportees have committed suicide, been kidnapped or killed in car bombs. <br />
 <br />
50 Iraqi Kurds were recently rounded up and held at Gatwick&#8217;s Tinsley House detention centre. A special deportation charter flight was scheduled from Stansted to Arbil, the capital of  Iraqi Kurdistan, last Tuesday. <br />
 <br />
Determined to stop this collective expulsion, campaigners blockaded the Tinsley House gate on Tuesday morning. Six people locked themselves together and then super-glued their hands to the gates. After six hours they were forcibly removed and arrested, along with three others, for aggravated trespass. Shortly after, a coach carrying between 8 and 12 Iraqi refugees due for deportation left for Stansted. Others were reportedly taken from Campsfield and Dover detention centres, although some had outstanding appeals and judicial reviews. The charter flight landed in Sulaimaniyya, northern Iraq, with approximately 60 people on board. At least two people had won High Court injunctions and were taken off the flight. <br />
 <br />
One of the deportees, whose name cannot be used for his own security in Iraq, said earlier on the phone: &#8220;<i>I&#8217;ve been in the UK for nine years. I have a partner and an 18-month-old son. If I am deported, all this will be gone. I&#8217;ve made a life for myself here, living as everyone else does in this country, but I&#8217;m now being treated like I&#8217;m a criminal, imprisoned then deported.</i>&#8221; He added: &#8220;<i>I left Iraq originally because my life was threatened by a radical Islamic group. That same group is now more powerful than they were before. I won&#8217;t be safe, I won&#8217;t be safe.</i>&#8221; <br />
 <br />
Another Iraqi refugee, deported last month, said: &#8220;<i>I don&#8217;t know when I&#8217;ll see my partner or my daughter again. I speak to them in tears on the phone every night. I am still in shock after being sent back. I have had to change my name so I&#8217;m not targeted by the same people who threatened to kill me before. My entire world has caved in.</i>&#8221; <br />
 <br />
Deportation flights to Iraq have been chartered from Czech Airlines and Hamburg International and the coaches transporting the detainees in UK are operated by WH Tours and Woodcock Coaches. The main corporate profiteers of this detention and deportation misery are Global Solutions Limited (GSL), owners of Group 4 Securicor (G4S), who run three detention centres, including Brook House, and provide detainee &#8216;escorting services.&#8217; Other detention centres are operated by Serco, Sodexo and GEO. <br />
 <br />
GSL has been repeatedly criticised by HM Chief Inspector of Prisons over their running of the detention centres. In her 2008 inspection report, Anne Owers expressed &#8220;<i>concerns over provision for children</i>&#8221; held at Tinsley House, and considered that conditions for the much-reduced number of single women were &#8220;unacceptable.&#8217;&#8217; <br />
 <br />
In 2006, Owers said Harmondsworth detention centre near Heathrow &#8220;<i>had been allowed to slip into a culture and approach which was wholly at odds with its stated purpose</i>&#8221; following the suicide of two detainees in 2004 and 2006. This week the Court of Appeal ruled that the government was wrong not to order an independent inquiry into allegations of mistreatment at Harmondsworth in 2006. Detainees had been denied food and drink, forced to urinate and defecate in front of each other, and strip searched in front of several officers.  <br />
 <br />
One of the latest victims of the deportation regime is Hussein Khedri, a young Iranian detained last week and facing deportation to Greece, despite an independent medical report confirming him to be a minor. Hussein is the youngest member of the Tuesday Night Project at The Square Centre in Nottingham and is widely liked by all who attend. <br />
 <br />
Adam Osman Mohammed was forced to flee to the UK when his village, in Darfur, was repeatedly attacked in 2005. He was refused asylum, and took &#8216;voluntary return&#8217; before being forced back anyway. A few days after he arrived in Darfur he was shot dead in front of his family, by government security forces. The Home Office now want to restart deportations to Darfur. <br />
 <br />
This Saturday (21st) sees a national day of action against detention centres. A protest march will go 11.30am from Bedford town centre to Yarls Wood detention centre, a demo in Manchester will target Pennine House and in Edinburgh campaigners will picket G4S.  <br />
 <br />
* See <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> for more details.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=669">asylum seekers</a>, <span style='color:#777777; font-size:10px'>brook house</span>, <a href="../keywordSearch/?keyword=campsfield&source=669">campsfield</a>, <a href="../keywordSearch/?keyword=congo&source=669">congo</a>, <span style='color:#777777; font-size:10px'>darfur</span>, <span style='color:#777777; font-size:10px'>detention centres</span>, <span style='color:#777777; font-size:10px'>gatwick</span>, <span style='color:#777777; font-size:10px'>harmondsworth</span>, <a href="../keywordSearch/?keyword=immigration&source=669">immigration</a>, <a href="../keywordSearch/?keyword=iraq&source=669">iraq</a>, <span style='color:#777777; font-size:10px'>jacqui smith</span>, <span style='color:#777777; font-size:10px'>kurdistan</span>, <a href="../keywordSearch/?keyword=no+borders&source=669">no borders</a>, <span style='color:#777777; font-size:10px'>phil woolas</span>, <a href="../keywordSearch/?keyword=refugees&source=669">refugees</a>, <span style='color:#777777; font-size:10px'>serco</span>, <span style='color:#777777; font-size:10px'>sodexo</span>, <span style='color:#777777; font-size:10px'>tinsley house</span>, <span style='color:#777777; font-size:10px'>yarls wood</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(248);
			
				if (SHOWMESSAGES)	{
				
						addMessage(248);
						echo getMessages(248);
						echo showAddMessage(248);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>