<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 677 - 29th May 2009 - Mosquito Bite</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, m�skito, nicaragua, daniel ortega, moskitia, sandinistas, latin america" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 677 Articles: </b>
<b>
<p><a href="../archive/news6771.php">Mosquito Bite</a></p>
</b>

<p><a href="../archive/news6772.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6773.php">No Country For Old Men</a></p>

<p><a href="../archive/news6774.php">Guerillas In Our Midst   </a></p>

<p><a href="../archive/news6775.php">Medical Alert</a></p>

<p><a href="../archive/news6776.php">Eco-village People</a></p>

<p><a href="../archive/news6777.php">Ronnie Easterbrook Rip</a></p>

<p><a href="../archive/news6778.php">Withdrawal Method</a></p>

<p><a href="../archive/news6779.php">Slipping The Net</a></p>

<p><a href="../archive/news67710.php">Inside Schnews</a></p>

<p><a href="../archive/news67711.php">Gone Postal</a></p>

<p><a href="../archive/news67712.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/677-ortega-mosquito-lg.jpg" target="_blank">
													<img src="../images/677-ortega-mosquito-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 29th May 2009 | Issue 677</b></p>

<p><a href="news677.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MOSQUITO BITE</h3>

<p>
<p><strong>AS A SWARM OF MISKITOS TAKES OVER THE COAST OF NICARAGUA</strong> <br />  <br /> On April 19th, representatives from 360 M&iacute;skito communities declared the secession of the entire Caribbean coast of Nicaragua, also known as the Mosquito Coast. They announced that the area, which accounts for 46% of Nicaragua&rsquo;s territory and an estimated 11% of the population, would form the independent Nation of Moskitia. <br />  <br /> Met with barely a mention from the world press, a stony silence from Nicaraguan President Daniel Ortega and scorn from local government, the indigenous Council of Elders annulled all land contracts with the Nicaraguan government pending renegotiation, cancelled government elections, called for businesses to begin paying tax to them instead of the government authorities and gave the police and military six months to organise an orderly and peaceful withdrawal. Not only have the new &lsquo;government&rsquo; already begun work on a currency, a flag and a national anthem they are also backed by the &lsquo;<strong>Indigenous Army of Moskitia</strong>&rsquo;, comprised of up to four hundred conflict veterans. <br />  <br /> One of the first acts of the &lsquo;<strong>Indigenous Army</strong>&rsquo; was the unarmed takeover of the party headquarters of the political party Yatama &ndash; the party which represents the indigenous population in central government and is an ally of the ruling Sandinista party, the FSLN. The party, like the members of the &lsquo;Indigenous Army&rsquo;, has its origins in the region&rsquo;s struggle against the Sandinista government in the 1980&rsquo;s. Following the overthrow of dictator Anastasio Somoza in 1979 the M&iacute;skito people, the region&rsquo;s majority indigenous group, found themselves caught up in the conflict between the new revolutionary Sandinista government and US backed militias, the Contras. Those in the firing line faced forced relocation into government camps, arbitrary imprisonment and even disappearances and torture. In response many took up arms, some later to join with the Contras, others independently, in a rebellion that lasted until 1987. <br />  <br /> The treatment of the Misqito people was always one noticeable stain on the Sandinista&rsquo;s less than impressive human rights record, which as well as accusations of arbitary imprisonment and disapearences also included the curtailing of freedom of speech and assembly and the freedom of the press. Most of the Sandinista abuses of the time could at least be put into the context of a conflict with well-armed, foreign-backed and completely ruthless paramilitary organisations which habitually employed the worst kind of terror tactics. The M&iacute;skito people, however, just lived in a strategically important location.&nbsp; <br />  <br /> The legislation which eventually brought an end to hostilities was the &lsquo;Autonomy of the Caribbean Coast&rsquo; law, passed by the Sandinista government in 1987. The law divided the region into two autonomous regions, one in the North &ndash; RAAN, and one in the South - RAAS which, in theory, had control of natural resources and a degree of self-determination. In practice any semblance of autonomy quickly came to an end following the 1990 electoral defeat of the Sandinistas and the accession of the conservative Violeta Barrios de Chamarro who had little time for autonomy but a keen interest in the region&rsquo;s natural resources. <br />  <br /> Since then the M&iacute;skitos have suffered in poverty. Struggling against an increasingly unpredictable climate and hit hard by natural disasters, the M&iacute;skitos have also witnessed the destruction of around 50% of their rainforest in the last 50 years from logging and the pollution of their water supplies with mercury and cyanide by gold mines. The issue of secession first came to attention in 2002 when the Council of Elders declared independence in theory but took no concrete steps to achieve it in practice. <br />  <br /> In 2006, Daniel Ortega, leader of the Sandinista party that first fought the M&iacute;skitos then granted them autonomy in the 80s, returned to power after 16 years in opposition. Back in the 80s Ortega and the Sandinistas gained world-wide renown following their ousting of the vicious Somoza. While they were demonised by the Americans, with Ronald Reagan labelling Sandinista Nicaragua a &ldquo;totalitarian dungeon&rdquo;, they were idolised by many on the left for being a uniquely multi-class, multi-ethnic, multi-doctrinal, and politically pluralistic movement. <br />  <br /> In its latest incarnation the Sandinistas have proved to be equally divisive, although for very different reasons. Throughout their years in opposition, Ortega was accused of accumulating personal power by strengthening his grip on the party machinery. This led to a rift in the party with dissidents breaking away to form their own Sandinista party, the Sandinista Renovation Movement (MRS). Unperturbed, Ortega began forging alliances with what had previously been regarded as Sandinista enemies.&nbsp; <br />  <br /> The process began in 1999 when Ortega entered into an agreement with Arnoldo Alem&aacute;n, an official during the Somoza regime who had been elected president in 1997. Amongst other things, the agreement was used to attempt to prevent the prosecution of Alem&aacute;n on corruption charges and Ortega on charges that he had sexually abused his stepdaughter, Zoilamerica Narvaez.&nbsp; <br />  <br /> With the 2006 elections approaching this was followed by Ortega converting to Catholicism &ndash; the church had been fierce opponents of the Sandinistas, seeing them as a threat to their traditionally privileged position. He then announced a former contra, Jaime Morales, as his running mate. <br />  <br /> Since regaining power Ortega has been attacked for clamping down on democracy. Things reached a head in the November municipal elections when candidates from MRS and the traditional Conservative party were banned from running altogether. International observers were barred from the elections while the tens of thousands of observers from the independent Nicargauan group, Ethics and Transparency, were reduced to trying to observe from outside the polling stations. Even from that distance they estimated that there were irregularities in a third of polling stations and questioned the results which saw a comprehensive win for the FSLN that flew in the face of independent surveys conducted before the election. The perception that the election was fraudulant led to mass street protests which were violently put down.&nbsp; <br />  <br /> Ortega has also faced criticism from disillusioned social movements, especially for his stance on women's rights. In the 80's the Sandinista commitment to gender equality saw women play a major role in the movement and in its leadership. Following the 1990 electoral defeat these gains were quickly reversed and by the end of 1991 around 16,000 working women had lost their jobs. Since returning to power, instead of  restoring the pre-eminence of women&rsquo;s struggle for equality, Ortega has pandered to his chauvinistic foes by not only supporting a blanket ban on abortion bill also by persecuting the Autonomous Women&rsquo;s Movement (MAM) for opposing it.&nbsp; <br />  <br /> Nevertheless Ortega's return has a seen the reintroduction of a number of progressive policies; literacy rates are approaching 100%, free health clinics are once again staffed and stocked and the small and medium farming sector has been revived.&nbsp; <br />  <br /> The current Ortega government is clearly considerably different in character to the one that battled the M&iacute;skito in the early 80s and to the one that signed a peace deal years later. So far it has yet to actively respond to the declaration, perhaps embarrassed by the fact that last year Ortega was the only world leader to recognize the breakaway Russian-backed republics of South Ossetia and Abkhazia. Or maybe they are biding their time waiting to see if the movement finds wider resonance in the region. <br />  <br /> The proposed Nation of Moskitia would be divided into seven autonomous regions, reflecting the ethnic diversity of the region which is home to Spanish speaking Mestizos (people of mixed Spanish and indigenous heritage), English speaking Creoles, Garifuna (a Caribbean population of mixed indigenous and African slave heritage) as well as a number of other indigenous groups. This diversity is already presenting the separatists with one of their biggest challenges. While the M&iacute;skito are the majority in the Northern region, in the South they form a minority. So far, although Southern M&iacute;skito leaders have declared their support for independence, the  rest of the regions population has remained detached and silent.&nbsp; <br />  <br /> While the national government has yet to comment the regional powers have been considerably more vocal. The Governor of the RAAN, Reynaldo Francis, stated that it is merely a &ldquo;poorly organised&rdquo; effort by a &ldquo;group of old men&rdquo;. This charge was strongly denied by separatist leader Rev. Hector Williams who has been been elected as Wihta Tara, or 'Great Judge' of Moskitia by the M&iacute;skito communities. In response to Francis' comments Williams said, "We are not puppets. We are men. And now we have the weight of a nation on our shoulders." <br />  <br /> Whether it proves successful or not, the independence movement of the Mosquito coast is further proof of the increasing rebelliousness of Latin America's indigenous population after 500 years of abuse and exploitation. With an indigenous president in power in Bolivia, the continuing Zapatista autonomous zones in Mexico and mass movements from the Minga in Colombia (see <a href='../archive/news656.htm'>SchNEWS 656</a>) to the continuing protests in Peruvian Amazonia (see <a href='../archive/news675.htm'>SchNEWS 675</a>), Latin America's indigenous peoples are becoming increasingly vocal in demanding  not just recognition and resources but also to live in a society organised on different principles, their own principles.</p> <br /> 
 <br />      <br />    <br /> 
<p></p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>daniel ortega</span>, <a href="../keywordSearch/?keyword=latin+america&source=677">latin america</a>, <span style='color:#777777; font-size:10px'>moskitia</span>, <span style='color:#777777; font-size:10px'>m�skito</span>, <span style='color:#777777; font-size:10px'>nicaragua</span>, <span style='color:#777777; font-size:10px'>sandinistas</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(334);
			
				if (SHOWMESSAGES)	{
				
						addMessage(334);
						echo getMessages(334);
						echo showAddMessage(334);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>