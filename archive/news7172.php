<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 717 - 9th April 2010 - Stan By Your Man</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, kyrgyzstan, russia, central asia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 717 Articles: </b>
<p><a href="../archive/news7171.php">Counter Strike</a></p>

<b>
<p><a href="../archive/news7172.php">Stan By Your Man</a></p>
</b>

<p><a href="../archive/news7173.php">Calloo, Calais</a></p>

<p><a href="../archive/news7174.php">Colombia: Crude Boys</a></p>

<p><a href="../archive/news7175.php">Cementing Friendship</a></p>

<p><a href="../archive/news7176.php">Schnews In Brief</a></p>

<p><a href="../archive/news7177.php">Going Ballistic</a></p>

<p><a href="../archive/news7178.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th April 2010 | Issue 717</b></p>

<p><a href="news717.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STAN BY YOUR MAN</h3>

<p>
<font face="courier new"><strong><u>Recipe for disorder:</u>&nbsp;</strong></font><font face="courier new"> <br />  <br /> <strong>Step 1</strong>. Take a corrupt politician with U.S. support (in this case President Kurmanbek Bakiyev) <br />  <br /> <strong>Step 2</strong>. Throw in an increase on tariffs of fuel, water and gas alongside human rights controversy <br />  <br /> <strong>Step 3</strong>. Finish off with a pinch of vexed protesters  and voil&agrave;... you&rsquo;ve got yourself an 11.2 on the Ruckter Scale uprsing!</font> <br />  <br /> The government in Kyrgyzstan was overthrown by protesting opposition forces following violent revolts in which more than 40 demonstrators died as they clashed with police. <br />  <br /> Kyrgyzstan is a former Soviet republic, which became independent when the USSR collapsed. This pint sized central Asian resident is home to a major US airbase, Manas, which supplies Afghanistan and seems to be a growing thorn of contention between Moscow and Washington. Bakiyev had pledged to shut the air base down last year. But then the Bakiyev regime began fraternising with the US and surprisingly decided to retract the decision after being promised higher rent cheques. <br />  <br /> Tension had built in Kyrgyzstan over human rights issues (like the persecution of opposition leaders and independent journalists through a series of arrests and physical assaults by government agents). Despite the escalating repressive nature of Bakiyev&rsquo;s policies however, the catalyst for the uprising was the imposed punitive increases on the price of utilities. <br />   <br /> Unrest began in rural areas on Tuesday (6th); by Wednesday the fires of revolution had spread nationwide - demonstrators had looted weapons, stolen an armoured personnel carrier and were in control of several principal government buildings including the parliament and television station. An failed attempt was made on the national security&rsquo;s service HQ. At least they managed to set the prosecutors office on fire... By the afternoon, gunfire and explosions reverberated around the capital city, Bishkek. The city witnessed a wave of public disorder as protesters stormed into the centre, igniting police cars and blockading roads. Police responded with tear gas, grenades and live bullets. <br />  <br /> Bakiyev fled the capital yesterday, his current whereabouts are unknown. Unfortunately former foreign minister, Rosa Otunbayeva, has reared an ugly head as a transitional government leader. This government has now stated the previous parliament has been dissolved and they will hold power for six months before staging a constitutional assembly. They also made sure to point out the American air base&rsquo;s supply line to Afghanistan wouldn&rsquo;t be interrupted. <br />  <br /> US national security puppet, Mike Hammer (Time), spewed forth, &ldquo;We are monitoring the situation closely. We are concerned about reports of violence and looting and call on all parties to refrain from violence and exercise restraint.&rdquo; Another case of do as we say, not as we do then. <br />  <br /> The Kremlin, who have been keen to assert their influence recently, has also called for restraint while denying that it had it&rsquo;s grubby hand behind the affair. <br />   <br /> As with all insurrections, the question remains what happens next. In Kyrgyzstan&rsquo;s case the future seems bleak, what with US attention and a transitional government already in place. Next time lets hope for a plan of action for after the destruction. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>central asia</span>, <span style='color:#777777; font-size:10px'>kyrgyzstan</span>, <a href="../keywordSearch/?keyword=russia&source=717">russia</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(729);
			
				if (SHOWMESSAGES)	{
				
						addMessage(729);
						echo getMessages(729);
						echo showAddMessage(729);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


