<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 720 - 30th April 2010 - Mayday! Mayday!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, london, mayday, election, politicians" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 720 Articles: </b>
<p><a href="../archive/news7201.php">Crash Of The Titans</a></p>

<b>
<p><a href="../archive/news7202.php">Mayday! Mayday!</a></p>
</b>

<p><a href="../archive/news7203.php">Meltdown Britain: Debts All Folks...</a></p>

<p><a href="../archive/news7204.php">Off The Rails</a></p>

<p><a href="../archive/news7205.php">Deportation Update</a></p>

<p><a href="../archive/news7206.php">Obtuse Angles</a></p>

<p><a href="../archive/news7207.php">Chinese Burn</a></p>

<p><a href="../archive/news7208.php">Radioactive Wasters</a></p>

<p><a href="../archive/news7209.php">Schnews In Brief</a></p>

<p><a href="../archive/news72010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th April 2010 | Issue 720</b></p>

<p><a href="news720.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MAYDAY! MAYDAY!</h3>

<p>
The four horseman of the apocalypse are set to swoop on central London once again, this time for an Election Meltdown Mayday march. The horsemen will drag the carcasses of four party leaders to Parliament Square, where the effigies of Gordon Brown, David Cameron, Nick Clegg and Nick Griffin will be hung, drawn and quartered - or guillotined, according to the whims of the assembled baying mob. After the symbolic excecution, a people&rsquo;s assembly will convene on the green to put the world to rights. <br />  <br /> The Silver Horse represents money meltdown and is the parade for you if it&rsquo;s bankers, bailouts, cuts, and expenses that gets yer bile flowing. It will drag David Cameron from Conservative party HQ, SW1P 4DP, at 2pm. <br />  <br /> The Red Horse represents war and is for those disgusted with the last nine years of Neo-Labour  conflict. With Gordon Brown trailing behind, it will make it&rsquo;s way to Parliament from Labour Party HQ, 39 Victoria Street, SW1H 0HA at 2pm. <br />  <br /> The Green Horse represents impending environmental disaster and will lead a column of eco-warriors. Departing Liberal Democrat HQ, 4 Cowley Street, SW1P 3NB at 2pm, it will also drag poor-man&rsquo;s Obama, Nick &lsquo;Break a&rsquo; Clegg. <br />  <br /> The Black Horse will ride under the banner of Anarchy and is for all you blac block-ers. Dragging the fetid Nick Griffin, the parade will form a contingent on the TUC march from Clerkenwell Green, EC1R 0DU, at 1pm. It will go to Trafalgar Square, then continue on to Parliament square, arriving for some gym-karma with the other horses at around 2.30pm. <br />   <br /> After last year&rsquo;s appearance at the G20 protests was reined in by repressive policing, here&rsquo;s hoping this time these harbingers of doom are chomping at the bit and ready to gallop in order to stay out of the Met&rsquo;s kettles... <br />   <br /> * See <a href="http://www.meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank">www.meltdown.uk.net/election/The_Plan_Mayday.html</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=720">direct action</a>, <span style='color:#777777; font-size:10px'>election</span>, <a href="../keywordSearch/?keyword=london&source=720">london</a>, <a href="../keywordSearch/?keyword=mayday&source=720">mayday</a>, <span style='color:#777777; font-size:10px'>politicians</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(755);

				if (SHOWMESSAGES)	{

						addMessage(755);
						echo getMessages(755);
						echo showAddMessage(755);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


