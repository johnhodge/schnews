<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 719 - 23rd April 2010 - And Finally</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, latin america, bolivia, homophobia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 719 Articles: </b>
<p><a href="../archive/news7191.php">Giving The Green Finger</a></p>

<p><a href="../archive/news7192.php">Crackdown After Death In Detention Centre</a></p>

<p><a href="../archive/news7193.php">Taken To The Cleaners</a></p>

<p><a href="../archive/news7194.php">Qulla Surprise</a></p>

<p><a href="../archive/news7195.php">Sieg Heil-lywood</a></p>

<p><a href="../archive/news7196.php">The Full English</a></p>

<p><a href="../archive/news7197.php">Put Yer Bacton Into It</a></p>

<b>
<p><a href="../archive/news7198.php">And Finally</a></p>
</b>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd April 2010 | Issue 719</b></p>

<p><a href="news719.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AND FINALLY</h3>

<p>
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/719-evo-morales-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/719-evo-morales-sm.jpg"  /></a></td></tr></table>Evo Morales, idiosyncratic leader of Bolivia, what a guy! Well, sort of, nearly. Er, but.... <br />  <br /> Speaking at the first Worldwide Peoples&rsquo; Conference on Climate Change and Mother Earth, he was hitting all the right notes, observing: &ldquo;Mankind is facing the choice of either continuing along the path of capitalism and death or to take the path of harmony with nature and respect for life so as to save mankind.&rdquo; <em>Right on!</em> <br />   <br /> He claimed the richest countries of the world owe a debt to the rest of the planet because the &ldquo;atmospheric space&rdquo; is &ldquo;filled with the emission of winter-effect gases&rdquo; and called for international borders to be lifted to allow free migration for the coming environmental refugees. <em>We&rsquo;re with you Evo, sock it to &lsquo;em!</em> He followed on to sing the praises of local Bolivian foods and folk medicines. <em>OK, we can forgive a bit of playing to the home crowd...</em> <br />   <br /> But then... &ldquo;Baldness appears to be a normal disease in Europe, almost all of them are bald, and that is because of the things they eat; while among the indigenous peoples there are no bald people, because we eat other things.&rdquo; <em>Erm, losing you a bit now mate....</em> Before winding up with claiming that homosexuality is also an egg-stra result of this fowl diet: &ldquo;The chicken that we eat is chock-full of feminine hormones. So, when men eat these chickens, they deviate from themselves as men.&rdquo; <em>Er, hang on, surely homosexuality slightly pre-dates Kentucky Fried Chicken?</em> <br />   <br /> Oh dear, we thinketh Evo doth protest too much; and the aggressive view just reflects his own self loathing for lacking the courage to come out of the closet. (&copy; all psychologists) Well, never fear, SchNEWS can help there... <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bolivia&source=719">bolivia</a>, <span style='color:#777777; font-size:10px'>homophobia</span>, <a href="../keywordSearch/?keyword=latin+america&source=719">latin america</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(753);

				if (SHOWMESSAGES)	{

						addMessage(753);
						echo getMessages(753);
						echo showAddMessage(753);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


