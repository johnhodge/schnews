<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 670 - 27th March 2009 - Apocolypse Wow</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, g20 summit, london, reclaim the streets, j18, recession, anarchy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.g-20meltdown.org/"><img 
						src="../images_main/g20-meltdown-banner.jpg"
						alt="G20 - Meltdown in the City"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 670 Articles: </b>
<b>
<p><a href="../archive/news6701.php">Apocolypse Wow</a></p>
</b>

<p><a href="../archive/news6702.php">Met Yer Match!</a></p>

<p><a href="../archive/news6703.php">Luton Vanguard</a></p>

<p><a href="../archive/news6704.php">--- In The Brown Stuff ---</a></p>

<p><a href="../archive/news6705.php">Right Shred Fred</a></p>

<p><a href="../archive/news6706.php">Edo Inside Schnews</a></p>

<p><a href="../archive/news6707.php">Huon Cry</a></p>

<p><a href="../archive/news6708.php">Famous Fife</a></p>

<p><a href="../archive/news6709.php">Saabotage</a></p>

<p><a href="../archive/news67010.php">Raven Mad</a></p>

<p><a href="../archive/news67011.php">Well Acquitted</a></p>

<p><a href="../archive/news67012.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/670-g20-dogs-lg.png" target="_blank">
													<img src="../images/670-g20-dogs-sm.png" alt="G20 Dogs" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 27th March 2009 | Issue 670</b></p>

<p><a href="news670.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>APOCOLYPSE WOW</h3>

<p>
<span style="font-size:13px; font-weight:bold">AS SchNEWS SPECULATES WILDLY ABOUT LIFE AFTER CAPITALISM</span> <br />
 <br />
This weeks G20 mobilisation in the City Of London - by calling up the ghosts of Reclaim The Streets and Stop The City, as well as incorporating elements of more recent movements such as Climate Change and anti-war campaigns &#8211; is shaping up to be one of the most important mobilisations in central London during the past decade. But, while previous Big Ones in the UK, such as the J18 protest in the City of London in 1999 (See <a href='../archive/news217.htm'>SchNEWS 217</a>) and the G8 Summit in Scotland in 2005 (See <a href='../archive/news503.htm'>SchNEWS 503</a>, <a href='../archive/news504.htm'>504</a>) took place in reaction to the unsustainable boom years or, a little later, the context of Bush and Blair&#8217;s crusade against terror, this time we&#8217;re smack bang in the middle of a full blown crisis. <br />
 <br />
The very fact that the ad-hoc G20 Summit is &#8216;needed&#8217; next week in London is confirmation that we were right all along and now it&#8217;s starting to kick off! Yep, the system was as unsustainable as it always looked, and the globalised capitalism which made us rich as it made others poor is finally reaching its end game. Poverty hasn&#8217;t been made history but is instead coming home to us. A response - albeit harsh - to those affected by the credit crunch is &#8216;welcome to the majority world&#8217;. 90% of the world&#8217;s population don&#8217;t go through consumer goods as if they fell out of a corn flakes packet, so why should we in Britain? <br />
 <br />
This crash looks like producing the kind of economic cutbacks environmentalists have been screaming out for for years. UK car production is down 60% in the past year &#8211; difficult for those who lost jobs and have families to feed - but maybe it heralds a less consumerist society. The global aviation industry is being severely hit by a large drop in demand and will lose up to $8 billion this year. Building developments are being dropped left, right and centre.   <br />
 <br />
<span style="font-size:13px; font-weight:bold">CRISIS WHAT CRISIS</span> <br />
 <br />
<blockquote>&#8220;<i>We are not in the least afraid of ruins. We are going to inherit the earth, there is not the slightest doubt about that. The bourgeoisie might blast and ruin its own world before it leaves the stage of history. We carry a new world, here, in our hearts. That world is growing this minute</i>&#8221;. - <b>Bienventura Durruti - 1936</b></blockquote> <br />
  <br />
With the cracks gaping wide in Capitalism&#8217;s shiny facade everything&#8217;s up for grabs as sections of society not used to scraping by on benefits or minimum wage will start to see just how vulnerable they really are. The test to will be whether people who grew up in the Thatcher Blair years will stand by the dog-eat-dog me-first attitudes which have become &#8216;common-sense&#8217; &#8211; which during a crisis could play into the hands of the far right. Or if they will turn to the growing set of solutions which has been developed over the past 40 years and which SchNEWS has been banging on about for the past 15: ecological and social sustainability and a equitable system. How? Using ideas gleaned from Anarchism, Ecology and anything else that has challenged the aura of inevitability that previously surrounded capitalism.  <br />
 <br />
There are many political activists in Britain and elsewhere who have both been preparing for a collapse and even cheering it on. But now the signs are here, do even the most hardened chaoto-nihilists really want to be reduced to fighting over the last tin of baked beans in the radioactive ruins of Sainsburys? Even stockbrokers may find themselves turning to previously marginal ideas, long the hobby-horse of apocalypse-fetishists and 60s beardy-weirdies, such as self sufficiency and non-capitalist modes of organisation. <br />
 <br />
The recession is already biting in certain key areas. The cost of basic living has shot up in the past 12 months: power has gone up 22%, food 11%, and transport fares 8%. Power, food and transport rank alongside housing, education, water, and health as essentials in any functioning society. If the capitalist state can no longer provide these affordably (or at least the illusion of them), then communities must take things into their own hands.  <br />
 <br />
Take just those three &#8211; power, food and transport. The key to wresting these from the clutches of multinational capital is self-sufficiency, conservation and the co-operative pooling of resources. <br />
It&#8217;s obvious that houses shouldn&#8217;t be so reliant on external power &#8211; and held to ransom by energy cartels. Not only should they be more energy efficient but also be able to generate their own power, renewably. Energy generation at all levels has to also come from renewable sources &#8211; it&#8217;s a no brainer. Currently with our government refusing to properly invest in renewables, energy companies like Shell are cutting back on their already meagre sustainable energy budgets. <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/670-billgates-brown-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/670-billgates-brown-sm.jpg"  alt='No Gordon, I can't lend you 500 billion quid until 2050...'  /></a></td></tr></table>Food is even more essential than power, and the average Brit is more of a hostage to a small number of international corporations than ever before (See <a href='../archive/news602.htm'>SchNEWS 602</a>). In the near future we can expect to see scenes reminiscent of post-war Britain, when people turned to allotments during the times of rations and scarcity. Allotments already made a comeback over a decade ago as people resisted the hegemony of the supermarkets, but the revival may have only just begun. Economies of scale should follow with an increasing need for more localised farming and co-operatively run food growing. Out of necessity, many people who&#8217;d never before imagined it possible may well find themselves involved in food production. Councils have so far refused to meet this growing demand with years-long waiting lists for a bit of yer own land, so now is the time for residents to take unused land into their own hands for a spot of guerilla gardening. With the high costs of organic food out of the reach for those forced to rely on the chemically saturated fare of budget supermarkets, allotments are the best option for the production of wholesome food. <br />
 <br />
Transport is not so life-or-death, but is still a basic necessity. However until anti-gravity devices and water powered engines are invented, we are reliant on oil. But again, economic forces are about to severely limit this oil consumption - and that includes the transportation of workers, food and other commodities. Once more, this points to localisation: more locally grown food &#8211; to lower food miles, and less commuting to work. If far fewer people are able to run a car, development of public transport infrastructure will be key and bike use will go up - great news for the smog-choked cities of Britain and everywhere else. Anyhow, countries much poorer than Britain manage to maintain public transport systems better than ours - which is no surprise given that it&#8217;s always been a policy of UK Plc to pander to the road lobby. <br />
 <br />
At this point it is more important than ever that people have these discussions and are informed of these potential solutions. A set of new/old ideas are about to have their day. Maybe people might think that they&#8217;d need to be dragged kicking and screaming into wearing tofu sandals in a vegan tree village, but actually they may find that the values of mutual aid and cooperation have always run through society. They may even come to relish the sense of community which could emerge from all this, and not want to go back to the atomised society they&#8217;ve left behind.  <br />
 <br />
Direct action will of course build up as protests attract larger groups of pissed-off people, and its targets will get more and more specific. This is the big difference between J18 and this week&#8217;s G20: J18 was a mobilisation against capitalism itself &#8211; not a specific definable target &#8211; while for today&#8217;s G20 protesters, they know exactly who to hold accountable.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>anarchy</span>, <a href="../keywordSearch/?keyword=g20+summit&source=670">g20 summit</a>, <span style='color:#777777; font-size:10px'>j18</span>, <a href="../keywordSearch/?keyword=london&source=670">london</a>, <span style='color:#777777; font-size:10px'>recession</span>, <span style='color:#777777; font-size:10px'>reclaim the streets</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(257);
			
				if (SHOWMESSAGES)	{
				
						addMessage(257);
						echo getMessages(257);
						echo showAddMessage(257);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>