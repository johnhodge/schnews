<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 713 - 12th March 2010 - A-fence-is Weapon</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, arms trade, court case" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 713 Articles: </b>
<p><a href="../archive/news7131.php">Sussex, Lies And Videotape</a></p>

<p><a href="../archive/news7132.php">Schcoop! - No Cutbacks For Sussex Fat Cats</a></p>

<p><a href="../archive/news7133.php">Sea Shepherd Roundup : Whaling Banshees</a></p>

<b>
<p><a href="../archive/news7134.php">A-fence-is Weapon</a></p>
</b>

<p><a href="../archive/news7135.php">Hungry For Reform</a></p>

<p><a href="../archive/news7136.php">Hey Joe</a></p>

<p><a href="../archive/news7137.php">Coal-amity</a></p>

<p><a href="../archive/news7138.php">Tesco A No Go</a></p>

<p><a href="../archive/news7139.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th March 2010 | Issue 713</b></p>

<p><a href="news713.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A-FENCE-IS WEAPON</h3>

<p>
After nearly two years of stressful legal wranglings, five Smash EDO activists are facing possible imprisonment for allegedly shaking a crowd barrier at the <strong>Carnival Against the Arms Trade</strong> in June 2008 in Brighton. <br />   <br /> Some of them were seriously assaulted by police during the demo, and then suffered multiple arrests, house searches and changes of charges. <br />   <br /> Their &lsquo;grievous crimes&rsquo; have been investigated by a highly motivated team of detectives from Sussex Police&rsquo;s Major Crime Unit. Their case is constructed from hundreds of evidence exhibits and police witness statements by the Crown&rsquo;s Complex Prosecutions Department, at an estimated cost of over a million quid. <br />   <br /> Sinister political police spy and professional wrong &lsquo;un Sean McDonald seems to have finally lost the plot, producing pages of obscure drivel detailing how he has been following certain activists around for the past five years. <br />   <br /> The three-week trial, during which we presume every split-second of the alleged fence-shaking will be exhaustively examined, begins at Brighton Magistrates&rsquo; Court, on 16th March. <br />   <br /> All five are on trial under Section 4 for &lsquo;fear or provocation of violence&rsquo; along with some more minor offences.  The original Section 2 &lsquo;violent disorder&rsquo; charges were dropped as they may have led to a jury trial where acquittals are more common The defendants have appealed for support at the trial. <br />  <br /> *Smash EDO are holding a picket at Barclays Bank on North Street at 12pm on March 17th to mark the seventh anniversary of the Iraq war &ndash; Barclays are ITT&rsquo;s market maker and the largest UK investor in the arms trade. <br />   <br /> There will also be a &lsquo;Horrors of War&rsquo; noise demo at the EDO&rsquo;s factory from 3pm-6pm. Bring noise-making equipment, bloody clothes, coffins. <br />  <br /> *See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=713">arms trade</a>, <span style='color:#777777; font-size:10px'>court case</span>, <a href="../keywordSearch/?keyword=smash+edo&source=713">smash edo</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(691);
			
				if (SHOWMESSAGES)	{
				
						addMessage(691);
						echo getMessages(691);
						echo showAddMessage(691);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


