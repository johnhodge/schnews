<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 791 - 7th October 2011 - First We Take Manhattan</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 791 Articles: </b>
<p><a href="../archive/news7911.php">Nhs Direct Action</a></p>

<p><a href="../archive/news7912.php">Welling Up</a></p>

<p><a href="../archive/news7913.php">Angel Delight</a></p>

<p><a href="../archive/news7914.php">Squat A Waste Of Time</a></p>

<p><a href="../archive/news7915.php">Core Values</a></p>

<p><a href="../archive/news7916.php">Factory Finish?</a></p>

<b>
<p><a href="../archive/news7917.php">First We Take Manhattan</a></p>
</b>

<p><a href="../archive/news7918.php">'cos I'm Worthing It</a></p>

<p><a href="../archive/news7919.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th October 2011 | Issue 791</b></p>

<p><a href="news791.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FIRST WE TAKE MANHATTAN</h3>

<p>
<p>  
  	It all started back on the 17th of September when a few dozen protesters tried to pitch tents outside the New York Stock Exchange. When they were blocked by police, hundreds camped out opposite the NYSE to take their anti-capitalist message to the belly of the beast. <br />  
  	 <br />  
  	Since then they and others have been camped out on Liberty Park (or, rather the park formerly known as Liberty Park, now renamed Zuccotti Park after it was bought out by a corporation called Brookfield Office Properties). Post &lsquo;Arab Spring&rsquo;, mass movements around the world have begun to demand the impossible, and the global wave of revolt has even spread to USA. <br />  
  	 <br />  
  	The US public has witnessed the total failure of the Obama administration to offer anything even remotely like an alternative to the neo-con-neo-liberal consensus that has hacked away at the rights and wealth of the population of the worlds richest and most powerful country. Today, something like one in four US children are born into poverty. The 99% are taking action against the greed and corruption of the 1%. <br />  
  	 <br />  
  	For weeks the protesters had to deal with an near total blackout from the mainstream media. But in the age of live streaming, blogs and twitter feeds the techies amongst them (i.e. most of them) are managing to get their own messages out. Instead of suffocating the movement, the demonstrators have proved adept at getting the news out to wider and wider audiences. So when the police attacked with batons and pepper spray, arresting 80 people, the whole thing was streamed around the world, leading to a massive wave of support. There is also the occupation&rsquo;s very own newspaper, the Occupied Wall Street Journal. <br />  
  	 <br />  
  	On the 1st of October 700 people were arrested when the movement tried to march on Brooklyn Bridge. The police lured the protesters into a false sense of security, letting them believe that they were allowed to march on the road, only to move in and nick the lot for not being on the sidewalk. Police used commandeered public buses to ferry the arrestees out. A class action has been brought against the NYPD. <br />  
  	 <br />  
  	Oct 5th saw the largest crowds yet, when some 10-15 thousand workers and students marched in solidarity with the occupiers. 28 people were arrested. In effect the protesters have liberated a space for the benefit of any and every campaign/movement for social justice. Crowds demonstrating against the deficit cuts, government bailouts and Afghan War are able to exploit the numbers and confidence of occupied Wall Street. The movement has spread to cities all around the USA, nominally grouped under the occupytogether.org umbrella. Occupations and massive demos in the very centre of American power has a huge effect- the government has been forced against its will to listen to the left out. A government elected on empty slogan of &lsquo;change&rsquo; now has to face a movement that has real, ideas for radical change to the order of wealth and power. Stay tooned folks!  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1384), 160); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1384);

				if (SHOWMESSAGES)	{

						addMessage(1384);
						echo getMessages(1384);
						echo showAddMessage(1384);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


