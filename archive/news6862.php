<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 686 - 7th August 2009 - Plugging Into The Mainshill</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, camp for climate action, scotland, climate change, coal" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 686 Articles: </b>
<p><a href="../archive/news6861.php">Blowing In The Wind</a></p>

<b>
<p><a href="../archive/news6862.php">Plugging Into The Mainshill</a></p>
</b>

<p><a href="../archive/news6863.php">Rabbiting On</a></p>

<p><a href="../archive/news6864.php">Rotten To The Caucasus</a></p>

<p><a href="../archive/news6865.php">The Garda They Come</a></p>

<p><a href="../archive/news6866.php">Down Under Fire</a></p>

<p><a href="../archive/news6867.php">Jungle Missive</a></p>

<p><a href="../archive/news6868.php">Yeah Yeah Yeah</a></p>

<p><a href="../archive/news6869.php">Afghan Hounding</a></p>

<p><a href="../archive/news68610.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th August 2009 | Issue 686</b></p>

<p><a href="news686.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PLUGGING INTO THE MAINSHILL</h3>

<p>
The first instalment of the UK climate camp trilogy of low-impact living and high-impact direct action opened this week with the Scottish Camp For Climate Action. The Mainshill Solidarity Camp, on a planned open-cast coal site in Lanarkshire near Glasgow, is the venue &ndash; and though the camp goes from 3rd-10th August, the protest site will of course continue afterwards. This new woodland protest occupation was taken in mid-June after Scottish Coal were given permission to mine despite the opposition of the local district (See <a href='../archive/news681.htm'>SchNEWS 681</a>) and has since become a sustainable community.&nbsp; <br />  <br /> Some local residents live at the site, which contains tree houses and tunnels, whilst others have been campaigning against Scottish Coal&rsquo;s applications to expand its coal-mining operations in the Douglas Valley for years. However, their objections to the trashing of their local environment have been consistently ignored by the authorities, and owner of the land - Lord Home - who is being investigated for a &pound;150m money laundering fraud. Just three weeks ago they learned of another proposal for open-cast coal mining in the valley. <br />  <br /> Drilling at Mainshill had been carried out by Welsh-based contractor Apex. Preparatory work for the mine by Apex such as tree-felling and bore sample drilling had already breached seven conditions of the planning approval, with regards to studies to ascertain the presence of certain endangered species such as badgers and bats, which the occupiers know to be present on site. Open-cast mines have been operational in the area for the last four years, and pulmonary illnesses have risen by 60% in the same time.&nbsp; <br />  <br /> Direct action is already underway though, with anonymous activists disabling a conveyor belt on Wednesday night (5th) at Scottish Coal&rsquo;s Glentaggart site. This belt carries coal from the mine to Drax power station in Yorkshire (the site of the first Climate Camp 2006 &ndash; see <a href='../archive/news558.htm'>SchNEWS 558</a>). <br />  <br /> Over 100 campers are already at the location expanding the camp and police presence has been low-key so far with cameras set up on the perimeters, although word was leaked last night of all police leave in the central section of Scotland suspended, with forces on high alert. So far, however, there has been none of the harassment of Kingsnorth last summer. In the next week there will be workshops, discussions and opportunities to link up with other people, groups and campaigns. <br />  <br /> * See <a href="http://climatecampscotland.org.uk" target="_blank">http://climatecampscotland.org.uk</a> and <a href="http://coalactionedinburgh.noflag.org.uk" target="_blank">http://coalactionedinburgh.noflag.org.uk</a> <br />  <br /> * When this camp finishes, it&rsquo;s off to Pembrokeshire for the Welsh Camp For Climate Action, August 13th-16th &ndash; see <a href="http://climatecampcymru.org," target="_blank">http://climatecampcymru.org,</a> then the London camp August 27th-Sept 2nd - see <a href="http://climatecamp.org.uk&nbsp;" target="_blank">http://climatecamp.org.uk&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=686">camp for climate action</a>, <a href="../keywordSearch/?keyword=climate+change&source=686">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=686">coal</a>, <a href="../keywordSearch/?keyword=direct+action&source=686">direct action</a>, <a href="../keywordSearch/?keyword=scotland&source=686">scotland</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(423);
			
				if (SHOWMESSAGES)	{
				
						addMessage(423);
						echo getMessages(423);
						echo showAddMessage(423);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>