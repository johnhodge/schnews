<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 722 - 14th May 2010 - Not A Clegg To Stand On</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, david cameron, tory, financial crisis, greece" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 722 Articles: </b>
<p><a href="../archive/news7221.php">Squaring Up To 'em</a></p>

<b>
<p><a href="../archive/news7222.php">Not A Clegg To Stand On</a></p>
</b>

<p><a href="../archive/news7223.php">Decommissioners Trial Delayed</a></p>

<p><a href="../archive/news7224.php">Glade Rags</a></p>

<p><a href="../archive/news7225.php">Sweaty Palms At Unilever</a></p>

<p><a href="../archive/news7226.php">Raising Arizona</a></p>

<p><a href="../archive/news7227.php">Bounty Bar-stards</a></p>

<p><a href="../archive/news7228.php">Lab- Lib Pact</a></p>

<p><a href="../archive/news7229.php">Crude Awakening</a></p>

<p><a href="../archive/news72210.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th May 2010 | Issue 722</b></p>

<p><a href="news722.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOT A CLEGG TO STAND ON</h3>

<p>
So the honeymoon is underway for Nick &amp; Dave, who surprised friends and relatives with a shotgun wedding this week, despite having been heard for a number of years forcefully listing all the reasons why the other is a complete twat. But what are the prospects for a happy marriage &ndash; as far as the rest of us are concerned?  <br />  <br /> Welcome to the real arrival of the Age of Austerity, due since the day the financial crisis broke but propped up and put back til the election was finally over. <br />  <br /> Swinging cuts across the whole public services sector (well you can&rsquo;t expect them to skimp on &lsquo;defence&rsquo;!) is not likely to ingratiate them with anybody but bankers and the richest slice of society.  <br />  <br /> And of course cuts don&rsquo;t begin to cover the black hole of debt. Large tax hikes - direct and indirect - are inevitable (a VAT rise looks likely for instance - which taxes the poorest most) as well as a certain amount of deliberate inflation (such as, but not limited to the recent &lsquo;quantitative easing&rsquo;). This is basically another indirect tax which reduces the real value of everybody&rsquo;s assets in order to make the fixed-amount government debts smaller in real terms.  <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/722-cameron-clegg-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/722-cameron-clegg-sm.jpg"  alt='Cameron and Clegg'  /></a></td></tr></table>And then there&rsquo;s Europe &ndash; the biggest crack in the Cam-Clegg cabal &ndash; which is lurching through a period of financial crisis and threatens to make everyone&rsquo;s recession even worse.&nbsp; The just-announced trillion dollar bailout is just that -&nbsp; a bailout &ndash; not any kind of solution to the underlying problems.  <br />  <br /> It&rsquo;s papering over the cracks and basically the &lsquo;richer&rsquo; countries &ndash; those with smaller, more manageable debt mountains than the others &ndash; have agreed to borrow even huger amounts to prop up the system in the short term. Much like the global minimal action on the environment, it fails to recognise that delaying an inevitable crisis only makes its impact bigger and more overwhelming when it eventually happens. <br />  <br /> Politicians are scared of the market&rsquo;s reactions to, and the political consequences of, allowing Greece (and potentially others) to default. It could kill the Euro, and lead to a crisis of confidence threatening the whole euro-superstate project. So they&rsquo;re throwing the kitchen sink at keeping the status quo going. <br />  <br /> But without debt restructuring and cancellation, some economists (even MOR FT types) think that all the problems faced down now will be back, bigger and more uncontainable, within three years. <br />  <br /> And it could be sooner than that if the Greeks don&rsquo;t start meekly accepting their bitter austerity pills more readily than they have been. Pausing after an intense period of mass riots, deaths and pitch battles with riot cops (see <a href='../archive/news711.htm'>SchNEWS 711</a>, <a href='../archive/news720.htm'>720</a>), more general strikes and actions are planned, starting on May 20th. The elites had better hope than the Spanish, Portuguese, Irish and ...er, British aren&rsquo;t watching and getting any ideas about how to respond when the next levels of Austerity get imposed...&nbsp;&nbsp;  <br />  <br /> &ldquo;<i>A strong, stable government in the national interest</i>&rdquo; - a soundbite which will look increasingly laughable from here on in...
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=david+cameron&source=722">david cameron</a>, <a href="../keywordSearch/?keyword=financial+crisis&source=722">financial crisis</a>, <a href="../keywordSearch/?keyword=greece&source=722">greece</a>, <a href="../keywordSearch/?keyword=tory&source=722">tory</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(774);

				if (SHOWMESSAGES)	{

						addMessage(774);
						echo getMessages(774);
						echo showAddMessage(774);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


