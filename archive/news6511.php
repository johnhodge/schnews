<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 651 - 17th October 2008 - Pepperazzi</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, shut itt!, fit watch, edo-mbm, itt, brighton, edo-mbm" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/shut-itt.htm"><img 
						src="../images_main/shut-itt-banner.png" 
						width="465" 
						height="90" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 651 Articles: </b>
<b>
<p><a href="../archive/news6511.php">Pepperazzi</a></p>
</b>

<p><a href="../archive/news6512.php">Crap School Suspension Of The Week</a></p>

<p><a href="../archive/news6513.php">Bank Job</a></p>

<p><a href="../archive/news6514.php">Upper Class War</a></p>

<p><a href="../archive/news6515.php">Careering Off</a></p>

<p><a href="../archive/news6516.php">'allo 'allo</a></p>

<p><a href="../archive/news6517.php">Black'n'read All Over</a></p>

<p><a href="../archive/news6518.php">White No Sugar</a></p>

<p><a href="../archive/news6519.php">Flight Fingered</a></p>

<p><a href="../archive/news65110.php">Free Speech Impediment</a></p>

<p><a href="../archive/news65111.php">And Finally 651</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th October 2008 | Issue 651</b></p>

<p><a href="news651.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>PEPPERAZZI</H3>

<p>
<b>COPS ATTACK AS SMASH EDO DEMO PAINTS THE TOWN RED AGAIN...</b><br />
<br />
Over 400 people gathered in Brighton on a very wet Wednesday in the latest protest called by Smash EDO against local bomb factory EDO MBM/ITT. The last demo, in June (See <a href="news634.htm">SchNEWS 634</a>), saw protesters invading the factory grounds and smashing windows. This time the cops were determined to have an overwhelming presence. At noon Sussex University campus was occupied by large gangs of cops as people arrived at the demo�s start point at the uni entrance. <br />
<br />
Police tactics soon became obvious. As the crowd gathered they issued a Section 60 notice, giving them the power to remove masks. Trying to stamp their authority, they quickly set about the gathering crowd demanding people remove any kind of face covering, photographing everyone and generally using any tactics to intimidate, attempting to seize banners and alienate as many onlookers as possible.<br />
<br />
Initially shocked � the mob soon found the resources to fight back and just after midday the march burst into life. The red and black-clad crowd sprang into action to the rallying call �Get behind the banner�. Behind the sturdy, massive SHUT ITT! banner - reinforced with a wooden frame - and waving flags, the noisy bloc moved at pace through Stanmer Park and out onto the Lewes Road, filling both lanes. <br />
<br />
At least ten pairs of police evidence gatherers with long lenses, video cameras and spotter cards, including the Met�s Forward Intelligence Teams (FIT) were in evidence from the start but spent most of the march foiled by FIT Watchers (See <a href="news639.htm">SchNEWS 639</a>). Hundreds of copies of FIT Watch�s spotter cards were distributed complete with photos, names, numbers and descriptions of  FIT police likely to be in attendance. Whenever FIT teams appear, shouts of �Block That Shot� is becoming a call to arms for activists sick of only being able to protest whilst constantly under surveillance. <br />
<br />
<b>CAGE FIGHTING</b><br />
<br />
Multiple sound systems and makeshift instruments kept spirits up in the procession towards the factory. Police seemed to have taken a leaf from the anarchist book and handily blockaded the whole of Lewes Road for several hours, with 8 vans bumper to bumper, urging people towards their sanctioned �protest pen� at the bottom of Home Farm road. Unsurprisingly the idea of being herded into a massive steel cage surrounded by a sea of fluorescent baton wielding cops didn�t appeal to anyone. Determined to march on, people surged towards the police lines, pushing the cops back behind their line of vans. Heavy use of pepper spray and batons on those at the front took the sting out of the crowd, who, nursing bruised bodies and the ill effects of an impromptu chemical eye-bath at the hand of Sussex�s finest, split into two groups. Half the crowd stood their ground, eyeballing the cops, whilst others in small groups gradually headed off-piste, up the slope and into the woods towards the back of the factory.<br />
<br />
<b>PUT THE KETTLE ON</b><br />
<br />
Marauding bands of masked militants swarmed through the forest, whilst clueless coppers couldn�t see the hoods for the trees. In a hail of irony, laser-guided paint-missiles bombarded the factory�s roof - staining the factory walls blood-red in a spot of unrequested decoration. After scuffles in the woods and open fields where someone narrowly avoided castration via a police dog bollock-biting attack, a group of about 50 managed to reclaim Lewes Road nearer town before being joined soon after by other cross-country cells and marched towards town. <br />
<br />
By half two, the crowd on Lewes Road had begun to disperse and the police line had moved across the road, freeing up one lane of traffic. The remaining crowd were able to launch themselves down Lewes Road towards the Level. Fearing that 100 or so anarchists might not cause enough trouble, the cops kindly contributed towards the mayhem by sending some 25 vehicles to create a police traffic jam stretching halfway down the road. <br />
<br />
When the converged marchers arrived at the Level (traditional end point of Brighton demos), police backed off, thinking that the crowd had had enough. As it turns out, the up-fer-it protesters saw the police begin to disperse and made a break for it to storm the city centre, with the local Army recruitment centre as a goal. Still singing and chanting, they carried on, pursued by police until they were finally kettled near Queens Road. Seeing their plight, locals started harassing the cops, kettling in the kettle and throwing food and water to the stalwart marchers. Police eventually followed the protesters to the beach for their final push, where they nicked a pebble-thrower.<br />
<br />
With a total of around ten arrests, one sore scrotum, plenty of bruised knees, inflamed sinuses, and stinging eyes, the last 100 intrepid protesters completed the 5� mile anti-arms trade mini marathon to bathe aching feet in the sea. Andrew Beckett, spokesperson said �<i>We didn�t let the police control events. We went where we wanted, when we wanted. All the police from four counties weren�t able to stop us making our stand against EDO/ITT</i>�.<br />
<br />
* See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a><br />

</p>

</div>


<br /><br />

		</div>
		
		
		
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>