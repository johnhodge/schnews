<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 711 - 26th February 2010 - Greek Fire</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, riots, protest, economy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 711 Articles: </b>
<b>
<p><a href="../archive/news7111.php">Greek Fire</a></p>
</b>

<p><a href="../archive/news7112.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7113.php">A Different Kettle Of Fash</a></p>

<p><a href="../archive/news7114.php">Political Climate</a></p>

<p><a href="../archive/news7115.php">Columbia: Struck Oil</a></p>

<p><a href="../archive/news7116.php">Titnore: Barking Mad</a></p>

<p><a href="../archive/news7117.php">Inside Schnews</a></p>

<p><a href="../archive/news7118.php">When Hunger Strikes</a></p>

<p><a href="../archive/news7119.php">Ra Ra Rash-putin</a></p>

<p><a href="../archive/news71110.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/711-300-poster-lg.JPG" target="_blank">
													<img src="../images/711-300-poster-sm-1.JPG" alt="Even the Spartans wouldn't accept these austerity measures!..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 26th February 2010 | Issue 711</b></p>

<p><a href="news711.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREEK FIRE</h3>

<p>
<strong>GREEK PROTESTS BOOM AS THE ECONOMY BUSTS...</strong> <br />  <br /> Greece has once again been rocked by protests, strikes and civil unrest. This time in response to a series of swinging government cutbacks aimed at bringing the country into line with neo-liberal dogma and reducing its budget deficit. In response to proposed attacks on workers&rsquo; rights and pensions, virtually the entire country came out on strike on Wednesday. 30,000 marched through Athens and violently clashed with police. There is widespread anger at the governments attempts to deal with the economic crisis by dipping into the pockets of the poor.  Marchers in Athens shouted, &ldquo;<strong>No sacrifices! Make the rich pay for the crisis!</strong>&rdquo; <br />  <br /> This is the second national strike to hit Greece in two weeks. The first, by public sector workers (almost 1 in 5 Greeks fall into this category), grounded flights and shut many state schools and offices. The second general strike was far broader, all flights to and from Greece were cancelled and  public transport ground to a near-standstill, with most trains and ferries standing idle. Schools, tax offices and council buildings remained closed. One group of metal-workers in Athens, satisfied that the strike at their own plant was rock-solid, went and blockaded their bosses&rsquo; workplace and physically ejected them. <br />  <br /> Europe&rsquo;s four poorest countries, Spain, Portugal, Ireland and Greece, are all facing similar programs designed to restore stability to the Euro. All are being forcefully encouraged by the EU big boys, with IMF backing, to introduce neo-liberal Thatcherite reforms, such as privatisation, raising of the pensionable age and cutting of public services. If Greece wants to stay in the Euro then it has to cut its budget deficit from 12.7% to 3%. These cuts will come at the expense of society&rsquo;s most marginalised. <br />  <br /> The Greek government is headed up by the centre-left party PASOK. They came to power after the previous government was forced out of office during the wave of social unrest precipitated by the shooting of Alexis Grigoropoulos (see <a href='../archive/news659.htm'>SchNEWS 659</a>). <br />   <br /> Although Alex&rsquo;s shooting sparked the rioting, it was fuelled by the economic downturn and the squeeze on Greece&rsquo;s workers. PASOK&rsquo;s bid for power was based on promises it couldn&rsquo;t keep -  to win the election they promised no wage freezes and a guaranteed pension. Previously, a government in their situation might just try and print money and inflate their way out of the crisis &ndash; not a solution open to Greece&rsquo;s leaders owing to the country&rsquo;s membership of the Euro zone. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/711-GreekGenStrikeRiot-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/711-GreekGenStrikeRiot-sm.jpg"  alt='Riots in Greece'  /></a></td></tr></table>The measures proposed include a rise in the retirement age, slashing of state bonuses and controls to diminish tax evasion together with higher taxes on fuel, tobacco and alcohol. However, any austerity measures will have to be forced through in a country with a population already verging on open revolt. A shock doctrine approach isn&rsquo;t going to catch the Greeks by surprise. So far the government has floundered and attempted to pin the blame elsewhere, with deputy Prime Minister Theodoros Pangalos excusing Greece&rsquo;s cooking of the books to join the Euro in the first place by saying, &ldquo;It is what everybody did and Greece did it to a lesser extent than Italy&rdquo;. He then went one better and blamed Greece&rsquo;s economic woes on the theft of bullion by the Third Reich in 1944.  Ah the old classic &ldquo;Nazi Gold&rdquo; excuse &ndash; who hasn&rsquo;t used that to get out of tight corner? <br />   <br /> On top of the national strike, protests hit Athens and Thessaloniki. The 7,000 strongdemonstration in Thessaloniki lasted for a few hours and went off without any clashes with police. Department stores that remained open got targeted by protesters, either by having their walls covered in graffiti or, in the case of cosmetics chain &ldquo;Hondos Centre&rdquo;, had rubbish scattered across the department&rsquo;s shop floor. The protest reached its conclusion outside the ministry with the burning of the EU flag. Only then did Greek police make an appearance, ten of them showing up, but keeping their distance. <br />   <br /> In contrast, the protests in Athens were explosive. More than 30,000 took part in two marches, one headed up by the Greek Communist Party. This was described by a SchNEWS eyewitness as &lsquo;boring&rsquo;. The other consisted of an amalgamation of libertarian communists, trotskyists and anarchists alongside striking workers from the Ministry of Economics. Particularly impressive was the mobilisation of recent immigrants to Greece, who formed their own heavily policed block. <br />  <br /> As the second march approached parliament at around 2pm, the police attempted to split it. According to our man on the scene, &ldquo;<em>It completely kicked off &ndash; people were throwing bricks and also bread was looted from a nearby bakers handcart &ndash; which was later used as a battering ram against police lines. One thing is clear, people&rsquo;s hatred of the police hasn&rsquo;t gone anywhere since December</em>.&rdquo;A few molotovs were thrown and violence continued sporadically throughout the day, with anarchists attacking banks. <br />  <br /> The majority of protesters were disgruntled workers from the country&rsquo;s two largest trade unions. A faction of young protesters entered a university building. Riot police followed, even though under article 16 of the Greek constitution, no police forces are allowed to enter university grounds. They withdrew to outside the gates soon after and dealt with the youths by firing tear gas. Further clashes followed, with police firing tear gas at protesters as the march made their way through the Athens streets. Greek newspapers reported that police arrested around 30 people during the protest and 23 police officers were hospitalised. <br />  <br /> At the moment, neither side is backing down. The government claims it is going to push forward with the cutbacks, while workers&rsquo; organisations have vowed another general strike on March 15. <br />  <br /> With Spanish trade unions also gearing up for a general strike and large scale protests in response to similar proposals for cutbacks - it looks as if the neo-liberal  Europe proposed in the Lisbon Treaty might be in serious crisis.   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>economy</span>, <a href="../keywordSearch/?keyword=greece&source=711">greece</a>, <a href="../keywordSearch/?keyword=protest&source=711">protest</a>, <a href="../keywordSearch/?keyword=riots&source=711">riots</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(669);
			
				if (SHOWMESSAGES)	{
				
						addMessage(669);
						echo getMessages(669);
						echo showAddMessage(669);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


