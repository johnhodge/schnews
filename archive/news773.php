<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(142);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	142;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 773 - 27th May 2011 - As mass protests in Madrid raise the bar for European anti-austerity actions</title>
<meta name="description" content="Pro-Real Democracy protests spread, As anti-G8 protests largely quashed in France, New Immigration Removal Centre opens next week, and more..." />
<meta name="keywords" content="SchNEWS, direct action, Brighton, france, le havre, anti-globalisation, anti-capitalism, immigration, migrants, swinderby, g20, ian thomlinson, police, real democracy now, spain, georgia, greece, bristol" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news772.htm">SchNEWS 772, 20th May 2011</a></b><br />
<b>A Bit of Hows Yer Intifada?</b> - 
	A mass non-violent (or at least unarmed) resistance movement is on the move in Palestine. Inspired by the events in neighbouring Arab countries but drawing on decades of resistance - the Third Intifada may be here. The waves of the Arab movement are beginning to lap at the Israeli shore.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news771.htm">SchNEWS 771, 13th May 2011</a></b><br />
<b>Fracking Hell</b> - 
	A few months since Fukishima, nearly a year since Deepwater Horizon but the global elite aren&rsquo;t resting on their laurels and it looks like we won&rsquo;t have to wait too long for the fossil fuel industry to cause the next environmental apocalypse. New kid on the block is the appropriately named &lsquo;fracking&rsquo; &ndash; or, more explicitly: hydraulic fracturing, a method of extracting natural gas from shale rock layers thousands of feet deep &ndash; and its coming here soon.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news770.htm">SchNEWS 770, 6th May 2011</a></b><br />
<b>Baa Baa Black Block</b> - 
	Operation Brontide swung into full force last week, coincidentally just before the Royal wedding. Cops claim to be after 276 people for offences including violent disorder and criminal damage committed during the March for the Alternative on 26th March. In fact this is&nbsp; a legally dubious fishing exercise designed to seize equipment and display state power.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news769.htm">SchNEWS 769, 29th April 2011</a></b><br />
<b>Stoking the Fires</b> - 
	Last Thursday (21st), the Stokes Croft area of Bristol was the scene of violent clashes between police and protesters of&nbsp; he kind not seen in the city since 1980. The hours of rioting that lasted from 9pm until five or six the following morning were prompted firstly by the eviction of the Telepathic Heights squat, and then tapped into deep anger in the community against the opening of another unwanted Tesco store
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news768.htm">SchNEWS 768, 22nd April 2011</a></b><br />
<b>Watch My Lips</b> - 
	A hunger strike by 6 Iranian asylum seekers, five of whom have sewn their mouths shut, is currently in progress in London. Three are camping outside Amnesty International&#39;s offices in Clerkenwell, and three are at the Home Office in Croydon.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news767.htm">SchNEWS 767, 15th April 2011</a></b><br />
<b>Taking Libya-Ties</b> - 
	Medyan Daireh, a Brighton-based Al Jazeera journalist who has previously covered conflicts in Iraq, Palestine, Afghanistan, Turkey and Kosovo has just returned from a stint in Libya. He covered the uprising against Colonel Gadaffi, spent weeks living with the rebels and visited all of the rebel-controlled areas. SchNEWS took the opportunity to get his views on the rebels&rsquo; struggle and the situation in Libya:
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news766.htm">SchNEWS 766, 8th April 2011</a></b><br />
<b>House Demolition</b> - 
	A police-escorted bulldozer razed the inside of migrant squat Africa House in Calais last Thursday (31st). In the early afternoon, large numbers of police descended on the site, home to tens of African migrants since the first Africa House was demolished last June.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news765.htm">SchNEWS 765, 1st April 2011</a></b><br />
<b>M26: Ritz Spirit</b> - 
	Well - somebody had to do it. The TUC&rsquo;s glad-hand fest was never going to change the ConDEMS course by even one degree. It was hijacked by anarchists and it deserved to be. The TUC mobilised its masses and did everything they could to make sure that genuine dissent didn&rsquo;t rear its ugly head. The emphasis was to be on a family-friendly day out without frightening the horses. Quite who they think is going to hand out prizes for niceness in the shark tank that is global capitalism remains a mystery.
	&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news764.htm">SchNEWS 764, 25th March 2011</a></b><br />
<b>No Flies on Us</b> - 
	It&rsquo;s March, so it&rsquo;s time for a new war. To no-one&rsquo;s great surprise we&rsquo;re bombing the Arabs again. A few facts for history buffs - not only did &ldquo;Operation Defend Libyans From Bombs By Bombing Libyans&rdquo; start almost exactly seven years after the start of the Iraq War, but 2011 also marks one hundred years of aerial bombardment. The target of those first ever bombs dropped from planes in 1911? Libya
</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">
									<div align="center" class='issue_main_image'>
										<a href="../images/773-homage-lg.jpg" target="_blank">
											<img src="../images/773-homage-sm.jpg" alt="" border="0" >
											<br />click for bigger image
										</a>
									</div>
								
<p><b><div style='width: 40%; padding: 10px 0px 10px 0px; border-bottom: 1px solid #bbbbbb; border-top: 1px solid #bbbbbb; margin: 10px 0px 10px 0px'>Friday 27th May 2011 | Issue 773</div></b></p>
<p><b><i>WAKE UP!! IT'S YER WEB ONLY...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news773.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news773.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news7731.php">No Spain, No Gain</a>  |  <a href="../archive/news7732.php">Le Harve It Large</a>  |  <a href="../archive/news7733.php">Mort In A Storm</a>  |  <a href="../archive/news7734.php">Baton Charged</a>  |  <a href="../archive/news7735.php">And Finally</a> 
</p>


<div id="article">

<h3>NO SPAIN, NO GAIN</h3>

<p>
<p>  
  	<strong>AS MASS PROTESTS IN MADRID RAISE THE BAR FOR EUROPEAN ANTI-AUSTERITY ACTIONS</strong> <br />  
  	 <br />  
  	We&#39;ve got the Arab Spring &ndash; what about a European summer? It&#39;s a week since thousands of pro-democracy demonstrators pitched up in central Madrid (see <a href='../archive/news772.htm'>SchNEWS 772</a>) - and revolution fever is spreading across Europe like nits in a playground. With the Spanish sit-in still going strong &ndash; and intending to remain until at least the 29th - street demonstrations have also hit Greece, Georgia, and, er, Bristol. Protests are spreading to Italy, France, Portugal, Austria even German - could it be that last year&#39;s initial protests against austerity measures are maturing, one year on, into a broader demand for political reform?  </p>  
   <p>  
  	Dealing with consequences of similar protect-the-rich IMF-style austerity measures as many millions of others in Europe, the Spanish have been mobilising on the streets with mass protests and strikes for months (see <a href='../archive/news741.htm'>SchNEWS 741</a>). This general air of resistance has now spawned a sit-down protest in the capital city. Demands are vague and the whole thing has the feel of a forum rather than a movement with a solid blueprint for the future, but what unites them is an anger with pro-business politicians who all conspired in the banking boom, bust and bail-out.  </p>  
   <p>  
  	In Madrid&#39;s central city square of Puerta del Sol, the &ldquo;alternative village&rdquo; set up to protest the government ahead of last Sunday&#39;s elections&nbsp; &ndash; which saw huge losses for Socialist Prime Minister Zapatero &ndash; continues unabated. The &lsquo;&iexcl;Democracia Real YA!&rsquo; (Real Democracy Now!) camp now boasts ramshackle but egalitarian canteens, day-care facilities, a press office, a pharmacy and even a zone for &ldquo;feminist information&rdquo;. Those involved in the movement have said they&#39;ll proliferate into local groups and assemblies around the country to spread the revolutionary message.  </p>  
   <p>  
  	<strong><img style="margin-right: 10px; border: 2px solid black"  align="left"  alt=""  src="http://www.schnews.org.uk/images/773-spain-storypic.jpg"  /> IT&#39;S A VERY MADRID WORLD</strong>  </p>  
   <p>  
  	In London, a camp-in opposite the Spanish embassy in Belgrave Square has seen hundreds arrive for daily rallies since 18th May. The group, calling themselves Real Democracy Now London, state their main driving force is their &ldquo;outrage and weariness at the misuse of the Spanish democratic system by politicians; their lack of respect and their constant mistreatment of the general interests of the electorate&rdquo;. It&#39;s a statement which sums up the widespread malcontent across the continent nicely, if you replace &ldquo;Spanish&rdquo; with pretty much any alternative nationality.  </p>  
   <p>  
  	The camp plans to remain for several more days. In Brighton, which is lacking in embassies, a solidarity protest has rocked up in a tourist-gateway lawn opposite the pier.  </p>  
   <p>  
  	In Bristol, a hefty number descended on the city centre on Sunday (22nd)&nbsp; - but rather than only offering solidarity for their Spaniard comrades, they were hoping to spark a similar uprising in the UK.&nbsp; Demonstrators bore banners with memes-of-the-moment slogans such as &ldquo;Real Democracy Now&rdquo;, and &ldquo;They don&#39;t represent us&rdquo;.  </p>  
   <p>  
  	Meanwhile tens of thousands of Greeks turned out in Syntagma Square, Athens - and in other cities - for four days running, declaring their &ldquo;indignation&rdquo; at the government and replicating the Spanish calls for &ldquo;All politicians to go&rdquo;. With Greek politicians having spent most of their time recently fielding accusations of incompetence on the international stage, they&#39;re now facing a double-whammy of pressure (and should possibly learn to take a hint). The protests have so far been peaceful, which suggests, ironically, that during these most explicitly anarchic demonstrations so far, the feisty Greek anarchists themselves have temporarily cast their black masks to the back of the wardrobe and decided that petrol-bombing cars is all a bit 2010.  </p>  
   <p>  
  	In Georgia, things have been a bit more rough n&#39; repressed, as police clamped down on several hundred opposition protesters demanding the resignation of President Mikheil Saakashvili. Led by former speaker of parliament Nino Burdjanadze, the protests against Saakashvili&#39;s authoritarian leadership, repression of independent voices and the country&#39;s widespread poverty began last Saturday (21st). Five days later, to clear the way for a military procession marking Georgia&#39;s Independence Day, riot cops moved in to disperse the crowds using tear gas, rubber bullets and water cannons. A police officer and protester were killed during the brutal attack, and another 37 were injured.  </p>  
   <p>  
  	Smaller solidarity protest camps have now sprouted outside Spanish Embassies worldwide, and may yet catalyse bigger mobilisations as the locals realise that their situation is not so different from the Iberian one.  </p>  
   <p>  
  	In all countries, the credit for&nbsp; the mobilisation&nbsp; has gone to Facebook and Twitter - making the claim that the internet is a tool for democracy much more literal than previously implied. Nonetheless, if such antics continue and escalate, we wonder how long before the surveillance / censorship / repression side of social networking is pressed into action by worried elites.  </p>  
   <p>  
  	In the meantime, the disaffected of Europe have the shining example of Iceland before them. In 2009, facing their own banker-lead economic collapse, people power overthrew the government &ndash; see <a href='../archive/news664.htm'>SchNEWS 664</a> &ndash; and then stuck two fingers up to the &#39;financial community&#39; by defaulting on their debt. The country is now, far from crashing back to the dark ages as an international pariah,&nbsp; actually doing rather well and looks set to recover more quickly than either Ireland or Greece where&nbsp;&nbsp; massive IMF loans and bail outs - mainly given to pay straight back to their overseas creditors - were rammed down taxpayers throats along with swingeing austerity cuts.&nbsp;&nbsp; An Ice lesson for us all.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1230), 142); ?><?php echo showComments(1230, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>LE HARVE IT LARGE</h3>

<p>
<p>  
  	Some 7,000 anti-globalisation activists converged on the French city of Le Havre last Saturday, effectively closing its commercial district, to kick off protests against this week&#39;s G8 summit of rich nations in nearby Deauville, Normandy. &quot;G8 get lost, people first, not finance,&quot; declared the main banner, as security forces took a hands-off approach to the big recognised NGO / lefty / union / human rights groups-arranged march &ndash; content merely to use checkpoints on roads coming into the city and doing some identity-checking searches in order to intimidate incoming demonstrators.&nbsp; The 12,000 military and police personnel drafted in were obviously saving themselves.  </p>  
   <p>  
  	This week has seen swift and heavy police action to stop or kettle attempted protests in Paris and Caen. Twenty members of &#39;No G8 2011&#39; were nicked in Northern Paris for drum-banging&nbsp; and sloganeering but 30 other protesters managed to get inside the offices of finance rating agency Standard &amp; Poor &ndash; where they rearranged furniture and sat on a specially mocked up monopoly-style board while throwing fake money around. Fifty others planning to head to a &quot;symbolic site of finance and capitalism&quot; were kettled, forcibly searched and detained before being able to start their action.  </p>  
   <p>  
  	More protests are planned in Paris, but protesters will have to get a little more, er, enterprising to avoid les flics.  </p>  
   <p>  
  	At Deauville itself there was the now obligatory military style lockdown with activists left with little to do but wave banners on the beach in Le Havre. While World Leaders inside chew over topics such as what to do about the Arab Spring and Libya, global finance and restricting freedom of the internet, perhaps anti-G8 energies should be put into more Madrid-style protest, sooner rather than later...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1231), 142); ?><?php echo showComments(1231, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>MORT IN A STORM</h3>

<p>
<p>  
  	A former prison, newly designated as an Immigration Removal Centre, will be officially opened by Immigration Minister Damian &#39;Bad Omen&#39; Green on Friday 1st June. Right-leaning media representatives will be welcome of course at the big opening event which starts at 10am - but not so protesters for liberty and human rights, who should certainly not consider trying to voice any dissent when the press meet the Minister at 11.30.  </p>  
   <p>  
  	Previously an RAF base, Morton Hall in Swinderby near Lincoln has operated as a prison since 1985. Plans to change it&#39;s purpose seemed dashed last summer when it was announced that the government could not afford the high costs of locking up yet more refugees and migrants.&nbsp; But the Con-Dem coalition has now miraculously found the money, presumably from savings made from&nbsp; cutting public services.  </p>  
   <p>  
  	Detention Action and other organisations have been campaigning against the expansion of detention. In a joint letter to the Immigration Minister last year, 25 organisations criticised the Morton Hall plans in the light of the UKBA&#39;s &quot;inefficient and damaging&quot; use of existing detention centres and called for a moratorium on expansion.  </p>  
   <p>  
  	* See <a href="http://www.detentionaction.org.uk" target="_blank">www.detentionaction.org.uk</a> and <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1232), 142); ?><?php echo showComments(1232, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BATON CHARGED</h3>

<p>
<p>  
  	PC Simon Harwood now faces criminal prosecution for striking the unsuspecting newspaper-seller Ian Tomlinson with his baton, and roughly pushing him into the crowd during the G20 protests.  </p>  
   <p>  
  	The inquest into event recently found that Tomlinson was unlawfully killed (see <a href='../archive/news770.htm'>SchNEWS 770</a>).&nbsp; Harwood now is set to appear in court on 20 June.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1233), 142); ?><?php echo showComments(1233, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY</h3>

<p>
<p>  
  	There is no paper or pdf issue this week (or indeed real &#39;...and finally&#39;) due to staff shortages, festival trips, holidays, laziness, austerity cuts and our forthcoming grand book launch.&nbsp; (Did someone say plug?! order your copy of our mildly humerous merchandis-tastic new &#39;SchNEWS in graphic detail&#39; now at <a href="../pages_merchandise/merchandise_books.php" target="_blank">http://www.schnews.org.uk/pages_merchandise/merchandise_books.php</a>).  </p>  
   <p>  
  	PS. There&#39;s probably no issue next week either, but stay tooned! <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1234), 142); ?><?php echo showComments(1234, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>As we're in dubious and shameless plug mode,  SchNEWS advises all readers to watch the latest Adam Curtis series 'All Watched Over by Machines of Loving Grace' on BBC iplayer. Honest</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A href="../schmovies/index.php" TARGET="_blank">RAIDERS OF THE LOST ARCHIVE</A></B>
			-<B> Part one of the SchMOVIES collection 2009-2010 </B>
			- This DVD features a number of films which were held by Sussex police for over a year following the raid and confiscation of all SchMOVIES equipment during an intelligence gathering operation in June 2009 related to the <a href='http://www.smashedo.org.uk/' target='_BLANK'>Smash EDO</a> campaign.</p>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->
</div>








</div>




</body>
</html>

