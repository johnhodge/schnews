<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 689 - 4th September 2009 - Hill Fought</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, hilaire purbrick, whitehawk hill, allotments, squatting, brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.rts.gn.apc.org/"><img 
						src="../images_main/rtf-05-banner.jpg"
						alt="Reclaim the Future"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 689 Articles: </b>
<p><a href="../archive/news6891.php">Common People?</a></p>

<p><a href="../archive/news6892.php">Factory Finish</a></p>

<p><a href="../archive/news6893.php">Herculean Task</a></p>

<p><a href="../archive/news6894.php">Circus Freaks</a></p>

<b>
<p><a href="../archive/news6895.php">Hill Fought</a></p>
</b>

<p><a href="../archive/news6896.php">Inside Schnews</a></p>

<p><a href="../archive/news6897.php">Dsei-ing With Death</a></p>

<p><a href="../archive/news6898.php">Brum Punch</a></p>

<p><a href="../archive/news6899.php">Arm-a-geddon Myself Some Of That</a></p>

<p><a href="../archive/news68910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th September 2009 | Issue 689</b></p>

<p><a href="news689.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HILL FOUGHT</h3>

<p>
Last Wednesday (26th August), Hilaire Purbrick was evicted from his second camp on a site at the Whitehawk Hill allotments in Brighton. The week before the council had got an injunction against Hilaire, and he was told to be off the site by midnight last Tuesday. On Wednesday morning they gave him three hours to gather his belongings before bailiffs and contractors with diggers trashed the encampment and gardens. <br />   <br /> Hilaire had been on this current site for six weeks, after a similar eviction of his camp on another part of the allotment site, where he&rsquo;d been working &ndash; and for the majority of the time living &ndash; since 1993. Having taken a group of allotments which were initially in disrepair and subject to flytipping and vandalism due to it being next to the poor estates of Whitehawk, Hilaire and friends went about the process of reinstating the fences, and eventually setting up a gardeners association to manage the allotments and help promote allotments and self-growing to the broader community. Hilaire began living on the site, to confront those who were flytipping and causing problems. <br />   <br /> The council has increasingly stepped down harder against Hilaire, and an injunction was taken out against him in 2002, but after leaving the site for nearly a year, he returned and continued work with the eight allotments, including building a 15ft wide storage chamber 20ft under the ground. Because of this chamber, the eviction of the site this June turned into an international news story after media from as far as Russia came to write stories about the &lsquo;<strong>Caveman Of Brighton</strong>&rsquo;, who was evicted from his cave because it didn&rsquo;t have a fire exit! <br />  <br /> He told SchNEWS, &ldquo;<em>I could see that there was a proper job to be done here, healing the land &ndash; and if you want to heal it you have to get intimate with it. To be here you have to engage in the real things in life &ndash; real friendships, real work, and healing the land is real &ndash; it&rsquo;s a subtle feeling of unity with the land and it&rsquo;s something that brings you great joy. You&rsquo;re sharing the land with magpies and badgers. I can&rsquo;t think of a happier place that has been destroyed today. People would wonder over here, and immediately got what was going on. They would be lost in their imagination, and kids who might otherwise have been pestering would suddenly stop nagging and just start joining in</em>.&rdquo; <br />   <br /> While some on the Hill support what he&rsquo;s doing but don&rsquo;t think people should be living there, Hilaire&rsquo;s story represents both the broader struggle for those wanting to live a low-impact, off-the-grid life - and the difficulties of doing this in Britain, and the issue of autonomously managing a heritage site away from the clutches of government and institutions. <br />  <br /> As he sat in his loaded-up van contemplating driving off from Whitehawk Hill, Hilaire told SchNEWS &ldquo;<em>This is gone, but all the knowledge that I&rsquo;ve gained will come with me and will help build something better.</em>&rdquo;  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>allotments</span>, <a href="../keywordSearch/?keyword=brighton&source=689">brighton</a>, <span style='color:#777777; font-size:10px'>hilaire purbrick</span>, <a href="../keywordSearch/?keyword=squatting&source=689">squatting</a>, <span style='color:#777777; font-size:10px'>whitehawk hill</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(478);
			
				if (SHOWMESSAGES)	{
				
						addMessage(478);
						echo getMessages(478);
						echo showAddMessage(478);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>