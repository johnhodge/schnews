<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 745 - 29th October 2010 - First Cuts Not the Deepest</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, financial crisis, david cameron, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 745 Articles: </b>
<p><a href="../archive/news7451.php">Serious Organised Crime</a></p>

<b>
<p><a href="../archive/news7452.php">First Cuts Not The Deepest</a></p>
</b>

<p><a href="../archive/news7453.php">Edl: Down The Rabbi Hole</a></p>

<p><a href="../archive/news7454.php">Somerset Sabs Attacked</a></p>

<p><a href="../archive/news7455.php">Cuts Both Ways</a></p>

<p><a href="../archive/news7456.php">Naples: Load Of Rubbish</a></p>

<p><a href="../archive/news7457.php">Snogfest Vs Nazi Nonce</a></p>

<p><a href="../archive/news7458.php">The Us Itt Parade</a></p>

<p><a href="../archive/news7459.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th October 2010 | Issue 745</b></p>

<p><a href="news745.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FIRST CUTS NOT THE DEEPEST</h3>

<p>
<p>
	OK Europe its not, but the first rumblings of discontent against the cuts are beginning to be heard - here&rsquo;s a few snapshots from around the shires of a very British mutiny... </p>
<p>
	Students at Birmingham University occupied their vice-chancellor&rsquo;s office on Friday (22nd) over &pound;20million worth of planned cuts and a hike in tuition fees. Police arrived but the assembled students were quick to point out they weren&rsquo;t actually committing any criminal offence, so the coppers assumed positions as &lsquo;observers&rsquo;. This forced the Registrar and Secretary into negotiations and the sit-in was called to a close after university management agreed to enter into talks with the student body on Monday (25th) about the planned cuts. Previously the only discussion had been with the student union president only, behind closed doors. </p>
<p>
	Cambridge, Sheffield, Manchester, York, Belfast and Bristol all saw demos on Saturday (23rd) with support from people in their hundreds marching through their city centres. Organised by groups opposing cuts in education, pensions and campaigning for the &lsquo;Right to Work&rsquo;, each action saw its fair share of banner waving and slogan shouting, although who knows what the French pensioner who joined the march in Cambridge thought about the difference in tactics. </p>
<p>
 </p>
<p>
	<a href="..//images/745-jenganomics-lg.jpg" target="_blank"><img style="border:2px solid black;width: 250px; height: 285px"  align="left"  alt="Jenganomics - The game for elites aged 45 - 60"  src="http://www.schnews.org.uk//images/745-jenganomics-sm.jpg"  /> </a> </p>
<p>
	Actions were mostly peaceful, but coppers in Bristol got stuck in with a hugely disproportionate presence, trying to steal banners and generally hassling demonstrators, resulting in a few scuffles and two arrests. </p>
<p>
	Oxford saw a 1,000 person strong march on the same Saturday which managed to successfully break through several police lines to deviate from their &lsquo;designated protest route&rsquo;. Originally planned to coincide with a visit to the city by Vince Cable, who later pulled out after the demo was announced, the campaigners managed to pass through the busy streets of Oxford city centre to the Examinations Schools of Oxford University where he had been scheduled to appear. </p>
<p>
	By far the best attended demo of Saturday was in Edinburgh, organised by The Scottish Trades Union Congress. Numbers are somewhat disputed with organisers estimating around 20,000 people took part, the police low-balling at 6,500. Made up of NHS and council staff, students, civil servants, private-sector workers and politicians, the demo started at Waverly train station and finished at Princes Street Gardens for a rally. </p>
<p>
	Nearly 1,000 angry pensioners descended on Downing Street on Wednesday (27th) to protest against the cuts and to demand a basic state pension of &pound;171 a week. The group gathered opposite parliament before going to lobby their MPs. The National Pensioners Convention general secretary addressed the crowd and called for everyone to stand together in the fight to save the one in five pensioners who already live below the poverty line. </p>
<p>
	Protesters closed down Vodafone&rsquo;s Oxford Street store on Wednesday (27th) in response to the government letting the mobile phone giant off a massive tax bill. The group of around 40 activists staged a sit-in at the shop holding banners saying &lsquo;Vodafone&rsquo;s unpaid tax bill: &pound;6billion &ndash; Cuts to welfare: &pound;7billion&rsquo;, and posted up signs with the company&rsquo;s logo and the phrase &lsquo;tax dodgers&rsquo;. The shop closed soon after with the metal shutters coming down on a line of sitting demonstrators who were roughly pulled out of the way by police. The group then sat outside the closed store and spoke to passersby, most of whom were outraged at the hypocrisy of the government cutting benefits for the most vulnerable while letting corporations have huge tax breaks. Another demo on Oxford </p>
<p>
	Street will take place this Saturday (30th) and the call has gone out for groups to do the same at their local branches. </p>
<p>
	RBS was another big business targeted by campaigners as branches across Leeds were attacked with paint, sand and glue. The activists were demonstrating against the massive bailouts the bank has received, leaving RBS 84% publicly owned, while cuts are made in benefits and pensions. </p>
<p>
	More demos are planned for this Saturday (30th). Brighton&rsquo;s Stop the Cuts march, starts at The Level at 12noon and Portsmouth is meeting at 11.30am at Guildhall Square. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(988), 114); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(988);

				if (SHOWMESSAGES)	{

						addMessage(988);
						echo getMessages(988);
						echo showAddMessage(988);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


