<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 706 - 22nd January 2010 - Haiti: The Aftershock Doctrine</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, haiti, usa, earthquake, disaster, anti-corporate" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 706 Articles: </b>
<b>
<p><a href="../archive/news7061.php">Haiti: The Aftershock Doctrine</a></p>
</b>

<p><a href="../archive/news7062.php">Mobs And Coppers</a></p>

<p><a href="../archive/news7063.php">Lanark'y In The Uk</a></p>

<p><a href="../archive/news7064.php">Rent A'sunderland</a></p>

<p><a href="../archive/news7065.php">Stoke'ing The Fires</a></p>

<p><a href="../archive/news7066.php">Girls With Latitude   </a></p>

<p><a href="../archive/news7067.php">Inside Schnews</a></p>

<p><a href="../archive/news7068.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/706-Obama-Haiti-lg.jpg" target="_blank">
													<img src="../images/706-Obama-Haiti-sm.jpg" alt="The U.S will help victims of Haitian earthquake in any way possible.... to become the free trade sweatshop slave workers we want you to be..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 22nd January 2010 | Issue 706</b></p>

<p><a href="news706.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HAITI: THE AFTERSHOCK DOCTRINE</h3>

<p>
As the rescue effort winds down and the body count piles up, SchNEWS takes a look at why this earthquake hit so many so hard, and how the international aid effort is being subverted by military and corporate power. <br />  <br /> As Haiti&rsquo;s total estimated dead hovers at around 200,000, the earthquake has hit the country so hard that it&rsquo;s dropped a whole digit off its former population of 10 million. It&rsquo;s almost a law of world injustice that the poorer the country, the more vague the body count. <br />   <br /> Haiti is a unique place. The only nation to be founded by a slave rebellion, its angry population of African slaves managed to defeat the French back in 1791. Renamed as Haiti - the original Arawak name for the island - it was the Cuba of its day- an inspiration to enslaved peoples the world over and a thorn in the side of the imperial regimes. <br />   <br /> Unfortunately for Haitians, the western powers have long memories. Unable to reconquer Haiti militarily, the people of Haiti have been subjected to blockades and one sided trade agreements ever since. The prize for most outrageous treaty probably goes to the 19th century French government, which demanded that Haitians pay France reparations for lost earnings following the loss of &lsquo;their&rsquo; slaves. <br />   <br /> As French neo-colonialism was replaced by the American variety, years of massive loans on impossible interest rates has meant that any money from Haiti&rsquo;s cash crops have gone directly to international banks, after corrupt leaders took their cut. <br />   <br /> The infamous hereditary dictatorship of &lsquo;Papa&rsquo; Doc and &lsquo;Baby&rsquo; Doc Duvalier ruled the country with a unique form of voodoo oppression between 1957 and 1986. The three decades of Duvalier rule saw Haiti change from being able to feed its population to becoming a cash crop economy reliant on US food imports. <br />   <br /> One of the many unpleasant sights from the Haitian disaster was former US presidents George W Bush and Bill Clinton standing together, earnestly encouraging people to help Haiti. Haiti&rsquo;s former president, Jean Bertrand Aristide, was deposed by Bush Senior in &rsquo;91, reinstated on Clinton&rsquo;s orders by US troops (on condition that he sign up to harsh neo-liberal measures), only to be deposed by the Marines in 2004. <br />  <br /> All of this provides some sort of context for the relief effort that&rsquo;s underway. Lack of money and the complete absence of almost any social function of the state meant that when the magnitude 7 quake hit so close to Port au Prince, whole neighbourhoods of the slum city disintegrated. The airport, seaport, roads and communication links were all so badly built and their destruction so total that the normal routes for aid and rescue teams were barred. <br />   <br /> Whilst everyone else from Medicine San Frontiers to the Icelandic government were busy sending food, medics and drinking water, the Americans dithered, sending the aircraft carrier USS Carl Vinson three days later, without emergency relief supplies but with plenty of sidewinder missiles and combat helicopters. Even as the rest of the world pitched in, the Americans claimed they couldn&rsquo;t deliver aid without making sure Haiti was safe first. In that peculiar form of helping that we&rsquo;ve learned to know so well, the Americans sent in the Marines (again). Some 10,000 troops have been, or are about to be deployed - a readymade army of occupation. <br />   <br /> As of Tuesday, the US military is officially in direct control of Haiti. But then, in reality, Haiti has been effectively under occupation on and off for years now. The troops providing aid should be seen in this light - as the Geneva Convention states: &ldquo;<em>The duties of the occupying power include ... providing the population with food and medical supplies; agreeing to relief schemes... maintaining medical facilities and services; [and] ensuring public health and hygiene</em>.&rdquo; <br />   <br /> In an effort to prove that the military presence is somehow necessary, the US media has been hard at work painting a picture (just like New Orleans post Katrina) of criminal gangs, mass lootings and &lsquo;human rats&rsquo; (to quote Time Magazine) that need pacifying in order to be given assistance. Yet the word from agencies on the ground is that apart from the desperate search for food and basic supplies, the population has been relatively quiet.  <br /> The militarisation of the Haiti aid effort has caused huge problems for international NGOs. Medicine Sans Frontiers has publicly criticised the USA for slowing down and mismanaging the aid effort. Since Sunday MSF medics and 5 planes carrying 85 tonnes of drugs and surgical supplies have been turned away from the airport due to the prioritisation of military air traffic. <br />   <br /> Francoise Saulnier, head of MSF&rsquo;s legal department said, &ldquo;<em>We lost three days, and these three days have created a massive problem with infection, with gangrene, with amputations that are needed now, while we could have really spared this to those people</em>.&rdquo; <br />  <br /> Just as it did in Iraq, the US military has been trying to control the flow of information out of Haiti. Yesterday ( 21st) the military ordered the removal of all international journalists from the Haitian capital&rsquo;s airport, without bothering to supply any explanation.  <br /> Assuming that aid workers can prevent starvation and disease from taking many more lives, the aid effort will slowly turn into a reconstruction effort. As much as Haitians no doubt <br />   <br /> appreciate any help right now, the form of aid will have a major bearing on Haiti&rsquo;s direction over next few decades. The IMF is talking about a Marshall Plan for Haiti. Originally the plan was for a loan of $100 (&pound;60) million that included demands for wage cuts and raised prices. <br />   <br /> Public pressure from debt relief activists such as Jubilee USA, complete with the Facebook campaign &ldquo;<strong>No shock doctrine for Haiti</strong>&rdquo; managed to curtail some of the most obviously exploitative aspects of the IMF&rsquo;s proposal. Anti-capitalista Naomi Klein called this victory &ldquo;<em>unprecedented in my experience and shows that public pressure in moments of disaster can seriously subvert shock doctrine tactics</em>.&rdquo; <br /> &nbsp; <br /> As witnessed during Katrina and the &lsquo;04 tsunami, the public response looks set to overwhelm the response of governments and international institutions. In Britain alone, the public has already donated &pound;31 million via the Disasters Emergency Committee. The DEC is part run by the government, and, although they may be good at distributing aid, don&rsquo;t expect much in the way of criticism over the handling of Haiti&rsquo;s ongoing disaster.  <br />  <br /> For anarchists/activists who do want to donate to the aid effort without compromising their anti-state credentials, Medicine Sans Frontiers (<a href="http://www.msf.org.uk" target="_blank">http://www.msf.org.uk</a>) and Medicine Du Monden (<a href="http://www.mdmuk.org.uk" target="_blank">http://www.mdmuk.org.uk</a>)  have a consistent track record of doing (and saying) the right thing.  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-corporate&source=706">anti-corporate</a>, <span style='color:#777777; font-size:10px'>disaster</span>, <span style='color:#777777; font-size:10px'>earthquake</span>, <span style='color:#777777; font-size:10px'>haiti</span>, <a href="../keywordSearch/?keyword=usa&source=706">usa</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(626);
			
				if (SHOWMESSAGES)	{
				
						addMessage(626);
						echo getMessages(626);
						echo showAddMessage(626);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


