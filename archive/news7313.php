<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 731 - 16th July 2010 - Gaza Defendants Appeal</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, met police, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 731 Articles: </b>
<p><a href="../archive/news7311.php">Against The Grain</a></p>

<p><a href="../archive/news7312.php">Boring Nonsense</a></p>

<b>
<p><a href="../archive/news7313.php">Gaza Defendants Appeal</a></p>
</b>

<p><a href="../archive/news7314.php">  On The Right Tracks</a></p>

<p><a href="../archive/news7315.php">Three Imprisoned In Iran</a></p>

<p><a href="../archive/news7316.php">Inside Schnews: Speak Activist Gets Ten Years</a></p>

<p><a href="../archive/news7317.php">  Outside Schnews: Joe Glenton Released</a></p>

<p><a href="../archive/news7318.php">No Oil Painting</a></p>

<p><a href="../archive/news7319.php">Fight Or Flight</a></p>

<p><a href="../archive/news73110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th July 2010 | Issue 731</b></p>

<p><a href="news731.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GAZA DEFENDANTS APPEAL</h3>

<p>
<p>
	Following last year&rsquo;s violent protests in London against the attacks on Gaza (see <a href='../archive/news661.htm'>SchNEWS 661</a><a href='../archive/news.htm'></a>-<a href='../archive/news662.htm'>662</a>) the first ten appeals against the harsh sentences dished out were heard on Tuesday (13th). Around 100 people were originally arrested, charged with offences as minor as throwing a plastic bottle. Some were sent down for up to 2 &frac12; years. </p>
<p>
	The majority of those in the appeal court on Tuesday were aged 17-21 (and mostly Muslim) and all had received custodial sentences, one of which the appeal judge termed as &lsquo;manifestly excessive&rsquo; (see <a href='../archive/news710.htm'>SchNEWS 710</a>). The judge also criticised the way the cases had been handled by the CPS as they had been randomly brought to court in a way that didn&rsquo;t allow for proper comparison. He also stated that as most of the defendants had pleaded guilty on the day they were arrested, their cases should have processed much quicker, especially because of the young age of most of the appellants and the negative effect on their education. </p>
<p>
	During the appeal the defence barristers stupidly failed to mention any of the provocative or violent action taken by the police during the Gaza march, and relied on pointing out that the Muslim defendants&rsquo; sentences were unjust as their actions were directed at the police rather than the public, therefore a less severe crime - something the appeal judge didn&rsquo;t take to too kindly. </p>
<p>
	Despite the failings of the defence counsel, six of the younger ones appealing got a reduction in their sentences and two were released on the day, one of whom had already served 12 months in prison &ndash; a punishment which the appeal judge swapped for a one night curfew. Three of the older defendants were not granted any reductions. </p>
<p>
	Judge Denniss who handed out the sentences in the first place had taken the 2001 riots in Bradford (see <a href='../archive/news313.htm'>SchNEWS 313</a>) as a precedent from which to derive the severity of the punishment given. Luckily the judge on Tuesday recognised the huge difference between a violent uprising of the public due to social injustice and a march that descended into brutal conflict at the hands of the police &ndash; changing the starting point of sentences to 27 months rather than 36. </p>
<p>
	Outside the court buildings a demo of 50-60 supporters had gathered and the public gallery inside was full of friends and relatives, causing the hearing to be moved to a bigger room so everyone could fit. There were many representative from the Stop the War Coalition, although one of those in the public gallery noted the marked absence of any of the leaders of the movement, despite their repeated emphasis that there needs to be more active support of the defenders. </p>
<p>
	Controversially, on Monday (12th) the Met awarded &pound;25,000 to twin brothers who were at the march last January and also suffered police violence, both having been batoned over the head while demonstrating. These two (white, non-Muslim) men made a complaint to the police which was quickly dismissed, and had since pursued the matter in the civil courts. </p>
<p>
	Many more appeal hearings against the sentences given out to others are lined up although dates have not been confirmed. </p>
<p>
	* See <a href="http://www.gazademosupport.org.uk" target="_blank">www.gazademosupport.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(861), 100); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(861);

				if (SHOWMESSAGES)	{

						addMessage(861);
						echo getMessages(861);
						echo showAddMessage(861);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


