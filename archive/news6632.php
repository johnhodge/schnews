<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 663 - 23rd January 2009 - Sentenced For Life</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, huntingdon life sciences, shac, hls, conspiracy to blackmail, vivisection, animal testing, stop huntingdon animal cruelty, shac 7" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 663 Articles: </b>
<p><a href="../archive/news6631.php">Decommission Accomplished</a></p>

<b>
<p><a href="../archive/news6632.php">Sentenced For Life</a></p>
</b>

<p><a href="../archive/news6633.php">Are You Being Served</a></p>

<p><a href="../archive/news6634.php">School Of Life</a></p>

<p><a href="../archive/news6635.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd January 2009 | Issue 663</b></p>

<p><a href="news663.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>SENTENCED FOR LIFE</H3>

<p>
<b>AS LONG PRISON TERMS ARE HANDED DOWN TO THE SHAC 7</b> <br />
 <br />
<i>&#8220;Gregg Avery, Natasha Avery, Dan Amos, Heather Nicholson, Gerrah Selby, Gavin Medd-Hall, Dan Wadham and all others accused in this politically motivated witch hunt are guilty of the highest level of compassion. They have given their all to save the lives of others and expose the inbuilt failings of animal experimentation. Prison and the threat of prison will not stop decent people from doing what is right.&#8221;</i> - <b>SHAC spokesperson.</b> <br />
 <br />
Vicious sentences have been handed down in the SHAC conspiracy to blackmail trial (see <a href="../archive/news661.htm">SchNEWS 661</a>). Heather Nicholson was jailed for 11 years, Gregg and Natasha Avery for nine years each, Gavin Medd-Hall for eight years, Daniel Wadham for five years and Dan Amos and Gerrah Selby were both sentenced to four years. Greg and Natasha had earlier pleaded guilty; their sentencing was left until the end of the trial. At no point did the prosecution allege that any of the defendants were involved in home visits, paint-stripping cars or in fact any illegal activity. All they had to prove, it seemed, was that the campaign against HLS influenced &#8216;persons unknown&#8217; into taking illegal direct action. Not that you&#8217;d have guessed that from browsing the mainstream media &#8211; which is united in saying that the defendants themselves were &#8216;extremists&#8217; guilty of the actions.  <br />
 <br />
Huntingdon Life Sciences (HLS) are in the business of poisoning healthy animals to death. Far from being the pioneers of life-saving medical research depicted by the media, they are a commercial contract testing company that tests artificial colourings, flavourings and sweeteners, herbicides, GM food, plastics, industrial chemicals, &#8220;health foods&#8221;, dietary supplements and a small amount of drugs for other commercial companies. They are the largest company of this kind in Europe. Five hundred animals are put to death every day by HLS.  <br />
 <br />
The world&#8217;s media, prompted by police press officers, have been quick to condemn activists by pointing to harassment against the employees of HLS and their customers, shareholders and investors. What is never examined is the ongoing cruelty, scientific fraud and the development of  repressive new laws targeted at the animal rights movement.  <br />
 <br />
SHAC&#8217;s activities have been lawful; the campaign publishes information about animal abuse inside HLS labs, reports campaigning activities and issues contact alerts calling on supporters to write polite letters to companies working with HLS and ask them to desist. If those companies continue to do business with HLS, protests would usually follow. All material on the SHAC website and newsletters is checked by a barrister. Even the judge at Winchester has said that SHAC is a legal campaign. Other pressure groups such as Greenpeace, Amnesty International and Friends of the Earth all use email alerts and website campaigns to highlight protests and campaigns. <br />
 <br />
Putting these campaigners on trial for &#8216;conspiracy to blackmail&#8217; was clearly a political decision. A �4 million inquiry - involving five police forces - targeted SHAC despite admitting that campaigners &#8220;rarely caused physical harm&#8221;. This is due to the effectiveness of the A.R. movement in confronting and challenging the power of corporations involved in animal abuse. The demonisation of animal rights campaigners in the media has made it easier for the state to repress them without public outcry. The conviction and sentencing of the SHAC 7 in Winchester is another nail in the coffin of the public&#8217;s right to voice their anger and dissent against corporate greed. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
<b>History of HLS</b> <br />
 <br />
HLS hit the headlines in 1997 when they were exposed by an undercover worker with a hidden camera. This expos� was screened on national TV and showed HLS workers screaming abuse at terrified beagle dogs. One is filmed punching a puppy in the face in anger! The outcry that followed rocked the foundations of this once profitable company as the public made their objections felt. HLS shares were devastated and never recovered. Today HLS are unable to trade in more than 10% of their shares. This is a very sick company. <br />
 <br />
Stop Huntingdon Animal Cruelty (SHAC) is a public pressure campaign that grew out of the prime time exposure of HLS into a global initiative. Using a variety of tactics intended to draw public attention and hamper operations such as mass protests, small demonstrations, leaflet drops, lock-ons, blockades and email alerts, SHAC has brought HLS to its knees. The informed public finds the business of HLS repulsive and will not allow them to regain their feet. <br />
 <br />
HLS have been exposed to the public time and time again. On each occasion a very familiar story comes out. It is in the public domain that HLS workers have falsified test results and been found guilty of animal cruelty. From 1995 to 2000 HLS broke the Good Laboratory Guidelines no less than 520 times in just one experiment! In 2004 The Observer exposed HLS for gassing beagle dogs to test a CFC chemical which was banned 15 years earlier. <br />
 <br />
Government legislation and public money has come thick and fast to ensure this distasteful, scientifically unproven commercial concern continues unhindered by the progress of public opinion. High Court injunctions restrict public protest to a few hours a week at the gates of HLS. The company no longer owns its property but has been reduced to leasing it back from those it sold out to. HLS has the Bank of England to sign its cheques because none of the High Street banks will provide banking services. The UK state is also funding HLS&#8217;s insurance and for the police to spend vast resources on imprisoning those who speak out against this abhorrent business. <br />
 <br />
* See <a href="http://www.shac.net" target="_blank">www.shac.net</a> or call 0845 458 0630 <br />
 <br />
<b><i>PRISONER SUPPORT:</i></b> Write letters of support to Natasha Avery NR8987 and Heather Nicholson VM4859 at HMP Bronzefield, Woodthorpe Road, Ashford, Middx, TW15 3JZ. Dan Amos VN7818, Gregg Avery TA7450, Gavin Medd-Hall WV9475 and Dan Wadham WV9474 at HMP Winchester, Romsey Road, Winchester, SO22 5DF. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
<b>SHAC 7 Solidarity Actions</b> <br />
 <br />
<b>CAMBRIDGSHIRE:</b> On Monday (19th), activists paid an early morning visit to HLS&#8217;s beagle breeding operation at Harlan (<a href="../archive/news466.htm">SchNEWS 466</a>) before moving onto the main HLS lab at Wooley. The front gates were blocked by the protests and a megaphone used to name and shame the workers inside. Tuesday saw a repeat performance at Harlan, where police were led on a merry dance round the perimeter fence before the roving protest  moved again to HLS Occuld in Suffolk. There was a good turnout &#8211; including an &#8216;intelligence&#8217; gathering camera cop. Noisy demos continued all day, whilst new faces attempted to avoid being served injunctions by the stony faced security. <br />
 <br />
<b>HEREFORD:</b> Wednesday saw similar visits to HLS suppliers Sequani (see <a href="../archive/news646.htm">SchNEWS 646</a>), and cage-makers Arrowmight Biosciences (see <a href="../archive/news638.htm">SchNEWS 638</a>). <br />
 <br />
<b>SOUTHAMPTON:</b> On Monday one of HLS&#8217;s newest suppliers, Promega, received a visit from SHAC supporters welcoming them to the HLS family and let their neighbours at the Southampton Science Park know just what the company is up to.  <br />
 <br />
<b>BIRMINGHAM:</b> Tuesday saw a Barclay&#8217;s branch targeted as SHAC activists went on a public information demo &#8211; to let customers know about the 500 animals a day suffering at the hands of HLS &#8211; and how their bank helps fund it all.  <br />
 <br />
<b>HORSHAM:</b> Tuesday also a demo at HLS client Novartis (See SchNEW 643) &#8211; a regular target for protests. Noisy demos took place outside all day, despite the injunction in place which allows only 12 people to protest at once and limits megaphone time to a mere five minutes.  <br />
 <br />
* For more details see <a href="http://www.shac.net" target="_blank">www.shac.net</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=663">animal rights</a>, <a href="../keywordSearch/?keyword=shac&source=663">shac</a>, <a href="../keywordSearch/?keyword=hls&source=663">hls</a>, <a href="../keywordSearch/?keyword=conspiracy+to+blackmail&source=663">conspiracy to blackmail</a>, <a href="../keywordSearch/?keyword=vivisection&source=663">vivisection</a>, <a href="../keywordSearch/?keyword=shac+7&source=663">shac 7</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(200);
			
				if (SHOWMESSAGES)	{
				
						addMessage(200);
						echo getMessages(200);
						echo showAddMessage(200);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>