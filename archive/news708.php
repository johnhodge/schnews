<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(77);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	77;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 708 - 5th February 2010 - As SchNEWS Interviews Al Jazeera Journalist Medyan Dairieh</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, honduras, manuel zelaya, coup, repression, smash edo, edo decommissioners, prisons, kingsnorth, camp for climate action, police, no borders, calais, immigration, royal bank of scotland, tar sands, tifanys, bushmen, indigenous people, africa, mainshill, scottish coal, protest camp, blair, iraq, chilcot, afghanistan, taliban, al-qaeda, war on terror" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.seedysunday.org/" target="_blank"><img
						src="../images_main/seedy-sunday-09-banner.jpg"
						alt="Seedy Sunday"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news707.htm">SchNEWS 707, 29th January 2010</a></b><br />
<b>Lanarky in Action</b> - As the National Eviction Team move in on Mainshill Solidarity Camp... plus, they finally get around to putting a price on Blair's head, the English Defence League embark on a racist rampage through Stoke, French police evict a night shelter and destroy the makeshift camp erected by migrants in protest, a coalition of No Borders activists and local nimbys help derail plans for a new deportation centre in Crawley, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news706.htm">SchNEWS 706, 22nd January 2010</a></b><br />
<b>Haiti: The Aftershock Doctrine</b> - As the rescue effort winds down SchNEWS looks at how the international aid effort is being subverted by military and corporate power... plus, Monday&rsquo;s Smash EDO demo forced the Brighton bomb makers to shut up shop, the EDL&rsquo;s racist roadshow is moving on to Stoke, Mainshill solidarity camp in South Lanarkshire is gearing up for an eviction date, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news705.htm">SchNEWS 705, 15th January 2010</a></b><br />
<b>Wild at Heart</b> - Our comrades at SMASH EDO have announced the location of their Remember Gaza demo... plus, Gaza convoy finally allowed in after being held at port and attacked by Egyptian police, legal observer arrested at Smash EDO Carnival Against the Arms Trade finally overturns charges, European Court rules Section 44 stop 'n search illegal, the Christmas period has seen no let up for migrants in the squats and camps of Calais, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news704.htm">SchNEWS 704, 20th December 2009</a></b><br />
<b>Not a Hopenhagen</b> - As World Leaders dither inside, SchNEWS looks to the thousands of dissenters outside the Copenhagen Climate Conference.... plus, the Camp for Climate Action activists target Canada for wanting to exploit the oil their climate koshing tar sands, as much of Iraqs's oil finally got auctioned off last week was was mission accomplished for the US?, a prominent member of the EDL is charged with soliciting murder and using threatening, abusive or insulting words, Israeli goverment irked as UK warrant for war crimes issued to foreign ministe, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news703.htm">SchNEWS 703, 11th December 2009</a></b><br />
<b>SchNEWS Guide to Copenhagen</b> - Due to too many people being away (probably) burning babylon, being sick or generally too lazy, there is no print issue this week. But we didn't want you not gettin' any of yer regular fix and wanted to give you the lowdown on going to Copenhagen, so have patched together this 'blink and you'll miss it' Climate Conference special edition for the web only (where special means er, compact.)</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news702.htm">SchNEWS 702, 4th December 2009</a></b><br />
<b>If You're Not Swiss-ed Off...</b> - Ten years after Seattle, thousands congregate in Geneva to resist world trade organisation meeting... plus, 25 years after the Bhopal disaster and still no justice for the victims, protests in Wales over biomass plant at Port Talbot, Sussex students protest at staff cuts, Lithuanian national is imprisoned over London G20 protests, we gloat as the capitalist edifice of Dubai crumbles, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news701.htm">SchNEWS 701, 27th November 2009</a></b><br />
<b>Buy Buy Cruel World</b> - SchNEWS hopes the anti-consumerist message will finally gain purchase with international 'Buy Nothing Day' tomorrow... plus, the Target Barclays campaign is launched, opposing their investment in the arms trade, protest camp against opencast coal mine in Scotland is under threat of eviction, protest camp by the sacked Vesta workers on the Isle Of Wight is evicted, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news700.htm">SchNEWS 700, 20th November 2009</a></b><br />
<b>Good Cop Bad Cop</b> - Things are hotting up for the COP15 UN Climate Conference in Copenhagen next month... plus, despite being out of the news, the plight of the Tamil people in Sri Lanka continues to be dire, protesters disrupt the NATO Parliamentary Assembly in Edinburgh, the state clampdown on animal rights protesters continues as four are raided and arrested, the far-right march in Glasgow &ndash; and again are outnumbered, but next month move to Nottingham, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news699.htm">SchNEWS 699, 13th November 2009</a></b><br />
<b>Mexican Wave</b> - Around 200,000 workers, teachers, students, unionists, farmers and social campaigners shut  Mexican cities down on Wednesday (12th) in a national strike... plus, importer of Israeli produce grown on occupied Palestinian territories sees sustained protests, ex-soldier who served in Afghanistan is arrested over his opposition to the war, protests against the NATO meeting in Edinburgh begin as anti-militarist convergence space is opened, and more...</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/708-medyan-lg.jpg" target="_blank">
											<img src="../images/708-medyan-sm.jpg" alt="...and now it's over to Tom with the weather" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th February 2010 | Issue 708</b></p>
<p><b><i>WAKE UP!! IT&#8217;S YER TALIB-ANARCHY...</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news708.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news708.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news7081.php">Eyewitness Afghanistan</a>  |  <a href="../archive/news7082.php">A Honduran Lobo-tomy</a>  |  <a href="../archive/news7083.php">Inside Schnews</a>  |  <a href="../archive/news7084.php">Stop 'n' Sue</a>  |  <a href="../archive/news7085.php">Voulez Vous Couche?</a>  |  <a href="../archive/news7086.php">Treading A Fine Line</a>  |  <a href="../archive/news7087.php">Diamonds Are For Never</a>  |  <a href="../archive/news7088.php">Over The Mainshill?  </a>  |  <a href="../archive/news7089.php">Blair Faced Cheek  </a>  |  <a href="../archive/news70810.php">And Finally...</a> 
</p>


<div id="article">

<h3>EYEWITNESS AFGHANISTAN</h3>

<p>
<p class="MsoNormal"><strong>FROM <st1:placename w:st="on">KEMP</st1:placename> <st1:placetype w:st="on">TOWN</st1:placetype> TO <st1:city w:st="on"><st1:place w:st="on">KABUL</st1:place></st1:city>, AS SCHNEWS INTERVIEWS AL JAZEERA JOURNALIST MEDYAN DAIRIEH ABOUT HIS TAKE ON THE WAR</strong> <br />  <br /> Ever keen to get an up close and personal look at the world&rsquo;s most troubled regions, SchNEWS was fortunate enough to sweet-talk Brightonian Al Jazeera journalist Medyan Dairieh recently returned from reporting in Afghanistan. As a Palestinian who has extensively covered the conflicts in the Occupied Palestinian Territories, Iraq and Turkish Kurdistan for many years, he&rsquo;s a man with a useful perspective on events on the ground in what&rsquo;s become Britain&rsquo;s longest war since the days of Empire. Medyan has become a well known figure amongst pro-Palestine and anti-war protesters, informing the public in the Arab-speaking world about the UK&rsquo;s solidarity movements. <br />  <br /> <strong><em>So what was it like arriving in Afghanistan?</em></strong> <br />  <br /> When I arrived in Kabul I could see even from the plane that it is a very poor city. When you walk from the airport you can see Turkish army and American armies, but the airport is very small and the electricity is cut off for half an hour at a time. Once I went to the street, I knew everything about this country. There is no life at all. The electricity is sometimes on, sometimes off, no clean water, no sanitation at all. And this is in Kabul. Outside the city is nothing, most people don&rsquo;t know what a cooker is - they just cook on wood. Most of the roads in Afghanistan have not been concreted. The water you see on the street is sewage water coming from the houses since no proper sewage system is in place. Some of the streets I visited have a stream of dirty water running, and they are really hazardous. The houses are very poor. This is Afghanistan. <br />  <br /> People said to me, &ldquo;<em>We wish we were in Soviet times, the Soviet period. We fought the Soviets, and they attacked us, but they also built for us. But the Americans, they attack us, destroy everything and don&rsquo;t build anything</em>.&rdquo; Now I think people would prefer to go back to the Soviet period. <br />   <br /> I met a man who lost 3 children. One son and two nephews. He ran away from where he lived after they were killed by an American aeroplane just a few months ago. The refugees say that the fighters don&rsquo;t come in the daytime, they come at night, so why do the Americans attack in the afternoon? Some people say that in the village there are no Taliban, the Taliban go away from the villages. They use the mountains; sometimes they come to the villages for water and food. But the Americans don&rsquo;t know where the fighters are so they attack civilians. <br />  <br /> <strong><em>What do people make of the Taliban?</em></strong> <br />   <br /> What can I say? People think that the Taliban will give them a better deal than the Americans will. People can&rsquo;t believe in the Americans, they can&rsquo;t believe in the government either. All the buildings, government departments, are corrupt. And so people hate America now. Not because of religion, but many people join the Taliban because they want to take revenge against America. There are many, many areas that the Americans can&rsquo;t go. Many areas of Afghanistan are under the control of the Taliban. The mayors of some cities in Helmand province have run away. The Americans take them back after heavy fighting, for just one day, two days, and then they run away again, because the Taliban is really completely in control of the area.&nbsp; <br />  <br /> I interviewed many Taliban leaders; one was an ex-minister, Mullah Abdul Azad. He is the leader of the Taliban in Kabul. The Afghani army surrounds his house all the time. When I went to see him he gave me food and [somewhere to] sleep and tea and everything, so we talked about this a lot. He said, &ldquo;<em>One day the Americans will come to this house to talk to us about the situation in Afghanistan</em>.&rdquo; He promised me this. <br />   <br /> <strong><em>So tell us about the Taliban. Are there &lsquo;moderate&rsquo; and &lsquo;extremist&rsquo; political factions within the Taliban? Do you think that the Americans will be able to negotiate?</em></strong> <br />  <br /> Absolutely not. Absolutely not. The Taliban are a united group who make decisions together. Brown is trying to talk to moderate Taliban; however, moderate Taliban do not take decisions independently from the other Taliban. The people in Kabul, they are the face of the Taliban. They don&rsquo;t do anything military, they&rsquo;re the political half of the Taliban. Their fathers fought, you know, and they believed, and they fought the Soviets because of religion and communism first, and secondly because of the land. The same has happened - the Americans are like the Soviets now. Many people who are not religious look at the Taliban and want to fight the Americans because of the land. Most Afghan people believe that they are under occupation and they have to do something with America. <br />  <br /> I asked Taliban leader Mullah Abdul Azad, &ldquo;<em>Where does your money come from?</em>&rdquo; He said, &ldquo;<em>We never had money from drugs. We fight the Afghan army for sometimes one hour, sometimes three hours. We control the area, we take everything from them, weapons, money. And some people give us money</em>.&rdquo; A Taliban leader said to me, &ldquo;<em>We have weapons, fighters, and the land to fight on. We can continue for many many years, and if the Americans do not believe us they must go and ask the Soviets, they will have their answer. When they come in, we go. When they&rsquo;re just standing around, we attack them. And then we go back. We can hold an area for a day or a maybe few hours,  just to show everyone that we are still here and we can fight America, that America is nothing</em>.&rdquo; <br />  <br /> <strong><em>What does all this have to do with &lsquo;America&rsquo;s Most Wanted&rsquo; - Osama Bin Laden? <br /> </em> <br /> </strong>Some people, he, and al Qaeda, were created by America. People say that Osama&rsquo;s time is over in Afghanistan. Everyone believes that al Qaeda is just a name; just a few people maybe. Just cassettes on al Jazeera. One guy, sitting in his bathroom, you know, saying, &ldquo;We will fight America!&rdquo; Maybe he&rsquo;s with the CIA - I don&rsquo;t know! <br />  <br /> <strong><em>In the UK, especially in Brighton, you report on the protest scene. Are people in Afghanistan aware that there&rsquo;s a big movement against the war?</em></strong> <br />   <br /> Well, I&rsquo;m sorry to tell you but so many people are without TVs. In some areas people don&rsquo;t know even who it is that they are fighting. They haven&rsquo;t gone to school, they&rsquo;ve lived in the mountains for many, many years, and they don&rsquo;t even know who the war is between! <br />   <br /> <strong><em>Before you left did you see any evidence of any development?</em></strong> <br />  <br /> When I came to the airport I saw there was one room, just a normal room - not even chairs, paintings, anything like that, not like an airport [lounge] at all. But when you leave you&rsquo;d think you&rsquo;re in an Israeli airport. Everybody in smart uniforms, x-ray machines, everybody checked one by one. I asked about this because normally they do this when people come to a country. They told me, &ldquo;<em>Well, if you want to bring weapons here we have many more. You would be crazy to bring heroin to Afghanistan when we give all the world its heroin. So we don&rsquo;t need anything from outside</em>.&rdquo; They said the Americans built this building, the airport. It was the only new thing I saw there. <br />  <br /> * Read the full interview - coming soon to the website features section.. <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=afghanistan&source=708">afghanistan</a>, <a href="../keywordSearch/?keyword=al-qaeda&source=708">al-qaeda</a>, <a href="../keywordSearch/?keyword=taliban&source=708">taliban</a>, <a href="../keywordSearch/?keyword=war+on+terror&source=708">war on terror</a></div>
<?php echo showComments(641, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>A HONDURAN LOBO-TOMY</h3>

<p>
Hundreds of thousands of Hondurans protested the appointment of a new president last week, taking to the streets in defiance of police repression. <br />  <br /> In a rejection of the legitimacy of the new president, protesters flooded the streets of the capital Tegucigalpa, San Pedro Sula and around the country. In the capital, the crowd gathered at the airport to see deposed president Manuel Zelaya leave for exile in the Dominican Republic. <br />  <br /> Porifrio Lobo was sworn in as president last Wednesday (27th) after triumphing in elections held last November following June&rsquo;s coup (see <a href='../archive/news692.htm'>SchNEWS 692</a>). The elections were widely touted as a return to democracy with Honduran and American officials crowing over how the 63% turnout legitimised the process. However, independent investigations showed the figure appeared to have been plucked out of the air and that even the more accurate figure of 49% was tarnished by allegations of fraud, intimidation and coercion. It was still enough for Hilary Clinton to swoon over the Honduran people&rsquo;s &ldquo;commitment to democracy&rdquo;. <br />  <br /> Until his flight into exile, Manuel Zelaya remained holed up in the Brazilian embassy, issuing tetchy press releases. His final chance for a return to power before elections slipped away when he agreed a deal with coup leader Roberto Micheletti that allowed him to return to power but (always check the small print) only if Congress voted for it. They didn&rsquo;t. The Americans backed the deal and Zelaya retreated to the embassy. <br />   <br /> The Honduran resistance movement has kept opposition to the coup alive throughout. The state has responded with disappearances, torture and assassinations. Military police shot dead five members of a resistance organisation on the streets of Teguicigalpa. A well known LGBT activist involved in the opposition, Walter Trochez, escaped from a police van after he had been abducted and beaten. A week later he was killed in a drive-by shooting. The decapitated body of Carlos Turcios, vice president of a regional resistance movement, was found days after he was kidnapped by police. Opposition radio stations have been burned to the ground and newspaper journalists abducted and tortured. <br />  <br /> Representatives from America, Colombia, Panama, the Dominican Republic and Taiwan attended Lobo&rsquo;s swearing in ceremony. Although most Latin American countries refuse to recognise the new president, even there doors were left ajar. A Brazilian official stated, &ldquo;<em>For now, Brazil does not recognise Lobo&rsquo;s government.</em>&rdquo; <br />  <br /> Lobo&rsquo;s first act as president was to give amnesty to the soldiers, politicians and judges behind the June 28th coup. The day before the ceremony a judge dismissed all charges against six military commanders implicated in the coup. <br />     <br /> With the world readying itself to look away, the Honduran resistance is bracing itself for the next stage in the ongoing assault on their liberty. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=coup&source=708">coup</a>, <a href="../keywordSearch/?keyword=honduras&source=708">honduras</a>, <a href="../keywordSearch/?keyword=manuel+zelaya&source=708">manuel zelaya</a>, <a href="../keywordSearch/?keyword=repression&source=708">repression</a></div>
<?php echo showComments(642, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>INSIDE SCHNEWS</h3>

<p>
<strong>EDO Decommissioner James Elijah Smith</strong> has been moved, again. He is now on remand at Sheppey Prison, Church Road, Eastchurch, Sheerness, Kent ME12 4DZ. Show your support by writing or visiting (VP7551).  <br />  <br /> Visiting times are 2pm-4pm, to book call 01795 882272. <br />  <br /> *see <a href="http://www.smashedo.org.uk/supportelijahsmith.html" target="_blank">http://www.smashedo.org.uk/supportelijahsmith.html</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=edo+decommissioners&source=708">edo decommissioners</a>, <a href="../keywordSearch/?keyword=prisons&source=708">prisons</a>, <a href="../keywordSearch/?keyword=smash+edo&source=708">smash edo</a></div>
<?php echo showComments(643, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>STOP 'N' SUE</h3>

<p>
Climate campers from the 2008 Kingsnorth power station protest have won a High Court ruling branding the stop and search tactics used by police &lsquo;unlawful&rsquo; and in violation of three Articles in the European Human Rights Constitution; the right to privacy, freedom of expression and freedom of association. <br />  <br /> The police operation at Kingsnorth was the largest and most expensive of its kind in UK history. Kent Police admitted &lsquo;total surrender&rsquo; after it was revealed that top brass were giving orders for blanket stop and searches in briefings. <br />  <br /> The decision has now opened the way for the other 3,500 people that were stopped and frisked at the gathering to sue the police and submit claims for damages. <br />  <br /> * <a href="http://www.climatecamp.org.uk/get-involved/working-groups/legal/kingsnorth-judicial-review" target="_blank">www.climatecamp.org.uk/get-involved/working-groups/legal/kingsnorth-judicial-review</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=708">camp for climate action</a>, <a href="../keywordSearch/?keyword=kingsnorth&source=708">kingsnorth</a>, <a href="../keywordSearch/?keyword=police&source=708">police</a></div>
<?php echo showComments(644, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>VOULEZ VOUS COUCHE?</h3>

<p>
After the closure of the much needed night shelter last week (see <a href='../archive/news706.htm'>SchNEWS 706</a>), No Borders and S&ocirc;S Soutien aux Sans Papiers have set up a large warehouse in Calais to offer an autonomous safe space for the migrant community and their supporters. <br />  <br /> The Kronsdadt building in the town will provide shelter and a forum for debate, information and practical solidarity. Talks with the local residents of Calais are planned to discuss how individuals and organisations can join in with the project and show support. <br />  <br /> <a href="*http://calaismigrantsolidarity.wordpress.com" target="_blank">*http://calaismigrantsolidarity.wordpress.com</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=calais&source=708">calais</a>, <a href="../keywordSearch/?keyword=immigration&source=708">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=708">no borders</a></div>
<?php echo showComments(645, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>TREADING A FINE LINE</h3>

<p>
In the wee hours of Wednesday morning (3rd) the Fine Line Art Collective targeted the Brighton branch of RBS by presenting a &lsquo;free public art installation&rsquo; on their doorstep. Stripes of black and red paint were sloshed across the floor outside RBS&rsquo; front entrance and the cash machine was decorated with black paint oozing from its money spitting orifice. <br />  <br /> The group chose the big bonus bank for its involvement in the tar sands extraction in Canada which is forcing indigenous people off their lands, and is shaping up to be the world&rsquo;s biggest emitter of CO2. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=royal+bank+of+scotland&source=708">royal bank of scotland</a>, <a href="../keywordSearch/?keyword=tar+sands&source=708">tar sands</a></div>
<?php echo showComments(646, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>DIAMONDS ARE FOR NEVER</h3>

<p>
On Wednesday (3rd), demonstrators from Survival International protested outside top nob jewellers Tiffany in London, San Francisco, Madrid, Paris and Berlin. Tiffany have been donating to the government in Botswana to fund boreholes in the Kalahari Game Reserve that are for the exclusive use of the game animals in the reserve. The indigenous people whose ancestral land they drilled to access the boreholes are forbidden to use them. <br />  <br /> The Botswanan government kicked the indigenous Bushmen tribe out of the area in 2002 then cut off their water supply. Four years the Botswanan High court condemned this as illegal, but officials still refuse to allow the Bushmen access to water. As well as drilling new boreholes that are reserved for animals but not people, the government has also allowed a tourist lodge (complete with swimming pool) with a &lsquo;Bushmen experience&rsquo; on the illegally seized land. <br />  <br /> Supporters of Survival International handed letters in to the Tiffany stores and picketed outside, telling the chain to support the indigenous people before they support the government. <br />  <br /> <a href="http://*www.survivalinternational.org/news/5500" target="_blank">*www.survivalinternational.org/news/5500</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=africa&source=708">africa</a>, <span style='color:#777777; font-size:10px'>bushmen</span>, <a href="../keywordSearch/?keyword=indigenous+people&source=708">indigenous people</a>, <span style='color:#777777; font-size:10px'>tifanys</span></div>
<?php echo showComments(647, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>OVER THE MAINSHILL?  </h3>

<p>
The eviction of Mainshill Solidarity Camp (see <a href='../archive/news697.htm'>SchNEWS 697</a>) finished last Friday, after five days of resistance. A total of 45 people were arrested. The last day was  a little confusing for the bailiffs as they discovered the last person in the tunnel didn&rsquo;t exist, but there was still an unseen protester lurking in the woods. He was forced out when the bulldozers started felling trees all around him. They also arrested someone for taking photographs. <br />   <br /> Bailiffs often put speed before safety. They cut a walkway with someone stood on it and lowered people from trees with lock-ons still attached. Some people were injured while being cut out of lock-ons. <br />   <br /> The eviction has been a massive financial burden, bad publicity for Scottish Coal and delayed work on site. Scottish Coal were apparently hoping the camp would be deserted during the freezing winter, but they obviously haven&rsquo;t experienced that warm glow you get from resisting corporate greed within a supportive and lively community (and a good wood burner). <br /> One Mainshiller told SchNEWS, &ldquo;<em>The experience and inspiration we&rsquo;ve gained will fuel further resistance to new coal in Scotland, building on the alliances we&rsquo;ve been developing. We must stop open casting coal now for any chance at creating a sustainable future</em>.&rdquo; <br />  <br /> *see <a href="http://www.coalactionscotland.noflag.org.uk" target="_blank">www.coalactionscotland.noflag.org.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=mainshill&source=708">mainshill</a>, <a href="../keywordSearch/?keyword=protest+camp&source=708">protest camp</a>, <a href="../keywordSearch/?keyword=scottish+coal&source=708">scottish coal</a></div>
<?php echo showComments(648, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BLAIR FACED CHEEK  </h3>

<p>
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/708-Chilcot-1-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/708-Chilcot-1-sm.jpg"  /></a></td></tr></table>As Blair smirked and simpered his way through his evidence at the Chilcot Inquiry, protesters outside demonstrated against the illegal Iraqi invasion and the buck passing that was taking place in the conference centre. <br />  <br /> Numbers were in the hundreds all day, with a &lsquo;Bliar&rsquo; handcuffed and on trial outside. At lunchtime the family members of soldiers of killed in Iraq who had been listening to the evidence inside came out to report the goings on and made speeches to the assembled crowd. <br />  <br /> As the inquiry finished, around 500 people gathered round the doors to tell Blair exactly what they thought as he left. True to form though, he&rsquo;d chickened out of facing his ex-public and disappeared, presumably down one of the many secret Westminster exits. <br />  <br /> *For pics, see <a href="http://www.indymedia.org.uk/en" target="_blank">http://www.indymedia.org.uk/en</a> <br /> /2010/02/445546.html <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>blair</span>, <span style='color:#777777; font-size:10px'>chilcot</span>, <a href="../keywordSearch/?keyword=iraq&source=708">iraq</a></div>
<?php echo showComments(649, 9); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>AND FINALLY...</h3>

<p>
<strong><em>No fin ventured nuffin gained.</em></strong> <br />  <br /> The interspecies environmental resistance movement is on. Total Oil have admitted that a lone eco-warrior swordfish launched a swift sabotaging attack on an oil loading pipe in Angola recently, causing a three day delay in tanker shipments. <br />   <br /> The shashbuckling young swordfish had obviously been paying attention at er... school (and eating a diet rich in omega 3 fish oils no doubt). It was well briefed on Total&rsquo;s environmental misdeeds and humanitarian mishaps and  prepared to back its eco-convictions to the hilt. <br />   <br /> Literally. We&rsquo;re now holding out for Dolphins with dynamite and porpoises with purpose wielding manta-ray-guns. Er...This article should have been funnier, but, on this occasion, the swordfish has proved mightier than the pen. <br />
</p>

</div>
<?php echo showComments(650, 10); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS warns all readers, Osama is a has Bin, but the Taliban has a plan . Honest. </p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

