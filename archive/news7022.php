<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 702 - 4th December 2009 - Dow And Out In Bhopal</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bhopal, india, corporate crime" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 702 Articles: </b>
<p><a href="../archive/news7021.php">If You're Not Swiss-ed Off...</a></p>

<b>
<p><a href="../archive/news7022.php">Dow And Out In Bhopal</a></p>
</b>

<p><a href="../archive/news7023.php">When Chips Were Down</a></p>

<p><a href="../archive/news7024.php">Uni-lateral Decision</a></p>

<p><a href="../archive/news7025.php">Inside Schnews</a></p>

<p><a href="../archive/news7026.php">Dubai Now, Pay Later</a></p>

<p><a href="../archive/news7027.php">De-tension Centre</a></p>

<p><a href="../archive/news7028.php">Schnews In Brief</a></p>

<p><a href="../archive/news7029.php">Raising The Barclays</a></p>

<p><a href="../archive/news70210.php">Dale Farm Alert</a></p>

<p><a href="../archive/news70211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th December 2009 | Issue 702</b></p>

<p><a href="news702.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DOW AND OUT IN BHOPAL</h3>

<p>
"<em>It took some time, but we managed this quickly, and we can say that we are providing 100% clean water [in Bhopal]. There was nothing hazardous for human lives there</em>." - <strong>Madhya Pradesh Chief Minister Shivraj Singh Chouhan, on the 25th anniversary of the world's worst industrial disaster at Bhopal.</strong> <br />  <br /> Nothing Tony Blair has ever said about the Iraq invasion could come close to this level of outright bullshit, and no climate-change denier or flat earth society proponent could speak so much in the face of reality or scientific evidence. <br />   <br /> On December 3rd 1984, the Union Carbide plant at Bhopal leaked 27 tonnes of highly toxic methyl isocyanate gas - used to manufacture the pesticide Sevin &ndash; killing some 8-10,000 within three days, leading to a current death toll of 23,000, and some 150,000 are left with chronic conditions, such as neurological, organ and chromosomal damage; tuberculosis, cancer and other diseases. One in 25 babies born in the area have congenital defects. The company has never been brought to justice for it. <br />  <br /> As the site was never cleaned up, the toxins have seeped into the water system, and now analyses commissioned by the Bhopal Medical Appeal have shown that the soil has at least 16 contaminants at levels greatly exceeding WHO levels, including Carbon Tetrachloride toxicity at more than 2400 times the 'safe' level &ndash; which has actually risen by 270% in the past year as the chemicals leech further into the soil. The toxins in the ground water are known carcinogens and neurotoxins, particularly harmful to children and the unborn. <br />  <br /> It's been left to the independently run, internationally funded Sambhavna Clinic &ndash; set up in 1996 &ndash; to treat the victims, which sees 150 people every day for conditions such as liver disease, anaemia and all manner of organ damage. The clinic constantly sees new victims - those born with defects, 25 years later. <br />   <br /> The other side to this whole sordid story is the efforts that Union Carbide &ndash; and its now parent company, Dow Chemicals &ndash; as well as the US Government and Chamber Of Commerce, have gone to avoid cleaning up the site, offering due compensation or facing criminal charges. <br />  <br /> The morning after the disaster there was clearly one thing on Union Carbide's mind &ndash; how they could get away with this scot-free. As the relief effort began in the immediate aftermath, emergency medical efforts were hampered by UC not disclosing what toxic chemicals had been unleashed. In the months that followed a US Court Judge demanded that UC put between $5-10 million into emergency medical funds - eventually $5 million went to the Red Cross. <br />   <br /> As a settlement pay-out for the victims, UC first offered $100 million &ndash; yet had the accident happened in the US, the liability would have been closer to $10bn. In 1989 UC paid a $470 million settlement &ndash; with a maximum of $1000 going to victims &ndash; and at the same time reclaiming the $5 million they gave the Red Cross, forcing them to close all their hospitals in Bhopal. By 1989 UC had paid $50 million on lawyers and an unknown amount to PR company Burson Marsteller to worm out of the fiasco. And all this was before any money had been spent cleaning up the site, which they claimed was the responsibility of the local government. <br />   <br /> Straight away criminal proceedings began against UC, with charges being brought against the CEO of UC, Warren Anderson, and several key leaders of UC India and those running the plant. Anderson and others were arrested, but he was bailed after four days (for the non-bailable offence of culpable homicide) &ndash; under promise to return when summonsed, but was flown out by an Indian State plane and hasn't returned since. Anderson was summonsed again in 1987 in a case brought by the Indian Central Bureau of Investigation, with further ignored summonses in the years that followed.&nbsp; <br />  <br /> Finally in 2003 the Indian Govt attempted to extradite Anderson, but Freedom Of Information requests by US Bhopal campaigners have shown the efforts the US Govt have gone to prevent Anderson being extradited, or Bhopal having any impact on US international corporate activity. The US Chamber Of Commerce hired a 'who's who of high-powered law firms' to protect UC and Anderson. Attempts to extradite Anderson again resumed this year, meanwhile he remains a 'fugitive', despite his New York address being well known. <br />  <br /> There has been a week of events in Bhopal to commemorate the disaster, including continual vigils and processions. In London yesterday a choir gathered at Trafalgar Square, and there were other events and demos around the world. <br />  <br /> The Bhopal Medical Appeal and Amnesty have been touring Europe in a bus since October travelling through seven countries from Sweden down to Italy, stopping off at Dow Chemicals plants, visiting campus's, human rights centres, and speaking to MEPs. The bus is due back in Britain soon, and will tour Britain next year visiting festivals, town centres and anything it can fit in &ndash; see <a href="http://bhopalbus.com" target="_blank">http://bhopalbus.com</a> <br />  <br /> * See also <a href='../archive/news238.htm'>SchNEWS 238</a>, <a href='../archive/news580.htm'>580</a>, <a href='../archive/news637.htm'>637</a> <br />   <br /> * For info about the UK-based International Campaign for Justice in Bhopal see <a href="http://bhopal.net" target="_blank">http://bhopal.net</a> , for more about the Medical Appeal see <a href="http://bhopal.org" target="_blank">http://bhopal.org</a> <br />  <br /> * To read the BMA water report see <a href="http://water.bhopal.org" target="_blank">http://water.bhopal.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bhopal&source=702">bhopal</a>, <span style='color:#777777; font-size:10px'>corporate crime</span>, <a href="../keywordSearch/?keyword=india&source=702">india</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(595);
			
				if (SHOWMESSAGES)	{
				
						addMessage(595);
						echo getMessages(595);
						echo showAddMessage(595);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>