<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 707 - 29th January 2010 - Lanarky In Action</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mainshill, climate change, coal, direct action, protest camp, scotland" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 707 Articles: </b>
<b>
<p><a href="../archive/news7071.php">Lanarky In Action</a></p>
</b>

<p><a href="../archive/news7072.php">Citizen's Arrest Of The Week</a></p>

<p><a href="../archive/news7073.php">Edl: Gone To Potteries</a></p>

<p><a href="../archive/news7074.php">Calais: Gimme Shelter</a></p>

<p><a href="../archive/news7075.php">Festival Clampdown: Toking The Piss</a></p>

<p><a href="../archive/news7076.php">Crawley: No Boarders</a></p>

<p><a href="../archive/news7077.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/707-mainshill-lg.jpg" target="_blank">
													<img src="../images/707-mainshill-sm.jpg" alt="It's almost as if someone has tipped them off..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 29th January 2010 | Issue 707</b></p>

<p><a href="news707.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LANARKY IN ACTION</h3>

<p>
<strong>AS THE NATIONAL EVICTION TEAM MOVE IN ON MAINSHILL...</strong> <br />  <br /> Early Monday morning bailiffs and police under the auspices of the <strong>National Eviction Team</strong>  (<a href="http://ukevict.com" target="_blank">http://ukevict.com</a>) began the long expected eviction of <strong>Mainshill Solidarity camp</strong>. (See <a href='../archive/news681.htm'>SchNEWS 681</a>) <br />   <br /> At the time of going to press the eviction was still ongoing with the tunnel teams working 24/7 to get at the hold outs underground - 43 arrests have been made so far - the majority charged with aggravated trespass. <br />  <br /> Mainshill in South Lanarkshire, is the proposed site of a massive open-cast coal mine. Ten years of local opposition (Mainshill is set to be the fifth such mine in the area) culminated in the protest site. The camp was occupied 7 months ago in solidarity with communities in the Douglas Valley and support has been consistent ever since, with the camp kept well supplied by neighbours -  including a full Christmas dinner.  The site is owned by Lord Home, who is set to profit from allowing Scottish Coal to dig out 1.7 million tonnes of coal from Mainshill. <br />  <br /> Local communities have been blighted by the detrimental health impacts of the 4 existing open casts in the immediate area. Harry Thompson, former chairman of the Douglas Community Council, said: <br />  <br /> &ldquo;<em>Despite massive community opposition to the mine at Mainshill, Scottish Coal and South Lanarkshire Council continue to disregard the interests of those living in proximity to the mines. The particulate matter released in the open cast mining process in this area has caused unusually high rates of cancer and lung disease. Granting permission to a new mine 1000 metres from the local hospital is the final straw</em>&rdquo;. Local supporters arrived as soon as news of the eviction spread. Interestingly local press were barred from site but national media were permitted entry (see Carluke Gazette 28/01/10). <br />  <br /> Mining in the Douglas Valley is intended to feed Britain&rsquo;s increasing reliance on coal as an energy source. Coal taken from the proposed mine at Mainshill would result in the release of 3.4 million tonnes of carbon dioxide into the atmosphere if burned. If this and the other 18 proposed mines in Scotland go ahead it will be a massive contributor to climate change. The Scottish Government is in the process of approving up to 33 new open cast coal sites. <br />  <br /> Rather than sitting round the firepit, the Mainshill crew have spent the last seven months waging a continual direct action campaign against the mining. Campers have organised physical blockades of work and at night groups of autonomous pixies have wreaked havoc on site equipment, with drilling rigs, vehicles and even giant timber harvesting equipment being put out of action. <br />   <br /> Spread out over 360 acres, the camp&rsquo;s fortifications are &ldquo;complex and varied&rdquo; and, thanks to an eviction tip-off, are now well manned. Over the months activists have dug tunnels, built tripods and hung sky-rafts from the trees. The &lsquo;fort&rsquo;, which took the whole of Tuesday to evict, was described by one treehugger as a &ldquo;multi-layered defence&rdquo; and by another as &ldquo;big logs and random metal and shit topped by a precarious scaffolding tower&rdquo; <br />   <br />  <strong>SchNOTE </strong>- A skyraft is a platform suspended between several trees. <br />  <br /> By the end of Monday there had been 19 arrests. Arrestees are being held overnight and after being brought before the courts are bailed away from site. One of the ground support monkeys told SchNEWS, &ldquo;<em>The two main barricades, the bunker and the &lsquo;buckfast communal&rsquo; were JCB-ed, with the underground lock-ons in the bunker proving a challenge for the bailiffs. Three treehouses at the &lsquo;buckfast&rsquo; gave the climbing team a run for their money, as protesters occupied walkways and climbed into the very highest branches of the trees. Behind one of the barricades a double-layered tripod with a prism shaped skyraft hanging from its apex cost the NET another three or four hours. It was eventually defeated when NET built their own walkway above the raft, attached ropes around it, cut the existing ropes which were suspending it, and lowered it to the ground. In a spectacular fit of risky behaviour, the NET then took down the double-layered tripod structure by kicking it. <br />   <br /> Bulldozers were forced to stop work on Wednesday night after discovering tree-sitters in an area of young pine trees known as &lsquo;the plantation&rsquo;. An overflight by a police helicopter was enough to convince contractors that there was no-one there. Floodlit bulldozers were only halted after frantic phone calls were made to the camps liaison with the eviction team. <br />  <br /> Also on Wednesday the nearby Ravenstruther coal rail terminal was brought to a standstill by one locked-on protester. Ravenstruther is where South Lanarkshire&rsquo;s coal is transported en masse to power stations in England.  In solidarity with the Mainshill Solidarity Camp (which is, like, solidarity squared) the plucky activist climbed to the top of a digger and locked on by his leg. Around fifteen coal trucks were then forced to dump their loads outside the terminal. <br />  <br /> On Thursday it was the turn of the sycamores and the &ldquo;Ewok village&rdquo; - the last major above ground hold outs on site. Large numbers of ground bailiffs and police were brought in to allow the specialist eviction teams to work without the need for a perimeter fence. There&nbsp;was a surprise waiting for them in the &ldquo;Ewok village&rdquo;, the macabrely entitled Deathworm - an industrial bin suspended between two trees with somebody locked on inside and surrounded by metal obstacles to the eviction team&rsquo;s cutters. The rate of arrests slowed with only nine made. <br />  <br /> One activist told SchNEWS, &ldquo;Although the majority of the site has now been evicted, the site is so big that it&rsquo;s impossible to fence off. People are sneaking in the perimeter and getting up trees. But what&rsquo;s important to remember is that the eviction of the site actually makes us more flexible, a lot of energy has gone into maintaining site, that energy can now be directed into action. The campaign against the Mainshill Opencast isn&rsquo;t over yet</em>.&rdquo; <br />  <br /> * see <a href="http://coalactionscotland.noflag.org.uk" target="_blank">http://coalactionscotland.noflag.org.uk</a> <br />  <br /> <a href="*http://www.indymedia.org.uk/en/2010/01/445308.html?c=on#c241714" target="_blank">*http://www.indymedia.org.uk/en/2010/01/445308.html?c=on#c241714</a> <br />  <br /> <a href="*http://www.indymediascotland.org" target="_blank">*http://www.indymediascotland.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=707">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=707">coal</a>, <a href="../keywordSearch/?keyword=direct+action&source=707">direct action</a>, <a href="../keywordSearch/?keyword=mainshill&source=707">mainshill</a>, <a href="../keywordSearch/?keyword=protest+camp&source=707">protest camp</a>, <a href="../keywordSearch/?keyword=scotland&source=707">scotland</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(634);
			
				if (SHOWMESSAGES)	{
				
						addMessage(634);
						echo getMessages(634);
						echo showAddMessage(634);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


