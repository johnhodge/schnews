<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 683 - 10th July 2009 - Bobbies On The Bleat</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police brutality, whitewash, right to protest, summer of rage, g20 summit, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 683 Articles: </b>
<b>
<p><a href="../archive/news6831.php">Bobbies On The Bleat</a></p>
</b>

<p><a href="../archive/news6832.php">Hans Off!</a></p>

<p><a href="../archive/news6833.php">Won&#8217;t Take It Zelaya-n Down  </a></p>

<p><a href="../archive/news6834.php">Oh, The Humanity</a></p>

<p><a href="../archive/news6835.php">Inside Schnews</a></p>

<p><a href="../archive/news6836.php">Nice Tosia, To  See You Nice...  </a></p>

<p><a href="../archive/news6837.php">Call Out! Keen To Squat!   </a></p>

<p><a href="../archive/news6838.php">Climate Campaigns Run Hot</a></p>

<p><a href="../archive/news6839.php">Digging In  </a></p>

<p><a href="../archive/news68310.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/683-chairman-mao-lg.jpg" target="_blank">
													<img src="../images/683-chairman-mao-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 10th July 2009 | Issue 683</b></p>

<p><a href="news683.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BOBBIES ON THE BLEAT</h3>

<p>
<strong>A SchNEWS RETORT TO THE POLICE REPORT</strong> <br />  <br /> SchNEWS has cast our cynical eye over the HMIC (Her Majesty&rsquo;s Inspectorate of Constabulary) report about the policing of the G20 protests, &lsquo;Adapting To Protest&rsquo;. The HMIC say they welcome feedback on the report. Well here&rsquo;s SchNEWS&rsquo; take... <br />  <br />   So why was this report commissioned? Maybe something about the police being out of order, the fact an innocent bystander was killed or that all this was caught on camera? In the words of the HMIC: &ldquo;<em>The high volume of publicly sourced footage of the protests, including the events leading up to the death of Ian Tomlinson, has demonstrated the influence of &lsquo;citizen journalists&rsquo; &ndash; members of the public who play an active role in collecting, analysing and distributing media themselves. Consequently, individual and collective police actions are under enormous public scrutiny</em>.&rdquo;  <br />  <br /> The report is more concerned about the perception of the police than the police actions themselves. Police were initially pleased with media coverage, but &ldquo;<em>by the 5th April this was becoming more critical. This intensified following the emergence of images relating to the death of Ian Tomlinson</em>.&rdquo; One suggested solution to this is to have &ldquo;embedded&rdquo; journalists with police on the front line! Police say they face &ldquo;<em>dilemmas around using potentially sensitive information connected with death or serious injuries at public order events which may subsequently become evidence in legal proceedings</em>.&rdquo; But this didn&rsquo;t stop them spinning a whole host of lies over the death of Ian Tomlinson (claiming there was no police contact, medics were assaulted, and he died from a heart attack - <a href='../archive/news672.htm'>SchNEWS 672</a>). <br />  <br />   We are told that in relation to the press there should be &ldquo;<em>Awareness and recognition of the UK press card by officers on cordons, to identify legitimate members of the press</em>.&rdquo; But what use is a press card when police officers don&rsquo;t give a damn if you are press or not? On April 2nd there was a Section 14 notice (under the Public Order Act) issued to the press. A City of London Police Inspector told the press to &ldquo;<em>Go away for half an hour and possibly come back to help us resolve this situation</em>.&rdquo; He was acting on behalf of Commander Broadhurst who was top cop for the day &ndash; so harassment of the press was coming right from the top and being carried out by an Inspector. Yet the report makes no mention of this &ndash; it is not a case of a few officers failing to recognise press cards, it is a systematic abuse of the police&rsquo;s powers to manage the news and restrict the freedom of the press (it&rsquo;s also a shame more press don&rsquo;t stand up for press freedom and refuse to cooperate with unreasonable police demands). <br />  <br /> In the build up to the G20 protests, there were lots of media reports anticipating violence. The report has a breathtaking omission: &ldquo;<em>An article titled &lsquo;The Summer of Rage Starts Here&rsquo; was published on a popular protester website by a member calling themselves London Anarchists</em>.&rdquo; But it totally fails to mention that the phrase &ldquo;Summer of Rage&rdquo; was coined by David Hartshorn, who heads the Met&rsquo;s public order branch. Also,&nbsp;no mention is made of police blogs gleefully spoiling for a fight, or Commander Simon O&rsquo;Brien&rsquo;s boast: &ldquo;We&rsquo;re up to it and we&rsquo;re up for it.&rdquo; Such spin can only be designed to try and deflect blame from the police themselves for ratcheting up the tension. <br />  <br /> The report also conducted an opinion poll survey on the public&rsquo;s attitude to the police and protesting (well, they need to frame their PR correctly). It did admit that there was a split on whether the police handled the G20 protests well, but did show the public were largely favourable of the police overall. Also it showed a distinct age and class bias, with young people and the working classes being the least in favour of the police &ndash; reflecting the obvious fact that the police are there for the rich rather than the poor and likely to hassle youth. <br />  <br /> Curiously, the report is illustrated throughout with rent-a-mob riot porn photos: masked protesters, a fire, smashed windows, brew crew with a bottle... but funnily no snarling riot cops without ID numbers hitting people with truncheons! <br />  <br /> There were some acknowledgements that the police did cock up on the day &ndash; apparently we will see all Met officers displaying their numbers in future (we wait with baited breath). The report is critical of police planning for the day saying that a whole protest should not be criminalised &ndash; it acknowledges that people have a right to protest and that this should be protected under human rights law: &ldquo;<em>the police, are required to show a certain degree of tolerance towards peaceful gatherings... even if these protests cause a level of obstruction or disruption</em>.&rdquo; How much disruption is a matter of &ldquo;debate&rdquo; according to the report. <br />  <br /> The police claimed they were at a disadvantage when it comes to communication on the ground: they have to cope with a &ldquo;<em>flexible and responsive protest community which is capable of advanced communication and immediate reaction to events on the ground. This is in stark contrast to the traditional communication capabilities of the police</em>.&rdquo; So let&rsquo;s get this straight &ndash; the police have at their disposal CCTV street coverage, helicopters tracking movements of crowds, radio communication equipment and a central command bunker overseeing the whole operation and they are at a disadvantage over us with mobile phones and a make-it-up-as-you-go-along attitude on the ground?! <br />  <br /> The most controversial tactic of all, &lsquo;kettling&rsquo; gets the thumbs up from the HMIC, which they qualify as being suitable as long as it is proportionate &ndash; but police should let people leave the areas if they er, like the look of you (ditch the black hoodie for a suit?). The report recommends updating ACPO&rsquo;s public order manual and says that the police need to adapt their public order tactics. However, the police have already tried to deflect criticism of their tactics, with Commander Bob Broadhurst, the head of the Public Order Unit, laying the blame at the feet of inexperienced ordinary police officers for the violent and repressive policing at the G20. They ignore the whole issue of kettling and the fact that the two most reported instances of police abuse (the death of Ian Tomlinson and the blatant assault on Nicola Fisher) were committed by the TSG - the Met&rsquo;s fully trained riot goons. The police are still trying to spin the few bad apples line, when we know it&rsquo;s the whole damn cart that is rotten. <br />  <br /> * See full report at: <a href="http://www.inspectorates.justice.gov.uk/hmic/docs/ap" target="_blank">www.inspectorates.justice.gov.uk/hmic/docs/ap</a> <br />  <br /> There will be a second instalment later in the year with &ldquo;a systematic review... to inform the ongoing debate on the policing of protest.&rdquo; We can&rsquo;t wait! <br />  <br /> More information following the campaign against police violence, inspired by Ian Tomlinson&rsquo;s death: <a href="http://againstpoliceviolence.blogspot.com" target="_blank">http://againstpoliceviolence.blogspot.com</a> <br />  <br />   <font face="courier new">&nbsp;</font> <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 228" width="228px"><a href="../images/683-g20-police-report-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/683-g20-police-report-sm.jpg"  alt='Conclusion: A LOAD OF HORSESHIT'  /></a></td></tr></table><font face="courier new"><strong><font size="4">Conclusion: A LOAD OF HORSESHIT</font></strong></font><font face="courier new"> <br />  <br /> </font> <br />   <font face="courier new">Here&rsquo;s an actual page from the G20 police report, wrapping up many pages of lucid argument and rigorous research. But just what is that pile of brown stuff next to the word &lsquo;conclusion&rsquo;? Under magnification, we can clearly see that it is in fact a well chosen visual metaphor to sum up the report. It&rsquo;s all complete horseshit. <br /> </font>&nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br /> &nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=g20+summit&source=683">g20 summit</a>, <a href="../keywordSearch/?keyword=police+brutality&source=683">police brutality</a>, <a href="../keywordSearch/?keyword=protest&source=683">protest</a>, <span style='color:#777777; font-size:10px'>right to protest</span>, <span style='color:#777777; font-size:10px'>summer of rage</span>, <span style='color:#777777; font-size:10px'>whitewash</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(393);
			
				if (SHOWMESSAGES)	{
				
						addMessage(393);
						echo getMessages(393);
						echo showAddMessage(393);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>