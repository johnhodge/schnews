<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 758 - 11th February 2011 - Shopped Cops Dropped</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, squatting, police, ipcc" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 758 Articles: </b>
<p><a href="../archive/news7581.php">The Withdrawal Method</a></p>

<p><a href="../archive/news7582.php">Edl Go To Hell, Well Luton</a></p>

<p><a href="../archive/news7583.php">Egypt: Tahrir We Go Again</a></p>

<p><a href="../archive/news7584.php">Greece: Hunger Strikes</a></p>

<p><a href="../archive/news7585.php">Greece: Trash Talk</a></p>

<p><a href="../archive/news7586.php">Harper Scarper</a></p>

<b>
<p><a href="../archive/news7587.php">Shopped Cops Dropped</a></p>
</b>

<p><a href="../archive/news7588.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th February 2011 | Issue 758</b></p>

<p><a href="news758.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SHOPPED COPS DROPPED</h3>

<p>
<p>
	Two cops who tried to fit up a squatter by planting dope on him, have been sacked from the Met. Their crimes would never have come to light if it hadn&rsquo;t been for the victim&rsquo;s ingenious use of a digital voice recorder concealed in his pocket. </p>
<p>
	Sgt Marcus Garvey and Pc Wayne Campbell faced a IPCC misconduct hearing during which they were found to have used unlawful force in Thacker&rsquo;s arrest and given inaccurate accounts, although they were acquitted by jury of perverting the course of justice last May. </p>
<p>
	The case began three years ago when the police broke into a squat in Kidbrooke, London with a sledgehammer, forcefully pulled Thacker out of the building, and then arrested him for possession of cannabis - which they&rsquo;d found in another room of the house. </p>
<p>
	The officers later claimed they had made the arrest outside the property and then broke into the property to check no-one else was inside. </p>
<p>
	Luckily, Thacker thought to turn on a voice recorder in his pocket - and the recording proved the cops had been lying. He presented the evidence whilst representing himself in court on the drugs charge, prompting the dropping of the case against him and the start of the case against the officers and the subsequent IPCC hearing. </p>
<p>
	Thacker is pretty meditative about the whole ordeal: &ldquo;<em>I&rsquo;ve thought about it a lot, yeah it&rsquo;s crap for them that they&rsquo;ve lost their jobs but who wants cops like that around? We don&rsquo;t need bullies around, especially with those that can abuse their powers. This is something I&rsquo;ve never really driven, it&rsquo;s always been the IPCC and the detective sergeants...I&rsquo;ve just given evidence, that&rsquo;s all. So yeah, live and learn, aye</em>.&rdquo; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1102), 127); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1102);

				if (SHOWMESSAGES)	{

						addMessage(1102);
						echo getMessages(1102);
						echo showAddMessage(1102);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


