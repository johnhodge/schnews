#!/usr/bin/perl
#
#  Archives all news files to comma separated text files
#

# Start main program

#UNIX:
#open(fileList,'/bin/ls -1 news*.htm|');

#DOS:
open(fileList,'dir /b news*.htm|');

open(issueFile, '>issues.txt');
open(articleFile, '>articles.txt');
open(failedFile, '>grabnews_failed.txt');

#DOS:
@newsFiles = <fileList>;

#UNIX:
#$_ = "<fileList>";
#@newsFiles = split/, */;

# Put headers on output files
print issueFile "issueno\tdate\twakeup\tfootf\tfootb\n";
print articleFile "issueno\tartno\tname\theadline\tbody\n";
foreach $filename (@newsFiles)
{
  processFile($filename);
}

close(fileList);
close(issueFile);
close(articleFile);
close(failedFile);

# End main program

sub processFile
{
  $myFilename = $_[0];
  chomp($myFilename);
  open(myFile, "$myFilename") or die "Unable to open $myFilename: $!";
  print "$myFilename...";
  @lines = <myFile>;
  close(myFile);

  # Put whole file into one scalar with newlines and tabs replaced by spaces,
  # comments removed and double quotes escaped:
  $" = " ";
  $_ = "@lines";
  s/\n/ /g;
  s/\t/ /g;
  s/\"/\"\"/g;
  s/<!--.*?-->//g;

  # extract issue information:
  $success = (s/.*?<strong>.*?<\/strong>.*?<hr>.*?<strong>.*?<\/strong>.*?<strong> *(.*?) *<\/strong>//i);
  $its_yer = $1;
  if ($success)
  {
    $success = (s/.*?<h2>.*?Issue (\d*), (.*?)<\/h2>//i);
    $issue_no = $1;
    $saved = $_;
    $date_string = convertDate($2);
    if ($date_string eq "0")
    {
      $success = 0;
    }
    $_ = $saved;
  }
  if ($success)
  {
    $success = (s/.*?<strong> *(.*?) *<\/strong>.*?<ul>.*?<\/ul>//i);
    $fp_footer = $1;
  }
  if ($success)
  {
    $success = (/.*?(<h2>.*?)<div align=\"\"left\"\">/i);
    $articles = $1;
  }
  if ($success)
  {
    $success = (/<div align=\"\"left\"\"> *(.*?) *<\/div>/i);
    $bp_footer = $1;
  }
  if ($success)
  {
    # write out line to issue file
    print issueFile "$issue_no\t\"$date_string\"\t\"$its_yer\"\t\"$fp_footer\"\t\"$bp_footer\"\n";
    # Process articles:
    $_ = $articles;
    $i = 0;

    while (s/(<h2>.*?)(<h2>|$)(.*)/$2$3/i)
    {
      $i++;
      $articles = $_;
      $_ = $1;
      if (/<h2>.*?<a name=\"\"(.*?)\"\"> *(.*?) *<\/a>*.?<\/h2> *(.*) *<a href=\"\"#top\"\">/i)
      {
        $name = $1;
        $title = $2;
        $body = $3;
      }
      elsif (/<h2> *(.*?) *<\/h2> *(.*) *<a href=\"\"#top\"\">/i)
      {
        $title = $1;
        $body = $2;
      }
      else
      {
        print "Failed (bad article $i)\n";
        print failedFile "$myFilename (bad article $i)\n"
      }
      print articleFile "$issue_no\t$i\t\"$name\"\t\"$title\"\t\"$body\"\n";
      $_ = $articles;
    }
  print "\n";
  }
  else
  {
    print "Failed\n";
    print failedFile "$myFilename\n";
  }
}

sub convertDate
{
  $_ = $_[0];
  if (/.*?(\d+).*?(january|february|march|april|may|june|july|august|september|october|november|december).*?(\d\d\d\d)/i)
  {
    $day=$1;
    $year=$3;
    $_ = $2;
    if (/january/i)
    {
      $month="01";
    }
    elsif (/february/i)
    {
      $month="02";
    }
    elsif (/march/i)
    {
      $month="03";
    }
    elsif (/april/i)
    {
      $month="04";
    }
    elsif (/may/i)
    {
      $month="05";
    }
    elsif (/june/i)
    {
      $month="06";
    }
    elsif (/july/i)
    {
      $month="07";
    }
    elsif (/august/i)
    {
      $month="08";
    }
    elsif (/september/i)
    {
      $month="09";
    }
    elsif (/october/i)
    {
      $month="10";
    }
    elsif (/november/i)
    {
      $month="11";
    }
    elsif (/december/i)
    {
      $month="12";
    }
    "$month/$day/$year";
  }
  else
  {
    "0";
  }
}
