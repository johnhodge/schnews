<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 747 - 11th November 2010 - Losing Their Faculties</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, students, direct action, austerity, financial crisis, tory, london" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 747 Articles: </b>
<b>
<p><a href="../archive/news7471.php">Losing Their Faculties</a></p>
</b>

<p><a href="../archive/news7472.php">Raven Mad For It</a></p>

<p><a href="../archive/news7473.php">Schmovies Dvd Launch</a></p>

<p><a href="../archive/news7474.php">Nukes? Gorle-blimey!</a></p>

<p><a href="../archive/news7475.php">Squat Crackdown, Bristol Fashion</a></p>

<p><a href="../archive/news7476.php">All Party'd Out</a></p>

<p><a href="../archive/news7477.php">Got The Hump Back</a></p>

<p><a href="../archive/news7478.php">Mexico: Day Of The 10,000 Dead</a></p>

<p><a href="../archive/news7479.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/747-student-riot-lg.jpg" target="_blank">
													<img src="../images/747-student-riot-sm.jpg" alt="Who's charing this meeting..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Thursday 11th November 2010 | Issue 747</b></p>

<p><a href="news747.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LOSING THEIR FACULTIES</h3>

<p>
<p>
	<strong>AS RAMPANT STUDENT PROTESTS LAUGH ALL THE WAY TO MILLBANK...</strong> </p>
<p>
	Finally, perhaps, the wave is cresting... The fightback against the vicious Tory cuts programme has so far been restricted to placard waving demos and petitions (<a href='../archive/news745.htm'>SchNEWS 745</a>). Not any more &ndash; the placards are still there but now they&rsquo;re being used to build bonfires in front of a windowless Tory HQ. And they say young people don&rsquo;t take an interest in politics. </p>
<p>
	The NUS &lsquo;Fund our Future&rsquo; demo - against Tory plans to extort &pound;9,000 per year of tuition fees out of students - kicked off big time on Wednesday (10th). The aggro caught the Met with their pants down: despite the initial guesses that 24,000 would show up the real figure was more than double that - leaving 225 coppers policing a demo of 50,000+ (Fewer cops than at the recent &lsquo;Hammertime&rsquo; Smash EDO demo - see <a href='../archive/news743.htm'>SchNEWS 743</a>). Not only were the numbers on the demo a total surprise but so was the militancy. Police are already admitting a major intelligence failure; they just didn&rsquo;t see it coming. </p>
<p>
	<strong>A CLEGG TO STAND ON</strong> </p>
<p>
	The demo met at Trafalgar Square, and after taking a frustratingly slow time to get a move on, the march filed past the Houses of Parliament with a chant of &lsquo;Nick Clegg, we know you, your a fucking Tory too&rsquo;. One participant told SchNEWS, &ldquo;<em>At first sight it looked like the normal crowd of students, socialists and trade unionists but the presence of an up-for-it anarchist block showed a little more potential</em>&rdquo;. </p>
<p>
	As soon as the demo got to Millbank Tower - the Conservative Party&rsquo;s headquarters - a group split off, ignoring the warnings of NUS and Union stewards. &ldquo;<em>This worked in our favour because it meant that those who ended up in front of Millbank were clearly the more up for it crowd. And there we were in front of Tory Party HQ - it&rsquo;s an obvious and inevitable target. People are going to find ways to express their anger</em>.&rdquo; </p>
<p>
	The frontrunners swiftly occupied the foyer letting off smoke bombs and catching the Tory faithful inside totally off-guard. Meanwhile those outside were burning effigies of Clegg and Cameron. The few police there formed a thin (blue) line to prevent anyone else from getting into the glass atrium, and were pelted with eggs and placard sticks. The crowd of around 2,000 b egan shouting to those inside to start breaking the windows. Soon glass was flying and entrances spontaneously opened, allowing the crowd to surge in. Cops stood by helpless. </p>
<p>
	With the initiative won the students went on the offensive, attacking coppers standing in their way, smashing any Tory property they could get their hands on as they ascended eight floors to occupy the roof of the Millbank Tower. The coppers below came under a barrage of missiles from all sides including one projectile fire extinguisher from the roof. Around two hundred people made it into the building. </p>
<p>
	As night fell the TSG cleared the area and by the end of the day more than 50 people had been nicked and 50 cops injured. The building was totally trashed, with damage allegedly running into the hundreds of thousands. The whole place was liberally festooned with graffiti. </p>
<p>
	Our student said &ldquo;<em>finally we see people fighting back against these cuts. Today was people telling the government that they can fuck their austerity - and pay for their own crisis. Twenty-seven grand for a degree is going to turn universities back into play areas for the rich. The NUS wanted a passive march today but what will that achieve? A-to-B marches and petitions aren&rsquo;t going to stop the government&rsquo;s plans. We need a proper uprising</em>.&rdquo; </p>
<p>
	<strong>SCHOOLBOY ERRORS</strong> </p>
<p>
	Most of those kicking off weren&rsquo;t your normal suspects and many were on their first demo. It seems many came unprepared and there are hundreds of photos of unmasked people smashing windows. The authorities have stated that they are determined to bring people to justice. A defence campaign has already started. Their advice to participants is currently: <br /> 
	&nbsp;You have the right to silence. This means you have the right not to incriminate yourself. So we recommend you do not say anything online or offline, on the phone or in person about the events. If the Police arrest you, interview you or put your name and photo in the newspaper, then call a good solicitor - in Central London, we recommend Bindmans 020 7833 4433 or Christian Khan 020 7631 9500. </p>
<p>
	If you were one of those arrested see <a href="http://nov10.wordpress.com" target="_blank">http://nov10.wordpress.com</a> or email: <a href="mailto:Remember10112010@gmail.com">Remember10112010@gmail.com</a> </p>
<p>
	* And remember folks, the police make no secret of actively monitoring Facebook and other sites and it can all be used as evidence in court. <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1004), 116); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1004);

				if (SHOWMESSAGES)	{

						addMessage(1004);
						echo getMessages(1004);
						echo showAddMessage(1004);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


