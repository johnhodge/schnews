<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 750 - 2nd December 2010 - SchNEWS 750: State of the Indignation</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 750 Articles: </b>
<p><a href="../archive/news7501.php">Fee Foes' Fine Fun</a></p>

<b>
<p><a href="../archive/news7502.php">Schnews 750: State Of The Indignation</a></p>
</b>

<p><a href="../archive/news7503.php">It's A Zad Day When...</a></p>

<p><a href="../archive/news7504.php">The Italian Jobless</a></p>

<p><a href="../archive/news7505.php">Ratcliffe 2o Trial Begins</a></p>

<p><a href="../archive/news7506.php">Town Hall Meeting</a></p>

<p><a href="../archive/news7507.php">Phil Yer Boots</a></p>

<p><a href="../archive/news7508.php">Edl Double Bill</a></p>

<p><a href="../archive/news7509.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 2nd December 2010 | Issue 750</b></p>

<p><a href="news750.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCHNEWS 750: STATE OF THE INDIGNATION</h3>

<p>
<p>
	&ldquo;<em>This is a moment to seize. The kaleidoscope has been shaken, the pieces are in flux, soon they will settle again. Before they do, let us re-order this world around us..</em>.&rdquo; <strong>stirring stuff from people&rsquo;s champion, Tony Bliar</strong> </p>
<p>
	It has been two years since SchNEWS last addressed the nation. In those two years the economic crisis has snowballed, taking down one country down after another and leaving the UK staring down the barrel of the gun of the Lib-Con cuts. Dominating both the news and the SchNEWS is the student rebellion over fees, cuts and the slashing of educational allowance. But it&rsquo;s not because were becoming some liberal rag that would crow with triumph merely because the Coalition were forced into an embarrassing policy climbdown. </p>
<p>
	The stakes for the &lsquo;protest movement&rsquo; / left / anarcho-radicalist free-thinking massive are way higher than whether students are forced to go into debt for &pound;3000 or &pound;9000 fees per year. This can all be but small potatoes when facing such monster spuds as severe civilisation threatening climate change and the ongoing sham of western free-market &lsquo;democracy&rsquo; as a practice (wikileaks anyone?), with its loaded dice, corporate corruption, war-mongering and exploitation. And of course you don&rsquo;t have to limit yourself to Western democracies in order to find elites oppressing, looting and lording it up left right and centre, while their &lsquo;subjects&rsquo; live in poverty and fear. </p>
<p>
	So it has been, more or less since the cosy post-WWII carve up of the world. But perhaps, although in ways different to those referred to by St Tony above, pieces are indeed in flux. <br /> 
	The aftershocks from the economic earthquake in 2008 have barely begun. The collapse of the global debt ponzi scheme has forced politicians to both slash public spending and curtail social support, whilst mortgaging the wealth of the young (and unborn) in order to prop up the system (well you can&rsquo;t expect the super-rich to cough up can you; they&rsquo;d just upsticks and move somewhere else...presumably to one of Jupiter&rsquo;s moons once this planet is totally screwed). </p>
<p>
	The economic tectonic plates are shifting. Europe is a busted flush &ndash; past and current bailouts mere sticking plasters failing to address fundamental structural flaws and imbalances likely to soon lead to a break up or major restructuring of the Euro. The US are now so in hoc to the Chinese they&rsquo;ll have to end up selling the rest of their assets to them to meet the vig. The West&rsquo;s elites will have to wake up to the fact that a new world order is forming &ndash; and they&rsquo;ve been demoted in the pecking (Peking?) order. </p>
<p>
	Domestically this is also a new era. Financially constrained and no longer able to bribe the electorate - with mostly borrowed cash &ndash; into worrying only about iphones and the x-factor, the politicians must be terrified of the masses discovering politics again, as they are exposed to much harsher economic realities than for 30 odd years. </p>
<p>
	And this is where the student revolt takes centre stage. They are the first real mass wave of discontent to surface here &ndash; showing both others, and the authorities, how powerful direct actions can be when done en masse. How this plays out may set the scene for much larger mobilisations of people in the coming months and years, should the rest of the populace finally come blinking out into the sunlight. </p>
<p>
	And this newly politicised generation, having realising their future under this system is a bleak one, and already willing to go much further than the usual grumbling and banner waving, are of course not going anywhere &ndash; they&rsquo;ll be here as the future unfolds, just older and poorer (and, perhaps, wiser). </p>
<p>
	But to turn discontent into real change needs well articulated and well thought out plans for what real change would look actually look like and how it would work. It&rsquo;s easy to be an &lsquo;anti&rsquo; and form loose coalitions to protest &ndash; but harder to convince others what they should be &lsquo;pro&rsquo; and how to enact it. </p>
<p>
	Time to get back to school... </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1026), 119); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1026);

				if (SHOWMESSAGES)	{

						addMessage(1026);
						echo getMessages(1026);
						echo showAddMessage(1026);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


