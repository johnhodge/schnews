<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 787 - 9th September 2011 - Tower to the People</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, london, racism, anti-fascists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 787 Articles: </b>
<b>
<p><a href="../archive/news7871.php">Tower To The People</a></p>
</b>

<p><a href="../archive/news7872.php">Crap Arrest Of The Week... Not</a></p>

<p><a href="../archive/news7873.php">Israel: Siege Mentality</a></p>

<p><a href="../archive/news7874.php">For Whom The Dale Tolls</a></p>

<p><a href="../archive/news7875.php">Name 'em And Shaman</a></p>

<p><a href="../archive/news7876.php">Another Throw Of The Dsei</a></p>

<p><a href="../archive/news7877.php">Calais Call Out</a></p>

<p><a href="../archive/news7878.php">Back To Shac</a></p>

<p><a href="../archive/news7879.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th September 2011 | Issue 787</b></p>

<p><a href="news787.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TOWER TO THE PEOPLE</h3>

<p>
<p>  
  	<strong>AS SCHEWS REPORTS BACK FROM THE ANTI-FASH FRONTLINE...</strong>  </p>  
   <p>  
  	<strong>It was going to be the &lsquo;Big One&rsquo;, the EDL&rsquo;s grand day out in the capital city.</strong> They proudly proclaimed that on 3rd September they were going into the &ldquo;Lion&rsquo;s Den&rdquo; the &ldquo;Islamic hell-hole&rdquo; &ndash; and what they no doubt had in mind when this hare-brained scheme was first announced was something like an Orange march through London&rsquo;s biggest Muslim community, with drums beating and the chant of&nbsp; &lsquo;E.E.EDL&rsquo; echoing off the walls of the East London Mosque as terrified Muslims fled. What actually happened was a half-arsed piss-up miles from Tower Hamlets. They predicted thousands would attend and only attracted hundreds. EDL Facebook posters have been reduced to claiming that Aldgate is, by some spurious definition, &lsquo;in Tower Hamlets&rsquo; which is the equivalent of&nbsp; trying to pass off a Calais booze cruise as a seaborne invasion of the European mainland.  </p>  
   <p>  
  	The Home Secretary intervened early and banned marches for thirty days in the six surrounding boroughs from September 2nd. This is old hat to the EDL, who until recently always had their marches banned and resorted to &lsquo;static demonstrations&rsquo; in car parks around the country. These static demos basically revolved around drunken EDLers trying with varying degrees of success to break out of their police cordon and go on the rampage.  </p>  
   <p>  
  	The ban seemed to come as bit of a surprise to the Unite against Fascism lot though &ndash; who had planned a march from Weaver&rsquo;s Field around Tower Hamlets. To their credit, despite calls from the authorities for anti-fascists to stay at home now that a ban had been implemented, they re-located their protest to within 200 yards of the East London Mosque (the EDL&rsquo;s stated target), meaning that there was at least a strong presence on Whitechapel High St.  </p>  
   <p>  
  	<strong>EDL.O.L</strong>  </p>  
   <p>  
  	In the run-up to the event it was clear that the EDL leadership were getting panicky about numbers, posting on their official Facebook page &ldquo;All the talking has been done leading up to today. Lets now see who walks the walk into tower hamlets for the ones who have been on the walls for weeks now saying cant wait for this one. well if you dont show you should hang your heads in shame.&rdquo;[sic].  </p>  
   <p>  
  	This seems to be a perennial problem for the League, they have thousands of wifi-warriors and facebook friends and for a demo in a safe zone like Luton they can turn out maybe three thousand on the streets, however anything more adventurous than that and their numbers start dropping into the hundreds. In fact Saturday&rsquo;s demo was the same sort of size as their last &lsquo;Big One&rsquo; in Bradford (see <a href='../archive/news737.htm'>SchNEWS 737</a>).  </p>  
   <p>  
  	From the start of the day it was clear the cops had mobilised huge numbers to keep the two sides apart. The recent riots (see <a href='../archive/news783.htm'>SchNEWS 783</a>) have left the Met with no wish to take chances. Their plan was to corral the EDL at Liverpool St Station and march them to within shouting distance of Tower Hamlets.  </p>  
   <p>  
  	But it was the Railways Union (RMT) who pulled off the biggest game-changer of the day. The rail workers refused to allow the EDL to travel by train, forcing them to change their plans repeatedly at short notice. Eventually they were forced to gather at King&rsquo;s Cross on totally the wrong side of the city.  </p>  
   <p>  
  	A representative of the RMT had this to say: &ldquo;The fascist EDL will be in London tomorrow morning. I have informed management that if they appear on any of our stations or trains, then there will be refusals to work on safety grounds of serious and imminent danger and stations will close. The RMT will not allow staff and the public to be put in danger by these thugs, who assaulted people on their last demo.&rdquo;  </p>  
   <p>  
  	When smaller groups of EDLers tried to get around on trains, some resourceful troublemakers stopped them in their tracks by pushing the emergency stop buttons on the trains. Eventually cops somehow got one train running and by 3pm had got the EDL to Moorgate tube.  </p>  
   <p>  
  	Spotters reported the crowd to be around the eight hundred mark. For reasons best known to himself EDL leader Tommy Robinson aka Stephen Yaxley-Lennon turned up to rally the drunken overweight patriots dressed as an Orthodox Rabbi (sadly not a rabbit as reported on Twitter).  </p>  
   <p>  
  	The EDL leader broke his bail to be there, so a disguise made sense, but mocking the religion of the state of Israel who they&rsquo;ve been trying to court is&nbsp; exactly the sort of boneheaded stupidity that makes the nationalist mob stand out.  </p>  
   <p>  
  	Meanwhile the anti-fascist presence in the East End was building up. With police concentrating on creating a sterile zone around the EDLs protest site numbers were gathering at the junction of Commercial Rd and Whitechapel High St from midday onwards. (for geography buffs this is actually the boundary of Tower Hamlets). The way ahead was totally blocked and small groups that went scouting for routes into the EDL pen were soon turned back.  </p>  
   <p>  
  	Eventually the bulk of the UAF counter demo made its way down to junction to stand with the local youth and the non-aligned anti-fascists, numbering over 2,000.  </p>  
   <p>  
  	An hour passed scuffling with the police (with a few de-arrests) and waiting for an EDL break-out that never came. Most only saw a few George Cross flags fluttering in the distance. Eventually the flags retreated and the day seemed to be over &ndash; a slightly anti-climactic no-score draw. Some groups of non-aligned anti-fash set off in the hope of encountering the league but were turned back by cops.  </p>  
   <p>  
  	<strong>DOUBLE-DECKED</strong>  </p>  
   <p>  
  	However by blind-chance (or possibly an over-reliance on Sat-Nav)&nbsp; locals were given the chance to go 1-0 in injury time. One EDL coach tried to make it past East London Mosque, with predictable results (see thoroughly amusing video at <a href="http://www.youtube.com/watch?v=AP9DTdIDn5A" target="_blank">www.youtube.com/watch?v=AP9DTdIDn5A</a>).  </p>  
   <p>  
  	The coach driver eventually sped off with hundreds of local youth and anti-fascists giving chase. Police vehicles raced alongside trying to head off the crowds and prevent a lynching. The coach was ambushed again in Mile End with rubble being hurled off a footbridge. Abandoning ship, the leaguers turned to the police for protection and were bundled onto a commandeered bus and promptly nicked for violent disorder. Unconfirmed reports of attacks on other coaches are circulating.  </p>  
   <p>  
  	Where now for the EDL? A dismal trudge around the country defending regional car-parks from the Islamic Caliphate? There are now splits aplenty as Tommy Robinson sets himself up as an unelected Fuhrer &ndash; with others being chased out of the movement or leaving to set up rival &lsquo;Infidels&rsquo; groups (North West, North East and counting). For more excellent analysis albeit with a slightly contrived Scottish accent check out <a href="http://malatesta32.wordpress.com." target="_blank">http://malatesta32.wordpress.com.</a> With the war in Afghanistan drawing to a close and Al-Qaeda on the back foot all over the Arab world their whole &lsquo;raison d&rsquo;etre&rsquo; seems to be withering away.  </p>  
   <p>  
  	Meanwhile judging by numbers on Saturday the autonomous anti-fascist movement does seem to be finally waking from its slumber and beginning to make its presence felt on the streets.  </p>  
   <p>  
  	<strong>TOMMY ROT</strong>  </p>  
   <p>  
  	Having pushed himself to the front of the EDL, Dear Leader Tommy Robinson seems to have developed a bit of a messiah complex. His Rabbi stunt was bound to end up with a re-arrest for breach of bail and considering that he vowed he&rsquo;d breach his bail conditions the moment he stepped out of court, remand was obviously on the table. Of course he&rsquo;s now trying to pass himself off as &lsquo;political prisoner&rsquo; by going on hunger strike and his loyal followers are even petitioning Amnesty International for recognition of his &lsquo;political prisoner&rsquo; status. Meanwhile in the SchNEWS office we&rsquo;re tucking into a falafel by way of solidarity.  </p>  
   <p>  
  	* <a href="http://www.brightonantifascists.wordpress.com" target="_blank">www.brightonantifascists.wordpress.com</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1347), 156); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1347);

				if (SHOWMESSAGES)	{

						addMessage(1347);
						echo getMessages(1347);
						echo showAddMessage(1347);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


