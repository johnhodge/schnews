<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 710 - 19th February 2010 - Rossport: Lobster Plot</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, shell, rossport, shell to sea, climate change" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 710 Articles: </b>
<p><a href="../archive/news7101.php">Gaza Defendants Hammered</a></p>

<p><a href="../archive/news7102.php">Block 'n' A.w.e  </a></p>

<b>
<p><a href="../archive/news7103.php">Rossport: Lobster Plot</a></p>
</b>

<p><a href="../archive/news7104.php">Calais: Hangar-ing On</a></p>

<p><a href="../archive/news7105.php">Never Ending Tory  </a></p>

<p><a href="../archive/news7106.php">Heckler Put Down </a></p>

<p><a href="../archive/news7107.php">Holloway To Go</a></p>

<p><a href="../archive/news7108.php">Sdl: Jocks-trap</a></p>

<p><a href="../archive/news7109.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th February 2010 | Issue 710</b></p>

<p><a href="news710.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROSSPORT: LOBSTER PLOT</h3>

<p>
Shell to Sea campaigner and fisherman Pat O Donnell, aka &lsquo;the chief&rsquo;, received a seven month  prison sentence last Thursday for breach of the peace and obstructing a Garda. Pat has consistently spoken out against the Corrib Gas Project and the collusion between Royal Dutch Shell and the Irish Government and has refused to be silenced despite violent attacks on him and his boat. <br />   <br /> Pat, who runs a shellfish company in Erris, refused to sign up to an agreement the previous year between Shell E&amp;P Ireland and the Erris Fishermen&rsquo;s Association over facilitating the laying of the offshore Corrib gas pipeline. Last June four armed and masked men sank his boat in Broadhaven Bay. Shortly after police arrested Pat and his son Jonathan - just before the arrival of Shell&rsquo;s pipelaying vessel, the Solitaire. They impounded both their boats but brought no charges against Pat and quickly dropped those against Jonathan.  Pat said, &ldquo;<em>All I am trying to do is protect my family and the seas that are our livelihood. My family has fished these waters for five generations - I have no authority to sell the rights to these waters</em>&rdquo;. <br />  <br /> Shell to Sea spokesperson Terence Conway said, &ldquo;<em>The jailing of Pat O Donnell shows selective prosecution on behalf of the Garda&iacute;. Pat O Donnell was picked out of a crowd of 60 people. The sentences are totally disproportionate to any alleged law-breaking and seem to be a punishment for opposing the Government&rsquo;s facilitation of Shell</em>&ldquo; <br /> &#8239; <br /> Pat will be spending the next 7 Months in Castlerea Prison. Letters of support can be sent to:  Pat O Donnell, Castlerea Prison, Harristown, Castlerea, Co Roscommon Ireland. <br />   <br /> Alongside Pat, 20 other campaigners against the Corrib Gas Project were also up in court. Five were sentenced, receiving fines, a suspended sentence and community service. <br />    <br /> Opposition to the exploitation of Ireland&rsquo;s waters, however, continues. Last weekend saw the first Rossport Solidarity UK gathering. About 40 people got together to plan and discuss the campaign to stop Shell&rsquo;s experimental gas pipeline in North West Ireland. <br />     <br /> On the agenda is a bike ride to the summer Rossport gathering travelling from Merthyr Tydfil in Wales to Rossport in North West Ireland. The bike ride aims to link up communities who are resisting fossil fuel extraction in the UK and Ireland and also internationally while raising awareness of the campaigns in Myther Tydvil and Rossport. It will end in Rossport for the summer gathering. If you are interested in joining the bike ride email: <a href="mailto:climatechains@aktivix.org">climatechains@aktivix.org</a> <br />   <br /> There will also be a Rossport gathering on the end of May bank holiday weekend. This will be an opportunity to visit the area, find out what will be happening this year and find out how you can become part of the campaign. People are needed to come before the gathering and help set up camp.  <br />  <br /> *To get involved email: <a href="mailto:rossportsolidaritycamp@gmail.com">rossportsolidaritycamp@gmail.com</a> <br />   <br /> *see <a href="http://www.shelltosea.com/" target="_blank">www.shelltosea.com/</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=710">climate change</a>, <a href="../keywordSearch/?keyword=rossport&source=710">rossport</a>, <a href="../keywordSearch/?keyword=shell&source=710">shell</a>, <a href="../keywordSearch/?keyword=shell+to+sea&source=710">shell to sea</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(662);
			
				if (SHOWMESSAGES)	{
				
						addMessage(662);
						echo getMessages(662);
						echo showAddMessage(662);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


