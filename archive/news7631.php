<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 763 - 18th March 2011 - Nuclear Power: Going Ciritical</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, nuclear energy, environment, climate change" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 763 Articles: </b>
<b>
<p><a href="../archive/news7631.php">Nuclear Power: Going Ciritical</a></p>
</b>

<p><a href="../archive/news7632.php">Clegg-stravaganza</a></p>

<p><a href="../archive/news7633.php">Dale Farm: 28 Days Later</a></p>

<p><a href="../archive/news7634.php">Corrie Anniversary</a></p>

<p><a href="../archive/news7635.php">Charity Begins Abroad</a></p>

<p><a href="../archive/news7636.php">Shopping Stewards</a></p>

<p><a href="../archive/news7637.php">Schnews Benefit</a></p>

<p><a href="../archive/news7638.php">Berk-ing Mad</a></p>

<p><a href="../archive/news7639.php">What Comes A Mound</a></p>

<p><a href="../archive/news76310.php">Losing His Wragg</a></p>

<p><a href="../archive/news76311.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th March 2011 | Issue 763</b></p>

<p><a href="news763.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NUCLEAR POWER: GOING CIRITICAL</h3>

<p>
<p>  
  	<strong>AS SCHNEWS WEIGHS UP THE FALL OUT FROM JAPAN&rsquo;S ATOMIC DISASTER</strong>  </p>  
   <p>  
  	The terrifying situation in Japan has rekindled the debate over nuclear power around the world. While in the US, Obama has reaffirmed his support for nuclear power to protect the massive investment his administration has made in the industry, over in Germany and Switzerland the governments have jammed the brakes on plans to build and replace nuclear plants. In the UK, David Cameron has already declared his intention to push on with plans to expand the UKs nuclear capacity, with up to eleven new power stations.  </p>  
   <p>  
  	Government and the nuclear industry have been close since the Blair government announced plans to build a new generation of nuclear power plants in 2006. In 2007, those plans were ruled &ldquo;unlawful&rdquo; by the High Court, who labelled the public consultation &ldquo;misleading, seriously flawed, manifestly inadequate and procedurally unfair&rdquo;. And yet despite the change in government we&rsquo;re still firmly set on the nuclear path.  </p>  
   <p>  
  	The final not-so-green flag was waved for the nuclear industry to start building in 2008, amid controversies over &ldquo;nuclear cronyism&rdquo; due to family links between leading industry figures, Gordon Brown (see <a href='../archive/news618.htm'>SchNEWS 618</a>) and Planning Minister Yvette Cooper. Since then, the government (is there any point distinguishing between red, and blue and yellow striped here?) has been carrying out &ldquo;facilitative actions&rdquo; to speed their progress.  </p>  
   <p>  
  	This wheel-greasing involved launching the Office for Nuclear Development, with its mission to &ldquo;[make] the UK the best market in the world for companies to invest in nuclear&rdquo;. In the first five months of the department&rsquo;s life, three senior civil servants were revealed to have been wined and dined a whopping 30 times in super swanky London restaurants by the industry.  </p>  
   <p>  
  	The government has further eased the way for the plutonium-to-profit businesses by &ldquo;facilitating&rdquo; in areas like planning, licensing and design. The industry is also still receiving financial support despite the initial pledge to ensure &ldquo;the full cost of new nuclear waste is paid by the market&rdquo;. Instead there will be a &ldquo;fixed unit price&rdquo; for waste disposal - a cap on the price of waste disposal at the time the plant is approved. Any additional or rise in costs will be met by the taxpayer.  </p>  
   <p>  
  	In 2008, the government commissioned a shake up of nuclear regulation, partly as an update, but mostly to sweep away outdated &lsquo;red tape&rsquo; and clear the way the for a new generation of fully private reactors. They brought in top KPMG consultant Dr Tim Stone, well known as a nuclear cheerleader, and a man with fingers in many PFIs. Stone duly reported and now all the previous responsibilities of the Nuclear Directorate, the Department for Transport&rsquo;s Radioactive Materials Transport Team and most of the Health and Safety Executive&rsquo;s role are being bundled together into a much more business friendly &lsquo;Office of Nuclear Regulation&rsquo; (ONR). a fact that the Nuclear Industry Association &lsquo;warmly welcomes&rsquo;.The ONR will be a separate legal entity and run by a largely non-exec board &lsquo;with relevant experience&rsquo;, presumably drawn then from the business world and nuclear industry (fox,henhouse?). At least we&rsquo;ll all know who to blame and sue after the next disaster, eh.  </p>  
   <p>  
  	<strong>CORPORATE ENRICHMENT</strong>  </p>  
   <p>  
  	Events in Japan may have momentarily derailed the nuclear PR train but if the reactor cores are cooled successfully (or even if not) the all-out push for more nuclear will carry on as before. That&rsquo;s because governments around the world are commited to a technology which is fuelled primarily by over optimism.  </p>  
   <p>  
  	This rose-tinted vision pretends that such highly complex machines will never fail (even aircraft crash occasionally but reactor failures are somewhat more costly). Of course after an aircraft has crashed (or been scrapped) you don&rsquo;t have to worry about it any more but, as we are seeing in Japan, the cooling ponds of spent fuel rods (which sit around long after the reactors are shut down) are just as dangerous as the reactors themselves (e.g. Kyshtym disaster in 1957).  </p>  
   <p>  
  	Given the new era of budget cuts, unstable political conditions and the increased probability of natural disasters (e.g. extreme weather due to climate change) it seems doubtful that the nuclear power will be getting any safer in the near future. The nuclear industry already has a long history of falsifying safety reports, covering up incidents and wangling extensions to the operation of reactors well past their designed lifetimes (as was the case for Reactor-1 at Fukashima recently).  </p>  
   <p>  
  	Then there&rsquo;s the issue of waste disposal for which no realistic solution exists. Large amounts of deadly high level waste - which will need close supervision for decades and will remain highly dangerous for thousands of years - have already been produced and a new generation of nuclear reactors would only add to this legacy. Currently the plan is that we stash it all in barrels of concrete and hide it in a big hole in the ground. Putting a pillow over your face and humming loudly will be optional.  </p>  
   <p>  
  	And yet even mainstream greenies (you know who you are) jumped on the radioactive bandwagon, hoping the nuclear genie would grant them the wish of a carbon-free energy future.  </p>  
   <p>  
  	Yes on the face of it, at the point of generation - the reaction itself - nuclear energy is carbon-free. However this completely ignores the nature of the whole nuclear power cycle. Once uranium mining, processing, transportation, power station construction and decommissioning - all of which require vast amounts of hydro-carbons - are taken into account then nuclear starts to come in only slightly below oil and coal based power stations in terms of emissions.  </p>  
   <p>  
  	And there&rsquo;s another whopping problem - peak uranium. Those slightly lower carbon emission figures per kilowatt hour are for power stations running on the highest quality uranium ore. But the world only has a limited amount of high quality uranium ore &ndash; maybe not more than 50 years&rsquo; worth at current consumption rates, and if there is a big global increase in nuclear power then the problem becomes as acute as peak oil. Mining lower quality ore will increase carbon emissions because it is more difficult to extract, thus requiring more energy.  </p>  
   <p>  
  	<strong>FIGHT THE POWER (STATION)</strong>  </p>  
   <p>  
  	UK activists are seizing the moment of nuclear&rsquo;s international notoriety to hammer home the point, with group &lsquo;Kick Nuclear&rsquo; saying, &ldquo;<em>Over the last 60 years, the nuclear industry has proved time and again that it is incapable of controlling such a dangerous &ndash; and expensive &ndash; form of energy</em>&rdquo;  </p>  
   <p>  
  	* This Sunday, 20th March, Kick Nuclear will support a vigil organised by CND outside Downing Street, London, opposing new nuclear builds in the U.K  </p>  
   <p>  
  	* A national rally against new nuclear build will take place outside the gates of Sizewell nuclear power station in Suffolk, England on Saturday 23rd April.  </p>  
   <p>  
  	* A weekend protest camp is to be held on the beach in front of the Sizewell plant from 22nd-25th April, organised by members of the Stop Nuclear Power Network. The camp will coincide with the 25th anniversary of the Chernobyl nuclear disaster.  </p>  
   <p>  
  	* Kick Nuclear is reinforcing its campaign for a national boycott of EDF Energy. The French state-owned energy giant is spearheading the push for new nuclear build in the UK, and elsewhere. See <a href="&#8239;http://boycottedf.org.uk" target="_blank">&#8239;http://boycottedf.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1142), 132); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1142);

				if (SHOWMESSAGES)	{

						addMessage(1142);
						echo getMessages(1142);
						echo showAddMessage(1142);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


