<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 780 - 22nd July 2011 - Calais Catering Callout</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, calais, no borders, migrants" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 780 Articles: </b>
<p><a href="../archive/news7801.php">Let Them Eat Lead</a></p>

<p><a href="../archive/news7802.php">Float Some And Jet Some</a></p>

<b>
<p><a href="../archive/news7803.php">Calais Catering Callout</a></p>
</b>

<p><a href="../archive/news7804.php">Schnews In Brief 780</a></p>

<p><a href="../archive/news7805.php">Taken For Ride</a></p>

<p><a href="../archive/news7806.php">Rip Lucero Aguilar</a></p>

<p><a href="../archive/news7807.php">Brazil Nuts</a></p>

<p><a href="../archive/news7808.php">Good Will Huntington</a></p>

<p><a href="../archive/news7809.php">Unhampered</a></p>

<p><a href="../archive/news78010.php">Waywards Heath</a></p>

<p><a href="../archive/news78011.php">Endure A Cell</a></p>

<p><a href="../archive/news78012.php">Ratcliffe Hanger</a></p>

<p><a href="../archive/news78013.php">Pompey & Circumstance</a></p>

<p><a href="../archive/news78014.php">Pie In The Sky</a></p>

<p><a href="../archive/news78015.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd July 2011 | Issue 780</b></p>

<p><a href="news780.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CALAIS CATERING CALLOUT</h3>

<p>
<p>  
  	Calais NO Borders activists are now having to shoulder the burden of food distribution in addition to their usual tasks of monitoring the police and helping migrants. Normally, two Calais based charities Belle Etoile and SALAM provide daily food distro to refugees. However, both groups are taking a break over July and August meaning the task has fallen to No Borders. They have coordinated a grass-roots activist catering collective operation, visiting Calais on a rota to provide material support to the &lsquo;sans-papiers&rsquo; community.  </p>  
   <p>  
  	The situation for migrants in Calais grows ever harder as the screws are tightened - SchNEWS spoke to one No Borders activist: &ldquo;After the eviction of Africa House (see <a href='../archive/news778.htm'>SchNEWS 778</a>), migrants started trying to sleep inside the food compound area - this was viciously evicted so some ended up in a makeshift camp on the railway sidings near the river.&rdquo; This was where No Borders started the food distro, but the &lsquo;River Camp&rsquo; was evicted a few days later, with the now familiar methods of tear gas and seizure of migrants&rsquo; property. There is a co-ordinated policy of psychological warfare against migrants in the Calais area, with arrests, harassment and seizure of shelters all leading to continual sleep deprivation for the sans-papiers groups.  </p>  
   <p>  
  	Police have also tried to drive a wedge between No Borders activists and migrants, threatening migrants with more raids if they believe that activists are present. The cops have also been using whistles (No Borders preferred way of letting migrants know about police presence) and conducting raids specifically to look for activists.  </p>  
   <p>  
  	Currently No Borders are running a food distro in the evenings on a pedestrianised railway bridge - &lsquo;The police watch us, but this is a very high visibility area so they seem reluctant to intervene. We have a lot of migrants who can come and cook with us now - so it seems like less of a hand-out&rsquo;.  </p>  
   <p>  
  	Calais Migrant Solidarity have issued a call out for food, tents and tarpaulins. If you can make it to Calais or just want to help, call 0033 631 869 878.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1288), 149); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1288);

				if (SHOWMESSAGES)	{

						addMessage(1288);
						echo getMessages(1288);
						echo showAddMessage(1288);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


