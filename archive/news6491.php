<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 649 - 3rd October 2008 - Debts The Way To Do It</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, credit crunch, henry paulson, anti-capitalism, debt, meltdown monday, banks, economy, bail out" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../schmovies/index-on-the-verge.htm"><img 
						src="../images_main/on-the-verge-banner-tor.jpg" 
						width="465px" 
						height="90px" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 649 Articles: </b><b><p><a href="../archive/news6491.php">Debts The Way To Do It</a></p> </b><p><a href="../archive/news6492.php">Cop Arrest Of The Week</a></p> <p><a href="../archive/news6493.php">Producing The Goods</a></p> <p><a href="../archive/news6494.php">Unru-ly</a></p> <p><a href="../archive/news6495.php">Dawn Of The Dread</a></p> <p><a href="../archive/news6496.php">Fur Flying</a></p> <p><a href="../archive/news6497.php">...and Finally...</a></p>  
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/649-banker-lg.jpg" target="_blank">
													<img src="../images/649-banker-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 3rd October 2008 | Issue 649</b></p>

<p><a href="news649.htm">Back to the Full Issue</a></p>

<h3>DEBTS THE WAY TO DO IT</h3>

<div id="article1">
<p><b>AS SCHNEWS RASHLY TAKES ON THE CREDIT CRUNCH AND THE FUTURE OF GLOBAL CAPITALISM</b></p>
<p><i>&quot;Whoever is to blame for this week's scenes on world stockmarkets, only the most churlish anarchist would welcome them.&quot;</i> - <b>the Guardian, 1st October 2008</b></p>
<p>Blimey... you spend 15 years struggling against global capitalism and then the bloody thing collapses of its own accord. Building societies, banks and all manner of financial institutions are going to the wall.. City wide-boys, hands bloody from their ruthless assault on the world's poor, are flinging themselves in front of trains - and nobody's had to lift a finger - let alone throw a Molotov.</p>
<p>Since our report on the welfare state for business (<a href="news647.htm">SchNEWS 647</a>) western governments have continued throwing infeasibly large amounts of money at the free-falling financial markets.</p>
<p>'Meltdown Monday' was only the start (Traumatic Tuesday, Woeful Wednesday etc) as here in the UK, Bradford and Bungly went belly up and had to be nationalised - well it's massive debts did anyway with Spanish bank Santender, already owners of Abbey, encouraged to pick up the best bits of B&B for a song.  Halifax nearly collapsed and had to be sold to Lloyd's bank - forget the monopoly issues just keep the sinking ships afloat! </p>
<p>Over in the States, Bush and his cronies are desperately trying to get through a whopping $700 billion bail out bill to shore up confidence in a financial system teetering on the edge.  They failed initially, leading to further market plummets before persuading Congress to approve a revised deal this week (being voted on by the House of Representatives today).  </p>
<p>The nattily named credit crunch appears to be getting more and more bite, so what's it really all about? </p>
<p>The explanation tossed around by most mainstream media tells us that it's due a rash 'sub-prime' mortgage lending - OK, but if you want to understood why it's knock on effects are so threatening to the system it's actually a little more complicated. </p>
<p>The comparative economic boom (ridden with such self-congratulation by the 'golden' chancellor at the time...er, a Mr G. Brown) since the last recession in the early 90's has been based on massively increasing levels of debt. Not just individual consumers spending their way to prosperity on credit cards, but banks, all the other types of financial institutions, corporations and  governments.</p>
<p>Household debt has increased from 50% of GDP in 1980 to 100% in 2007.  Financial sector borrowing has gone from 21% to 116% of assets in the same period. In fact, a chief cheerleader of the brave new financial world was the former boss of now bust Goldman Sachs - one Henry Paulson. He took them  from $20 billion in debts in 1999 to $100 billion when he left. Having helped cause the crisis, and getting rich off it, he's now the man putting forward the bail out plan as US Treasury Secretary. Despite self-imposed limits, Governments have also ramped up their debt levels - achieved by privatising everything in sight and putting all the deals 'off balance sheet' (thanks, Gordon!)  </p>
<p>So lenders now routinely now lend out more than the total assets of the company.  It was all made possible by massive deregulation, the completion of the project started in the Thatcher / Reagan free market era, as big business and their lobbies finally succeeding in getting politicians completely in their pocket, and indeed direct pay. Light touch regulation gave way to feather light. </p>
<p>The confidence of banks to throw ever more cash around was underpinned by the invention of the Credit Default Swap (CDS) market. This allows organisations exposing themselves by loaning out money to buy a kind of insurance against a default on that loan. In return for paying small regular premiums, priced depending on the perceived risk of default, that organisation could think of itself as no longer exposed to any risk, able to reduce any provisions put aside in case of default (so called 'bad debt') - and therefore free to lend out even more cash. </p>
<p>A culture of risky, unsound lending was thus created. To make things worse, all these debt contracts are traded, and indeed speculated on. They change hands multiple times as different people estimate their current value and risk differently. A tasty profit opportunity for canny get-rich-quick investors, but difficult for buyers removed from the original business to assess what they'd really bought. </p>
<p>In effect this all meant that many billions of debt could be considered assumed by people only having to put up in hard cash a tiny percentage of that figure. No problem as long as house prices, shares, bond prices etc all kept rising and more debt could be given out cheaply and easily to anyone who might otherwise be close to default. A debt mountain was gradually accumulated. In 2008, the amount of debt in the CDS market is estimated to be more than $50 trillion. That's over twice the value of the entire US stock market. </p>
<p>Confidence started to collapse as the risks of sub-prime default got reassessed and foreclosure and bankruptcy rates started to climb. Banks panicked and realised they were caught in a kind of pyramid scheme. If people started defaulting in numbers nobody would have enough cash to pay out. The availability of cheap CDS contracts dried up and banks refused to lend to each other, wary that anyone of them could go under at any time.  The cost of servicing the CDS exposures lept up, to the point where banks like Goldman Sachs and Bradford & Bingly couldn't afford them and, unable to just borrow more to cover it, went swiftly bust.  As credit availability goes down, the levels of debt exposure now threaten to bring down all types of companies, wrecking the economy from all sides at once. </p>
<p>Facing meltdown, governments have been forced to step in to avert complete collapse of the system. But it won't work in the long term as they're effectively giving a blood transfusion to a badly haemorrhaging patient. The bail outs may buy some more time - gambling taxpayers money for years to come on a high risk strategy financed through yet more debt (China and India have been helping by buying up US govt bonds, leading some to wonder whether this will see a further shift in the balance of economic power, but it's all interconnected baby!) - but the fundamental flaws of capitalism will remain and bleed everyone dry in the end. In fact, the hand outs will just ensure that it's the same old elite who will get richer as the system creaks on to it's inevitable demise -  it's just a question of how long (end of the world in 2012 anyone?). </p>

<h3>BACK IN THE REAL WORLD</h3>
<p>In the meantime, what will it all mean for the average SchNEWS reader in the street? What's gonna happen next?! If you're poor, lacking large debts, a mortgage, share portfolio and high paying job, you might even enjoy the ride.</p>
<p>If the credit crunch triggers a full-blown recession we're going to see a surge in  repossessions of houses. Squatters paradise! The number of endless yuppie flat developments and ego-driven showpiece towers will plummet. Less 4x4s, less sports cars. The consumer slowdown will be good for the environment - economic collapse is the only realistic way of reaching those carbon emission targets!  On the down side there'll be less food available for looting from skips as bargain hunting shoppers clear out the aisles, but local food production will have to increase. </p>
<p>As the job queues swell, access to social services will become less punitive. When you're one of three million as opposed to one of 300,000, there's only so much hassle at the dole office to go round.</p>
<p>The wave of depression should throw up new political opportunities. For a long time in the developed west, the majority of the opposition to capitalism was essentially moral. Fair trade and charity was thought good enough to stave off the guilt of being disproportionately wealthy. But as the spoils of globalisation become increasingly only available to a smaller and smaller elite, interest in alternative ways of doing things should also increase. </p>
<p>Recent events have shown capitalism is a hothouse flower - it has to exist swaddled in a life-support system of regulations and laws protecting private property, allowing corporations to exist. Most importantly it requires the state to be a lender of last resort. </p>
<p>Despite the endless free market rhetoric we've been forced to swallow since the Thatcher era, the government has always functioned as a welfare state for the rich.  This life support system has been filtering the real wealth upwards in society for years but now it's all out on the open as the bankers stretch out their begging bowls.</p>
<p>It's now been demonstrated to all and sundry (who'd previously not been reading SchNEWS) that the 'free' market is no such thing. Pundits might spew about 'irresponsible' lending and try to pin the blame on a few bad apples but in fact all the markets were doing is what markets are supposed to do - chase after the largest amount of profit in a single-minded ruthless way - and human beings are just a minor obstacle in that pursuit.  </p>
<p>Perhaps as times get tougher, people might finally get it together to demand  more fundamental changes - and not leave the super rich in charge of it. </p>
</div>

		</div>
		
		
		
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>