<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 705 - 15th January 2010 - Frisky Business</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police, civil liberties" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 705 Articles: </b>
<p><a href="../archive/news7051.php">Wild At Heart</a></p>

<p><a href="../archive/news7052.php">Ships In The Fight</a></p>

<p><a href="../archive/news7053.php">Pyramid Schemers</a></p>

<p><a href="../archive/news7054.php">Gaza Arrests Witness Call Out</a></p>

<p><a href="../archive/news7055.php">Funky Gibbons</a></p>

<b>
<p><a href="../archive/news7056.php">Frisky Business</a></p>
</b>

<p><a href="../archive/news7057.php">Xmas-size Attack</a></p>

<p><a href="../archive/news7058.php">Bough Down</a></p>

<p><a href="../archive/news7059.php">Cat On A Squat Tin Roof</a></p>

<p><a href="../archive/news70510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th January 2010 | Issue 705</b></p>

<p><a href="news705.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FRISKY BUSINESS</h3>

<p>
Of the 16,761 people stopped and searched under section 44 counter terrorism laws in 2002/03, 18 were arrested in connection with terrorism, while 359 were arrested for &lsquo;other reasons&rsquo;. Of the remaining 16,384, two, journalist Pennie Quinton and activist Kevin Gillan, took their complaints against the police to the highest level. <br />  <br /> Seven years later and the European Court of Human Rights has ruled that not only were the two unlawfully abused as they made their way to a demonstration at the Excel centre arms fair but also that section 44&rsquo;s random stop and search powers were too widely drawn and lacked adequate parliamentary and legal safeguards against abuse. In short, illegal. <br />   <br /> The crucial fact about Section 44 is that allows the police to conduct stop and search without &lsquo;reasonable suspicion&rsquo;. It has been used regularly against protesters. The whole of Greater London has been designated a Section 44 zone for years. <br />  <br /> While Quinton and Gillan were awarded &pound;30,400 each, Home Secretary Alan Johnson was left to bleat about how the powers were an &ldquo;important tool&rdquo; in fighting terrorism. Ever the sore loser, Johnson announced he would take the unusual step of appealing to the court&rsquo;s &lsquo;grand chamber&rsquo; of 17 judges, which requires him to show &lsquo;exceptional circumstances&rsquo;. Johnson said until the appeal is heard the police powers would remain in place. <br />         <br /> Since the powers were introduced in 2000 their use has exploded, with 117,000 recorded searches in 2008. Anyone that has attended a demo, taken &lsquo;too many&rsquo; photos, looked a bit shady or not white or just looked at a copper funny knows the police have used these powers arbitrarily and will continue to do so. <br />  <br /> * For more info about your rights see <a href="http://www.freebeagles.org" target="_blank">www.freebeagles.org</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=civil+liberties&source=705">civil liberties</a>, <a href="../keywordSearch/?keyword=police&source=705">police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(621);
			
				if (SHOWMESSAGES)	{
				
						addMessage(621);
						echo getMessages(621);
						echo showAddMessage(621);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


