<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 733 - 30th July 2010 - EDO: Lend me Your Smears</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, edo decommissioners, israel" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 733 Articles: </b>
<p><a href="../archive/news7331.php">Robot Wars</a></p>

<p><a href="../archive/news7332.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7333.php">Greece: Fuel On The Fire</a></p>

<b>
<p><a href="../archive/news7334.php">Edo: Lend Me Your Smears</a></p>
</b>

<p><a href="../archive/news7335.php">Rossport: Not Bored Yet</a></p>

<p><a href="../archive/news7336.php">Ian Tomlinson Protests</a></p>

<p><a href="../archive/news7337.php">Earth First! Gathering</a></p>

<p><a href="../archive/news7338.php">Radical Media Roundup</a></p>

<p><a href="../archive/news7339.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th July 2010 | Issue 733</b></p>

<p><a href="news733.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDO: LEND ME YOUR SMEARS</h3>

<p>
<p>
	After the historic victory of the EDO Decommissioners earlier this month (see <a href='../archive/news729.htm'>SchNEWS 729</a>), the judge in the case, George Bathurst-Norman, has become the target of a concerted smear campaign by a number of right-wing columnists. The Zionist Federation and the Board of Deputies of British Jews have both claimed that his summing up of the case was &lsquo;anti-semitic&rsquo;. </p>
<p>
	As a result of organised lobbying by Zionist groups, the Office of Judicial Complaints (OJC) has opened an investigation into his handling of the case. The inquiry is at an early stage and it&rsquo;s likely the already retired judge will receive nothing more than a ticking off, but the news has been spread via news outlets, like the Daily Mail, who were remarkably uninterested in the original verdict. </p>
<p>
	SchNEWS couldn&rsquo;t care less what happens to Bathurst-Norman, who in his day was a fearsome hander down of harsh sentences - he once sent down Paul Kelleher for three months for knocking the head off a statue of Maggie Thatcher (see <a href='../archive/news394.htm'>SchNEWS 394</a>). But the fact is that this smear campaign is really aimed at the defendants and anyone else who wants to run a similar defence. </p>
<p>
	Smash EDO told SchNEWS &ldquo;<em>The charge of anti-semitism is a cynical attempt to undermine the achievement of the Decommissioners. There is nothing anti-semitic in putting evidence of Israeli war crimes to a jury</em>.&rdquo; </p>
<p>
	Helpfully the Jewish Chronicle has stuck up the full transcript of the summing up here for you to make up your own mind. <a href="http://www.hejc.com/35771/judge-bathurst-norman-full-summing" target="_blank">www.hejc.com/35771/judge-bathurst-norman-full-summing</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(884), 102); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(884);

				if (SHOWMESSAGES)	{

						addMessage(884);
						echo getMessages(884);
						echo showAddMessage(884);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


