<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - Turkey Shoot</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, palestine, gaza, aid convoy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<b>
<p><a href="../archive/news7275.php">Turkey Shoot</a></p>
</b>

<p><a href="../archive/news7276.php">The Itt Crowd</a></p>

<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>

<p><a href="../archive/news7278.php">Hoto Finish</a></p>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TURKEY SHOOT</h3>

<p>
<p>
	Making a token gesture to an outraged international community, Israel launched its own investigation into the circumstances surrounding the killing by Israeli commandos of nine Turkish peace activists, aid workers and journalists on board the Mavi Marmara (see <a href='../archive/news726.htm'>SchNEWS 726</a>). </p>
<p>
	True to form, the Turkel inquiry will neither investigate the soldiers&rsquo; actions nor question the politicians responsible. Not only that, but the youngest member of the three man committee is 75. Yaakov Tirkel, the young-un of the panel, doesn&rsquo;t believe that there should be an inquiry at all (quoted in Israeli army radio). 86-year-old former General Amos Horev, will no doubt provide a fair and impartial opinion on his own soldiers. The other member, Shabbtai Rozen, is the very definition of elder statesman, weighing in at an impressive 93 years young. As the Israeli news site Ynet put it, the inquest will need &lsquo;strong cups of tea and a loud bell&rsquo; to function. </p>
<p>
	There are two international observers to monitor the inquiry. One of these is our very own David Trimble (former Northern Ireland First Minister), who joined the &lsquo;Friends of Israel Initiative&rsquo; the day after the attack on the Mavi Mamara. The other observer is a Canadian general who helped ship Canadian citizens to be tortured in Guantanamo and then tried to cover it up. </p>
<p>
	Israeli-Palestinian MP Haneen Zoubi is facing a witchhunt for her decision to sail to Gaza with the flotilla. Israel&rsquo;s interior minister has called for her citizenship to be revoked, as part of Israeli ultra-right&rsquo;s policy of &lsquo;no citizenship without loyalty.&rsquo; Currently she&rsquo;s been assigned bodyguards due to death threats against her. </p>
<p>
	Activist pressure from around the world is beginning to have some effect on Gaza&rsquo;s besieged status. Israel has finally agreed to allow all of the the 10,000 tons of aid aboard the flotilla in to Gaza, under UN supervision. There&rsquo;s also talk of Israel easing some of the blockade&rsquo;s restrictions (Tony Blair&rsquo;s unpleasant mush can be spotted in these negotiations). But these token gestures are beside the point. As the (normally unconfrontational) Red Cross pointed out on Tuesday - it&rsquo;s not a question of allowing some more goods in; the blockade itself is an illegal act of collective punishment, and the Palestinians wouldn&rsquo;t need humanitarian aid if they were allowed to import and export stuff the same way everyone else is. </p>
<p>
	Finally after the fog of misinformation has cleared, the truth about the attack on the Mavi Mamara is coming to light. Culturesofresistance.org has published dramatic and revealing footage smuggled out from the ship - see <a href="http://www.culturesofresistance.org/gaza-freedom-flotilla" target="_blank">www.culturesofresistance.org/gaza-freedom-flotilla</a> <br /> 
	 <br /> 
	For eyewitness accounts check out <a href="http://www.counterpunch.org/lindorff06162010.html" target="_blank">www.counterpunch.org/lindorff06162010.html</a> <a href="http://blogs.aljazeera.net/middle-east/2010/06/06/kidnapped-israel-forsaken-britain" target="_blank">http://blogs.aljazeera.net/middle-east/2010/06/06/kidnapped-israel-forsaken-britain</a> </p>
<p>
	Undaunted, more aid ships are planning to sail to Gaza again next month - see <a href="http://www.gazaflotilla.org" target="_blank">www.gazaflotilla.org</a> &amp; <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(821), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(821);

				if (SHOWMESSAGES)	{

						addMessage(821);
						echo getMessages(821);
						echo showAddMessage(821);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


