<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 774 - 3rd June 2011 - Hasta La Democracia</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, spain, austerity, mass demonstrations" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 774 Articles: </b>
<p><a href="../archive/news7741.php">Going To Hell-as</a></p>

<p><a href="../archive/news7742.php">Hinkley: Gone Frission</a></p>

<p><a href="../archive/news7743.php">Not Playing Workfare</a></p>

<p><a href="../archive/news7744.php">Basildon Brush Off</a></p>

<p><a href="../archive/news7745.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news7746.php">Hasta La Democracia</a></p>
</b>

<p><a href="../archive/news7747.php">Smooth Operators</a></p>

<p><a href="../archive/news7748.php">Gaza Freedom Flotilla Ii</a></p>

<p><a href="../archive/news7749.php">Ukba-rmy</a></p>

<p><a href="../archive/news77410.php">Schnews In Graphic Detail Out Now</a></p>

<p><a href="../archive/news77411.php">Gm: Sooner Or Tater</a></p>

<p><a href="../archive/news77412.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd June 2011 | Issue 774</b></p>

<p><a href="news774.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HASTA LA DEMOCRACIA</h3>

<p>
<p>  
  	Two dramatic turns in the Spanish revolution took place this week with five demonstrators arrested in Valencia after protests turned violent, while in Madrid activists decided to dismantle the camp in Puerta del Sol.  </p>  
   <p>  
  	Hundreds gathered in Valencia on Wednesday (8th) outside the regional parliament offices to protest against political corruption. Francisco Camps, the now re-elected mayor of the region, is currently under investigation for accepting bribes, along with several other Popular Party politicians. Police attempted to break up the crowd of demonstrators on Thursday morning, resulting in 12 activists being injured, along with eight police officers. Five campaigners were arrested for public disorder and assault, and are still in custody as SchNEWS goes to press.  </p>  
   <p>  
  	Wednesday saw large gatherings of protesters in many cities throughout Spain, including the Madrid square which has attracted thousands of &lsquo;los indignados&rsquo; since its beginning on May 15th. After lengthy discussions at the camp on Wednesday evening, it was agreed that the movement would &lsquo;restructure&rsquo; by dismantling the camp and focusing efforts on more localised, smaller groups. Nationwide protests outside town halls have been called for on Saturday (11th), as mayors are sworn into office across the country.  </p>  
   <p>  
  	Sunday (19th) has also been set as a date for Europe-wide demonstrations to protest against the signing of the controversial Euro Plus Pact, a list of political reforms supposedly intended to improve the fiscal strength of the country. The agreement has come under fire for austerity measures imposed across the EU such as abolishing wage indexation (wage indexation is the linking of wages with inflation - if this is abolished it allows for wage decreases &ndash; supposedly &lsquo;increasing competitiveness&rsquo;) and the raising of pension ages. Heads of state in the EU are gathering to sign the agreement on June 27th.  </p>  
   <p>  
  	A global protest against political corruption and false democracy has been announced for October 15th.  </p>  
   <p>  
  	Most of the smaller solidarity camps that sprung up across the UK and Europe are also deciding to dismantle mass encampments in favour of more localised demonstrations, but as a Spanish camp spokesperson said, &ldquo;A restructuring does not mean we are dissipating&rdquo;. It&rsquo;s now up to those who took to the streets over the last month to keep the momentum going.  </p>  
   <p>  
  	* See <a href="http://www.democraciarealya.es" target="_blank">www.democraciarealya.es</a> (in Spanish)  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1240), 143); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1240);

				if (SHOWMESSAGES)	{

						addMessage(1240);
						echo getMessages(1240);
						echo showAddMessage(1240);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


