<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 670 - 27th March 2009 - Met Yer Match!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, met police, london, public order" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.g-20meltdown.org/"><img 
						src="../images_main/g20-meltdown-banner.jpg"
						alt="G20 - Meltdown in the City"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 670 Articles: </b>
<p><a href="../archive/news6701.php">Apocolypse Wow</a></p>

<b>
<p><a href="../archive/news6702.php">Met Yer Match!</a></p>
</b>

<p><a href="../archive/news6703.php">Luton Vanguard</a></p>

<p><a href="../archive/news6704.php">--- In The Brown Stuff ---</a></p>

<p><a href="../archive/news6705.php">Right Shred Fred</a></p>

<p><a href="../archive/news6706.php">Edo Inside Schnews</a></p>

<p><a href="../archive/news6707.php">Huon Cry</a></p>

<p><a href="../archive/news6708.php">Famous Fife</a></p>

<p><a href="../archive/news6709.php">Saabotage</a></p>

<p><a href="../archive/news67010.php">Raven Mad</a></p>

<p><a href="../archive/news67011.php">Well Acquitted</a></p>

<p><a href="../archive/news67012.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 27th March 2009 | Issue 670</b></p>

<p><a href="news670.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MET YER MATCH!</h3>

<p>
<span style="font-size:13px; font-weight:bold">YOUR CUT-OUT-AND-KEEP GUIDE TO SURVIVING THE SUMMER OF RAGE</span> <br />
 <br />
<b>Police have promised an overwhelming response to protests in the City of London this week.</b> <br />
 <br />
Here&#8217;s yer ten point guide to keeping the Met on their toes. Their primary tactic is likely to be an attempt to &#8216;kettle&#8217; the crowd &#8211; i.e. surround them either in a pre-prepared space with crash barriers, vans and/or lines of cops. Once the crowd is contained they will only be released once searched and photographed. The best time to beat this tactic is as the cordon is being prepared. <br />
 <br />
1) Stick together &#8211; in small groups (preferably affinity groups agreed on beforehand) <br />
 <br />
2) Be prepared to react quickly &#8211; get through police lines as they are forming &#8211; cops don&#8217;t like being outflanked and may abandon the attempt to form a kettle. <br />
 <br />
3) Avoid surveillance &#8211; UK police are notorious for poring over hours of footage to pick out faces and get convictions. Stop police from filming by putting placards in front of the cameras (A stock tactic of FITwatch &#8211; see <a href="http://www.fitwatch.blogspot.com" target="_blank">www.fitwatch.blogspot.com</a>). Caps, hooded tops and masks are all worth using. A determined crowd can stop a mass de-masking &#8211; saving prison sentences later. <br />
 <br />
4) Baton charges &#8211; are used in an attempt to make people run or force them into pre-prepared cordons. A football shin pad under the sleeve of yer jacket can take a lot of the sting out of a truncheon blow. <br />
 <br />
5) Pepper Spray &#8211; now used increasingly. If affected wipe away from eyes and through hair with dry rag, followed by water to flush eyes. <br />
 <br />
6) Horses &#8211; a charge by mounted police is one of the most severe tactics deployed by British police. One tactic that has worked is the stringing of lines between lamp-posts about 7 feet off the ground. <br />
 <br />
7) Snatch Squads &#8211; Police will attempt to invade a static crowd to grab individuals. Keep an eye on senior cops (often wearing different coloured hi-vis jackets) pointing people out. Warn those people and defeat the snatch squad by surrounding it. <br />
 <br />
8) De-arresting &#8211; many people have survived an attempted nicking when their mates have grabbed them and held on tightly. <br />
 <br />
9) Fighting with the cops is possible &#8211; if there&#8217;s a supply of throwables to hand &#8211; otherwise they&#8217;re of course better armed than us. <br />
 <br />
10) The best form of defence of all is CHAOS! The police have a hierarchy which takes orders from individuals making informed decisions. If the situation changes constantly they simply cannot keep up. Keep moving all the time, weave in and out of the crowd. Change your appearance. Open up new directions and possibilities, be unpredictable. If you find yourself still and passive for more than a minute then you&#8217;ve stopped acting defensively. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:50%; align:left" align="left">&nbsp;</div> <br />
 <br />
<b><i>If you're stopped and searched you don't have to give your name and address. If you're unlucky enough to get nicked then saying 'no comment' is always the best policy. For more see the SchNEWS guide <a href="../diyguide/defendantsguidetoarrest.htm" target="_blank">www.schnews.org.uk/diyguide/defendantsguidetoarrest.htm</a> <br />
</i></b>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=670">direct action</a>, <a href="../keywordSearch/?keyword=london&source=670">london</a>, <span style='color:#777777; font-size:10px'>met police</span>, <span style='color:#777777; font-size:10px'>public order</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(258);
			
				if (SHOWMESSAGES)	{
				
						addMessage(258);
						echo getMessages(258);
						echo showAddMessage(258);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>