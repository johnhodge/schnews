<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 724 - 28th May 2010 - Pan Handled</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police racism, police brutality" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 724 Articles: </b>
<p><a href="../archive/news7241.php">Flight Brigade</a></p>

<p><a href="../archive/news7242.php">Schmovies In Brief</a></p>

<p><a href="../archive/news7243.php">Cops Circle The Square</a></p>

<p><a href="../archive/news7244.php">War-sore</a></p>

<b>
<p><a href="../archive/news7245.php">Pan Handled</a></p>
</b>

<p><a href="../archive/news7246.php">H&k: A Kirk In The Teeth</a></p>

<p><a href="../archive/news7247.php">Homes Alone</a></p>

<p><a href="../archive/news7248.php">Bp: Block In The Pipeline</a></p>

<p><a href="../archive/news7249.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th May 2010 | Issue 724</b></p>

<p><a href="news724.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PAN HANDLED</h3>

<p>
We only recently became aware of the following, but because we believe that flagging up stories of police racism - swiftly followed by police brutality - should never go unpublished, we decided to run with it anyway.  <br />  <br /> Samuel, a steel pan musician, having busked in London for ten years, was having a regular busk at the South Kensington tube station on September 29th last year when two police came up and told him to move on. Being savvy of the busking laws, he asked if there had been a complaint, which cops said there had, so he agreed to leave and began packing up. When one policeman asked him if he had a permit, and Samuel replied (correctly) that the local council for that area didn&rsquo;t issue permits, things spiralled downwards...  <br />  <br /> The policeman got arsey because Samuel, now engaged in packing up his instruments, appeared to be ignoring the snide remarks coming from the copper, who told him &ldquo;Stop, I&rsquo;m speaking to you&rdquo;.&nbsp; To which he replied &ldquo;what&rsquo;s wrong, little man?&rdquo; (apparently the copper was short). The PC said &ldquo;if you say &lsquo;little man&rsquo; one more time, you&rsquo;re nicked&rdquo;. He did. He was then forcibly handcuffed, his steel drums getting trashed in the process, and CS was sprayed into his face. He was pushed face-down onto the pavement and assaulted as he vomited from ingesting the spray. More police arrived on the scene.  <br />  <br /> While on the ground, one copper put his knee into Samuel&rsquo;s back and whispered, &ldquo;You fucking black c**t, you black c**t&rdquo;, and other police joined in the racial abuse as they stuck him in a van. Once at the station, Samuel asked which one had uttered these words to him, and all the officers looked at one another saying &lsquo;not me&rsquo;. Police claim there is no CCTV footage of the incident &ndash; yeah right, this is a central London tube station. He spent a whole night in a Notting Hill police station cell. He has facial scarring from the violent arrest.  <br />  <br /> Samuel was found guilty at West London Court on two counts of assaulting an officer, one count of resisting arrest, and on April 1st was sentenced to 70 hours of community service.  <br />  <br /> An appeal will only now be allowed if witnesses come forward or CCTV footage is found &ndash; if you happened to see this vile racist attack happen, contact us and we will pass the info on.  <br />  <br /> * See <a href="http://www.facebook.com/group.php?gid=177276456245&amp;ref=ts" target="_blank">www.facebook.com/group.php?gid=177276456245&amp;ref=ts</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=police+brutality&source=724">police brutality</a>, <span style='color:#777777; font-size:10px'>police racism</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(796);

				if (SHOWMESSAGES)	{

						addMessage(796);
						echo getMessages(796);
						echo showAddMessage(796);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


