<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 740 - 24th September 2010 - All Mex'd Up</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, autonomous communities, oaxaca" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 740 Articles: </b>
<p><a href="../archive/news7401.php">Brussels Sprouts Camp</a></p>

<p><a href="../archive/news7402.php">Rewrite His-tory</a></p>

<b>
<p><a href="../archive/news7403.php">All Mex'd Up</a></p>
</b>

<p><a href="../archive/news7404.php">Running Ham-mock</a></p>

<p><a href="../archive/news7405.php">Stockholme Syndrome</a></p>

<p><a href="../archive/news7406.php">Good Chap On Trial</a></p>

<p><a href="../archive/news7407.php">Schnews In Brief</a></p>

<p><a href="../archive/news7408.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 24th September 2010 | Issue 740</b></p>

<p><a href="news740.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ALL MEX'D UP</h3>

<p>
<p>
	The besieged Mexican autonomous community San Juan Copala has been evacuated after an outbreak of paramilitary violence left five seriously injured and one dead. </p>
<p>
	Around 500 gunmen took over San Juan Copala&rsquo;s town hall last Monday (13th). According to reports from the community, they spent the days that followed raking the town with constant gunfire, as well as going from house to house beating residents and burning the homes of those who had already fled the violence. </p>
<p>
	Last Friday (17th), the paramilitaries issued a statement threatening to massacre the 50 families that had stayed in the town if they didn&rsquo;t leave immediately. All but two of the families have now managed to escape. The two families left are now completely surrounded by gunmen. </p>
<p>
	In the town square of the city of Oaxaca, women and children, camping there in protest since being forced out of their homes in August, have now gone on hunger strike in an attempt to pressure the government to send police in to evacuate the remaining families. </p>
<p>
	The community declared itself autonomous in January 2007 following the Oaxaca uprising which nearly drove the governor out of office (see <a href='../archive/news567.htm'>SchNEWS 567</a>). It has been under siege since February when paramilitaries cut off water and electricity and blockaded the town. Earlier this year, attempts to break the siege by international and Mexican activists ended violently when the paramilitaries opened fire on the caravans &ndash; in one incidence, killing two (see <a href='../archive/news726.htm'>SchNEWS 726</a>). </p>
<p>
	Community members have claimed governor Ulises Ruiz Ortiz is behind the two paramilitary groups, Movement for Triqui Unification and Struggle (MULT) and the Union for the Social Well-being of the Triqui Region (UBISORT), who are responsible for around 20 deaths and numerous beatings, rapes and disappearances. </p>
<p>
	*For more see <a href="http://www.glasgowchiapassolidarity.wordpress.com" target="_blank">www.glasgowchiapassolidarity.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(946), 109); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(946);

				if (SHOWMESSAGES)	{

						addMessage(946);
						echo getMessages(946);
						echo showAddMessage(946);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


