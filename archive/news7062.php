<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 706 - 22nd January 2010 - Mobs And Coppers</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edo, gaza, sussex police, smash edo" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 706 Articles: </b>
<p><a href="../archive/news7061.php">Haiti: The Aftershock Doctrine</a></p>

<b>
<p><a href="../archive/news7062.php">Mobs And Coppers</a></p>
</b>

<p><a href="../archive/news7063.php">Lanark'y In The Uk</a></p>

<p><a href="../archive/news7064.php">Rent A'sunderland</a></p>

<p><a href="../archive/news7065.php">Stoke'ing The Fires</a></p>

<p><a href="../archive/news7066.php">Girls With Latitude   </a></p>

<p><a href="../archive/news7067.php">Inside Schnews</a></p>

<p><a href="../archive/news7068.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd January 2010 | Issue 706</b></p>

<p><a href="news706.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MOBS AND COPPERS</h3>

<p>
Monday&rsquo;s <strong>Smash EDO</strong> demo forced the Brighton bomb makers to shut up shop after surrounding the factory with the <strong>Remember Gaza</strong> funeral procession. <br />  <br /> The protest, commemorating the victims of the Israeli massacre in Gaza a year ago, began in Wild Park, close to the Home Farm Road site. As hundreds of protesters gathered the police attempted to issue Section 14 notices in an attempt to impose conditions on the demo. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/706--gaza-horse-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/706--gaza-horse-sm.jpg"  /></a></td></tr></table>In a memorial to the 1400+ Palestinians killed, around 300 black-clad protesters carried coffins and banners towards the factory. At the bottom of the road the protest split, with most of the crowd scrambling up the bank into the woods behind in an attempt to make it to the factory cross country. The rest remained at the bottom of the road, which was blocked off by lines of police vans, where they read the names of the victims of Israeli aggression. The back of EDO was surrounded, as mounted police, and dogs guarded the fence. With the police beating protesters with batons, some managed to get into the back of a nearby bakers through a wire fence, only to be met by more of the baton-brigade, causing a tactical withdrawal into the safety of the woods. The police weren&rsquo;t risking any re-run of the Shut ITT demo in October 2008 (see <a href='../archive/news651.htm'>SchNEWS 651</a>) when protesters managed to redecorate the back of the factory in shades of red, and despite the ingenuity of those seeking redress for the Gaza atrocities, the police presence was too overwhelming on this occasion to make it onto site. <br />  <br /> The crowd regrouped on the main road, with several requiring medical attention. The demo then moved off into town, peacefully but purposefully. At several points police attempted to block off the road but the protesters surged round the sides of police lines. <br />  <br /> The procession was briefly kettled on Lewes Road, before police allowed it to progress to The Level park. The police announced the march could go no further but protesters pushed on into town. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/706--gaza-demo-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/706--gaza-demo-sm.jpg"  /></a></td></tr></table>As the crowd began to disperse into the streets, police locked down the city centre, closing off roads even when there was no sign of marchers. Around 50 peaceful protesters were kettled in the narrow streets of the North Laines for around an hour as evening shoppers looked on. A roaming group of four marchers carrying a Remember Gaza banner tried to give a stray copper a taste of his own medicine by using the banner to kettle him against a building but the humourless plod made his escape by tearing it down.    <br /> Leading up to the demo police had been ramping up the tension, staking out pre-demo meetings and following and harassing known activists. On Sunday night, a local squat was raided by armed police on spurious burglary grounds. One man was held until the demo was over, then released without charge. <br />  <br /> Coverage from the local rag The Argus once again sidestepped the real story focussing instead on the &lsquo;hijacking&rsquo; of the city centre and the chaos and disruption of a single day&rsquo;s dissent. The expression of outrage for the presence of activists in the bohemian shopping resort and for non-compliance with the police before the event fails to see or ask the obvious. As a spokesperson for Smash EDO rightly pointed out: where is the outrage for the devastating loss of human life in Gaza? Where is the support for the legal right to protest? Why would activists campaigning against the production of arms in their own town ever consider complying with the political policing of an event designed to silence all dissent? Wake up Brighton: while bomb makers remain at large in our town we should be thankful that there are those who have something to say about it. <br />   <br /> *See smashedo.org.uk <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=edo&source=706">edo</a>, <a href="../keywordSearch/?keyword=gaza&source=706">gaza</a>, <a href="../keywordSearch/?keyword=smash+edo&source=706">smash edo</a>, <a href="../keywordSearch/?keyword=sussex+police&source=706">sussex police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(627);
			
				if (SHOWMESSAGES)	{
				
						addMessage(627);
						echo getMessages(627);
						echo showAddMessage(627);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


