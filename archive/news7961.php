<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 796 - 11th November 2011 - Nov 9 Summed Up</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 796 Articles: </b>
<b>
<p><a href="../archive/news7961.php">Nov 9 Summed Up</a></p>
</b>

<p><a href="../archive/news7962.php">High Sieze</a></p>

<p><a href="../archive/news7963.php">Having A Field Day</a></p>

<p><a href="../archive/news7964.php">Remember, Remember</a></p>

<p><a href="../archive/news7965.php">Tour Of Duty</a></p>

<p><a href="../archive/news7966.php">Maison D'etre</a></p>

<p><a href="../archive/news7967.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7968.php">R.i.p. Mark Rivers</a></p>

<p><a href="../archive/news7969.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/796-cops-in-tents1-lg.jpg" target="_blank">
													<img src="../images/796-cops-in-tents1-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th November 2011 | Issue 796</b></p>

<p><a href="news796.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOV 9 SUMMED UP</h3>

<p>
<p>  
  	<strong>AS &lsquo;TOTAL&rsquo; POLICING ENTERS STUDENT ANTI-CUTS DEMO EQUATION...</strong>  </p>  
   <p>  
  	Wednesday (9th) saw yet another demonstration against cuts to education and public services, this time organised by the National Campaign Against Fees &amp; Cuts (NCAFC). So SchNEWS donned its best black hoody and attempted to infiltrate the &lsquo;violent minority&rsquo;. Much to our disappointment, by the end of the day, it was clear that the monopoly of violence was still very much in the hands of the state.  </p>  
   <p>  
  	A mixed bag of around 10,000 students, socialists, liberals, workers, unemployed and black block (not necessarily mutually exclusive) left University of London, Mallet St at 12pm. Armed with fiddles, tambourines, drums, sound systems and an extensive collection of placards including &ldquo;Creed, not Greed&rdquo;, &ldquo;Tax the wealthy 1%&rdquo;, &ldquo;I wish my boyfriend was as dirty as your policies&rdquo; and &ldquo;Dumbledore wouldn&rsquo;t stand for this&rdquo;.  </p>  
   <p>  
  	Three helicopters flew overhead as the assembly got ready to rally. The demo shuffled, zombie-like through the streets of London, with progress severely restricted by a heavy police presence. Effectively the cops had decided to place the whole demonstration in a mobile kettle, with lines of police marching alongside; every side street was barricaded with police horses dictating the pace at the front and a further 15 riot vans, 6 police cars and a dog van bringing up the rear. Overall more than 4,000 cops (nearly 1 for every 2 protesters) were deployed. No one was allowed to join the demo and you certainly couldn&rsquo;t leave &ndash; &lsquo;Total Policing&rsquo; was the Met&rsquo;s new buzzword for this particular form of oppressive action.  </p>  
   <p>  
  	<a href="../images/796-Nov9-undercover-lg.jpg" target="_blank"><img style="margin-right: 10px; border: 2px solid black"  align="left"  alt=""  src="http://www.schnews.org.uk/images/796-Nov9-undercover-sm.jpg"  /> </a>Friendly bobbies handed out leaflets explaining the new commissioner&rsquo;s latest brainwave and how all this visible policing was just there to help &ldquo;facilitate peaceful protest&rdquo;. Unfortunately it failed to mention the less visible undercovers (who were deployed en-masse), nor did it reference the threatening letters they had sent to people arrested in previous demos (regardless of whether or not their charges were dropped) and police threats to deploy rubber bullets &ndash; which had been issued earlier in the week. Fortunately the threats failed to dampen protesters&rsquo; spirits and chants of &ldquo;You can shove your rubber bullets up your arse!&rdquo; were heard throughout the day.  </p>  
   <p>  
  	The use of large numbers of (barely) undercover police was probably the most noticeable deviation from past tactics. Pairs of fairly obvious plain clothed officers walked along with the crowd looking for potential &lsquo;trouble-makers&rsquo;; once they found someone they didn&rsquo;t like the look of they&rsquo;d single them out for a snatch squad to drag away. Numerous photos of this particularly nasty &lsquo;violent minority&rsquo;, along with pictures of cops continuing to hide their ID numbers and masking-up, are starting to surface.  </p>  
   <p>  
  	The actions of the police meant there was very little contact between the protesters and the general public. The police couldn&rsquo;t prevent construction workers from showing solidarity however and any time the march passed construction sites there was an evident unity and affinity between the builders who cheered the crowd on&nbsp; - which responded in kind with &ldquo;Students and workers unite and strike!&rdquo;  </p>  
   <p>  
  	A few bankers also got a glimpse of what&rsquo;s in store for them in the near future as the angry mob passed through the city. Cries of &ldquo;Tory Scum&rdquo; and &ldquo;Banks Got Bailed Out, We Got Sold Out&rdquo; were directed towards a number of financial institutions. As cops attempted to delay the march at the end of New Fetter Lane, Lloyds staff decided unwisely to bait the crowd from the upper floors of Thavies Inn House, at one point even throwing objects from the windows. A well aimed potato sailed in the opposite direction, taking out a window on its way; staff then beat a hasty retreat.  </p>  
   <p>  
  	By Fleet Street, a call travelled from the front of the protest right to the back informing people that 700 electricians were trying to join the demo but were stopped by a police barricade. The sparks have been on strike over pay cuts and had already marched to parliament. The painfully slow progress of the student demo slowed to a halt as attempts were made to join the two blocks together, but these came to nought, so the demo carried on towards Holborn.  </p>  
   <p>  
  	The fact that the electricians couldn&rsquo;t join a demonstration when they hadn&rsquo;t committed any offences to warrant any exclusion may not come as a surprise to regular SchNEWS reader types, but the only positive aspect of yet another example of police tyranny will be that it will make Joe Bloggs and John Doe wake up to hard faced police tactics. Sounds of &ldquo;Why are they doing that?&rdquo; travelled through the ether as injustice hit them in the face.  </p>  
   <p>  
  	Meanwhile, the portable teapot got piloted as far as Moorgate by a route that avoided all the hot spots such as Parliament and the St Paul&rsquo;s occupy camp. Billy Bragg entertained the St Paul&rsquo;s campers with ole favourite trade union ditties.  </p>  
   <p>  
  	At the finish line the protesters were released in small manageable groups. Many were fairly keen to leave by this point, with most of the enthusiasm and energy successfully drained by the police tactics. Others headed down to the Finsbury Sq occupation, while some stayed at the junction of Moorgate and London Wall to dance to the assorted sound systems.  </p>  
   <p>  
  	Throughout the day 60 arrests were made, though only 20 of those actually spent the night in the cells. 3 for violent disorder and affray, 3 for going equipped to cause criminal damage (including our hero with a pen &ndash; see Crap Arrest), 1 for breaching S60AA (refusing to take off a mask), 1 for possession of an offensive weapon and 12 for breach of the peace.  </p>  
   <p>  
  	One of the less successful actions of the day was the attempted occupation of Trafalgar Sq, which consisted of 30 pop-up tents pitching up, followed shortly after by the arrest of their occupants and the removal of the tents. London cabbies had a bit more luck as areas of central London were blockaded in the afternoon by a swarm of black cabs that were supporting the taxi union protest, with the aptly named &lsquo;United Cabbies Group&rsquo;. Around 4,000 cabs descended on different parts of London from 4pm, Trafalgar Sq, Pall Mall, Aldwych, Fleet St, Shaftesbury Avenue and Victoria St had all been on the menu.  </p>  
   <p>  
  	In recent times there has been a global trend for waves of protests and demonstrations that are slowly joined by all sorts &ndash; not just the usual suspects. People across the board are showing their discontent and experiencing police brutality for the first time. It is not just your black bloc, leftists, brew crew, jobless.... these days the discontented come in all shapes and sizes, from all backgrounds, and many workplaces. Builders, nurses, electricians and Harry Potter fans to name but a few are joining the students, anarchists and seasoned activists in the fight.  </p>  
   <p>  
  	So even though the police seemed in control in London it is worth considering that many people there are not experienced in the ways of demonstrations, riots and confronting police. The more exposed they become to police brutality and the reality of who the police really serve the more they may react. Here endeth the lesson. Ah, men.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1420), 165); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1420);

				if (SHOWMESSAGES)	{

						addMessage(1420);
						echo getMessages(1420);
						echo showAddMessage(1420);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


