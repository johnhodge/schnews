<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - Inter-NETCU</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police, netcu, internet, indymedia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>
</b>

<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>

<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>

<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<p><a href="../archive/news75511.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/755-mark-kennedy-lg.jpg" target="_blank">
													<img src="../images/755-mark-kennedy-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>INTER-NETCU</h3>

<p>
<p>
	<strong>AS GOVERNMENT AGENCY CAUGHT INFILTRATING ACTIVIST MEDIA OUTLET</strong> </p>
<p>
	&ldquo;<em><strong>No - stuff that - SHUT the place: Let&rsquo;s not all stand around like lemmings - lets shut the place!Bring ladders and wire cutters. If there are enough of us we can shut it!</strong></em>&rdquo; - a pretty average comment on Indymedia you might think - if a little gung-ho. In fact it was posted there by the police - and SchNEWS has the proof. </p>
<p>
	For the benefit of anyone who&rsquo;s been hiding in a hole wearing a tinfoil hat for the last fortnight (i.e most of our readership), it turns out that the U.K direct action/anarchist/environmental movement was infiltrated for number of years by undercover police. At least four cops have already been outed and its safe to assume there may be more. But while the mainstream media has focussed on the sleazy antics and dodgy love lives of these professional liars, SchNEWS can reveal that police attempts to disrupt our movement goes much further than a few unshaven plants in grubby t-shirts, and includes attacks on activist media and communications. For years now, police have been using activist website Indymedia in attempts to sow mistrust, demoralise movements and to incite violence and illegality. </p>
<p>
	<strong>IP FREELY</strong> </p>
<p>
	Police postings came to light following an internal investigation on persistent disinformation being published to Indymedia UK. Technically Indymedia is supposed to safeguard it&rsquo;s posters by not logging IP addresses. In actual fact there are IP filters. Although IP addresses are only stored temporarily, those of persistent abusers are kept in order to prevent the site from being overwhelmed. Moderators of Indymedia UK identified the Gateway-303 server as being the source of numerous such posts. A filter was set up to capture the behaviour of the individual(s) who were hiding behind the server. </p>
<p>
	One IP address so identified was 62.25.109.196 , which correlates with the server gateway303.energis.gsi.gov.uk. There are similar servers, gateway-301 &amp; gateway-302 with IP addresses 62.25.109.194 and 62.25.109.195 respectively. Other servers identified are gateway-101, gateway-202, gateway-201, etc </p>
<p>
	GSI stands for Government Secure Intranet. It is a network established by the UK Government to allow secure transfer of files across its computers. The Police National Network is separate from it, but can connect to it. Currently GSI is operated by Energis, a UK based internet company now owned by Cable &amp; Wireless. </p>
<p>
	It is clear from the consistency of the usage of gateway-303 server that the IPs are probably assigned to particular premises or else specific units within the UK Government. One of the purposes of the GSI network is to provide a secure proxy network behind which users can maintain their anonymity. Hence the lack of solid information as to exactly who is behind the postings. However, SchNEWS is gonna take a stab in the dark (if only) as to who they are; in fact some actually signed NETCU. Of course it could be the old double bluff, but given the level of intelligence behind some of the postings even this level of sophistication seems unlikely. </p>
<p>
	<strong>INDY-SCRIBABLE</strong> </p>
<p>
	Indymedia is a key tool in activist organisation and communication. Given the overtly political nature of the site and the types of news stories published it is hardly a surprise that it attracts weirdos with strange axes to grind. Its open publishing format has left it wide open to trolling, spam and, of course, state / police abuse. </p>
<p>
	This abuse has mostly taken the form of posting comments under a variety of pseudonyms. </p>
<p>
	These postings have targeted individual activists, put out information about activists that is not in the public domain, attacked campaigns, and urged the disruption of peaceful protests. Many comment on (then) ongoing court cases. Some are just personal abuse directed at well-known activists, while others contain information only known to the police. Two postings - made on 21st Jan and 9th Jun 2010 - provide personal details, including mobile numbers, of potential targets for the animal rights movement, one a fur-shop in Leeds, the other the owner of an animal circus. What the motivations were for posting these details can only be surmised. But off the record we reckon it&rsquo;s blatant attempt at entrapment. </p>
<p>
	As part of the activist scene, SchNEWS isn&rsquo;t quite as shocked as elements of the mainstream media have been about the cops&rsquo; sleazy abuse of personal friendships to disrupt the organisation of alternative social movements. It&rsquo;s long been the case that to be an activist you&rsquo;ve got to get used the idea of surveillance (both covert and overt), violence from the state and the deliberate silencing of voices. The antics of Kennedy, Watson, Jacobs and Boyling are just part of a tactical bag of tricks including kettling, the use of F.I.T teams and spotter cards, anti-stalking injunctions, spreading rumours in the mainstream press and a constant stream of new laws criminalising dissent (See SchNEWS 1-754). To that list we can now add infiltration of the core platform for anti-authoritarian radical media activism in the UK. </p>
<p>
	From a brief analysis of the activities of the undercovers it seems that their mission was as much to disrupt as much as it was to gather information. Attacks on our movements by disruption are a handy weapon in the state&rsquo;s arsenal with no need for actual evidence or convictions. </p>
<p>
	<strong>PLAINLY INCITE</strong> </p>
<p>
	The targets of the fake Indymedia postings span the activist spectrum, from anti-fascists to anti-militarists, but have been most concerned with environmental campaigns and, in particular the Animal Rights movement. On April 27th, 2010, a comment appeared on the Indymedia UK newswire entitled Don&rsquo;t use SPEAK as a model. The comment, on an article entitled New animal lab at Leicester; New nationwide campaign to start urged readers to respond to the campaign by &ldquo;Model[ling] the campaign on a successful AR campaign such as Hillgrove cats or Darnley Oaks etc&rdquo; (sic). Those familiar with those campaigns will be aware that they campaigns are alleged to have included violent actions against individuals. Other postings referenced a what appears to be an entirely fictitious new AR campaign in Leicester &ndash; Stop Leicester Animal Cruelty. SLAC (ha bloody ha) </p>
<p>
	It should be remembered corporations seeking injunctions under the Protection from Harassment Act (link) have relied heavily on comments made on Indymedia as evidence. This act has been used against campaigns such as Smash EDO to criminalise all forms of dissent targeting specific companies, even entirely peaceful and completely legal protest. </p>
<p>
	As a whole the comments build a bewildering picture of a campaign of disparagement, incitement and support, together with wry asides and outright attempts at demoralisation. A few really stand out as obvious incitement to illegality while some are so bizarre that we can&rsquo;t work out if they are part of a sinister master plan to bring down the whole activist scene - or just the ramblings of bored coppers with faces full of doughnuts and nothing better to do. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1066), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1066);

				if (SHOWMESSAGES)	{

						addMessage(1066);
						echo getMessages(1066);
						echo showAddMessage(1066);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


