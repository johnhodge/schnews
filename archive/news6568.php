<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 656 - 21st November 2008 - 3 Reichs And Yer Out</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bnp, fascism, british national party, nick griffin, blackpool" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://edinantimil.livejournal.com/"><img 
						src="../images_main/anti-war-gathering-banner.png" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 656 Articles: </b>
<p><a href="../archive/news6561.php">Minga The Merciless</a></p>

<p><a href="../archive/news6562.php">Witness Appeal Of The Week</a></p>

<p><a href="../archive/news6563.php">So Solidarity Crew</a></p>

<p><a href="../archive/news6564.php">Fruity Number</a></p>

<p><a href="../archive/news6565.php">Snap Crackle Cop</a></p>

<p><a href="../archive/news6566.php">Rail Against The Machine</a></p>

<p><a href="../archive/news6567.php">Rnc U L8r</a></p>

<b>
<p><a href="../archive/news6568.php">3 Reichs And Yer Out</a></p>
</b>

<p><a href="../archive/news6569.php">T'rubble At Mill (rd)</a></p>

<p><a href="../archive/news65610.php">Off With Their Arms</a></p>

<p><a href="../archive/news65611.php">Identity Politics</a></p>

<p><a href="../archive/news65612.php">And Finally 656</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st November 2008 | Issue 656</b></p>

<p><a href="news656.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>3 REICHS AND YER OUT</H3>

<p>
Here&#8217;s a data loss we can all be happy about. The BNP&#8217;s entire membership (around 12,000 people) has gone online - you can find your local goosestepping moron at <a href="http://www.bnpmemberslist.co.uk" target="_blank">www.bnpmemberslist.co.uk</a> <br />
 <br />
&#8216;Nasty&#8217; Nick Griffin, BNP leader, suspects the individual responsible is a disgruntled &#8220;hardliner&#8221; senior employee who left the party last year.  While Nick claims he welcomes the publicity, saying the list contradicted the perception of the average BNP member as a &#8220;skinhead oik&#8221;. BNP spokesman, Simon Darby took it less well: &#8220;If we find out the name of the person who published this list it will turn out to be one of the most foolish things they have done in their life&#8221;. <br />
 <br />
Additional published information includes addresses, emails, occupations, phone numbers and even supporters&#8217; interests, (including a witch and a Crawley based kite-maker). Those exposed include social workers, prison officers, police, military officers, solicitors and even teachers.  <br />
 <br />
Extra notes include: &#8220;Discretion requested (employment concerns) government employee&#8221; and &#8220;activist (discretion requested), teacher (secondary school)&#8221;. Such &#8216;discretion&#8217; comes as no surprise as many supporters could now face dismissal from work or disciplinary action.  <br />
Despite frantic legal scrabbling from the fascists, the list has now been mirrored all over the internet. The BNP is now ironically relying on the Human Rights Act, (which it opposes) to try and protect the privacy of its members. <br />
 <br />
SchNEWS politely requests its readers not to sign up unsuspecting BNP members to timeshare schemes, catalogue orders, the army etc etc <br />
 <br />
** The BNP&#8217;s conference in Blackpool last weekend was met with a 200-strong anti-fascist demonstration. After handing out leaflets along the promenade, the protesters ended up outside the salubrious facade of the New Kimberly Hotel, where, behind the floral curtains, the BNP delegates thrashed out issues like the seating arrangement and dinner menu. Police had ordered the BNP to stay out of sight and the grubby curtains of the hotel were drawn to protect the public from the ugly mugs of the BNP.  <br />
 <br />
For full report see <a href="http://www.lancasteruaf.blogspot.com/2008/11/over-200-protest-against-bnp-conference.html" target="_blank">www.lancasteruaf.blogspot.com/2008/11/over-200-protest-against-bnp-conference.html</a>
</p>

</div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(140);
			
				if (SHOWMESSAGES)	{
				
						addMessage(140);
						echo getMessages(140);
						echo showAddMessage(140);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>