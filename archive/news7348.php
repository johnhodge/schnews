<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 734 - 6th August 2010 - Wrekin Crew</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, coal, open cast mine, shropshire, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 734 Articles: </b>
<p><a href="../archive/news7341.php">Grassroots Struggle</a></p>

<p><a href="../archive/news7342.php">Campsfield House Hunger Strikes</a></p>

<p><a href="../archive/news7343.php">Climate Camp Wales</a></p>

<p><a href="../archive/news7344.php">Renata Zelazna Is Free!</a></p>

<p><a href="../archive/news7345.php">A Dreadful Bore</a></p>

<p><a href="../archive/news7346.php">More Bang For Your Buck</a></p>

<p><a href="../archive/news7347.php">Ian Tomlinson Protest</a></p>

<b>
<p><a href="../archive/news7348.php">Wrekin Crew</a></p>
</b>

<p><a href="../archive/news7349.php">Eco-battle In Russia</a></p>

<p><a href="../archive/news73410.php">On Yer Bike</a></p>

<p><a href="../archive/news73411.php">Edo Attacked</a></p>

<p><a href="../archive/news73412.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 6th August 2010 | Issue 734</b></p>

<p><a href="news734.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WREKIN CREW</h3>

<p>
<p>
	Protesters from the Huntington Lane Camp in Shropshire successfully navigated a raft down the River Severn on Sunday to raise awareness of the proposal by UK Coal to mine 900,000 tons of coal. The raft, made from recycled materials, flew a huge &ldquo;<strong>No New Coal</strong>&rdquo; banner as it sailed past EON&rsquo;s Buildwas B coal-fired power station and on to Ironbridge where locals and tourists cheered the protesters making their photo-op opposition plain. </p>
<p>
	Another group from the camp spread the message on land by walking along the riverside and educating anyone crossing their path. There is no shortage of issues raised by the proposed mine and the impact that it would have on the surrounding Shropshire Hills Area of Natural Beauty. The extraction is set to include 250,000 tonnes of fireclay as well as the coal and devastate 230 hectares of prime heritage land, all in the shadow of one of the area&rsquo;s most prominent landmark hills, The Wreckin. </p>
<p>
	Let&rsquo;s not even dwell on the at least 2,430,000 tonnes of climate changing CO2 emissions due to be pumped into the atmosphere during its lifetime. </p>
<p>
	The camp at Huntington Lane Surface Mine Site was set up in March after trees were felled on the site indicating that UK Coal intended to begin mining (see <a href='../archive/news714.htm'>SchNEWS 714</a>). Since then, there has been a permanent camp at Huntington, where residents have been busy building tunnels, structures and defences around the camp. They&rsquo;ve managed also to interest local press and keep the campaign visible from beyond the nearest hedge. </p>
<p>
	Already the proposed date for the commencement of work has been forced back, first from early June to the end of June, then to mid July to early August; latest word on the street is that it has now been put back until the end of August. </p>
<p>
	It&rsquo;s not too late for more bodies to make a difference. Anyone wanting to get involved is welcome at the camp and donations of food, tools and water are always needed. </p>
<p>
	* See <a href="http://www.defendhuntingtonlane.wordpress.com" target="_blank">www.defendhuntingtonlane.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(897), 103); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(897);

				if (SHOWMESSAGES)	{

						addMessage(897);
						echo getMessages(897);
						echo showAddMessage(897);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


