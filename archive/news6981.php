<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 698 - 6th November 2009 - Post Apocalypse   </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, workers struggles, strike, privatisation, neo labour, unions, public services" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://natowc.noflag.org.uk/"><img 
						src="../images_main/nato-pa-09-banner.png"
						alt="Smash NATO"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 698 Articles: </b>
<b>
<p><a href="../archive/news6981.php">Post Apocalypse   </a></p>
</b>

<p><a href="../archive/news6982.php">A Far Right Palava</a></p>

<p><a href="../archive/news6983.php">Lights Out</a></p>

<p><a href="../archive/news6984.php">Fuc-toff</a></p>

<p><a href="../archive/news6985.php">Court Of Carmel-ot</a></p>

<p><a href="../archive/news6986.php">Bordering On Insanity</a></p>

<p><a href="../archive/news6987.php">Lawless-cruttenden</a></p>

<p><a href="../archive/news6988.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/698-postman-pat-lg.jpg" target="_blank">
													<img src="../images/698-postman-pat-sm.jpg" alt="Postman Pat" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 6th November 2009 | Issue 698</b></p>

<p><a href="news698.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>POST APOCALYPSE   </h3>

<p>
<strong>POSTAL WORKERS STRIKE AS YET ANOTHER PUBLIC SERVICE IS NEO-LABOURISED <br />  <br /> </strong><strong>Selfish postal workers are threatening to ruin Christmas as thousands of aunty knitted jumpers get caught up in a mail back-log thanks to a series of commie strikes.</strong> <br />   <br /> Just think of your local bus service or overpriced, unreliable trains if you want to see what prospects are in store if private companies take over the <strong>Royal Mail</strong>. Yet the government are at it again, and want to break the union, cut workers pay and conditions and parcel up the service to the highest bidder, heralding a privatised, fragmented service. <br />   <br /> One postie told SchNEWS why he is supporting the strike: &ldquo;<em>Royal Mail recently announced that mail traffic was due to fall by 10% a year and to counter this they were going to engage in a period of &lsquo;modernisation&rsquo;. The reality is that they&rsquo;re going to lay off 30% of the workforce, freeze pay indefinitely and engage in a period of Executive Action which would see a postie&rsquo;s workload increase between 30-40%. All of this bypassed a previously negotiated agreement with the Communications Workers Union. <br />   <br /> &ldquo;Not a single one of us wants to go on strike and lose money but neither do we want to be exploited for poor wages while the guys at the top continue to earn massive bonuses for what, they keep telling us, is a failing company. A company in so much trouble that they managed to make profits last year of ONLY &pound;321 million. <br />  <br /> &ldquo;Of course really Royal Mail is worried about the massive self-inflicted pension deficit, not to mention their desperation to sell off the loss making deliveries sector to the highest bidder. So, for modernisation read privatisation. Try and send a birthday card to your relatives in Inverness for 39p after all this happens.</em>&rdquo; <br />  <br /> While 3,500 post offices have closed across the country, the wages of Royal Mail boss, <strong>Adam Crozier</strong>, have shot up from a just-scraping-by &pound;114,000 a year when he started in 2003 to over a million quid today. Meanwhile he has been busy telling the union to shut up and is employing 30,000 scabs to break the union resolve. <br />   <br /> The CWU are going to the High Court to apply for an injunction against RM taking on workers to cover strikers, an employment practice which is illegal. RM claims the temps are there to clear backlogs in the run up to the festive mayhem. In Slough, Manpower has even set up a recruitment office to get scab posties - more short-term insecure contracted posts and money to middleman recruiters. <br />  <br /> Nine-lives-Mandelson says not enough capital will be raised in the current climate if the Royal Mail is put on the market, so privatisation has been deferred. But when it&rsquo;s pared down after &lsquo;modernising&rsquo;, it&rsquo;ll be a different story. <br />  <br /> Whatever happens over the next couple of days, the post office faces considerable squeezes now and in the near future. This means more ordinary folks will lose their jobs and the likely just-as-expensive-to-run RM will offer a service which is less affordable and equitable (especially for those in the sticks). <br />   <br /> The talks with Royal Mail continue behind closed doors at the TUC, with all sides posturing and brandishing their plans of attack. David Barber of the CWU says that the last three days have been productive, but strikes will go ahead, on November 6th and 9th. Well over a hundred and twenty thousand are set to strike unless the proposal they have tabled is accepted.  <br />  <br /> At least we won&rsquo;t have to wear those f***ing jumpers. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>neo labour</span>, <a href="../keywordSearch/?keyword=privatisation&source=698">privatisation</a>, <span style='color:#777777; font-size:10px'>public services</span>, <span style='color:#777777; font-size:10px'>strike</span>, <span style='color:#777777; font-size:10px'>unions</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=698">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(551);
			
				if (SHOWMESSAGES)	{
				
						addMessage(551);
						echo getMessages(551);
						echo showAddMessage(551);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>