<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 786 - 2nd September 2011 - Leaky Arguments</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, wikileaks" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 786 Articles: </b>
<p><a href="../archive/news7861.php">Edl: It Takes One To E1</a></p>

<p><a href="../archive/news7862.php">Kick Ass Moves</a></p>

<p><a href="../archive/news7863.php">Dale Of Reckoning</a></p>

<b>
<p><a href="../archive/news7864.php">Leaky Arguments</a></p>
</b>

<p><a href="../archive/news7865.php">Seeds Of More Ruction</a></p>

<p><a href="../archive/news7866.php">Hideous Khimki</a></p>

<p><a href="../archive/news7867.php">Tuckers Luck</a></p>

<p><a href="../archive/news7868.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd September 2011 | Issue 786</b></p>

<p><a href="news786.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LEAKY ARGUMENTS</h3>

<p>
<p>  
  	Who&rsquo;d want to be Julian Assange? The whole WikiLeaks thing, whereby Jules tried to find a way to go public with a ton of secret sensitive data without getting bumped off or locked up, must have been a bit stressful to say the least.  </p>  
   <p>  
  	He could have chosen to put it out there on the web anonymously right away, knowing his part in the story would be over, the information therein could never be stopped or removed, and the 250,000 cables would quickly get thoroughly combed by a distributed multitude.  </p>  
   <p>  
  	But he didn&rsquo;t. We assume that was because he was aware that amongst the data, any number of people &ndash; from informants, to political figures to underground activists, could have been identified and their lives could literally be at direct risk from recriminations... And not &lsquo;cos his ego loved all the attention, fancied controlling the saga for as long as possible, and thought &lsquo;man of the century&rsquo; had a nice ring.  </p>  
   <p>  
  	And this meant that he tried to keep control, negotiate (with fear for his life probably) with the US government for &lsquo;sanitised&rsquo; gradual release of information and a deal with big newspapers, knowing they were covered by agreements not to publish material the military / intelligence services can label &lsquo;national interest&rsquo;. No professional journo would risk his career for it. (A so-so bargain for transparency perhaps; one can imagine the censoring of plenty of material other than only the life- threatening revelations about undeserving people).  </p>  
   <p>  
  	And the media, establishment and legal shit storm whipped up since this thing blew has certainly left Assange feeling hounded and under pressure from all quarters.  </p>  
   <p>  
  	A sign of the strain telling, perhaps, was the announcement by WikiLeaks this week that they are taking legal action against the Guardian for er, leaking. (Well it&rsquo;s just not c-wiki-t!) They allege that a book by two Guardani hacks published a few months ago contains reference to a secret password that has caused them to lose control of all of the data. This is despite the fact that full unredacted version of the cables has been available to bittorrent online at various times ever since WikiLeaks first went public. The Guardian also retort that Assange told them the password in question was only a temporary one, shortly to expire be deleted - and that it any event only relates to one unidentified file and was not a golden key to the whole shebang.&nbsp; &nbsp;  </p>  
   <p>  
  	But with little irony about dancing with the devil, a WikiLeaks tweet declared; &ldquo;We have already spoken to the [US] State Department and commenced pre-litigation action.&rdquo; - in a bizarre twist the people he has most pissed off are the only ones even more keen to stop unredacted cables circulating than he is. Strange bedfellows indeed &ndash; but merely attempting to deflect pressure by pointing the finger at the Guardian is never going to save his skin, if it needs saving, nor win many new friends.  </p>  
   <p>  
  	But, thanks in part to Assange, this genie can&rsquo;t&nbsp; ever be put back in the bottle. For years it will provide hard evidence of all kinds of Realpolitik in action; some shedding light on the real elite agendas we know to exist round the world but are rarely officially acknowledged... (And the US will redouble efforts to terrorise their subjects out of ever daring to whistle-blow in the future.)  </p>  
   <p>  
  	But we do worry for poor old Jules...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1342), 155); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1342);

				if (SHOWMESSAGES)	{

						addMessage(1342);
						echo getMessages(1342);
						echo showAddMessage(1342);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


