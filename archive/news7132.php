<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 713 - 12th March 2010 - Schcoop! - No Cutbacks For Sussex Fat Cats</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, sussex university, students" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 713 Articles: </b>
<p><a href="../archive/news7131.php">Sussex, Lies And Videotape</a></p>

<b>
<p><a href="../archive/news7132.php">Schcoop! - No Cutbacks For Sussex Fat Cats</a></p>
</b>

<p><a href="../archive/news7133.php">Sea Shepherd Roundup : Whaling Banshees</a></p>

<p><a href="../archive/news7134.php">A-fence-is Weapon</a></p>

<p><a href="../archive/news7135.php">Hungry For Reform</a></p>

<p><a href="../archive/news7136.php">Hey Joe</a></p>

<p><a href="../archive/news7137.php">Coal-amity</a></p>

<p><a href="../archive/news7138.php">Tesco A No Go</a></p>

<p><a href="../archive/news7139.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th March 2010 | Issue 713</b></p>

<p><a href="news713.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCHCOOP! - NO CUTBACKS FOR SUSSEX FAT CATS</h3>

<p>
As Sussex students take their resistance to funding cuts to the heart of their university&rsquo;s governing elite (see <a href='../archive/news712.htm'>SchNEWS 712</a>), details of the lifestyles of those making the cuts are trickling out. The &pound;100,000+ salaries of 14 Sussex managers isn&rsquo;t enough with expense accounts regularly dipped into to make up the shortfall. <br />  <br /> Claims seen by students include pro vice chancellor for education Joanne Wright spending &pound;1,811.25 on a Times Higher Education award jolly at the Grosvenor House Hotel on Park Lane on 15th October 2008. Although dinner was included they still claimed &pound;253.95 for refreshments. Colleague pro vice chancellor Robert Allison has matched her appetite for fine food, even wining and dining arms company bosses &ndash; in July 2008 he spent &pound;147.50 at Drakes Hotel&rsquo;s Gingerman restaurant with Rolls Royce representatives. Since the start of the current round of spending cuts last September the gourmet has submitted &pound;1,265.41 just in claims for restaurant meals. <br />   <br /> Despite the cuts to courses and staff Sussex bosses saw fit to appoint another Pro Vice Chancellor last year, to try and get a cut of some of the lucrative foreign market. Last November Professor Chris Marlin was appointed Pro-Vice Chancellor to &lsquo;provide senior academic leadership for the University&rsquo;s internationalisation strategy&rsquo;, according to the uni&rsquo;s website. Even before taking up his post the prof had already benefited from Sussex hospitality. In claims seen exclusively by SchNEWS, the computer science boffin, jetting in from Australia for his interview last July, stayed at the &pound;132 a night Hilton Metropole racking up a total expense bill of over &pound;2,500 for his seaside jaunt. Such was Chris&rsquo; concern at being out of pocket that he e-mailed the uni a week after his return chasing up payment. <br />  <br /> He must have been chuffed at his appointment and the prospect of sampling more of the local culinary scene with tastes ranging from the traditional &ndash; fish restaurants &ndash; to international cuisine, as is only consistent with his &lsquo;internationalisation strategy&rsquo;. Catching the last of the summer sun Chris spent another week here in September, but perhaps the cutbacks were already kicking in, as he only claimed around a grand for this outing.  <br /> However, this is all dwarfed by his claims on taking up his post in mid-November. Relocation  expenses claimed from then until Christmas amount to around &pound;12,000, with shipping and accommodation in a cottage in Lewes comprising the bulk. <br />  <br /> More dubious than such run of the mill gravy train high living, is a memo from vice-chancellor Paul Layzell requesting the transfer of his &pound;27,906.55 consultancy fees from the University of Manchester to &lsquo;an account to which I can have free access&rsquo;, so as not to be counted as taxable salary. No lazy accounting here, honest guv, as the money is to be used for &lsquo;academic travel, personal development, and sundry items of IT equipment etc.&rsquo; according to the memo. <br />   <br /> As if calling in the cops weren&rsquo;t enough, the university is planning to charge the Students&rsquo; Union (SU) for the policing and security costs of the occupations, suggesting they distance themselves from the Stop The Cuts campaign. Registrar Owen Richards&rsquo; proposal for a &lsquo;much stronger dynamic&rsquo; is to directly bill the SU for the costs of past and future occupations despite any proof the union is behind them. Should this exceed the &pound;50k SU discretionary annual funding, next year&rsquo;s funding will be cut. Subsequent hints made by the bosses before the latest occupation to the SU in a petty attempt to weaken the campaign can only end in tears for senior management. This OTT response of the university has merely exposed their fear of direct action protest, and are hoping by means fair or foul to nip them in the bud. <br />   <br /> Other interesting reading material by the uni kleptocracy includes advice from a certain Rt Hon Lord Mandelson from the Department for Business Innovation and Skills (BIS) on extremism in universities. Bucking the trend of the cutbacks a &lsquo;Universities-led UK working group on terrorism&rsquo; was recently set up. Mandy will be &lsquo;BISy&rsquo; with &lsquo;a series of regional workshops, with local police and security officials&rsquo;. Other tips include advice for unis to collaborate with local authorities&rsquo; Prevent partnership to &lsquo;stop people being drawn into terrorism&rsquo;. Money well spent... <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=students&source=713">students</a>, <a href="../keywordSearch/?keyword=sussex+university&source=713">sussex university</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(689);
			
				if (SHOWMESSAGES)	{
				
						addMessage(689);
						echo getMessages(689);
						echo showAddMessage(689);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


