<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 745 - 29th October 2010 - Serious Organised Crime</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, shac, hls, vivisection" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 745 Articles: </b>
<b>
<p><a href="../archive/news7451.php">Serious Organised Crime</a></p>
</b>

<p><a href="../archive/news7452.php">First Cuts Not The Deepest</a></p>

<p><a href="../archive/news7453.php">Edl: Down The Rabbi Hole</a></p>

<p><a href="../archive/news7454.php">Somerset Sabs Attacked</a></p>

<p><a href="../archive/news7455.php">Cuts Both Ways</a></p>

<p><a href="../archive/news7456.php">Naples: Load Of Rubbish</a></p>

<p><a href="../archive/news7457.php">Snogfest Vs Nazi Nonce</a></p>

<p><a href="../archive/news7458.php">The Us Itt Parade</a></p>

<p><a href="../archive/news7459.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/745-mark-kennedy-lg.jpg" target="_blank">
													<img src="../images/745-mark-kennedy-sm.jpg" alt="Mark Stone (Kennedy)" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 29th October 2010 | Issue 745</b></p>

<p><a href="news745.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SERIOUS ORGANISED CRIME</h3>

<p>
<p>
	<strong>UK PLC CONSPIRES TO PROTECT VIVISECTION LAB AS MORE ACTIVISTS ARE SENT TO JAIL</strong> </p>
<p>
	The six defendants in the second SHAC trial were hammered with vicious sentences this week (see <a href='../archive/news738.htm'>SchNEWS 738</a>). The harshest was the six years handed down to 53-year-old Sarah Whitehead. Sentences for the other defendants Tom Harris, Nicole Vosper, Jason Mullan, Nicola Tapping and Alfie Fitzpatrick ranged from a two year suspended sentence for the youngest, Alfie, to three and a half years for Nicole Vosper. All the defendants also copped lengthy ASBOs, which will prevent them from any further participation in animal rights activism. </p>
<p>
	All six were charged with &lsquo;conspiracy to interfere with the contractual relations of an animal research organisation&rsquo; under the SOCPA legislation, a law brought in by New Labour to specifically target the animal rights movement. They were arrested in a massive series of raids in 2007. </p>
<p>
	A lot of shit has been talked about this trial and the defendants in the mainstream media. Most of the press has been happy to reprint the NETCU line that the defendants were personally involved in planting bombs and conducting violence against individuals. The Mail has claimed that SHAC and the ALF are one and the same thing. The BBC carried an article entitled &lsquo;Under Siege&rsquo; that claimed that the six individuals sentenced were personally responsible for vandalism at his home over a period of seven years. </p>
<p>
	In fact the nature of the conspiracy charge means that the prosecution did not have to prove anything, except that the defendants knew one another and were part of the SHAC campaign against HLS - and that at least part of this campaign involved illegal activity. These &lsquo;catch-all&rsquo; conspiracy cases are being used to attack a militant and successful movement against UK corporate interests. The last SHAC trial (see <a href='../archive/news663.htm'>SchNEWS 663</a>) saw activists receive up to 11 year sentences on similar conspiracy charges. </p>
<p>
	NETCU were of course gloating about the verdict, saying, &ldquo;<em>We are very satisfied with the outcome today. In 2004 a major police investigation was launched into the criminal activities linked to SHAC. Specialist support was also provided by the Serious Organised Crime Agency and the City of London Police&rsquo;s Economic Crime Unit</em>&rdquo; then adding laughably, &ldquo;<em>The police service remains committed to facilitating the peaceful protest of the majority</em>&rdquo;. </p>
<p>
	Of course in real life the police have never neglected their role as defenders of power against truth. It is unlikely that the &lsquo;major police investigation&rsquo; ended after the 2007 raids. Long-term police infiltrator PC Mark Kennedy (AKA Stone - see graphic) attended animal rights events as recently as September 2010. </p>
<p>
	Has this stopped SHAC? Hardly. During the sentencing, which took three days, demonstrations took place simultaneously, in London, at the gates of the two HLS sites as well as their beagle suppliers at Harlan Huntingdon and their rabbit breeders at Highgate Lincolnshire. There was even a brief foray into Cambridgeshire Police HQ looking for the National Extremism Tactical Co-ordinating Unit&rsquo;s (NETCU) office. </p>
<p>
	* See <a href="http://www.shac.net" target="_blank">www.shac.net</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(987), 114); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(987);

				if (SHOWMESSAGES)	{

						addMessage(987);
						echo getMessages(987);
						echo showAddMessage(987);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


