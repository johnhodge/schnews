<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 780 - 22nd July 2011 - RIP Lucero Aguilar</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, cartel, drugs, narco wars" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 780 Articles: </b>
<p><a href="../archive/news7801.php">Let Them Eat Lead</a></p>

<p><a href="../archive/news7802.php">Float Some And Jet Some</a></p>

<p><a href="../archive/news7803.php">Calais Catering Callout</a></p>

<p><a href="../archive/news7804.php">Schnews In Brief 780</a></p>

<p><a href="../archive/news7805.php">Taken For Ride</a></p>

<b>
<p><a href="../archive/news7806.php">Rip Lucero Aguilar</a></p>
</b>

<p><a href="../archive/news7807.php">Brazil Nuts</a></p>

<p><a href="../archive/news7808.php">Good Will Huntington</a></p>

<p><a href="../archive/news7809.php">Unhampered</a></p>

<p><a href="../archive/news78010.php">Waywards Heath</a></p>

<p><a href="../archive/news78011.php">Endure A Cell</a></p>

<p><a href="../archive/news78012.php">Ratcliffe Hanger</a></p>

<p><a href="../archive/news78013.php">Pompey & Circumstance</a></p>

<p><a href="../archive/news78014.php">Pie In The Sky</a></p>

<p><a href="../archive/news78015.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd July 2011 | Issue 780</b></p>

<p><a href="news780.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RIP LUCERO AGUILAR</h3>

<p>
<p>  
  	Mexico&rsquo;s drug war claimed another life in Ciudad Juarez, the border city that is officially the world&rsquo;s most violent city. In a city with an an average death toll of eight murders a day, this is shocking but not surprising. The victim was 19 year old Lucero Aguilar, an activist for MORENO, a left-wing youth movement. A former drug dealer himself, Lucero had quit the bloody business and dedicated himself to rescuing other youths in the poorest neighbourhoods from getting sucked in by the promises of money and power (and a short lifespan) offered by the cartels.  </p>  
   <p>  
  	Lucero was ambushed in the local football field, where gunmen shot him in the back. He died shortly afterwards in hospital. It&rsquo;s highly unlikely that his killing will ever be investigated, or that his killers will be brought to justice. In Juarez, like most of Mexico, the narcos (gangsters) operate with complete impunity- if his killers weren&rsquo;t actually policemen, they will have collaborated with the police and city authorities.  </p>  
   <p>  
  	Friends from his neighbourhood said, &ldquo;He was a man of his word, he cared for the kids, and he worked because he loved his neighbourhood&rdquo;  </p>  
   <p>  
  	It is believed that his killing was meant to be a warning to Juarez&rsquo;s peace activists and community organisers, who have been getting more visible and confident since the arrival of the Peace Caravan back in June (see <a href='../archive/news775.htm'>SchNEWS 775</a>). Lucero&rsquo;s death is another reminder of just how cheap life is in Juarez, and how vulnerable activists are in Mexico.  </p>  
   <p>  
  	* More Moreno (for Spanish readers) at <a href="http://www.regeneracion.mx" target="_blank">www.regeneracion.mx</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1291), 149); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1291);

				if (SHOWMESSAGES)	{

						addMessage(1291);
						echo getMessages(1291);
						echo showAddMessage(1291);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


