<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 777 - 1st July 2011 - Campaign for Real Bail</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police, high court" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 777 Articles: </b>
<p><a href="../archive/news7771.php">Libya: Anti-nato Classes</a></p>

<p><a href="../archive/news7772.php">Africa House Evicted</a></p>

<p><a href="../archive/news7773.php">Simon Levin Rip</a></p>

<p><a href="../archive/news7774.php">A Ship Off The Ol'bloc</a></p>

<b>
<p><a href="../archive/news7775.php">Campaign For Real Bail</a></p>
</b>

<p><a href="../archive/news7776.php">Greece: The Golden Fleecing</a></p>

<p><a href="../archive/news7777.php">Peru - What A Scorcher</a></p>

<p><a href="../archive/news7778.php">Turn On The Waterways</a></p>

<p><a href="../archive/news7779.php">Sheikh-down</a></p>

<p><a href="../archive/news77710.php">Holding The Fortnum</a></p>

<p><a href="../archive/news77711.php">[headline On Strike]</a></p>

<p><a href="../archive/news77712.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st July 2011 | Issue 777</b></p>

<p><a href="news777.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CAMPAIGN FOR REAL BAIL</h3>

<p>
<p>  
  	Since the Police and Criminal Evidence Act 1984 (PACE) came into effect it has been routine for cops to arrest and lock up suspects for most of the day, interview, then release on police bail for weeks or months. This bail often comes with restrictive conditions like banning use of the internet, curfews, restriction on movement and association, the forcing of &lsquo;suspects&rsquo; to sign on at ridiculously inconvenient times and places and more.  </p>  
   <p>  
  	The period of bail and the conditions attached to it are arbitrarily decided by the police themselves, who have a particular penchant for applying them to political cases - restricting people&rsquo;s ability to protest and making their day-to-day life a struggle (see SchNEWS 1-776). In many cases police bail has been used as a form of extra-judicial punishment, with long bail periods and more draconian conditions often hinting that police feel they have little or no chance of successful prosecution.  </p>  
   <p>  
  	This may all be set to change, after the High Court upheld a ruling that police bail should be treated as detention and, therefore, can only be applied for a maximum of 96 hours. Of course, this won&rsquo;t mean police can only investigate an offence for four days, merely that they would have to gather evidence and charge people prior to punishing them, so surely a step forward for the &lsquo;justice&rsquo; system?  </p>  
   <p>  
  	Apparently not, if the police and politician&rsquo;s pleas are anything to go by. Having spent the past 25 years abusing the power of indefinite police bail, officers are now understandably upset they may have to investigate crime and put a case before the courts in future. No longer will not liking someone&rsquo;s politics, attitude or skin colour be enough to make someone&rsquo;s life a misery.  </p>  
   <p>  
  	Chief Constable Norman Bettison of West Yorkshire Police stated that his force is &ldquo;<em>running round like headless chickens...wondering what this means to the nature of justice</em>.&rdquo; Fortunately these powerless coppers have friends in high places with Home Secretary Theresa May &ldquo;<em>looking at a number of possibilities</em>&rdquo; including appealing the ruling for a second time or bringing in emergency legislation.  </p>  
   <p>  
  	Even Liberty have come down on the side of the cops with their legal director saying &ldquo;<em>Being out on bail pending investigation is not the equivalent of being detained. Limits on the time that suspects can be held in police custody are necessary but there are good reasons why the police should be allowed to bail suspects for more than 96 hours</em>.&rdquo;  </p>  
   <p>  
  	Until the scales of justice can be fully rebalanced back in the state&rsquo;s favour it is unclear exactly what this will mean if you are on bail now or expecting to be in the near future. After the ruling, the CPS and Acpo received legal advice which stated that it set new case law and had to be adhered to. However, senior officers have already indicated they will continue to use police bail as before, regardless of whether or not it is actually legal. Meanwhile Greater Manchester Police (the hapless force that brought this state of affairs about in the first place) is attempting to appeal at the Supreme Court.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1267), 146); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1267);

				if (SHOWMESSAGES)	{

						addMessage(1267);
						echo getMessages(1267);
						echo showAddMessage(1267);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


