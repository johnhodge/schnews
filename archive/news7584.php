<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 758 - 11th February 2011 - Greece: Hunger Strikes</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, migrant rights, hunger strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 758 Articles: </b>
<p><a href="../archive/news7581.php">The Withdrawal Method</a></p>

<p><a href="../archive/news7582.php">Edl Go To Hell, Well Luton</a></p>

<p><a href="../archive/news7583.php">Egypt: Tahrir We Go Again</a></p>

<b>
<p><a href="../archive/news7584.php">Greece: Hunger Strikes</a></p>
</b>

<p><a href="../archive/news7585.php">Greece: Trash Talk</a></p>

<p><a href="../archive/news7586.php">Harper Scarper</a></p>

<p><a href="../archive/news7587.php">Shopped Cops Dropped</a></p>

<p><a href="../archive/news7588.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th February 2011 | Issue 758</b></p>

<p><a href="news758.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREECE: HUNGER STRIKES</h3>

<p>
<p>
	On Jan 25th, 300 undocumented migrant workers began a hunger strike in Greece, demanding legalization of all migrants in Greece, with political and social rights equal to Greek employees. <br /> 
	Fifty of the 300 are at The Labor Centre in Thessaloniki. The others are in Athens, now based in a privately owned building after a night-time eviction from the out of use Athens University Law School. </p>
<p>
	Police, backed by the rector, violated the &ldquo;University Asylum&rdquo; law, which declares all public university grounds off limits to police. Under this law, only university rectors councils can lift the asylum, and only in life-threatening cases or if an automatically prosecutable crime is committed - clearly not the case. </p>
<p>
	The Law School was laid siege by hundreds of cops, with traffic banned in surrounding streets. Thousands gathered outside the Law School in solidarity, while gatherings and demonstrations took place around Greece. </p>
<p>
	The state proposed a new building for the migrants, which they initially refused due to the severe conditions imposed on its use &ndash; a ring of cops around the building, no-one but the hunger strikers themselves to be allowed inside, and it would only be available for one week. </p>
<p>
	After a long hard night of negotiations, at 4:30am demonstrators moved with the hunger strikers to the new building. On arrival, many of the rooms were locked, no heating was available and there was only one toilet. Many of the hunger strikers are sleeping in tents in the courtyard. <br /> 
	This building is only available until this Friday (11th). As yet there is nowhere for the hunger strikers, by now well into their third week, to move to. </p>
<p>
	Solidarity actions and demonstrations continue around Greece, with messages of support from far and wide. A common day of action has been called for Friday 11th February. </p>
<p>
	Messages of support can be sent to <a href="mailto:ypografes.allilegyi.stin.apergia@gmail.com">ypografes.allilegyi.stin.apergia@gmail.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1099), 127); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1099);

				if (SHOWMESSAGES)	{

						addMessage(1099);
						echo getMessages(1099);
						echo showAddMessage(1099);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


