<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 768 - 22nd April 2011 - What Now?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, protest, party, demonstration, anti fascist, ecology, climate camp, rossport, shell, may day, nuclear power, sizewell" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 768 Articles: </b>
<p><a href="../archive/news7681.php">Watch My Lips</a></p>

<p><a href="../archive/news7682.php">Slicking With It</a></p>

<p><a href="../archive/news7683.php">Carry On Suing</a></p>

<p><a href="../archive/news7684.php">Rotten To The Core</a></p>

<b>
<p><a href="../archive/news7685.php">What Now?</a></p>
</b>

<p><a href="../archive/news7686.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd April 2011 | Issue 768</b></p>

<p><a href="news768.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WHAT NOW?</h3>

<p>
<p>  
  	The sun is shining, there&#39;s four public holidays coming up and you&#39;ve already bought your <a href="../pages_merchandise/misc.php">SchNEWS commemorative teatowel</a> for the wedding, so what now? Don&#39;t worry, there&#39;s plenty more to keep you busy, so get involved with one of the upcoming actions planned for the next week.  </p>  
   <p>  
  	<b>22nd - 25th April, Suffolk:</b> <a href="http://stopnuclearpower.blogspot.com/2011/03/sizewell-camp-2011.html" target="_BLANK">Sizewell Camp</a> - Spend Easter weekend camping on the beach at Sizewell, on the picturesque Suffolk coast, eastern England, to show your opposition to new nuclear power and highlight the need for sustainable energy solutions. Mass demo on Saturday 23rdth, 12noon at the entrance to Sizewell A and B nuclear power stations. Workshops and cinema on the Sunday.  </p>  
   <p>  
  	<b>23rd April - 1st May, Sussex:</b> <a href="http://brightonclimateaction.org.uk/" target="_BLANK">Resurrection for the Insurrection</a> - South Coast Climate Camp host their own week-long action somewhere in Sussex. Workshops are invited from, UK Uncut, Plane Stupid, SolFed, Smash EDO, No Borders, Grow Heathrow, Transition Towns, the Rebel Clown Army and many more. Skill shares will be held on permaculture, guerilla gardening, workers rights and tactics of resistance, cooking, building rocket stoves, bee keeping &amp; community organising; among other stuff. A mass action at the end of the week to join Brighton&#39;s convergence of direct action groups at the May Day celebrations (see below).  </p>  
   <p>  
  	<b>24th April, Brighton:</b> <a href="http://brightonantifascists.wordpress.com/2011/04/19/oppose-the-march-for-england-sunday-24th-april/" target="_BLANK">Oppose the March for England</a> - The far right March for England will be holding a demonstration in Brighton this Sunday, 24th April. Despite their claims of being a non political celebration of St George&rsquo;s Day, their supporters have a large overlap with the English Defence League and other far right and fascist groups, including those who recently attacked a Unite Against Fascism meeting on multiculturalism in Brighton. Meet at the bus stops&nbsp; in front of Brighton Station at 11am.  </p>  
   <p>  
  	<b>29th April - 6th May, Rossport, Ireland:</b> <a href="http://www.rossportsolidaritycamp.org/content/may-day-may-day-rossport-solidarity-camp-summer-gathering-sat-30-april-mon-2nd-may-1" target="_BLANK">May Day! May Day! Rossport Solidarity Camp Summer Gathering</a> - Come up to the Rossport Solidarity Camp Summer Gathering for the May Bank Holiday weekend to see for yourself what&#39;s at stake and learn more about the campaign.&nbsp;Come for a weekend of workshops &amp; actions. Meet the affected community and hear their stories of resistance to Shell. See this incredible place and go on the Walk the Pipe tour. This will be a family friendly space with a kid&#39;s area. This will be a busy time for actions as Shell will be attempting to work on the pipeline so if you haven&#39;t been up to the camp yet, the gathering is a great time to get involved.  </p>  
   <p>  
  	<b>30th April, Brighton:</b> <a href="http://brightonmayday.wordpress.com/" target="_BLANK">May Day! May Day! Mass Party and Protest</a> - A coalition of direct action groups including Brighton Anarchist Black Cross, Smash EDO, South Coast Climate Camp, Sussex IWW, No Borders Brighton, Squatters&rsquo; Network of Brighton, Brighton AntiFascists, Brighton Hunt Saboteurs, Brighton Uncut, Queer Mutiny Brighton and many more gather for a day of mass action. The bankers&rsquo; crisis is being used as an excuse to push through a far-right agenda of vicious cuts &ndash; cuts which will hurt the poorest and line the pockets of the rich. The NHS, Education and Welfare systems are being stripped back while the bankers are being paid million pound bonuses. Their only answer to the disaster is more of the same, capitalism with the gloves off. More wars, more ecological devastation and we&rsquo;re being asked to pay for it. We are being robbed blind &ndash; it&rsquo;s time to reclaim what is ours. Meeting time and location TBA.  </p>  
   <p>  
  	<b>30th April, Bath:</b> <a href="http://bathanticutsalliance.blogspot.com/2011/04/defend-our-may-day.html" target="_BLANK">Defend Our Mayday</a> - The Con-Dem coalition have announced plans to scrap the May Day bank holiday and relocate it to the autumn as &#39;Trafalgar Day&#39;. Yet there are already far too many religious, military, rich or royal figures that we are forced to revere; May Day is one of the few days celebrating the achievements of ordinary people, and our self-styled leaders want to take it from us. So, to defend our history, Bath Trades Council, the Bath Anti-Cuts Alliance and others will gather on Saturday the 30th May, from 11.30am, outside Marks &amp; Spencers at the bottom of Stall Street. Why not join us with banners, placards and leaflets? Remember: the powerful give nothing up willingly, and neither will we.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1192), 137); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1192);

				if (SHOWMESSAGES)	{

						addMessage(1192);
						echo getMessages(1192);
						echo showAddMessage(1192);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


