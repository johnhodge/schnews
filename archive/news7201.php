<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 720 - 30th April 2010 - Crash Of The Titans</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, imf, economy, anarchists, financial crisis" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 720 Articles: </b>
<b>
<p><a href="../archive/news7201.php">Crash Of The Titans</a></p>
</b>

<p><a href="../archive/news7202.php">Mayday! Mayday!</a></p>

<p><a href="../archive/news7203.php">Meltdown Britain: Debts All Folks...</a></p>

<p><a href="../archive/news7204.php">Off The Rails</a></p>

<p><a href="../archive/news7205.php">Deportation Update</a></p>

<p><a href="../archive/news7206.php">Obtuse Angles</a></p>

<p><a href="../archive/news7207.php">Chinese Burn</a></p>

<p><a href="../archive/news7208.php">Radioactive Wasters</a></p>

<p><a href="../archive/news7209.php">Schnews In Brief</a></p>

<p><a href="../archive/news72010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/720-greek.lg.jpg" target="_blank">
													<img src="../images/720-greek.sm.jpg" alt="What's a Grecian Urn? Not nearly enough..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 30th April 2010 | Issue 720</b></p>

<p><a href="news720.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CRASH OF THE TITANS</h3>

<p>
<p class="MsoNormal"><strong>AS PROPOSED IMF SOLUTION SPARKS RESISTANCE...</strong> <br />  <br /> Greece is further troubled as government officials get ready to announce IMF imposed wage cuts  that will resonate across the country&rsquo;s entire public sector. Emergency demonstrations were held across the country (Thursday 29th April) in protest against the cuts by grass-roots trade unions, leftist and anarchist organisations.&nbsp; <br />  <br /> Demonstrators took to the streets of Athens and Thessaloniki, banging drums and chanting slogans such as &ldquo;no sacrifice for plutocracy&rdquo;, and &ldquo;real jobs, higher pay&rdquo;. They were joined by uniformed police, coast guard and fire service officer (who were on strike). &ldquo;The fight must be constant until the stability pact - these unpopular measures passed by the government - is overturned,&rdquo; said demonstrator Olga Raptou. An unofficial police estimate put the Athens crowd at about 20,000; organisers said the actual number was much higher. Petite scale riot occurred in Exarcheia, Athens&rsquo; anarchist hub, between police and protesters where cops cleared the scene with tear gas. There were clashes across most of the capital&rsquo;s neighbourhoods as reported in Greek Indymedia. <br />   <br /> There has been no end to incensed incidents since the general strikes began this year. Wednesday saw solidarity actions for Giannis Dimitrakis, an anarchist that was arrested and heavily wounded by police fire after a robbery at a branch of the National Bank more than 4 years ago. There was an 1000 strong demonstration in Athens with 100&rsquo;s of cops also in attendance.  <br />  <br /> Many other cities also particpated. Three radio stations were occupied by protesters on the day. In Buenos Aires, Argentina, a protest took place outside the Greek Embassy where 50 activists attacked the building with Molotovs. Five were arrested. On the 14th April, during the evening news, 70 anarchists occupied the TV station &lsquo;Creta TV&rsquo; in Heraclion, Crete; where they were left to broadcast unhindered. Six anarchists were arrested suspected of being members of Revolutionary Struggle, a terrorist group accused of a rocket strike against the U.S. Embassy and the shooting of a riot policeman on April 11th. <br />   <br /> The IMF is set to pour further fuel on the fire by demanding that the government steps up the unpopular policies to rein in a public sector deficit of 13.6% and &euro;300bn debt over the next three years. Greece&rsquo;s debt has come home to haunt the big powers within the EU. The crisis in Greece risks undermining the euro and spreading to the rest of Europe. This situation is posing a huge dilemma for the major financial institutions, in particular for the European Central Bank and all the major capitalist powers within the EU. If they allowed the &ldquo;market to let rip&rdquo; and let Greece default that would be the beginning of the end for the euro.  <br />  <br /> The fact is that the Greek crisis is part of the overall global crisis and because of that should it go into a downward spin there could be no end in sight and this could spread to the rest of Europe and have a major impact on the world economy. Among the measures under consideration is an increase of VAT from 21% to 25%, a further 10% increase in levies on fuel, alcohol and cigarettes and a cut in bonuses. This is the toughest austerity programme the country has seen since the second world war &ndash; a mix of wage cuts and tax hikes &ndash; the income of the average Greek is down nearly 20%, with low- and middle-income earners especially hard-hit. The inequity of the measures has created a hostile climate as crime has also risen and the mood in city centres has become increasingly edgy.  <br />  <br /> There is another 24hour general strike planned on May the 5th, <em>watch this space... <br /> </em>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anarchists&source=720">anarchists</a>, <a href="../keywordSearch/?keyword=economy&source=720">economy</a>, <a href="../keywordSearch/?keyword=financial+crisis&source=720">financial crisis</a>, <a href="../keywordSearch/?keyword=greece&source=720">greece</a>, <a href="../keywordSearch/?keyword=imf&source=720">imf</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(754);

				if (SHOWMESSAGES)	{

						addMessage(754);
						echo getMessages(754);
						echo showAddMessage(754);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


