<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 746 - 4th November 2010 - Students Do It Dub-Style</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, students, ireland, austerity, financial crisis" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 746 Articles: </b>
<p><a href="../archive/news7461.php">The Vodafone-y War</a></p>

<b>
<p><a href="../archive/news7462.php">Students Do It Dub-style</a></p>
</b>

<p><a href="../archive/news7463.php">Need Scumbody To Love</a></p>

<p><a href="../archive/news7464.php">Coal-ition Politics</a></p>

<p><a href="../archive/news7465.php">Above Their Station</a></p>

<p><a href="../archive/news7466.php">G4s Deportation Death</a></p>

<p><a href="../archive/news7467.php">3-pronged Attack</a></p>

<p><a href="../archive/news7468.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th November 2010 | Issue 746</b></p>

<p><a href="news746.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STUDENTS DO IT DUB-STYLE</h3>

<p>
<p>
	A 40,000 strong student protest against education cuts and increases in student fees ended in brutal clashes with the Gardai after an attempt to occupy the Department of Finance in Dublin on Wednesday (3rd). The protest was the largest student mobilisation in a generation as over 200 busloads descended on the capital from all over Ireland. </p>
<p>
	In what had been a largely peaceful march, with placards waving such messages as &lsquo;Pay my fees or pay my dole&rsquo;, things heated up when a 1000-strong breakaway group got closer to the Dail - the Irish parliament - chucking eggs and bottles. At around 2.30pm, around 50 students pushed past the Gardai into the Department of Finance building and staged a sit-in. </p>
<p>
	Both the students in the building and the several hundred outside were then subjected to the Gardai in full-on riot mode. Baton-happy cops began attacking protesters, some of whom were trampled and kicked by police horses and dragged by their hair. </p>
<p>
	Despite even the likes of the Belfast Telegraph taking a sympathetic line to the day&rsquo;s events, the two-faced Union of Students in Ireland (USI) distanced itself from the occupiers and appeared to be siding with police. In a statement the Union claimed to be <em>&ldquo;saddened by the actions of a small minority</em>&rdquo;, who resorted to &ldquo;<em>anti-social behaviour</em>&rdquo;. All this came after the president of the USI had addressed the march saying &ldquo;<em>the sleeping giant that is the student movement has been awoken</em>.&rdquo; </p>
<p>
	He didn&rsquo;t know how right he was. </p>
<p>
	* See <a href="http://www.indymedia.ie" target="_blank">www.indymedia.ie</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(997), 115); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(997);

				if (SHOWMESSAGES)	{

						addMessage(997);
						echo getMessages(997);
						echo showAddMessage(997);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


