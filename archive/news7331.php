<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 733 - 30th July 2010 - Robot Wars</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, predator drones, afghanistan, anti-war" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 733 Articles: </b>
<b>
<p><a href="../archive/news7331.php">Robot Wars</a></p>
</b>

<p><a href="../archive/news7332.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7333.php">Greece: Fuel On The Fire</a></p>

<p><a href="../archive/news7334.php">Edo: Lend Me Your Smears</a></p>

<p><a href="../archive/news7335.php">Rossport: Not Bored Yet</a></p>

<p><a href="../archive/news7336.php">Ian Tomlinson Protests</a></p>

<p><a href="../archive/news7337.php">Earth First! Gathering</a></p>

<p><a href="../archive/news7338.php">Radical Media Roundup</a></p>

<p><a href="../archive/news7339.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th July 2010 | Issue 733</b></p>

<p><a href="news733.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROBOT WARS</h3>

<p>
<p>
	<strong>SCHNEWS ASKS, DRONES? ARE UAV-ING A LAUGH?</strong> </p>
<p>
	Want to keep fighting an unwinnable war while keeping down the home team casualty figures? You need remote-control warfare. Drones or UAVs (Unmanned Aerial Vehicles) are increasingly becoming the face of modern imperial war. Remotely piloted Predator, Reaper and now Avenger drones mean that the world&rsquo;s great powers can unleash destruction from afar without risking a single soldiers life. </p>
<p>
	Following a series of well documented massacres of Afghan civilians by US bombers, former general McCrystal more or less banned the use of airstrikes in the Afghan campaign, with exception of UAV drones. The result, a massive increase in the number of combat drones (and massacres by combat drones). </p>
<p>
	This weekend saw four CIA drone strikes in Pakistan with a death toll estimated to be around 35.&#8239;&#8239; This has brought the number of drone strikes in Pakistan since Nobel Peace Prize winning President Barack Obama came to power to 100.&#8239;&#8239;The Guardian reports that&#8239;British Reapers have been used 97 times to launch attacks in Afghanistan since 2008. Meanwhile, according to Pakistani sources, they have also killed some 700+ civilians. This is 50 civilians for every militant killed, a hit rate of 2 percent. </p>
<p>
	The &lsquo;big three&rsquo; of the War on Terror (US, Britain and Israel) are the driving forces behind the rise of the remote control assassins and although the Britain-Israel arms trade has dropped slightly following the attacks on Gaza (see <a href='../archive/news661.htm'>SchNEWS 661</a>) the big winner has been joint UK-Israeli UAV research. </p>
<p>
	Back in 2005 the UK government awarded a $500 million contract to UAV Tactical Systems Ltd - a partnership of Thales UK (British arm of the massive French arms company) and Israel&rsquo;s Elbit. The Brits are developing their own &lsquo;Watchkeeper&rsquo; based on the Israeli Hermes drone. The Watchkeeper had its maiden flight in April of this year. In the meantime, the UK has to make do with Israeli UAVs that it hires by the hour. To date the UK has totted up 30,000 hours of use over Afghanistan. </p>
<p>
	Keen to put a bit of positive spin on the long-distance killers, M.O.D spokesman Lt Cdr Gerry Corbett said, &ldquo;<em>The public perception is either: they&rsquo;re spying on us; they&rsquo;re shooting at us; or they&rsquo;re not safe. We are trying to get rid of the phrase UAV. They are aircraft and they are piloted, albeit remotely</em>.&rdquo; </p>
<p>
	Although pilotless drone aircraft were used in the first Iraq War (and Israelis used drones against Lebanon and Syria as early as 1982), armed unmanned drones were first used in the Balkans in 1995, and were used for assassinations in Afghanistan in 2000. Afghanistan, the poorest country in Asia, has been subjected to attacks from machines inspired by Arnie films. The British satellite system which enables Nevada based RAF pilots to fly the skies over the &lsquo;Stan is even called Skynet &ndash; just what we need, heavily armed geeks. </p>
<p>
	Unveiled three weeks ago was BAE&rsquo;s contribution, the Taranis. Named after a Celtic Thunder God (Yep this is the real World of Warcraft) the Taranis is designed to go one step beyond remote control. This baby is intended to operate on autonomous &lsquo;hunter-killer&rsquo; missions. To make the aircraft &lsquo;more stealthy&rsquo; i.e. invisible to radar, the drone&rsquo;s bombs and missiles are carried internally. A small protest greeted it&rsquo;s unveiling with a banner saying &ldquo;The only good drone is a Vuvuzuela&rdquo;. </p>
<p>
	Fourteen U.S activists are facing charges after trespassing on an airbase used for piloting the drones. The activists entered the base&rsquo;s gates and refused to leave in protest against Creech Air Force Base&rsquo;s role as the little-known headquarters for U.S. military operations involving unmanned aerial vehicles, or drones, over Afghanistan, Iraq and Pakistan. They are awaiting trial. &ldquo;<em>It&rsquo;s just something that has clearly made killing so much easier</em>,&rdquo; said Iowa-based activist Brian Terrell, one of the so-called Creech 14 now facing charges. &ldquo;<em>Removing a combatant from the battlefield has a certain coldness, a weirdness about it. The idea that someone is sitting at a console at Creech and shooting missiles at people half a world away is very spooky</em>.&rdquo; </p>
<p>
	Because of this advanced weapons system, officials are able to claim that they have killed 14 top Al Qaeda leaders, such as Ilas Kashmir (supposed leader of Al Qaeda&rsquo;s Lakshar al-Zil - which translates as the &lsquo;The Shadow Army&rsquo;) and Beitullah Meshud (Pakistani Taliban leader). Meshud&rsquo;s position was quickly filled by his brother, Hakimullah, and its not certain whether or not Lakshar al-Zil has even ever existed. Ilas Kashmiri rather spoiled things by turning up alive in an interview a month later. Meanwhile civilian casualties mount. </p>
<p>
	The US has two separate fleets of UAVs- one run by the Airforce and one run by the CIA. While the airforce programme is run according to the usual (lax) rules of war, the CIA&rsquo;s parallel programme is entirely secret, and limited by neither borders nor congressional oversight. <br /> 
	Even the well publicised spat between Turkey and Israel wasn&rsquo;t enough to get in the way of Turkey hiring Israeli trainers to help Turkish drone operators target Kurdish rebels - &ldquo;Let them drown in their own blood&rdquo; as the Turkish pm tactfully put it recently. </p>
<p>
	With remarkably little public debate, drones have become the legacy weapon of the War on Terror. Drone strikes have allowed military action in countries the US is not officially at war with - notably in Pakistan, but also against perceived enemies in Yemen and Somalia. Targeted killing has become an official part of US foreign policy. </p>
<p>
	President Obama has dramatically increased the number of drone strikes, exceeding Bush&rsquo;s tally. Obama&rsquo;s first two uses of drones killed 80 at a wedding party. That was three days into his presidency. He&rsquo;s even joked about setting Predator drones on prospective suitors of his daughters. </p>
<p>
	US Department of Defense documents state that by 2015 they would like to see drones accounting for one third of missions flown by the US airforce. To army planners drones seem like a dream come true - low cost ($ 4million for a Reaper versus $17 million for an F16) and with almost no risk to US personnel- a boon to politicians concerned that dead troops mean unpopular wars. </p>
<p>
	*The website, <a href="http://dronewarsuk.wordpress.com/" target="_blank">http://dronewarsuk.wordpress.com/</a> is regularly updated with info about UAV warfare, with an emphasis on British involvement. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(881), 102); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(881);

				if (SHOWMESSAGES)	{

						addMessage(881);
						echo getMessages(881);
						echo showAddMessage(881);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


