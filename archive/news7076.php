<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 707 - 29th January 2010 - Crawley: No Boarders</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, no borders, immigration, detention centres, deportation" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 707 Articles: </b>
<p><a href="../archive/news7071.php">Lanarky In Action</a></p>

<p><a href="../archive/news7072.php">Citizen's Arrest Of The Week</a></p>

<p><a href="../archive/news7073.php">Edl: Gone To Potteries</a></p>

<p><a href="../archive/news7074.php">Calais: Gimme Shelter</a></p>

<p><a href="../archive/news7075.php">Festival Clampdown: Toking The Piss</a></p>

<b>
<p><a href="../archive/news7076.php">Crawley: No Boarders</a></p>
</b>

<p><a href="../archive/news7077.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th January 2010 | Issue 707</b></p>

<p><a href="news707.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CRAWLEY: NO BOARDERS</h3>

<p>
A grand coalition of No Borders activists and local nimbys managed to scupper the proposed transformation of the Mercure Hotel, Crawley into a deportation centre this Monday (25th). <br />  <br /> Activists from No Borders Brighton and No Borders London were alerted after planning permission was granted in principle for the building of the detention centre. Owners Arora International Hotels had planned to turn their 254 bed hotel into a Class C2A Secure Residential Institution. <br />   <br /> Around 20 No Borders activists showed up with banners and flyers, to be met by twice their number of security and police. Despite the overzealous security presence, the protesters were allowed into the meeting to spectate on the adrenalin-fuelled world of local council politics. One No Borders activist was nearly chucked out for daring to ask for a minute to speak on behalf of the detainees. <br />   <br /> The Arora hotels chain had decided to go it alone with this development. Rather than responding to demand set by the Home Office and the Borders Agency, the owners had decided to pre-empt on the assumption that the governments&rsquo; send-em-back plans would continue unabated. <br />   <br /> What they hadn&rsquo;t banked on was the opinions of the locals, who, not exactly motivated by love for their fellow man, were much more concerned about such fundamental issues as the appearance of the proposed perimeter fence and, of course, the crime that escaping Johnny Foreigners would commit. The application for planning permission was voted down 14 to 1. Described by one activists as &lsquo;kicking in an open door&rsquo; the strength of the local aesthetics lobby was powerful enough to ensure that narrow minded provincial concerns outdid draconian racial policies this time. <br />   <br /> One well prepared activist from the Close Campsfield campaign had managed to get a whole 3 minutes to explain the effect that detention has on young children and point out the findings of last November&rsquo;s &lsquo;National Inquiry into Children in Immigration Detention&rsquo;  that detailed the traumatic effects on children&rsquo;s mental health that detention invariably has. <br />   <br /> Leaving home empty handed, the Arora Hotels&rsquo; management were met by jeers and heckles from the No Borders mob. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=deportation&source=707">deportation</a>, <a href="../keywordSearch/?keyword=detention+centres&source=707">detention centres</a>, <a href="../keywordSearch/?keyword=immigration&source=707">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=707">no borders</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(639);
			
				if (SHOWMESSAGES)	{
				
						addMessage(639);
						echo getMessages(639);
						echo showAddMessage(639);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


