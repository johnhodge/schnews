<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 731 - 16th July 2010 - Boring Nonsense</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, rossport, oil industry, ireland, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 731 Articles: </b>
<p><a href="../archive/news7311.php">Against The Grain</a></p>

<b>
<p><a href="../archive/news7312.php">Boring Nonsense</a></p>
</b>

<p><a href="../archive/news7313.php">Gaza Defendants Appeal</a></p>

<p><a href="../archive/news7314.php">  On The Right Tracks</a></p>

<p><a href="../archive/news7315.php">Three Imprisoned In Iran</a></p>

<p><a href="../archive/news7316.php">Inside Schnews: Speak Activist Gets Ten Years</a></p>

<p><a href="../archive/news7317.php">  Outside Schnews: Joe Glenton Released</a></p>

<p><a href="../archive/news7318.php">No Oil Painting</a></p>

<p><a href="../archive/news7319.php">Fight Or Flight</a></p>

<p><a href="../archive/news73110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th July 2010 | Issue 731</b></p>

<p><a href="news731.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BORING NONSENSE</h3>

<p>
<p>
	<strong>DIRECT ACTION AT ROSSPORT HOTS UP</strong> </p>
<p>
	The &lsquo;Beat The Boreholes&rsquo; campaign &ndash; the latest chapter in the long-term protests to stop Shell building an offshore gas pipeline and refinery on the west coast of Ireland at Rossport (see <a href='../archive/news730.htm'>SchNEWS 730</a>) &ndash; got off to a dramatic start this week with several sea actions. </p>
<p>
	At 7pm on Wednesday night (14th) a group of Shell-to-Sea protesters took to the waters of Broadhaven Bay in kayaks and rafts to stop a borehole drilling platform being brought in. They were met by a flotilla of five Garda water boats, with 16 coppers on board. Kayaks were capsized by police, with one protester who managed to get near the platform, Eoin Lawless, being tipped overboard. Garda jumped into the water, grabbed him and dragged him onto their boat, where he was violently manhandled with one policeman throttling him with a hand to the throat for 90 seconds. According to Lawless, the copper told him, &ldquo;<em>I have your last breath in my hands</em>.&rdquo; Afterwards Lawless called for human rights observers to be present at the site. </p>
<p>
	The following morning at 7am, more protesters were back on the water to continue to protect the bay from drilling. Again the five Garda boat with 16 on board were out, as well as 10 security boats. </p>
<p>
	Again the police overturned their small boats, and three kayaks and the ribbed-inflatable boat were seized and two protesters arrested for public order offences. </p>
<p>
	The 80 planned boreholes into the estuary are to survey the ground for the intended gas pipeline, which will go inland to the proposed refinery. Protests will continue and all are urged to get to Rossport to stop the boreholes. </p>
<p>
	* See <a href="http://www.rossportsolidaritycamp.org," target="_blank">www.rossportsolidaritycamp.org,</a> <a href="http://www.shelltosea.com" target="_blank">www.shelltosea.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(860), 100); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(860);

				if (SHOWMESSAGES)	{

						addMessage(860);
						echo getMessages(860);
						echo showAddMessage(860);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


