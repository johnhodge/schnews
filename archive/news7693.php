<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 769 - 29th April 2011 - Hove for the Best</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, hove, squatting, m26, students" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 769 Articles: </b>
<p><a href="../archive/news7691.php">Stoking The Fires</a></p>

<p><a href="../archive/news7692.php">Tes-cop-oly</a></p>

<b>
<p><a href="../archive/news7693.php">Hove For The Best</a></p>
</b>

<p><a href="../archive/news7694.php">Sticking To The Task</a></p>

<p><a href="../archive/news7695.php">Georgian Facade</a></p>

<p><a href="../archive/news7696.php">More Benefit Cuts</a></p>

<p><a href="../archive/news7697.php">Climate Camp: Lewes</a></p>

<p><a href="../archive/news7698.php">Ahava Got News For You</a></p>

<p><a href="../archive/news7699.php">Manning The Barricade</a></p>

<p><a href="../archive/news76910.php">I Can't Believe It's Not Mayday</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th April 2011 | Issue 769</b></p>

<p><a href="news769.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HOVE FOR THE BEST</h3>

<p>
<p>  
  	It was a triple whammy Wednesday (27th) as Met Police violently raided 3 squats in Hove, East Sussex at 7am. They made four arrests for abstraction (nicking &lsquo;leccy), two for possession of drugs and one for conspiracy to commit violent disorder. This disorder was alleged to have happened at the March 26th TUC demo (see <a href='../archive/news765.htm'>SchNEWS 765</a>) and the police had arrived with spotter cards of those they wanted.  </p>  
   <p>  
  	 <br />  
  	As the first six were handed over to Sussex police, the one man accused of violent disorder was driven up to London. Once there he was booked into custody and briefly interviewed, only to be released without charge. The squats have now all been illegally evicted; &lsquo;evidence&rsquo; including several laptops has been seized and much of the occupants stuff was either broken or sealed up in their former homes.  </p>  
   <p>  
  	SchNEWS spoke exclusively to the arrestee: &ldquo;<em>There were four vans of Met police in full riot gear at [our house], plainclothes officers pointed me out and I was arrested</em>.&rdquo; After being driven all the way to the Big Smoke two &ldquo;useless&rdquo; junior officers asked such inane questions as &ldquo;what do you do in your squat?&rdquo; Faced with the standard &ldquo;no comment&rdquo; line they then sent him on his (not so) merry way, with no charges and no home.  </p>  
   <p>  
  	With the royal wedding (didn&rsquo;t they see the tea-towels?) and May Day just around the corner, police apparently seemed to think that the anarchist high command is currently based in sleepy Hove. Given that the only arrest relating to the warrants resulted in nothing more than an unwanted lift to London it seems police intelligence is even lower than previously imagined. Alternatively this was a fishing expedition intended to seize as much material as possible, while pissing off and making homeless many known &ldquo;troublemakers&rdquo;.  </p>  
   <p>  
  	As SchNEWS went to press on Thursday (28th) morning more squats were being raided in London- including the Grow Heathrow community project and the Rat Star and Offmarket&nbsp; social centres.  </p>  
   <p>  
  	Meanwhile, on Tuesday 26th, Met Police charged a further 11 people (including Alfie Meadows who had&nbsp; brain surgery after being hit by a truncheon) for alleged offences committed during the tuition fees protests in on 9th December. All of whom have been bailed away from Westminster for the weekend; wedding fever (and wedding budget) probably the real reason for the recent crackdown. But will all this guarantee a fairytale on Friday? If you read the mainstream press, you&rsquo;d be forgiven for thinking that anarchist plots to re-enact V for Vendetta are in final preparation &ndash; but no-one&rsquo;s sent us our masks and outfits yet...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1196), 138); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1196);

				if (SHOWMESSAGES)	{

						addMessage(1196);
						echo getMessages(1196);
						echo showAddMessage(1196);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


