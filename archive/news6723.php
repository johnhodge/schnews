<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 672 - 17th April 2009 - Pathological Liars</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, ian thomlinson, g20 summit, london, police, met police, forward intelligence team" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 672 Articles: </b>
<p><a href="../archive/news6721.php">Easy, Tiger...</a></p>

<p><a href="../archive/news6722.php">Day Of Anger</a></p>

<b>
<p><a href="../archive/news6723.php">Pathological Liars</a></p>
</b>

<p><a href="../archive/news6724.php">Right To Roma</a></p>

<p><a href="../archive/news6725.php">Too Cruel For School</a></p>

<p><a href="../archive/news6726.php">W-hacked In Jail</a></p>

<p><a href="../archive/news6727.php">Mallets Mallet</a></p>

<p><a href="../archive/news6728.php">Ray Of Blight   </a></p>

<p><a href="../archive/news6729.php">Getting On Our Tits</a></p>

<p><a href="../archive/news67210.php">Fools Gold</a></p>

<p><a href="../archive/news67211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th April 2009 | Issue 672</b></p>

<p><a href="news672.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PATHOLOGICAL LIARS</h3>

<p>
It didn&#8217;t just take a man&#8217;s death &#8211; it took incontrovertible proof of that man getting an unprovoked beating for the mainstream media to raise their heads out of their Islington ghetto and take a real look at 21st century British policing. Following the death of Ian Tomlinson at the G20 protests there&#8217;s been plenty of coverage of the violence inflicted by cops. But while the liberal media expresses &#8220;shock&#8221; SchNEWS knows that police violence is par for the course. When a &#8220;public order&#8221; situation arises the police are always keen to exert their authority in a zealous manner. <br />
 <br />
Of course there&#8217;s now a media storm around police brutality and the scrutiny has to be welcomed, but are we going get palmed off with the blame being pinned on dodgy operational decision-making and a few &#8216;bad apples&#8217;? Are we going to see another de Menezes whitewash? Violence by the police on the scale we saw at the G20 must have been sanctioned right from the top. The truth is that Tomlinson was merely unlucky &#8211; it could have been any one of us. <br />
 <br />
Police used a compliant media to ratchet up the temperature before the summit with Commander Simon O&#8217;Brien boasting: &#8220;<i>We&#8217;re up for it and we&#8217;re up to it.</i>&#8221; The name given to the police operation, &#8220;Glencoe&#8221; refers to an infamous 17th century Scottish massacre. <br />
 <br />
There was also the &#8220;G20 Terror Plot&#8221; - more media spin about nothing &#8211; the weekend before the G20. A Plymouth man was arrested for graffiti, his house searched and police allegedly found imitation guns and fireworks.  All his housemates were arrested and held for days. Eventually the five were either released without charge or released on police bail &#8211; there was no evidence of terrorism. <br />
 <br />
On the day the Met as predicted (by us anyway) kettled the crowd. Contrary to mainstream media &#8211; the kettle is not a new tactic, having been used since 1999 at least. By the end of the day bands of police were roaming the centre of London attacking people at will. In almost every piece of footage surfacing of police violence there are officers in the characteristic blue-topped uniform of the Forward Intelligence Team see (<a href='../archive/news639.htm'>SchNEWS 639</a>). <br />
 <br />
The climate camp at Bishopsgate was viciously broken up and Ian Tomlinson was tragically caught up in it on his way home from work. Immediately the police media machine went spluttering into gear, claiming no contact with him &#8211; a lie; that he died solely of a heart attack &#8211; another lie. The pathologist involved had been reprimanded by the General Medical Council for dodgy verdicts, and a second autopsy was carried out by the Forensic Pathology Services which usually deal with suspicious deaths. Another lie is the claim that the police medicos treating Ian at the scene came under a hail of bottles thrown at them &#8211; when in fact one plastic bottle was thrown, then no more as protesters realised someone was injured. <br />
 <br />
Utterly unfazed by the criticism, the Met have been back in the headlines for battering pro-Tamil protesters (see front page) &#8211; the state&#8217;s bully boys and executioners know they&#8217;re above the law. They&#8217;ve even attacked vigils for Ian Tomlinson - see YouTube. <a href="http://www.youtube.com/watch?v=6THfDGy1hN4" target="_blank">www.youtube.com/watch?v=6THfDGy1hN4</a> <br />
 <br />
But what the media have totally failed to question is why was there a police kettle around an entirely peaceful demo on the day of the vigil. The video shows a man trying to leave the cordon and being manhandled. A woman outside the cordon objects to the behaviour and is back-handed across the face and batoned around the legs. Police then invoked Section 14 of the Public Order Act to move press away from the scene &#8211; sadly most press obeyed this order rather then risk arrest &#8211; so much for speaking truth to power. <br />
 <br />
We can&#8217;t rely on the media or the IPCC to protect us from the boys in black. The police are the front-line of the corporate state. The only question is how long are we gonna put up with this shit?
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=forward+intelligence+team&source=672">forward intelligence team</a>, <a href="../keywordSearch/?keyword=g20+summit&source=672">g20 summit</a>, <span style='color:#777777; font-size:10px'>ian thomlinson</span>, <a href="../keywordSearch/?keyword=london&source=672">london</a>, <a href="../keywordSearch/?keyword=met+police&source=672">met police</a>, <a href="../keywordSearch/?keyword=police&source=672">police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(286);
			
				if (SHOWMESSAGES)	{
				
						addMessage(286);
						echo getMessages(286);
						echo showAddMessage(286);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>