<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 748 - 18th November 2010 - Complete Hissy Fit</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, fitwatch, internet, met police, students, surveillance" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 748 Articles: </b>
<b>
<p><a href="../archive/news7481.php">Complete Hissy Fit</a></p>
</b>

<p><a href="../archive/news7482.php">  City Subs Picket Shame</a></p>

<p><a href="../archive/news7483.php">Degrees Of Resistance</a></p>

<p><a href="../archive/news7484.php">France: Screws Loose</a></p>

<p><a href="../archive/news7485.php">Love To Haiti</a></p>

<p><a href="../archive/news7486.php">There Will Be Mud</a></p>

<p><a href="../archive/news7487.php">New Feature: Dyer Warnings</a></p>

<p><a href="../archive/news7488.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 18th November 2010 | Issue 748</b></p>

<p><a href="news748.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COMPLETE HISSY FIT</h3>

<p>
<p>
	<strong>AS FORWARD INTELLIGENCE BOSS STUPIDLY TRIES TO SILENCE FITWATCH</strong> </p>
<p>
	Still smarting from the failure to contain the student rioters in central London (see <a href='../archive/news747.htm'>SchNEWS 747</a>), the Met are peevishly taking out their anger against an old enemy. On Monday evening the hosts of the FIT WATCH website were asked to close it down on the grounds of it &ldquo;<em>being used to undertake criminal activities&rdquo;. In a letter from the MET, the company Justhost.com were warned that the website was being used in an attempt &ldquo;to pervert the course of justice</em>&rdquo;. The DI demanded that the site be taken down for &lsquo;twelve months&rsquo;. Deciding caution was the better part of valour, Justhost decided to take it down. </p>
<p>
	In fact all that the FITWATCH site had done was hand out sensible and timely advice to those who caught up in the right wing witch hunt for the brave folks who trashed Tory HQ. The Telegraph has run a shop-a-rioter feature and the SUN has named individuals they claim were involved in violence. David Cameron has demanded that the &lsquo;full weight of the law&rsquo; be brought against those involved. Given the hammering that the defendants in the Gaza protests (see <a href='../archive/news710.htm'>SchNEWS 710</a>) took, anyone who has the slightest fear that they might get picked up would do well to heed FITWATCH&rsquo;s advice. </p>
<p>
	<strong>FIT TO BURST</strong> </p>
<p>
	For two days then, cyber-cops stopped a few people from looking at the FITWATCH site. SchNEWS tracked one of the not particularly shadowy figures behind FITWATCH. &ldquo;<em>It&rsquo;s been been real success. We&rsquo;ve reached an audience we&rsquo;ve never been able to before - They forced us to take the website down - but now we&rsquo;re in the Guardian, BBC</em>&rdquo; (even SchNEWS!) </p>
<p>
	&ldquo;The advice on the site was almost immediately reprinted on dozens of sites. And in any case the site is now back up under its own steam, with a different host and a secure server. &ldquo;<em>This pathetic attempt has failed miserably. Within minutes of networking what had happened, people were re-publishing the post anywhere and everywhere. There are now over 100 sites carrying the original post &ndash; we haven&rsquo;t managed to count them all. We have been overwhelmed by the support and solidarity and send massive thanks to everyone who&rsquo;s offered to help and reposted the information</em>. </p>
<p>
	&ldquo;<em>By making vague threats of illegality to a net hosting company the cops were able to bypass such boring democratic details as a court of law. Although the communication was sent by the Met&rsquo;s e-crime unit, according to FITWATCH the letter originated from one Acting Detective Inspector Will Hodgeson, who works for CO11, the METs public order intelligence unit, who run the notorious FIT teams (see <a href='../archive/news639.htm'>SchNEWS 639</a>). &rdquo;They&rsquo;ve acted extra-legally - it&rsquo;s one thing to take us to court but this was a way of getting around that. It was an obvious attempt to give us a hard time - the usual harassment, bullying and disruption. It was a way of getting to us outside the court system</em>.&rdquo; </p>
<p>
	<strong>QUICK FIT</strong> </p>
<p>
	FITWATCH as a practice arose out of the need to confront the chilling effect constant police surveillance was having on activism. Within a year of starting, people adopting the tactics had forced police cameramen away to the fringes of our gatherings. </p>
<p>
	This was all instrumental in focussing public and media attention on police interaction with demonstrators. But by publishing the names, numbers and photographs of the country&rsquo;s political police, its no surprise that FITWATCH have made enemies amongst the boys in blue. </p>
<p>
	Our interviewee said, &ldquo;<em>This isn&rsquo;t the first time that the FITWATCH blog has come up. The cops think they have a pretty good idea who&rsquo;s behind it. The blog has twice been introduced as evidence in Obstruct Police court cases to do with actions against police surveillance, but it has been ruled inadmissible. Certainly people have been asked about it in interviews. They&rsquo;ve been thinking about what to do about it for a while</em>&rdquo;. </p>
<p>
	<em><strong>And for entertainment purposes only.. here&rsquo;s that controversial advice in full:</strong></em> </p>
<p>
	51 people have been arrested so far, and the police have claimed they took the details of a further 250 people in the kettle using powers under the Police Reform Act. There may be more arrests to come. </p>
<p>
	Students who are worried should consider taking the following actions: </p>
<p>
	* If you have been arrested, or had your details taken &ndash; contact the legal support campaign. As a group you can support each other, and mount a coherent campaign. </p>
<p>
	* If you fear you may be arrested as a result of identification by CCTV, FIT or press photography... </p>
<p>
	<strong>DON&rsquo;T </strong>panic. Press photos are not necessarily conclusive evidence, and just because the police have a photo of you doesn&rsquo;t mean they know who you are. </p>
<p>
	<strong>DON&rsquo;T</strong> hand yourself in. The police often use the psychological pressure of knowing they have your picture to persuade you to &lsquo;come forward&rsquo;. Unless you have a very pressing reason to do otherwise, let them come and find you, if they know who you are. </p>
<p>
	<strong>DO</strong> get rid of your clothes. There is no chance of suggesting that the person in the video is not you if the clothes worn on film have been found in your wardrobe. Get rid of ALL clothes you were wearing at the demo, including YOUR SHOES, your bag, and any distinctive jewellery you were wearing at the time. Yes, this is difficult, especially if it is your only warm coat or decent pair of boots. But it will be harder still if finding these clothes in your flat gets you convicted of violent disorder. </p>
<p>
	<strong>DON&rsquo;T </strong>assume that because you can identify yourself in a video, a judge will be able to as well. &lsquo;That isn&rsquo;t me&rsquo; has got many a person off before now. </p>
<p>
	<strong>DO</strong> keep away from other demos for a while. The police will be on the look-out at other demos, especially student ones, for people they have put on their &lsquo;wanted&rsquo; list. Keep a low profile. </p>
<p>
	<strong>DO</strong> think about changing your appearance. Perhaps now is a good time for a make-over. Get a haircut and colour, grow a beard, wear glasses. It isn&rsquo;t a guarantee, but may help throw them off the scent. </p>
<p>
	<strong>DO</strong> keep your house clean. Get rid of spray cans, demo related stuff, and dodgy texts / photos on your phone. Don&rsquo;t make life easy for them by having drugs, weapons or anything illegal in the house. </p>
<p>
	<strong>DO</strong> get the name and number of a good lawyer you can call if things go badly. The support group has the names of recommended lawyers on their site. Take a bit of time to read up on your rights in custody, especially the benefits of not commenting in interview. </p>
<p>
	<strong>DO</strong> be careful who you speak about this to. Admit your involvement in criminal damage / disorder ONLY to people you really trust. </p>
<p>
	<strong>DO</strong> try and control the nerves and panic. Waiting for a knock on the door is stressful in the extreme, but you need to find a way to get on with business as normal. Otherwise you&rsquo;ll be serving the sentence before you are even arrested. <a href="http://</p>
<p>
	www.fitwatch.org.uk" target="_blank"></p>
<p>
	www.fitwatch.org.uk</a> </p>
<p>
	* The defence campaign is at: <a href="http://nov10.wordpress.com/" target="_blank">http://nov10.wordpress.com/</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1013), 117); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1013);

				if (SHOWMESSAGES)	{

						addMessage(1013);
						echo getMessages(1013);
						echo showAddMessage(1013);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


