<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - The ITT Crowd</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, anti-war, arms trade, edo, edo decommissioners, itt, smash edo" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<p><a href="../archive/news7275.php">Turkey Shoot</a></p>

<b>
<p><a href="../archive/news7276.php">The Itt Crowd</a></p>
</b>

<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>

<p><a href="../archive/news7278.php">Hoto Finish</a></p>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE ITT CROWD</h3>

<p>
<p>
	As the EDO Decommissioners trial goes into its second week, the defence celebrated their first victory when the case against one of the defendants was dropped. </p>
<p>
	Rosa Bellamy walked free on Tuesday (15th) when the judge ruled the prosecution&rsquo;s evidence lacked the substance it needed to prove she was part of a conspiracy to cause damage and directed the jury to find her not guilty. He said, &ldquo;<em>There is no guilt by association. The fact that she knows some of the co-defendants is neither here nor there</em>.&rdquo; The defendant&rsquo;s silver dress brandished by the prosecution as a main piece of evidence didn&rsquo;t reel in the desired effect, as it was deemed not typical of the clothing anyone would wear if they were on their way to break into a factory. </p>
<p>
	This week also saw Paul Hills, managing director of EDO/MBM/ITT finish his statement, followed by the wrap up of the prosecution case. Hills was accused of lying by the defence and kindly warned by the judge of the risks of perjuring himself. </p>
<p>
	It is significant to note that the jury will be given access as background briefing into the case of a brief summary of the Goldstone report - an independent international fact finding mission to investigate violations of international human rights law and international humanitarian law in the Occupied Territories, which investigated last year&rsquo;s military assault on the Gaza Strip. The defendants told the jury it highlighted how the democratic processes had failed to deal with the situation, which shows political inadequacy to deal with problems that will affect profit margins. They argued it provided a strong case for a moral and legal obligation to stop a greater crime through non-violent direct action in order to save a life. </p>
<p>
	On Thursday (17th), the defence case entered the arena with the first of the decommissioners to take the stand. To the SchNEWS court reporter, Ornella Saibene seemed clear and thorough throughout without getting ruffled by the prosecution&rsquo;s cross examinations. Her answers came across with conviction and honesty. Proceedings continue at 10.30am Friday (18th), supporters and the public are welcome to attend inside and outside the court as the trial of the remaining decommissioners continues. </p>
<p>
	Decommissioner Elijah James Smith will remain on remand for the duration of the trial. Letters of support are welcomed at the usual address - A3186AM, HMP Lewes, 1 Brighton Rd, Lewes, East Sussex, BN7 1EA. <br /> 
	 <br /> 
	<strong>* For updates</strong> see <a href="http://decommissioners.co.uk" target="_blank">http://decommissioners.co.uk</a> <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> </p>
<p>
	<strong>* For Goldstone report</strong> see <a href="http://www2.ohchr.org/english/bodies/hrcouncil/specialsession/9/docs/UNFFMGC_Report.pdf" target="_blank">http://www2.ohchr.org/english/bodies/hrcouncil/specialsession/9/docs/UNFFMGC_Report.pdf</a> </p>
<p>
	&nbsp; </p>
<p>
	<u><strong>MORE TRIALS AND TRIBULATIONS</strong></u> </p>
<p>
	<strong>* Four Christian peace activists</strong> who shut down a secret Australian military base have had charges against them dismissed by a magistrate. The members of the Bonhoeffer Peace Collective had swum to the base on Swan Island in March. They then infiltrated the base and spent several hours shutting down a switchboard and a satellite, causing a lock down on the base and interrupting Australian warmongering in Afghanistan. The charges of tresspass were dismissed by the magistrate. </p>
<p>
	According to one of the four, Simon Reeves, &ldquo;(The) magistrate agrees sanctity of life is more important than unjust laws&rdquo;. For a video of the group explaining their actions see <a href="http://paceebene.org/blog/jarrod-mckenna/breaking" target="_blank">http://paceebene.org/blog/jarrod-mckenna/breaking</a> <br /> 
	 <br /> 
	<strong>* Nine Plane Stupid activists</strong> are also planning to defend themselves on the grounds that they were acting to prevent a bigger crime &ndash; the crime of escalating climate change. The nine face charges of breach of the peace and vandalism after they shut down Aberdeen airport for two and a half hours in March. The trial began on Monday (14th). See <a href="http://www.planestupid.com" target="_blank">www.planestupid.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(822), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(822);

				if (SHOWMESSAGES)	{

						addMessage(822);
						echo getMessages(822);
						echo showAddMessage(822);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


