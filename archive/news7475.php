<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 747 - 11th November 2010 - Squat Crackdown, Bristol Fashion</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, autonomous spaces, bristol, squatting, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 747 Articles: </b>
<p><a href="../archive/news7471.php">Losing Their Faculties</a></p>

<p><a href="../archive/news7472.php">Raven Mad For It</a></p>

<p><a href="../archive/news7473.php">Schmovies Dvd Launch</a></p>

<p><a href="../archive/news7474.php">Nukes? Gorle-blimey!</a></p>

<b>
<p><a href="../archive/news7475.php">Squat Crackdown, Bristol Fashion</a></p>
</b>

<p><a href="../archive/news7476.php">All Party'd Out</a></p>

<p><a href="../archive/news7477.php">Got The Hump Back</a></p>

<p><a href="../archive/news7478.php">Mexico: Day Of The 10,000 Dead</a></p>

<p><a href="../archive/news7479.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 11th November 2010 | Issue 747</b></p>

<p><a href="news747.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUAT CRACKDOWN, BRISTOL FASHION</h3>

<p>
<p>
	Autonomous society in Bristol is under serious threat as eviction faces the Stokes Croft Free Shop and Emporium, Frowning Motorcycle events space, Smiling Chair anarchist library, and three other squat houses. In a move that will effectively mean making 60-70 people homeless, the bailiffs have been getting busy all around Stokes Croft. </p>
<p>
	The anarchist library was hit by &lsquo;heavies&rsquo; on Monday 8th,who started putting metal sheeting over doors and windows whilst people were still inside. The illegal bailiffs were quickly dispatched by the coppers, having been called in by the occupants, for attempting to imprison the squatters inside. Later the same night the Frowning Motorcycle events space, which is also home to 5 people, was targeted by more illegal bailiffs trying to crowbar their way in. Luckily, the space was secure and they left without gaining access. </p>
<p>
	Cheltenham Mansions, which houses around 40 people, has been served with an eviction notice, and Telepathic Heights and Magpie squats - collectively housing another 15 or so - have also been served with papers and are waiting to get their eviction notices any day. </p>
<p>
	The Free Shop and Emporium art gallery had their first court hearing for the eviction proceedings on Monday (3rd), despite absentee landlords Jerwood Trading Limited making no move to use, upkeep, repair, or develop the buildings in their seventeen years of ownership. This neglect saw the buildings being placed on the &lsquo;at risk&rsquo; register by the council who also issued the owners with a compulsory purchase order. </p>
<p>
	The buildings have been transformed in the two-and-a-half years of squatted occupation,with shop fronts built from scratch, collapsing floors repaired, hole-ridden roofs patched up, windows refitted, asbestos removed, beds built and sewing machines installed. This effort by a group of determined individuals has turned two urban eyesores into thriving community hubs hosting exhibitions, film nights, discussion groups and skill-shares, not to mention providing clothing, books and household supplies to those who need it most, all free of charge. </p>
<p>
	The judge adjourned the hearing until December, to allow for the possibility of viable arguments against the owner&rsquo;s claim of possession. </p>
<p>
	The community is determined to keep these free spaces occupied and stop the buildings falling empty and useless again. All evictions will be resisted and new spaces will be taken in the next week or two. </p>
<p>
	In the meantime, for those in the neighbourhood, get down to the Freeshop, open every Thursday, Friday and Saturday from 2-5pm. Or hit the benfit gig at the Frowning Motorcyle at 8pm on 26th November for punk, local cider, and involvement. </p>
<p>
	* See <a href="http://emporium37.wordpress.com" target="_blank">http://emporium37.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1008), 116); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1008);

				if (SHOWMESSAGES)	{

						addMessage(1008);
						echo getMessages(1008);
						echo showAddMessage(1008);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


