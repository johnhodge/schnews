<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 653 - 31st October 2008 - Any Port In A Storm</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, israel, free gaza movement, ss dignity, ism, international solidarity movement" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.peasantsrevolt.org/"><img 
						src="../images_main/peasants-revolt-banner.jpg" 
						width="465" 
						height="90" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 653 Articles: </b>
<b>
<p><a href="../archive/news6531.php">Any Port In A Storm</a></p>
</b>

<p><a href="../archive/news6532.php">Olive & Let Die</a></p>

<p><a href="../archive/news6533.php">Christiana Almighty</a></p>

<p><a href="../archive/news6534.php">Oax Murderers</a></p>

<p><a href="../archive/news6535.php">Through The Roof</a></p>

<p><a href="../archive/news6536.php">Shipley Shape</a></p>

<p><a href="../archive/news6537.php">Shock And A.w.e.</a></p>

<p><a href="../archive/news6538.php">You Cannot Be Syria's</a></p>

<p><a href="../archive/news6539.php">653 And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/653-us-election-lg.jpg" target="_blank">
													<img src="../images/653-us-election-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 31st October 2008 | Issue 653</b></p>

<p><a href="news653.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>ANY PORT IN A STORM</H3>

<p>
<b>AS INTERNATIONAL SOLIDARITY TAKES TO THE HIGH(LY DANGEROUS) SEAS</b><br />
<br />
Gaza siege breakers have done it again! The SS Dignity carrying 27 crew and passengers arrived in Gaza at 8:10am local time, in spite of Israeli threats to stop them &#8211; all part of their ongoing blockade and siege of the whole coastline. In the pouring rain, the boat pulled into port amid cheers from the people of Gaza and tears from the passengers. Crew included Nobel Peace Prize winner Mairead McGuire. The first boat sent by the Free Gaza movement arrived back in August (<a href="news644.htm">SchNEWS 644</a>). Both voyages were undertaken despite threats of forcible boarding and deportation from the Israeli military.<br />
<br />
One of the organizers, Huwaida Arraf cheered, &#8220;<i>Once again we&#8217;ve been able to defy an unjust and illegal policy while the rest of the world is too intimidated to do anything. Our small boat is a huge cry to the international community to follow in our footsteps and open a lifeline to the people of Gaza.</i>&#8221;<br />
<br />
Along with it&#8217;s crew, the SS Dignity was carrying half a tonne of medical supplies &#8211; desperately needed in the besieged Strip. They were taken immediately to Gaza&#8217;s main hospital. The Free Gaza movement said, &#8220;<i>For the second time, we have demonstrated that the might of the Israeli navy is no match for a small boat of human rights activists determined to call to the attention of the world the occupation of the people of Gaza</i>&#8221;.<br />
<br />
The Palestinians of Gaza have survived isolation, siege, bombardment and starvation that have brought the living conditions in Gaza to (in the words of the UN) &#8216;the worst levels since 1967 (the start of the Occupation). Since March, when Israeli attacks on Gaza killed 120 people in a weekend to no noticeable advantage to the Israeli state/military (it&#8217;s often hard to tell the difference), a ceasefire has slightly improved conditions in the Strip.<br />
<br />
Unable to defeat Hamas in Gaza but needing to stop rocket attacks, Israel reluctantly agreed to a one sided ceasefire with the Hamas movement- basically saying &#8220;stop the rocket attacks, beg nice and we might let people eat today.&#8221; Now Gaza is allowed some more medicine, some more food and other supplies - if they play ball. But everything is severely restricted - people still die for lack of medical treatment, and most industry has ground to a halt. <br />
<br />
<b>ONE ISM TO BELIEVE IN</b><br />
<br />
Some of the crew from the first voyage stayed on and immediately re-established the Gaza branch of the International Solidity movement (see <a href="http://www.palsolidarity.org" target="_blank">www.palsolidarity.org</a>). They are now engaged in human rights work across the whole of the Gaza Strip. SchNEWS spoke to one of activists about their work and the situation in the Strip.<br />
<br />
&#8220;<i>Despite the supposed &#8216;ceasefire&#8217; the siege is ongoing &#8211; the price of food is sky-high and families are slipping deeper into poverty. When I was first in Rafah [A Gazan town on the Egyptian border] you never saw people begging in the streets &#8211; now there are plenty. You now see children trying to sell things in the street. Since the ceasefire a few more supplies are making it in but the slide towards mass poverty and hunger is sickening. It&#8217;s just the slow strangulation of an entire population.<br />
<br />
The world ignores it because it&#8217;s not one massive event. It&#8217;s not like that because it&#8217;s slow and gradual. The siege affects everyone in the Strip &#8211; it&#8217;s collective punishment on a massive scale, outdoing even what&#8217;s being inflicted in the West Bank.</i>&#8221;<br />
 <br />
ISM activists are now engaged in non-violent direct action in the Strip &#8211; accompanying those who find themselves under threat of Israeli military violence, mainly farmers and fishermen. <br />
<br />
&#8220;<i>Farmers whose land borders the green line [Israel&#8217;s imposed border] are under constant threat. The Israeli Defence Forces (IDF) have created a buffer zone on the land border. They&#8217;ve bulldozed areas hundreds of metres into supposedly Palestinian areas. They&#8217;ve destroyed absolutely everything, uprooting literally thousands of olive and fruit trees. There were massive incursions on 1st of May in Khan Younis &#8211; destroying olive trees, greenhouses and even barns full of sheep. They&#8217;ll frequently shoot farmers for going into their own land. By walking with these people we&#8217;re able to give them a small amount of protection.</i>&#8221;<br />
<br />
40,000 people depend on the fishing industry in Gaza, and yet anyone who ventures out into what are technically Palestinian waters find themselves at risk of attack from IDF gunboats. Israel has unilaterally enforced a six-mile limit for seaborne trips from Gaza. <br />
<br />
 &#8220;<i>The limit [for seaborne trips from Gaza] has supposedly been set at six miles but in reality any Palestinian boat can be considered fair game. One eighteen-year-old who was out with his friend in a rowing boat just hundreds of metres from the Rafah beach was shot in the leg when they came under &#8216;targeted sniper fire&#8217;. He may lose his leg. Many other fishermen have been shot.</i>&#8221;<br />
<br />
&#8220;<i>So we decided to start going out on the boats. They have a right to be in those waters. Since we started accompanying the fishing boats &#8211; there has been a 200% increase in the catch landed. When internationals are aboard the trawlers the Israeli gunboats are more likely to just fire warning shots and use tear gas &#8211; giving the boats a chance to get into deeper waters where fish are more plentiful. This is a really effective form of direct-action.</i>&#8221;  Footage of the action can be seen <a href="http://www.youtube.com/watch?v=aZBwcPcAeFA&feature=related" target="_blank">http://www.youtube.com/watch?v=aZBwcPcAeFA&feature=related</a><br />
 <br />
&#8220;<i>With the boats from Cyprus we meant to break the siege &#8211; symbolically at first, but now the Free Gaza movement aims to literally break it - providing a lifeline of hope for Gaza.</i>&#8221; <br />
<br />
* See <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> and <a href="http://www.electronicintifada.net" target="_blank">www.electronicintifada.net</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=palestine&source=653">palestine</a>, <a href="../keywordSearch/?keyword=israel&source=653">israel</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
			
				if (SHOWMESSAGES)	{
				
						addMessage(102);
						echo getMessages(102);
						echo showAddMessage(102);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>