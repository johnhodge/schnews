<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 758 - 11th February 2011 - The Withdrawal Method</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, libraries, austerity, cuts, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 758 Articles: </b>
<b>
<p><a href="../archive/news7581.php">The Withdrawal Method</a></p>
</b>

<p><a href="../archive/news7582.php">Edl Go To Hell, Well Luton</a></p>

<p><a href="../archive/news7583.php">Egypt: Tahrir We Go Again</a></p>

<p><a href="../archive/news7584.php">Greece: Hunger Strikes</a></p>

<p><a href="../archive/news7585.php">Greece: Trash Talk</a></p>

<p><a href="../archive/news7586.php">Harper Scarper</a></p>

<p><a href="../archive/news7587.php">Shopped Cops Dropped</a></p>

<p><a href="../archive/news7588.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/758-library-lg.jpg" target="_blank">
													<img src="../images/758-library-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th February 2011 | Issue 758</b></p>

<p><a href="news758.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE WITHDRAWAL METHOD</h3>

<p>
<p>
	As noisy protests continued against tax avoidance by big business and cuts in education and benefits another, altogether quieter, national campaign took off this week at our most unsung of public services: the libraries. </p>
<p>
	You can&rsquo;t imagine that Waterstones, WHSmiths, Amazon and others mind too much that libraries were forged from great social ideals. Not only is encouraging universal education and literacy good econincally for society (potential talent can rise from anywhere to realise its potential and then productively benefit society) it&rsquo;s availability to serve as community hub, public space, creche and more to those without access to alternatives make it an all round force for social good. Investing equally in all, for the future of all, paid for from the wealth of all. But such aspirations are like, so last millennium. and the shiny new &lsquo;Big Society&rsquo; has no need for such rubbish. </p>
<p>
	Across the country, councils faced with savage funding cuts are putting libraries top of their list of low hanging fruit and axing swathes of library services. In some places they are offering the meagre crumb of offering to give over premises to volunteers to take over and run what non-funded services they can. </p>
<p>
	And despite the gargantuan (learnt that word in a book I borrowed from a library) sums of taxpayers money shovelled into the banks to stop them going bust (effectively later ending up as bonuses to smug bankers claiming they&rsquo;re successfully earning their way to &lsquo;recovery&rsquo;), or the billions lost in cosy corporate tax-avoidance, or wasted on mega IT projects etc etc - the government is happy to sit by and force councils to retreat from core social support and non life-essential services. </p>
<p>
	But the book-reading masse s are queuing up to resist. OK, so it ain&rsquo;t smashing up the treasury, but a wave of imaginative actions in libraries have been taking place up and down the UK, instigated for the most part by some unlikely direct action comrades. </p>
<p>
	Saturday 5th saw a national day of action against the mass closures that are threatening nearly 500 services, including mobile libraries. Huge numbers of people came out in even the smallest of towns, many libraries seeing queues down the street with crowds of hundreds coming to max out their cards and make overdue demonstrations of visible support for these valuable institutions. The turnouts were best attended by those which use the libraries most, families with young children and the elderly, with plenty of those on lower incomes - set to be some of the most affected by benefit cuts - and others who never use &lsquo;em but believe in the principle. </p>
<p>
	Most of the day&rsquo;s actions mirrored the work of borrowers in Stony Stafford, Milton Keynes back in January. There a library that holds more than 16,000 books was cleared of its stock in just a few days after a Facebook campaign called for local people to go in and take their maximum allowance. Aimed to highlight the massive hole that would be left in the community if Milton Keynes council went ahead with its plans to close the popular centre, it hit the national headlines and campaigns started to spring up all over the UK. </p>
<p>
	On the 5th, many libraries had visits from local writers and poets, including in Gloucestershire, where a battalion of seven &lsquo;Flying Authors&rsquo; were jetting round the libraries of the county in not the greenest of actions, but certainly the most expensive. Entertaining people who&rsquo;d turned up to support their local lender with stories, poems and songs, the seven managed to visit all thirteen centres in one day. Others had read-ins with their most famous local scribe or B-list celeb supporter. </p>
<p>
	Sheffield had a mass Shhh!-in at their city library attended by over 200 people. After a group exclamation of &lsquo;Shhh!&rsquo; and three cheers for their library, people went and took out their allowance of 15 books each, leaving the shelves looking decidedly bare and the floor filled with people sitting down for an impromptu read-in. </p>
<p>
	Milborne Port library in Somerset was infiltrated by a phantom &lsquo;book snatcher&rsquo; who went around grabbing books from reading people&rsquo;s hands, replacing it with signs saying things like &lsquo;illiteracy&rsquo;, &lsquo;poor life chances&rsquo;, and &lsquo;social isolation&rsquo;. </p>
<p>
	People took to the streets in Scotland and Cambridge, with a group protesting outside the Scottish Parliament in Edinburgh and a flashmob in Cambridge reading aloud from their favourite books. </p>
<p>
	We hope the public keep on throwing the book at &lsquo;em &ndash; and wonder how the story will turn out, although we&rsquo;re not holding out for a happy ending. </p>
<p>
	The fact that sections of the poor, young, disabled, socially marginalised or even middle class end up getting screwed is tough Shhh. </p>
<p>
	* See <a href="http://publiclibrariesnews.blogspot.com/" target="_blank">http://publiclibrariesnews.blogspot.com/</a> for a map of libraries under threat from councilcuts and local campaign in your area. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1096), 127); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1096);

				if (SHOWMESSAGES)	{

						addMessage(1096);
						echo getMessages(1096);
						echo showAddMessage(1096);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


