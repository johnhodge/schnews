<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 776 - 24th June 2011 - Greek Chorus of Doom</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, financial crisis, eu, greece" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 776 Articles: </b>
<p><a href="../archive/news7761.php">Squatting On Heaven's Door</a></p>

<p><a href="../archive/news7762.php">Fair And Square: R.i.p. Brian Haw</a></p>

<p><a href="../archive/news7763.php">Edl: Ruck On Tommy</a></p>

<p><a href="../archive/news7764.php">What's The Pig Idea?</a></p>

<p><a href="../archive/news7765.php">Not Back To Iraq</a></p>

<b>
<p><a href="../archive/news7766.php">Greek Chorus Of Doom</a></p>
</b>

<p><a href="../archive/news7767.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 24th June 2011 | Issue 776</b></p>

<p><a href="news776.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREEK CHORUS OF DOOM</h3>

<p>
<p>  
  	Next Tuesday (28th) the perilous Greek parliament will vote on whether to impose new, even more savage EU-IMF austerity measures in return for being loaned the next &euro;12bn instalment of a bailout agreed last year. If they don&rsquo;t, and the loans are withheld, they will default on loan payments in July, sparking economic chaos in European finance markets. If they do, more and more of the Greek public are likely to come out in open revolt (see <a href='../archive/news775.htm'>SchNEWS 775</a>, <a href='../archive/news771.htm'>771</a>). &nbsp;  </p>  
   <p>  
  	Greece are &euro;350bn in debt. This is 150% of GDP &ndash; all the money they get in each year, before spending anything at all on government, the public sector, defence, law, health etc. etc. And they currently spend 110% of GDP on all that. This not only leaves them unable to make debt repayments, it means they need to borrow more just to stand still. And the debt mountain just keeps rising &ndash; and faster than any foreseeable future scenario could ever start paying it off.  </p>  
   <p>  
  	A leading economist this week claimed just this, having run a model where Greece successfully imposed all austerity and enforced privatisation measures, and somehow managed to get &lsquo;highly optimistic&rsquo; 3% growth (even the rich countries are predicting levels of less 2% anywhere in Europe). By 2015 their debt mountain will be near &euro;400bn. Meanwhile in reality, all the public sector cuts and collapse of services are reducing the size of the economy and GDP, actually increasing the spiral of unserviceable debt.&nbsp;&nbsp; &nbsp;  </p>  
   <p>  
  	This all means that without a different approach one thing is inevitable. Greece will default. But when?  </p>  
   <p>  
  	Sooner will mean that the other Eurozone countries (mainly Germany and France) and will have to swallow up to &euro;250bn of losses (Greek banks have around 25% of the debt) and deal with the political turmoil and fallout. Would contagion bring down Ireland, Portugal or Spain? Would it push the Eurozone into recession, would the masses in all these countries start thinking &lsquo;if Greece, why not us...?&rsquo;  </p>  
   <p>  
  	Later and Eurozone countries will merely face ever larger losses and all the same problems as above.  </p>  
   <p>  
  	But nonetheless the EU&rsquo;s major interests just can&rsquo;t seem to agree that major debt restructuring is the best bet forward, and seem paralysed into merely committing more and more of their taxpayers cash to loanshark bailouts that can never be repayed.  </p>  
   <p>  
  	Whatever happens this coming week, Greece is a simmering pot ready to boil over - either financially or socially. Will the power elite find a radical way out of the crazy financial system death-slide in time? &nbsp;  </p>  
   <p>  
  	How ironic if the &lsquo;birth place of democracy&rsquo; is where the final downfall of the liberal democracy Euro-cartel begins. We for one will welcome abject poverty and our new Chinese overlords with humble gratitude and servility.&nbsp; &nbsp;  </p>  
   <p>  
  	May EU live in interesting times...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1261), 145); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1261);

				if (SHOWMESSAGES)	{

						addMessage(1261);
						echo getMessages(1261);
						echo showAddMessage(1261);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


