<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 718 - 16th April 2010 - Tar-mageddon</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bp, climate change, oil, tar sands, direct action, environmentalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 718 Articles: </b>
<b>
<p><a href="../archive/news7181.php">Tar-mageddon</a></p>
</b>

<p><a href="../archive/news7182.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7183.php">For Pete's Sake</a></p>

<p><a href="../archive/news7184.php">Camp Hill Races</a></p>

<p><a href="../archive/news7185.php">Counter Punch</a></p>

<p><a href="../archive/news7186.php">Lunartics And Asylum  </a></p>

<p><a href="../archive/news7187.php">Experimentalists</a></p>

<p><a href="../archive/news7188.php">Mains Still At It</a></p>

<p><a href="../archive/news7189.php">Milking It</a></p>

<p><a href="../archive/news71810.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/718-cameron-lg.jpg" target="_blank">
													<img src="../images/718-cameron-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 16th April 2010 | Issue 718</b></p>

<p><a href="news718.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TAR-MAGEDDON</h3>

<p>
<strong>AS ACTIVISTS TARGET BP&rsquo;S REACH FOR THE TARS.....</strong> <br />  <br /> After two weeks of direct action, the &ldquo;BP Fortnight of Shame&rdquo; concluded yesterday (15th) with protests outside the oil giant&rsquo;s AGM and a rooftop occupation in Brighton. Throughout the country, activists targeted BP over its plans to extract oil from the Alberta tar sands in Canada (see <a href='../archive/news716.htm'>SchNEWS 716</a>) in protests which peaked last Saturday (10th) when hundreds of climate activists in London, Brighton, Oxford and Cambridge descended on BP garages for the &ldquo;Party at the Pumps&rdquo; . <br />  <br /> Described by campaigners as &lsquo;the most destructive project on earth&rsquo;, the Alberta tar sands extractions are already the world&rsquo;s largest industrial operation. Millions of barrels of tar sands oil are being extracted every day, producing three to five times as many greenhouse gas emissions as conventional oil extraction - committing eco-trocity across vast areas of ancient forest whilst producing lakes of toxic waste.&#8239;By 2020, the tar sands are expected to have released over 141 megatonnes of greenhouse gases &ndash; twice that produced by all the cars and trucks in Canada combined. <br />  <br /> Oil prices are now at around $90 a barrel &ndash; but in the long term prices simply have to rise as peak oil nears. The decline in availability of easily refinable oil means that the carbon and labour intensive process of getting refined oil from the tar sands is economically viable. BP claims that the reserves in the area are second only to Saudi Arabia&rsquo;s alleged oil stock and  there are many greedy mouths waiting to suck the tar sands dry. <br />  <br /> After purchasing a stake in the &lsquo;Sunrise Project&rsquo; (the wonderfully fluffy name for this last ditch attempt to feed the developed world&rsquo;s oil-guzzling lifestyle) in 2007, BP have since announced its potential involvement in two other tar sands developments. <br />   <br /> The &lsquo;Party at the Pumps&rsquo; campaign, organised by the UK Tar Sands Network, Rising Tide, the Camp for Climate Action and the Indigenous Environmental Network, has been hailed a huge success in bringing these issues to light. <br />  <br /> On Saturday (10th), amidst sunshine and soundsystems, party ready acivists took part in simultaneous demonstrations, including forecourt invasions, which shut down three BP petrol stations in London and Brighton. <br />   <br /> Spokesperson Sheila Laughlin said, &ldquo;Today we did exactly what we set out to do &ndash; we hit BP&rsquo;s profits by shutting down their petrol stations, and we hit their brand by informing thousands of people about their destructive tar sands plans. Nearly everyone we spoke to was shocked and outraged by the horrific climate, ecological and human impacts of tar sands extraction. If BP want to completely alienate the UK public, they&rsquo;re going about it in exactly the right way.&rdquo; <br />   <br /> The  oil giant didn&rsquo;t get off lightly inside their own AGM either. Lobbyists, environmental groups and shareholders called on BP to take a vote on a resolution at their AGM on Thursday (15th) that would force the corporation to review its investment in the tar sands project. BP shareholders however proved their commitment to the systematic destruction of the planet by voting with a 93.9% majority to sweep the issues under the carpet and not review the ecological and economical viability of the scheme. With BP&rsquo;s &lsquo;strategic move&rsquo; into the tar sands hailed as &ldquo;a good example of how we aim to tackle the challenges facing us&rdquo; by chairman Carl-Henrik Svanberg (who trousers &pound;4m quid a year) there seems to be, more than ever, a need for those with a slightly less biased viewpoint to take action to ensure projects like this are not accepted as a solution to the world&rsquo;s energy crisis. <br />  <br /> <strong>ACTION ROUNDUP:</strong> <br />  <br /> <strong><u>Brighton: </u></strong> <br /> Activists successfully invaded and shut down two separate BP petrol stations&#8239;(Lewes Rd and Ditchling Rd) for the afternoon by camping out in tents in the forecourt.&#8239;They brought with them soundsystems and a jovial attitude. Several banners were dropped in the vicinity and lots of leaflets went out. There were no arrests, police reportedly taking a very hands-off approach, much to the annoyance of the BP garage management. The Lewes Rd garage saw more protest on Thursday (15th) as activists occupied the roof and shut the station down for the day, police only turning up to discuss the results of the AGM before wandering off. As SchNEWS goes to press they&rsquo;ve been up there for 7 hours and counting... <br />  <br /> <strong><u>London: </u></strong> <br /> Around 150 people invaded BP&rsquo;s Shepherd&rsquo;s Bush petrol station and held a ceilidh in the forecourt. They climbed on the pumps, hung banners from the roof and stopped operations for the remainder of the day. There was a heavy police presence, but no arrests. <br />   <br /> <strong><u>Oxford: </u></strong> <br /> About 25 people from the Thames Valley Climate Action group reconstructed a model of the tar sands on Oxford&rsquo;s central shopping parade, including a pipeline and a &lsquo;toxic&rsquo; tailings pond complete with toy ducks. They played music and made speeches to thousands of Saturday shoppers using a cycle-powered sound system. Around 5,000 anti-BP leaflets were given out and video messages were collected from the public to send to BP&rsquo;s AGM. <br />   <br /> <strong><u>Cambridge: </u></strong> <br /> Local activists from the Cambridge Tar Sands Network led an unconventional tour group through the city, visiting RBS Branches and University Facilities funded by BP, all of which are linked to tar sands investment. The event caught public interest and was hailed as a successful public expose of Cambridge&rsquo;s Tar Sand affiliations. <br />   <br /> <strong><u>14 DAYS LATER....</u></strong> <br />  <br /> Since the annual Fossil Fools Day on April 1 kicked off the &ldquo;Fortnight of Shame&rdquo; the UK has seen actions springing up all over the place &bull; 22,000 &ldquo;rebranded&rdquo; BP logos were delivered to BP HQ &ndash; see the video:&#8239; &#8239; <a href="http://www.youtube.com/watch?v=KNLzN3zld7o" target="_blank">http://www.youtube.com/watch?v=KNLzN3zld7o</a> <br />   <br /> &bull; A BP petrol station was blockaded in Plymouth, with protesters chaining themselves to petrol pumps. The station was closed for an hour and a half, and there were two <a href="arrests:http://www.thisisplymouth.co.uk/news/Greens-protest-closes-petrol-station/article-1992261-detail/article.html" target="_blank">arrests:http://www.thisisplymouth.co.uk/news/Greens-protest-closes-petrol-station/article-1992261-detail/article.html</a> <br />  <br /> &bull; A demonstration by Youth Against Climate Change in St. Albans, targeting RBS, who are one of BP&rsquo;s key funders in the tar sands: http://www.stalbansreview.co.uk/news/6646160.St_Albans_demo_targets__RBS/ <br />  <br /> &bull; RBS cash machines were rendered temporarily out of order by Brighton Against Tar Sands(BATS)<a href=":http://www.indymedia.org.uk/en/2010/04/448446.html" target="_blank">:http://www.indymedia.org.uk/en/2010/04/448446.html</a>  <br />  <br /> &bull; A walking tree from Alberta, Canada, turned up at BP HQ (and other key London locations) to complain about tar sands deforestation &ndash; video here:&#8239; <a href="http://vimeo.com/10630598" target="_blank">http://vimeo.com/10630598</a>  <br />  <br /> &bull; &ldquo;Free money&rdquo; stained with oil was given out at a Natwest (owned by RBS) branch in Norwich: <a href="http://felixinnorwich.wordpress.com/2010/04/01/fossil-fools-day-in-norwich-tar-sand-protest-at-natwest/" target="_blank">http://felixinnorwich.wordpress.com/2010/04/01/fossil-fools-day-in-norwich-tar-sand-protest-at-natwest/</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bp&source=718">bp</a>, <a href="../keywordSearch/?keyword=climate+change&source=718">climate change</a>, <a href="../keywordSearch/?keyword=direct+action&source=718">direct action</a>, <a href="../keywordSearch/?keyword=environmentalism&source=718">environmentalism</a>, <a href="../keywordSearch/?keyword=oil&source=718">oil</a>, <a href="../keywordSearch/?keyword=tar+sands&source=718">tar sands</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(736);
			
				if (SHOWMESSAGES)	{
				
						addMessage(736);
						echo getMessages(736);
						echo showAddMessage(736);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


