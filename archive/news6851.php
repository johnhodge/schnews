<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 685 - 27th July 2009 - The Hippy, Hippy Shakedown</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, big green gathering, festivals, somerset, police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 685 Articles: </b>
<b>
<p><a href="../archive/news6851.php">The Hippy, Hippy Shakedown</a></p>
</b>

<p><a href="../archive/news6852.php">Still Camping It Up</a></p>

<p><a href="../archive/news6853.php">Still Kemping It Up</a></p>

<p><a href="../archive/news6854.php">Breaking Wind</a></p>

<p><a href="../archive/news6855.php">Testing Times</a></p>

<p><a href="../archive/news6856.php">Calais Abouts</a></p>

<p><a href="../archive/news6857.php">La Lutte Continue</a></p>

<p><a href="../archive/news6858.php">Twisty Tierney   </a></p>

<p><a href="../archive/news6859.php">Republic Gone Bananas</a></p>

<p><a href="../archive/news68510.php">Saving It For An Iran-y Day</a></p>

<p><a href="../archive/news68511.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/685-big-green-lg.jpg" target="_blank">
													<img src="../images/685-big-green-sm.jpg" alt="You wouldn't like me when I'm angry" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Monday 27th July 2009 | Issue 685</b></p>

<p><a href="news685.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE HIPPY, HIPPY SHAKEDOWN</h3>

<p>
<strong>BREAKING NEWS.... THE BIG GREEN GATHERING HAS BEEN SHUT DOWN BY POLICE!&nbsp;</strong> <br />  <br /> <strong>The Big Green Gathering</strong>, a fixture in the alternative calendar, was due to return after two years this week. 15&ndash;20,000 people were expected to turn up on Wednesday (29th) to the site near Cheddar, Somerset, for Europe&rsquo;s largest green event - a five-day festival promoting sustainability and renewable energy, with everything from allotments to alternative media. Hundreds of staff and volunteers are already on site, and its cancellation comes just days before gates were due to open. Organisers, most of whom work for nothing, are gutted. One told SchNEWS &ldquo;<em>We are so disappointed not to be having this year&rsquo;s gathering &ndash; it means so much to so many people</em>&rdquo;. <br />  <br /> A last-minute injunction by Mendip District Council, supported by Avon and Somerset Police, put the ki-bosh on the entire event - citing the potential for &lsquo;<strong>crime and disorder</strong>&rsquo; and safety concerns. This was despite the fact that the festival had actually been granted a licence on the 30th of June. According to Avon and Somerset police&rsquo;s website  &ldquo;<em>[We] went above and beyond the call of duty to ensure this event took place.</em>&rdquo; This is of course utter bollocks. <br />  <br /> The injunction was due to be heard in the High Court in London on Monday (27th). However, before that could happen the BGG organisers surrendered the festival licence on Sunday morning. As soon as this was done a police commander at the meeting was overheard saying into his radio &ldquo;Operation Fortress is go&rdquo;. Police have already set up roadblocks and promised to turn festival-goers back. Chief Inspector Paul Richards, festival liaison, later confirmed to one of the festival organisers that &ldquo;<strong>This is political</strong>&rdquo;, adding that the decision had been made over his head at county level. One of SchNEWS&rsquo; sources on site said that the police were frank about the fact that the closure had been planned for two weeks. &ldquo;<em>This was a blatant act of political sabotage &ndash; the Big Green Gathering is now completely bankrupt, they knew that we were going to be closed down and yet they carried on allowing us to spend money hand over fist on infrastructure</em>&rdquo;. <br />  <br /> The BGG collapsed financially in 2007 under the weight of increased security costs. The new licensing act added an extra &pound;120k to their costs, leaving them with a loss of &pound;80k. Security accounted for a third of their overall overheads and the road marshalling bill rose from &pound;5k to over &pound;23k. In spite of these setbacks, they managed to scrape themselves back off the floor with shareholder cash and some potentially dubious corporate involvement. Every effort had been made by the gathering&rsquo;s organisers to accommodate the increasingly niggling demands of police and licensing authorities. The procedure lasted over six months &ndash; just check out <a href="http://www.mendip.gov.uk/CommitteeMeeting.asp?id=SX9452-A782D404" target="_blank">www.mendip.gov.uk/CommitteeMeeting.asp?id=SX9452-A782D404</a> for the minutes of meetings held between organisers and the authorities. Demands included a steel fence, watchtowers and perimeter patrols, having the horsedrawn field inside a &lsquo;secure compound&rsquo; and wristbands for twelve undercover police. At a multi-agency meeting on Thursday, police took those wristbands in order to maintain the pretence that the festival stood a chance of going ahead. A catalogue of other obstacles were also continually placed in the organiser&rsquo;s path. <br />  <br /> All of the businesses associated with the BGG came under scrutiny, licensing authorities contacted South West ambulances, the Fire Brigade and the fencing contractors and asked them to get payment up front from the BGG. Needless to say this caused huge problems.&nbsp; <br />  <br /> Under the terms of the Licensing Act 2005, police can insist on certain security firms being used by organisers. This of course leads to a totally unhealthy hand-in-glove relationship, open to abuse. Stuart Security were forced on the BGG by police, and on Wednesday last week, they suddenly announced that they wanted 60% of their fee up front. Even though the BGG scraped the cash together, the company still wanted out. So the BGG hired another firm &ndash; against police wishes. The fact that Stuart Security rely on police approval for lucrative contracts at Glastonbury Festival, the Royal Bath &amp; West Show, WOMAD, Reading Festival, and Glade Festival has, of course, no bearing on the matter.&nbsp; <br />  <br /> <strong>UNCERTAIN FETE</strong> <br />  <br /> The last issue at stake was road closures. Mendip District Council had insisted on road closures as part of the licensing requirements. A festival organiser contacted the highways agency to process this fairly routine request. The decision was passed to junior management who reportedly came under intense pressure not to grant the closure. As the road closures were not secured, the council were able to claim that the BGG was in breach of licence. A nice little legal stitch-up that according to one QC meant the BGG stood fuck-all chance of fighting the injunction. Of course, now that &ldquo;<strong>Operation Fortress</strong>&rdquo; is in full swing, there are road-blocks throughout the area. The BGG is itself a limited company and could have fought the injunction - risking no more than bankruptcy - but in a nasty twist two individuals were also named, meaning that should proceedings have gone ahead against the festival then Mendip Council would have had a claim on their assets to settle court costs. Police also threatened to place the farmer on the injunction, risking his entire livelihood. <br />  <br /> Anyone who has ever been to the Big Green will know that the atmosphere is more like a village fete than any of the mainstream events. There is virtually no aggro. It&rsquo;s more about chai and gong-massages than Stella and fisticuffs. All power is 12V solar and the amplification is correspondingly quiet. Music stops at midnight. Compare that to the 24 hr Technomuntfucks that go on with state blessing across the country. Of course it would be cynical to suggest that the BGG represents an alternative that the authorities fear. It&rsquo;s a gathering place for eco-activists, where the likes of Plane Stupid and No-Borders hang out and exchange ideas while trying to avoid being button-holed by 9-11 truthers. It&rsquo;s clear now that the state views events like the Big Green in the same light as Climate Camp and the anti-G20 protests. The BGG saga is showing that there may no longer be any &lsquo;safe&rsquo; legal spaces for us to gather. The third way of quasi-legal free-ish festivals is looking like a dead-end.&nbsp; <br />  <br /> <strong>IT&rsquo;S NOT EASY BEING GREEN</strong> <br />  <br /> It&rsquo;s clear that the Big Green has been singled out &ndash; and any gathering promoting those values or trying to organise in a grass-roots way will probably suffer the same fate once they get to a certain size. As corporate-branded Glasto has become a fixture on the mainstream calendar, like Ascot or Wimbledon, many have turned towards smaller more &lsquo;grass-roots&rsquo; festivals. Niche festivals have bloomed across the British landscape. No matter what your bent, be it faerie wings or S&amp;M, there&rsquo;s probably a muddy weekend in a field for you.&nbsp; <br />  <br /> Of course this isn&rsquo;t the first time that Britain&rsquo;s had a thriving festival scene. See previous SchNEWS&rsquo; for how the free festival scene came under ruthless attack from the forces of Babylon (or just skin up for an old hippy and listen to them bang on about the glories of the White Goddess Fayre or Torpedo Town).&nbsp; <br />  <br /> Some have tried to go down the quasi-legal route, such as Strawberry Fair and even Glastonbury, until the aptly named Mean Fiddler intervened in 2002. Unfortunately the corporate dollar is never far behind. Witness how Glastonbury went from a fence-jumping free-for-all where the festival organisers built the infrastructure, but the fly-pitchers, buskers and random naked lunatics made it a real festie rather than a fenced in, heavily policed corporate theme park. <br />  <br /> The Big Green was an exceptional festival, which managed to leap through the legal process while being crew-heavy and retaining a lot of the free-festival atmosphere (Not all of course  - we still had to put up with plod wandering around site). It was a unique gathering place for fringe movements, from eco-activists to crop-circle nutters.&nbsp; <br />  <br /> We&rsquo;re not just banging on about festivals being free because we miss the good &lsquo;ol days &ndash; there&rsquo;s a huge difference between being a punter who has a whole experience laid on for them (e.g. Glasto&rsquo;s themed areas with helpful stewards pointing you in the direction of the consumer delights), and being part of a festival/free party where everyone&rsquo;s responsible for the entertainment, and even infrastructure like welfare. A crowd that feels it owns an event behaves differently to one that feels it has paid to have an experience. The fact that undercover police now feel free to operate and arrest people, without any back-up, for cannabis use or nudity (See <a href='../archive/news684.htm'>SchNEWS 684</a> and 603) at festivals has a lot do with the sheep-like behaviour of punters - a mentality that our masters are keen to see enforced. <br />  <br /> In the SchNEWS office we&rsquo;re hearing rumours that people aren&rsquo;t going to be put off &ndash; alternative sites are being looked at and people are heading to the West Country anyway. In the words of one participant &ldquo;<em>Things are just getting interesting</em>&rdquo;. Time for the Big Black Barney? <br />  <br /> <strong>* Festivals and free parties are going on all around the country:-</strong>  <br />  <br /> For festivals see <a href="http://www.festivaleye.com" target="_blank">www.festivaleye.com</a> and <a href="http://www.efestivals.co.uk" target="_blank">www.efestivals.co.uk</a>  <br />  <br /> For free parties see <a href="http://www.partyvibe.com" target="_blank">www.partyvibe.com</a>  <br />  <br /> For both of these as well as protest camps and lots more see <a href="../pap&nbsp;" target="_blank">www.schnews.org.uk/pap&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>big green gathering</span>, <a href="../keywordSearch/?keyword=festivals&source=685">festivals</a>, <a href="../keywordSearch/?keyword=police&source=685">police</a>, <span style='color:#777777; font-size:10px'>somerset</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(411);
			
				if (SHOWMESSAGES)	{
				
						addMessage(411);
						echo getMessages(411);
						echo showAddMessage(411);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>