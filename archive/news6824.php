<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 682 - 3rd July 2009 - A Lot To Ansar For</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bangladesh, sweatshops, ansars, repression, workers struggles" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 682 Articles: </b>
<p><a href="../archive/news6821.php">Rumble In The Jungle</a></p>

<p><a href="../archive/news6822.php">Coup Blimey</a></p>

<p><a href="../archive/news6823.php">Don&#8217;t Coal Home  </a></p>

<b>
<p><a href="../archive/news6824.php">A Lot To Ansar For</a></p>
</b>

<p><a href="../archive/news6825.php">Island Mentality  </a></p>

<p><a href="../archive/news6826.php">Sez Who?</a></p>

<p><a href="../archive/news6827.php">Ich Bin Ein Burnin</a></p>

<p><a href="../archive/news6828.php">Lions And Tigers</a></p>

<p><a href="../archive/news6829.php">Squats Up: South West London Squat Round Up</a></p>

<p><a href="../archive/news68210.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd July 2009 | Issue 682</b></p>

<p><a href="news682.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A LOT TO ANSAR FOR</h3>

<p>
Two sweatshop workers in Bangladesh were shot dead last weekend during protests over pay cuts and outstanding wages. Another worker is in a critical condition and dozens more injured as the police and Ansar civilian volunteer forces attacked the workers with tear gas and live bullets at a garment zone near Dhaka. <br /> Following several days of strikes last week by 1800 RMG (Ready Made Garments) workers at one sweatshop, management finally agreed to the demands last Thursday. But on returning to work on Saturday (27th), three workers who had taken leading roles in the agitation and negotiations were told they were sacked &ldquo;<em>on charges of leading the demonstrations</em>&rdquo;. Upon learning this, the workforce immediately left the factory to demonstrate and demand the reinstatement of their fellow workers. This led to fierce arguments followed by scuffles with the factory bosses, two of whom were reported to have been beaten up. <br />  <br /> The workers blockaded a main road and resisted police attempts to disperse them using tear gas, responding with hails of stones and bricks. The Ansars then shot into the crowd, killing two, later claiming that the workers were about to seize their guns. <br />  <br /> As news of the fatalities spread during the afternoon the workers&rsquo; numbers swelled, joined by other factories striking in solidarity and RMG factories being closed early by bosses due to fears of the unrest spreading to their premises. The insurgent crowd then occupied a factory, smashing windows and wrecking offices. They remained in occupation for an hour and a half, during which time they set fire to the the factory&rsquo;s warehouse and the hated Ansar camp on factory grounds. The Ansars have a history of clashes with RMG workers in recent years and are sometimes deployed within factory compounds as a semi-permanent para-military presence. <br />  <br /> Thousands of workers gathered on the outskirts of Dhaka on Monday morning and set off to the nearby Export Processing Zone, location of many sweatshops. Police blocked their way with tear gas and rubber bullets leaving 100 protesters injured. Numbers swelled to 50,000, overwhelming the security forces and reducing a still operating complex to ashes &ndash; the fire brigade was denied access by those blocking the road. Meanwhile, others roamed the area and attacked another 50 factories and 20 vehicles. <br />  <br /> Clothes sweatshops in Bangladesh have long been notorious for appalling pay and conditions and  the latest unrest coincides with the release of a report by the Bangladeshi Government Factory Inspector&rsquo;s office. At least one in every seven garment factories does not pay salaries to the workers regularly and one in every three factories breaches labour laws. Labour leaders say these violations are actually more widespread, with factory owners using the global recession as a pretext for worker  exploitation. Although conditions had improved since the last major worker strikes in 2006 with workers earning up to $100 a month for 7-day weeks, wages have recently been cut by 20-30%. <br />  <br /> Back in 2006, 4000 factories in Dhaka went on wildcat strike; sixteen factories were burnt down, three strikers were killed plus thousands injured, and the army was brought in to restore order. Last week&rsquo;s events are only the latest in a series of recent similar clashes in the Bangladeshi garment sector. With the deepening economic crisis and further downward pressure on wages fuelled by greedy rich world consumerism, such conflicts look set to escalate. <br />  <br /> * For more see <a href="http://www.libcom.org" target="_blank">www.libcom.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>ansars</span>, <span style='color:#777777; font-size:10px'>bangladesh</span>, <span style='color:#777777; font-size:10px'>repression</span>, <span style='color:#777777; font-size:10px'>sweatshops</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=682">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(386);
			
				if (SHOWMESSAGES)	{
				
						addMessage(386);
						echo getMessages(386);
						echo showAddMessage(386);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>