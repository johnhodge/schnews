<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 692 - 24th September 2009 - Zelaya Cake</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, manuel zelaya, honduras, latin america" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 692 Articles: </b>
<p><a href="../archive/news6921.php">Junglist Missive</a></p>

<p><a href="../archive/news6922.php">Ffos'n' & Fightin'</a></p>

<p><a href="../archive/news6923.php">Tesco Alfresco</a></p>

<p><a href="../archive/news6924.php">Sean Free</a></p>

<p><a href="../archive/news6925.php">Co-mutiny Service</a></p>

<p><a href="../archive/news6926.php">Serb Your Enthusiasm</a></p>

<p><a href="../archive/news6927.php">Coal D-locks</a></p>

<p><a href="../archive/news6928.php">Glam Rocked</a></p>

<b>
<p><a href="../archive/news6929.php">Zelaya Cake</a></p>
</b>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 24th September 2009 | Issue 692</b></p>

<p><a href="news692.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ZELAYA CAKE</h3>

<p>
After two days hitching rides on tractors through the mountainous back roads of Honduras, hiding from military checkpoints in car boots, Honduras&rsquo; democratically elected president Manuel Zelaya quietly slipped into the Brazilian embassy in Tegucigalpa last Monday (21st). Zelaya promptly appeared on the embassy balcony to announce his return, sparking a siege of the embassy and a wave of protest and repression across the country. <br />  <br /> On hearing of Zelaya&rsquo;s return, thousands immediately descended on the embassy to show their support and protect the president. After seeing their laughing denial of Zelaya&rsquo;s return disproved, the coup regime responded by announcing a national curfew, cut off the embassy&rsquo;s electricity and water and sent hundreds of soldiers and police to surround the embassy. At 5am the next morning the military began to disperse the peaceful crowd with tear gas, rubber bullets, water cannons and live rounds. <br />   <br /> During the assault, police also attacked the offices of the Committee for Detained and Disappeared Persons of Honduras (COFADEH), firing tear gas into the building. COFADEH Director Bertha Oliva said: &ldquo;<em>The people can&rsquo;t even walk the streets in peace. They&rsquo;re being beaten just for stepping out of doors. [The police] hunt them as if for sport</em>.&rdquo; COFADEH alone documented 36 serious injuries and at least two deaths while independent reports indicated police arrested around 350 people and detained them in the Villa Olympica football stadium. <br />   <br /> After dispersing the crowds, masked police and soldiers cordoned off the streets around the embassy, preventing access even for international journalists and human rights workers. A few hours later they wheeled in a pick-up with massive speakers in the back and began to direct a constant stream of loud music at the embassy building. <br />  <br /> Honduran congressman Marvin Ponce, who was with Zelaya in the embassy, described the regime&rsquo;s reaction as: &ldquo;<em>a reflection of their philosophies, this government of putschists. They don&rsquo;t respect human rights. They don&rsquo;t want a political dialogue</em>.&rdquo; <br />  <br /> Despite the curfew remaining in place throughout Tuesday and Wednesday, Hondurans have continued to take to the streets, with thousands marching against the coup, setting up barricades of burning tires and participating in protest caravans of hundreds of vehicles. Attempting to regain control, police and military have been confronting marchers on the city streets and in residential areas, raiding houses in poor neighbourhoods in search of dissidents. <br />   <br /> Coup leader Roberto Micheletti initially demanded Brazil either grant Zelaya asylum in Brazil, or turn him over to be tried. However, he now appears to be cracking, first stating: &ldquo;<em>I will talk with anybody anywhere at any time, including with former President Manuel Zelaya</em>&rdquo;, before inviting an Organisation of American States (OAS) delegation to the country to broker an agreement. Short of a full-on assault on the embassy of the country with Latin America&rsquo;s biggest air force, it might be the only chance he&rsquo;s got. <br />  <br /> * For background see <a href='../archive/news691.htm'>SchNEWS 691</a>, <a href='../archive/news682.htm'>682</a> <br />  <br /> ** See also <a href="http://www.narconews.com" target="_blank">www.narconews.com</a>  and <a href="http://www.hondurasresists.blogspot.com" target="_blank">www.hondurasresists.blogspot.com</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=honduras&source=692">honduras</a>, <a href="../keywordSearch/?keyword=latin+america&source=692">latin america</a>, <a href="../keywordSearch/?keyword=manuel+zelaya&source=692">manuel zelaya</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(510);
			
				if (SHOWMESSAGES)	{
				
						addMessage(510);
						echo getMessages(510);
						echo showAddMessage(510);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>