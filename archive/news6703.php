<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 670 - 27th March 2009 - Luton Vanguard</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bnp, luton, military parades, direct action, far-right, neo-nazi" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.g-20meltdown.org/"><img 
						src="../images_main/g20-meltdown-banner.jpg"
						alt="G20 - Meltdown in the City"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 670 Articles: </b>
<p><a href="../archive/news6701.php">Apocolypse Wow</a></p>

<p><a href="../archive/news6702.php">Met Yer Match!</a></p>

<b>
<p><a href="../archive/news6703.php">Luton Vanguard</a></p>
</b>

<p><a href="../archive/news6704.php">--- In The Brown Stuff ---</a></p>

<p><a href="../archive/news6705.php">Right Shred Fred</a></p>

<p><a href="../archive/news6706.php">Edo Inside Schnews</a></p>

<p><a href="../archive/news6707.php">Huon Cry</a></p>

<p><a href="../archive/news6708.php">Famous Fife</a></p>

<p><a href="../archive/news6709.php">Saabotage</a></p>

<p><a href="../archive/news67010.php">Raven Mad</a></p>

<p><a href="../archive/news67011.php">Well Acquitted</a></p>

<p><a href="../archive/news67012.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 27th March 2009 | Issue 670</b></p>

<p><a href="news670.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LUTON VANGUARD</h3>

<p>
&#8220;<i>In relation to the last soldiers&#8217; parade in Luton &#8211; just who were the extremists? A small group of young Muslim men holding placards and shouting at the soldiers or the people chanting racist insults at them, making Nazi salutes, trashing the houses and cars of the protesters?</i>&#8221; - <b>Luton For Peace.</b> <br />
 <br />
Military parades of returning servicemen in British cities are becoming the scenes of conflict with anti-war and Muslim groups against far-right demonstrators. This Saturday there will be a second parade in a month through Luton of soldiers back from the Middle East. The last parade on March 10th turned into a knee-jerk Islamaphobic tabloid media frenzy when local Muslims held a noisy but non-violent anti-war protest and were condemned up and down the country for being unpatriotic. What wasn&#8217;t so fervently reported was that these protesters were met with scenes of Nazi salutes, racist chanting and violence by right-wing army supporters. The fact that two were arrested made it into most news reports, but what didn&#8217;t get mentioned was that they were in fact army supporters charged with racially aggravated harassment. <br />
 <br />
While it&#8217;s not clear whether the right-wingers on March 10th were part of an organised mobilisation, it&#8217;s now known that the Luton & Bedfordshire BNP will be there this Saturday to cheer the boys home and try to make mileage from anti-Muslim sentiments. Local Muslim groups will be very wary about demonstrating this time &#8211; both because of the hostility against them shown in the media, but also because one Muslim demonstrator had the windows of his house and car smashed after March 10th, and another was sacked from his job at Luton Airport because he was seen holding an anti-war placard that day. Anti-fascists &#8211; who have no links with the Muslims - will be there however to disrupt the BNP&#8217;s goose-stepping during the next parade. <br />
 <br />
Military parades haven&#8217;t come into the media spotlight until recently, but our source in Luton claims that there have been a number of parades in adjacent areas in the past few weeks including ones in Watford, Colchester, and four or five other places. While some of these have seen anti-war protests, the majority just take place without incident. Yet even though anybody would be glad to see people returning safely from a war-zone, these parades are still a valid time to ask the question &#8211; should these soldiers have gone there in the first place? <br />
 <br />
Also it may be a coincidence, but it seems some of these parades are happening in areas where the BNP are building support &#8211; recently the BNP have held Saturday afternoon stalls in Colchester, Southend and Epping. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
*Anti-fascists are urging all to come down and support them against the BNP at the military parade in Luton, and will be gathering at 11am on Saturday, March 28th near to the BNP&#8217;s meeting at 12 noon outside the Luton Town Hall. <br />
 <br />
* Email <a href="mailto:kittyplant@btinternet.com">kittyplant@btinternet.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>bnp</span>, <a href="../keywordSearch/?keyword=direct+action&source=670">direct action</a>, <span style='color:#777777; font-size:10px'>far-right</span>, <span style='color:#777777; font-size:10px'>luton</span>, <span style='color:#777777; font-size:10px'>military parades</span>, <span style='color:#777777; font-size:10px'>neo-nazi</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(259);
			
				if (SHOWMESSAGES)	{
				
						addMessage(259);
						echo getMessages(259);
						echo showAddMessage(259);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>