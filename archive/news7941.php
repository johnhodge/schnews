<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 794 - 28th October 2011 - Squatting: Empty Premises</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 794 Articles: </b>
<b>
<p><a href="../archive/news7941.php">Squatting: Empty Premises</a></p>
</b>

<p><a href="../archive/news7942.php">Occupy London: Loose Canon Fired</a></p>

<p><a href="../archive/news7943.php">Run Of The Millbank</a></p>

<p><a href="../archive/news7944.php">Cuadrilla Thriller</a></p>

<p><a href="../archive/news7945.php">Wood You Believe It</a></p>

<p><a href="../archive/news7946.php">Dale Farm: Nomads Land</a></p>

<p><a href="../archive/news7947.php">Edl: Brum Fash Clash</a></p>

<p><a href="../archive/news7948.php">Poor's Pay Reigned In</a></p>

<p><a href="../archive/news7949.php">I Am Sparktacus</a></p>

<p><a href="../archive/news79410.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/794-stPauls-lg.jpg" target="_blank">
													<img src="../images/794-stPauls-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 28th October 2011 | Issue 794</b></p>

<p><a href="news794.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUATTING: EMPTY PREMISES</h3>

<p>
<p>  
  	<strong>AS SCHNEWS CROWBARS OPEN THE DOOR OF THE TORY SQUAT CRACKDOWN..</strong>.  </p>  
   <p>  
  	On Wednesday (26th) the government published its response to the squatting consultation (see <a href='../archive/news791.htm'>SchNEWS 791</a>). While none of us were expecting anything sensible to come from a Tory government foaming at the mouth at the prospect of kicking an unpopular minority, the speed of the knee-jerk reaction and its implementation is beyond even our cynical predictions.  </p>  
   <p>  
  	The consultation ran from 13th July to 5th October &ndash; the shortest possible period, despite the government admitting they knew very little about the subject. In case anyone doubted that this consultation was merely a box ticking exercise (Ken Clarke promised criminalisation before the consultation had even ended), the response published this week ignores the vast majority of respondents and proposes &ldquo;as a first step... a new offence of squatting in residential buildings&rdquo; with offenders facing up to 6 months in prison and/or a &pound;5000 fine.  </p>  
   <p>  
  	The justification according to Crispin Blunt (Under [in]Justice Secretary) is &ldquo;the appalling impact squatting can have on... homes, businesses and local communities&rdquo;.&nbsp; Though there is very little evidence of this from their own consultation or any other research into homelessness and squatting. As MP John Mcdonnell (one of the few MPs willing to come out in support of squatting) said &ldquo;there was over 2,200 responses to the consultation on squatting so there is no way the government could have acknowledged all the evidence&rdquo;.  </p>  
   <p>  
  	Only seven respondents to the consultation (0.3% of the total responses) were &lsquo;victims&rsquo; of residential squatting. This is despite the fact the consultation was aimed at this group and received widespread publicity from the right-wing press. Other groups in favour of a new law included the CPS, the British Property Federation, some local authorities and Network Rail. On the other hand the vast majority of individuals, the Police, Magistrates&rsquo; Association, NUS, University and College Union, Criminal Bar Association, Law Society, High Court Enforcement Officers, SQUASH and all the major homeless charities were against legislation (but hey, what would they know?).  </p>  
   <p>  
  	So a law will be based on the views of seven people, reactionary media reports and a handful of self-interest groups. Meanwhile it will adversely affect thousands of vulnerable people as well as those who choose one of the few ways left to live &lsquo;outside the system&rsquo;.  </p>  
   <p>  
  	While the government claims that &ldquo;stopping short of criminalising squatting in non-residential buildings represents a balanced compromise&rdquo; &ndash;&nbsp; this clearly poses the wrong kind of compromising situation for people living in unused buildings. Assuming this law gets through (almost guaranteed) and is enforced (not so certain), an exodus from residential to non-residential property will no doubt follow. An increase in high profile commercial and industrial squats will be all the justification the government need to extend the law to cover all buildings. The government has already said that this is merely the first step, the reason the they haven&rsquo;t gone further already is because of complications around university and workplace occupations &ndash; something that would bring much more resistance from the unions and student groups. This tactic obviously worked as on Thursday evening (27th), Labour signalled its support for a &lsquo;squatting crackdown&rsquo;.  </p>  
   <p>  
  	To make matters worse the government are attempting to are burying the new law as an amendment to the already bulging Legal Aid, Sentencing and Punishment of Offenders Bill. This bill already takes away legal aid from many of those who need it most (see <a href='../archive/news754.htm'>SchNEWS 754</a>) while the amendment regarding squatting has been shoved in at the same time as another populist measure allowing people to attack intruders in their homes.  </p>  
   <p>  
  	With the amendments set to be debated next week and receiving cross party support they&rsquo;ll probably go through pretty easily (it remains to be seen whether the LibDems will go out on a limb, but don&rsquo;t hold your breath). The law is likely to come into effect early next year. So, parliamentary democracy has failed yet again, where next for the resistance campaign?  </p>  
   <p>  
  	Step forward Squatters&rsquo; Housing Action Group (SHAG). A SHAG spokesman told SchNEWS that they&rsquo;re &ldquo;calling for mass civil disobedience on Monday [31st], we&rsquo;re not going to stand for this totally undemocratic shit, we need everybody; squatters, trade unionist, students, protesters, homeless charities and more to hit the streets. We need to exercise our democratic right to enter parliament and tell these fuckers they can&rsquo;t get away with this&rdquo;  </p>  
   <p>  
  	Unfortunately, this message was later toned down and, as things stand, there will be a mass sleep out on Monday night followed by a possible meeting in Parliament on Tuesday. Though it&rsquo;s very much a &lsquo;watch this space&rsquo; situation with squatting groups around the country holding emergency meetings as SchNEWS went to print.  </p>  
   <p>  
  	While a mass attack on parliament sounds like fun, squatters are going to need wide ranging support from unions, the fledging Occupy movement and even politicians if there is any hope to actually hinder the legislation itself.  </p>  
   <p>  
  	That said, it&rsquo;s not entirely hopeless. Squatting is as old as land ownership and a few vote grubbing Tory bastards aren&rsquo;t going to stop it. Squatters might have to become even more organised and better networked, but they&rsquo;re not going to disappear. Holland criminalised squatting over a year ago, many continue to defy the ban and none have (as yet) served any prison time.  </p>  
   <p>  
  	So, if you&rsquo;re sat on a damp mattress, clutching a baseball bat and using your copy of SchNEWS as particularly poor roach material; worried that the man might be at the door any moment to snatch away your own personal utopia, fear not! Squatting is still legal, justifiable and necessary and will continue to be so for some time yet.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1402), 163); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1402);

				if (SHOWMESSAGES)	{

						addMessage(1402);
						echo getMessages(1402);
						echo showAddMessage(1402);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


