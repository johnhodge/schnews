<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 730 - 9th July 2010 - Uzbeks Against The Wall</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, oil, russia, central asia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 730 Articles: </b>
<p><a href="../archive/news7301.php">The Hole Truth</a></p>

<p><a href="../archive/news7302.php">Decommissioners All Out</a></p>

<p><a href="../archive/news7303.php">Bog Roll Of Honour</a></p>

<p><a href="../archive/news7304.php">Village Fate</a></p>

<b>
<p><a href="../archive/news7305.php">Uzbeks Against The Wall</a></p>
</b>

<p><a href="../archive/news7306.php">Besetting A President</a></p>

<p><a href="../archive/news7307.php">Raven's Law</a></p>

<p><a href="../archive/news7308.php">Gettin On Their Wiki</a></p>

<p><a href="../archive/news7309.php">The Paper Trail</a></p>

<p><a href="../archive/news73010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th July 2010 | Issue 730</b></p>

<p><a href="news730.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>UZBEKS AGAINST THE WALL</h3>

<p>
<p>
	The former Soviet republic known as Kyrgyzstan has recently been making headlines. Following a historic change to long established Scrabble rules allowing proper nouns, Kyrgyzstan stands out as the highest scoring word on the board. They also had a revolution recently; a popular uprising that deposed the corrupt, self-serving and incompetent Kurmanbek Bakiyev, who himself came to power on the back of the last revolution-but-one, the so-called &lsquo;Tulip Revolution&rsquo;. </p>
<p>
	Waves of revulsion following Bakiyev&rsquo;s massacre of protesters back in April led to his exile and a hastily assembled interim government led by Roza Otunbayeva. The new leaders immediately promised elections and, again, that a new leaf had been turned in Central Asian politics away from authoritarianism towards a democratic future. If only. The Bakiyev family, who controlled lucrative smuggling, transport and energy interests, have been not only sulking but also plotting in exile. </p>
<p>
	Bakiyev&rsquo;s son, Maksim, has been sowing chaos and destruction across the country as a ploy to bring the family back in from the cold. </p>
<p>
	And with 400,000 refugees and internally displaced persons and perhaps 2,000 dead in inter-ethnic violence between Kyrgyz and Uzbek communities, it looks like Bakiyev junior&rsquo;s plan has come to fruition. Much of the city of Osh and surrounding areas have been burned and ransacked. Fleeing civilians speak of mass rape and pogroms organised by paramilitary groups. </p>
<p>
	This widespread destruction has left the interim government looking weak and rudderless. The president asked for (and was refused) Russian military assistance to quell the violence. Despite such a large section of the population&rsquo;s absence the government is still insisting that the referendum/election is the most pressing priority. Meanwhile groups as disparate as the International Crisis Group and the Anarchist International Embassy are urging the Kyrgyz government to put their efforts into establishing some peace and stability in the country first. </p>
<p>
	Having belatedly sent in the security forces, they seem to be concentrating more on policing the Uzbek communities (that have already suffered the worst) than stemming the violence. Just how much attention the security forces, reformed under Bakiyev, pay to the new government is an unknown quantity. Under Bakiyev, the elite, US trained anti-narcotics force was integrated into the country&rsquo;s internal security, their US-honed skills turned on an unarmed civilian population. </p>
<p>
	Kyrgyzstan is at the centre of the &lsquo;new great game&rsquo; played out in Central Asia between the USA and Russia. Kyrgyzstan is the unfortunate host to both a US and a Russian base at opposite ends of the country, and both have used fair means and foul to force Kyrgyzstan into their respective orbits. The country is one of the main routes to Europe for Afghan opiates, and with the Taliban&rsquo;s attacks on the US&rsquo; Pakistani transit routes into Afghanistan, it has become central to the US Afghan war effort. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(853), 99); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(853);

				if (SHOWMESSAGES)	{

						addMessage(853);
						echo getMessages(853);
						echo showAddMessage(853);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


