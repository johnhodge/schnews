<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 676 - 22nd May 2009 - Not A Blind Brit Of Difference</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, colombia, farc, democratic security, minga, quilcue, alvaro uribe, human rights" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 676 Articles: </b>
<p><a href="../archive/news6761.php">Final Frontiers</a></p>

<b>
<p><a href="../archive/news6762.php">Not A Blind Brit Of Difference</a></p>
</b>

<p><a href="../archive/news6763.php">War That Broke The Tamils' Back</a></p>

<p><a href="../archive/news6764.php">Now We're Rolling</a></p>

<p><a href="../archive/news6765.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd May 2009 | Issue 676</b></p>

<p><a href="news676.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOT A BLIND BRIT OF DIFFERENCE</h3>

<p>
With an uncharacteristic display of conscience the British government has finally stopped military aid to Colombia. So what provoked this moral U-turn from what was the second biggest financial backer of the Colombian military (which still receives billions of dollars from the US under the controversial &lsquo;Plan Colombia&rsquo;). And will it make any difference? <br />  <br /> Since coming to power in 2002 Colombian president Alvaro Uribe has vigorously pursued his policy of &lsquo;Democratic Security&rsquo; - which involved strengthening the military and going on the offensive against the FARC, Colombia&rsquo;s dominant guerilla force. While the FARC have been retreating, the policy has also seen the Colombian military overtake both the FARC and Colombia&rsquo;s numerous right-wing paramilitary groups to become the leading perpetrators of human rights abuses. In 2002 the state was responsible for 17% of recorded violence. By 2006 that number had jumped to 56%. <br />  <br /> In recent months it has become increasingly obvious that recorded human rights violations only represent a small proportion of the atrocities committed in Colombia. Both the military and the police have a long history of colluding with paramilitary organisations committing numerous massacres throughout the country. Recent weeks have seen allegations that they have also been conspiring with the militias to burn the bodies of massacre victims in an effort to conceal the number of people killed. <br />  <br /> In addition to this, investigators are looking into the 1,296 cases of extra-judicial executions that have occurred since 2002. The murders were so called &lsquo;false positives&rsquo; &ndash; a practice where soldiers murder civilians in rural areas before dressing them up in combat fatigues and labelling them guerillas. In the last few weeks 67 soldiers have been convicted for murdering civilians while more than 400 have been arrested. Uribe, meanwhile, has stated that he believes there are many false accusations and he wants the state to take up the legal defence of the accused military personnel. <br />  <br /> While much of the violence appears to be determined by little more than location &ndash; most victims were from strategically or economically important areas &ndash; some of it is far more targeted. More union leaders are assassinated each year in Colombia than in the rest of the world combined. The most recent, Edgar Martinez of the farmers and miners union in the department of Bolivar, was killed earlier this month shortly after being turned back from a police road block. &nbsp; <br />  <br /> The indigenous communities of Colombia have also been repeatedly targeted. Following the &lsquo;Minga&rsquo; protest movement of last November (see <a href='../archive/news656.htm'>SchNEWS 656</a>), the husband of  Aida Quilcue, Chief Council of CRIC (Indigenous Regional Council of Cauca) - the architects of the uprising - was assassinated at a military roadblock in what appeared to be an attempt on Quilcue&rsquo;s life. The assassinations have continued since, last week Roberth Guachet&aacute;, another community leader and a protected person by the Inter-American Court of Human Rights, was found beaten to death. Indigenous communities throughout rural Colombia have also been targeted by both the army and the guerillas with military attacks, occupation of villages and disappearances all routine. <br />  <br /> Journalists critical of Uribe also continue to be subjected to death threats, forcing many into exile. Two weeks ago veteran Colombian journalist Jos&eacute; Everardo Aguilar, a noted critic of official corruption, was shot dead on his doorstep.&nbsp; <br />  <br /> Throughout his time in power Uribe has consistently accused trade unionists, indigenous activists and journalists of collaborating with the guerillas, singling them out as targets for paramilitary and military attacks. <br />  <br /> While it makes a welcome change for the British government to take notice of a state complicit in the murder and abuse of its citizens, Britain will continue to send Uribe and his military force secret and unconditional counter-narcotics assistance. With a government quite happy to label dissenters in any way that suits their purpose and a military quite happy to do the state&rsquo;s dirty work it seems unlikely that this latest development will make much difference to the hundreds of thousands of Colombians caught up in the conflict.&nbsp; <br />  <br /> See <a href="http://www.colombiajournal.org/index.htm" target="_blank">www.colombiajournal.org/index.htm</a> and <a href="http://mamaradio.blogspot.com" target="_blank">http://mamaradio.blogspot.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>alvaro uribe</span>, <a href="../keywordSearch/?keyword=colombia&source=676">colombia</a>, <span style='color:#777777; font-size:10px'>democratic security</span>, <span style='color:#777777; font-size:10px'>farc</span>, <span style='color:#777777; font-size:10px'>human rights</span>, <span style='color:#777777; font-size:10px'>minga</span>, <span style='color:#777777; font-size:10px'>quilcue</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(330);
			
				if (SHOWMESSAGES)	{
				
						addMessage(330);
						echo getMessages(330);
						echo showAddMessage(330);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>