<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 750 - 2nd December 2010 - Ratcliffe 2o trial Begins</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, environment, coal" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 750 Articles: </b>
<p><a href="../archive/news7501.php">Fee Foes' Fine Fun</a></p>

<p><a href="../archive/news7502.php">Schnews 750: State Of The Indignation</a></p>

<p><a href="../archive/news7503.php">It's A Zad Day When...</a></p>

<p><a href="../archive/news7504.php">The Italian Jobless</a></p>

<b>
<p><a href="../archive/news7505.php">Ratcliffe 2o Trial Begins</a></p>
</b>

<p><a href="../archive/news7506.php">Town Hall Meeting</a></p>

<p><a href="../archive/news7507.php">Phil Yer Boots</a></p>

<p><a href="../archive/news7508.php">Edl Double Bill</a></p>

<p><a href="../archive/news7509.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 2nd December 2010 | Issue 750</b></p>

<p><a href="news750.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RATCLIFFE 2O TRIAL BEGINS</h3>

<p>
<p>
	The trial of 20 climate change campaigners planning to shut down Ratcliffe power station began last week (see <a href='../archive/news672.htm'>SchNEWS 672</a>). It has been over a year and a half since police raided a school in Nottingham where activists were staying before the planned action and pre-emptively arrested 114 people. The 20 to face prosecution stand accused of Conspiracy to Commit Aggravated Trespass. They will argue they acted to prevent death and serious injury as a result of the hundreds of thousands of tonnes of CO2 the coal-fired power station emits every week. </p>
<p>
	The prosecution opened with a bizarre slanderous assault on the defendants, questioning their motives by saying, &ldquo;Was it more fun to plan this action or to vote for Zac Goldsmith? Did the defendants do all this, because they didn&rsquo;t have a Glastonbury ticket?&rdquo; The verbal volley provoked a baffled response from the jury, who passed the judge a note asking about the relevance of her ramblings. </p>
<p>
	After exposing her ignorance of both the electoral appeal of non-dom fops and of festivals of choice for discerning activists, prosecutor Miss Gerry proceeded to lay out the detailed plans to shut down the CO2 spewing plant and present the jury with neverending lists of seized objects spanning from lock-on tubes to portions of cheese. </p>
<p>
	After this two pronged attack, claiming the defendants were in it for a giggle on one hand while banging on about the fiendish complexity of their alleged conspiracy on the other, Miss Gerry went on to suggested they should have sought celebrity endorsements, saying the money spent on the action would have been more wisely used to hire Cheryl Cole to model second-hand clothes. </p>
<p>
	On Tuesday (30th) it was the defence&rsquo;s turn. Their star witness was James Hansen, Head of National Aeronautics and Space Administration NASA&rsquo;s Goddard Institute and respected climate change expert. Dr Hansen outlined the catastrophic effects of climate change for the court and declared his sympathy with the actions of the defendants. Two of the defendants have also taken the stand to explain their actions. Up soon will be Green Party Leader, Caroline Lucas, who will describe the failure of domestic and European politicians to act on climate change. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1029), 119); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1029);

				if (SHOWMESSAGES)	{

						addMessage(1029);
						echo getMessages(1029);
						echo showAddMessage(1029);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


