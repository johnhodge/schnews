<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 658 - 5th December 2008 - Get It E.on</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, e.on, coal power, climate change, royal bank of scotland, bristol, norwich, brighton, london, coventry, camp for climate action, national climate march" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 658 Articles: </b>
<p><a href="../archive/news6581.php">Koch-a-hoop</a></p>

<p><a href="../archive/news6582.php">Whack-a-mole</a></p>

<p><a href="../archive/news6583.php">Od'd On Edo</a></p>

<b>
<p><a href="../archive/news6584.php">Get It E.on</a></p>
</b>

<p><a href="../archive/news6585.php">Somali-argh</a></p>

<p><a href="../archive/news6586.php">  Tactical Sabbatical</a></p>

<p><a href="../archive/news6587.php">Spit And Polish</a></p>

<p><a href="../archive/news6588.php">Schnews In Brief</a></p>

<p><a href="../archive/news6589.php">And Finally 658</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 5th December 2008 | Issue 658</b></p>

<p><a href="news658.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>GET IT E.ON</H3>

<p>
Across the UK climate change organisations and activists staged 48 hours of action against E.ON Corp &#8211; the energy giants funding new coal fired power stations.    <br />
 <br />
Last weekend&#8217;s actions ranged from &#8216;greenwash guerrillas&#8217; at E.ON offices in London to a &#8216;die in&#8217; &#8211; a mass extinction of activists disguised as animals - outside a Brighton shopping centre.  <br />
Numerous groups targeted The Royal Bank of Scotland (RBS) for buffering E.ON with massive loans. Outside RBS in Bristol, and in Norwich, campaigners set up stalls manned by fake E.ON staff telling real E.ON facts about new coal. One group in Brighton dressed up as cleaners and tried to scrub their RBS clean.  <br />
 <br />
At the FA Cup&#8217;s HQ (the FA is sponsored by E.ON) an eco-football squad, sporting t-shirts showing pictures of new coal stations, turned up to lobby staff and the public. <br />
 <br />
The F.OFF prize however goes to the protesting Santas, who turned 48 into 96 hours, occupying E.ON HQ in Coventry. Some entered the building, toured the offices and crashed board meetings. Others blocked the front, gluing themselves to the entrance and dumping sacks of coal in the revolving door. <br />
 <br />
E.ON corp is funding the first of seven new coal power stations planned to be built in the UK. Not a single coal powered station has been built in the UK for over 30 years. (Read about this year&#8217;s Climate Camp at Kingsnorth in <a href="../archive/news641.htm">SchNEWS 641</a>).  <br />
 <br />
The greenwash behind a push for new coal lies with the promise of so-called &#8216;clean coal&#8217;.  <br />
Which is a farce as the idea behind &#8216;clean coal&#8217; is premised on &#8216;carbon capture and storage&#8217; &#8211; catching the carbon dioxide released after burning coal and burying it underground &#8211; technology which does not exist yet. Coal is still the dirtiest form of energy supply.  <br />
 <br />
* The UK government are hovering above the big red GO-ahead button for new coal power plants. Join the National Climate March in London this Saturday (December 6th) and say NO to NEW COAL. The march begins at Grosvenor Square 12 noon and ends in Parliament Square. It is aimed to co-insides with the United Nations Climate Change Conference, currently being held in Poland from 1st-12th December 2008. <br />
 <br />
* For more see <a href="http://www.risingtide.org.uk" target="_blank">www.risingtide.org.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=658">climate change</a>, <a href="../keywordSearch/?keyword=bristol&source=658">bristol</a>, <a href="../keywordSearch/?keyword=brighton&source=658">brighton</a>, <a href="../keywordSearch/?keyword=london&source=658">london</a>, <a href="../keywordSearch/?keyword=camp+for+climate+action&source=658">camp for climate action</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(157);
			
				if (SHOWMESSAGES)	{
				
						addMessage(157);
						echo getMessages(157);
						echo showAddMessage(157);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>