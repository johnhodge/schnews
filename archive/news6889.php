<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 688 - 21st August 2009 - Gross-ery Shop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tesco, tescopoly, titnore woods, supermarkets, direct action, worthing" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 688 Articles: </b>
<p><a href="../archive/news6881.php">Stock Horror At Hls</a></p>

<p><a href="../archive/news6882.php">Climate Of Anticipation  </a></p>

<p><a href="../archive/news6883.php">A Wales Of A Time</a></p>

<p><a href="../archive/news6884.php">Fash On Parade</a></p>

<p><a href="../archive/news6885.php">Straight From The P'smouth</a></p>

<p><a href="../archive/news6886.php">Back To Base</a></p>

<p><a href="../archive/news6887.php">Cease And Resist</a></p>

<p><a href="../archive/news6888.php">Smarter In Vestas</a></p>

<b>
<p><a href="../archive/news6889.php">Gross-ery Shop</a></p>
</b>

<p><a href="../archive/news68810.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st August 2009 | Issue 688</b></p>

<p><a href="news688.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GROSS-ERY SHOP</h3>

<p>
The threat of eviction seems to be looming larger over the protest camp at <strong>Tinore Woods</strong> (See <a href='../archive/news672.htm'>SchNEWS 672</a>). This week the site was invaded by police under the guise of a &lsquo;health and safety&rsquo; check. The TesCops seemed more out to video everyone, initimidate, in-terror-gate and generally size up an all out attack on the site, so inconveniently protecting ancient woodland from ruinous development plans. <br />  <br /> It can be no coincidence that this comes just weeks after the poor just-trying-to-earn-an-honest-living-guv grocers finally overcame all legal hurdles to get the official go-ahead for the store. <br />  <br /> It&rsquo;s Tesco standard operating procedure to use its team of crack lawyers and deep pockets to bully and threaten anyone attempting to muscle in or derail any sweet deal they&rsquo;ve got going.&nbsp; <br />  <br /> Using the same firms of consultants that the council use (councils are forced to pick from a goverment-approved shortlist of firms, most already in the employ of Tesco &ndash; but hey, no conflict!) they launch PR campaigns full of mis-truths and creative statistics, and conduct &lsquo;independent&rsquo; surveys full of biased questions, all to manufacture &lsquo;evidence&rsquo; of the desperate need and support for the new store - which can all be presented to counter the unconvinced.  They are then happy to sweat out any opposition by fighting endless legal battles, tweaking plans to extend the planning objection process, and sitting by until the opposition runs out of funds and the will to live. <br />  <br /> And if that doesn&rsquo;t work, they can always bend a few ears in Westminister and get the government to overrule any denial of planning permission. Variations of these tactics are almost certainly now being played out in a town near you (for an idea of just how many battles there are going on round the country see <a href="http://www.tescopoly.com" target="_blank">www.tescopoly.com</a>) <br />  <br /> Back in Titnore, rumours are circulating that Worthing Borough Council are shortly to give the final go-ahead for the 875-home housing development and that the camp will be cleared by force before work starts, possibly as soon as October.&nbsp; <br />  <br /> The campers are appealing for all those who have supported them during their three-year marathon protest to make the effort to come and visit them now - before it&rsquo;s too late.  <br />  <br /> See <a href="http://www.myspace.com/camptitnore&nbsp;" target="_blank">www.myspace.com/camptitnore&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=688">direct action</a>, <a href="../keywordSearch/?keyword=supermarkets&source=688">supermarkets</a>, <a href="../keywordSearch/?keyword=tesco&source=688">tesco</a>, <a href="../keywordSearch/?keyword=tescopoly&source=688">tescopoly</a>, <a href="../keywordSearch/?keyword=titnore+woods&source=688">titnore woods</a>, <a href="../keywordSearch/?keyword=worthing&source=688">worthing</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(472);
			
				if (SHOWMESSAGES)	{
				
						addMessage(472);
						echo getMessages(472);
						echo showAddMessage(472);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>