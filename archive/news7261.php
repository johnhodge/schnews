<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 726 - 11th June 2010 - Slick Publicity</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bp, oil, climate change, usa" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 726 Articles: </b>
<b>
<p><a href="../archive/news7261.php">Slick Publicity</a></p>
</b>

<p><a href="../archive/news7262.php">And So Itt Begins</a></p>

<p><a href="../archive/news7263.php">Calais Eviction Alert</a></p>

<p><a href="../archive/news7264.php">For Who's Benefit?</a></p>

<p><a href="../archive/news7265.php">Chilli Reception</a></p>

<p><a href="../archive/news7266.php">Hell And High Water</a></p>

<p><a href="../archive/news7267.php">Striking Blow</a></p>

<p><a href="../archive/news7268.php">Blowing A Raspberry</a></p>

<p><a href="../archive/news7269.php">Gas-tly Business</a></p>

<p><a href="../archive/news72610.php">Edl Offside In Cardiff</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/726-world-cup-lg.jpg" target="_blank">
													<img src="../images/726-world-cup-sm.jpg" alt="It must be the World Cup, they're trotting us out again." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th June 2010 | Issue 726</b></p>

<p><a href="news726.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SLICK PUBLICITY</h3>

<p>
<strong>&nbsp;REPORT FROM NEW ORLEANS ON THE BP OIL-SPILL DISASTER...</strong> <br /> 
	 <br /> 
	&nbsp;In response to America&rsquo;s largest ever environmental disaster, Tony Hayward, CEO of BP, said, &ldquo;I&rsquo;d like my life back.&rdquo; We&rsquo;re sure he does, as would the eleven workers who died on BP&rsquo;s Deepwater Horizon oil rig when it exploded 40 miles off the Louisiana coast, in the Gulf of Mexico, on April 20th. <br />  <br /> 

	But you have to sympathise with Tony. The fact that BP failed to test the strength of the cement in the well, despite knowing that the casing was &ldquo;the riskier of two options&rdquo; and that it &ldquo;might collapse under pressure&rdquo; can&rsquo;t be held against him. Even though the same disregard for safety killed 15 and injured 170 BP workers in the Texas Oil Refinery explosion of 2005. <br />  <br /> 

	BP has also had over 20 years since the Exxon-Valdez oil spill in Alaska to learn from its botched response to that disaster. Yes, despite Exxon having its name on the tanker, it was actually BP who disastrously failed to contain the toxic sludge that spewed into pristine wilderness. But that was then. BP has since spent billions of dollars on advertising telling us they are &lsquo;Beyond Petroleum&rsquo; &ndash; so beyond it in fact they don&rsquo;t care how much they lose. <br />  <br /> 

	BP has had over 8,000 minor and major recorded spills since 1990 alone. While all eyes have been on the current ecocide in the Gulf of Mexico, their Alaskan Pipeline burst in late May, spewing 100,000 gallons of oil into the environment. State inspectors say this occurred because &ldquo;procedures weren&rsquo;t properly implemented,&rdquo; in other words &ndash; they didn&rsquo;t give a damn. <br />  <br /> 

	The ho-hum lackadaisical attitude of Tony Hayward is indicative of BP&rsquo;s disaster response in general. It has been shocking to see BP&rsquo;s slow response to contain the oil. There is a complete lack of any oil containment technology, beyond stringing some booms (vinyl tubes) over the ocean that deflate and blow away. While the oil industry has poured billions of dollars into riskier deep-water drilling, it has not invested in responses to the leaks and disasters that have increased four fold in the last decade. <br />  <br /> 

	<strong>CRUDE JOKE</strong> <br />  <br /> 

	Maybe we&rsquo;re overreacting. Let us again heed the soothing words of Tony, or Tiny Tony as he is now known in the US due to the following comment, <em>&ldquo;The Gulf of Mexico is a very big ocean. The amount of volume of oil and dispersant we are putting into it is tiny in relation to the total water volume</em>.&rdquo; That would be the estimated 47 million to 235 million gallons of toxic crude oil that has poured into the ocean over the last eight weeks. Not to mention the use in the &lsquo;clean up&rsquo; of one million tonnes of the Corexit oil dispersant, a neuro toxin pesticide banned in the UK, arguably as harmful as the oil itself. <br />  <br /> 

	We need not worry our silly little heads about the ecological destruction either. Tiny Tony assures us, &ldquo;<em>the environmental impact of this disaster is likely to have been very, very modest.</em>&rdquo; Come now Tony, it&rsquo;s you that is being modest. This disaster is going to kill thousands of dolphins and sea turtles, hundreds of thousands of seabirds and billions of fish and shellfish. <br />  <br /> 

	The communities along the Louisiana coast, whose main work is fisheries, have lost their catch to a greater predator, and BP are playing on their vulnerability and dividing them by giving the &lsquo;lucky&rsquo; few work cleaning up the toxic waste. BP has refused to provide masks and other safety equipment to those in these communities, and when this resulted in several cases of clean-up workers being hospitalised, Tony said it was probably due to food poisoning. Shrimp &agrave; la oil? <br />  <br /> 

	The Louisiana wetlands that nurture the wildlife and communities have already been decimated by the oil industry. The wetlands are a 4.2 million acre region where the Mississippi River flows into the Atlantic. They have formed over 7,500 years by the rivers flooding and annually depositing silt creating long fingers of land and barrier islands. It has taken just 75 years for them to have been almost completely destroyed. They are disappearing faster than any other part of the world, with an area the size of a football field lost every 38 minutes. <br />  <br /> 

	Oil and natural gas were discovered in Louisiana&rsquo;s coastal areas in the early 1900s. This lead to long canals being cut through the wetlands to transport drilling equipment and oil. As well as being destructive in itself, this allowed more salt water to seep in to the wetlands, which killed the freshwater plants&rsquo; roots leading to further soil erosion. These wetlands act as a natural defence against hurricane storm surges. The further they are destroyed the more vulnerable coastal towns all the way up to New Orleans become. The storm surge from Hurricane Katrina in 2005 flooded 80% of the city, killing over 2,000 people. <br />  <br /> 

	As New Orleans and the coastal region still struggle to recover five years after Katrina they face a real kick in the teeth. Meteorological experts are predicting, &ldquo;the most active season on record&rdquo; for hurricanes, comparing ocean temperatures that contribute to hurricane formation to those in the summer of Katrina. Oil from the BP rig is predicted to continue to spew in to the gulf for months to come. A hurricane sweeping a surge of oil into the wetlands and towns of Louisiana would be apocalyptic for the region. <br />  <br /> 

	Tiny Tony put it best last year, saying BP&rsquo;s &ldquo;primary purpose was to generate profit for our share holders&rdquo; and that &ldquo;our primary purpose in life was not to save the world.&rdquo; Really Tony, you don&rsquo;t say. <br />  <br /> 

	* See <a href="http://ragingpelican.wordpress.com" target="_blank">http://ragingpelican.wordpress.com</a> <br /> 
	 <br /> 
	&nbsp; <br /> 
	 <br /> 
	&nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bp&source=726">bp</a>, <a href="../keywordSearch/?keyword=climate+change&source=726">climate change</a>, <a href="../keywordSearch/?keyword=oil&source=726">oil</a>, <a href="../keywordSearch/?keyword=usa&source=726">usa</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(807);

				if (SHOWMESSAGES)	{

						addMessage(807);
						echo getMessages(807);
						echo showAddMessage(807);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


