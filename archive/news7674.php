<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 767 - 15th April 2011 - Mexico: Grave Situation</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, narco wars, drugs, organised crime" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 767 Articles: </b>
<p><a href="../archive/news7671.php">Taking Libya-ties</a></p>

<p><a href="../archive/news7672.php">Southern Climes</a></p>

<p><a href="../archive/news7673.php">Ed-f Off</a></p>

<b>
<p><a href="../archive/news7674.php">Mexico: Grave Situation</a></p>
</b>

<p><a href="../archive/news7675.php">Benefit Of Doubt</a></p>

<p><a href="../archive/news7676.php">Social Centre Plus</a></p>

<p><a href="../archive/news7677.php">Inside Schnews</a></p>

<p><a href="../archive/news7678.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th April 2011 | Issue 767</b></p>

<p><a href="news767.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MEXICO: GRAVE SITUATION</h3>

<p>
<p>  
  	On April 6th, Mexican authorities uncovered a mass grave of some 59 people at the La Joya ranch, San Fernando. Over the next few days more and more bodies surfaced; at the time of writing 120 bodies had been discovered. Men, women and children have been found in several pits. Amongst the corpses are those of two girls, bound together and shot in the head. These bodies were discovered in exactly the same place where 72 Central and South American migrants were killed in a mass execution in late August last year.  </p>  
   <p>  
  	The dead are almost undoubtedly victims of the Zetas cartel - one of the bloodiest in a bloody business, having learned their trade as Mexican special forces in the USA&rsquo;s very own &lsquo;School of the Americas&rsquo; (see SchNEWS feature &lsquo;Grim Up North&rsquo;), before deciding to go independent.  </p>  
   <p>  
  	Judging by the varying states of decomposition of the victims, some have been recently killed while others have been dead for over a year. Somehow the investigations into last year&rsquo;s massacre didn&rsquo;t quite get round to checking the rest of the ranch for bodies. It is suspected that they were all kidnapped from buses that travel the North-South highway through the state of Tamaulipas to the US border. In this massacre, just like last year&rsquo;s, they were most likely killed for either refusing to work for the cartel (as drug mules or as low-level narcos), or failing to pay ransom money.  </p>  
   <p>  
  	Back in August when the bodies of the 72 migrants were found, the government faced a major diplomatic backlash from neighbouring countries whose citizens had been killed. This time round the government was quick to point out that there was no need for alarm; all the dead were Mexican. The head of state security, Jaime Canseco Gomez, stated that he knew that the victims were all Mexicans because (in his own words) &ldquo;None of them had tattoos.&rdquo; This was said in the first few days following their discovery, before a single corpse had been identified. A day later the first two victims were identified by (you guessed it...) their tattoos. Anytime the Mexican government says anything with a degree of certainty you can assume it is either lying or just plain wrong. So far the dead include at least one Guatemalan and one US citizen.  </p>  
   <p>  
  	The forensic investigation looks like it might take some time. Despite the huge number of dead (quite likely the largest mass grave in the Americas since the &lsquo;dirty wars&rsquo; of Central America during the &lsquo;80s) there are only four pathologists working the case. The city of Matamoros, where the bodies have been taken, has literally run out of morgue space.  </p>  
   <p>  
  	The Victoria-Matamoros highway (one of the major Mexican routes) has become a no-go area. Bus services are suspended as companies cannot guarantee the safety of their staff or passengers. Those who have money travel by air, and those who don&rsquo;t are trapped in their cities. And the official response is depressingly ambivalent - authorities could only confirm that two buses had &lsquo;disappeared&rsquo; during the last few months, but that maybe another one or two buses had also gone missing. If the authorities can&rsquo;t keep track of buses (and their passengers), what hope is there for families looking for missing relatives? Over 200 family members have arrived in Matamoros looking for lost loved ones.  </p>  
   <p>  
  	Meanwhile the government has announced that it will drastically increase the military presence in the state, which is already some 5,000 strong. The military (Federal police, Marines and Mexican Army) has never been particularly good at stopping crime; blocking roads and shooting kids is more their speciality. They arrive as an occupying force, travelling in convoys, so that the narcos&rsquo; lookouts can easily keep their bosses forewarned. And that&rsquo;s when they&rsquo;re not directly involved in the transit and sale of drugs themselves.  </p>  
   <p>  
  	Meanwhile, local cops are broadly divided into two types - those who work for the narcos and those who are narcos. The cartels have infiltrated the frontier&rsquo;s police forces so thoroughly that the police that you see patrolling are only concerned with defending cartel territory. Then there are the &lsquo;clones&rsquo;- civilian vehicles painted up to look like police vehicles, driven by gangsters in home-made police uniforms. The latter are relatively easy to spot; cops don&rsquo;t normally carry more than handguns whilst the narcos don&rsquo;t like to leave home with anything less than an AK47.  </p>  
   <p>  
  	During the last 4&frac12; years of the suicidally ill-conceived &lsquo;war on drug-trafficking&rsquo;, Mexican civil society has been slowly but surely imploding. In the next few days or weeks, prosecutors will drag out the supposed &lsquo;perpetrators&rsquo; of these latest massacres in San Fernando (just like they did for the one in August). These suspects, bruised, battered and tortured into confession, will be bandied before the press as proof of a crime solved. They may have had nothing whatsoever to do with the massacres, or they may be the lowest of the low in the Zeta hierarchy. What is for certain is that the government won&rsquo;t be looking higher up the food chain for those who really profit - the bankers and politicians who launder money and take their cut of the drug trade&rsquo;s billions.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1183), 136); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1183);

				if (SHOWMESSAGES)	{

						addMessage(1183);
						echo getMessages(1183);
						echo showAddMessage(1183);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


