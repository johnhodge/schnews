<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 735 - 20th August 2010 - Mapuche Hunger Strikes</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, chile, political prisoners, hunger strike, indigenous people" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 735 Articles: </b>
<p><a href="../archive/news7351.php">High Pressure Front</a></p>

<p><a href="../archive/news7352.php">Climate Camp Scotland</a></p>

<p><a href="../archive/news7353.php">Holy Waters</a></p>

<p><a href="../archive/news7354.php">Wrekin Havok</a></p>

<p><a href="../archive/news7355.php">Hammertime</a></p>

<p><a href="../archive/news7356.php">Rattling The Coppers</a></p>

<p><a href="../archive/news7357.php">Going Out On A High</a></p>

<p><a href="../archive/news7358.php">Dirty Sheets</a></p>

<b>
<p><a href="../archive/news7359.php">Mapuche Hunger Strikes</a></p>
</b>

<p><a href="../archive/news73510.php">Nationalist Express</a></p>

<p><a href="../archive/news73511.php">Gaza Defendant Appeal</a></p>

<p><a href="../archive/news73512.php">Taking The Ciss</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th August 2010 | Issue 735</b></p>

<p><a href="news735.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MAPUCHE HUNGER STRIKES</h3>

<p>
<p>
	<strong>MAPUCHE SOLIDARITY VIGILS AT CHILEAN EMBASSIES</strong> </p>
<p>
	Torrents of protests hit the globe in support of the Mapuche indigenous political prisoners in Chile. </p>
<p>
	The 12th of August saw the first wave of action initiated by the families of the prisoners take place outside Chilean Embassies worldwide, including London and Washington DC. The date fell on the one month anniversary of the start of a liquid hunger strike by the imprisoned Mapuche activists, who are being held under a Pinochet-era law that defines almost all political opposition as terrorism. Sound familiar? The notorious Chilean anti-terrorism law aims to squash discord under a dictatorship. </p>
<p>
	The Chilean Government has ignored protests from the UN and the Ethical Committee Against Torture over the warped use of this law and continues unhindered to try captives under a law that gives carte blanche to questionable means of prosecution such as anonymous witnesses, indefinite pre-trial detention and double simultaneous trials before both civil and military courts. </p>
<p>
	Thirty-two Mapuche prisoners are incarcerated in the prisons of Valvidia, Conception, Temuco, Lebu and Angol. They began the hunger strike as a means of drawing national and international attention to the violation of these people by entrenched government and commercial interests. Recent struggles are a part of an ongoing saga that began with colonialism. The Mapuche people have been abused in turn by European colonialists, multinational companies (yes that includes you Benetton), the Chilean Government, industrial farming and the building of hydro-electric dams. </p>
<p>
	The second wave struck on the 18th of August across Chilean Embassies in Argentina, England, France, Norway and others. Amongst the demonstrators and supporters in London were UK based NGO, Mapuche International Link, the Association of Chilean ex-political prisoners in the UK, Colombian Solidarity Campaign, Latin American Coordination, Memoria Historica, and many independent pro-Mapuche sympathisers. </p>
<p>
	The peaceful protests taking place in Temuco, Chile, were violently disrupted by police interference. Arrests hit the 56 mark, including that of Catalina Catrileo, sister of Matias Catrileo, a Mapuche youth previously murdered by Chilean police during a peaceful land rights demonstration. </p>
<p>
	The hunger strike is now entering its fifth week and medical reports indicate the development of serious health conditions. The liquid protest is experiencing serious problems as the prisoners have started exhibiting symptoms akin to heart failure such as visual disturbances, nausea, dizziness, disorientation, stomach cramps and more.They have lost &lsquo;between 8-11kg in weight&rsquo;. The denial of medical care for the prisoners further adds to the gravity of the situation as it increases the risk to their already deteriorating health. </p>
<p>
	* For upcoming actions and current information check <a href="http://www.mapuche-nation.org" target="_blank">www.mapuche-nation.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(910), 104); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(910);

				if (SHOWMESSAGES)	{

						addMessage(910);
						echo getMessages(910);
						echo showAddMessage(910);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


