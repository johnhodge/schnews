<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 671 - 3rd April 2009 - Spanners In The Works</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, workers struggles, visteon, ford, car industry, belfast, dagenham, dundee, waterford crystal, argentina, indugraf" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 671 Articles: </b>
<p><a href="../archive/news6711.php">Bank Statement</a></p>

<p><a href="../archive/news6712.php">Crapping Arrest Of The Week</a></p>

<p><a href="../archive/news6714.php">Boiling Kettle</a></p>

<p><a href="../archive/news6715.php">Tents Affair</a></p>

<p><a href="../archive/news6716.php">Storming The Ramparts</a></p>

<p><a href="../archive/news6717.php">Reaching The Summit</a></p>

<p><a href="../archive/news6718.php">Ravens Art</a></p>

<b>
<p><a href="../archive/news6719.php">Spanners In The Works</a></p>
</b>

<p><a href="../archive/news67110.php">Nato Summit  Strasbourg</a></p>

<p><a href="../archive/news67111.php">Creepy Crawley</a></p>

<p><a href="../archive/news67112.php">Mal Maison</a></p>

<p><a href="../archive/news67113.php">Lady Of The Lakenheath</a></p>

<p><a href="../archive/news67114.php">Edo Briefs</a></p>

<p><a href="../archive/news67115.php">Right Off</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd April 2009 | Issue 671</b></p>

<p><a href="news671.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SPANNERS IN THE WORKS</h3>

<p>
While protesters were taking out their anger on the causes of the financial crash in London, groups of sacked car industry workers directly affected by British car production dropping by 60% are occupying factories in Britain and Ireland. Some are considering restarting production as worker run businesses, as has been happening in Argentina (see below). <br />
 <br />
It's not that you'll catch SchNEWS trotting out some old lefty dogma about good 'ol working class jobs being more important than the environmental impact of the industry they are part of - particularly when it comes to the car industry. But what's notable here is workers' attempts to re-take control of their workplaces. <br />
 <br />
Hardest hit by the protests has been car part manufacturer Visteon who last week announced the winding down of its British operations, a process which will see 565 of its 600 UK based employees lose their jobs. Most of the cuts were to take place immediately - redundancy pay for many workers was slashed or none existent and pensions were frozen. <br />
 <br />
The protest kicked off in Belfast last Wednesday when all 200 workers were dismissed by an administrator with an hours notice. "<i>The worst thing about it is the way we were treated - that's why we're so angry,</i>" one protester was quoted as saying. "<i>There's no humanity in it - they wouldn't even let us stay to empty our lockers</i>". In response, 100 of the workers began an occupation of the building and are demanding action to save jobs and treat workers fairly.   <br />
 <br />
Since then the protest has spread to the mainland. Around 80 workers have now occupied a factory in Enfield while 100 more have been staging a sit-in outside the site in Basildon. Workers have now also descended on Britain's main Ford factory in Dagenham, establishing a picket outside the entrance and appealing for solidarity from the Ford workers. <br />
 <br />
Until 2008, Visteon was owned by Ford. Although now officially separate companies, Ford were Visteon's sole customers and many of the employees had been there so long that they were still on Ford contracts. Workers claim that Ford had promised that any redundancies from the company would receive the same compensation as employees employed directly by Ford, a promise on which they subsequently reneged. Ford is currently denying any responsibility for the workers and refuses to get involved in the dispute. <br />
 <br />
If the Visteon workers are looking for something to keep them busy during the occupations they would do well to look to Dundee for inspiration. As SchNEWS reported earlier in the month (see <a href='../archive/news667.htm'>SchNEWS 667</a>) employees at Prisme Packaging responded to a similar situation also by occupying the factory. Already into their fifth week of occupation they are now investigating the possibility of restarting the business as a workers co-op. <br />
 <br />
In Ireland however, the occupation at Waterford Crystal glass finally came to an end after an eight week stand off. The workers had kept the factory running during the occupation but finally had to admit defeat when the American private equity firm that took it over threatened to withhold EUR 10m of pension payments. Before its demise the protest caught the imagination of Irish workers and led to a number of actions in support, including a rally of around 10,000 people in Dublin on February 21st - the largest in 30 years. <br />
 <br />
* Continuing to show the way towards a genuine alternative is the workers co-op movement in Argentina. Towards the end of January SchNEWS reported on the worker seizure of Indugraf - a Buenos Aires graphics company (see <a href='../archive/news664.htm'>SchNEWS 664</a>). Operating under the name of the 10th of December Cooperative, the workers have since recommenced production despite continued threats of eviction. <br />
 <br />
Meanwhile, workers at another Buenos Aires company 'Disco de Oro' have also occupied the factory after owners tried to close down and strip the premises without informing employees. They have since begun the process of meeting with the Ministry of Work having laid their claim to the business while attempting to restart the factory. <br />
 <br />
Both new co-ops receive extensive support not only from the network of other worker run businesses but also from their community - one of the reasons there are more than a 150 of these 'recurperated businesses' operating in Argentina. The movement in Argentina began after the 2001 economic crash. In the eight years since, what began out of desperation and necessity has grown into a viable option in Argentina and a potential a model for action here in Britain.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=argentina&source=671">argentina</a>, <span style='color:#777777; font-size:10px'>belfast</span>, <span style='color:#777777; font-size:10px'>car industry</span>, <span style='color:#777777; font-size:10px'>dagenham</span>, <span style='color:#777777; font-size:10px'>dundee</span>, <span style='color:#777777; font-size:10px'>ford</span>, <span style='color:#777777; font-size:10px'>indugraf</span>, <span style='color:#777777; font-size:10px'>visteon</span>, <span style='color:#777777; font-size:10px'>waterford crystal</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=671">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(277);
			
				if (SHOWMESSAGES)	{
				
						addMessage(277);
						echo getMessages(277);
						echo showAddMessage(277);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>