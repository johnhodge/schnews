<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 776 - 24th June 2011 - Fair and Square: R.I.P. Brian Haw</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, brian haw, parliament square" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 776 Articles: </b>
<p><a href="../archive/news7761.php">Squatting On Heaven's Door</a></p>

<b>
<p><a href="../archive/news7762.php">Fair And Square: R.i.p. Brian Haw</a></p>
</b>

<p><a href="../archive/news7763.php">Edl: Ruck On Tommy</a></p>

<p><a href="../archive/news7764.php">What's The Pig Idea?</a></p>

<p><a href="../archive/news7765.php">Not Back To Iraq</a></p>

<p><a href="../archive/news7766.php">Greek Chorus Of Doom</a></p>

<p><a href="../archive/news7767.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/776-haw-lg.jpg" target="_blank">
													<img src="../images/776-haw-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 24th June 2011 | Issue 776</b></p>

<p><a href="news776.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FAIR AND SQUARE: R.I.P. BRIAN HAW</h3>

<p>
<p>  
  	This week peace campaigner Brian Haw passed away after a year-long fight with lung cancer. <br />  
  	Chances are he&rsquo;ll be remembered for many years to come as an iconic figure in British life. He transformed himself into a fixture in both Parliament Square and public consciousness. His one-man protest began in July 2001, initially against the sanctions regime which was destroying life in Iraq.  </p>  
   <p>  
  	Then came 9/11 and the subsequent wars. Haw lived, slept, eulogised and shouted one helluva lot in his one-man peace camp for the last ten years of his life, all for the sake of child victims of war.  </p>  
   <p>  
  	At the beginning he represented an outsider&rsquo;s take on UK foreign policy, ten years later and he was articulating mainstream wisdom; our leaders are crooks and liars who took us into an illegal and unjustifiable war with devastating consequences for millions.  </p>  
   <p>  
  	His intransigence was a constant niggle to the authorities. Not many people can say they&rsquo;ve had laws drafted purely to move them on, but Brian won that honour after the Parliament Square clauses of the Serious Organised Crime and Police Act 2005 were passed. These failed hilariously when the courts ruled that as his protest&nbsp; had started before the Act he was the only person in the world it didn&rsquo;t apply to.  </p>  
   <p>  
  	Unable to pass the Brian Haw and Associated Matters Act 2006, the authorities were then forced to adopt more underhand means of getting him to move on, including dubious charges of obstructing the highway, a constant stream of arrests and police harassment. Not to mention fencing off of the entire grassy area of the Square. Westminster Council stooped as low as claiming that the placards had to be cleared away in case terrorists seized an opportunity to stash a bomb amongst the tatty cardboard.  </p>  
   <p>  
  	The public unveiling of the Nelson Mandela statue in 2007 had the authorities getting themselves in a twist with that whole tricky democracy / totalitarianism thing: Ironically, Haw was fenced off and hassled to keep him out of the cameras (see <a href='../archive/news601.htm'>SchNEWS 601</a>).  </p>  
   <p>  
  	Even though child victims of war remain, Haw&rsquo;s efforts became more than an anti-war and anti-sanction protest. He was making a statement about freedom of speech and assembly &ndash; one he won despite repeated and concerted efforts to get him moving on. For all the criticisms levelled at the guy, he understood one thing well: Irritating those in high places (and surrounding offices) isn&rsquo;t a crime, it&rsquo;s a tool. The Square&rsquo;s going to be quieter without him.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1257), 145); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1257);

				if (SHOWMESSAGES)	{

						addMessage(1257);
						echo getMessages(1257);
						echo showAddMessage(1257);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


