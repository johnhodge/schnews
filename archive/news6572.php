<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 657 - 28th November 2008 - Radio Active</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, venezuela, chavez, radio, alternative media, caracas" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://veggieclimatemarch.50webs.com/"><img 
						src="../images_main/vegan-climate-march-banner.jpg"
						alt="Vegan Climate March, Dec 6th 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 657 Articles: </b>
<p><a href="../archive/news6571.php">Frozen Assets</a></p>

<b>
<p><a href="../archive/news6572.php">Radio Active</a></p>
</b>

<p><a href="../archive/news6573.php">Agenda Bender</a></p>

<p><a href="../archive/news6574.php">Free Wheelers</a></p>

<p><a href="../archive/news6575.php">Pointing The Minga</a></p>

<p><a href="../archive/news6576.php">Bruges Crew</a></p>

<p><a href="../archive/news6577.php">Dedicated Followers Of Fascism</a></p>

<p><a href="../archive/news6578.php">Schnews In Brief</a></p>

<p><a href="../archive/news6579.php">657 And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th November 2008 | Issue 657</b></p>

<p><a href="news657.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>RADIO ACTIVE</H3>

<p>
As usual, the media coverage of Sunday&#8217;s regional elections in Venezuela swung between trumpeting Chavez&#8217;s &#8216;stinging defeat&#8217; or crowing about &#8216;the deepening&#8217; of the revolution. Depending on, as usual, if you see Mr Chavez as a Stalinist demagogue keen to subject the world to oppressive communism, or a 21st Century Socialist saviour bearing gifts of peace, happiness, income redistribution and amusing Bush-insults.   <br />
 <br />
For ordinary Venezuelans however, as usual, they were caught somewhere between a borderline paranoid psychotic right and a potty, blinkered, ten-years-of-power-out-of-touch-with-reality left. This polarisation is just as evident in Venezuelan media as it is abroad. While the privately owned commercial media openly encourages presidential assassinations, the comically subservient state media has recently reproduced wire-tapped conversations of opposition leaders bragging about the swoosh watches they were planning to buy (evil dastardly capitalist swine). <br />
 <br />
Meanwhile, many of those stuck in the middle of this partisan posturing have decided to tell their own stories with their own media. For many of Venezuela&#8217;s - mostly poor - urbanites, Sunday&#8217;s election results were delivered by co-operatively produced coverage from a number of independent community radio stations. Under the banner &#8216;ni privados ni estatales&#8217; (not private, not state) these radio stations, alongside various community TV Channels and websites, date back to 1999 and the new constitution voted in by referendum. Following their role in the mass mobilisation in 2002 that brought Chavez back to power after a coup the number and popularity of these outlets exploded.    <br />
 <br />
As these media operate as a communication outlet for the barrio people and are run by community and popular organisers they don&#8217;t tend to attract many fans from amongst the right wingers in opposition to Chavez. That doesn&#8217;t, however, mean that they are starry-eyed cheerleaders for El Comandante. In fact, coming from the people directly affected, they often offer the most constructive and practical criticism. It now appears they could have a key role to play in reigning in the excesses and blindspots of the government and perhaps more importantly, moving the &#8216;Bolivarian Revolution&#8217; outside of the constraints of the cult of personality surrounding Chavez. <br />
 <br />
The election results were genuinely consolidating for Chavez with 17 out of 22 contests won and a 10.4% advantage in the total vote. But there is also no doubt that the opposition made huge and important inroads amongst one of Chavez&#8217;s natural constituencies, the urban poor, even taking the capital, Caracas. It doesn&#8217;t take much to work out why: most Venezuelans list rocketing crime rates and crumbling urban infrastructure along with sky high inflation as their top concerns.   <br />
 <br />
If the &#8216;Revolution&#8217; is to continue, with or without Latin America&#8217;s favourite socialist clown, the government has to start accepting criticism and listening. And aside from the election results, there&#8217;s no better place to get the message than from the community media coming straight from the barrios. <br />
 <br />
* <a href="http://www.medioscomunitarios.org/pag/index.php" target="_blank">www.medioscomunitarios.org/pag/index.php</a>
</p>

</div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(146);
			
				if (SHOWMESSAGES)	{
				
						addMessage(146);
						echo getMessages(146);
						echo showAddMessage(146);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>