<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 747 - 11th November 2010 - Nukes? Gorle-Blimey!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, nuclear energy, germany, direct action, anti-nukes" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 747 Articles: </b>
<p><a href="../archive/news7471.php">Losing Their Faculties</a></p>

<p><a href="../archive/news7472.php">Raven Mad For It</a></p>

<p><a href="../archive/news7473.php">Schmovies Dvd Launch</a></p>

<b>
<p><a href="../archive/news7474.php">Nukes? Gorle-blimey!</a></p>
</b>

<p><a href="../archive/news7475.php">Squat Crackdown, Bristol Fashion</a></p>

<p><a href="../archive/news7476.php">All Party'd Out</a></p>

<p><a href="../archive/news7477.php">Got The Hump Back</a></p>

<p><a href="../archive/news7478.php">Mexico: Day Of The 10,000 Dead</a></p>

<p><a href="../archive/news7479.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 11th November 2010 | Issue 747</b></p>

<p><a href="news747.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NUKES? GORLE-BLIMEY!</h3>

<p>
<p>
	Over 3,000 anti-nuclear protesters took part in the blockading of a shipment of nuclear waste into north-western Germany this week. After activists and local communities spent four days blocking them at every turn, the toxic containers finally arrived at their Gorleben destination on Tuesday. </p>
<p>
	Thanks to a creative approach to getting the &lsquo;No Nuclear&rsquo; message across, the journey took the longest-ever time of 92 hours. It was waylayed by hundreds of locked on protesters, tractors and a Greenpeace vehicle masquerading as a beer truck. Even 400 environmentally-minded sheep blocked the tracks in a case of inter-species solidarity, standing fast until police removed them one by one. </p>
<p>
	The so-called CASTOR containers of German waste are transported roughly once a year from Valognes in France, where they are reprocessed, back to Gorleben, Germany. Critics say that neither the waste containers or the Gorleben temporary storage facility are safe. </p>
<p>
	On Saturday, a further 50,000 people turned out to demonstrate against the shipment in the town of Dannenburg. The mass mobilisation was meant to show Angela Merkel that as far as her new pro-nuclear stance goes, she&rsquo;s in the (radioactive) dog house. </p>
<p>
	Nuclear energy is unpopular in Germany, which felt some of the effects of the Chernobyl disaster in 1986. The country was heralded as a nuclear-free icon after the previous government promised to phase out nuclear power entirely by 2021 - but Merkel backtracked by pushing plans to extend the operating lifespan of Germany&rsquo;s 17 nuclear power plants through parliament on 28th October. </p>
<p>
	The previous phase-out plan had the backing of the German public, but this recent retreat takes an undemocratic hammer to this consensus - for the sake of extra profits for the large energy and nuclear industries. Meanwhile the government is also set to make a mint from demanding millions in extra funds from the energy companies for the transition to renewables. </p>
<p>
	Despite German riot cops normally being the stuff of nightmares, the security operation surrounding the four days of protest seemed relatively calm. The biggest clashes came on Sunday when riot police used water cannons, pepper spray and brute force to try and stop 4,000 protesters making their way through woods to the tracks near Dannenberg. </p>
<p>
	In a &lsquo;police-with-morals&rsquo; shocker, the police have even expressed that they can&rsquo;t be counted on to be the government&rsquo;s lackeys. It has been suggested that many of the officers on the day shared the anti-nuclear sentiment of the protesters. The German police union has stated that they shouldn&rsquo;t have to deal with the fall-out of unpopular political decisions, and besides, they didn&rsquo;t like having to stand out in the cold for up to 20 hours. </p>
<p>
	Union Chief Konrad Freiberg described an &ldquo;absolute&rdquo; lack of support for their efforts, which involved 17,000 officers and cost around 50million Euros. The nuclear companies won&rsquo;t foot a penny of this bill- leaving anti-nuclear taxpayers even more pissed off. </p>
<p>
	* See <a href="http://www.castor2010.de" target="_blank">www.castor2010.de</a> (translate in browser). </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1007), 116); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1007);

				if (SHOWMESSAGES)	{

						addMessage(1007);
						echo getMessages(1007);
						echo showAddMessage(1007);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


