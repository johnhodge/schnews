<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 767 - 15th April 2011 - Inside SchNEWS</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, political prisoners, mumia abu-jamal, iran" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 767 Articles: </b>
<p><a href="../archive/news7671.php">Taking Libya-ties</a></p>

<p><a href="../archive/news7672.php">Southern Climes</a></p>

<p><a href="../archive/news7673.php">Ed-f Off</a></p>

<p><a href="../archive/news7674.php">Mexico: Grave Situation</a></p>

<p><a href="../archive/news7675.php">Benefit Of Doubt</a></p>

<p><a href="../archive/news7676.php">Social Centre Plus</a></p>

<b>
<p><a href="../archive/news7677.php">Inside Schnews</a></p>
</b>

<p><a href="../archive/news7678.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th April 2011 | Issue 767</b></p>

<p><a href="news767.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>INSIDE SCHNEWS</h3>

<p>
<p>  
  	Activists are planning a week of action for <strong>Jock Palfreeman</strong>, an Australian jailed for murder in Bulgaria following a confrontation with fascist thugs in 2007 (see <a href='../archive/news738.htm'>SchNEWS 738</a>). Jock appealed against his conviction in court hearings late last year and early this year but despite mounting evidence of judicial incompetence and corruption, his 20-year sentence was upheld.  </p>  
   <p>  
  	With Jock&rsquo;s final appeal approaching, supporters are aiming to pile the pressure on the Bulgarian authorities. They are calling out for people to organise actions wherever they can and to bombard the Bulgarian Govt with demands for justice. For more information on how to get involved, plus details of who to direct your protests towards (including a sample letter) see <a href="http://www.anarchistsolidarity.wordpress.com" target="_blank">www.anarchistsolidarity.wordpress.com</a>  </p>  
   <p>  
  	* For Jock&rsquo;s full story see <a href="http://www.freejock.com" target="_blank">www.freejock.com</a>  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	Iranian political, social and human rights activists are trying to instigate an international campaign to prevent the execution of <strong>Shirkou Moaarefi</strong>, a young Kurdish political prisoner. They are hoping to spark demonstrations, protest and publicity in Iran and internationally. Moaarefi is due to be hung on May Day.  </p>  
   <p>  
  	* See <a href="http://www.facebook.com/home.php?sk=group_192889200748656" target="_blank">www.facebook.com/home.php?sk=group_192889200748656</a> for details or contact <a href="mailto:Kawah2000@gmail.com">Kawah2000@gmail.com</a>  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	UK campaigners are organising a rally in Brixton against the scheduled execution of journalist, author and activist<strong> Mumia Abu-Jamal</strong>, who has been languishing on death row for three decades.  </p>  
   <p>  
  	A global icon for justice and human-rights and the focus for a worldwide campaign against the death penalty, Mumia was arrested for the murder of a policeman in 1981 and convicted in proceedings riddled with racism, ineptitude and corruption (see <a href='../archive/news584.htm'>SchNEWS 584</a>). Mumia&rsquo;s legal team is currently embroiled in a legal battle to get a new trial.  </p>  
   <p>  
  	Activists will be assembling on April 23rd at 1pm in Windrush Square to march through Brixton. A demonstration will also be held outside the US Embassy when the court&rsquo;s decision is announced. For further information contact Tongogara on 07597078221  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1186), 136); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1186);

				if (SHOWMESSAGES)	{

						addMessage(1186);
						echo getMessages(1186);
						echo showAddMessage(1186);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


