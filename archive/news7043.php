<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 704 - 20th December 2009 - No Oil For War</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, iraq, war on terror, oil" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<div class="bannerGraphic">
			<a href="http://www.smashedo.org.uk/remember-gaza.htm"><img 
						src="../images_main/remember-gaza-banner.png"
						alt="Remember Gaza"
						width="465" 
						height="90" 
						border="0" 
			/></a>
			</div>


</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<a href='../archive/index.htm'>
			<div class='navBar_item'>
				Back Issues
			</div>
		</a>
		<a href='http://www.schnews.org.uk/schmovies/index.php'>
			<div class='navBar_item'>
				SchMOVIES
			</div>
		</a>
		<a href='http://www.schnews.org.uk/features'>
			<div class='navBar_item'>
				Feature Articles
			</div>
		</a>
		<a href='http://www.schnews.org.uk/links/index.php'>
			<div class='navBar_item'>
				Contacts and Links
			</div>
		</a>
		<a href='http://www.schnews.org.uk/diyguide/index.htm'>
			<div class='navBar_item'>
				DIY Guide
			</div>
		</a>
		<a href='http://www.schnews.org.uk/monopresist/index.htm'>
			<div class='navBar_item'>
				Archive
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/about.php'>
			<div class='navBar_item'>
				About Us
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/subscribe.php'>
			<div class='navBar_item'>
				Subscribe
			</div>
		</a>

			
		<!--	SCROOGLE SEARCh BAR 	-->
		<div class='search_bar'>		<form action="https://duckduckgo.com/" method="get"  target="_blank"> 
		
		 						
				<input type="search" placeholder="Search" name="q" maxlength="300" > 
				&nbsp;&nbsp;
				<input type="hidden" name="sites" value="schnews.org"> 
				    	<input type="hidden" value="1" name="kh">
    					<input type="hidden" value="1" name="kn">
    					<input type="hidden" value="1" name="kac">
						<input type="hidden" value="w" name="kw">
						
			<!--<input name="submit2" type="submit" value="Go">-->
			<!--<button type="submit">Search</button>-->
				
		</form>
		</div>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 704 Articles: </b>
<p><a href="../archive/news7041.php">Not A Hopenhagen</a></p>

<p><a href="../archive/news7042.php">Tar Very Much</a></p>

<b>
<p><a href="../archive/news7043.php">No Oil For War</a></p>
</b>

<p><a href="../archive/news7044.php">Nazi Pieces Of Work</a></p>

<p><a href="../archive/news7045.php">Back In Feline-ment</a></p>

<p><a href="../archive/news7046.php">The Livni Daylights</a></p>

<p><a href="../archive/news7047.php">Ahava Go Heroes</a></p>

<p><a href="../archive/news7048.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">
	<img src="../images_main/description-new.png" width="643" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" />
</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Sunday 20th December 2009 | Issue 704</b></p>

<p><a href="news704.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NO OIL FOR WAR</h3>

<p>
The first of the long-planned major sell-offs of Iraqi oil fields took place last weekend. The auction of Production Sharing Agreements (PSAs) in Baghdad has long been fantasised about by Bush, Cheney and the rest of the neoconservative gang that we can now safely call &lsquo;former regime loyalists&rsquo;. <br />   <br /> It didn&rsquo;t exactly go as planned for the Americans though. In a bidding war that Iraqi officials insisted was &lsquo;not political,&rsquo; virtually everywhere except the USA scored rights to share the profits (sorry development) of some of Iraq&rsquo;s major oilfields. <br />    <br /> Rights to explore/exploit Iraq&rsquo;s biggest oilfields went first to China and Russia, with European oil corporations having to compete with Asian companies. SchNEWS&rsquo; perennial favourite Shell managed to get its grubby fingers round some of Iraq&rsquo;s oil pumps by partnering up with Petronas, a Malaysian corp. <br />   <br /> Meanwhile US oil corporations won virtually nothing. Neither Chevron nor ConnocoPhilips, both of which had been lobbying heavily, won anything. Exxon was the only US corporation to buy the rights to any large oilfield. <br />   <br /> Its not just that the US corporations were sidelined. The main winners were national corporations - Gazprom (Russia&rsquo;s nationalised oiligarchy), the China National Petroleum Corporation, Norwegian Statoil, Malaysia&rsquo;s Petronas,  France&rsquo;s Total and Angola&rsquo;s Sonangol. <br />   <br /> After several years of marching behind &lsquo;No War For Oil&rsquo; banners and statements from former US politicians that the war was &lsquo;mostly about oil&rsquo;, this seems like pretty weak stuff. The Americans spent some $2trillion on the war. There&rsquo;s also the small matter of the million of lives lost. But the Iraqi government has been desperate to play the nationalist card and to let US corporations in would be politically disastrous; European and Asian corporations are a little more palatable to Iraqis. <br />  <br /> Yet these deals between Iraqi politicians and oil execs totally ignore the wishes of the Iraqi people, unions and oil workers. Sami Ramadani, British-based spokesman for the Iraqi General Union of Oil Employees, had this to say, &ldquo;<em>The barometer of public opinion is the oil union. They think that these contracts with the corporations will give Iraqis a very poor deal. The corporations want to use cheaper imported labour, like from Dubai and Kuwait. Our oil is very close to the surface, we don&rsquo;t need outside investments to pump the oil out. Why do we need these guys except to steal a maximum proportion of profits?</em>&rdquo; <br />  <br /> As well as this, no actual oil law has been agreed on, leaving open the possibility of legal challenges. And let&rsquo;s not forget there is always strike action. <br />  <br /> Ever since the invasion, the US has faced the same problem in Iraq: they&rsquo;ve never had any real power over the country. Bombing the shit out of somewhere isn&rsquo;t the same as controlling it. In fact (cue all of the lessons of the war on terror) bombing the shit out of somewhere is likely to make it much less controllable. The Americans invaded Iraq, destroyed its Baathist state and forced &lsquo;democracy&rsquo; on them. The government that Iraqis elected is nationalist and, in the last few years, has done some very clever political manoeuvring to limit US influence in their country. Just before last year&rsquo;s US elections Iraqi PM Nuri al Maliki sweet-talked Obama - basically handing him a vote winning &lsquo;get out of Iraq free&rsquo; card just before he got elected. <br />   <br /> Added to their problem is that US corporations shot themselves in the foot by being just too damn greedy. The Iraqi government (we can use the word without inverted commas these days) had insisted on no more than a $2 per barrel profit for corporate Production Sharing Agreements. The Americans weren&rsquo;t happy about this and set their side of the deal much higher, only to find that they&rsquo;d been out-bid by the Angolans. <br />   <br /> With violence on the increase, an increasingly authoritarian president with a tight grip on power and their natural resources divied up between foreign corporations, things aren&rsquo;t looking much improved for the average Iraqi. <br />   <br /> But with nothing to show for their trillion dollar foreign adventure but over a million dead and a shattered international reputation, things haven&rsquo;t quite gone the way the Americans wanted either. - oh well, at least W. and friends can be content that a slew of US corporations got in a few years of unregulated looting with their war, and while the causalities mounted, managed to suck a vast amount of American taxpayer cash straight into their pockets as profits into the bargain.  Never  mis-under-estimate the scale of what happened in Iraq...  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=iraq&source=704">iraq</a>, <a href="../keywordSearch/?keyword=oil&source=704">oil</a>, <a href="../keywordSearch/?keyword=war+on+terror&source=704">war on terror</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(610);
			
				if (SHOWMESSAGES)	{
				
						addMessage(610);
						echo getMessages(610);
						echo showAddMessage(610);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<font face="Arial, Helvetica, sans-serif"> 
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p></font>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>
		
</div>








</div>




</body>
</html>


