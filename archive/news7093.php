<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 709 - 12th February 2010 - I-ran Away</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, iran, deportation, immigration, hunger strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 709 Articles: </b>
<p><a href="../archive/news7091.php">Slung Out On Yer Frontier</a></p>

<p><a href="../archive/news7092.php">Carmel Agrexco : Slay It With Flowers</a></p>

<b>
<p><a href="../archive/news7093.php">I-ran Away</a></p>
</b>

<p><a href="../archive/news7094.php">Serco-mplicit  </a></p>

<p><a href="../archive/news7095.php">Cold Turkey  </a></p>

<p><a href="../archive/news7096.php">West Bank Story </a></p>

<p><a href="../archive/news7097.php">Flying In The Face</a></p>

<p><a href="../archive/news7098.php">Sus-sex Appeal</a></p>

<p><a href="../archive/news7099.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th February 2010 | Issue 709</b></p>

<p><a href="news709.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>I-RAN AWAY</h3>

<p>
As opposition protesters clashed with security forces  in Iran yesterday (11th) on the anniversary of the 1979 revolution, a campaigner against the regime is entering the fourth week of her hunger strike in a desperate attempt to avoid deportation from Britain. If she returns to Iran she faces almost certain execution. <br />   <br /> Bita Ghaedi, 24, of Whetstone, London, has been refused an asylum application by British Immigration authorities and placed on a fast track scheme by the UK Border Agency, which means she may be deported at any time. <br />  <br /> Having fled from Iran after two decades of beatings and mental torture at the hands of her family and former lover, she arrived in the UK only to be arrested and incarcerated for 45 days in Holloway prison and then held at Yarl&rsquo;s Wood detention centre whilst UK authorities assessed her claim. <br />   <br /> Bita said, &ldquo;<em>I feel my life is going to be finished. I&rsquo;m so scared. I can&rsquo;t sleep during the night, I think immigration will come any time and take me away. It&rsquo;s better for me to die here</em>.&rdquo; <br />   <br /> Afraid that she will be hunted down by her family and killed upon her return, she now also faces torture and execution at the hands of the state for her involvement in the demonstrations against the Iranian regime last year. <br />   <br /> Since her release from Yarl&rsquo;s Wood she has campaigned endlessly for regime change in Iran, a country where she says women are &ldquo;treated like slaves&rdquo;. <br />   <br /> During the crisis in Ashraf City in Iraq last year - where Iraqi security forces attacked an Iranian encampment and killed 11 people in their attempt to force them back to Iran for execution &ndash; a solidarity campaign lasting 72 days erupted all around the world, calling for the release of the 36 key members arrested. <br />   <br /> Ghaedi campaigned outside both Downing St and the US embassy and went on hunger strike in protest.  The 36 were eventually released no thanks to the US authorities who refused to intervene. No surprises there. <br />  <br /> Campaigns like these, organised by the National Council for the Resistance of Iran (NCRI) and the Peoples Mujahedin Organisation of Iran (PMOI) have put her in direct confrontation with the Mullahs of Iran. Her Iranian boyfriend, Moshen Zadshir, is also known for his work as a long-time opponent of the clerical regime and spent 12years in prison for his views. <br />   <br />  As the Iranian mullahs wrestle to gain control of its citizens, those active against the religious regime have been officially declared &lsquo;mohareb&rsquo;, which literally means &lsquo;enemies of God&rsquo;, an offence punishable by death. Public executions have been taking place to uphold the &lsquo;velayat-e faqih&rsquo; (absolute rule of the clergy) by violent force. <br />   <br /> As Ahmad Jannati, secretary of the &lsquo;guardian council&rsquo; of the regime, said, &ldquo;<em>Rioters [and] advocates of corruption on earth...those who try to shatter the structure [of the regime]...must not be treated with compassion. It is now time for toughness</em>&rdquo;. <br />   <br /> Despite this, the end of 2009 saw Iranians rising up against the fraudulent re-election of Mahmoud Ahmadinejad and yesterday, opposition was evident on the streets of the capital as protestors and police violently clashed. <br />   <br /> Ghaedi now faces deportation to a country actively searching for activists to make an example of. Sign the petition to stop her deportation today. <br />  <br /> *see <a href="http://www.gopetition.co.uk/online/33778.html" target="_blank">www.gopetition.co.uk/online/33778.html</a>  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=deportation&source=709">deportation</a>, <a href="../keywordSearch/?keyword=hunger+strike&source=709">hunger strike</a>, <a href="../keywordSearch/?keyword=immigration&source=709">immigration</a>, <a href="../keywordSearch/?keyword=iran&source=709">iran</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(653);
			
				if (SHOWMESSAGES)	{
				
						addMessage(653);
						echo getMessages(653);
						echo showAddMessage(653);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


