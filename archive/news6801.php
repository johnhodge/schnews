<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 680 - 19th June 2009 - Cops, Lies And Videotape</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, schmovies, sussex police, police harassment, civil liberties, smash edo" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 680 Articles: </b>
<b>
<p><a href="../archive/news6801.php">Cops, Lies And Videotape</a></p>
</b>

<p><a href="../archive/news6802.php">Yarl&#8217;s Wood Hunger Strikers Attacked  </a></p>

<p><a href="../archive/news6803.php">Clean Sweep</a></p>

<p><a href="../archive/news6804.php">Ofog Of War</a></p>

<p><a href="../archive/news6805.php">Darkest Peru</a></p>

<p><a href="../archive/news6806.php">Aldermast-off</a></p>

<p><a href="../archive/news6807.php">Guerilla Garden To Stay</a></p>

<p><a href="../archive/news6808.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/680-b-b-lg.jpg" target="_blank">
													<img src="../images/680-b-b-sm.jpg" alt="...The next category is 'most police repression on an independent film'" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 19th June 2009 | Issue 680</b></p>

<p><a href="news680.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COPS, LIES AND VIDEOTAPE</h3>

<p>
<strong>AS SUSSEX POLICE RAID BRIGHTON FILM MAKER</strong> <br />  <br /> Given the amount of aggro handed out to those brave enough to document Babylon&rsquo;s excesses we wondered how long it was before we started getting our collars felt. Of course they&rsquo;d need a task force to batter down the steel doors of the SchNEWS bunker - let alone take us alive - so Sussex Police took a more indirect route. <br />  <br /> At approximately 8.30am on Tuesday 2nd June, Sussex cops arrived at the home of Paul Light, now revealed as one of the SchMOVIES collective. (For anyone thinking that SchNEWS was still stuck in the Dark Ages with just a scrappy sheet of A4, we&rsquo;ve moved right into the twentieth century and have our own slightly estranged film-making department.) Paul&rsquo;s arrest has wide ranging implications for anyone who reports on controversial issues.&nbsp; <br />  <br /> When the cops came round Paul was getting his ten-year old son ready for school. He was immediately arrested on suspicion of being involved in the &lsquo;decommissioning&rsquo; of the EDO ITT weapons factory in January (see <a href='../archive/news663.htm'>SchNEWS 663</a>). The actual charge was &lsquo;<strong>conspiracy to commit criminal damage</strong>&rsquo;. <br />  <br /> This vague charge was enough for the police to launch into a full-blown fishing expedition.&ldquo;<em>The only evidence they [Sussex police] have is that someone phoned me, asking if I wanted to film the police response</em>,&rdquo; Paul told SchNEWS, &ldquo;<em>I said I couldn&rsquo;t because I had my son and his friend sleeping over that night. That was the end of it as far as I was concerned. As a film maker I frequently get phone calls about possible incidents to film &ndash; this was one shoot I couldn&rsquo;t and didn&rsquo;t make</em>.&rdquo; <br />  <br /> Paul was held and questioned for eight hours and his home was raided. The police took Paul&rsquo;s son to school in a police car, where he burst into tears due to the stress. His father was released on bail and is awaiting the outcome of the arrest. <br />  <br /> Police took every item that could possibly be used for data storage as evidence, including cameras, computers, external hard drives, software, mobile phones, personal accounts, diaries and even his music collection.&ldquo;<em>Put bluntly they have concocted this arrest to see what material I have</em>,&rdquo; said Paul, who regularly films at protests and demonstrations. <br />  <br /> Police have recently attempted to use a film credited to SchMOVIES as evidence in court. The film &lsquo;Batons and Bombs&rsquo; documents events at last year&rsquo;s Carnival Against the Arms Trade (See <a href='../archive/news634.htm'>SchNEWS 634</a>) and was downloaded from the web by cops. They were refused its use as evidence in court on the grounds that the film was edited and they did not possess the original footage. The Crown Prosecution Service are currently appealing that decision via judicial review. The discovery of the original footage would greatly strengthen their hand. <br />  <br /> &ldquo;<em>The raid has effectively shut me down and I am no longer able to make films</em>,&rdquo; said Paul. &ldquo;<em>Film work is my livelihood, so effectively they have put me on the dole</em>.&rdquo; Paul makes films about many different campaigns and issues, and the arrest and raid has been a major setback in his ability to earn a living.&ldquo;<em>They have left me with nothing</em>,&rdquo; he said, &ldquo;<em>I have three commissions in production at the moment and I have had to phone people up and tell them the news &ndash; that their films will have to be postponed indefinitely!</em>&rdquo; <br />  <br /> * See <a href="../schmovies" target="_blank">www.schnews.org.uk/schmovies</a> <br />  <br /> * <strong>SchMOVIES is not new to controversy</strong>. The release of the 2008 film &ldquo;<strong>On The Verge</strong>&rdquo; (<a href="../schmovies/index-on-the-verge.htm" target="_blank">www.schnews.org.uk/schmovies/index-on-the-verge.htm</a>), a documentary about the Smash EDO campaign was met with bans across the UK by various police forces, claiming the film would need certification to be publicly viewed (See <a href='../archive/news626.htm'>SchNEWS 626</a>). <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=civil+liberties&source=680">civil liberties</a>, <span style='color:#777777; font-size:10px'>police harassment</span>, <span style='color:#777777; font-size:10px'>schmovies</span>, <a href="../keywordSearch/?keyword=smash+edo&source=680">smash edo</a>, <a href="../keywordSearch/?keyword=sussex+police&source=680">sussex police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(365);
			
				if (SHOWMESSAGES)	{
				
						addMessage(365);
						echo getMessages(365);
						echo showAddMessage(365);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>