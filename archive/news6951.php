<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 695 - 16th October 2009 - La La La Bomba</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, anarchists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 695 Articles: </b>
<b>
<p><a href="../archive/news6951.php">La La La Bomba</a></p>
</b>

<p><a href="../archive/news6952.php">Kicking Up A Stink</a></p>

<p><a href="../archive/news6953.php">Harverster Loons</a></p>

<p><a href="../archive/news6954.php">Quids Pro Quo</a></p>

<p><a href="../archive/news6955.php">Art Attack</a></p>

<p><a href="../archive/news6956.php">Fash In The Pan</a></p>

<p><a href="../archive/news6957.php">695 Schnews In Brief</a></p>

<p><a href="../archive/news6958.php">Just Deserts</a></p>

<p><a href="../archive/news6959.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th October 2009 | Issue 695</b></p>

<p><a href="news695.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LA LA LA BOMBA</h3>

<p>
<p class="MsoNormal"><strong>SchNEWS LOOKS TO <st1:country-region w:st="on"><st1:place w:st="on">MEXICO</st1:place></st1:country-region>, WHERE BUTANE IS IN THE EYE OF THE BOMB-HOLDER</strong> <br />  <br /> Amid the annual September celebrations of independence and revolution, a series of explosions hit symbols of capitalism across Mexico, each accompanied by anarchist graffiti. What the Mexican press have labelled the &lsquo;Anarcho-bombings&rsquo; has since been used by the authorities to launch an attack on Mexico&rsquo;s autonomous universities &ndash; hotbeds of radical politics and action. <br />  <br /> After the first explosion on September 1st, the improvised butane bombs went off once or twice a week throughout the month, torching banks, car-showrooms, fast food restaurants and animal testing labs, causing thousands of pounds of damage but no injuries. As well as classic circled &lsquo;A&rsquo;s, the bombers daubed the targets with slogans denouncing animal rights abuses, demanding an end to prison construction and calling for an end to capitalism. <br />  <br /> A previously unknown group called the Subversive Alliance for the Liberation of the Earth, Animals and Humans (ASLTAH) claimed responsibility for the bombings in an internet communiqu&eacute;, raging, &ldquo;<em>your filthy techno-industrial system provokes our rage and hatred and we say to you now that we will not stop until we see your ashes</em>.&rdquo; <br />  <br /> On September 30th, federal agents arrested Ramses Villareal Gomez, a 27 year-old student at the Autonomous Metropolitan University (UAM) in Mexico City, in connection with the bombings. Villareal Gomez claims police threatened him with a 40-year sentence and said they would rape his wife if he did not co-operate in their investigations. Cops also searched his mother&rsquo;s house, reportedly stealing money and two computers. They initially claimed they found a rifle, a pistol, explosives and documentation linking him to a &ldquo;subversive&rdquo; movement in the search. <br />  <br /> Police claim they arrested Villareal Gomez after a newspaper clipping of the bombs with &ldquo;Ramses&rdquo; was left in an anonymous tip box. Three days later, following violent protests by anarchist groups, a judge ordered the authorities to release Villareal Gomez due to a lack of evidence. After his release  Police admitted that they had been &ldquo;mistaken&rdquo; about the weapons, explosives and incriminating documents. <br />  <br /> Since the arrest, a case dossier has been leaked to the Mexican media. Of the suspects named in the dossier, at least 15 are students at public universities and high schools. All are under 26. The dossier links the suspects to organisations such as the People&rsquo;s Front in Defence of the Land (FPDT) in Atenco (See <a href='../archive/news543.htm'>SchNEWS 543</a>), the Popular Assembly of the People of Oaxaca (APPO - see <a href='../archive/news567.htm'>SchNEWS 567</a>), the National Autonomous University of Mexico (UNAM) General Strike Council, and &ldquo;national and international insurgent organisations.&rdquo; <br />  <br /> The Mexican authorities have been engaged in a struggle with students and academics over how autonomous universities are run since the UNAM opened in 1910. The universities have control over their budgets and the appointment of rectors and regents. They also prohibit the police and the military from entering the campuses without the rector&rsquo;s permission and deny the authorities access to student records and biographies. <br />  <br /> The level of political freedom and protection from repression and harassment on autonomous campuses has created an environment where students have been able to play important roles in political struggles. As well as the groups mentioned in the dossier, students have been particularly active in the Zapatista movement (See <a href='../archive/news250.htm'>SchNEWS 250</a>), and the man the authorities claim is Subcomandante Marcos, Rafael Sebasti&aacute;n Guill&eacute;n Vicente, was even a student at UNAM and a professor at UAM. <br />  <br /> Some students have also been involved in solidarity campaigns with the Revolutionary Armed Forces of Colombia (FARC), who are at the centre of Colombia&rsquo;s bloody civil war (see <a href='../archive/news656.htm'>SchNEWS 656</a>). Last year four Mexican students were killed in the Colombian military&rsquo;s airstrikes in Ecuador which killed FARC commander Raul Reyes. One Mexican student, Lucia Morett, survived the attack. <br />  <br /> Following the bombings, the search for Morett and her associates led to increased harassment and surveillance on campus activists under the pretext of &lsquo;combating terrorism&rsquo;. Morett&rsquo;s name was also in the &lsquo;Anarcho bombings&rsquo; dossier&rsquo;. <br /> The dossier claims Morett was in contact with Villareal Gomez, a charge she denies. Not one of the newspapers which have a copy of the dossier has printed any evidence related to Morett, Villareal Gomez or any of the 15 students. <br />   <br /> Since the bombings, the ASLTAH have been making noise about &ldquo;the battle for the dissolution of civilisation&rdquo;. But, in a state riven with social strife and in danger of being overwhelmed by a bloody narco war, it is unlikely anyone is taking a fringe group of pyro-enthusiasts too seriously. The radicals of Oaxaca, Chiapas and Atenco however, remain a serious challenge to the status quo and the autonomous universities a real tool in their struggle. The &lsquo;Anarcho-bombings&rsquo; now look set to be leapt on by the Mexican authorities as another opportunity to try and repress that struggle. <br />  <br /> * See <a href="http://www.narconews.com" target="_blank">www.narconews.com</a> and <a href="http://www.counterpunch.org/ross10062009.html" target="_blank">www.counterpunch.org/ross10062009.html</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>anarchists</span>, <a href="../keywordSearch/?keyword=mexico&source=695">mexico</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(526);
			
				if (SHOWMESSAGES)	{
				
						addMessage(526);
						echo getMessages(526);
						echo showAddMessage(526);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>