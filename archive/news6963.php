<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 696 - 23rd October 2009 - It&#8217;s Brim Up North</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, arms trade, sussex police, manchester" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 696 Articles: </b>
<p><a href="../archive/news6961.php">Sooty & Swoop</a></p>

<p><a href="../archive/news6962.php">Edo Aquittals  </a></p>

<b>
<p><a href="../archive/news6963.php">It&#8217;s Brim Up North</a></p>
</b>

<p><a href="../archive/news6964.php">Cymru Have A Go If You Think Yer Hard Enuff</a></p>

<p><a href="../archive/news6965.php">Bright Sparks</a></p>

<p><a href="../archive/news6966.php">Schnews In Brief</a></p>

<p><a href="../archive/news6967.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd October 2009 | Issue 696</b></p>

<p><a href="news696.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IT&#8217;S BRIM UP NORTH</h3>

<p>
Britain&rsquo;s newest anti-arms campaign, <strong>Target Brimar</strong>, staged their inaugural demo on Saturday (17th). Target Brimar are, as it says on the tin, targeting Brimar, a Greater Manchester based arms-components factory that manufactures targeting systems for Apache helicopters and tanks, among other killing machines (see <a href='../archive/news694.htm'>SchNEWS 694</a>). <br />  <br /> Around 70 protesters marched on the Brimar site, including <strong>Critical Mass</strong>. There was a family atmosphere with speeches, food, bike-powered sound systems and even a kids play area, all under the watchful eye of security and cops. <br />     <br /> Also present were two of Sussex&rsquo;s finest EDO cops, CID&rsquo;s DC Mark Butcher and our very own increasingly supersized David Brent-alike, Sean MacDonald. Both had decided to travel up for a spot of protester harassment outside their usual jurisdiction. <br />   <br /> Once there, Brighton&rsquo;s finest urged Manchester police to take a liberal interpretation of the law and arrest three people bailed in connection to the EDO decommissioning (see <a href='../archive/news663.htm'>SchNEWS 663</a>) for breaching their &lsquo;not to associate&rsquo; bail conditions. Just one snag: the bail conditions were &lsquo;not to contact&rsquo; and the three didn&rsquo;t speak and remained at a distance from each other the whole time..  <br />  <br /> The police held them for exactly 48 hours, moving them between three police stations. The three appeared in court on Monday morning with police claiming that although the three had not spoken, they could hold them as they had associated by being at the demo.  <br />  <br /> The farce continued as the police came to court without a copy of the bail conditions and spent the first hour trying to get one. Meanwhile the clock on how long the three could be held  without a magistrate&rsquo;s decision slowly counted down towards the 48hr mark and freedom.  <br />  <br /> With just an hour and a half left police began proceedings in an attempt to remand the defendants. Legal chaos ensued as cops frantically tried to blur the line between &lsquo;association&rsquo; and &lsquo;contact&rsquo;. Proceedings only managed to get half-way through the first defendant&rsquo;s evidence before hitting 1.18pm, exactly 48 hours after their incarceration. The three were then released, without charge, missing two days of their lives.  <br />  <br /> In the meantime Sean MacDonald had clearly been left unsupervised and absconded to Brighton with one of the defendants&rsquo; phones. It was taken from the evidence bag in the police station to an undisclosed location in the south of England, thought to be Sean&rsquo;s secret lair, and&nbsp;not one of the two Brighton cop shops.  The defendant is still waiting for their phone back despite a legal right to claim it as soon as the case collapsed. <br />  <br /> Despite the dodgy arrests it was a successful show and tell &ndash; showing everyone where the Brimar site is and telling &lsquo;em what goes on there.  Even at this early stage it looks like the campaign is having an impact. Target Brimar&rsquo;s internet service provider have been contacted and asked to remove the campaign group&rsquo;s website as they are extremists. Looks like someone is worried... <br />       <br /> * See www.targetbrimar.org.uk <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=696">arms trade</a>, <a href="../keywordSearch/?keyword=manchester&source=696">manchester</a>, <a href="../keywordSearch/?keyword=sussex+police&source=696">sussex police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(537);
			
				if (SHOWMESSAGES)	{
				
						addMessage(537);
						echo getMessages(537);
						echo showAddMessage(537);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>