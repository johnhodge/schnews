<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 796 - 11th November 2011 - Tour of Duty</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 796 Articles: </b>
<p><a href="../archive/news7961.php">Nov 9 Summed Up</a></p>

<p><a href="../archive/news7962.php">High Sieze</a></p>

<p><a href="../archive/news7963.php">Having A Field Day</a></p>

<p><a href="../archive/news7964.php">Remember, Remember</a></p>

<b>
<p><a href="../archive/news7965.php">Tour Of Duty</a></p>
</b>

<p><a href="../archive/news7966.php">Maison D'etre</a></p>

<p><a href="../archive/news7967.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7968.php">R.i.p. Mark Rivers</a></p>

<p><a href="../archive/news7969.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th November 2011 | Issue 796</b></p>

<p><a href="news796.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TOUR OF DUTY</h3>

<p>
<p>  
  	Smash Edo is going on tour around the country gathering support for the 2012 Summer of Resistance. The workshop will include a rundown of the campaign so far, a direct action workshop and a chance to plan actions for next summer in Brighton. Come along and find out how to get involved.  </p>  
   <p>  
  	Confirmed dates so far:  </p>  
   <p>  
  	<u><strong>NOVEMBER </strong></u>  </p>  
   <p>  
  	<strong>13th: Manchester </strong>- OKasional Cafe, 7pm, see <a href="http://www.okcafe.wordpress.com" target="_blank">www.okcafe.wordpress.com</a>  </p>  
   <p>  
  	<strong>14th: Sheffield</strong> &ndash; Occupy Sheffield, 1pm and 7pm, see <a href="http://www.occupysheffield.org.uk" target="_blank">www.occupysheffield.org.uk</a>  </p>  
   <p>  
  	<strong>15th: Liverpool </strong>&ndash; Next to Nowhere, 7pm, see&nbsp; <a href="http://www.liverpoolsocialcentre.org/index.php/calendar/month.calendar/2011/11/28/-.html" target="_blank">www.liverpoolsocialcentre.org/index.php/calendar/month.calendar/2011/11/28/-.html</a>  </p>  
   <p>  
  	<strong>16th: Edinburgh</strong> &ndash; Autonomous Centre of Edinburgh, 7pm, see <a href="http://www.autonomous.org.uk" target="_blank">www.autonomous.org.uk</a>  </p>  
   <p>  
  	<u><strong>DECEMBER </strong></u>  </p>  
   <p>  
  	<strong>3rd: Manchester and Salford</strong> Anarchist Bookfair,&nbsp; People&rsquo;s History Museum &ndash; Time TBA, see <a href="http://www.underthepavement.org/bookfair" target="_blank">www.underthepavement.org/bookfair</a>  </p>  
   <p>  
  	<strong>4th: Newcastle </strong>&ndash; Star and Shadow, 4pm, see <a href="http://www.starandshadow.org.uk/on/201/12/04/?os=2" target="_blank">www.starandshadow.org.uk/on/201/12/04/?os=2</a> &nbsp; <br />  
  	Weekly noise demos are still occuring every Wednesday, 4-6pm outside the EDO ITT/MBM factory in Mouslecoombe  </p>  
   <p>  
  	A commemoration demo in honour of long term Smash EDO campaigner Mark Rivers, who sadly passed away on Saturday (5th) will be held soon. Check smashedo.org.uk for the announcement.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1424), 165); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1424);

				if (SHOWMESSAGES)	{

						addMessage(1424);
						echo getMessages(1424);
						echo showAddMessage(1424);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


