<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 759 - 18th February 2011 - Middle Eastern Promise</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dictatorship, egypt, mubarak, arab revolt" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 759 Articles: </b>
<b>
<p><a href="../archive/news7591.php">Middle Eastern Promise</a></p>
</b>

<p><a href="../archive/news7592.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7593.php">Commission Accomplished</a></p>

<p><a href="../archive/news7594.php">Shac To The Lab Again</a></p>

<p><a href="../archive/news7595.php">Supermarket Sabo-taj</a></p>

<p><a href="../archive/news7596.php">Cheesed Off</a></p>

<p><a href="../archive/news7597.php">Bunga Jump</a></p>

<p><a href="../archive/news7598.php">Shire Madness</a></p>

<p><a href="../archive/news7599.php">Dale Farm Callout</a></p>

<p><a href="../archive/news75910.php">Commie Garden</a></p>

<p><a href="../archive/news75911.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/759-egypt-lg.jpg" target="_blank">
													<img src="../images/759-egypt-sm.jpg" alt="Victory for Egyptian Revolution!!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 18th February 2011 | Issue 759</b></p>

<p><a href="news759.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MIDDLE EASTERN PROMISE</h3>

<p>
<p>
	<strong>AS PEOPLE POWER PUTS ARAB DICTATORSHIPS TO THE TEST</strong> </p>
<p>
	Egyptian dissidents (along with the masses) celebrated on Friday as Hosni Mubarak finally threw in the towel after the mass protests in Cairo&rsquo;s Tahrir Square and across the country refused to abate (and presumably the Americans finally ordered him to load up the plane with gold and go in an attempt to ensure the power structures &ndash; and thus their influence &ndash; didn&rsquo;t collapse completely). But the Egyptian revolution is not yet won as the military have stepped in, repressed protest and threatened to declare martial law. </p>
<p>
	Nonetheless there was jubilation across the country following last weeks events. The Egyptian people who have lived most, if not all, of their lives under Mubarak&rsquo;s rigid military regime had successfully shaken off the dictator. But the question is what will happen now? On Sunday the army removed the protesters from Tahrir square. The fact that the army prevented cameras and reporters from recording the eviction, thus continuing the media blackout and repression of free speech of the Mubarak regime should act as a warning. </p>
<p>
	The Egyptian revolution, like the Tunisian uprising, is ongoing; away from Tahrir Square, strikes have been held by everyone from airport staff, public transport workers and nurses in Cairo to workers in the sweatshops of Mahalla al-Koubra and Mansoura to oil industry employees. </p>
<p>
	Egyptian banks, briefly reopened last week are now closed and the stock-exchange lies dormant. The demands of the strikers represent the other side of the Egyptian rebellion, a call for an end to corruption, for better wages and for lower prices. Predictably, Egypt&rsquo;s new military rulers have called for an end to the strikes on the grounds of &lsquo;security of the nation&rsquo; while Egypt&rsquo;s caretaker finance minister is preaching the language of austerity. </p>
<p>
	The dissidents still have everything to win. If the Egyptian revolution is to achieve more than a cosmetic regime change, replacing one system of oppression with another, the struggle must challenge the military&rsquo;s power. Without that, Mubarak&rsquo;s departure will simply herald a consolidation of US colonialism in Egypt. The military, now, will preside over a reorganisation of power in negotiation with the US &ndash; the regime&rsquo;s bankrollers. If it is up to them, this will mean a tightening of imperialism&rsquo;s grip over the Egypt, either through a new dictatorship or through a move to cement control through a parliamentary democracy subjugated to US control, similar to the puppet &lsquo;democracies&rsquo; already in place in Iraq and the West Bank. </p>
<p>
	SchNEWS spoke to a Brighton anarchist who rushed to Cairo for a spot of revolutionary goal hanging last week. He said, &ldquo;The Tahrir occupation was a shining example of grassroots organising in action. Occupiers set up community barricades, crewed by both male and female volunteers, complete with piles of rocks to use as weapons in case of attack. Medical clinics were set up staffed by volunteer doctors and surgeons. Hundreds of tents and shelters were erected around the square, rubbish collection was organised and food was distributed. At night the occupiers slept in front of the tanks surrounding the square to prevent the army from entering.&rdquo; </p>
<p>
	<strong>A HARD BAHRAIN&#39;S GONNA FALL</strong> </p>
<p>
	Inspired by events in Egypt and Tunisia, (where President Zine El Abidine Ben Ali was forced to flee the country on the 14th January), popular protests have ripped through the Middle East this week in Iran, Yemen, Algeria, Libya. </p>
<p>
	In Manama, Bahrain, protesters mimicking the Tahrir occupiers have occupied the central Pearl Square after two anti-government activists were killed by police. Iranian and Libyan governments have taken a leaf out of Mubarak&rsquo;s book and restricted access to the internet while, in the West Bank, an election has been called to stave off murmurings of a popular uprising. </p>
<p>
	Meanwhile, thousands of protesters have been demonstrating for democracy in Yemen and the president has already promised to step down this year. In Algeria the obligatory thousands took to the streets on the 13th carrying Egyptian and Tunisian flags. Libyans fought police with petrol bombs in Benghazi on Wednesday amid calls for a &lsquo;day of rage&rsquo; against Colonel Gaddafi&rsquo;s regime and anti-government protests have been reignited in Iran. In Tunisia, where it all began, the revolution continues: with attacks on police cars and security buildings as activists call for the dismissal of officials with ties to the old regime. </p>
<p>
	&ldquo;<em>Whatever people say, there are no leaders here, the square and the uprising belong to the people</em>&rdquo;- <strong>Tahrir Square demonstrator</strong> </p>
<p>
	Read first-hand reports from Egypt and elsewhere at <a href="http://www.reportsfromthegyptianuprising@wordpress.com" target="_blank">www.reportsfromthegyptianuprising@wordpress.com</a> <a href="http://</p>
<p>
	www.occupiedlondon.org/cairo" target="_blank"></p>
<p>
	www.occupiedlondon.org/cairo</a> <a href="http://</p>
<p>
	www.arabawy.org" target="_blank"></p>
<p>
	www.arabawy.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1104), 128); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1104);

				if (SHOWMESSAGES)	{

						addMessage(1104);
						echo getMessages(1104);
						echo showAddMessage(1104);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


