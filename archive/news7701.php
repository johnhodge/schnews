<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 770 - 6th May 2011 - Baa Baa Black Block</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police, m26" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT June 1st 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 770 Articles: </b>
<b>
<p><a href="../archive/news7701.php">Baa Baa Black Block</a></p>
</b>

<p><a href="../archive/news7702.php">Tomlinson Verdict</a></p>

<p><a href="../archive/news7703.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7704.php">Totally Stoked</a></p>

<p><a href="../archive/news7705.php">Maydays Of Their Lives</a></p>

<p><a href="../archive/news7706.php">Indymedia: From The Rubble Of Double Trouble</a></p>

<p><a href="../archive/news7707.php">Spineless Facebook</a></p>

<p><a href="../archive/news7708.php">A Different Thanet</a></p>

<p><a href="../archive/news7709.php">The Lying Dutchman</a></p>

<p><a href="../archive/news77010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/770-sheep-lg.jpg" target="_blank">
													<img src="../images/770-sheep-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 6th May 2011 | Issue 770</b></p>

<p><a href="news770.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BAA BAA BLACK BLOCK</h3>

<p>
<p>  
  	<strong>AS COPS LAUNCH ACTIVIST WITCHUNT AFTER MARCH 26TH ACTIONS</strong>  </p>  
   <p>  
  	Operation Brontide swung into full force last week, coincidentally just before the Royal wedding. Cops claim to be after 276 people for offences including violent disorder and criminal damage committed during the March for the Alternative on 26th March (see <a href='../archive/news765.htm'>SchNEWS 765</a>). In fact this is&nbsp; a legally dubious fishing exercise designed to seize equipment and display state power.  </p>  
   <p>  
  	The Met followed up Wednesday morning&rsquo;s raids in Hove (see <a href='../archive/news769.htm'>SchNEWS 769</a>) with a number of &ldquo;proactive raids across London&rdquo;, including Offmarket, Transition Heathrow, Petrosiege and Ratstar Social Centre, while images of &ldquo;suspects&rdquo; have been circulated amongst regional forces. These raids are allegedly &ldquo;part of an intelligence led operation&rdquo;; yet only two arrests were made directly related to the warrants executed at seven properties, neither of which have resulted in any charges.  </p>  
   <p>  
  	The police are treating anyone who was in the convergence space or near the black bloc as under suspicion of Conspiracy to Commit Criminal Damage and Conspiracy to Commit Violent Disorder. The logic being if you wore similar clothes to someone who smashed a window or punched a copper, you must have spoken to them about it beforehand. Obviously they would never even be able to charge, let alone get a conviction, based on this &lsquo;evidence&rsquo;. However the Met are willing to raid houses, seize phones and laptops, transport people to London, interview them, then release them on police bail with no way of getting home.  </p>  
   <p>  
  	Having spoken to several of those already hauled in, the interrogators&rsquo; level of intelligence seems to be extremely low all round. Highlights include questions such as &ldquo;have you ever been to a Black Bloc squat?&rdquo; and &ldquo;is the Black Bloc an organisation you hold dear to your heart?&rdquo;&nbsp; Most people are then being released on police bail, which begs the question of why bother nicking them&nbsp; in the first place with bugger all evidence?  </p>  
   <p>  
  	If you&rsquo;re worried about being nicked yourself check out the advice from one arrestee (<a href="http://indymedia.org.uk/en/2011/05/478691.html" target="_blank">http://indymedia.org.uk/en/2011/05/478691.html</a>) and FITwatch (<a href="http://northern-indymedia.org/articles/1079" target="_blank">http://northern-indymedia.org/articles/1079</a>): <em>&ldquo;If either you, or anyone you live with, went on the March for the Alternative and you &ldquo;associated with members of Black Block&rdquo; or went to the Mayfair convergence space, it&rsquo;s time for a clear-out. Anything you wore on the day needs to go, check through your phone if there are any dodgy texts deleting them is pointless- get a new number. Get a new hard drive for your computer or better still get rid of the whole thing, even just temporarily, cause if the police take it you can never trust it again</em>.&rdquo;&nbsp; &nbsp;  </p>  
   <p>  
  	* On Kate and Will&rsquo;s happy day itself, a further 55 people were arrested - resulting in a whole 5 people being charged. Arrests included all round nutter Chris Knight, for conspiracy to cause public nuisance, one man for singing &ldquo;We All Live in a Fascist Regime&rdquo; and&nbsp; several zombies.  </p>  
   <p>  
  	The zombies were part of a Queer Resistance demo highlighting the impact of the cuts on the LGBTQI (lesbian, gay, bisexual, trans, queer and intersex) community. It was an entirely peaceful &lsquo;Royal Zombie Wedding Celebration&rsquo; complete with cake and marriage blessings, yet even this was too much for the Met to handle, with large numbers of cops snatching as many of the undead as they could get their hands on.  </p>  
   <p>  
  	It seems that, thanks to some scaremongering news stories, the Met were left with more money than sense to protect the happy couple from the marauding anarchist menace. Having blown stacks of taxpayers money on trips to the seaside, mass preventative arrests and repression of peaceful protest hopefully they&rsquo;ve got enough left to cover the inevitable legal bill.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1204), 139); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1204);

				if (SHOWMESSAGES)	{

						addMessage(1204);
						echo getMessages(1204);
						echo showAddMessage(1204);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


