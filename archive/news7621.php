<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 762 - 11th March 2011 - Profit Hungry</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, austerity, banks, anti-capitalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 762 Articles: </b>
<b>
<p><a href="../archive/news7621.php">Profit Hungry</a></p>
</b>

<p><a href="../archive/news7622.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7623.php">A Liberal Dose</a></p>

<p><a href="../archive/news7624.php">The Mound That Roared</a></p>

<p><a href="../archive/news7625.php">Awful Rebellion</a></p>

<p><a href="../archive/news7626.php">Don't Mansion The War</a></p>

<p><a href="../archive/news7627.php">Ship Off, The Old Block</a></p>

<p><a href="../archive/news7628.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/762-wall-st-lg.jpg" target="_blank">
													<img src="../images/762-wall-st-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th March 2011 | Issue 762</b></p>

<p><a href="news762.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PROFIT HUNGRY</h3>

<p>
<p>  
  	<strong>AS INVESTORS&rsquo; BOTTOM LINE HITS THOSE ON THE BREADLINE</strong>  </p>  
   <p>  
  	If you&rsquo;re the type to buy food, rather than only get it from bins, you&rsquo;ve probably noticed that food prices have been rising again. In fact, the international trading prices of major grains are 70% higher than they were this time last year, and in most cases near or above the levels during the price hikes of 2008.  </p>  
   <p>  
  	Demonstrations have already taken place this year in Tunisia, Jordan, Algeria and India. Should the problems continue many more can be expected. But despite what the mainstream news tells us, food price hikes are not just because it rained too much or too little, or because we&rsquo;re eating too much, or that China has suddenly discovered how much it likes sugar and meat. These reasons are only part of the story. It&rsquo;s also due to speculation on food as a commodity in the financial markets.  </p>  
   <p>  
  	Back in 2008, the price of food hit crisis levels. As the world toppled into the financial crash, people from Haiti to Cameroon, despairing that they could not feed themselves or their children, rioted in the streets and protested futilely to their governments.  </p>  
   <p>  
  	Prices then fell again, equally suddenly. But it wasn&rsquo;t just the poor and hungry that let out a sigh of relief. So too did the rich and greedy who&rsquo;d been making millions speculating on the price of food worldwide. Their unregulated system of speculative banking and derivatives trading - that had greatly contributed to the price increases - was left intact.  </p>  
   <p>  
  	The food crisis of 2008, and what may be another in 2011, are the result of a uniquely 21st century cruelty. UN officials referred to the effects as, &ldquo;a silent mass murder&rdquo;, and the desperation and violence appearing as, &ldquo;the new face of hunger&rdquo;. So how is it that hedge funds and investment banks can have such an influence on the food that ends up on plates in Kenya, India or Bangladesh? The answer involves examining a series of changes that have taken place in the complex world of high finance.  </p>  
   <p>  
  	Just as now, the sky-rocketing of food prices three years ago were blamed at the time largely on a combination of &ldquo;real&rdquo; economic factors - that is, those concerned with supply and demand. These included an increasing global appetite, bad harvests, and declines in agricultural productivity due to under-investment and agro-fuel production (although the imposition of structural adjustment programmes in the global South, forcing them to prioritise export rather than national food security, were also flagged up).  </p>  
   <p>  
  	The problem with these explanations was that the sums didn&rsquo;t add up. At all. Analysts, NGOs and economists began studying the mechanisms of the market searching for answers, and it became clear that it was &lsquo;the market&rsquo; itself that played the lead in an entirely man-made crisis. <br />  
  	&nbsp;Soon even the Rolex-wearing free-marketeers had to admit that volatile food prices were partly resting on their shoulders. The IMF, for example, admitted that speculation causes food price hikes when it wrote vaguely that &ldquo;pure financial factors, including the mood of the markets, can have short term effects on the price of oil and other commodities&rdquo;. The &lsquo;mood of the markets&rsquo; in this case had put 120 million additional people into poverty in 2008 and around 100 million into chronic hunger. It&rsquo;s estimated that during 2011 the number of chronically hungry people in the world could top one billion.  </p>  
   <p>  
  	The reason the finance industry is able to cause price hikes and hunger lies in the fact that they&rsquo;ve stuck their ore into the food industry in the first place. Food has long been traded in &lsquo;futures&rsquo; contracts, which do what they say on the tin. A buyer has a contractual arrangement with a producer to buy their wares for a fixed price at a fixed future date. This is designed to give those involved in the agriculture industry some stability.  </p>  
   <p>  
  	<strong>VEGETABLE STOCK MARKET</strong>  </p>  
   <p>  
  	Until the late nineties, regulation was in place that only allowed food to be traded in this way by those directly involved in the industry. These regulations were weakened after aggressive lobbying by the finance industry - and this caused the commodity market to detach from the &ldquo;real&rdquo; economy. Now hedge funds, pension funds and derivatives traders could bet huge sums of money on food prices in the futures market with the possibility of achieving a huge profit.  </p>  
   <p>  
  	Food, a necessity of human life, was now fair game - and play the game they did, especially in the financal hothouses of the London, Chicago and New York exchanges.  </p>  
   <p>  
  	To ensure the profit potential in these futures contracts, something needed to be done to separate them from real supply and demand factors (which could, naturally, lead certain commodities to drop in value, at the cost of investors). The answer of traders at the likes of Goldman Sachs is to &ldquo;bundle&rdquo; commodities into a &ldquo;special index fund&rdquo;. In the case of Goldman Sachs, each fund has an average composition of 30% agricultural and 70% non-agricultural commodities, mostly oil. This diversifies risk to, supposedly, reduce them for investors.  </p>  
   <p>  
  	In 2004, further exemptions from regulation were granted after a petition by some of the biggest hitters in US banking, Lehman Brothers, Morgan Stanley, Bear Stearns and JP Morgan, who had argued (laughably) that they were so well capitalised and sophisticated in risk management they couldn&rsquo;t fail. Following the exemptions, the banks released billions of dollars more for speculation.  </p>  
   <p>  
  	The spinning plates were balanced precariously, and it was the 2007 sub-prime mortgage crash in the US which saw them come tumbling down. The influx of new speculators in the commodities market after the property crash drove up prices at an alarming rate. It was this price &lsquo;bubble&rsquo; - the rate of food prices reaching an artificial high - that caused the scenes of hunger and panic. In 2008, Goldman Sachs earned $1.5 billion of their bottom line from commodity speculation, about a third of their estimate net income.  </p>  
   <p>  
  	Now history is repeating itself. Today the blame&rsquo;s being put on a bad harvests of wheat in Russia and other black sea countries - but again, it doesn&rsquo;t quite make sense. North America has a massive surplus, plenty to make up for the shortfall. Predicted dips in production, whether it materialises or not, is enough for speculators to spot an investment opportunity. When it looks like the price of wheat, sugar or whatever, might go up, people rush to invest in those futures contracts in the hope that its value will keep increasing and they&rsquo;ll be in the money.  </p>  
   <p>  
  	The impact of the subsequent price rises on those already living on the poverty line doesn&rsquo;t come into it. Whilst the bankers wouldn&rsquo;t let their own kids starve to make a quick few million bucks for them and their investors (actually, maybe they would...?), in their warped and self-obsessed minds, people in the developing world don&rsquo;t really exist so its fine.  </p>  
   <p>  
  	Speaking of warped and calculating minds, George Osborne is the only EU Chancellor digging his heels in about banking regulation - a subject on the EU agenda for this spring. Afraid that reform would cause a mass exodus of his banking buddies from the City of London, and under intense lobbying pressure from the City itself, it looks like Gideon (yes, that&rsquo;s real name from Eton days, not George &ndash; he changed it to sound more man-o-the people!) Osbourne&rsquo;s going to do his best to make sure the needs of the rich to get richer get met before the needs of millions of people to carry on living.  </p>  
   <p>  
  	The topic also surfaced in talks between the G20 finance ministers in February in Paris, which resulted only in weak promises to &ldquo;study&rdquo; commodity price volatility and have a bit more of a chinwag about it at the next meeting. Food speculation is a subject the political and economic elites are reluctant get their teeth into - aware that it&rsquo;s the murderous, bastard offspring of the capitalist system they are working so hard to protect. Yet another reason to hate bankers - the bread they earn from food speculation can literally take the bread out of the mouths of millions. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1134), 131); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1134);

				if (SHOWMESSAGES)	{

						addMessage(1134);
						echo getMessages(1134);
						echo showAddMessage(1134);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


