<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 719 - 23rd April 2010 - Giving The Green Finger</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, squat, community garden, occupation, bristol" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 719 Articles: </b>
<b>
<p><a href="../archive/news7191.php">Giving The Green Finger</a></p>
</b>

<p><a href="../archive/news7192.php">Crackdown After Death In Detention Centre</a></p>

<p><a href="../archive/news7193.php">Taken To The Cleaners</a></p>

<p><a href="../archive/news7194.php">Qulla Surprise</a></p>

<p><a href="../archive/news7195.php">Sieg Heil-lywood</a></p>

<p><a href="../archive/news7196.php">The Full English</a></p>

<p><a href="../archive/news7197.php">Put Yer Bacton Into It</a></p>

<p><a href="../archive/news7198.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd April 2010 | Issue 719</b></p>

<p><a href="news719.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GIVING THE GREEN FINGER</h3>

<p>
<p class="MsoNormal"><strong>AS GUERILLA GARDENERS SET UP AN ECO-VILLAGE IN <st1:city w:st="on"><st1:place w:st="on">BRISTOL</st1:place></st1:city>...</strong> <br />  <br /> <strong>Bristol eco-village set up camp this week</strong> with huge support from local people and activists from all around the country. The off-grid, urban community kicked off last Saturday (17th) on a patch of waste land in St Werburghs - previously notorious for fly tipping, arson attacks and hard drug users. <br />  <br /> The &lsquo;swoop&rsquo; to take the land started with activists forming a walking block and cycle block, trailed by an obligatory police presence. After a wild goose chase to lose the cops (who eventually got bored and wandered off), both teams took the site at about midday. <br /> The first day was spent clearing the site of rubbish, yielding a plethora of waste building materials which have been put to good use with tyre walls for an earthship being constructed, two composting loos on the go and recycling facilities sorted. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/719-eco-village-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/719-eco-village-sm.jpg"  /></a></td></tr></table>St. Werburghs is known for being a &lsquo;green&rsquo; part of Bristol with a city farm and a number of self-build eco-houses nearby, so the neighbours to the site have responded with gusto. Residents have donated tools, a grey water system and polytunnel, as well as plants and seedlings, which were planted with help from the kids from the primary school opposite. <br />  <br /> Police have reacted positively to the squat and offered their support to help prevent a planned illegal eviction on Wednesday (21st). Bailiffs had turned up the day before and served occupants with an eviction notice (after which a fire was mysteriously started in the corner of the site), and a call out went out for people to help protect the village. After a good turnout of supporters, the bailiffs didn&rsquo;t show, having been served notice that the eviction would be illegal under common and criminal law. Another attempt to evict is expected soon, so campers are urging people to continue to respond to call-outs. <br />  <br /> One camper told SchNEWS that the collective plans to set up the site with buildings and a flourishing garden, then leave it for the local community to enjoy before moving on to another piece of derelict land which needs regeneration. The camp invites all visitors to come for the day or to stay for longer - so grab your tools and gardening gloves and get down to help green-up an urban wasteland. <br />  <br /> <strong>FIRST IN THE KEW</strong> <br />  <br /> The Bristol project was inspired by green-fingered activists in London who set up an eco-village at Kew Bridge in June 2009 (See <a href='../archive/news677.htm'>SchNEWS 677</a>, <a href='../archive/news679.htm'>679</a>, <a href='../archive/news682.htm'>682</a>). This provided an example of post-peak oil, sustainable, urban living created from capitalist society&rsquo;s waste - creating a thriving community space which is still going strong nearly a year later. <br />  <br /> The two hectare site had been unoccupied for 23 years after failed development plans for an eight storey block of flats were continuously rejected at the planning stage. The land has been left alone by the landowners, who have not tried to evict the occupants, and by managing to keep the local residents and police sweet, the camp is now a hub of community interaction. There is a packed weekly itinerary of workshops including permaculture, cookery and art, wild food walks around the local area, public meetings and guided tour open days. The site is also a launch pad for protest with people getting involved with demos like the anti-Counter Terror Expo action (See <a href='../archive/news718.htm'>SchNEWS 718</a>), and the village is closed on Mondays as the occupants go and demo outside the MOD and &lsquo;emanate love&rsquo; to promote peace. <br />  <br /> With 30 permanent residents, nine of whom are original crew from the swoop at the site, they also welcome visitors with a custom built guest house if you want to stay (though call first to check if there&rsquo;s space), and encourage all to come and help with the community vegetable and herb garden or just chill out. <br />  <br /> <strong>DIGGING DEEP</strong> <br />  <br /> The eco-village phenomena is not a new concept, with a six-month long community set up in 1996 (See <a href='../archive/news73.htm'>SchNEWS 73</a>, <a href='../archive/news95.htm'>95</a>) by a collective called Pure Genius, who occupied a site in Wandsworth before being evicted to make way for a supermarket and flats (who says nothing changes...). The land there had previously been empty for six years and was owned by drinks giant Guinness. This camp was however plagued by problems caused by the more fringe members of society flocking to the &lsquo;autonomous safe space&rsquo;, resulting in personality clashes and conflicts of interest caused by - to quote SchNEWS from back in the day - &ldquo;people not willing, or able, to take responsibility for their own actions&rdquo;. Luckily this does not seem to be an issue facing the recent eco-villages. <br />   <br /> The action of squatting unused land based on principals of harmony with the land and equality goes back further to before anyone had even come up with the genius idea of abbreviating ecology into a soundbite, and when villages had composting loos as standard. The Diggers movement from 1649, a network of communes which reclaimed land after the English Civil War, has been heralded as the prime inspiration for all of these modern day actions. Following the execution of Charles I, the Diggers started in protest against the treatment of the common people who had fought and died for a supposedly more egalitarian parliamentary system, while the MPs lived in luxurious comfort (sound familiar..?). The network however was quickly squashed by the military who went round smashing up all the settlements (ditto). <br />  <br /> To get involved in this ground-up movement, go and visit for a workshop or a chat, or even better, start one up yourself in a neglected spot near you. <br />  <br /> <strong>* The Bristol Eco-Village</strong> is on land between the M32 and Saxon Rd, Bristol, BS2 9SH - for map see <a href="http://wtp2.appspot.com/wheresthepath.htm?lat=51.469250...oz=10%3E." target="_blank">http://wtp2.appspot.com/wheresthepath.htm?lat=51.469250...oz=10%3E.</a> For more info and contact details see the Facebook group: Bristol Eco Village - the Urban Centre for Alternative Technology. <br />   <br /> <strong>* The Kew Bridge</strong> site is at 2 Kew Bridge Road, Brentford, TW8 0JF, site mob 07967864370 see Kew Bridge Eco Village! on Facebook and <a href="http://kewbridgeecovillage.wordpress.com." target="_blank">http://kewbridgeecovillage.wordpress.com.</a> <br />  <br /> * There are currently two other occupied community gardens, both of which were toxic, disused petrol station sites, and are now thriving gardens and community spaces: The <strong>Lewes Road Community Garden</strong> in Brighton has been an oasis of green on a grinding high street since last May (See <a href='../archive/news680.htm'>SchNEWS 680</a>) <a href="http://www.lewesroadcommunitygarden.org," target="_blank">www.lewesroadcommunitygarden.org,</a> see also Facebook group of the same name. In East Oxford, the <strong>Barracks Lane Community Garden</strong> has been running since 2007, with regular events see <a href="http://www.barrackslanegarden.org.uk" target="_blank">www.barrackslanegarden.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bristol&source=719">bristol</a>, <a href="../keywordSearch/?keyword=community+garden&source=719">community garden</a>, <a href="../keywordSearch/?keyword=occupation&source=719">occupation</a>, <a href="../keywordSearch/?keyword=squat&source=719">squat</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(746);

				if (SHOWMESSAGES)	{

						addMessage(746);
						echo getMessages(746);
						echo showAddMessage(746);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


