<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 661 - 9th January 2009 - Eyewitness : Gaza</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, israel, idf, genocide, rafah, ewa jasiewicz, red crescent, raytheon, carmel agrexco, caterpillar, edo, bae systems" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 661 Articles: </b>
<p><a href="../archive/news6611.php">Eyewitness : Rafah</a></p>

<b>
<p><a href="../archive/news6612.php">Eyewitness : Gaza</a></p>
</b>

<p><a href="../archive/news6613.php">Global Outrage</a></p>

<p><a href="../archive/news6614.php">Gaza Relief Boat Rammed</a></p>

<p><a href="../archive/news6615.php">Ceasefire? What Ceasefire?</a></p>

<p><a href="../archive/news6616.php">Gaza Gameplan</a></p>

<p><a href="../archive/news6617.php">Media War</a></p>

<p><a href="../archive/news6618.php">Elementary My Dear Watson</a></p>

<p><a href="../archive/news6619.php">Inside Schnews</a></p>

<p><a href="../archive/news66110.php">Outside Schnews</a></p>

<p><a href="../archive/news66111.php">Shac Verdict</a></p>

<p><a href="../archive/news66112.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th January 2009 | Issue 661</b></p>

<p><a href="news661.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>EYEWITNESS : GAZA</H3>

<p>
<b>Ewa Jasiewicz is an international peace activist currently in northern Gaza. This is an edited version of a piece written on Thursday January 8th 2008:</b> <br />
 <br />
"<i>This is the the deliberate terrorisation of an entire civilian population..They're going to come in as deep as they can - they're going to kill as many as they can</i>" - <b>Ewa Jasciewicz, 06/01/09.</b> <br />
 <br />
I've been working with the Palestinian Red Crescent ambulance services in Jabaliya, Beit Hanoun and Beit Lahiya for the past 5 days and nights.  <br />
 <br />
Paramedic Ali Khalil's team was shot at on Monday afternoon. He told me, 'We had been told we had the go-ahead from the Israeli army through co-ordination with the Red Cross but when we arrived at the area we were shot at. We had to turn back'. Yesterday afternoon, a medical volunteer, Hassan, was shot in the leg as he and his colleague had to drop the stretcher they were carrying after coming under Israeli sniper fire. There are reports of scores of dead bodies lying in the streets un-claimed. The Palestinian Red Crescent Society estimates there are 230 injured which they haven't been able to pick up. <br />
 <br />
There are reports of 18 corpses in one home alone and the injured dying from treatable wounds because of a lack of access to medical treatment. <br />
 <br />
Sporadic battles are taking place between Palestinian resistance fighters, armed with basic machine guns, the odd grenade, and warm clothes. They're up against the fourth most powerful army in the world, armed with state-of-the-art war planes, Merkava tanks, regional governmental co-ordination and intelligence, a green light to kill with impunity in the name of self defence, body armor, night vision, and holidays in Goa when it all gets too much. <br />
 <br />
The paramedics, drivers and volunteers at the emergency services risk their lives every time they leave their base and even working within their bases. Medics evacuated their original base near Salahadeen street due to heavy shelling from Israeli forces early last week. <br />
 <br />
Yesterday around 1am we were called out to a strike in the Moaskar Jabaliya area. The area was pitch black, our feeble torches lighting up broken pipes streaming water, glass, chunks of concrete and twisted metal. '<i>They're down there, down there, take care</i>', people said. The smell of fresh severed flesh, a smell that can only come from the shedding of pints of blood and open insides, was in the air. I got called back by a medic who screamed at me to stay by his side. It turned out I'd been following the Civil Defence, the front line responders who check to see if buildings are safe and put out fires, rather than the medics.  <br />
 <br />
The deep ink dark makes it almost impossible to see clearly, shadows and faces lit up by swiveling red ambulance lights and arms pointing hurriedly are our guides for finding the injured. 'Lets get out of here, lets get out' say the guys, and we're leaving to go, empty handed, but straining to seeing what's ahead when a missile hits the ground in front of us. We see a lit up fountain of what could be nail darts explode in front of us. They fall in a spray like a thousand hissing critters, we cover our heads and run back to the ambulance. One of the volunteers inside, Mohammad, is shocked, '<i>Did you see? Did you see? How close it was?</i>' <br />
 <br />
At approximately 4am, we hit the streets in response to an F16 war plane attack on the house of Abdullah Sayeed Mrad in the Block Two area of Jabaliya Camp in the Northern Gaza Strip. <br />
 <br />
In Naim Street Beit Hanoun, at 9.30pm on Sunday, Samieh Kaferna , 40, was hit by flying shrapnel to his head. Neighbours called him to come to their home. Fearing his home would be struck, he and a group of relatives began to move from one home to another, to be safer. <br />
 <br />
The second missile struck them down directly. When we arrived one man, eyes gigantic, was being dragged into the pavement, half of his lower body shredded, his intestines slopping out. He was alive, his relatives were screaming, we managed to take four, whilst six others, charred and dismembered, were brought in on the back of an open cattle truck. Beit Hanoun Hospital was chaos, with screaming relatives and burning bodies. Three men died in the attack, 10 were injured, six from the same Abu Harbid family. Three had to have leg amputations, and one a double amputation.  <br />
 <br />
Burning shrapnel in eyes is a common injury, shrapnel slices deep into to any soft fleshy parts of the body. We brought a boy from Beit Hanoun with a distorted heavily bandaged head wrapped in bandages, to Al Nasser hospital with its specialist eye unit and mental health clinic. When we get there, its pitch black, doctors are sitting around candles, the place is freezing and full of shadows. Both the doctors and their patients have been blinded with Israeli-controlled power cuts that intensify the confusion, fear, and psychological darkness caving in on people here.  <br />
 <br />
Khalil Abu Shammalah, Director of Al Dhumeer Association based in Gaza City said: '<i>It is a breach of the fourth Geneva Convention to target emergency medical services under conditions of war and occupation. Battlefield casualties are also protected under the Geneva Conventions and cannot be targeted once injured. Israel is in breach of international law</i>'.  <br />
 <br />
People were coming back to their homes for the first time. The Hamdan Family had three homes in a row destroyed. I asked one woman sitting amongst the ruins of her home where she would go now? She replied, 'Beit Hanoun UNRWA school'. '<i>But do you think that will be safe?</i>' I ask her. '<i>No, but I have nowhere else to go</i>' she replied. <br />
 <br />
<table width='250px' align='right'><tr><td><div style='width:250px; font-size: 13px; font-family:times new roman; font-style: italic ; padding:18px; color: #777777; text-align:center'>THEY'RE after another Sabra and Shatila - the deliberate terrorisation of an entire civilian population..They're going to come in as deep as they can - they're going to kill as many as they can. - Ewa Jasciewicz, 06/01/09</div></td></tr></table>The Al Naim Mosque was also completely destroyed, holy books still smouldering from the attacks. Approximately one in 10 of the some 100 mosques in the Jabaliya area have been destroyed in Israel 's assault. '<i>We see them as personal centers for us, they're not Hamas, and we paid for them out of our own money, they belong to us, not anyone else</i>', explained one Imam based in Jabaliya. <br />
 <br />
On Sunday night, all Sikka Street residents were given five minutes to leave their homes, ordered out through loudhailers, unable to take any belongings with them, rounded up by Israeli occupation forces and taken to the Al Naim Mosque. Women, children and the elderly were put inside and men aged between 16-40 were kept in a field outside in the cold and interrogated. Six were taken to Erez, three were released a day later and were told by soldiers, according to a witness, that it was safe for them to make their own way home along Salahadeen Street. It was there that special forces allegedly shot 33 year-old Shaadi Hissam Yousef Hamad 33, in the head. <br />
 <br />
Whether people stay in their homes or leave, they are being bombed. Majid Hamdan Wadeeya, 40, was hit in the leg and spine with shrapnel while he and his family were preparing to leave their home in Jaffa Street, Jabaliya. We arrived at his home on Tuesday afternoon to find the family's decrepit red car still running and the family minivan stuffed with mattresses, towels, blankets, and belongings, blasted open. They had been hit by a missile from either a drone of apache. 'We were going from the bombing, from the bombing', screamed his children, all terrified. We managed to take half of the family, the rest got in their red car and followed. <br />
 <br />
Everyone here knows someone who has been killed in Israel 's massacres. I can't keep up with the stories of missile struck cousins, nephews, brothers, the jailed, the humiliated, the shot, the unreachable, the homeless, the now even more vulnerable than ever, people, not pieces, piling up in morgues all over Gaza, not pieces, people. These people are struggling to live and breathe another day, to avoid the lethal use of F16s, F15s, Apache Helicopters, Cobra Gun Ships, Israeli naval gun ships, that are targeting them. <br />
 <br />
Whilst people say they are resisting the worst attack on them since the Nakba, Israel proceeds to break up the West Bank, under a project of roads and tunnels 'for Palestinains' which reinforce the existing illegal settlement system, apartheid wall, land and water theft and Palestinian bantustanisation.  <br />
 <br />
How do you break a people that won't be broken? 'They will have to kill each and everyone of us' people tell me. From the first days here people were expecting 'the shoah' threatened upon them by Matan Villai , Israel's deputy defence minister this February. It is happening now -this is the Shoah.  <br />
 <br />
The third Intifada being urged now has to be our intifada too. As Israel steps up its destruction of the Palestinian people, we need to step up our reconstruction of our resistance, our movements, of our communities in our own counties, where so many of us live in alienation and isolation. We need to be the third intifada - people here need more and say repeatedly that they need more than the demonstrations, because they are not stopping the killing here. Demonstrations alone, are not stopping the killing here.  <br />
 <br />
The arms companies making the weapons that are targeting people here, the companies that are selling stolen goods from occupied land pillaging settlements, the companies building the apartheid wall, the prisons, the East Jerusalem Light Railway system. These companies, Carmel Agrexco, Caterpillar, Veolia, Raytheon, EDO, BAE Systems, they are complicit in the crimes against humanity being committed here. If the international community will not uphold international law, then a popular movement should and can - we can use the legal system of international law as one of many means to hold on to our collective humanity. <br />
 <br />
The European Union decision, undertaken by the Council of Ministers this December, to upgrade relations with Israel , from economic ties to cultural, security, and political relations must be reversed. The EU represents a core strategic market of legitimacy and political economic reinforcement of Israel and as such its  capacity to commit crimes against humanity, with impunity.  <br />
 <br />
We can cut this tie, we can halt this decision which if approved this April, will empower Israel further, bring it closer to the 'community of nations' of the EU, and give a green light for further terror and crimes against humanity be inflicted upon the Palestinian people. This is a decision which has not yet been ratified. We can influence that which hasn't happened yet.  <br />
 <br />
There are concrete steps that people can take, learning from the lessons of the first Intifada and the Boycott, Divestment and Sanctions campaign to dismantle the South African Apartheid regime. Strategies of popular resistance, strikes, occupations, direct actions. From the streets into the offices, factories and headquarters is where we need to take this fight, to the heart of decision-makers that are supposedly making decisions on our behalf and the companies making a killing out of the occupation. The third intifada needs to be a global intifada.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=661">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=661">palestine</a>, <a href="../keywordSearch/?keyword=israel&source=661">israel</a>, <a href="../keywordSearch/?keyword=idf&source=661">idf</a>, <a href="../keywordSearch/?keyword=genocide&source=661">genocide</a>, <a href="../keywordSearch/?keyword=raytheon&source=661">raytheon</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(181);
			
				if (SHOWMESSAGES)	{
				
						addMessage(181);
						echo getMessages(181);
						echo showAddMessage(181);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>