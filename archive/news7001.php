<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 700 - 20th November 2009 - Good Cop Bad Cop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, direct action, international mobilisations, summit hopping" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 700 Articles: </b>
<b>
<p><a href="../archive/news7001.php">Good Cop Bad Cop</a></p>
</b>

<p><a href="../archive/news7002.php">Tamil Torture</a></p>

<p><a href="../archive/news7003.php">Not A Leg To Stand On</a></p>

<p><a href="../archive/news7004.php">Walls Of Steel</a></p>

<p><a href="../archive/news7005.php">Nae To Nato</a></p>

<p><a href="../archive/news7006.php">Blackboard Rumble</a></p>

<p><a href="../archive/news7007.php">Crasbo Selecta</a></p>

<p><a href="../archive/news7008.php">Schnews In Brief</a></p>

<p><a href="../archive/news7009.php">Truck Or Treat</a></p>

<p><a href="../archive/news70010.php">Taking The Cissbury</a></p>

<p><a href="../archive/news70011.php">Out Of Their League</a></p>

<p><a href="../archive/news70012.php">French Letter</a></p>

<p><a href="../archive/news70013.php">Atomic Acquitten</a></p>

<p><a href="../archive/news70014.php">R . I . P Ivan Khutorskoy</a></p>

<p><a href="../archive/news70015.php">...and Finally... Milestone Or Millstone?</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/700obama-lg.jpg" target="_blank">
													<img src="../images/700obama-sm.jpg" alt="As the US and China agree not to agree anything meaningful at Copenhagen" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 20th November 2009 | Issue 700</b></p>

<p><a href="news700.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GOOD COP BAD COP</h3>

<p>
<strong>INFORMATION FOR ACTION FOR THE COPENHAGEN COP 15 CLIMATE CONFERENCE</strong> <br />  <br /> The climate summit at Copenhagen is drawing closer and closer, and various factions are mobilising across the world, some to push global elites into waking up and smelling the carbon and others to shout that capitalism cannot be reformed - it must be destroyed. Based on our leaders&rsquo; last sorry effort don&rsquo;t expect too much from COP 15. Kyoto amounted to not much more than a system of carbon bribing offsetting (and even that the US pulled out of). Politicians are already baulking at the idea of actually limiting growth and are announcing that COP 15 will not produce a binding agreement. <br />   <br /> The talk is of carbon trading i.e more capital speculation to undo the damage caused by  capital speculation. So once again the responsibility falls to us to push the pressing and essential need for a dramatic rethink in environmental policy on a global scale. With this in mind thousands of activists and concerned environmentalists will be attending the talks held on 7th - 18th December. Here&rsquo;s just a quick round-up of the actions and groups who will be converging on Denmark. <br />  <br /> <strong>KLIMAFORUM</strong> <br />  <br /> Running from 7th - 18th December alongside the COP15 is Klimaforum, the climate counter-summit run for and by grass roots activist movements, the scientific community and individuals to &lsquo;create an open space where people, movements and organisations can develop constructive solutions to the climate crisis. It&rsquo;s taking place in DGI-byen, (a conference complex in Copenhagen&rsquo;s centre). Free and open to all. <a href="http://www.klimaforum09.org" target="_blank">www.klimaforum09.org</a> <br />  <br /> <strong>OPERATION BIKE BLOC</strong> <br />  <br /> Climate Camp and Bristol-based artists the Laboratory of Insurrectionary Imagination is constructing the ultimate &lsquo;resistance machine&rsquo;- a  pedal-powered, art-bike carnival. Sounds good huh? On 16th December the Bike Bloc will take to the streets as part of the Reclaim Power! event. To get involved go along to the Arnolfini Gallery, Bristol 24th and 29th November, or at Copenhagen at the Candyfactory to help put together the final design. <a href="http://www.climatecamp.org.uk/actions/copenhagen-2009/bike-bloc" target="_blank">www.climatecamp.org.uk/actions/copenhagen-2009/bike-bloc</a> <br />  <br /> <strong>CLIMATE CARAVAN</strong> <br />  <br /> As well as COP 15, the WTO Summit in Geneva from 30th November - 2nd December is another target for action. 60 activists from the global South, plus local affinity groups, will be travelling across Europe from Geneva to Copenhagen. The 12 day caravan kicks off with 5 days of action based in Geneva, then leaves on 3rd December to protest their way to COP 15, arriving on the 9th. <a href="http://www.climatecaravan.org" target="_blank">www.climatecaravan.org</a> <br />  <br /> <strong><font size="4">Now your appetite for action is truly whetted, what can you expect to be going on during the event?</font> <br />  <br /> </strong><strong>* 11th December: Our Climate! Not Your Business!</strong> Day of direct action targeting participating  corporations. See &lsquo;Don&rsquo;t Buy The Lie&rsquo; on Facebook. <br />  <br /> <strong>* 12th December: Flood for Climate Justice</strong> - People power on the streets organised by Friend of the Earth. <a href="http://www.foei.org" target="_blank">www.foei.org</a>  <br />  <br /> <strong>Never Trust a COP</strong> - March to conference centre through Copenhagen see <a href="http://nevertrustacop.org/Main/GetHeardNotHerded" target="_blank">http://nevertrustacop.org/Main/GetHeardNotHerded</a>  <br />  <br /> <strong>Global Day of Action -</strong> If you can&rsquo;t make it all the way to Denmark, plan your own day of action wherever you are. <a href="http://www.globalclimatecampaign.org" target="_blank">www.globalclimatecampaign.org</a> <br />  <br /> <strong>* 13th December: Hit the Production! </strong>Mass blockade targeting the harbour of Copenhagen and  industry. <a href="http://htp.noblogs.org" target="_blank">http://htp.noblogs.org</a>  <br />  <br /> <strong>Farmer&rsquo;s Action</strong> - Direct actions against the agro-industry organised by La Via Campesina <a href="http://www.viacampesina.org" target="_blank">www.viacampesina.org</a>  <br />  <br /> <strong>14th December: No Borders, No Climate Refugees!</strong> Day of action in support of freedom of movement  and international no borders groups. See <a href="http://info.interactivist.net/node/13135" target="_blank">http://info.interactivist.net/node/13135</a> <br />  <br /> <strong>15th December: Resistance is Ripe! Agriculture Action Day</strong> - Collective action in support of changing the global food production system. See <a href="http://www.climate-justice-action.org/mobilization/agriculture-action-day" target="_blank">www.climate-justice-action.org/mobilization/agriculture-action-day</a> <br />  <br /> <b>16th December: Reclaim Power! Pushing for Climate Justice </b>- This is the biggie. Co-ordinated mass action on the streets of Copenhagen, drawing together for a people&rsquo;s summit for climate justice in the middle of the lion&rsquo;s den. <a href="http://www.climatecamp.org.uk/actions/copenhagen-2009" target="_blank">www.climatecamp.org.uk/actions/copenhagen-2009</a> <br />  <br /> <b>These are just the published events. More actions will be taking place throughout the conference by affinity groups and individuals, so where do you go to find out about them once you&rsquo;re there?</b> <br />  <br /> <b>CONVERGENCE CENTRES / INFO POINTS</b> <br />  <br /> Four spaces will be the hub of action planning and accommodation: The &ldquo;main&rdquo; convergence, St&oslash;beriet on Bl&aring;g&aring;rds Plads on N&oslash;rrebro, which will feature medics, trauma support, legal aid, meeting and social spaces and info point; Bolsjefabrikken, The Candy Factory social center (Copenhagen near N&oslash;rrebro) will host a kitchen and workshop areas (both kinds - tools and discussions/hand waggling).  <br />  <br /> A smaller convergence, R&aring;huset by KlimaForum09 on Vesterbro will feature a social space and info point along with the main communal kitchen;  <br />  <br /> The House of Solidarity on N&oslash;rrebro &ndash; near the main centre - will feature a be-the-media activist centre open for everyone.  <br />  <br /> There will also be an info point at the Central Train Station, to redirect you onto these centres. <br />  <br /> For loads more info and advice <a href="http://www.climate-justice-action.org/practical-info/copenhagen-info" target="_blank">www.climate-justice-action.org/practical-info/copenhagen-info</a> <br />  <br /> <b>So now you know who will be there, when they will be doing, and how you can get involved with it. All that remains is getting there...</b> <br />  <br /> <b>TRANSPORT</b> <br />  <br /> <b>* Ryanair flights</b> are a bargain at &pound;29.99 plus airport tax - YEAH RIGHT. Don&rsquo;t fly there, like the team of writers for The Guardian will be. <br />  <br /> <b>* Two coaches are leaving from London and Leeds</b>, organised by Camp for Climate Action. For times and booking info see <a href="http://www.climatecamp.org.uk/actions/copenhagen-2009/coaches" target="_blank">www.climatecamp.org.uk/actions/copenhagen-2009/coaches</a> <br />  <br /> * Or get in touch with your local branch of <b>Climate Campers</b>, most groups are sorting their own transport. See <a href="http://www.climatecamp.org.uk/get-involved/local-groups" target="_blank">www.climatecamp.org.uk/get-involved/local-groups</a> <br />  <br /> <b>* For ferries and sleeper trains</b> see <a href="http://www.seat61.com/Denmark.htm" target="_blank">www.seat61.com/Denmark.htm</a> or coach <a href="http://www.eurolines.com" target="_blank">www.eurolines.com</a> <br />  <br /> <b>* Once you&rsquo;re in Denmark</b>, use Danish Railways to get about the city. For KlimaForum09 you need to get off at Hovedbaneg&aring;rden, the main station in Copenhagen; and for the convergence centres in N&oslash;rrebro, get off at  N&oslash;rreport St. For times and booking see <a href="http://www.dsb.dk" target="_blank">www.dsb.dk</a> (Right corner to switch to English) <br />  <br /> <font size="4"><b>Stand up, fight for your voice, and make it heard. See you there!</b></font> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=700">climate change</a>, <a href="../keywordSearch/?keyword=direct+action&source=700">direct action</a>, <span style='color:#777777; font-size:10px'>international mobilisations</span>, <a href="../keywordSearch/?keyword=summit+hopping&source=700">summit hopping</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(569);
			
				if (SHOWMESSAGES)	{
				
						addMessage(569);
						echo getMessages(569);
						echo showAddMessage(569);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>