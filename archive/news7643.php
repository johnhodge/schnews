<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 764 - 25th March 2011 - TUC Right Off</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, protest, uk uncut, london" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://london.indymedia.org/events/7176" target="_blank"><img
						src="../images_main/763-banner-march-26th.png"
						alt="Mass Demonstration Against The Cuts - London March 26th 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 764 Articles: </b>
<p><a href="../archive/news7641.php">No Flies On Us</a></p>

<p><a href="../archive/news7642.php">Edl: Reading Lessons</a></p>

<b>
<p><a href="../archive/news7643.php">Tuc Right Off</a></p>
</b>

<p><a href="../archive/news7644.php">Lucky Hetherington</a></p>

<p><a href="../archive/news7645.php">Squat Thrusts</a></p>

<p><a href="../archive/news7646.php">0742 Club Evicted</a></p>

<p><a href="../archive/news7647.php">Glengad Abouts</a></p>

<p><a href="../archive/news7648.php">Wood Cuts</a></p>

<p><a href="../archive/news7649.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 25th March 2011 | Issue 764</b></p>

<p><a href="news764.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TUC RIGHT OFF</h3>

<p>
<p>  
  	Wednesday&rsquo;s budget was another banker-friendly cop-out, with a cut in corporation tax (like they don&rsquo;t make enough from tax avoidance...), designed to encourage the multi-nationals to set up in the UK, while ordinary people feel the brunt of the spending cuts. The freezes in fuel and aviation tax may be a PR-friendly concession for the Ford Focus, Florida holidaying classes, but the massive anti-cuts sentiment has been once again ignored by the Con-dems.  </p>  
   <p>  
  	Don&rsquo;t despair though, on Saturday 26th March the TUC will be leading the masses into the revolution. The so-called &ldquo;March for The Alternative&rdquo; will gather on Victoria Embankment from 11am aiming to arrive in Hyde Park shortly around 1:30pm for a speech by Milliband the Younger. You can then get a coach home and vote Labour at the next available opportunity, after all it&rsquo;s about time they had a turn.  </p>  
   <p>  
  	If that&rsquo;s not quite enough for you there are a number of other activities that are planned, however &ldquo;There are no official feeder marches!&rdquo; so don&rsquo;t get carried away by ideas of autonomy or empowerment. Despite this here&rsquo;s a run-down of just some of the fun things to do in London:  </p>  
   <p>  
  	The<strong> Radical Workers&rsquo; bloc</strong> will assemble 11am Kensington Park, 11.30am march over Westminster Bridge, 12pm march to Hyde Park. Nearest tube Oval. See <a href="http://www.afed.org.uk" target="_blank">www.afed.org.uk</a>  </p>  
   <p>  
  	The <strong>Education bloc</strong> (including militant workers&rsquo; and anarchist blocs) will assemble 10am Universtiy of London Union, Malet St. Apparently anarcho types should follow the banners and sound-systems which will end up at Oxford St. at 2pm. Nearest tube Goodge St. (more info see <a href="http://www.studentassembly.org.uk" target="_blank">www.studentassembly.org.uk</a>)  </p>  
   <p>  
  	&ldquo;<strong>Occupy For the Alternative</strong>&rdquo; called by UK Uncut, will form up at 11.30am at the National Theatre on the South Bank, heading to Oxford St. for 2pm for a variety of occupations and actions, then gathering at Oxford Circus for a mass occupation of a top secret target. Nearest tube Oxford Circus. (more info see <a href="http://www.ukuncut.org.uk" target="_blank">www.ukuncut.org.uk</a>)  </p>  
   <p>  
  	<strong>Pink and Black Bloc</strong> called by <strong>Queer Resistance</strong>. Soho Square, London W1D, 10am moving off 10.30am to Cambridge Circus then on to the main march. Nearest tube Tottenham Court Rd.  </p>  
   <p>  
  	<strong>Dissident feeder </strong>march Cable Street, E1, 11am. Nearest tube Shadwell or Aldgate East.  </p>  
   <p>  
  	<strong>Women&rsquo;s bloc</strong>. Meet 10am Royal Courts of Justice, The Strand. Wear purple and green bring noise making equipment. Nearest tube Temple. For more details Twitter @WACLon or call 07582288913 on the day.  </p>  
   <p>  
  	<strong>North London</strong> feeder march Lincoln Inn Fields at 11am. Nearest tube Holborn. <br />  
  	&nbsp;There&rsquo;ll be an IWW rally by Park Lane fountain, Hyde Park 3.30pm.  </p>  
   <p>  
  	Missed out on getting busted at Earl St? Get down to the <strong>Big Society HQ</strong> - a squatted convergence space at 61 Curzon Street and you too could win a stale pasty and &pound;3,500 courtesy of the Met. Nearest tube Green Park. (more info see <a href="http://reallyfreeschool.org" target="_blank">http://reallyfreeschool.org</a>)  </p>  
   <p>  
  	<strong>Resist 26</strong>! (Chris Knight - see SchNEWS nutter-alert 942) are calling for &ldquo;peoples occupation&rdquo; of Hyde Park. They want people to bring sound-systems, car batteries, lighting, food etc. (see <a href="http://www.resist26.org" target="_blank">http://www.resist26.org</a>)  </p>  
   <p>  
  	All in all expect heavy policing, intrusive surveillance, stewards collaborating with police, confusion, chaos, kettling and just possibly something to make the day worth while. See you on the streets.  </p>  
   <p>  
  	* See Indymedia.org.uk for a full listing of the day&rsquo;s action.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1155), 133); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1155);

				if (SHOWMESSAGES)	{

						addMessage(1155);
						echo getMessages(1155);
						echo showAddMessage(1155);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


