<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 757 - 4th February 2011 - Behind the Irony Curtain</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, belarus, dictatorship" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 757 Articles: </b>
<p><a href="../archive/news7571.php">Cut To The Quick...</a></p>

<p><a href="../archive/news7572.php">High Street Tax Maniacs</a></p>

<p><a href="../archive/news7573.php">Mubarak's Attacks Causing Chaos In Cairo</a></p>

<p><a href="../archive/news7574.php">Don't Mention The Wall</a></p>

<p><a href="../archive/news7575.php">Positive Schnews</a></p>

<b>
<p><a href="../archive/news7576.php">Behind The Irony Curtain</a></p>
</b>

<p><a href="../archive/news7577.php">On The Hoof</a></p>

<p><a href="../archive/news7578.php">Grow To Like It</a></p>

<p><a href="../archive/news7579.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th February 2011 | Issue 757</b></p>

<p><a href="news757.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BEHIND THE IRONY CURTAIN</h3>

<p>
<p>
	When the fine nation of Belarus was given the opportunity to express itself in the much hailed democratic function of &lsquo;free elections&rsquo;, there were unable to pull it off with the finesse of say, western governments. Alexander Lukashenko, often referred to as Europe&rsquo;s last dictator, was inaugurated for a fourth term as president in January 2011 after elections held on the 19th December 2010. </p>
<p>
	Lukashenko has been president of Belarus since 1994. Using a whole load of cunning, with only the occasional resort to violence, and only slightly sponsored by Russian interests, he has managed to remain in power unchallenged. </p>
<p>
	Since about 2005, however, a few minor tiffs with the Russians and a bit of pesky EU interference, not to mention that darn internet, more voices of dissent are being heard and noticed. So a few protests here, a few arrests there and then, true to form, on the 19th Dec pole, with chutzpah George. W. could only dream of, Lukashenko polled an alleged 87.7%. </p>
<p>
	This announcement caused a lot of angry people to gather in central Minsk to express their disgust. This triggered the expected police reaction &ndash; as they so often do when faced with peaceful protesters worldwide - choosing to kick the shit out of everyone in sight. All usual dirty tricks ensued with 700 arrests and lots of good old-fashioned middle-of-the-night, break into house, bag over head and off to Siberia for 5-15 years we go malarkey. Relatives of those currently incarcerated are unable to contact their family and have no knowledge of where they are held, let alone what the evidence for their charges is. With the government threatening to even take the young children of those arrested into custody, solidarity for the Belarusian prisoners is much needed. </p>
<p>
	* To show support and find out more: <br /> <a href="http://
	www.petitionbuzz.com/petitions/ffppob" target="_blank">
	www.petitionbuzz.com/petitions/ffppob</a> <br /> <a href="http://
	www.charter97.org/en/news/2011/1/13/35188" target="_blank">
	www.charter97.org/en/news/2011/1/13/35188</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1092), 126); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1092);

				if (SHOWMESSAGES)	{

						addMessage(1092);
						echo getMessages(1092);
						echo showAddMessage(1092);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


