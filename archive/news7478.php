<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 747 - 11th November 2010 - Mexico: Day Of The 10,000 Dead</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, narco wars, organised crime" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 747 Articles: </b>
<p><a href="../archive/news7471.php">Losing Their Faculties</a></p>

<p><a href="../archive/news7472.php">Raven Mad For It</a></p>

<p><a href="../archive/news7473.php">Schmovies Dvd Launch</a></p>

<p><a href="../archive/news7474.php">Nukes? Gorle-blimey!</a></p>

<p><a href="../archive/news7475.php">Squat Crackdown, Bristol Fashion</a></p>

<p><a href="../archive/news7476.php">All Party'd Out</a></p>

<p><a href="../archive/news7477.php">Got The Hump Back</a></p>

<b>
<p><a href="../archive/news7478.php">Mexico: Day Of The 10,000 Dead</a></p>
</b>

<p><a href="../archive/news7479.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 11th November 2010 | Issue 747</b></p>

<p><a href="news747.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MEXICO: DAY OF THE 10,000 DEAD</h3>

<p>
<p>
	On 3rd November, as Mexicans wind down the famous festival &#39;The Day of the Dead&#39; the Mexican National Commission of Human Rights released the most complete study of this year&#39;s narco (drug) violence. Between the beginning of January 2010 and November 1st, 10,035 people are known to have been killed in Mexico&#39;s narco-civil-war.Ten thousand dead in ten months. One thousand per month. Over 2,700 of these killings occurred in one city, Ciudad Juarez. Over one percent of the children of Ciudad Juarez are orphans thanks to the war on/for narcotrafficking. Ciudad Juarez is officially the most violent city in the world (or perhaps joint 1st place with Mogadishu). </p>
<p>
	<strong>GONE LOCO IN ACAPULCO</strong> </p>
<p>
	Massacres, more or less indiscriminate, are on the rise at an alarmingly rapid rate. Last week, 14 people were killed at a car wash, another 16 at a party, and more at a drug rehab centre. On 30th of September twenty workers taking a holiday together in Acapulco &#39;disappeared&#39; - kidnapped by the narcos. Apparently taken by mistake (the US army doesn&#39;t have a monopoly on collateral damage), no word has been heard, either from the victims or from their kidnappers. </p>
<p>
	Mexico&#39;s politicians never cease to amaze with their level of stupidity, but this year&#39;s &#39;Pendejo of the Year award (trans: Pendejo- complete and utter @*nt) might just go to the mayor of Acapulco who said, in response to the mass kidnapping, &ldquo;Don&#39;t worry, Acapulco is safe, people don&#39;t just disappear. It&#39;s not the Bermuda Triangle you know.&rdquo; On 2nd November police in Guerrero were directed to a &#39;narcofosso&#39; (mass grave of gang victims) containing 20 bodies. Of these 18 were confirmed as the bodies of the missing tourists. </p>
<p>
	<strong>GULF WAR</strong> </p>
<p>
	Meanwhile the militarisation of President Calderon&#39;s drug war continues to escalate with no clear objective or end in sight. The latest high profile operation targeted the leader of the Gulf Cartel, Tony Tormenta. The marines, federal police and elite army units took out their man in his safe house in the border city of Matamoros last week. It took 650 troops to kill him, and 300 grenades were used in the operation. The Gulf Cartel tried to rescue their leader with a reaction force of some 80 4x4s full of armed gangsters, but, when it was obvious that he was dead (there was nothing left of the building) they went back underground. Government figures put the death toll for the weekend&#39;s violence at eight. Newspapers based in Brownsville, Texas, on the US side of the border, put the death toll at around 100, based on eyewitness reports. </p>
<p>
	The violence was further complicated by the involvement of the Zeta Cartel, who also fought running battles with with the Gulf Cartel. Many people speak of a conspiracy between the Zetas and the government- pointing to the likely source of the &#39;intelligence&#39; that led to his discovery (the Zetas used to work for the Gulf Cartel). More likely is that the Mexican government just needs to regularly show scalps to their US bosses and the world press to prove success in the drug war, without any thought for strategy beyond the immediate term. The Zeta&#39;s have a reputation for being the most ruthless and violent cartel, and it&#39;s hard to see how replacing the Gulf Cartel with the Zetas could help the government in its war. </p>
<p>
	In such a bleak situation it&#39;s hard for civil society groups to organise, with so much of the north (and more) of the country paralysed with fear. But actions of one sort or another have been spotted. In Guadalajara, a city with a reputation for being a centre of calm and peace, a grenade attack in a residential neighbourhood left five injured, including two three-year-old kids. &#39;Concerned citizens&#39; responded by posting signs (in the style of &#39;no littering&#39; signs) around the neighbourhood urging people &ldquo;<em>please don&#39;t shoot, throw your weapons away in the bins provided</em>.&rdquo; </p>
<p>
	Other, more ballsy (or just plain fed up) groups have taken a more direct approach. In Ciudad Juarez (see above) over the weekend of the 29th of October the city held the &#39;Forum Against Violence and Militarisation.&#39; Part of the event was a peace rally held near the university. A bit of enthusiastic chanting riled the Federal police, who responded by shooting 19 year old student (and member of the Zapatista&#39;s &#39;Other Campaign&#39;) Jos&eacute; Dar&iacute;o Alvarez Orrantia in the back with an assault rifle. He survived, but only just, minus much of his intestines. That&#39;s what happens to people who are willing to fight for peace in Ciudad Juarez. </p>
<p>
	<strong>For more info see:</strong> <br /> <a href="http://
	www.somoslosdemas.wordpress.com" target="_blank">
	www.somoslosdemas.wordpress.com</a> (news and analysis, irregularly updated) <br /> <a href="http://
	www.laotradivisiondelnorte.wordpress.com" target="_blank">
	www.laotradivisiondelnorte.wordpress.com</a> (Spanish, but more satirical graphics that text) </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1011), 116); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1011);

				if (SHOWMESSAGES)	{

						addMessage(1011);
						echo getMessages(1011);
						echo showAddMessage(1011);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


