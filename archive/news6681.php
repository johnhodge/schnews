<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 668 - 13th March 2009 - Taking It All In</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, israel, egypt, aid convoy, west bank, international solidarity movement" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 668 Articles: </b>
<b>
<p><a href="../archive/news6681.php">Taking It All In</a></p>
</b>

<p><a href="../archive/news6682.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6683.php">Zion And Off</a></p>

<p><a href="../archive/news6684.php">Squats Yer Lot</a></p>

<p><a href="../archive/news6685.php">On Garda</a></p>

<p><a href="../archive/news6686.php">Hell-icopter</a></p>

<p><a href="../archive/news6687.php">Tied In Legal Notts</a></p>

<p><a href="../archive/news6688.php">G20-20 Vision</a></p>

<p><a href="../archive/news6689.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/668-gaza-ambulance-lg.jpg" target="_blank">
													<img src="../images/668-gaza-ambulance-sm.jpg" alt="And you think the UK Health service is under attack..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 13th March 2009 | Issue 668</b></p>

<p><a href="news668.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>TAKING IT ALL IN</H3>

<p>
<span style="font-size:13px; font-weight:bold">EYEWITNESS ACCOUNTS AS UK AID CONVOY REACHES GAZA</span> <br />
 <br />
Last Saturday afternoon the first of the 110-vehicle convoy from the UK entered Gaza after a three-week, 6,000-mile journey through Europe and north Africa. Just before the four 7.5 ton trucks, four transit vans, an ambulance and jeep entered the besieged territory, a shell fired by an Israeli tank exploded a few hundred metres away. <br />
 <br />
Most of the over 5,000 injured during the Israeli bombardment struggle to get medical care due to the Israeli-Egyptian siege. According to the UN, 43% of the injured were wounded by shrapnel, with spinal cord injuries common. Hospital staff say they are struggling to provide medical care with intermittent electricity supplies and shortages of items like wheelchairs and medication as well as the more sophisticated equipment needed for patients with paralysis.  <br />
 <br />
Fifteen hospitals out of a total of 27, and 41 primary health care clinics out of 118 in Gaza were damaged during the war, according to the World Health Organisation, and about half the ambulance fleet was damaged or destroyed. Patients die daily through lack of medical treatment in Gaza and Israel/Egypt&#8217;s refusal to let them leave. <br />
 <br />
Convoy organiser Abdul Aziz says that the medical supplies they were carrying were handed over to Gaza&#8217;s largest hospital, Al-Shifa, as well as the keys to several vehicles. He describes conditions in the hospital as &#8220;desperate&#8221;, saying that a British doctor working at the hospital reckoned that people were dying for the lack of medicines that would cost �10. <br />
 <br />
On Sunday, the rest of the vehicles carrying medical aid worth $1.4m were let in by the Egyptian authorities, while those with non-medical aid like clothes, toys, generators, a boat and a fire engine had to enter Gaza through an Israeli-controlled crossing. <br />
 <br />
Saair Yildirim, a driver from Bristol acknowledged the reception in Gaza had been &#8220;tremendous&#8221;, but was shocked to see the wreckage of ambulances destroyed by Israeli fire. <br />
 <br />
And the journey itself had not been an easy one. Despite meeting cheering crowds in the other north African countries, the Egyptian authorities had prevented any show of support for the convoy, tightly managing it&#8217;s progress. <br />
 <br />
Herded through the country with police and soldiers practically every 50 metres, the convoy was then kept in the Sinai town of Al Arish for two days during which time it was attacked by the police and unknown thugs.  <br />
 <br />
According to convoy member Yvonne Ridley, &#8220;<i>hundreds of riot squad officers, wearing visors, carrying shields and batons tumbled in to one of the two car parks in a large town centre compound in the port of al Arish and set about the unarmed peace activists</i>&#8221;. Later, as night fell, &#8220;<i>suddenly the area was plunged into darkness by a power cut which coincided with brick, bottle and stone attacks on the convoy by youths. Seconds before the lights went out some convoy members saw a couple of unidentified men scrawling anti-Hamas slogans on the lorries. The lights remained out for some minutes, during which time the vicious attack was unleashed. The whole proceedings failed to warrant one single Egyptian police officer swinging his baton into action. Those who had wielded their sticks with such a passion before, stood impassively by and watched the onslaught.</i>&#8221; <br />
  <br />
Such treatment can only be expected from a regime that routinely quashes any sign of support for their Palestinian neighbours. Magdi Hussein, secretary-general of Egypt&#8217;s banned Socialist Labor Party, was recently sentenced to two years prison by a military court for &#8216;infiltrating&#8217; into the Gaza Strip, while two other activists received a year each for the same &#8216;crime&#8217;. The US-puppet Mubarak regime had efficiently fulfilled their masters&#8217; orders to discredit the democratically elected Hamas government in crude attacks on the solidarity convoy.  <br />
 <br />
After eventually making it to their final destination, the stark reality of post-war Gaza struck the convoy members immediately. According to one, Naveed, &#8220;<i>Reality hit home. The very reason we embarked on this journey was there in front of our naked eyes. Seeing the destruction on the TV on our nice cosy sofas at home is one thing, but to see it in real life is another. Each hole in a building, each burnt vehicle, each pile of rubble has its own story to tell, about how a Palestinian life was lost, how a Palestinian child became an orphan, how a mother became childless, how a wife became a widow. Today we will hear these stories so that we may report back to the world.</i>&#8221; <br />
 <br />
<span style="font-size:13px; font-weight:bold">THE STRANGLEHOLD CONTINUES</span> <br />
 <br />
Since the end of the 22-day bombardment of Gaza in mid-January (See <a href='../archive/news661.htm'>SchNEWS 661</a>), Gaza has continued to be sealed from the outside world by Israel and Egypt with only humanitarian aid allowed in, as has been the case since Hamas seized power in June 2007. Factories, government buildings, schools and Gaza&#8217;s biggest wheat flour mill were destroyed. Today over 90% of the people are totally dependent on UN food aid. Lacking basic materials like glass and cement Gazans have been unable to rebuild their demolished homes and infrastructure. <br />
The Israeli military has continued to target civilians. Four Palestinian farmers have been shot by Israeli forces while working within 700m of the &#8216;Green Line&#8217; since January 18th.   <br />
 <br />
On February 14th, the Israeli navy attacked several fishing boats in Gazan territorial waters two nautical miles out from the port of Gaza city, damaging the boats and shooting 23 year-old Rafiq abu Reala. Palestinian fishermen have come under daily assaults from Israeli gunboats since Israel announced a it&#8217;s &#8216;unilateral ceasefire&#8217;. Rafiq is the third Gazan fisherman to be shot by the Israeli navy during this ceasefire. Due to such attacks fishing has almost completely stopped and contributed to malnutrition. Fish has always been a key source of protein for Gazans, especially since the siege. <br />
 <br />
On February 24th, Palestinian farmers, accompanied by international Human Rights Workers, were fired upon as they attempted to work on land around 300m from the &#8216;Green Line&#8217;. Eva Bartlett of the International Solidarity Movement reported, &#8220;<i>We were only in the fields for about five minutes before the Israeli forces began firing. I believe the firing was coming from four army jeeps and a hummer. The shots were coming very close, and were sniper-type shots.</i>" <br />
 <br />
&#8220;<i>One old woman was so paralysed by fear that she couldn&#8217;t move off the ground before we were finally able to accompany her out of the fields. While the majority of the farmers left the area, some say they must return to work the land later on in the day. There is great concern that the Israeli army will continue their targeting of these farmers.</i>&#8221; Later that same day, seventeen-year-old Wafa An-Najjar was shot in the leg by Israeli forces when she returned with her mother and brother to see the remains of their demolished family home. <br />
 <br />
And meanwhile, business continues as usual in the West Bank. On Wednesday an unarmed youth was shot and killed by the Israeli military near Ramallah. At a demo against the Apartheid Wall in Nilin last week Israeli forces shot four youths with a new type of live ammunition, a special low calibre bullet known as the &#8216;0.22&#8217;. All were shot in the leg and hospitalised.  <br />
 <br />
In short, Israel&#8217;s bloody and inhumane siege of Gaza continues and only hardens the determination of international activists, at often great personal risk, to offer help, support and the chance to tell the world what&#8217;s really going on there.  <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
<div align="center"> <br />
For updates see <br />
* <a href="http://www.rodcoxandgaza.blogspot.com" target="_blank">www.rodcoxandgaza.blogspot.com</a> <br />
* <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> <br />
* <a href="http://www.vivapalestina.org" target="_blank">www.vivapalestina.org</a> <br />
* <a href="http://www.palsolidarity.org" target="_blank">www.palsolidarity.org</a> <br />
</div>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>aid convoy</span>, <span style='color:#777777; font-size:10px'>egypt</span>, <a href="../keywordSearch/?keyword=gaza&source=668">gaza</a>, <a href="../keywordSearch/?keyword=international+solidarity+movement&source=668">international solidarity movement</a>, <a href="../keywordSearch/?keyword=israel&source=668">israel</a>, <span style='color:#777777; font-size:10px'>west bank</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(239);
			
				if (SHOWMESSAGES)	{
				
						addMessage(239);
						echo getMessages(239);
						echo showAddMessage(239);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>