<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 793 - 21st October 2011 - Dale Farm Evicted</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 793 Articles: </b>
<b>
<p><a href="../archive/news7931.php">Dale Farm Evicted</a></p>
</b>

<p><a href="../archive/news7932.php">Crap Alchemist Of The Week</a></p>

<p><a href="../archive/news7933.php">Occupy Everywhere</a></p>

<p><a href="../archive/news7934.php">Uganda: Dropping A Kalanga</a></p>

<p><a href="../archive/news7935.php">Lsx: Occupy Eyed</a></p>

<p><a href="../archive/news7936.php">Brought To The Book</a></p>

<p><a href="../archive/news7937.php">Blackpool Rocked</a></p>

<p><a href="../archive/news7938.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st October 2011 | Issue 793</b></p>

<p><a href="news793.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DALE FARM EVICTED</h3>

<p>
<p>  
  	<strong>AS IRISH TRAVELLERS ETHNICALLY CLEANSED FROM ESSEX</strong>  </p>  
   <p>  
  	At 7am on Wednesday (19th) morning hundreds of riot police stormed the rear of Dale Farm (see <a href='../archive/news669.htm'>SchNEWS 669</a>, <a href='../archive/news785.htm'>785</a>). They met with stiff resistance on the barricades, resorting to Tasering at least one person as they ran away from them. Eventually breaching the outer wall of the site, police then had to fight their way through several more roving barricades (some aflame) as they made their way to the main gate under a hail of half-bricks.  </p>  
   <p>  
  	The nature of the attack surprised even the most battle hardened protesters on site. There was no visible attempt to deploy evidence gathering teams and barely any media present until later in the day (though one camera crew did get footage of the first Tasering of the morning). In the words of one activist &ldquo;it was a more continental style of policing&rdquo;, with police relying more on physical violence than the threat of judicial violence.  </p>  
   <p>  
  	Only once police had secured the rear of the main gate were any arrests even attempted. A couple of people got taken by a snatch squad, but the vast majority of arrests were of protesters manning towers and lock-ons. Others were able to regroup and attack lines of cops almost at leisure, with the&nbsp; bulk of the police guarding the main gate from the inside. During the clearing of the gate one quick witted activist managed to D-lock a cherry picker to the scaffold tower it was sent in to evict. Though a couple of attempts were made to get more territory under police control they were quickly forced to retreat. Despite the police claim (circulated widely by most media present) that they had secured the whole of the site within 45 minutes, this was clearly not the case.  </p>  
   <p>  
  	No bailiffs were present at the initial assault, when they did arrive (around 9am) they made an attempt to get vehicles on site through the newly created hole in the fence. However this was thwarted by activists up trees and scaffolding making the job of widening the gap impossible.  </p>  
   <p>  
  	It was not until the following afternoon (once the last of the lock-ons had been removed) that any machinery would have been able to get past the main gate &ndash; by which time those left on site had decided to walk off together. Despite being aware of this, the council decided to then send in a representative to serve eviction notices on all those present &ndash; cue farcical scene of a heavily guarded council lackey being followed around site by a baying mob.  </p>  
   <p>  
  	This led to some having second thoughts about the walkout (no one wants to leave when they&rsquo;re told to) though it went ahead as planned &ndash; albeit a couple of hours late.  </p>  
   <p>  
  	Now that the flames have turned into smouldering ashes, the barricades have been bulldozed along with what was once a settled community, and the last residents of Dale Farm walk away from their home into a displaced existence, what else is their to say about an event that will almost certainly be remembered by history for what it was: an brutal attack on a persecuted minority at the hands of the state, after abject failure by the legal system to protect their human rights?  </p>  
   <p>  
  	The amount of bullshit spouted by the authorities during the eviction, and happily relayed without scrutiny by mainstream media dimshits, deserves a special round-up all of its own.  </p>  
   <p>  
  	Step up, Councillor Tony Ball: &ldquo;Sadly, this [travellers leaving] could have been achieved many years ago and without the scenes of violence which we have witnessed over the last 48 hours and the accompanying expense to the taxpayer.&rdquo;  </p>  
   <p>  
  	Basildon Council has led a concerted campaign to oust the Dale Farm community from the land it owns, knowing that it would cost at least &pound;18 million before it even kicked off. Though the right-wing press are attempting to blame the travellers and supporters for the bill, it was the council who ultimately decided it was worth devoting a vast amount of resources to knock down a small number of structures built on an old scrapyard (many of the buildings on the &lsquo;illegal&rsquo; half of Dale Farm are actually completely legal).  </p>  
   <p>  
  	The fight is not over yet, with travellers and supporters deciding what to do next. For more info see dalefarm.wordpress.com  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1394), 162); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1394);

				if (SHOWMESSAGES)	{

						addMessage(1394);
						echo getMessages(1394);
						echo showAddMessage(1394);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


