<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 781 - 29th July 2011 - In League with the Devil?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, fascism, daily mail, islamophobia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 781 Articles: </b>
<b>
<p><a href="../archive/news7811.php">In League With The Devil?</a></p>
</b>

<p><a href="../archive/news7812.php">Prison Hunger Strikes</a></p>

<p><a href="../archive/news7813.php">Not So Neat</a></p>

<p><a href="../archive/news7814.php">Stirchley Speaking</a></p>

<p><a href="../archive/news7815.php">Targeting Refugees</a></p>

<p><a href="../archive/news7816.php">Is-raeli Expensive Here!</a></p>

<p><a href="../archive/news7817.php">Shelling Out</a></p>

<p><a href="../archive/news7818.php">Sea Shepherd: Not Fareoes</a></p>

<p><a href="../archive/news7819.php">Vedenta: Tin Foiled</a></p>

<p><a href="../archive/news78110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th July 2011 | Issue 781</b></p>

<p><a href="news781.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IN LEAGUE WITH THE DEVIL?</h3>

<p>
<p>  
  	<strong>AS SCHNEWS TAKES AIM AT THE &lsquo;DAILY MAIL&rsquo; MASSACRE</strong>  </p>  
   <p>  
  	<strong><em>&quot;The lunatic is all idee fixe, and whatever he comes across confirms his lunacy. You can tell him by the liberties he takes with common sense, by his flashes of inspiration, and by the fact that sooner or later he brings up the Templars.&rdquo;</em> - Umberto Eco, Foucault&rsquo;s Pendulum</strong>  </p>  
   <p>  
  	The Norway attacks have led to frenzied speculation on the nature of the crazed ideology that could lead to such horrific acts. Handily Andre Brievik (or Andrew Berwick) not only allowed himself to be captured but posted a 1500 page manifesto online - grandly entitled 2083 - A European Declaration of Independence (<a href="http://www.slideshare.net/darkandgreen/2083-a-european-declaration-of-independence-by-andrew-berwick" target="_blank">http://www.slideshare.net/darkandgreen/2083-a-european-declaration-of-independence-by-andrew-berwick</a>), and apart from a load of weird stuff about body-armour, the Knights Templar and the appropriate use of steroids, at the heart is a vision eerily familiar to readers of such fringe esoteric publications&nbsp; as say the Daily Express, Daily Mail or Sun.  </p>  
   <p>  
  	The fact that before the smoke had cleared from the first detonation in Oslo the mainstream media immediately pointed the finger at Al-Qaeda or one of its offshoots shows that their underlying assumptions aren&rsquo;t so different from the self-proclaimed Knight Templar Justiciar with the &lsquo;piercing blue eyes&rsquo; - (&copy; all newspapers.).  </p>  
   <p>  
  	Experts and pundits queued up to explain why peaceful, civilised Norway had been attacked by the jihadist hordes. The Sun called it Norway&rsquo;s 9/11 and even name-checked Al-Qaeda. Even when the Norwegian press revealed that the killer was an &lsquo;ethnic Norwegian&rsquo; the pundits raised the prospect of a brainwashed convert to Islam. A spectacular amount of back-pedalling ensued when the actual facts surfaced (not that this of course stopped the very same commentators becoming sudden experts on the far-right)  </p>  
   <p>  
  	<strong>PHILIPPS SKREWDRIVER</strong>  </p>  
   <p>  
  	The idea that we are at war with the entire Muslim World and that a &lsquo;politically correct&rsquo; elite has manipulated immigration to the detriment of the ethnic majority is now a mainstream belief in Western Europe. There&rsquo;s no need to go trawling through SpearofOdin88&rsquo;s lunatic ramblings on Stormfront when you can go straight to the Daily Mail and read Melanie Philipps, who recently demanded to know &ldquo;Is Britain finally about to go over the cliff into official Islamisation?&rdquo;. Philipps is also author of Londonistan, the English Defence League&rsquo;s (EDL) bible, she has the honour of having parts of her work reproduced in their entirety in Breivik&rsquo;s manifesto, but rather side-stepped the issue this week by focussing instead on the tragic but perhaps less significant demise of Amy Winehouse.  </p>  
   <p>  
  	The Daily Mail has now denounced Breivik as neo-Nazi, despite his explicit rejection of Nazism - in fact politically he wasn&rsquo;t much more right-wing than them. There is now a concerted attempt to divorce Breivik&rsquo;s ideas from his actions - to suggest that he was just &lsquo;insane&rsquo; or &lsquo;sick&rsquo;, even calls to not allow his monstrous actions to &lsquo;shut down the debate on immigration&rsquo;. Even the notion of demonic possession has entered the debate.When jihadists commit atrocities there isn&rsquo;t usually such a rush to let their ideology off the hook.  </p>  
   <p>  
  	It&rsquo;s the Desmond papers that lead the pack though - in any given week the Express is rarely without an anti-immigrant front-page splash and generally one that suggests EU compulsion such as &lsquo;Eurocrats will force British families to have an asylum-seeker in&nbsp; spare bedroom&rsquo;. OK, we made that one up but in fact the headline BRITAIN &lsquo;MUST TAKE MORE MIGRANTS&rsquo; was last Friday&rsquo;s offering, claiming the EU bureaucrats are forcing the pace on migration. The idea that the left is forcing &lsquo;political correctness&rsquo; down the the throats of the &lsquo;silent majority&rsquo; of the population is continually represented as a given. The pornographer&rsquo;s other paper the Star has virtually become the official paper of the EDL.  </p>  
   <p>  
  	A big part of this ideology is an old racist canard - the demographic time-bomb. The fear is that&nbsp; &lsquo;They&rsquo; will outbreed us and force us to adapt to their culture. Of course the fact that all this was being said in the 70s about the Afro-Caribbean population and before that the Irish and the Jews doesn&rsquo;t stop&nbsp; the idea. &ldquo;We have only a few decades to consolidate a sufficient level of resistance before our major cities are completely demographically overwhelmed by Muslims&rdquo; - Breivik &ldquo;Will the white British population be in a minority in 2066?&rdquo; - headline Daily Mail December 2010.  </p>  
   <p>  
  	From the outside of this particular reality tunnel, it seems insane to fear that the U.K could become subject to anything like sharia law. Apart from a few hardliners,many of them zealous converts, nobody Muslim wants there to be sharia law here either. Yet resistance to this is the main platform of the EDL, who at the same time have no idea&nbsp; about what the culture is that they are defending - is it British, English or white European?&nbsp;&nbsp; Breivik is sure that the foundation of Western Civilisation should be Catholicism -some of the EDL make vague noises about Christianity but don&rsquo;t really seem that sure who they&rsquo;re in league with or what they&rsquo;re defending.  </p>  
   <p>  
  	In fact it&rsquo;s fair to say that a large part of the population exists in an echo-chamber, where these are received truths and amount to &lsquo;common sense&rsquo;. The online world, rather than a gateway to a variety of&nbsp; points of view provides a fertile ground for reinforcement of ideas (something the left should be as wary of as the right). Backing up the overt statements in the mainstream press are a host of circular e-mails, with a racist agenda doing the rounds as well as blogs, facebook postings etc. The EDL are the physical manifestation of this belief system. It&rsquo;s because their beliefs are reinforced from all sides that they&rsquo;ve been able to grow so quickly.  </p>  
   <p>  
  	So in a sense - it doesn&rsquo;t matter whether Breivik was in direct contact with the EDL, whether he regarded them as &lsquo;naive&rsquo; or an inspiration- their ideas spring from the same soil. However evidence is emerging that he admires them, did attend several of their demos and was active on their forums and Facebook. He is positive about their provocation/reaction strategy, baiting Muslim youth into a reaction. The EDL is very much an online phenomenon - they don&rsquo;t do public meetings or paper sales, their membership exists in the right wing &lsquo;echo-chamber&rsquo;. Who or what else is going to bubble up from the EDL cauldron?  </p>  
   <p>  
  	<strong>&ldquo;<em>It is highly advisable to structure any street protest organisation after the English Defence League (EDL) model as it is the only way to avoid paralyzing scrutiny and persecution</em>&rdquo; - Breivik.</strong>  </p>  
   <p>  
  	<strong>NORSE CODE</strong>  </p>  
   <p>  
  	The EDL are also part of a new-wave of European nationalism, one that claims to defend a &lsquo;pan-European&rsquo; culture. The U.K is in a strange situation with the far-right. Despite a certain overlap in membership there is little co-operation between the EDL and the BNP. The EDL articulate a modern racism, based on Englishness rather than Britishness. In fact attempts to set up Welsh and Scottish Defence Leagues have proved a failure. The BNP are still too firmly tied to old-school &lsquo;It was the Elders of Zion wot dunnit&rsquo; white supremacist fascism. The EDL has side-stepped these issues and by representing the conflict with Islam as cultural rather than racial, have out-boxed their flatter footed leftwing opposition (for now).  </p>  
   <p>  
  	The EDL are closer to the likes of Geert Wilders, leader of the Netherlands third largest political party - the Freedom Party, whose main platform is anti-Islamic. He travelled to the UK in March 2010, after a ban on his presence was overturned. This event was greeted with great enthusiasm by the EDL who organised a rally for him. Other European politicians have latched onto the fear of Islam and they too are fascinated by the EDL as a potential model for a street army. The EDL leadership in turn have shown an interest in more conventional electoral politics in the UK.  </p>  
   <p>  
  	In a time of economic crisis, ideas like these are dangerous. As Mark Twain put it &ldquo;History doesn&rsquo;t repeat itself, but it does rhyme&rdquo;. The scapegoating of outsider groups is a tempting path for ruling elites in turbulent times. If one good thing comes out of Breivik&rsquo;s murder spree it will be a re-examination of the real meaning of the anti-Islamic agenda. The worst thing to do in these circumstances would be to dismiss his actions as those of a &lsquo;lone nutter&rsquo;.  </p>  
   <p>  
  	The EDL&rsquo;s next major outing is September 3rd in Tower Hamlets - anti-fascists will be counter-mobilising.  </p>  
   <p>  
  	More good analysis - <a href="http://libcom.org/library/new-integralist-conservatism-briefing" target="_blank">http://libcom.org/library/new-integralist-conservatism-briefing</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1301), 150); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1301);

				if (SHOWMESSAGES)	{

						addMessage(1301);
						echo getMessages(1301);
						echo showAddMessage(1301);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


