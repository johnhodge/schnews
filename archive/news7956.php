<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 795 - 4th November 2011 - Hearts of Oakland</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 795 Articles: </b>
<p><a href="../archive/news7951.php">Dedicated Followers Of Fascism</a></p>

<p><a href="../archive/news7952.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7953.php">Squat Next?</a></p>

<p><a href="../archive/news7954.php">Extraordinairy Petition</a></p>

<p><a href="../archive/news7955.php">Bring 'em To Book</a></p>

<b>
<p><a href="../archive/news7956.php">Hearts Of Oakland</a></p>
</b>

<p><a href="../archive/news7957.php">Quake Up Call</a></p>

<p><a href="../archive/news7958.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th November 2011 | Issue 795</b></p>

<p><a href="news795.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HEARTS OF OAKLAND</h3>

<p>
<p>  
  	<strong>OCCUPY EVERYWHERE ROUND UP</strong>  </p>  
   <p>  
  	Amonst all the violence this week, Occupy Oakland was the site of an assault on an Iraq veteren, Scott Olsen, by police on the 25th October. He was shot in the face with a gas canister that fractured his skull. As others rushed to help cops threw a thunderflash.  </p>  
   <p>  
  	Sometimes it only takes one; in Tunisia it was a fruit stall holder setting himself on fire, in Greece the death of a 15 year old boy...could this be the moment that stirs more of America into action?  </p>  
   <p>  
  	Occupy Oakland General Assembly responded, by consensus, to the clearing of their camp by calling for a city-wide general strike on Wednesday (2nd). The last general strike in Oakland was back in 1946.  </p>  
   <p>  
  	On the day thousands marched down to Oakland&rsquo;s port (one of the US&rsquo;s busiest) and shut it, and much of the city, down. Cops used teargas and stun grenades to attack the protesters later that night. By midnight an unused building had become a beacon for protesters, and a party was in full swing, but not for long as large numbers of police in full riot gear crashed the blowout.  </p>  
   <p>  
  	* See&nbsp; <a href="http://www.occupyoakland.org" target="_blank">www.occupyoakland.org</a>  </p>  
   <p>  
  	<u><strong>London:</strong></u> Protesters plan to occupy Canary Wharf on November 30th during the public sector general strike (<a href="http://www.facebook.com/pages/Occupy-Canary-Whaft/215157485213150" target="_blank">www.facebook.com/pages/Occupy-Canary-Whaft/215157485213150</a>). Canary Wharf Group have pre-emptively secured a High Court injunction to prevent Occupy LSX protesters loitering in the East London business district.  </p>  
   <p>  
  	Meanwhile the Occupy London protesters may well get to spend the festive season outside St Paul&rsquo;s Cathedral. The City of London has promised to put off eviction measures until the Christmas bells cease to jingle. Negotiations between protesters and officials&nbsp; resulted in the offer to halt eviction for now in return for the number of tents being reduced. Occupy London member Tina Rothery said: &lsquo;We would have to make a slight reduction in tents in order to free up space for the fire brigade. There&rsquo;s a hindrance of access for St Paul&rsquo;s churchyard. We&rsquo;re not blocking it but they would like more space.&rsquo; After the U-turns from St. Pauls top brass, the Archbishop of Canterbury Rowan Williams came out sounding sympathetic to the cause. He spoke out for a &lsquo;Robin Hood tax&rsquo; on banks and understands &lsquo;the widespread resentment with the financial establishment&rsquo;  </p>  
   <p>  
  	* <a href="http://occupylondon.org.uk" target="_blank">http://occupylondon.org.uk</a>  </p>  
   <p>  
  	<u><strong>Bournemouth:</strong></u> The council have now announced they&rsquo;ll be seeking eviction in court on Friday(4th).  </p>  
   <p>  
  	<u><strong>Birmingham:</strong></u> Protesters in Birmingham city centre moved camp on Wednesday(2nd) from Victoria square to the community gardens by Symphony Hall, following the council&rsquo;s request to make way for an annual German Christmas market! How bloody civilised. The protest movement against corporate greed which has spread across four continents takes a back step for Christmas... Protesters said &ldquo;there is no contradiction between our aims and moving for the Christmas traders.  </p>  
   <p>  
  	&rdquo; No comment.* <a href="http://occupybirmingham.org" target="_blank">http://occupybirmingham.org</a>  </p>  
   <p>  
  	<u><strong>Germany:</strong></u> Thousands of protesters took to the streets in Germany this Saturday(29th) to protest over the wealth schism in our society. Occupy activists stationed outside the European Central Bank in Frankfurt plan to stay camped for the next two weeks. According to organising group Attac 5,000 demonstrators marched in Frankfurt past the German National Bank HQ and the European Central Bank. In Berlin, 1,000 protesters congregated outside the main city hall in similar furore. Further protests are planned in both cities for November 12.  </p>  
   <p>  
  	* <a href="http://occupygermany.org/" target="_blank">http://occupygermany.org/</a> <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1417), 164); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1417);

				if (SHOWMESSAGES)	{

						addMessage(1417);
						echo getMessages(1417);
						echo showAddMessage(1417);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


