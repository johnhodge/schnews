<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 699 - 13th November 2009 - Veg Of  Darkness</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, carmel agrexco, israel, palestine, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://natowc.noflag.org.uk/"><img 
						src="../images_main/nato-pa-09-banner.png"
						alt="Smash NATO"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 699 Articles: </b>
<p><a href="../archive/news6991.php">Mexican Wave</a></p>

<b>
<p><a href="../archive/news6992.php">Veg Of  Darkness</a></p>
</b>

<p><a href="../archive/news6993.php">Corporal Punishment</a></p>

<p><a href="../archive/news6994.php">Extermi-nato</a></p>

<p><a href="../archive/news6995.php">Big Peace Up</a></p>

<p><a href="../archive/news6996.php">Bad Apples</a></p>

<p><a href="../archive/news6997.php">Cross Words</a></p>

<p><a href="../archive/news6998.php">Unfasten Yer Seatbelts</a></p>

<p><a href="../archive/news6999.php">Tomlinson Vigil</a></p>

<p><a href="../archive/news69910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 13th November 2009 | Issue 699</b></p>

<p><a href="news699.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>VEG OF  DARKNESS</h3>

<p>
Israeli settlement profiteers <strong>Carmel Agrexco</strong> suffered three days of disruption to their internationally illegal export business this week as feisty protesters launched the most audacious sustained blockade of the Israel-owned depot yet. <br />   <br /> Between Friday 6th November and Sunday the 8th, activists endured crappy weather and attitude from Carmel staff and police at the headquarters in Hayes, Middlesex. On the Friday, London students, Anarchists Against the Wall and the International Solidarity Movement joined (literally, with locks) together using fencing panels to carry out the blockade, managing to disrupt Carmel&rsquo;s business all day. Some activists were trapped inside the premises by the Carmel staff, who the cops claimed had the right to use &ldquo;<strong>reasonable force</strong>&rdquo; to remove those on their work premises, indicating when questioned that breaking their necks was consistent with &ldquo;reasonable force&rdquo;. The blockade lasted for eight hours before the d-lockers unlocked themselves. The best Carmel could do was to lift some produce over the gates with forklifts - not quite &lsquo;business as usual.&rsquo; None were arrested. <br />   <br /> Day two saw the protesters locked on to oil drums, blocking access roads, and suffering pain when police removed them. Three protesters were nicked and eventually those locked-on were dragged, still attached to their barrels, away from the gates by police after five hours. <br />   <br /> By Sunday (the final day of action) the Carmel staff had virtually given up, and a large public demo blockaded trucks. Five people were arrested, although the police have rarely charged anyone demonstrating against Carmel (the people nicked on the Valentine&rsquo;s protests were the first lot to be put on trial for three years (See <a href='../archive/news666.htm'>SchNEWS 666</a>, <a href='../archive/news698.htm'>698</a>).   <br />  <br /> The Palestinians are experiencing their 42nd year of occupation. Carmel Agrexco is a part- Israeli company that operates out of the Geneva Convention defying settlements in Palestine. Vegetables make up 80% of Israeli exports and the UK is their largest market.  <br />  <br /> Lately there&rsquo;s been a serious escalation in the actions against Carmel, and not before time as the Israeli government feels free to lurch further and further to the right. A ragtag group of British based protesters are able to do what even the US president seems incapable of &ndash; impact, however slightly, on the Israeli settlement programme.  <br />  <br /> * For information on direct action and the boycott see <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a> and <a href="http://www.bdsmovement.net" target="_blank">www.bdsmovement.net</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=carmel+agrexco&source=699">carmel agrexco</a>, <a href="../keywordSearch/?keyword=direct+action&source=699">direct action</a>, <a href="../keywordSearch/?keyword=israel&source=699">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=699">palestine</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(560);
			
				if (SHOWMESSAGES)	{
				
						addMessage(560);
						echo getMessages(560);
						echo showAddMessage(560);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>