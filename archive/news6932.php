<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 693 - 2nd October 2009 - Press Ganged</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, sussex police, edo decommissioners, smash edo, edo, newspapers" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 693 Articles: </b>
<p><a href="../archive/news6931.php">Who Are Ya?</a></p>

<b>
<p><a href="../archive/news6932.php">Press Ganged</a></p>
</b>

<p><a href="../archive/news6933.php">Given A Barak-ing</a></p>

<p><a href="../archive/news6934.php">On Yer Denmarks....</a></p>

<p><a href="../archive/news6935.php">Vestas Interests</a></p>

<p><a href="../archive/news6936.php">All Sizewell That Ends Well</a></p>

<p><a href="../archive/news6937.php">693 Schnews In Brief</a></p>

<p><a href="../archive/news6938.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd October 2009 | Issue 693</b></p>

<p><a href="news693.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PRESS GANGED</h3>

<p>
<strong>AS COPS USE JOURNALISTS FOR EVIDENCE GATHERING</strong> <br />  <br /> Earlier this week Sussex Police forced Brighton&rsquo;s local rag The Argus to hand over footage of the <strong>Brighton Mayday demo</strong> (see <a href='../archive/news675.htm'>SchNEWS 675</a>). Pushing a &lsquo;<strong>special procedure production order</strong>&rsquo;  through Lewes Crown Court, under section 9 of the Police and Criminal Evidence Act, the police got the courts to grant an order for &ldquo;all media containing moving and/or still images...published and unpublished of the march/procession/assembly/demonstration and subsequent disturbances which took place in Brighton, East Sussex on May 4, 2009.&rdquo; <br />  <br /> After crossing the hurdle of their own stupidity (the police had to resubmit their application to the courts as they&rsquo;d made crucial mistakes in their original submission), Sussex&rsquo;s boys in blue found it fairly easy to persuade the judges that it was necessary to dump on journalistic integrity from a great height in order to find the culprits responsible for kicking over some bins and redecorating the front of McDonalds and Barclay&rsquo;s. <br />  <br /> According to the legal team at Newsquest (The Argus&rsquo; dark overlords) it&rsquo;s &lsquo;fairly standard&rsquo; for the police to request/demand/threaten footage and information from journalists, with 700 demands like this made every year. What&rsquo;s different about this case is that, as anyone who was at the Mayday demo (or saw it on telly) will know, the entire event from beginning to end was already filmed by FIT cops and took place in some of the most CCTV&rsquo;d areas of the UK. Presumably, the police want to use The Argus&rsquo; footage to identify protesters. In other words, force the media into operating as a conscripted FIT team. <br />   <br /> We spoke to Argus crime reporter Ben Parsons about the implications for both the press and protesters of this latest police boot stamping on the fragile neck of civil liberties. Parsons said: <br />  <br /> &ldquo;<em>It puts us in a difficult position. We act in a spirit of co-operation but not collaboration with the police. The line we (Argus/Newsquest) take is that we won&rsquo;t just hand stuff over because they ask for it, they have to go through the formal legal process. We don&rsquo;t send our cameramen out to act as extra eyes for the police. It&rsquo;s not how we see our role, we&rsquo;re not data gathering source for the police</em>.&rdquo; <br />   <br />  <br /> <font face="courier new"><strong>The EDO decommissioners who targeted the Brighton-based arms factory back in January</strong> (See <a href='../archive/news663.htm'>SchNEWS 663</a>) have had their court case adjourned until May 17th 2010, due to delays in finding legal aid and the prosecution&rsquo;s seeming reluctance to provide evidence. <br />  <br /> Despite this, the planned week of solidarity events is going ahead during 17th - 26th of October. The line-up includes: <br />  <br /> <strong>* Sat 17th</strong> - Decommissioners solidarity demo in Bristol. Meeting opposite Hippodrome at 2pm  <br />  <br /> <strong>* Launch demo of Target Brimar</strong>, Manchester see http://targetbrimar.org.uk  <br />  <br /> <strong>* Mon 19th</strong> - Rally outside Brighton Town Hall 12 noon-2pm calling on Brighton and Hove Council to condemn EDO.  <br />  <br /> <strong>* Thurs 22nd</strong> - Rally 12noon-2pm outside the Foreign Office, London, demanding an end of arms exports to Israel.  <br />  <br /> <strong>* Fri 23rd</strong> &ndash; Bass Not Bombs benefit gig, Bristol, featuring Lowkey, Ironside, Dub Revolution and more. <br />  <br /> For more see <a href="http://www.smashedo.org.uk&nbsp;" target="_blank">www.smashedo.org.uk&nbsp;</a> <br /> </font> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=edo&source=693">edo</a>, <a href="../keywordSearch/?keyword=edo+decommissioners&source=693">edo decommissioners</a>, <a href="../keywordSearch/?keyword=newspapers&source=693">newspapers</a>, <a href="../keywordSearch/?keyword=smash+edo&source=693">smash edo</a>, <a href="../keywordSearch/?keyword=sussex+police&source=693">sussex police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(512);
			
				if (SHOWMESSAGES)	{
				
						addMessage(512);
						echo getMessages(512);
						echo showAddMessage(512);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>