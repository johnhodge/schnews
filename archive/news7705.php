<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 770 - 6th May 2011 - Maydays of Their Lives</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mayday" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT June 1st 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 770 Articles: </b>
<p><a href="../archive/news7701.php">Baa Baa Black Block</a></p>

<p><a href="../archive/news7702.php">Tomlinson Verdict</a></p>

<p><a href="../archive/news7703.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7704.php">Totally Stoked</a></p>

<b>
<p><a href="../archive/news7705.php">Maydays Of Their Lives</a></p>
</b>

<p><a href="../archive/news7706.php">Indymedia: From The Rubble Of Double Trouble</a></p>

<p><a href="../archive/news7707.php">Spineless Facebook</a></p>

<p><a href="../archive/news7708.php">A Different Thanet</a></p>

<p><a href="../archive/news7709.php">The Lying Dutchman</a></p>

<p><a href="../archive/news77010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 6th May 2011 | Issue 770</b></p>

<p><a href="news770.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MAYDAYS OF THEIR LIVES</h3>

<p>
<p>  
  	One and a quarter centuries after the police killing spree against workers in Haymarket Sq, Chicago, the workers are still a-agitatin&rsquo;. Mayday 2011 saw demonstrations against cuts, joblessness, inequality and casino capitalism up and down the country, and around the world.  </p>  
   <p>  
  	The largest UK crowd assembled in London, with over ten thousand stomping through the city streets. Although the march was peaceful, a scout of the surrounding streets showed that several riot vans were never too far away, and the small black bloc presence at the back was closely guarded by some high-vis companions. Prominent amongst the marchers was a large number of Sri Lankans, protesting against the genocide of the Tamils. The strong international presence also included those from Turkey, Iraq, Iran and Greece. At the end point rally in Trafalgar Square, many joined the &lsquo;Occupation Against the Cuts&rsquo; camp, which has been setting up in the square weekly since 26th March.  </p>  
   <p>  
  	There was a smattering of direct action in Birmingham following a city centre protest of a few hundred protesters. After the march, a breakaway Fortnum &amp; Mason Solidarity Bloc expressed their support for the 146 UK-Uncut activists nicked at the M26, by targeting a Topshop in shopping centre the Bullring. After blocking the shop entrance, heavy-handed security gorillas hauled the activists away. Later in the night branches of HSBC, Nat-West and Lloyds TSB were sabotaged with black spray paint rendering cashpoints inoperable. The Lloyds TSB&rsquo;s front doors were also D-locked shut.  </p>  
   <p>  
  	Things also got a little tetchier down in sunny Brighton. &ldquo;Protesters hold city to ransom&rdquo; screamed local toilet-roll-daily the Argus, the day after the 200 strong demo. However it was less of an armed kidnapping with threats to send back bits of the Pavillion one by one until demands were met, and more a gathering at the sea front and an escorted walkabout.  </p>  
   <p>  
  	The mood was one of celebration and mischief, with most attendees in face-paint, several clowns and a big dice to decide which route the march would take. Despite the fluffy feel, ridiculous numbers of coppers on ma-hoof-sive horses wasted no time in strapping on the riot gear and kettled the group almost immediately. The kettle was then led on a tour of the sights, with anyone who tried to deviate from the coppers&rsquo; itinerary getting a shove and a baton back into the kettle. The demonstrators eventually broke away and had the usual game of &lsquo;run around until the plod gets knackered&rsquo;. Eight were arrested during the day with four now facing charges.  </p>  
   <p>  
  	Newcastle, Leeds, Manchester, Bradford, Portsmouth, Cambridge and Oxford also saw peaceful marches through their high streets.  </p>  
   <p>  
  	A lively anarchist bloc together with union groups reclaimed Newcastle&rsquo;s town centre in support of workers rights, the peace movement, anti-racism and anti-cuts. The march ended in a rally with speeches, bookstalls and dancing.  </p>  
   <p>  
  	Oxford&rsquo;s action was led by Oxford Save Our Services and had a strong union presence marching against public sector cuts. There was a similar demographic in Cambridge where hundreds came out into the sunshine to protest against the millions of public funds the county council has been slashing from their budget. The day finished with an hour of music and poetry in Market Square. Both events had a low police presence and no arrests were made.  </p>  
   <p>  
  	Across the pond, activists in Spain, Germany, France, Italy, Greece, Ukraine and Bulgaria, marched in their cities with perceptibly more anarchist representation. Turkey, Iraq, Egypt, South Korea, Taiwan, Hong Kong and the Philippines also saw their fair share of action.  </p>  
   <p>  
  	In Berlin, thousands took to the streets and clashed several times with cops throughout the day. Protesters attacked banks and shops, throwing stones and smashing windows, whilst others did a banner drop from a nearby building. The banner proclaimed the word &lsquo;yalla&rsquo; (let&rsquo;s go!) in Arabic script, and one of the key messages of the day was solidarity with the uprisings in the Arab world. Police eventually turned on the march with water cannons and broke up the groups just after dark. There were reports of &lsquo;several&rsquo; arrests.  </p>  
   <p>  
  	In other European locations, most marches were less well attended than last year, despite the full force of continent wide public sector cuts being felt. Spain, France, Italy, Greece, Bulgaria and Ukraine had actions taking place in various cities with numbers going into the thousands. The day&rsquo;s events passed off relatively peacefully, despite eggs and wet paper towels being the weapon of choice for the Italian demonstrators and minor scuffles kicking off in Athens in the evening.  </p>  
   <p>  
  	Heading south, around 200,000 protesters gathered in Taksim Square, Istanbul for the second year running. Until last year, May Day rallies had been banned in the square since 1980 after gunshots triggered a stampede at a union gathering.  </p>  
   <p>  
  	Workers in East Asia rallied to protest over the widening gap between rich and poor, and to demand more protection for those on the lowest wages. Seoul saw 50,000 people march, with numbers topping 2,000 in Tawain, 3,000 in the Phillipines and 5,000 in Hong Kong. <br />  
  	In Baghdad, demonstrators mainly from the Iraqi Communist Party marched for equal labour rights for women and in Egypt, Tahrir Square played hosts to crowd protesters once again, with the walls graffitted around the square: &lsquo;Enjoy the Revolution&rsquo;. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1208), 139); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1208);

				if (SHOWMESSAGES)	{

						addMessage(1208);
						echo getMessages(1208);
						echo showAddMessage(1208);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


