<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 774 - 3rd June 2011 - Going to Hell-as</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, asylum seekers, refugees, calais, no borders" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 774 Articles: </b>
<b>
<p><a href="../archive/news7741.php">Going To Hell-as</a></p>
</b>

<p><a href="../archive/news7742.php">Hinkley: Gone Frission</a></p>

<p><a href="../archive/news7743.php">Not Playing Workfare</a></p>

<p><a href="../archive/news7744.php">Basildon Brush Off</a></p>

<p><a href="../archive/news7745.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7746.php">Hasta La Democracia</a></p>

<p><a href="../archive/news7747.php">Smooth Operators</a></p>

<p><a href="../archive/news7748.php">Gaza Freedom Flotilla Ii</a></p>

<p><a href="../archive/news7749.php">Ukba-rmy</a></p>

<p><a href="../archive/news77410.php">Schnews In Graphic Detail Out Now</a></p>

<p><a href="../archive/news77411.php">Gm: Sooner Or Tater</a></p>

<p><a href="../archive/news77412.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/774-poundland-lg.jpg" target="_blank">
													<img src="../images/774-poundland-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 3rd June 2011 | Issue 774</b></p>

<p><a href="news774.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GOING TO HELL-AS</h3>

<p>
<p>  
  	<strong>MIGRANTS IN GREECE CAUGHT BETWEEN REPRESSIVE STATE AND RACISM</strong>  </p>  
   <p>  
  	Over a thousand migrants have been arrested and made homeless over the last two months after the Greek government launched a sweeping crackdown in the port city of Igoumenitsa.&nbsp; &nbsp;  </p>  
   <p>  
  	Last month the Greek government announced the scorched earth policy at Igoumenitsa alongside plans to begin the construction of 14 new detention and deportation centres. Since then police have evicted squats and razed camps in an increasingly violent campaign that has left many migrants badly beaten.  </p>  
   <p>  
  	Igoumenitsa is the second largest departure point for migrants escaping from Greece for Italy and up to1000 migrants live there at any time, including refugees from many of the world&rsquo;s warzones. The migrants live in squalid conditions, often in makeshift huts without water, electricity and sanitation, in camps in the mountains and forests near the port. Poverty is rife and many have to dig through bins to find enough to eat.  </p>  
   <p>  
  	The raids have been constant as migrants are chased out of one temporary home after another. In the latest assault, police raided a camp in a forest near the harbour yesterday (9th), arresting 50 migrants and destroying around 250 huts and tents.  </p>  
   <p>  
  	The migrants are also facing an increasingly hostile reaction from city residents. A growing movement protesting the presence of the migrants has been joined by organised fascists and there have been several demonstrations with residents blocking the port. The demos have seen confrontations between the residents, fascists and the migrants. At the start of May, the third such protest - supposedly &ldquo;For a more human city&rdquo; - ended in protesters screaming &ldquo;Burn them! Kill them!&rdquo; followed by skirmishes between a handful of fascists and a group of migrants.  </p>  
   <p>  
  	The attacks on the migrants at Igoumenitsa come at a time of mounting racism around the country. While last month saw anti-migrant pogroms tear through Athens, where around 17% of the population are immigrants, other hotspots such as the western port of Patra have also seen anti-migrant protests.  </p>  
   <p>  
  	Unlike in Calais, there is no established, constant activist presence in Igoumenitsa. Activists are calling out for anyone that can to get down there to observe and document police violence as well as provide desperately needed support and solidarity. Contact <a href="mailto:solidarity_igoumenitsa@yahoo.com&nbsp;">solidarity_igoumenitsa@yahoo.com&nbsp;</a>  </p>  
   <p>  
  	<strong>* The situation in Greece</strong> will all sound chillingly familiar to those migrants still holed up in Calais. The CRS, alongside an increasingly active PAF (border police) have been maintaining their ceaseless harassment of migrants and increasingly hostile behaviour towards Calais solidarity activists. They are also continuing the piece by piece sealing off and demolition of Africa House, home to many of Calais&rsquo; African migrants.  </p>  
   <p>  
  	Towards the end of last month, around 150 migrants and activists marched through Calais in protest against their treatment. The demo was part of a French national day of action against racist immigration policies called by &ldquo;D&rsquo;ailleurs nous sommes d&rsquo;ici&rdquo;, a collective of people involved in solidarity work with migrants in the town, and supported by No Borders Calais as well as trade union members (SUD and CGT), Lille anarchist group, leftist political party NPA, and others.  </p>  
   <p>  
  	In Calais too, activists are thin on the ground and more support is needed.  </p>  
   <p>  
  	* See <a href="http://www.calaismigrantsolidarity.wordpress.com" target="_blank">www.calaismigrantsolidarity.wordpress.com</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1235), 143); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1235);

				if (SHOWMESSAGES)	{

						addMessage(1235);
						echo getMessages(1235);
						echo showAddMessage(1235);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


