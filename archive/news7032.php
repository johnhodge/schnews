<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 703 - 11th December 2009 - Not Waving But Frowning</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, london, wave" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 703 Articles: </b>
<p><a href="../archive/news7031.php">Once You Cop You Just Can't Stop</a></p>

<b>
<p><a href="../archive/news7032.php">Not Waving But Frowning</a></p>
</b>

<p><a href="../archive/news7033.php">Inside Schnews</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th December 2009 | Issue 703</b></p>

<p><a href="news703.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOT WAVING BUT FROWNING</h3>

<p>
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/703-climate-wave-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/703-climate-wave-sm.jpg"  alt='Climate Wave 2009 / Climate Wave 2029'  /></a></td></tr></table><strong>A SchNEWS CORRESPONDENT REPORTS BACK FROM THE CLIMATE&nbsp; WAVE... <br />  <br /> </strong>This year&rsquo;s national climate march. tagged the 'Wave', started at Hyde Park as more of a ripple. The usual sorry bunch of mealy-mouthed MPs held forth - Michael &lsquo;six houses&rsquo; Meacher, Simon Hughes &ndash; and so it fell to a Bolivian speaker and merry pranksters Seize the Day to try to add some urgency to proceedings. Not that you&rsquo;d have noticed, this looked more like a mass Unison meeting &ndash; the rumoured black bloc having packed up and gone home if they&rsquo;d bothered to turn up at all. <br />  <br /> Numbers were down on last year&rsquo;s opening when the US embassy was given some stick. This time it all went multi-media on our ass when we arrived in Grosvenor Sq, although at least by then crowds had built (to around 50,000). Two or three giant screens set up at various points were a complete and utter waste. No one stopped to look at them, in fact more interest was generated by the Hare Krishnas, who turned out with free hot grub, and a four-piece band which included a long-haired fat bloke on drums being drawn along on a bicycle-towed stage. SchNEWS can exclusively reveal that Jim Morrison is in fact alive and well and living in a squat in Tooting from which he exits now and then to spread some loving kindness. <br />  <br /> As the march veered into Whitehall anyone hoping for a bit of argy-bargy outside Downing Street would have been sorely disappointed. Decibel counts may have risen as we came into Parliament Square - but they needed to in order to drown out the chanting of young Labour Party bucks shouting inane support for Brown. Who he? Yes the same greedy twat who for the past decade has lorded it over some of the most conspicuous consumption known to man. How the climate march has changed in the space of just four or five years. <br />  <br /> This SchNEWSer turned in despair to the weather-beaten figure of Brian Haw and slipped him a copy of last week&rsquo;s issue, as others donated tenners &ndash; well, come on we are skint. At this point some kind of blue Chinese dragon thing was being waved around up by Big Ben. <br />  <br /> Fortunately just then &ldquo;The Cop Out Come Down&rdquo; text from Climate Camp came through. We legged it up to the Trafalgar Sq set up &ndash; but  instead of the battering handed out at Bishopsgate last April by the Met the only hindrance this time came from the driving rain. <br />   <br /> The  camp was up and running by early evening with all-new-friendly-face of the Met Julia Pendry in smiley attendance. Keep up to date with camp actions &ndash; originally the plan was to stay for 48 hrs but now the campers are pushing to stay for the full length of the climate talks in Copenhagen. <br />  <br /> So far a group of sulky Santas have disrupted the departures lounge at London City Airport and the entrance to the European Climate Exchange in Bishopsgate has been blockaded. A farewell meal was also organised for COP15-ers heading off to Denmark in the hope of more demonstrative demos in the week to come... <br />  <br /> * For more about those out there still surfin that wave, see <a href="http://www.climatecamp.org.uk/actions/copenhagen-2009/cop15-out" target="_blank">www.climatecamp.org.uk/actions/copenhagen-2009/cop15-out</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=703">climate change</a>, <a href="../keywordSearch/?keyword=london&source=703">london</a>, <span style='color:#777777; font-size:10px'>wave</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(606);
			
				if (SHOWMESSAGES)	{
				
						addMessage(606);
						echo getMessages(606);
						echo showAddMessage(606);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>