<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 742 - 8th October 2010 - Itt's Hammertime</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, sussex police, brighton, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 742 Articles: </b>
<p><a href="../archive/news7421.php">Frontier Law</a></p>

<p><a href="../archive/news7422.php">Red Leicester</a></p>

<p><a href="../archive/news7423.php">It's In Their Make-up</a></p>

<p><a href="../archive/news7424.php">Cut It Out!</a></p>

<p><a href="../archive/news7425.php">Fur Crying Out Loud</a></p>

<p><a href="../archive/news7426.php">Dam Squatters</a></p>

<b>
<p><a href="../archive/news7427.php">Itt's Hammertime</a></p>
</b>

<p><a href="../archive/news7428.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 8th October 2010 | Issue 742</b></p>

<p><a href="news742.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ITT'S HAMMERTIME</h3>

<p>
<p>
	Smash EDO are holding a mass demo at EDO/ITT in Brighton on Wednesday 13th October. Anti arms trade campaigners plan to lay siege and shut them down for the day. This will be the first mass demonstration since the EDO Decommissioners, who smashed up the Brighton death dealers during Israel&rsquo;s massacre in Gaza, were acquitted by a unanimous jury verdict (See <a href='../archive/news729.htm'>SchNEWS 729</a>). </p>
<p>
	<strong>Info for the Day:</strong> Meet at the Wild Park Cafe at 10am, or if you&rsquo;re coming a long way a convergence space will open on the 12th. There will be a meeting about the demo at 7pm at the Cowley Club, an anarchist theme pub, 12 London Rd on the 12th where you can find out more about the demo and convergence space locations. </p>
<p>
	Previous mobilisations at EDO have seen paint pelted at EDO from Wild Park, windows put in after a mass invasion of EDO&rsquo;s property, MD Paul Hills&rsquo; car redecorated and thousands of activists targeting companies investing in EDO in Brighton&rsquo;s city centre. Smash EDO press spokesperson Andrew Beckett said, &ldquo;<em>this time the message is clear &ndash; our aim is to close down the factory for the day. The police cannot stop us from surrounding EDO/ITT</em>&rdquo;. </p>
<p>
	Sussex Police, gearing up for a charm offensive while dusting off their teloscopic koshes and ordering in extra pepper spray, have recorded a YouTube appeal calling for Smash EDO to negotiate with them. Smash EDO have always maintained a non-negotiation stance on the grounds that &ldquo;<em>dissent is meaningless if it can only occur with state approval</em>&rdquo;. </p>
<p>
	Despite the PR veneer, police attempts to clamp down on the campaign continue. This week a five year restraining order was slapped on Smash EDO campaigner Elijah Smith. One of the EDO decommissioners (see <a href='../archive/news729.htm'>SchNEWS 729</a>), he was convicted of witness intimidation after making a political outburst at Brighton Magistrates&rsquo; Court while he was on trial for another EDO related offence. Smith had been on remand for the previous six months and was understandably pissed off. The order will impose an exclusion zone, preventing him from continuing to protest against EDO/ITT. </p>
<p>
	The judge used a new amendment to the Protection from Harassment Act (PHA) 1997, a law originally used to protect people from stalkers, but now frequently used against protesters. </p>
<p>
	Restraining orders, which can now even be imposed on people who are found not guilty, may turn out to be a replacement for the increasingly out of fashion ASBOs. Previous attempts by EDO to slap injunctions on anti arms trade activists resulted in a year long high court battle ending in failure (see <a href='../archive/news492.htm'>SchNEWS 492</a>). </p>
<p>
	* See <a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank">www.smashedo.org.uk/hammertime.htm</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(970), 111); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(970);

				if (SHOWMESSAGES)	{

						addMessage(970);
						echo getMessages(970);
						echo showAddMessage(970);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


