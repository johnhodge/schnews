<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 689 - 4th September 2009 - Herculean Task</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, no borders, lesvos, greece, refugees, asylum seekers, direct action, hunger strike, frontex, fortress europe" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.rts.gn.apc.org/"><img 
						src="../images_main/rtf-05-banner.jpg"
						alt="Reclaim the Future"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 689 Articles: </b>
<p><a href="../archive/news6891.php">Common People?</a></p>

<p><a href="../archive/news6892.php">Factory Finish</a></p>

<b>
<p><a href="../archive/news6893.php">Herculean Task</a></p>
</b>

<p><a href="../archive/news6894.php">Circus Freaks</a></p>

<p><a href="../archive/news6895.php">Hill Fought</a></p>

<p><a href="../archive/news6896.php">Inside Schnews</a></p>

<p><a href="../archive/news6897.php">Dsei-ing With Death</a></p>

<p><a href="../archive/news6898.php">Brum Punch</a></p>

<p><a href="../archive/news6899.php">Arm-a-geddon Myself Some Of That</a></p>

<p><a href="../archive/news68910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th September 2009 | Issue 689</b></p>

<p><a href="news689.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HERCULEAN TASK</h3>

<p>
While we&rsquo;ve lately had our attention on the immigration hell close to home in Calais (see <a href='../archive/news686.htm'>SchNEWS 686</a>), the imposition of fortress Europe has led to the development of similar, and even worse, situations across the continent. Last week saw a <strong>No Borders</strong> camp descend on the Lesvos in an attempt to raise awareness of the plight of people detained in the Greeks island&rsquo;s refugee detention facility. <br />   <br /> Activists from all over Europe converged on the picturesque Aegean island, hoping to help clean out Europe&rsquo;s dirty stables, Camp Pagani. In a converted warehouse originally designed to hold 250, over 1,200 people (200 or so of them being unaccompanied children) have been rounded up and herded in to be kept like cattle - all for daring to seek a better life. <br />    <br /> Migrants attempting to enter Greece have been confronted with the armed pit-bulls of the shadowy Europe-wide border agency, the nattily corporately-named Frontex (see <a href='../archive/news676.htm'>SchNEWS 676</a>). Patrols continue to sweep the surrounding coast looking for any new boats full of desperate people, even as the space to imprison them is at bursting point. <br /> Earlier in August, 160 incarcerated parentless-children went on hunger strike to demand their immediate freedom. All of them, some under 10, were detained in just one room, sharing one toilet and with many sleeping on the floor (video footage available at <a href="http://www.youtube.com/watch?v=lP2yT6EjBXo" target="_blank">www.youtube.com/watch?v=lP2yT6EjBXo</a>). These inhuman conditions had been withstood for months at a time, not to mention that detention of minors is of course illegal under Greek and International law. <br />   <br /> The children were joined by over 900 other detainees demanding release and tension was rising as No Borders hit town. Their camp was welcomed by locals, especially the Bineio Squat, which provided a media centre and a meeting and recreational space. It was used as a platform to launch over a week&rsquo;s worth of actions, including a rooftop occupation and a harbour boating critical mass which managed to drive a Frontex ship back out to the open sea. <br />   <br /> Renewed protests a few days after the arrival of No Borders activists may have contributed to the release of some 250 detainees (told to get out of Fortress Europe within 30 days or else) shortly after &ndash; although it was no time for over celebrating as conditions remain dire and a new supply of inmates is never far from the gates. <br />  <br /> The camp wound up earlier this week but, with 47 more minors going on hunger strike last Wednesday (26th), 4 also refusing water and with no medical support or hope in sight, it remains crucial to keep up the pressure on the authorities. No Borders have announced that they will return in 2010 and every year till Pagani is closed for good. <br />   <br /> * See <a href="http://lesvos09.antira.info" target="_blank">http://lesvos09.antira.info</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=689">asylum seekers</a>, <a href="../keywordSearch/?keyword=direct+action&source=689">direct action</a>, <span style='color:#777777; font-size:10px'>fortress europe</span>, <span style='color:#777777; font-size:10px'>frontex</span>, <a href="../keywordSearch/?keyword=greece&source=689">greece</a>, <a href="../keywordSearch/?keyword=hunger+strike&source=689">hunger strike</a>, <span style='color:#777777; font-size:10px'>lesvos</span>, <a href="../keywordSearch/?keyword=no+borders&source=689">no borders</a>, <a href="../keywordSearch/?keyword=refugees&source=689">refugees</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(476);
			
				if (SHOWMESSAGES)	{
				
						addMessage(476);
						echo getMessages(476);
						echo showAddMessage(476);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>