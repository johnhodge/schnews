<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 793 - 21st October 2011 - LSX: Occupy Eyed</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 793 Articles: </b>
<p><a href="../archive/news7931.php">Dale Farm Evicted</a></p>

<p><a href="../archive/news7932.php">Crap Alchemist Of The Week</a></p>

<p><a href="../archive/news7933.php">Occupy Everywhere</a></p>

<p><a href="../archive/news7934.php">Uganda: Dropping A Kalanga</a></p>

<b>
<p><a href="../archive/news7935.php">Lsx: Occupy Eyed</a></p>
</b>

<p><a href="../archive/news7936.php">Brought To The Book</a></p>

<p><a href="../archive/news7937.php">Blackpool Rocked</a></p>

<p><a href="../archive/news7938.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st October 2011 | Issue 793</b></p>

<p><a href="news793.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LSX: OCCUPY EYED</h3>

<p>
<p>  
  	<strong>Just some of Occupy LSX’s demands:</strong> “The current system is unsustainable. It is undemocratic and unjust. We need alternatives; this is where we work towards them. We refuse to pay for the banks’ crisis. We do not accept the cuts as either necessary or inevitable. We demand an end to global tax injustice and our democracy representing corporations instead of the people.   </p>  
   <p>  
  	We support the strike on 30 November and the student action on 9 November, and actions to defend our health services, welfare, education and employment, and to stop wars and arms dealing. We want structural change towards authentic global equality. The world’s resources must go towards caring for people and the planet, not the military, corporate profits or the rich.   </p>  
   <p>  
  	<strong>Join the democracy in action:</strong>   </p>  
   <p>  
  	<u><strong>London</strong></u> <br />  
  	Occupy the Stock Exchange, St Paul’s Cathedral, EC4M 8AD. <br />  
  	Web: <a href="http://occupylondon.org.uk" target="_blank">http://occupylondon.org.uk</a> <br />  
  	Twitter:@OccupyLSX   </p>  
   <p>  
  	<u><strong>Nottingham </strong></u> <br />  
  	Old Market Square. <br />  
  	Twitter: @OccupyNotts <br />  
  	Email: <a href="mailto:occupynottingham@hotmail.co.uk">occupynottingham@hotmail.co.uk</a> <br />  
  <a href="http://	www.facebook.com/pages/Occupy-Nottingham/176572135759014?sk=wall" target="_blank">	www.facebook.com/pages/Occupy-Nottingham/176572135759014?sk=wall</a>   </p>  
   <p>  
  	<u><strong>Bristol</strong></u> <br />  
  	College Green. <br />  
  	Twitter: @OccupyBristolUK  <br />  
  <a href="http://	www.bristol.indymedia.org.uk/article/706047 " target="_blank">	www.bristol.indymedia.org.uk/article/706047 </a> <br />  
  <a href="http://	www.facebook.com/pages/Occupy-Bristol/156092691150973?sk=info" target="_blank">	www.facebook.com/pages/Occupy-Bristol/156092691150973?sk=info</a>   </p>  
   <p>  
  	<u><strong>Manchester</strong></u> <br />  
  	Peace Gardens, near St.Peters Sq. <br />  
  	Twitter: @OccupyMCR <br />  
  <a href="http://	www.occupymanchester.org" target="_blank">	www.occupymanchester.org</a> <br />  
  <a href="http://	www.facebook.com/OccupyMCR" target="_blank">	www.facebook.com/OccupyMCR</a>   </p>  
   <p>  
  	<u><strong>Norwich</strong></u> <br />  
  	Haymarket NR2 1QD <br />  
  	Twitter: @OccupyNorwich <br />  
  <a href="	http://occupynorwich.tumblr.com " target="_blank">	http://occupynorwich.tumblr.com </a> <br />  
  <a href="http://	www.facebook.com/pages/Occupy-Norwich/291233767570027" target="_blank">	www.facebook.com/pages/Occupy-Norwich/291233767570027</a>   </p>  
   <p>  
  	<u><strong>Newcastle</strong></u> <br />  
  	Twitter: @OccupyNewcastle <br />  
  <a href="	http://occupynewcastle.org" target="_blank">	http://occupynewcastle.org</a>   </p>  
   <p>  
  	<u><strong>Edinburgh</strong></u> <br />  
  	Twitter: @OccupyEdinburgh  <br />  
  <a href="http://	www.facebook.com/Occupyedinburgh" target="_blank">	www.facebook.com/Occupyedinburgh</a>   </p>  
   <p>  
  	<u><strong>Glasgow</strong></u> <br />  
  	Twitter: #occupyglasgow  <br />  
  <a href="http://	www.facebook.com/occupyglasgow" target="_blank">	www.facebook.com/occupyglasgow</a> <br />  
  <a href="http://	www.indymediascotland.org/node/25189" target="_blank">	www.indymediascotland.org/node/25189</a>   </p>  
   <p>  
  	<u><strong>Birmingham</strong></u> <br />  
  	Victoria Square (UK apparently Birmingham, Alabama is also occupied!) <br />  
  	Twitter: @OccupyBhamUK <br />  
  <a href="http://	www.facebook.com/pages/Occupy-Birmingham-UK/154845231274653?sk=wall" target="_blank">	www.facebook.com/pages/Occupy-Birmingham-UK/154845231274653?sk=wall</a>   </p>  
   <p>  
  	* Also check out <a href="http://www.occupybritain.co.uk" target="_blank">www.occupybritain.co.uk</a> and <a href="http://www.occupytogether.org" target="_blank">http://www.occupytogether.org</a> - two websites that are trying to connect the movements around Britain and around the world.   </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1398), 162); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1398);

				if (SHOWMESSAGES)	{

						addMessage(1398);
						echo getMessages(1398);
						echo showAddMessage(1398);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


