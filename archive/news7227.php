<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 722 - 14th May 2010 - Bounty Bar-stards</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, calais, immigration, france, refugees, no borders" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 722 Articles: </b>
<p><a href="../archive/news7221.php">Squaring Up To 'em</a></p>

<p><a href="../archive/news7222.php">Not A Clegg To Stand On</a></p>

<p><a href="../archive/news7223.php">Decommissioners Trial Delayed</a></p>

<p><a href="../archive/news7224.php">Glade Rags</a></p>

<p><a href="../archive/news7225.php">Sweaty Palms At Unilever</a></p>

<p><a href="../archive/news7226.php">Raising Arizona</a></p>

<b>
<p><a href="../archive/news7227.php">Bounty Bar-stards</a></p>
</b>

<p><a href="../archive/news7228.php">Lab- Lib Pact</a></p>

<p><a href="../archive/news7229.php">Crude Awakening</a></p>

<p><a href="../archive/news72210.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th May 2010 | Issue 722</b></p>

<p><a href="news722.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BOUNTY BAR-STARDS</h3>

<p>
On Tuesday 11th, France&rsquo;s CRS riot squad raided &lsquo;Africa House&rsquo;, the squat in Calais of African migrants. Six riot vans full of police and two unmarked &lsquo;arrest&rsquo; vans arrived to round people up for the crime of having no papers. They spent an hour combing the building and eventually left with 23 people. Most people were only held for a few hours before being made to make the long walk from the Coquelles detention centre back into town.  <br />  <br /> A day later the police rounded up five Hazara Afghans who are camping out in the shrubbery near the old hover port and also five Palestinians trying to make it to the UK in a truck. The CRS units receive a 1000 euro bonus at the end of a tour of duty if they exceed their arrest quota of migrants. Policing of migrants has been transformed into bounty-hunting. <br />  <br /> Police raids on &lsquo;jungles&rsquo; are an almost daily occurrence and are part of a concerted campaign of repression by the French state to make Calais a migrant free zone. In April 2009 the French minister of immigration, Eric Besson, vowed to drive all migrants out of Calais. In September of that year the process began with the smashing of the large Pashtun &lsquo;jungle&rsquo; and the arrest of 278 people. Since then all pre-existing squats and jungles have been evicted with the exception of Africa House, which is set for demolition in the next two months.  <br />  <br /> On Saturday 8th, as Calais celebrated the defeat of the Nazis, No Borders activists and migrants organised a march through Calais to highlight their situation. Two No Borders activists attempted to display photos of police brutality at the official commemoration at the town hall but were swiftly dragged away by the police along with two observers and detained until the rally ended.  <br /> <b> <br /> * No Borders Transnational Day Of Action For Freedom Of Movement</b>: Paris, May 15th. Meet 2pm at Jaures Train Station. <a href="http://calaismigrantsolidarity.wordpress.com" target="_blank">http://calaismigrantsolidarity.wordpress.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=calais&source=722">calais</a>, <a href="../keywordSearch/?keyword=france&source=722">france</a>, <a href="../keywordSearch/?keyword=immigration&source=722">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=722">no borders</a>, <a href="../keywordSearch/?keyword=refugees&source=722">refugees</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(779);

				if (SHOWMESSAGES)	{

						addMessage(779);
						echo getMessages(779);
						echo showAddMessage(779);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


