<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 674 - 1st May 2009 - Schnews In Brief</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, schnews in brief, bae, no borders, shell to sea, david venegas, bank of england, strangers into citizens, direct action, mayday, squatting" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 674 Articles: </b>
<p><a href="../archive/news6741.php">May The Fourth Be With You</a></p>

<p><a href="../archive/news6742.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6743.php">Teenage Kicks</a></p>

<p><a href="../archive/news6744.php">End Of The Road</a></p>

<p><a href="../archive/news6745.php">Gorky Spark</a></p>

<p><a href="../archive/news6746.php">Mexican Pay-off</a></p>

<p><a href="../archive/news6747.php">Troupes Out</a></p>

<b>
<p><a href="../archive/news6748.php">Schnews In Brief</a></p>
</b>

<p><a href="../archive/news6749.php">Running The Gauntlet</a></p>

<p><a href="../archive/news67410.php">Well Hung</a></p>

<p><a href="../archive/news67411.php">Fish Out Of Water</a></p>

<p><a href="../archive/news67412.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st May 2009 | Issue 674</b></p>

<p><a href="news674.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCHNEWS IN BRIEF</h3>

<p>
* Arms manufacturer <strong>BAE</strong> was targeted on Monday (27th) in a day of action organised by No Borders. While No Borders London activists picketed BAE&rsquo;s London HQ, members of Break the Siege! blockaded the Middleton factory in Manchester. <br /> <a href="http://www.nobordersmanchester.blogspot.com" target="_blank">www.nobordersmanchester.blogspot.com</a> <br />  <br /> * In <strong>Ireland</strong>, protesters opposed to the <strong>Shell Corrib Gas Project</strong> resumed actions against Shell&rsquo;s Glengrad site last Sunday (26th), refusing to be intimidated following last week&rsquo;s severe beating of protester Willie Corduff at the hands of Shell hired thugs (see <a href='../archive/news673.htm'>SchNEWS 673</a>). <a href="http://www.shelltosea.com" target="_blank">www.shelltosea.com</a>  <br />  <br /> * After successfully resisting two evictions in one day last week (See <a href='../archive/news673.htm'>SchNEWS 673</a>), <strong>Birmingham</strong> squatters lost the <strong>Beechwood Hotel</strong> in the early hours of last Friday morning as bailiffs piled in. See <a href="http://justicenotcrisis.wordpress.com" target="_blank">http://justicenotcrisis.wordpress.com</a> <br />  <br /> * Following on from <a href='../archive/news672.htm'>SchNEWS 672</a>, Oaxacan activist <strong>David Venegas</strong> has been declared innocent in a trial after he was fitted up with drugs. He had already beaten charges ranging from sedition to arson, and was declared innocent of all charges with the judge accepting his defence that the Oaxacan police planted drugs on him. <a href="http://narcosphere.narconews.com" target="_blank">http://narcosphere.narconews.com</a> <br />  <br /> * <strong>May 1st - Convicts Party at The Bank of England</strong> - We are a nation who are guilty until proven innocent. Turn yourself in at The Bank Of England, dressed in your best stripes. Sneak some contraband (Sound Systems, Moonshine, Cake, Kettles, Party Games) past the guard... Meet 5pm, Bank of England, London <a href="http://www.spacehijackers.org" target="_blank">www.spacehijackers.org</a> <br />  <br /> * <strong>May 4th - Strangers into Citizens</strong>, a campaign supporting the integration of immigrants into UK society, is holding a London National Rally. The campaign calls for amnesties for migrants who have been in the UK for a long periods of time, many who are escaping danger and can&rsquo;t return to their home countries. 11.30am at Tothill Street, marching to Trafalgar Square. With Asian Dub Foundation live. <a href="http://www.strangersintocitizens.org.uk" target="_blank">www.strangersintocitizens.org.uk</a> <br />  <br /> * For the full <strong>PARTY &amp; PROTEST</strong> listings updated weekly see <a href="../pap" target="_blank">www.schnews.org.uk/pap</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bae&source=674">bae</a>, <a href="../keywordSearch/?keyword=bank+of+england&source=674">bank of england</a>, <span style='color:#777777; font-size:10px'>david venegas</span>, <a href="../keywordSearch/?keyword=direct+action&source=674">direct action</a>, <span style='color:#777777; font-size:10px'>mayday</span>, <a href="../keywordSearch/?keyword=no+borders&source=674">no borders</a>, <span style='color:#777777; font-size:10px'>schnews in brief</span>, <a href="../keywordSearch/?keyword=shell+to+sea&source=674">shell to sea</a>, <a href="../keywordSearch/?keyword=squatting&source=674">squatting</a>, <span style='color:#777777; font-size:10px'>strangers into citizens</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(312);
			
				if (SHOWMESSAGES)	{
				
						addMessage(312);
						echo getMessages(312);
						echo showAddMessage(312);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>