<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 704 - 20th December 2009 - Not A Hopenhagen</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, environmentalism, direct action, copenhagen, climate change" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<div class="bannerGraphic">
			<a href="http://www.smashedo.org.uk/remember-gaza.htm"><img 
						src="../images_main/remember-gaza-banner.png"
						alt="Remember Gaza"
						width="465" 
						height="90" 
						border="0" 
			/></a>
			</div>


</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<a href='../archive/index.htm'>
			<div class='navBar_item'>
				Back Issues
			</div>
		</a>
		<a href='http://www.schnews.org.uk/schmovies/index.php'>
			<div class='navBar_item'>
				SchMOVIES
			</div>
		</a>
		<a href='http://www.schnews.org.uk/features'>
			<div class='navBar_item'>
				Feature Articles
			</div>
		</a>
		<a href='http://www.schnews.org.uk/links/index.php'>
			<div class='navBar_item'>
				Contacts and Links
			</div>
		</a>
		<a href='http://www.schnews.org.uk/diyguide/index.htm'>
			<div class='navBar_item'>
				DIY Guide
			</div>
		</a>
		<a href='http://www.schnews.org.uk/monopresist/index.htm'>
			<div class='navBar_item'>
				Archive
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/about.php'>
			<div class='navBar_item'>
				About Us
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/subscribe.php'>
			<div class='navBar_item'>
				Subscribe
			</div>
		</a>

			
		<!--	SCROOGLE SEARCh BAR 	-->
		<div class='search_bar'>		<form action="https://duckduckgo.com/" method="get"  target="_blank"> 
		
		 						
				<input type="search" placeholder="Search" name="q" maxlength="300" > 
				&nbsp;&nbsp;
				<input type="hidden" name="sites" value="schnews.org"> 
				    	<input type="hidden" value="1" name="kh">
    					<input type="hidden" value="1" name="kn">
    					<input type="hidden" value="1" name="kac">
						<input type="hidden" value="w" name="kw">
						
			<!--<input name="submit2" type="submit" value="Go">-->
			<!--<button type="submit">Search</button>-->
				
		</form>
		</div>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 704 Articles: </b>
<b>
<p><a href="../archive/news7041.php">Not A Hopenhagen</a></p>
</b>

<p><a href="../archive/news7042.php">Tar Very Much</a></p>

<p><a href="../archive/news7043.php">No Oil For War</a></p>

<p><a href="../archive/news7044.php">Nazi Pieces Of Work</a></p>

<p><a href="../archive/news7045.php">Back In Feline-ment</a></p>

<p><a href="../archive/news7046.php">The Livni Daylights</a></p>

<p><a href="../archive/news7047.php">Ahava Go Heroes</a></p>

<p><a href="../archive/news7048.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">
	<img src="../images_main/description-new.png" width="643" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" />
</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/704-earth-accord-lg.jpg" target="_blank">
													<img src="../images/704-earth-accord-sm.jpg" alt="As Earth hangs by accord..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Sunday 20th December 2009 | Issue 704</b></p>

<p><a href="news704.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NOT A HOPENHAGEN</h3>

<p>
<strong><font size="3">AS WORLD LEADERS DITHER INSIDE, SchNEWS LOOKS TO THE THOUSANDS OF DISSENTERS OUTSIDE</font> <br />  <br /> In the end there was no deal on the table in Copenhagen (see <a href='../archive/news700.htm'>SchNEWS 700</a>) and COP 15 was the big global cop-out we expected.</strong> &nbsp; <br />   <br /> The rich countries, the biggest polluters and the ones who have clawed their way to pre-eminence on the back of one hundred and fifty years of carbon emissions are simply not going to compromise enough. Contrast the rapidity with which the global financial system was rescued last year with the bureaucratic bog-down that happened in Denmark.  Capitalism must be maintained at all costs &ndash; life on earth is not so urgent.  <br />  <br /> Having no real deal suits the dominant powers very well -  they can afford to hang on until the poorer nations buckle (suggestions that the World Bank should administer the Climate Fund gives you an idea what sort of deals have been proposed). It&rsquo;s full-scale carbon trading and business as usual or nothing for the richest nations. Despite half-hearted last ditch attempts to salvage something to save media face, the volutary &lsquo;accord&rsquo; arm-twisted through at the last moment is virtually meaningless. <br />   <br /> So much for what was (or wasn&rsquo;t) going on inside the fortress-like Bella centre to the south of the city. If the delegates of countries like the Maldives (which will literally disappear if sea levels continue to rise) can&rsquo;t get themselves heard &ndash; what hope did the protests have? Accredited delegates at the main conference on Wednesday who attempted to leave and join the People&rsquo;s Assembly outside the gates got the same rough treatment as those already assembled there. <br />  <br /> The message from the authorities in Copenhagen was clear, there is no place for dissenting voices, however inept political attempts to save the world prove. For the climate activists who arrived in Copenhagen the story was one of stop-n-searches and mass arrests. New legislation passed by the Danish government allowed for preemptive arrests of anyone. Countering protest in the city was effectively given a police state mandate. <br />  <br /> Intentions became apparent with the arrests of around 750 people forming the anti-capitalist block in the middle of the main march. In a coordinated swoop (described by one arrestee as &lsquo;incredibly slick&rsquo;) police sealed off the roads with vehicles. They then charged the crowd with batons drawn before forcing protesters to sit in rows, hands cuffed behind their backs with cable ties. Some were there for hours sat in the road in sub-zero temperatures. <br />   <br /> Others were taken to a pre-prepared detention facility in the Valby area of the city. Here the police had built wire cages &ndash; a human containment facility.  One activist told SchNEWS, &ldquo;<em>They&rsquo;re around the size of a shipping containers, built of wire. Only 2 &frac12; metres high with bare concrete floors &ndash; roll mats to sleep on. They put ten or fifteen of us in each cage</em>.&rdquo; <br />  <br /> In cases where angry activists kicked off inside the cages &ndash; some pulling benches off the walls &ndash; police fought back with  pepper spray and batons. All in all not a pleasant experience of &lsquo;liberal&rsquo; Denmark to take home from the summit-hopping city break.   <br />  <br /> In fact pepper spray and batons were much in attendance all week &ndash; when not charging crowds or spraying those assembled too close to the conference venue gates, cops continued to arrest anyone they wanted left right and centre, taking whole groups en ma sse. There was a constant flow of reports from the twittersphere of rough treatment and baton-beatings from many of those detained. <br />     <br /> And, let loose to be as proactive as they liked, it was no surprise when on the morning of the 16th  police arrived at all the convergence centres for a mass stop and search.  All part of the intimidation service, along with complementary repeat van stop-n-searches for anyone unlucky enough to be driving. <br />   <br /> Wednesday&rsquo;s &lsquo;<strong>Hit the Production</strong>&rsquo; day of direct action at the dock was typical, with riot cops moving in to arrest 270 before anything had actually happened. <br />   <br /> While many of the arrestees were later released, a fair few were deported for minor public order offenses and others were bailed until 4th January. <br />   <br /> As the dust settles, it&rsquo;s clear that Copenhagen was no Seattle or Genoa.  Despite the respectable mobilisation of local and international activists,  they were no match for the scale of the lockdown, cops given carte blanche to allow world leaders to fail the world unhindered. One thing the big build up and hype does seem to have achieved is that the authorities were obviously extremely worried - and fear just breeds aggression on their part. <br />  <br /> But the protesters presence alone stole some of the media spotlight and reminded a global audience that when conventional political solutions fail, as they inevitably will on current course, there are other options. How long before the visible effects of climate change convince enough of the public to make the cause truly mass? The longer the politicians dither away the earth&rsquo;s future, the sooner that day comes. <br />       <br /> <font face="courier new">*If this conference has been notable for one thing other than pre-emptive repression, its the sheer amount of content produced by activist media. You could drown in it. A full round up of events is beyond the meagre space of SchNEWS - just some of the minute by minute timelines, reports, pictures, footage, a million twitters and links are all available at <a href="http://icop15.org" target="_blank">http://icop15.org</a>  - Go on, gorge yerself... <br />  <br /> ** List and links to reports of many other climate actions from around the world at <a href="http://www.indymedia.dk/articles/1928" target="_blank">www.indymedia.dk/articles/1928</a>  <br />  <br /> </font><strong><font size="3">TOP COP TIP OF THE WEEK</font></strong> <br />  <br /> It is very common for activists to get compensation from the police in Denmark. If you are arrested and released without charge you can apply and you can automatically receive compensation (and quite quickly, unlike in Britain). <br />  <br /> This does not apply for the preventative arrests they have been using, but if you think you have been unfairly treated you still stand a fair chance of receiving compensation. <br />  <br /> <strong>Contact:</strong> Copenhagen Legal Support: <br /> Retshj&aelig;lpen Rusk, Baggesensgade 6, basement &ndash; N&oslash;rrebro. Phone: 0045 28255320 <br /> E-mail: <a href="mailto:retshjaelprusk@hotmail.com">retshjaelprusk@hotmail.com</a> <br /> &nbsp; <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=704">climate change</a>, <a href="../keywordSearch/?keyword=copenhagen&source=704">copenhagen</a>, <a href="../keywordSearch/?keyword=direct+action&source=704">direct action</a>, <a href="../keywordSearch/?keyword=environmentalism&source=704">environmentalism</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(608);
			
				if (SHOWMESSAGES)	{
				
						addMessage(608);
						echo getMessages(608);
						echo showAddMessage(608);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<font face="Arial, Helvetica, sans-serif"> 
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p></font>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>
		
</div>








</div>




</body>
</html>


