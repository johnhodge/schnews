<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 708 - 5th February 2010 - Eyewitness Afghanistan</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, afghanistan, taliban, al-qaeda, war on terror" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<?php

				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";

			?>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 708 Articles: </b>
<b>
<p><a href="../archive/news7081.php">Eyewitness Afghanistan</a></p>
</b>

<p><a href="../archive/news7082.php">A Honduran Lobo-tomy</a></p>

<p><a href="../archive/news7083.php">Inside Schnews</a></p>

<p><a href="../archive/news7084.php">Stop 'n' Sue</a></p>

<p><a href="../archive/news7085.php">Voulez Vous Couche?</a></p>

<p><a href="../archive/news7086.php">Treading A Fine Line</a></p>

<p><a href="../archive/news7087.php">Diamonds Are For Never</a></p>

<p><a href="../archive/news7088.php">Over The Mainshill?  </a></p>

<p><a href="../archive/news7089.php">Blair Faced Cheek  </a></p>

<p><a href="../archive/news70810.php">And Finally...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/708-medyan-lg.jpg" target="_blank">
													<img src="../images/708-medyan-sm.jpg" alt="...and now it's over to Tom with the weather" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th February 2010 | Issue 708</b></p>

<p><a href="news708.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EYEWITNESS AFGHANISTAN</h3>

<p>
<p class="MsoNormal"><strong>FROM <st1:placename w:st="on">KEMP</st1:placename> <st1:placetype w:st="on">TOWN</st1:placetype> TO <st1:city w:st="on"><st1:place w:st="on">KABUL</st1:place></st1:city>, AS SCHNEWS INTERVIEWS AL JAZEERA JOURNALIST MEDYAN DAIRIEH ABOUT HIS TAKE ON THE WAR</strong> <br />  <br /> Ever keen to get an up close and personal look at the world&rsquo;s most troubled regions, SchNEWS was fortunate enough to sweet-talk Brightonian Al Jazeera journalist Medyan Dairieh recently returned from reporting in Afghanistan. As a Palestinian who has extensively covered the conflicts in the Occupied Palestinian Territories, Iraq and Turkish Kurdistan for many years, he&rsquo;s a man with a useful perspective on events on the ground in what&rsquo;s become Britain&rsquo;s longest war since the days of Empire. Medyan has become a well known figure amongst pro-Palestine and anti-war protesters, informing the public in the Arab-speaking world about the UK&rsquo;s solidarity movements. <br />  <br /> <strong><em>So what was it like arriving in Afghanistan?</em></strong> <br />  <br /> When I arrived in Kabul I could see even from the plane that it is a very poor city. When you walk from the airport you can see Turkish army and American armies, but the airport is very small and the electricity is cut off for half an hour at a time. Once I went to the street, I knew everything about this country. There is no life at all. The electricity is sometimes on, sometimes off, no clean water, no sanitation at all. And this is in Kabul. Outside the city is nothing, most people don&rsquo;t know what a cooker is - they just cook on wood. Most of the roads in Afghanistan have not been concreted. The water you see on the street is sewage water coming from the houses since no proper sewage system is in place. Some of the streets I visited have a stream of dirty water running, and they are really hazardous. The houses are very poor. This is Afghanistan. <br />  <br /> People said to me, &ldquo;<em>We wish we were in Soviet times, the Soviet period. We fought the Soviets, and they attacked us, but they also built for us. But the Americans, they attack us, destroy everything and don&rsquo;t build anything</em>.&rdquo; Now I think people would prefer to go back to the Soviet period. <br />   <br /> I met a man who lost 3 children. One son and two nephews. He ran away from where he lived after they were killed by an American aeroplane just a few months ago. The refugees say that the fighters don&rsquo;t come in the daytime, they come at night, so why do the Americans attack in the afternoon? Some people say that in the village there are no Taliban, the Taliban go away from the villages. They use the mountains; sometimes they come to the villages for water and food. But the Americans don&rsquo;t know where the fighters are so they attack civilians. <br />  <br /> <strong><em>What do people make of the Taliban?</em></strong> <br />   <br /> What can I say? People think that the Taliban will give them a better deal than the Americans will. People can&rsquo;t believe in the Americans, they can&rsquo;t believe in the government either. All the buildings, government departments, are corrupt. And so people hate America now. Not because of religion, but many people join the Taliban because they want to take revenge against America. There are many, many areas that the Americans can&rsquo;t go. Many areas of Afghanistan are under the control of the Taliban. The mayors of some cities in Helmand province have run away. The Americans take them back after heavy fighting, for just one day, two days, and then they run away again, because the Taliban is really completely in control of the area.&nbsp; <br />  <br /> I interviewed many Taliban leaders; one was an ex-minister, Mullah Abdul Azad. He is the leader of the Taliban in Kabul. The Afghani army surrounds his house all the time. When I went to see him he gave me food and [somewhere to] sleep and tea and everything, so we talked about this a lot. He said, &ldquo;<em>One day the Americans will come to this house to talk to us about the situation in Afghanistan</em>.&rdquo; He promised me this. <br />   <br /> <strong><em>So tell us about the Taliban. Are there &lsquo;moderate&rsquo; and &lsquo;extremist&rsquo; political factions within the Taliban? Do you think that the Americans will be able to negotiate?</em></strong> <br />  <br /> Absolutely not. Absolutely not. The Taliban are a united group who make decisions together. Brown is trying to talk to moderate Taliban; however, moderate Taliban do not take decisions independently from the other Taliban. The people in Kabul, they are the face of the Taliban. They don&rsquo;t do anything military, they&rsquo;re the political half of the Taliban. Their fathers fought, you know, and they believed, and they fought the Soviets because of religion and communism first, and secondly because of the land. The same has happened - the Americans are like the Soviets now. Many people who are not religious look at the Taliban and want to fight the Americans because of the land. Most Afghan people believe that they are under occupation and they have to do something with America. <br />  <br /> I asked Taliban leader Mullah Abdul Azad, &ldquo;<em>Where does your money come from?</em>&rdquo; He said, &ldquo;<em>We never had money from drugs. We fight the Afghan army for sometimes one hour, sometimes three hours. We control the area, we take everything from them, weapons, money. And some people give us money</em>.&rdquo; A Taliban leader said to me, &ldquo;<em>We have weapons, fighters, and the land to fight on. We can continue for many many years, and if the Americans do not believe us they must go and ask the Soviets, they will have their answer. When they come in, we go. When they&rsquo;re just standing around, we attack them. And then we go back. We can hold an area for a day or a maybe few hours,  just to show everyone that we are still here and we can fight America, that America is nothing</em>.&rdquo; <br />  <br /> <strong><em>What does all this have to do with &lsquo;America&rsquo;s Most Wanted&rsquo; - Osama Bin Laden? <br /> </em> <br /> </strong>Some people, he, and al Qaeda, were created by America. People say that Osama&rsquo;s time is over in Afghanistan. Everyone believes that al Qaeda is just a name; just a few people maybe. Just cassettes on al Jazeera. One guy, sitting in his bathroom, you know, saying, &ldquo;We will fight America!&rdquo; Maybe he&rsquo;s with the CIA - I don&rsquo;t know! <br />  <br /> <strong><em>In the UK, especially in Brighton, you report on the protest scene. Are people in Afghanistan aware that there&rsquo;s a big movement against the war?</em></strong> <br />   <br /> Well, I&rsquo;m sorry to tell you but so many people are without TVs. In some areas people don&rsquo;t know even who it is that they are fighting. They haven&rsquo;t gone to school, they&rsquo;ve lived in the mountains for many, many years, and they don&rsquo;t even know who the war is between! <br />   <br /> <strong><em>Before you left did you see any evidence of any development?</em></strong> <br />  <br /> When I came to the airport I saw there was one room, just a normal room - not even chairs, paintings, anything like that, not like an airport [lounge] at all. But when you leave you&rsquo;d think you&rsquo;re in an Israeli airport. Everybody in smart uniforms, x-ray machines, everybody checked one by one. I asked about this because normally they do this when people come to a country. They told me, &ldquo;<em>Well, if you want to bring weapons here we have many more. You would be crazy to bring heroin to Afghanistan when we give all the world its heroin. So we don&rsquo;t need anything from outside</em>.&rdquo; They said the Americans built this building, the airport. It was the only new thing I saw there. <br />  <br /> * Read the full interview - <a href='http://www.schnews.org.uk/features/show_feature.php?feature=13'>SchNEWS Feature Section</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=afghanistan&source=708">afghanistan</a>, <a href="../keywordSearch/?keyword=al-qaeda&source=708">al-qaeda</a>, <a href="../keywordSearch/?keyword=taliban&source=708">taliban</a>, <a href="../keywordSearch/?keyword=war+on+terror&source=708">war on terror</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(641);

				if (SHOWMESSAGES)	{

						addMessage(641);
						echo getMessages(641);
						echo showAddMessage(641);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


