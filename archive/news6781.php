<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 678 - 5th June 2009 - Convoy Polloi</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, stonehenge, savernake forest, police brutality, free party, new age travellers, police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 678 Articles: </b>
<b>
<p><a href="../archive/news6781.php">Convoy Polloi</a></p>
</b>

<p><a href="../archive/news6782.php">The Rich Get Richter</a></p>

<p><a href="../archive/news6783.php">Tara For Now  </a></p>

<p><a href="../archive/news6784.php">Deportation Alert</a></p>

<p><a href="../archive/news6785.php">Holy Unjustified</a></p>

<p><a href="../archive/news6786.php">Photo Finish</a></p>

<p><a href="../archive/news6787.php">Rebels Without A Causeway</a></p>

<p><a href="../archive/news6788.php">Brian Of Britain</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/678-beanfield-lg.jpg" target="_blank">
													<img src="../images/678-beanfield-sm.jpg" alt="The Battle of the Beanfield, June 1st, 1985" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th June 2009 | Issue 678</b></p>

<p><a href="news678.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CONVOY POLLOI</h3>

<p>
<p><strong>24 YEARS ON, SchNEWS REVISITS THE BATTLE OF THE BEANFIELD...</strong> <br />  <br /> <em>&ldquo;What I have seen in the last thirty minutes here in this field has been some of the most brutal police treatment of people that I&rsquo;ve witnessed in my entire career as a journalist. The number of people who have been hit by policemen, who have been clubbed whilst holding babies in their arms in coaches around this field, is yet to be counted. There must surely be an enquiry after what has happened today.&rdquo;</em> - <strong>Ken Sabido ITN journalist.</strong>&nbsp; <br />  <br /> Twenty four years have passed since the defining moment of the Thatcher government&rsquo;s assault on the traveller movement - the Battle of the Beanfield. On June 1st 1985 a convoy of vehicles set off from Savernake Forest in Wiltshire towards Stonehenge, with several hundred travellers on their way to setting up the 14th Stonehenge Free Festival. But this year English Heritage, who laughably were legally considered the owners of the Stonehenge Sarsen circle (built several thousand years before by god knows who), had secured an injunction against trespass naming 83 people. This was considered legal justification enough for a brutal assault on the entire convoy. What followed was a police riot and the largest mass arrest in British history. <br />  <br /> As the Convoy made its way to the Stones the road was blocked with tonnes of gravel and it was diverted down a narrow country lane, which was also blocked. Suddenly a group of police officers came forward and started to break vehicle windows with their truncheons. Trapped, the convoy swung into a field, crashing through a hedge. <br />  <br /> For the next four hours there was an ugly stalemate. The Convoy started trying to negotiate, offering to abandon the festival and return to Savernake Forest or leave Wiltshire altogether. The police refused to negotiate and told them they could all surrender or face the consequences. <br />  <br /> At ten past seven the &lsquo;battle&rsquo; began. In the next half hour, the police operation &ldquo;<em>became a chaotic whirl of violence</em>.&rdquo; Convoy member Phil Shakesby later gave his account of the day: <br />  <br /> &ldquo;<em>The police came in [to the grass field] and they were battering people where they stood, smashing homes up where they were, just going wild. Maybe about two-thirds of the vehicles actually started moving and took off, and they chased us into a field of beans.&nbsp; <br />  <br /> By this time there were police everywhere, charging along the side of us, and wherever you went there was a strong police presence. Well, they came in with all kinds of things: fire extinguishers and one thing and another. When they&rsquo;d done throwing the fire extinguishers at us, they were stoning us with these lumps of flint</em>.&rdquo;&nbsp; <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/678-henge-84-stones-herb-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/678-henge-84-stones-herb-sm.jpg"  /></a></td></tr></table>By the end of the day over four hundred were under arrest and dispersed across police stations around the whole of the south of England. Their homes had been destroyed, impounded and in some cases torched. <br />  <br /> <strong>THE VAN GUARD?</strong> <br />  <br /> In today&rsquo;s surveillance society Britain it is seems inconceivable that festivals like the Stonehenge Free Festival ever happened. At their height these gatherings attracted 30,000 people for the solstice celebration - 30,000 people celebrating and getting on with it without any need for the state or its institutions. The festivals themselves were just the highpoint of a year-round lifestyle of living in vehicles. As one traveller said at the time, &ldquo;<em>The number of people who were living on buses had been doubling every year for four years. It was anarchy in action, and it was seen to be working by so many people that they wanted to be a part of it too</em>.&rdquo;&nbsp; <br />  <br /> Having seen off the miners strike &ndash; the first casualties in the plan to re-order Britain according to  neo-liberal economics (or as it was known locally - Thatcherism), the state turned its force on a more subtle threat. This time not people fighting for jobs and a secure place in the system but people who rejected that system outright. Although prejudice against travellers was nothing new, the traditional &lsquo;ethnic&rsquo; travelling minority represented no significant threat to the status quo that couldn&rsquo;t be dealt with by local authorities. But to many of the millions left unemployed by the Thatcher revolution, life on the road looked increasingly appealing. This was inconvenient for a state determined that conditions for the unemployed be miserable enough to spur them into any form of low-paid work. <br />  <br /> <strong>WHEELS ON FIRE</strong> <br />  <br /> The propaganda directed against the so-called &lsquo;peace convoys&rsquo; by all sections of the media created an atmosphere which allowed draconian action. The Beanfield was not an isolated incident. The Nostell Priory busts of the previous year were a vicious foreboding of what was to come. Months before the Beanfield a convoy-peace camp site at Molesworth was evicted by police acting with 1500 troops and bulldozers headed by a flak-jacketed Michael Heseltine, then Defence Secretary. In 1986 Stoney Cross in the New Forest saw another mass eviction. At the time Thatcher said she was "<em>only too delighted to do what we can to  make things difficult for such things as hippy convoys</em>". This attempt to create a separate yet peaceful existence from mainstream society was to be ruthlessly suppressed.&nbsp; <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/678-tash-1-sm.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/678-tash-1-sm.jpg"  /></a></td></tr></table>Over the next ten years &ndash; notably with the Public Order Act 1986 and the Criminal Justice Act 1994 the whole lifestyle was virtually outlawed. As John Major said at the Tory Party conference in 1992 to thunderous applause: &ldquo;New age travellers &ndash; not in this age &ndash; not in any age&rdquo;. The CJA removed the duty of councils to provide stop-over sites for travellers and regular evictions began to punctuate traveller life. But it wasn&rsquo;t all one way, thousands stayed on the road and the free festival circuit was infused with fresh blood from the rave scene. Even after the massive crackdown that followed the Castlemorton free festival the convoys in many cases moved onto road protest sites. <br />  <br /> Ultimately however travellers were forced to adapt - abandoning the garish war paint of the hippy convoys for more anonymous vans, moving and taking sites in smaller groups. Many went abroad or were driven back into the cities. However, despite the worst excesses of the cultural clampdown, travellers remain all over the country. Many are now in smaller groups, inconspicuous and unregistered. It&rsquo;s become more common for vehicle dwellers to take dis-used industrial sites blurring then lines between travelling and squatting.&nbsp; <br />  <br /> The fact that Stonehenge is now open again on the solstice might - on the face of it - look like a victory. But the event is a top-down affair complete with massive police presence, burger vans and floodlights &ndash; a far cry from the anarchistic experiments of the 70s and 80s. A smaller gathering had been permitted just down the road at the Avebury stone circle over recent years with the National Trust taking a far more lenient stance on live-in vehicles than English Heritage. But even there, since 2007, there's now a ban on overnight stays on the solstice.&nbsp; <br />  <br /> Much of the festival circuit these days is in the hands of profit-motivated commercial promoters apart from the growing shoots of a range of smaller festivals, who continue in the spirit of people-led celebrations of community co-operation. But festivals today are also mostly buried under an avalanche of red tape and security, health and safety requirements - The Big Green gathering saw its security costs treble in one year (2007) as they were told to &lsquo;terrorist harden&rsquo; the event. <br />  <br /> When popular history recalls the pivotal moments in the mid-80s for Thatcher's Britain, the Battle Of The Beanfield rarely adequately takes its place alongside the Miners Strike and Wapping. For UK Plc, travellers became - and remain - another 'enemy within', to be dealt with by organised state violence, like all others who have found an escape route from a society subordinated to profit, where freedom had been reduced to a series of consumer choices. <br />  <br /> * See also <a href='../archive/news25.htm'>SchNEWS 25</a> <br />  <br /> * For the definitive account see Andy Worthington&rsquo;s book &lsquo;The Battle Of The Beanfield&rsquo; - <a href="http://www.andyworthington.co.uk" target="_blank">www.andyworthington.co.uk</a> <br />  <br /> </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>free party</span>, <span style='color:#777777; font-size:10px'>new age travellers</span>, <a href="../keywordSearch/?keyword=police&source=678">police</a>, <span style='color:#777777; font-size:10px'>police brutality</span>, <span style='color:#777777; font-size:10px'>savernake forest</span>, <span style='color:#777777; font-size:10px'>stonehenge</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(346);
			
				if (SHOWMESSAGES)	{
				
						addMessage(346);
						echo getMessages(346);
						echo showAddMessage(346);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>