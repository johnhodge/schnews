<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 715 - 26th March 2010 - Met With Undue Force</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, camp for climate action, g20, met police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 715 Articles: </b>
<p><a href="../archive/news7151.php">A Twist Of Fete</a></p>

<p><a href="../archive/news7152.php">Thimble Pleasures</a></p>

<b>
<p><a href="../archive/news7153.php">Met With Undue Force</a></p>
</b>

<p><a href="../archive/news7154.php">New Deal Or No Deal</a></p>

<p><a href="../archive/news7155.php">Gimme Fife</a></p>

<p><a href="../archive/news7156.php">Edl: Bolton Blunderers</a></p>

<p><a href="../archive/news7157.php">Ee By Heckler</a></p>

<p><a href="../archive/news7158.php">Speak-ing Out</a></p>

<p><a href="../archive/news7159.php">Titnore: Jack Out The Box</a></p>

<p><a href="../archive/news71510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th March 2010 | Issue 715</b></p>

<p><a href="news715.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MET WITH UNDUE FORCE</h3>

<p>
It&rsquo;s been a sticky week for SchNEWS&rsquo; favourite state thugs. The Met face a possible &pound;250,000 compensation bill after admitting a raid on a squat the day after the G20 protests was illegal. The two demonstrators who took the Met to court are to receive &pound;3,000 each which leaves 68 other protesters able to claim. <br />  <br /> Cops burst into the squat armed with tasers, riot gear and a FIT team and proceeded to violently arrest the occupants (who had promptly sat down and offered no resistance) repeatedly cracking one activist on the head while he was on the floor. Despite cop claims that some &lsquo;violent&rsquo; protesters from the day before were in the squat, none of the squatters matched any of the photos. <br />   <br /> This followed a ruling in February in favour of climate camp, which declared the stop &lsquo;n&rsquo; search tactics at Kingsnorth unlawful (see <a href='../archive/news708.htm'>SchNEWS 708</a>), anyone who&rsquo;s ever been a bit pissed off with the way the cops have treated them &ndash; now&rsquo;s your chance! <br />  <br /> If you were one of the squatters &ndash; find out how to file your claim by emailing <a href="mailto:legal@climatecamp.org.uk">legal@climatecamp.org.uk</a> <br />   <br /> Also under the spotlight this week has been Met officer Delroy Smellie for his attack on protester Nicola Fisher at the memorial event for Ian Tomlinson, killed by police the day before. Despite footage showing Smellie striking Nicola with the back of the hand and then twice with a baton he denies assault. This &lsquo;flick of the hand&rsquo; was apparently prompted by Smellie&rsquo;s trouser wetting at the rampaging hordes around him, and the weapon Fisher was holding (which turned out to be a carton of organge) a terror kept well hidden from all the cameras around. The trial continues at Westminster Mags next week. <br />  <br /> Another embarrassment for the Met came when footage was released of them beating a protester with batons and shields at the Gaza demo last year before arresting and charging him with violent disorder. After withholding seven and a half hours of  video evidence until the day before the trial the cops dropped the case. Had he be found guilty Jake Smith would have been facing up to three years inside. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=715">camp for climate action</a>, <a href="../keywordSearch/?keyword=g20&source=715">g20</a>, <a href="../keywordSearch/?keyword=gaza&source=715">gaza</a>, <a href="../keywordSearch/?keyword=met+police&source=715">met police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(708);
			
				if (SHOWMESSAGES)	{
				
						addMessage(708);
						echo getMessages(708);
						echo showAddMessage(708);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


