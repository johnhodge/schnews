<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 720 - 30th April 2010 - Meltdown Britain: Debts All Folks...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, imf, banks, economy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 720 Articles: </b>
<p><a href="../archive/news7201.php">Crash Of The Titans</a></p>

<p><a href="../archive/news7202.php">Mayday! Mayday!</a></p>

<b>
<p><a href="../archive/news7203.php">Meltdown Britain: Debts All Folks...</a></p>
</b>

<p><a href="../archive/news7204.php">Off The Rails</a></p>

<p><a href="../archive/news7205.php">Deportation Update</a></p>

<p><a href="../archive/news7206.php">Obtuse Angles</a></p>

<p><a href="../archive/news7207.php">Chinese Burn</a></p>

<p><a href="../archive/news7208.php">Radioactive Wasters</a></p>

<p><a href="../archive/news7209.php">Schnews In Brief</a></p>

<p><a href="../archive/news72010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th April 2010 | Issue 720</b></p>

<p><a href="news720.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MELTDOWN BRITAIN: DEBTS ALL FOLKS...</h3>

<p>
It&rsquo;s the elephant in the room as we lurch towards another election - IMF-style austerity cuts in the UK. In order to bail the banks out of their self-induced crisis, the UK government directly spent &pound;50bn from tax revenue income in 2008, not to mention the hundreds of billions of pounds of support committed since to prop up the whole sorry system.&nbsp; <br />  <br /> Not that any of these selfless acts of charity stopped bank bosses handing out giant bonuses to themselves last year of course - a whacking &pound;6bn in total - with the wholly taxpayer-owned RBS handing out &pound;1bn in &lsquo;reward&rsquo; bonuses. <br />  <br /> This huge increase in government spending has led to the ballooning of the national debt to &pound;900 billion. The majority of the increase went to staving off financial Armageddon. This means that 10% of all taxation will go just to financing the national debt. <br />  <br /> International credit agencies like Standard &amp; Poors are warning that the UK&rsquo;s triple-A credit rating is under review (i.e. the perceived likelihood of the UK being able to repay its debts). If they do downgrade the credit rating it&rsquo;s likely to trigger a capital flight from the UK the likes of which haven&rsquo;t been seen since &lsquo;76 when Denis Healey called in the IMF. <br />  <br /> After the election, whichever bunch of clowns in suits have been elected, we&rsquo;re gonna find out who really runs the country. As the Greeks, Spanish and Portuguese are finding out - the shadowy forces of international capitalism can impose ruthless solutions on economies - always with the result that public services are slashed and money is channelled upwards. <br />  <br /> All three main parties are promising big restructuring after the election - meaning big cuts in public services. The Institute for Fiscal Studies thinktank, whose widely accepted number-crunching usually forms the basis of government policy and opposition attack alike, this week said that all three of the main parties&rsquo; manifestos had failed to account for any more than a tiny fraction of the actual level of cuts needed. <br />   <br /> And of course those cuts aren&rsquo;t going to fall on the necks of those whose profiteering caused the crisis in the first place (or those still making a mint). Nope, it&rsquo;s front-line services and welfare that are gonna get the axe. Will this be the trigger to get the hitherto apathetic British public out onto the streets? &hellip;here&rsquo;s hoping. <br />    <br /> To paraphrase the ancient anarchist saying-  &ldquo;<em>It doesn&rsquo;t matter who you vote for...a bunch of  bankers are gonna get in</em>.&rdquo;  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>banks</span>, <a href="../keywordSearch/?keyword=economy&source=720">economy</a>, <a href="../keywordSearch/?keyword=imf&source=720">imf</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(756);

				if (SHOWMESSAGES)	{

						addMessage(756);
						echo getMessages(756);
						echo showAddMessage(756);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


