<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 790 - 30th September 2011 - Israel-Palestine: Stating the Obvious</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 790 Articles: </b>
<p><a href="../archive/news7901.php">Fighting Frack Addiction</a></p>

<p><a href="../archive/news7902.php">Smash Edo On Tour</a></p>

<p><a href="../archive/news7903.php">Inside Schnews</a></p>

<p><a href="../archive/news7904.php">Sabs Sabbed</a></p>

<b>
<p><a href="../archive/news7905.php">Israel-palestine: Stating The Obvious</a></p>
</b>

<p><a href="../archive/news7906.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th September 2011 | Issue 790</b></p>

<p><a href="news790.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ISRAEL-PALESTINE: STATING THE OBVIOUS</h3>

<p>
<p>  
  	The Palestinian Authority&#39;s bid for recognition as a UN member state has barely left President Mahmoud Abbas&#39; lips (see <a href='../archive/news798.htm'>SchNEWS 798</a>) and already the over-zealous elements of Israel&#39;s right-wing have kicked into action. This week leaders from several political factions, including chairman of Prime Minister Benyamin Netanyahu&#39;s Likud party, called for the annexation of West Bank settlements in response to Abbas&#39; move in New York. <br />  
  	 <br />  
  	In a letter to Netanyahu they also urged cutting Palestinian aid money, accelerating settlement expansion, and the prohibition of any Palestinian construction in areas controlled by Israeli security forces. These intrepid politicians have obviously never been to the Jordan Valley (or any other part of the West Bank classified as Area C) otherwise they would realise the last recommendation is already a policy of the Israeli occupation forces. <br />  
  	 <br />  
  	Such ideas have also been aired across the pond. A group of 25 Republicans attempted to introduce a bill in the US Congress supporting Israel&#39;s right to annex the whole of the West Bank should Abbas follow through with the statehood bid. The idea is apparently not popular in Israel itself &ndash; presumably because it would mean over one million Palestinians being absorbed into the Israeli state altering the demographic balance. <br />  
  	 <br />  
  	An idea which is gaining traction in Congress, however, is legislation to cut some or all of America&#39;s $600 million (approx) annual aid to the Palestinian Authority as punishment for daring to seek statehood. This has been met with unlikely opposition from the pro-Israel lobby with J-Street and even the American Israel Public Affairs Committee calling it counter-productive. <br />  
  	 <br />  
  	Back in the West Bank settlement expansion is insatiable and this week Egypt&#39;s Foreign Minister, Mohamed Amr, criticised Israel over plans to construct 1,100 homes in the Gilo neighbourhood of occupied East Jerusalem. An estimated 6,000 settlement housing units have been given the green light in the last two months. Whilst Hilary Clinton meekly called the plans for Gilo &#39;counterproductive&#39;, there is, unsurprisingly, no Congressional plan to cut US aid to Israel; which in 2010 totalled more than $2bn. <br />  
  	 <br />  
  	Despite the guarantee the the US will veto Palestine&#39;s statehood a final call from the UN Security Council is likely to take weeks. On Wednesday the Council announced it will delay any decision in order to give more time for international efforts to revive direct peace talks. Considering the Palestinians have said, quite rightly, they are unwilling to come to the table unless settlement expansion is frozen it looks as though we are in for a long wait. <br />  
  	 <br />  
  	Not that peace talks are likely to bring any respite to the millions of Palestinians suffering daily under the barbarity of Israeli occupation. The 1994 Oslo Accords carved up the West Bank, gave Israel control over most of it, and did nothing to prevent subsequent Israeli military incursions into both the West Bank and Gaza that killed thousands of innocent Palestinians. Even the normally pliant PLO leadership now openly refers to quartet envoy Tony Blair as &ldquo;the Israeli ambassador&rdquo;. How things are going to get any better is any ones guess. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1376), 159); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1376);

				if (SHOWMESSAGES)	{

						addMessage(1376);
						echo getMessages(1376);
						echo showAddMessage(1376);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


