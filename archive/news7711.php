<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 771 - 13th May 2011 - Fracking Hell</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, energy industry, climate change, gas industry" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT June 1st 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 771 Articles: </b>
<b>
<p><a href="../archive/news7711.php">Fracking Hell</a></p>
</b>

<p><a href="../archive/news7712.php">Greece Not Lightening</a></p>

<p><a href="../archive/news7713.php">Stay Alive Dont' Drive 55</a></p>

<p><a href="../archive/news7714.php">Political Football</a></p>

<p><a href="../archive/news7715.php">Hardest Hit Parade</a></p>

<p><a href="../archive/news7716.php">Schnews In Brief</a></p>

<p><a href="../archive/news7717.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 13th May 2011 | Issue 771</b></p>

<p><a href="news771.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FRACKING HELL</h3>

<p>
<p>  
  	<strong>AS SCHNEWS DIGS DEEP TO EXPOSE CRACKS IN EXPLOSIVE ENERGY CRAZE</strong>  </p>  
   <p>  
  	A few months since Fukishima, nearly a year since Deepwater Horizon but the global elite aren&rsquo;t resting on their laurels and it looks like we won&rsquo;t have to wait too long for the fossil fuel industry to cause the next environmental apocalypse. New kid on the block is the appropriately named &lsquo;fracking&rsquo; &ndash; or, more explicitly: hydraulic fracturing, a method of extracting natural gas from shale rock layers thousands of feet deep &ndash; and its coming here soon.  </p>  
   <p>  
  	Already causing waves of mass panic across the US, fracking is a technique that involves pumping huge quantities of water, sand and highly toxic chemicals into shale formations, then blowing it all sky high with explosives in order to fracture the rocks, stick a bore down and get to the fuel resource. This energy-intensive process results in vast amounts of radioactive waste &lsquo;water&rsquo;, earthquakes and subsistence in the areas around the mines, noxious chemicals being vented into the air, and causes significantly more CO2 emissions than conventional gas extraction.  </p>  
   <p>  
  	<strong>IT&rsquo;S ALL RIGGED</strong>  </p>  
   <p>  
  	But it&rsquo;s all hands on the rig in the scramble for more &lsquo;homegrown&rsquo; alternatives to Arab oil.&nbsp; The recent surge in fracking is largely due to changes in the US Energy Policy Act, pushed by Dick Cheney in 2005. Known as the &lsquo;Halliburton loophole&rsquo;, it exempted fracking wells from federal regulation under the Safe Drinking Act -&nbsp; letting energy companies pollute Americans&rsquo; drinking water without having to by bothered by pesky government interference.  </p>  
   <p>  
  	The fracturing fluid contains carcinogenic chemicals, such as benzene, and endocrine disruptors &ndash; compounds which can be blamed for a huge range of developmental problems such as birth defects, deformations of the body and sexual development problems. It&rsquo;s made radioactive by the presence of radium, and other naturally occurring hazardous chemicals such as arsenic and mercury, all&nbsp; brought to the surface through the extraction process. It&rsquo;s unclear exactly what goes into this noxious cocktail in the first place as the corporations using it are keeping conveniently schtum about their recipe and, despite Dow Chemical admitting to supplying antimicrobial poisons to go into the mix, no regulator is demanding disclosure from the gas miners.  </p>  
   <p>  
  	Toxic sludge from over 71,000 wells is currently sitting in huge open storage tanks and leeching into the water table in Pennsylvania, Texas, Wyoming, Arkansas and other US states. The methane that the extractors are after is also seeping into underground aquifers, causing one of the most visible polluting effects: the phenomena of inflammable water. Residents of Pennsylvania, some living up to 1km from mining operations, have water coming out of their taps that has such a high methane content it will actually burst into flames if a match is held near it. An accidental spill in the same state has killed trees and contaminated waterways, with one local explaining, &ldquo;<em>it&rsquo;s killed all the fish. [The pond] turns colours different times of the day. Now it&rsquo;s fluorescent orange</em>.&rdquo;  </p>  
   <p>  
  	Arkansas and Texas have seen unprecedented numbers of earthquakes since mining operations began. In six months Arkansas had over 1,000 earthquakes with more than a dozen registering over 3.0 on the Richter scale, 90% of which have all been within 6km of waste water disposal wells.  </p>  
   <p>  
  	Not only is it polluted drinking water and unheard-of seismic activity, but air pollution in Texas has gone off the scale. Even considering the notoriously relaxed environmental quality standards in Texas, in some towns the carcinogen benzene is at levels 55 times higher than laws allow, and neurotoxicants xylene and carbon disulphide, and the blood poison napthalene were found to be up to 384 times over the safe limits.  </p>  
   <p>  
  	But don&rsquo;t worry, at least there&rsquo;s lots of cold hard cash involved. BP paid more than $3billion for fracking rights in 2008, BHP Bilton spent $5 billion on sites in Arkansas, and Shell have snapped $4.7 billion&rsquo;s worth of assets in the Marcellus Shale formation in West Virginia and prime mining land in Pennsylvania. The Marcellus Shale is a formation the size of Greece, more than a mile underground, and valued at around $2 trillion dollars. No wonder they&rsquo;re happy to pay out small change in reparations if needed, as Cabot Oil &amp; Gas did recently when coughing up $4 million in damages to 14 families in Dimock, Pennsylvania, and agreeing to replenish the water supply after a defective well-casing poisoned the drinking water.  </p>  
   <p>  
  	<strong>BLACKPOOL ILLUMINATI</strong>  </p>  
   <p>  
  	Never one to be too far behind an ill-thought-out American blunder, the UK has eagerly embarked on a fracking frenzy of its own. Blackpool is now not only famous for sticks of rock, naughty postcards and the Pleasure Beach, but also the UK&rsquo;s first frack up. Cuadrilla Resources, a UK energy company, has started extracting methane from the Bowland shale formation 4,000 feet underground and less than four miles away from Blackpool Tower.  </p>  
   <p>  
  	More sites are planned. Cuadrilla has a licence which covers most of Lancashire and is planning two more wells. In a recent government inquiry, potential areas for drilling included the Weald - the area of the North and South Downs running through Hampshire, Surrey, Kent and Sussex; Cambrian Bay - an area stretching from central England into Wales; the length of the Pennines; and in the Cheshire area in the north-west. See <a href="https://www.og.decc.gov.uk/upstream/licensing/shalegas.pdf" target="_blank">https://www.og.decc.gov.uk/upstream/licensing/shalegas.pdf</a> for a map of known shale formations in the UK, and where mining operations have found gas along them. Other energy companies Island Gas, Composite Energy and Nexes are also sniffing around for new wells.  </p>  
   <p>  
  	Looking to the horizon, offshore reserves are ringing even more pound signs in the corporate world&rsquo;s eyes. Fracking in the sea is something that hasn&rsquo;t yet been explored by the energy industry&rsquo;s boardrooms, but the potential is massive compared to what could be extracted inland. According to Nigel Smith a Geophysicist from the&nbsp; British Geological Survey who gave evidence at the government inquiry, &ldquo;<em>There will not be any environmental problems there either, I would not say. As for the saline [waste] water, it depends how saline it is. Maybe you could just put it into the seawater.</em>&rdquo; Anyone fancy a paddle?  </p>  
   <p>  
  	Elsewhere, Shell are drilling wells. They have already drilled a shale gas well in Sweden, and there is a lot of drilling activity going to take place this year across the rest of Europe, and South Africa. China and India have also recently announced plans to search for shale deposits to extract methane.  </p>  
   <p>  
  	Some communities have been rallying around to fight this terrifyingly polluting process.  </p>  
   <p>  
  	Thousands of people turned out to protest against planned operations in France last month, resulting in the French government becoming one of the first to ban fracking, despite being one of the most&nbsp; &lsquo;promising&rsquo; territories for shale gas drilling in Europe, with extensive deposits in the south and in the area near Paris.  </p>  
   <p>  
  	In the US, students and communities around the wells have been organising against the work of the gas companies, and the American government recently relaunched an inquiry into the environmental impact of fracking following widespread protest. A 2004 report gave fracking a clean bill of health, but the new inquiry is due to reconsider the verdict, but is only due to announce the results in 2013.  </p>  
   <p>  
  	Meanwhile, drilling continues apace. One of the key issues is the total lack of knowledge around shale drilling and how the explosions can impact the bedrock. Each shale formation will have its own particular characteristics, and the possibility of blowing holes that will cause the waste fluid to pass into the water table is unknown. Each new well will need to be examined independently to determine the level of risk.  </p>  
   <p>  
  	Until the potential polluting consequences are known it is obviously typical money-blinded lunacy to start blasting England&rsquo;s bedrock with radioactive and carcinogenic chemicals, in return for even more CO2 emissions. They should all fracking leave the well(s) alone. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1214), 140); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1214);

				if (SHOWMESSAGES)	{

						addMessage(1214);
						echo getMessages(1214);
						echo showAddMessage(1214);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


