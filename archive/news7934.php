<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 793 - 21st October 2011 - Uganda: Dropping a Kalanga</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 793 Articles: </b>
<p><a href="../archive/news7931.php">Dale Farm Evicted</a></p>

<p><a href="../archive/news7932.php">Crap Alchemist Of The Week</a></p>

<p><a href="../archive/news7933.php">Occupy Everywhere</a></p>

<b>
<p><a href="../archive/news7934.php">Uganda: Dropping A Kalanga</a></p>
</b>

<p><a href="../archive/news7935.php">Lsx: Occupy Eyed</a></p>

<p><a href="../archive/news7936.php">Brought To The Book</a></p>

<p><a href="../archive/news7937.php">Blackpool Rocked</a></p>

<p><a href="../archive/news7938.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st October 2011 | Issue 793</b></p>

<p><a href="news793.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>UGANDA: DROPPING A KALANGA</h3>

<p>
<p>  
  	SchNEWS always welcomes submissions from readers - we can&rsquo;t be everywhere and know about everything, hard as we try - and it eases the load on our strung out, RSI withered writing team.&nbsp; Here&rsquo;s something we got sent, found of interest and passed on to you:  </p>  
   <p>  
  	&ldquo;The latest Oxfam campaign adverts recently caught my eye. A barren field with only Caterpillar digger and the poignant phrase: &lsquo;Home is where the bulldozers are,&rsquo; and &lsquo;This is where my family used to live.&rsquo;  </p>  
   <p>  
  	This image and others struck a chord, reminding me of the current plight faced by the families at Dale Farm. Imagine my horror when, under further investigation, I discovered that this campaign was to highlight the forced eviction of 22,000 people of of their own land in Uganda, between 2007 and 2011.  </p>  
   <p>  
  	This eviction took place after the New Forest Company (N.F.C.), a British Timber company, took over the management of the Luwunga Forest Reserve. N.F.C., although painting an image of sustainability and social responsibility, are your typical climate criminals. Funded by the World Bank and other similar organisations, H.S.B.C. also owns a 20 per cent share. They have been buying up land all over Africa, clearing it and replanting Pine and Eucalyptus (two of the most ecologically damaging trees around), plus some indigenous species as well. (In Uganda the annual rate of deforestation has climbed 21 per cent since the end of the nineties, resulting in the loss of two thirds of its forest over the last 20 years.) Although claiming that they are investing in the community by building additional infrastructure creating jobs and water, the fact is N.F.C&nbsp; have commandeered the local school as their head office in the field.  </p>  
   <p>  
  	Land tenure is a major issue, with the government distributing parcels of land and then taking them back (sound familiar?). Many of the evicted claim they were given the land by the Idi Amin government after fighting for the British during WW2, while others have legally bought it. Termed &lsquo;squatters&rsquo; by the Ugandan Courts and N.F.C., and given only three months notice before being forced of of their own land, (during which they experienced physical violence and destruction of their property, crops and livestock) the people are in shock and traumatised.  </p>  
   <p>  
  	The N.F.C. states that Oxfam are exaggerating, and that this land grab only affected 800 people, most of whom left peacefully after being given four months notice by the police. &nbsp;  </p>  
   <p>  
  	The stories of the local people are different. Many of them have been left destitute, receiving no compensation or alternative land. They are no longer able to send their children to school, and they question how they are going to survive. One small farmer (termed a &lsquo;squatter&rsquo; by the authorities) who grew three acres of coffee, five of bananas, plus avocados, mangos, beehives and two permanent houses, has lost everything. Previously self-sufficient, he now struggles to feed his family and has recognised that &ldquo;if you don&rsquo;t eat well, you become sick and weak&rdquo;; a state that those in charge seem to be happy for us to be in.  </p>  
   <p>  
  	On the 11th September 2011, the &lsquo;Human rights network for journalists in Uganda&rsquo; reported (on Facebook) that &lsquo;plans are under-way for Kalanga District Leadership to pass a resolution banning all media activity in the area&rsquo;. This is a draft resolution prepared by the council and seems to confirm the treatment of media who question the state. Oxfam are requesting an investigation into the actions of N.F.C. and, looking at the facts, it appears that both this British company - and the international organisations which support it - are complicit in this inhumane land grab from the indigenous people and the destruction of the natural environment: all in the name of producing cheap resources for people overseas.  </p>  
   <p>  
  	This story has so many parallels with the planned eviction of Dale Farm in Essex. Earlier this year, the Romany Council began a world-wide campaign of &lsquo;zero evictions&rsquo;, stating people have a right to have a safe space to live whatever their financial situation. I am aware that President Chavez of Venezuela has committed to this. Safe, secure housing in whatever form we choose is a basic human right, and it is important at this time that we unite against the common enemies who threaten this. In Uganda locals have already started to uproot the trees planted by the N.F.C. Remember: actions speak louder than words.&rdquo;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1397), 162); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1397);

				if (SHOWMESSAGES)	{

						addMessage(1397);
						echo getMessages(1397);
						echo showAddMessage(1397);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


