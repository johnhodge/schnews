<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";
	
	incrementIssueHitCounter(57);
?>

<html>
<head>

<title>SchNEWS - 27th August 2009 - London Climate Camp Report</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, hls, huntingdon life sciences, vivisection, direct action, shac, morgan stanley, bayer, camp for climate action, climate change, twitter, direct action, camp for climate action, open cast, direct action, climate change, wales, bnp, red white and blue festival, far-right, nick griffin, uaf, unite against fascism, anti-fascists, portsmouth green fair, zippos circus, animal rights, faslane peace camp, direct action, trident missiles, hicham yezza, ceasefire, nottingham, vestas, isle of wight, workers struggles, climate change, renewable energy, tesco, tescopoly, titnore woods, supermarkets, direct action, worthing" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="/issue.css" type="text/css" /> 


<!--<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/archiveIndex.css" type="text/css" />
<link rel="stylesheet" href="../old_css/issue_summary.css" type="text/css" />-->


<!--[if IE]> <style type="text/css">@import "../issue_ie.css";</style> <![endif]-->



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'/images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'/images_main/schmovies-button-over.png',
					'/images_main/contacts-lhc-button-mo.gif',
					'/images_main/book-reviews-button-mo.gif',
					'/images_main/merchandise-button-mo.gif')"
></body>




<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg" 
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_main/description-new.png" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar">
		
		
			
		<!-- DONATE BUTTON -->
		<table width="110" style="border-color:#000000; border-style:solid; border-width:1px" align="center">
			<tr> 
				<td height="2"> 
					<p align="center">
						<font face="Arial, Helvetica, sans-serif" size="1">
							<b>SchNEWS - on the blag for money <BR>since 1994..</b>.<br /> 
						</font>
						<a 	href="/extras/help.php" 
							onMouseOut="MM_swapImgRestore()" 
							onMouseOver="MM_swapImage('Image45','','../images_main/donate-mo.gif',1)">
								<img 	name="Image45" 
										border="0" 
										src="../images_main/donate.gif" 
										width="84" 
										height="29" 
										alt="Send us a donation via PayPal or cheque" />
						</a>
					</p>
				</td>
			</tr> 
		</table>
		
		<!--	REST OF THE LEFT BAR 
		
				Due to the shitness of IE rendering engine it is better to leave this block of code as it is with no 
				other white space between tags, otherwise IE renders parts of the white spaces and messes up the display. -->
		<div style="border:none"> <img src="../images_main/horizontal-line.gif" width="110" height="4" align='center' style='margin-top: 4px; margin-bottom: 4px' > 
    <a href="../pages_merchandise/merchandise_video.php#uncertified"><img style="border: none" align='center' src='../images_main/uncertified-banner-110.jpg' width="110" border="0" alt="Uncertified" /></a></div>
		
		<form style="padding:0; margin: 0" action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
    <font size="2" face="Arial, Helvetica, sans-serif"> 
    <input type="hidden" name="cmd" value="_xclick" />
    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
    <input type="hidden" name="undefined_quantity" value="1" />
    <input type="hidden" name="item_name" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection" />
    <input type="hidden" name="item_number" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection" />
    <input type="hidden" name="amount" value="6.00" />
    <input type="hidden" name="no_shipping" value="2" />
    <input type="hidden" name="return" value="../extras/complete.htm" />
    <input type="hidden" name="cancel_return" value="../extras/cancel.htm" />
    <input type="hidden" name="currency_code" value="GBP" />
    <input type="hidden" name="lc" value="GB" />
    <input type="hidden" name="bn" value="PP-BuyNowBF" />
    </font>
	<div align="center" style='width:100px; border-top: 1px solid #cccccc; padding-top: 4px; margin-top:5px; margin-left:10px; margin-right:10px '>
	<font size="2" face="Arial, Helvetica, sans-serif"> 
          <input type="image" src="../images_main/buyNow.gif" class="backgroundColorChange" style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
          </font><br />
		 <font style='font-size:8px' face="Arial, Helvetica, sans-serif">&pound;6 
          per copy (inc. P&amp;P) <br>
          (via Paypal) </font></div></form><form style='padding:0; margin: 0' action="https://www.paypal.com/cgi-bin/webscr" target="_blank" method="post">
    <font size="2" face="Arial, Helvetica, sans-serif"> 
    <input type="hidden" name="cmd" value="_xclick" />
    <input type="hidden" name="business" value="paypal@schnews.org.uk" />
    <input type="hidden" name="undefined_quantity" value="1" />
    <input type="hidden" name="item_name" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection + Take Three 2007 Collection" />
    <input type="hidden" name="item_number" value="UNCERTIFIED - SchMOVIES 2008 DVD Collection + Take Three 2007 Collection" />
    <input type="hidden" name="amount" value="7.00" />
    <input type="hidden" name="no_shipping" value="2" />
    <input type="hidden" name="return" value="../extras/complete.htm" />
    <input type="hidden" name="cancel_return" value="../extras/cancel.htm" />
    <input type="hidden" name="currency_code" value="GBP" />
    <input type="hidden" name="lc" value="GB" />
    <input type="hidden" name="bn" value="PP-BuyNowBF" />
    </font>
	<div align="center" style='width:100px; border-top: 1px solid #cccccc; padding-top: 4px; margin-top:5px; margin-left:10px; margin-right:10px '>
		<font size="2" face="Arial, Helvetica, sans-serif"> 
          <input type="image" src="../images_main/buyNow.gif" class="backgroundColorChange"style="border: 1px solid black" border="0" name="submit2" alt="Make payments with PayPal - it's fast, free and secure!" />
          </font><br />
		  <font style='font-size:8px' face="Arial, Helvetica, sans-serif">&pound;7 
          per copy inc. last years <strong>Take Three 2007 collection</strong>(inc. P&amp;P) <br />
          (via Paypal) </font>
	</form></div><img src="../images_main/horizontal-line.gif" width="110" height="4" align='center' style='margin-top: 4px; margin-bottom: 4px' >
		
		
		<div align="center"> <p><a href="../pages_menu/defunct.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image40','','../images_main/rss-button-mo.gif',1)"><img name="Image40" border="0" src="../images_main/rss-button.gif" width="110" alt="SchNEWS RSS feed, click here for information on how to subscribe"></a><img src="../images_main/horizontal-line.gif" width="110" height="4"><BR> 
		<A HREF="/schmovies/index.php" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image46','','../images_main/schmovies-button-over.png',1)"><IMG NAME="Image46" BORDER="0" SRC="../images_main/schmovies-button.png" ALT="SchMOVIES - short films produced by SchNEWS, free to download"><BR> 
		</A><img src="../images_main/horizontal-line.gif" width="110" height="4"><a href="links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image42','','../images_main/contacts-lhc-button-mo.gif',1)"><img name="Image42" border="0" src="../images_main/contacts-lhc-button.gif" alt="Contacts listings"></a><img src="../images_main/horizontal-line.gif" width="110" height="4"><BR> 
		<A HREF="/satire/index.html" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage('Image48','','../images_main/satire-button-over.png',1)"><IMG NAME="Image48" BORDER="0" SRC="../images_main/satire-button.png" WIDTH="110" HEIGHT="40"><BR> 
		<IMG SRC="../images_main/horizontal-line.gif" WIDTH="110" HEIGHT="4" BORDER="0"></A><a href="/book-reviews/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image43','','../images_main/book-reviews-button-mo.gif',1)"><img name="Image43" border="0" src="../images_main/book-reviews-button.gif" width="110" alt="Books (and films) reviewed by SchNEWS hacks"><BR> 
		</a><img src="../images_main/horizontal-line.gif" width="110" height="4"><br> <a href="/pages_merchandise/merchandise.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image44','','../images_main/merchandise-button-mo.gif',1)"><img name="Image44" border="0" src="../images_main/merchandise-button.gif" width="110" height="45" alt="DVDs, books, t-shirts and other SchNEWS merchandise"></a><font size="2"><br /> 
		</font><img src="../images_main/merchandisearrow.gif" width="120" height="20" /><br> 
		<a href="/pages_merchandise/merchandise_tshirts.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchNEWS T-Shirts','','../images_main/t-shirts-button-over.png',1)"><img name="SchNEWS T-Shirts" border="0" src="../images_main/t-shirts-button.png" width="110" height="40"><br> 
		</a><b><a href="/pages_merchandise/merchandise_tshirts.php"></a></b>
		<p><b><a href="../pages_merchandise/merchandise_video.php#uncertified"><img src="../schmovies/images/uncertified-100.jpg" width="100" height="141" border="0" alt="Uncertified - SchMOVIES DVD Collection 2008"></a></b></p>
		<a href="schmovies/index-on-the-verge.htm"><img src="../schmovies/images/on-the-verge-100.jpg" alt="On The Verge" border="0"></a></p><p><a href="/pages_merchandise/merchandise_video.php#take3"><img src="../schmovies/images/take-three-100.jpg" width="100" height="142" border="0" alt="Take Three - the SchMOVIES DVD collection 2007"></a></p><font size="3"><b></b></font></div><font size="3"><b> 
		<p align="center"><a href="/pages_merchandise/merchandise_video.php#v"><img src="../schmovies/v-for-video-activist-100.jpg" width="100" border="0" alt="V For Video Activist - SchNEWS DVD Collection 2006"></a></p><p align="center"><b><a href="/pages_merchandise/merchandise_books.php#schnewsat10"><img src="../at10/schnewsattensmall.jpg" width="100" border="1" alt="SchNEWS At Ten - our first decade in one handy book" /></a><br /> 
		</b><br /> <a href="/pages_merchandise/merchandise.html#PDR"><img src="../images_main/Peace%20de%20Resistance%20web.jpg" width="100" height="145" border="1" alt="Peace De Resistance - SchNEWS annual 2003" /></a></p><p align="center"><a href="/pages_merchandise/stickers.html"><img src="../images_main/sticker.gif" width="100" height="70" border="0" alt="SchNEWS stickers - sold out but you can download the templates and print your own" /></a></p></b></font><FONT COLOR="#FFFFFF"><FONT COLOR="#000000"><FONT SIZE="1"><B><FONT FACE="Arial, Helvetica, sans-serif">&quot;Definitely 
		one of the best party and protest sites to come out of the UK. Updated weekly, 
		brilliantly written, bleakly humourous, and essential reading for anyone who gives 
		a shit. And we all should.&quot;</FONT></B></FONT> <FONT FACE="Arial, Helvetica, sans-serif"><B><FONT SIZE="1">- 
		Radiohead<BR /> <A HREF="/pages_merchandise/bookreviews.html">Other Reviews</A></FONT></B></FONT></FONT></FONT><font size="3"><b> 
		<p align="center">&nbsp;</p></b></font>

</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">
<br />
<h1 style='text-align:center'>The London Camp For Climate Action is on</h1>
<br />

<div style="text-align: center; padding: 20px; margin-left: 30px; margin-right: 30px; border-top: 1px solid #666666; border-bottom: 1px solid #666666; font-size: 11px; font-family:'Courier New', Courier, mono">
	We're updating this story as the Climate Camp continues, so keep checking back for the latest news...
</div>

<br /><br />

<div style='text-align:right'><b>August 26th - September 2nd.</b></div>
<br />
<p>At midday on Wednesday 26th people gathered at the meeting points before the site of the camp was announced, and the 'swoop' began. The meeting points were significant spots around London - eg outside the Bank Of England which saw the <strong>G20 protests</strong> in April and the scene of the murder by police of <strong>Ian Thomlinson</strong>; at <strong>Stockwell Tube Station</strong> where <strong>Jean Charles de Menezes</strong> was murdered; Stratford Station near the site of the 2012 Olympics; plus the offices of culpable corporations like BP, Shell and Rio Tinto.

<p>The site taken is on Blackheath, in between Shooters Hill Road and Hare and Billet Road. Immediately work began setting up marquees, tents, tripods and around 1000 were quickly on the site. There is no big day of action planned, and while there is an emphasis on building towards the COP 15 Climate Talks in Copenhagen in November, actions will take place. 

<p>For those coming bring tents and other camping gear, there will be food on the site.

<p>Public transport: Cutty Sark (DLR), Greenwich (Rail & DLR), Lewisham (Rail & DLR), Blackheath (Rail).

<p>For details and map see <a href='http://www.climatecamp.org.uk/actions/london-2009' target="_blank">www.climatecamp.org.uk/actions/london-2009</a>
<br />For updates and more info see <a href='http://london.indymedia.org' target="_blank">http://london.indymedia.org</a>

<p>SchNEWS writers will be posting additions as the camp unfolds...

<br />
<div style='text-align:right; border-top:1px solid #999999; width: 50%; margin-left: 50%; padding-top:6px; margin-top: 30px'><b>Update :: August 29th 4pm</b></div>
<br />
<p>So <strong>Climate Camp London 2009</strong> has been in full swing for four eventful and informative days.

<p>Swooping on Wednesday proved easy as pie as police kept their distance, with only one van circling the perimeter fences during the afternoon. This mood of co-operation has continued with officials respecting the police-free zone inside the camp, with daily off-site meetings between the campers and the capitalist enforcers being the only contact. The softly-softly pledge appears to be ringing true, a CCTV tower being the only real intrusion the police have made so far.

<p>SchNEWS has attended workshops on the links between economics and climate change, DSEi mobilisation, connecting local grass roots action globally, how to communicate climate science, and also found out how to save 4 tons of carbon (ditch the car and go vegan chaps!).

<p>Many more have been going on around the camp with everything from student action, building an eco village and activist guides to the law.

<p>With the bright lights of Canary Wharf in sight, campers are constantly reminded of the reason we are having these discussions, the bankers, the politicians and the corporations. Or are they one and the same..?

<p>As Climate Camp enjoys the sunshine, organise yourself to join us for this bank holiday weekend of education, networking and sustainability in action.

<br />
<div style='text-align:right; border-top:1px solid #999999; width: 50%; margin-left: 50%; padding-top:6px; margin-top: 30px'><b>Update :: August 31st 1pm</b></div>
<br />

<p>What a beautiful day!

<p>As <strong>Climate Camp 2009</strong> heads into its 6th day, the paddling pools are out and sunbathing abounds as people flit between workshops and lunch.

<p>There has been definite shift in the focus of the cam- as people are now starting to plan all the direct actions the have grown out of the information, networking and affinity group meetings that have occured over the last few days. 

<p>Vestas, DSEi, City Airport and of course the Copenhagen Climate Summit are all top of the agenda, as well as global connections being made with grass roots action groups from all over the world. There have been representatives from Africa, Canada, Ecuador, Colombia and many more gving talks at the camp.

<p>Last night the result of the online poll to choose the main target of October's Great Climate Swoop (17th-18th) was announced. Out of a choice between Drax and Ratcliffe (both coal fired power stations) Ratcliffe was the site chosen by majority vote. 

<p>Ratcliffe-On-Soar power station is one of the benchmark plants of E.On (F. Off anyone?) and one of the foremost examples E.On are using to promote the lie of 'clean' coal. For more info see wwww.climatecamp.org/actions.

<p>As SchNEWS leaves the camp this afternoon, we will take away three things from this community gathering. 

<p>Firstly, the important message of global solidarity and the need for campaigners in the West to link up with the groups that face the direct impact of climate chaos in the developing world, who are fighting just as hard, if not harder.

<p>Second, that the need for direct action is NOW! Although these camps provide a vital forum for debate and are essential to the climate movement in the UK, it is important that the mobilisation of people is not left at that.

<p>And lastly, that a sustainable community can be built in the centre of London to cater for the needs of thousands of people, purely through consensus decision making and hard work, without a police presence or intervention by the state.

<p>SchNEWS can't wait for October! 

<p>17th-18th October 2009 at Ratcliffe-On-Soar - The Great Climate Swoop. 

<p>See you there!


<br />
<br />

<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
					
				if (SHOWMESSAGES)	{
				
						addMessage(1000000000);
						echo getMessages(1000000000);
						
						$text		 =	"<div style=\"width: 80%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
						$text		.=	"<form method=\"POST\" action=\"news_climate_camp_2009.php\" >";
						$text		.=	"<b><i>Add a new comment on this story...</i></b><br /><br />";
						$text		.=	"<div align=\"right\" style=\"width:70%\">Who are you :: <input type=\"input\" name=\"author\" /></div>";
						$text		.=	"<div class='website'>Website : <input type='text' class='website' name='website' /></div>";
						$text		.=	"<textarea name=\"newComment\" style=\"width:70%; height:100px; margin-top:5px\"></textarea>";
						$text 		.=	"&nbsp;&nbsp;&nbsp;";
						$text		.=	"<input type=\"submit\" name=\"addCommentSubmit\" value=\"Add Comment\" />";
						$text		.= "</form>";
						$text		.=	"<div style=\"width: 100%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
						$text		.=	"<div style='margin-left:30px; font-size:9px; padding:3px; border-bottom:1px solid #cccccc; width: 70%'>SchNEWS reserves the right to remove any comments made by nutters or idiots...</div>";
						$text		.=	"<div style='margin-left:30px; font-size:9px; padding:3px'>No IP addresses or other identifiable information is logged with your post</div>";
						$text		.=	"<div style=\"width: 80%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
						
						echo $text;
				
				}
			
			?>
		
		</div>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />





<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />

		

			
			
			
			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B> 
			</FONT></H3>
			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>
  <P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON 
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film 
			police tried to ban - the account of the four year campaign to close down a weapons 
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p 
			(profits to Smash EDO)</P>
			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE 
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct 
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash 
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others. 
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V 
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve 
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES 
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005. 
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS 
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS 
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc 
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace 
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS 
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS 
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again 
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B> 
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS 
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS 
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P><FONT FACE="Arial, Helvetica, sans-serif"><B>(US 
			Postage &pound;6.00 for individual books, &pound;13 for above offer).</B><BR /> 
			<BR /> These books are mostly collections of 50 issues of SchNEWS from each year, 
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts, 
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year 
			round-up, containing a lot of new articles.</FONT> <P></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe 
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations 
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS 
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to 
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P><p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" /> 
			
			
			

</div>








</body>
</html>
