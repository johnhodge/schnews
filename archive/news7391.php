<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 739 - 17th September 2010 - Gauling Behaviour</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, roma, travellers, france" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 739 Articles: </b>
<b>
<p><a href="../archive/news7391.php">Gauling Behaviour</a></p>
</b>

<p><a href="../archive/news7392.php">Tesco Check-out</a></p>

<p><a href="../archive/news7393.php">Cut To The Chase: Anti-austerity Coalition Launched</a></p>

<p><a href="../archive/news7394.php">Fire To The Prisons</a></p>

<p><a href="../archive/news7395.php">Calais: Channel Hoping</a></p>

<p><a href="../archive/news7396.php">Scottish Open Cast Mining: What's Happendon?</a></p>

<p><a href="../archive/news7397.php">Fash Mobs</a></p>

<p><a href="../archive/news7398.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th September 2010 | Issue 739</b></p>

<p><a href="news739.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GAULING BEHAVIOUR</h3>

<p>
<p>
	<strong>AS THOUSANDS OF ROMA SUFFER MASS EXPULSIONS IN FRANCE..</strong> </p>
<p>
	Weak to begin with, France&rsquo;s attempts to deny that recent mass expulsions of Roma people were racist have been dealt a blow after a Ministry of Interior circular ordering evacuation of camps of Roma, as &ldquo;a matter of priority&rdquo; was leaked. </p>
<p>
	From mid-August to early September this year, approximately 1000 Roma were deported from France and 128 Roma camps dismantled. In many cases the deportations were carried out with rough policing, destruction of Roma homes and the confiscation of identity papers. It is believed that the policy could effect up to 365,000 Roma. This is not the first time - France has closed down illegal Roma camps and sent their inhabitants home for years. Last year 10,000 Roma were sent back to Romania and Bulgaria. </p>
<p>
	Only a couple of days ago, the EU&rsquo;s Justice Commissioner Viviane Reding drew comparisons between France&rsquo;s actions and the Nazis who persecuted the Roma during the Holocaust saying, &ldquo;<em>This is a situation I had thought Europe would not have to witness again after the Second World War</em>.&rdquo; </p>
<p>
	In spite of ample information proving that France has been targeting the Roma, including previous expulsions, a piece of paper was required &ndash; apparently- to add clout. Reding has now called for the EU to take a legal case against France. </p>
<p>
	So far Jose Manuel Barroso President of the European Commission has kept quiet, so it remains to be seen if the Commission will act against this gross human rights violation. <br /> 
	Doing so could have implications across Europe, as France is far from the only EU nation that has treated the Roma like they are second class. </p>
<p>
	<strong>LIFE THROUGH A CLEANSE</strong> <br /> 
	 <br /> 
	The incident which officially triggered this wave of expulsions (or ethnic cleansing) was a clash between Roma and police in the Loire valley in July. </p>
<p>
	Some Roma set upon a police station after one of their own, 22-year-old Roma Luigi Duquenet was shot dead. According to French police the shooting occured after he knocked a police officer down driving through a check-point. </p>
<p>
	As a result Mr Sarkozy ordered the expulsion of all Roma immigrants, calling for 300 illegal camps and squats to be dismantled within three months. <br /> 
	By suggesting that all Roma are criminals, France is using security as the reason to justify their actions. </p>
<p>
	The french authorities are trying to excuse themselves on the grounds that they are only enforcing conditions imposed on migration from Eastern European countries that have recently joined the EU. However, by targeting one ethnic group, France is in fact breaking international human rights law. </p>
<p>
	<strong>UNFINISHED ARTICLE</strong> </p>
<p>
	Article 18 of the Treaty on the Functioning of the European Union enshrines the right to non-discrimination, and the undisputable fact is the vast majority of immigrants that have been returned home over the past months are Roma. Singling one ethnic group out for less favourable treatment constitutes a clear case of race discrimination. </p>
<p>
	In a press release from Tuesday 14th September, the Romani Union (a Roma-led NGO based in Spain) stated that it has taken steps to execute a lawsuit of its own saying, &ldquo;<em>Collective expulsions are prohibited under European law, including in cases where such measures are targeted solely against those who have overstayed the three month residency period allowed under the Freedom of Movement Directive and have failed to register with local authorities</em>.&rdquo; </p>
<p>
	According to the Equal Rights Trust, &ldquo;The cash incentive of 300 Euro for each adult and 100 Euro for each child deported &ldquo;voluntarily&rdquo; does not change the fact due process was not followed. </p>
<p>
	* See <a href="http://www.errc.org" target="_blank">www.errc.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(936), 108); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(936);

				if (SHOWMESSAGES)	{

						addMessage(936);
						echo getMessages(936);
						echo showAddMessage(936);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


