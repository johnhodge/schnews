<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 794 - 28th October 2011 - Dale Farm: Nomads Land</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 794 Articles: </b>
<p><a href="../archive/news7941.php">Squatting: Empty Premises</a></p>

<p><a href="../archive/news7942.php">Occupy London: Loose Canon Fired</a></p>

<p><a href="../archive/news7943.php">Run Of The Millbank</a></p>

<p><a href="../archive/news7944.php">Cuadrilla Thriller</a></p>

<p><a href="../archive/news7945.php">Wood You Believe It</a></p>

<b>
<p><a href="../archive/news7946.php">Dale Farm: Nomads Land</a></p>
</b>

<p><a href="../archive/news7947.php">Edl: Brum Fash Clash</a></p>

<p><a href="../archive/news7948.php">Poor's Pay Reigned In</a></p>

<p><a href="../archive/news7949.php">I Am Sparktacus</a></p>

<p><a href="../archive/news79410.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th October 2011 | Issue 794</b></p>

<p><a href="news794.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DALE FARM: NOMADS LAND</h3>

<p>
<p>  
  	Last Thursday (20th) after a month long struggle of battles and barricades, the Dale Farm residents and Camp Constant supporters walked off the site, leaving their homes to the bailiffs&rsquo; tools of destruction. Resident Mary Sheridan said, &ldquo;Leaving with supporters today is about our own dignity and our appreciation of the support we&rsquo;ve received. We&rsquo;re leaving together as one family, and we are proud of that- you can&rsquo;t take away our dignity&rdquo;.  </p>  
   <p>  
  	In the aftermath, questions continue to fly at Tory Councillor Tony Ball and local MP John Baron as the issues of where the travellers go now take priority. Out of the links forged between supporters and travellers at Dale Farm, a new collective, the Traveller Solidarity Network has been formed. The group &lsquo;aims to provide solidarity with Travellers, Roma and Gypsies in fighting ethnicity and class based discrimination and persecution&rsquo; on a national scale.  </p>  
   <p>  
  	Despite the main eviction have taken place, the carnage doesn&rsquo;t stop. One remaining resident sent this in to SchNEWS:  </p>  
   <p>  
  	&ldquo;<em>I am writing from Dale Farm, I am a legal observer and a traveller. Since returning here today I have been shocked how much demolition and destruction Constant &amp; Co have been able to do. </em>  </p>  
   <p>  
  	<em>They have fenced off the entire site and created a mini Palestine. There are a few of us on the ground who are legal observers but we need more, we also need photographers and film makers. The bailiffs are being racist to the travellers and there are a number of female and elderly travellers who were injured by the police during their attack on site. One woman who was on the original Dale Farm house plot and who is allowed to remain here in her property was assaulted by the police after a number of them broke through the brick wall and then proceeded to kick her in the back. She has a fractured spine and a chip in one vertebrae. </em>  </p>  
   <p>  
  	<em>Please, there are a lot of traumatised people here, can you ask your friends and family to show some support to the travellers. If we are not able to resist then the least we should do is bear witness to the start of the ethnic cleansing that is happening to travellers in Basildon</em>.&rdquo;  </p>  
   <p>  
  	Next Friday (5th) a meeting has been called by the Traveller Solidarity Network to discuss and debate the issues that arose at Dale Farm and which face traveller communities all over Britain.  </p>  
   <p>  
  	Meet at Cityside House, 40 Adler St, London E1 1EE. RSVP to <a href="mailto:savedalefarm@gmail.com">savedalefarm@gmail.com</a> to attend.  </p>  
   <p>  
  	*dalefarm.wordpress.com  </p>  
   <p>  
  	*travellersolidarity.org  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1407), 163); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1407);

				if (SHOWMESSAGES)	{

						addMessage(1407);
						echo getMessages(1407);
						echo showAddMessage(1407);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


