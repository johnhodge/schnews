<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 759 - 18th February 2011 - SHAC To The Lab Again</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, shac, hls, animal rights" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 759 Articles: </b>
<p><a href="../archive/news7591.php">Middle Eastern Promise</a></p>

<p><a href="../archive/news7592.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7593.php">Commission Accomplished</a></p>

<b>
<p><a href="../archive/news7594.php">Shac To The Lab Again</a></p>
</b>

<p><a href="../archive/news7595.php">Supermarket Sabo-taj</a></p>

<p><a href="../archive/news7596.php">Cheesed Off</a></p>

<p><a href="../archive/news7597.php">Bunga Jump</a></p>

<p><a href="../archive/news7598.php">Shire Madness</a></p>

<p><a href="../archive/news7599.php">Dale Farm Callout</a></p>

<p><a href="../archive/news75910.php">Commie Garden</a></p>

<p><a href="../archive/news75911.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th February 2011 | Issue 759</b></p>

<p><a href="news759.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SHAC TO THE LAB AGAIN</h3>

<p>
<p>
	Twelve years later, after injunctions, prison sentences and a massive campaign of state orchestrated vilification in the mainstream media, the SHAC story returns to it beginnings - the reason for it all: the senseless abuse of captive animals. </p>
<p>
	Previously unseen research papers, anonymously sent to the Stop Huntingdon Animal Cruelty (SHAC) campaign at the start of 2011, have shed new light on experiments carried out by Huntingdon Life Sciences (HLS) </p>
<p>
	The research papers detail invasive experiments which took place on rats, mice, rabbits and rhesus monkeys at HLS&rsquo; Cambridgeshire and New Jersey laboratories between 2001 and 2010. Substances tested included petrol, grape seed extract, a toxic compound PCB, Botox, soybean fibre, and paraffin wax. There were also several duplicate tests for a food additive known as PVA and experiments to test a fragrance that occurs naturally in plants, which is used in cosmetics and perfumes. All the experiments involved lengthy distressing experiences for the animals before they were killed and dissected. In one test pregnant rats were forced to breathe vapourised petrol for up to six hours a day to examine the effects on their foetuses. </p>
<p>
	A summary of all 11 shocking documents can be found in &lsquo;HLS Unmasked&rsquo;, which can be downloaded at <a href="http://www.shac.net/HLS/HLS_Unmasked2011.pdf" target="_blank">www.shac.net/HLS/HLS_Unmasked2011.pdf</a> </p>
<p>
	All the substances tested had already been tested on animals before. All of them are already used by humans around the globe on a daily basis, except for the PCB which was banned in 1979 because of its known toxic effects. The majority of the substances tested, such as Botox, have nothing to do with &lsquo;life saving research&rsquo; and are used mainly in cosmetic products. </p>
<p>
	HLS&rsquo;s own documents point out the ludicrous nature of animal testing. One document involving the use of rats to test a naturally-occurring scent known as &lsquo;coumarin&rsquo; concludes rather frankly that, &ldquo;<em>the rat is a very poor model for humans, and toxicity in the rat cannot be extrapolated to humans</em>.&rdquo; In another experiment, in which rats are force fed paraffin wax, the conclusion states that the results proved to be &ldquo;<em>of questionable relevance for human safety evaluation</em>&rdquo;. Of course none of this stops HLS from using rats as their main experimental animal. </p>
<p>
	Debbie Vincent of SHAC said, &ldquo;<em>these new research papers reveal, once again, the true horror and idiocy of this failing laboratory. When they are not getting exposed for gross misconduct or severe animal welfare breaches, they are thrust into the spotlight for testing bizarre and useless products on animals &ndash; products which we all use already on a daily basis. While cosmetic testing is supposedly banned in the UK, here we see products which are largely used in cosmetics still being tested on vast numbers of animals at HLS. We strongly condemn all experimentation on animals, but this blatant abuse of a gaping legal loophole regarding cosmetic testing is absolutely not acceptable. HLS must finally be made accountable for their cruel and unnecessary experiments which are taking place on a regular basis behind locked doors</em>.&rdquo; </p>
<p>
	Despite the massive state harassment the SHAC campaign is still going strong - find out more at <a href="http://www.shac.net" target="_blank">www.shac.net</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1107), 128); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1107);

				if (SHOWMESSAGES)	{

						addMessage(1107);
						echo getMessages(1107);
						echo showAddMessage(1107);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


