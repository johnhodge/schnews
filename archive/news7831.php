<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 783 - 12th August 2011 - The Big Smoke</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, riots, london, police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 783 Articles: </b>
<b>
<p><a href="../archive/news7831.php">The Big Smoke</a></p>
</b>

<p><a href="../archive/news7832.php">Charities Shopped</a></p>

<p><a href="../archive/news7833.php">R.i.p. Gary Ds </a></p>

<p><a href="../archive/news7834.php">Unlucky Seven</a></p>

<p><a href="../archive/news7835.php">Uae Having A Laugh</a></p>

<p><a href="../archive/news7836.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th August 2011 | Issue 783</b></p>

<p><a href="news783.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE BIG SMOKE</h3>

<p>
<p>  
  	<strong>AS SCHNEWS ASKS WHY IS LONDON BURNING?</strong>  </p>  
   <p>  
  	Across the entire media spectrum the opinions are now flying thicker and faster than the half-bricks were just two nights ago. So here&rsquo;s SchNEWS&rsquo; half-assed crack at explaining the recent turmoil on the streets of the U.K.  </p>  
   <p>  
  	The recent riots are far too complex for a simple explanation (Maybe it&rsquo;s just something about Tory governments and Royal Weddings). The facts aren&rsquo;t all in yet by any means. The riots certainly weren&rsquo;t &lsquo;pure criminality&rsquo; but neither were they anything like a politicised insurrection.  </p>  
   <p>  
  	Riots in different parts of the country and even different parts of the capital each had their own flavour. If they had one thing in common it was the fact that they didn&rsquo;t happen in the estates &ndash; the fight was taken to the High St. Hatred of the cops and the possibility of free stuff on a long summer&rsquo;s evening proved to be an intoxicating mixture. As an aside &ndash; both the footage and the roll-call of those in court right now show &ndash; this was a long way from race riot - black and white can uniteunite &ndash; if only to loot Foot Locker.  </p>  
   <p>  
  	It seems to us that everyone is asking the wrong question &ndash; why did they riot? When in fact the more obvious question would be &ndash; why did it take &lsquo;til now? A toxic combination of neo-liberalism and the gangsterism that goes with the criminalisation of drugs has created ghettoes in the London and throughout the U.K. Everyone knows it &ndash; and the &lsquo;feral underclass&rsquo; has become the acceptable focus of snobbery, hate and fear.  </p>  
   <p>  
  	Through the boom-time, all political parties supported the same solution &ndash; a benefits system that would keep the poor out of good people&rsquo;s faces. Subsistence cash to stop them bothering the rest of the population as they grabbed their little piles of&nbsp; buy-to-let loot, their own little capitalist empires. Much was made of catering for &lsquo;stakeholders&rsquo; in society, the underlying assumption being that those who didn&rsquo;t hold a stake could safely be fenced in with ASBOs, CCTV, youth curfews, increased police powers and a prison building program. Nobody wanted to know what was going on inside these areas.  </p>  
   <p>  
  	The middle class knew they were being screwed when they had to bail out the bankers but they also knew that any real justice might dismantle a lifetime&rsquo;s savings. En masse they voted Tory and made the decision to bonfire the benefits system in a massive transfer of wealth from the chavs to the chav-nots. This is what lies behind the right-wing&rsquo;s blanket refusal to even consider what might have caused the uprising &ndash; preferring to dismiss it as &lsquo;mindless&rsquo; amid the predictable calls for water cannons and the immediate return of national service.  </p>  
   <p>  
  	<strong>SHOE&rsquo;S ON THE OTHER FOOT</strong>  </p>  
   <p>  
  	Now they&rsquo;re reaping the whirlwind of structuring society entirely around&nbsp; people bought into a system where acquisition of material wealth was the only legitimate aspiration. Massive disparities in income and status developed and in a city like London the different social strata exist cheek-by-jowl. Ghettoes are colonised by wealthy enclaves. A short wander around the streets of Stoke Newington or Hackney shows how the process of gentrification rubs the noses of the poor in a wealth and security they will never have. The current recession has made it clear to those not already safely a rung or two up the property ladder that they will never make it.  </p>  
   <p>  
  	A blind eye has been turned to the corrosive affect of advertising on kids &ndash; the insidious viral marketing scams of the very brands whose stores we&rsquo;ve seen go up in flames. A list of the targets of the London riots reads like a list of daytime TV adverts- Curry&rsquo;s, Matalan, JD Sports, Aldi, Footlocker and PC World. The initial spark for the riots was the killing of Mark Duggan. But once it was clear that the police were no longer in control of events youth squared the circle of being born in a consumer culture without any cash. They&rsquo;ve been outside the mall with their noses pressed against the glass for a long time. Now the window&rsquo;s shattered and they&rsquo;ve scarpered with the lot.  </p>  
   <p>  
  	Back to the spark that began it all - the death of Mark Duggan &ndash; shot by a police armed response unit in Tottenham on Thursday 4th August. This was initially reported&nbsp; in the mainstream media as an exchange of fire, in fact it was reported as a positive news story - with prominence given to the angle of a police officer being miraculously saved from a bullet that struck his radio. After four days of rioting the facts are slowly coming out. Duggan&rsquo;s gun (if he had one) was never fired. As with Tomlinson, as with De Menezes first the police kill and then they start to lie. Those lies were repeated as fact by a compliant mass media.  </p>  
   <p>  
  	A demonstration of family and friends gathered outside Tottenham police station on Saturday (the family were not given access to Duggan&rsquo;s body until 36 hours after his death). The situation turned violent &ndash; footage has emerged of a group of tooled up cops beating a young girl on the ground. That&rsquo;s when it all kicked off and that night Tottenham went up in flames.  </p>  
   <p>  
  	What&rsquo;s missing from the picture presented by most media analysts is what a bunch of c*nts the police actually are. As anarchist political activists we have a choice about our engagement with the police, we run up against them when we mobilise and so we get a tiny glimpse of what it&rsquo;s like to live in an area where everyone is treated as potential criminal. Check out the youtube video of Darcus Howe pointing out the humiliation of constant stop&amp;search on young men, while virtually being shouted down by a BBC interviewer Fiona Armstrong who doesn&rsquo;t sound as if she&rsquo;s too well acquainted with the grim realities of inner city life <a href="http://www.youtube.com/watch?v=mzDQCT0AJcw" target="_blank">http://www.youtube.com/watch?v=mzDQCT0AJcw</a>  </p>  
   <p>  
  	Then the rioting went viral &ndash; not just because of the intoxicating power of Twitter or Blackberry but because the conditions for it exist all over the country. Currently the Tory government is talking tough, with Boris Johnson talking of rioters as if they were foxes to be run to earth. Cameron promising new laws and threatening a New Moral Army, almost as if he hadn&rsquo;t been caught glad-handing people who hack into dead girls phone messages in the pursuit of profit. As they continue to push forward the austerity agenda SchNEWS asks - is this only the beginning?  </p>  
   <p>  
  	<span style="font-family:times new roman,times,serif;"><strong>EDL OPPORTUNISM</strong></span>  </p>  
   <p>  
  	<span style="font-family:times new roman,times,serif;">Our it&rsquo;ll be all white on the night mates seem to forgotten to look up vigilantism on Wikipedia before they set off on their pub crawl in Enfield on Wednesday. By the way lads, the basic idea is to go where there is crime and then prevent it &ndash; not to go somewhere peaceful, start making monkey noises at the black folks and then throw bottles at the cops.</span>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1319), 152); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1319);

				if (SHOWMESSAGES)	{

						addMessage(1319);
						echo getMessages(1319);
						echo showAddMessage(1319);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


