<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 689 - 4th September 2009 - Dsei-ing With Death</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dsei, arms trade, anti-war, gaza, barclays, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.rts.gn.apc.org/"><img 
						src="../images_main/rtf-05-banner.jpg"
						alt="Reclaim the Future"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 689 Articles: </b>
<p><a href="../archive/news6891.php">Common People?</a></p>

<p><a href="../archive/news6892.php">Factory Finish</a></p>

<p><a href="../archive/news6893.php">Herculean Task</a></p>

<p><a href="../archive/news6894.php">Circus Freaks</a></p>

<p><a href="../archive/news6895.php">Hill Fought</a></p>

<p><a href="../archive/news6896.php">Inside Schnews</a></p>

<b>
<p><a href="../archive/news6897.php">Dsei-ing With Death</a></p>
</b>

<p><a href="../archive/news6898.php">Brum Punch</a></p>

<p><a href="../archive/news6899.php">Arm-a-geddon Myself Some Of That</a></p>

<p><a href="../archive/news68910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th September 2009 | Issue 689</b></p>

<p><a href="news689.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DSEI-ING WITH DEATH</h3>

 <br /> It&rsquo;s that time of year again when delegates from 40 different countries, including some of the world&rsquo;s leading human rights abusers, come to view the wares of 1352 exhibitors from around the world at the biennial  <strong>Defence Systems Equipment International (DSEi)</strong> &ndash; the world&rsquo;s largest arms fair. <br />  <br /> Over a million people have died since the US/UK invasion of Iraq. 1,400 Palestinian civilians were killed during the Israeli attack on Gaza in January 2009.  The exhibitors at DSEi are the very people who supplied the weapons that made January&rsquo;s massacre possible. They are making huge profits from these conflicts &ndash; the devastation in Gaza was facilitated by UK arms sales of over &pound;27 million in 2008. <br />  <br /> Disarm DSEi have been mobilising against the biannual fair since it moved to Docklands in 1999. On September 11th 2001, hundreds of protesters gathered for a &lsquo;Fiesta of Life&rsquo; outside DSEi as the twin towers fell (see <a href='../archive/news322.htm'>SchNEWS 322</a>). In 2003, activists locked on to DLR trains forcing delegates to travel by bus to the eXcel centre (<a href='../archive/news422.htm'>SchNEWS 422</a>). In 2005, a mass demo marched on the exhibition while blockades stopped traffic (<a href='../archive/news511.htm'>SchNEWS 511</a>) and in 2007 protesters invaded the concourse of eXcel during DSEi&rsquo;s press photo call (<a href='../archive/news602.htm'>SchNEWS 602</a>). However, the Met have rolled out some of their most repressive policing against anti-DSEi protests. In 2005 police used section 44 of the Terrorism Act to harrass and search activists bound for the fair, in 2007 the Met besieged the convergence centre and made dozens of spurious arrests.  <br />  <br /> It&rsquo;s incredibly easy for the police to lock down the area around eXcel and contain activists. That&rsquo;s why this year, on Tuesday September 8th, Disarm DSEi are taking their protest to The City of London, the financial heart of the global arms trade.  <br /> And it&rsquo;s likely that The Met, fresh from their propaganda offensive at the Camp for Climate Action, won&rsquo;t want a rerun of the G20 scuppering their new community policing image. <br /> &nbsp; <br /> The target of Tuesday&rsquo;s mass action will be the banks and investors financing DSEi and the international arms trade. Barclays is the largest UK investor in the global arms trade with &pound;7.3 billion worth of shares in the international arms trade. Barclays also financed the purchase of Clarion, the owners of the DSEi arms fair, by investment company VSS. RBS is the biggest financier of the global arms trade having loaned &pound;44.6 billion in the last ten years. <br />   <br /> Disarm DSEi are calling for people to join together to resist these institutions, expose the devastation they cause, and hold them to account for their complicity in the ethnic cleansing and genocide facilitated by the global arms trade: <br />  <br /> <strong>* Tuesday 8th September &ndash; Battle the Banks, Annihilate the investors, Disarm DSEi </strong>&ndash; 12 noon, City of London &ndash; Meeting place to be announced on Saturday 5th September on <a href="http://www.dsei.org" target="_blank">www.dsei.org</a> and <a href="http://www.indymedia.org.uk." target="_blank">www.indymedia.org.uk.</a> Updates on twitter (<a href="mailto:@dsei.org">@dsei.org</a>) <br />  <br /> Disarm DSEi are also calling for groups around the UK and beyond to take action against the exhibitors at DSEi. A map has been created showing thousands of company premises across the UK. Type your postcode into <a href="http://www.dsei.org/exhibitor-map" target="_blank">http://www.dsei.org/exhibitor-map</a> to find exhibitors in your area. <br />  <br /> <strong><font face="courier new"><font size="4">More anti DSEi events:</font></font></strong><font face="courier new"> <br /> </font><font face="courier new"> </font><font face="courier new"> <br /> </font><font face="courier new"><strong><font size="3">7th September:</font></strong>  <br />  <br /> <strong>UK Defence Conference</strong>, QE2 Centre, Parliament Square. Noise demo to meet and greet the arms dealers from 8am (this conference is being organised by ITT, owners of EDO MBM &ndash; see <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>) </font><font face="courier new"> <br /> </font><font face="courier new"> <br /> </font><font face="courier new"><strong>Fourth Plinth Flash Mob</strong> against the the arms fair. &ldquo;Never mind the art. Disarm the arms trade!&rdquo; Meet 8:40am, St-Martin-in-the-Field chruch steps. Wear black and something you don&rsquo;t mind getting dusty!  <br />  <br /> <strong>Workshops at convergence space</strong>, including legal briefing and public order policing information. See <a href="http://www.dsei.org" target="_blank">www.dsei.org</a> for location.  <br />  <br /> <strong>Public meeting, 6pm</strong>, at convergence space for 8th September action in City of London ** East London Against the Arms Trade (ELAAF) silent candlelit vigil for the victims of the arms trade. 6:30pm at Royal Victoria DLR station.&nbsp; <br /> </font><font face="courier new"> <br /> </font><font face="courier new"><strong><font size="3">8th September:</font></strong>  <br />  <br /> <strong>12 noon, mass protest against investors in the arms trade</strong> and DSEi in the City of London &ndash; Location to be announced 5th September. ** End the DSEi arms fair, Close UKTI DSO. 12 noon at the Excel Centre, then 1pm at UK Trade &amp; Investment. Organised by Campaign Against Arms Trade. <br />  </font><font face="courier new"> <br /> </font><font face="courier new"><strong><font size="3">9th September:</font></strong>  <br />  <br /> </font></span><span lang="EN-GB"><font face="courier new">Automonous actions against the arms fair.</font></span><span lang="EN-GB"><font face="courier new"> <br /> </font><font face="courier new"> <br /> </font><font face="courier new"><strong><font size="3">10th September:</font></strong>  <br />  <br /> <strong>Action against the DSEi exhibitors dinner</strong>, Central London. Details to be released at <a href="http://www.dsei.org." target="_blank">www.dsei.org.</a> <br />  </font><font face="courier new"> <br /> </font><font face="courier new"><strong><font size="3">11th September:</font></strong>  <br />  <br /> <strong>ELAAF memorial procession.</strong> Meet at junction of Victoria Dock Road and Freemasons Road, 2pm.</font><font face="courier new"> <br /> </font></span> <br />  <br />   <font face="courier new"> <br /> </font>
</p>

<div style='text-align:center'>
<div style="font-weight:bold; font-size: 14px; border-top: 1px solid #888888; padding-top:10px">DSEI Action Map</div><br />
<div style='padding:5px'><a href='../images/689-dsei_city_map_a3.jpg' target="_blank">Download as Ultra-High-Res (7016px x 4961px)</a></div>
<div style='padding:5px'><a href='../images/689-dsei_city_map_1200.jpg' target="_blank">Download as High-Res (1200px x 849px)</a></div>
<img align="center" src="../images/689-dsei_city_map_sm.jpg" />
</div>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-war&source=689">anti-war</a>, <a href="../keywordSearch/?keyword=arms+trade&source=689">arms trade</a>, <a href="../keywordSearch/?keyword=barclays&source=689">barclays</a>, <a href="../keywordSearch/?keyword=direct+action&source=689">direct action</a>, <a href="../keywordSearch/?keyword=dsei&source=689">dsei</a>, <a href="../keywordSearch/?keyword=gaza&source=689">gaza</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(480);
			
				if (SHOWMESSAGES)	{
				
						addMessage(480);
						echo getMessages(480);
						echo showAddMessage(480);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>