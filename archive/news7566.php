<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 756 - 28th January 2011 - My Big Fat Gypsy Eviction</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dale farm, gypsy, roma, travellers, essex" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 756 Articles: </b>
<p><a href="../archive/news7561.php">Mubarak's Against The Wall</a></p>

<p><a href="../archive/news7562.php">A Funny Thing Happened On The Way To The Forum</a></p>

<p><a href="../archive/news7563.php">Under The Covers Cops</a></p>

<p><a href="../archive/news7564.php">A Cut Above</a></p>

<p><a href="../archive/news7565.php">Bristol: Branching Out</a></p>

<b>
<p><a href="../archive/news7566.php">My Big Fat Gypsy Eviction</a></p>
</b>

<p><a href="../archive/news7567.php">Rossport At The Ready</a></p>

<p><a href="../archive/news7568.php">Inside Schnews</a></p>

<p><a href="../archive/news7569.php">A Roamer Therapy</a></p>

<p><a href="../archive/news75610.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th January 2011 | Issue 756</b></p>

<p><a href="news756.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MY BIG FAT GYPSY EVICTION</h3>

<p>
<p>
	The UK&rsquo;s largest traveller site, Dale Farm, has been bracing itself for eviction as the deadline for the full-scale ethnic cleansing of the site by Tory Basildon council draws closer. Prepared to fight to the bitter end, representatives from Dale Farm and the Gypsy Council met with Billericay MP John Baron and Basildon council head Tony Ball for a crisis meeting at Portcullis House on Thursday (27th). The meeting ended with a tiny shuffle in the right direction (the council have rejected or refused to consider countless other previous planning applications); the council agreed to consider a planning application for one caravan park in the area to house some of the Dale Farm residents, but only on the condition that the group looked for another site outside the borough at the same time. </p>
<p>
	However, John Baron maintained that there was still &lsquo;the political will and the timetable&rsquo; to evict the site in the spring if the negotiations failed. This would end up costing the taxpayer around &pound;13million to pay an army of riot pigs to get the job done. A follow-up meeting between council authorities and community leaders will take place on March 1st. </p>
<p>
	After the the meeting there was a rally in Russell Square, London, to highlight the plight of another Essex-based traveller family who have been continually harassed by an anti-traveller &lsquo;vigilante camp&rsquo; set up on the entrance to their land since last year. At around 7pm, the rally headed off to join others at a Holocaust commemoration and a Dale Farm info night with films and speakers. </p>
<p>
	The Holocaust memorial, commemorating the Nazi genocide, which saw half a million Roma murdered across Europe, is a reminder of what can happen when we fail to stand up to establishment violence. </p>
<p>
	Dale Farm Solidarity is organising human rights monitors, legal and arrestee support, action training, media and outreach to be as prepared as possible for the worst. They have issued an call-out for anyone who wants to get down on site and help with preparations in two open workdays - on Saturday 12th &amp; 19th February. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1082), 125); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1082);

				if (SHOWMESSAGES)	{

						addMessage(1082);
						echo getMessages(1082);
						echo showAddMessage(1082);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


