<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 792 - 14th October 2011 - Honduran-ce Test</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 792 Articles: </b>
<p><a href="../archive/news7921.php">False Profits</a></p>

<p><a href="../archive/news7922.php">Greece Tightening</a></p>

<b>
<p><a href="../archive/news7923.php">Honduran-ce Test</a></p>
</b>

<p><a href="../archive/news7924.php">Far Right Angels</a></p>

<p><a href="../archive/news7925.php">A Healthy Turnout</a></p>

<p><a href="../archive/news7926.php">Dale Farm Update</a></p>

<p><a href="../archive/news7927.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th October 2011 | Issue 792</b></p>

<p><a href="news792.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HONDURAN-CE TEST</h3>

<p>
<p>  
  	Continuing the long tradition of US presidents propping up illegitimate, undemocratic and brutal Latin American leaders, Obama last week hailed the return of a &ldquo;strong commitment to democracy&rdquo; in Honduras. <br />  
  	 <br />  
  	He was speaking during the first White House visit of Honduran leader Porfirio Lobo, who was there to discuss economic and security issues. However, proceedings were dominated by Honduras&#39; recent return to the Organisation of American States (OAS). They were kicked out following the coup two years ago which led to far-right Lobo being installed (see <a href='../archive/news708.htm'>SchNEWS 708</a>). The country has become a haven for rich businessmen, out to make a killing with the help of violent paramillitary forces, who also assist with the ongoing brutal repression of any political opposition. <br />  
  	 <br />  
  	In June 2009, soldiers acting under orders of the Honduran Supreme Court pulled then president Manuel Zelaya &ndash; of the centre-left Liberal Party - from his bed early one Sunday morning and dumped him, in his pj&#39;s, on a plane to Costa Rica. Head of Congress, Roberto Micheletti, was named as acting president and elections were held in November of the same year (Zelaya&#39;s term was due to end in January 2010). A US-led bid to restore Zelaya to office failed, and many Latin American governments vowed to shun the interim administration and any successor. <br />  
  	 <br />  
  	Since this time, Honduras has been named the most dangerous place in the world to be a journalist and political activists have been murdered with impunity for their involvement in opposition movements. President Lobo&#39;s electoral victory was illegitimate: many people abstained in protest at the coup, and those casting votes did so amid widespread corruption and military intimidation. <br />  
  	 <br />  
  	Depending on which side of the fence you sit the reasoning behind Zelaya&#39;s ousting varies wildly. Apologists on the right, largely with ties to Honduran capitalists and the desire to attract foreign multi-nationals, claim it was because of Zelaya&#39;s attempts to rewrite the constitution and allow presidents to run for re-election. Whether this was the catalyst or not, it should also be recognised this was not a gross abuse of executive power and any such rewriting was to be decided by popular vote at a referendum. <br />  
  	 <br />  
  	Others paint a more sinister picture. While a member of the Liberal Party, once elected Zelaya increasingly pursued left-wing policies and resisted neoliberal advances such as privatisation. Only a few weeks before the coup he had agreed to grant land titles to campesinos in the plush Aguan Valley, northern Honduras; an area controlled by three land-owning oligarchs who grow African oil palms for biofuel production. The region is a crucible of violence as powerful elites kill anyone who dare stand in the way of their profits. <br />  
  	 <br />  
  	Aguan&#39;s chief oligarch, and the richest man in Honduras, is Miguel Facuss&eacute;, a murderous zealot who owns vertically integrated agribusiness Dinant Corporation. He was a big supporter of the coup and strongly opposes any land reform in the Aguan region. Denied the titles promised by Zelaya, campesinos have began to occupy and work the land &ndash; needed even more now the cost of basic food has doubled since the coup. In response, Facuss&eacute; has ordered his private paramilitary force to run riot. <br />  
  	 <br />  
  	The latest assassination came on Tuesday 11th October, when 33-year-old Santos Seferino Zelaya Ruiz, a member of the Unified Campesino Movement of Aguan (MUCA), was gunned down by six masked security guards as he worked land bordering a plantation owned by Facuss&eacute;. But it&#39;s not only bullets people fear; when they aren&#39;t been killed, politically active campesinos are captured and tortured. <br />  
  	 <br />  
  	According to human rights groups, between January 2010 and early October 2011 around 40 people involved in peasant organisations in the Aguan Valley have been murdered. This has been carried out with the tacit approval of the Honduran military, who at one point occupied the Aguan Valley National Land Reform Institute for two months in a blatant show of intimidation and disagreement with progressive redistribution. <br />  
  	 <br />  
  	Elsewhere in the country, teachers and students have joined forces in protest against attempts to privatise the Honduran education system. Their actions have been met with a volley of tear gas and batons; one teacher, 59-year-old Ilse Ivania Vel&aacute;squez Rodr&iacute;guez, was killed after being shot in the head by a tear gas canister at point-blank range. Around 305 teachers were suspended for up to six months and, when nascent negotiations broke down, the government threatened to suspend a further five thousand. <br />  
  	 <br />  
  	In response, grass-roots movements have formed the National Popular Resistance Front &ndash; which is unified in it&#39;s call for a new constitution to replace the one written in 1982 under the US-backed military dictatorship of Policarpo Paz Garcia. In a relatively short time they have formed regional assemblies and elected delegates who headed to the first national meeting earlier this year. Unfortunately, it seems many more innocent people are set to die before the Honduran government begin to listen... if at all. <br />  
  	 <br />  
  <a href="	http://upsidedownworld.org/main/honduras-archives-46/1930-honduras-president-overthrown-in-military-coup" target="_blank">	http://upsidedownworld.org/main/honduras-archives-46/1930-honduras-president-overthrown-in-military-coup</a> <br />  
  	 <br />  
  <a href="	http://www.thenation.com/article/160472/honduras-teargassed-open-business" target="_blank">	http://www.thenation.com/article/160472/honduras-teargassed-open-business</a> <br />  
  	 <br />  
  <a href="	http://hondurasresists.blogspot.com/" target="_blank">	http://hondurasresists.blogspot.com/</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1389), 161); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1389);

				if (SHOWMESSAGES)	{

						addMessage(1389);
						echo getMessages(1389);
						echo showAddMessage(1389);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


