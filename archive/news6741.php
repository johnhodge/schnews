<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 674 - 1st May 2009 - May The Fourth Be With You</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, edo-mbm, mayday, brighton, paul hills, anti-military gathering, fitwatch, critical mass, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 674 Articles: </b>
<b>
<p><a href="../archive/news6741.php">May The Fourth Be With You</a></p>
</b>

<p><a href="../archive/news6742.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6743.php">Teenage Kicks</a></p>

<p><a href="../archive/news6744.php">End Of The Road</a></p>

<p><a href="../archive/news6745.php">Gorky Spark</a></p>

<p><a href="../archive/news6746.php">Mexican Pay-off</a></p>

<p><a href="../archive/news6747.php">Troupes Out</a></p>

<p><a href="../archive/news6748.php">Schnews In Brief</a></p>

<p><a href="../archive/news6749.php">Running The Gauntlet</a></p>

<p><a href="../archive/news67410.php">Well Hung</a></p>

<p><a href="../archive/news67411.php">Fish Out Of Water</a></p>

<p><a href="../archive/news67412.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/674-swine-flu-lg.jpg" target="_blank">
													<img src="../images/674-swine-flu-sm.jpg" alt="Sorry Sarge, no kissing today - I'm worried about swine flu." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 1st May 2009 | Issue 674</b></p>

<p><a href="news674.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MAY THE FOURTH BE WITH YOU</h3>

<p>
<p><strong>As Brighton Gears Up For Smash EDO Mayday Mayhem! <br /> </strong> <br /> Things are hotting up ahead of Brighton&rsquo;s promised Mayday Mayhem this long weekend - in fact it&rsquo;s forecast for sun, sun, sun down in the City-by-the-Sea for &lsquo;Bank Holiday&rsquo; Monday.  <br />  <br /> Eagerly grabbing onto the coat-tails of the media&rsquo;s new-found obsession with police brutality, the Smash EDO campaign seems to have Brighton in a bit of a stir. Local rag the Evening Anus has had two EDO related front pages this week and Sussex Police have been reduced to pleading on Indymedia for organisers to come forward. The Telegraph sounded an alarmist note on Saturday with &ldquo;<strong>Repeat of G20 violence feared as protesters plan to bring Brighton to a standstill</strong>.&rdquo;  <br />  <br /> EDO have broken their five-year silence in an effort to undermine the campaign. According to Paul Hills, EDO&rsquo;s M.D. &ldquo;<em>I strongly object to the use of emotive language such as calling us an &lsquo;arms factory&rsquo; and a &lsquo;bomb factory&rsquo;. It&rsquo;s very annoying and it&rsquo;s simply not true</em>.&rdquo; In fact according to the suddenly talkative Paul, &ldquo;<em>We make things that ensure the safe carriage and release of weapons from aircraft</em>.&rdquo; Hmm. Here at SchNEWS we were puzzled about what was safe about the release of weapons from great heights on to populated areas... <br /> <strong> <br />  <br />  <br />  <br />  <br /> The Latest info on May Day from Smash EDO <br /> </strong> <br /> The meeting point will not be released until very shortly before the demo starts. For location and updates call the Mayday! Mayday! infoline 07506705509. A few more infoline numbers might be added in case they are needed so keep checking <a href="http://www.smashedo.org.uk/mayday-09.htm" target="_blank">www.smashedo.org.uk/mayday-09.htm</a> <br />  <br /> If you want a text message giving you the meet point text 'update' to 07983084019  or email it to <a href="mailto:smashedo@riseup.net">smashedo@riseup.net</a> <br /> <strong> <br /> Local Groups Coming to Mayday</strong>  <br />  <br /> These groups are arranging transport from around the country. Contact them if you&rsquo;d like to travel with someone from your local area: <br />  <br /> * <strong>Hereford</strong> - <a href="mailto:hereford@afed.org.uk">hereford@afed.org.uk</a> <br /> * <strong>Sheffield</strong> - <a href="mailto:sheffield@af-north.org">sheffield@af-north.org</a> <br /> * <strong>Bath</strong> - <a href="mailto:bathactivistnet@yahoo.com">bathactivistnet@yahoo.com</a> <br /> * <strong>Cardiff</strong> - <a href="mailto:cardiffanarchists@riseup.net">cardiffanarchists@riseup.net</a> <br /> * <strong>London</strong> &ndash; see <a href="http://www.indymedia.org.uk/en/2009/04/428512.html" target="_blank">www.indymedia.org.uk/en/2009/04/428512.html</a> <br /> <strong> <br /> Accommodation <br /> </strong> <br /> If you need accommodation for either Mayday or the Anti-Militarist Gathering email us at <a href="mailto:smashedo@riseup.net">smashedo@riseup.net</a> or call 07706 689722 (limited spaces available)  <br /> <strong> <br /> Fitwatch <br /> </strong> <br /> The Forward Intelligence Team will, we&rsquo;re sure, be at Mayday! To find out about FIT and how to sabotage their surveillance see <a href="http://www.fitwatch.blogspot.com" target="_blank">www.fitwatch.blogspot.com</a> Remember you&rsquo;re under no obligation to comply with their filming. <br /> <strong> <br /> Critical Mass</strong>  <br />  <br /> Critical Mass will be meeting at 11am at Brighton Station before the main Street Party... Bikes Not Bombs, Bring sound systems! <br /> <strong> <br /> Smash EDO Radio  <br /> </strong> <br /> Broadcasting throughout the day on 101.4fm  giving you vital info and a soundtrack to the street party. If you&rsquo;ve got a radio or sound system with a tuner, blast out the broadcast. <br /> <strong> <br /> Bike-ride from London <br /> </strong> <br /> There will be a cycle caravan leaving from  Brixton on Sunday (3rd), riding to Brighton for the demo. Meet 9am outside Ritzy Cinema, Brixton leaving at 9.30am. See <a href="http://www.indymedia.org.uk/en/2009/04/428512.html" target="_blank">www.indymedia.org.uk/en/2009/04/428512.html</a> <br /> <strong> <br /> Kids Block  <br /> </strong> <br /> There'll be a kid's block on the day, with parents and kids looking out for each other - email <a href="mailto:rubychard@riseup.net">rubychard@riseup.net</a> <br /> <strong> <br /> Callout for Bands and DJs <br /> </strong> <br /> If you are in a band or can DJ contact <a href="mailto:smashedo@riseup.net">smashedo@riseup.net</a> <br /> <strong> <br /> Legal Support <br /> </strong> <br /> If you or someone you know are arrested or assaulted by police and need legal support 07522024454. For emotional support call 07962 406940 <br /> <strong> <br /> Press Enquiries <br /> </strong> <br /> Tel 07754 135290 or email <a href="mailto:smashedopress@riseup.net">smashedopress@riseup.net</a> For latest press release see <a href="http://www.smashedo.org.uk/pressreleases.htm" target="_blank">www.smashedo.org.uk/pressreleases.htm</a> <br /> <strong> <br />  <br />  <br />  <br />  <br /> Anti-Militarist Gathering</strong> <br />  <br /> May 2nd-3rd is the Anti Militarist Network gathering in Brighton. This will be a chance for local anti-arms trade/anti-militarist groups to exchange ideas and to plan our response to the 2009 DSEI arms fair and the NATO summit in Edinburgh in November. <br />  <br /> For more info see here <a href="http://antimilitaristnetwork.noflag.org.uk" target="_blank">http://antimilitaristnetwork.noflag.org.uk</a> or call 07983084019 <br />  <br /> </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-military+gathering&source=674">anti-military gathering</a>, <a href="../keywordSearch/?keyword=brighton&source=674">brighton</a>, <span style='color:#777777; font-size:10px'>critical mass</span>, <a href="../keywordSearch/?keyword=direct+action&source=674">direct action</a>, <a href="../keywordSearch/?keyword=edo-mbm&source=674">edo-mbm</a>, <a href="../keywordSearch/?keyword=fitwatch&source=674">fitwatch</a>, <span style='color:#777777; font-size:10px'>mayday</span>, <span style='color:#777777; font-size:10px'>paul hills</span>, <a href="../keywordSearch/?keyword=smash+edo&source=674">smash edo</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(305);
			
				if (SHOWMESSAGES)	{
				
						addMessage(305);
						echo getMessages(305);
						echo showAddMessage(305);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>