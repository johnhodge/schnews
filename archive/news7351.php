<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 735 - 20th August 2010 - High Pressure Front</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate camp, wales, coal, mining, direct action, climate change" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 735 Articles: </b>
<b>
<p><a href="../archive/news7351.php">High Pressure Front</a></p>
</b>

<p><a href="../archive/news7352.php">Climate Camp Scotland</a></p>

<p><a href="../archive/news7353.php">Holy Waters</a></p>

<p><a href="../archive/news7354.php">Wrekin Havok</a></p>

<p><a href="../archive/news7355.php">Hammertime</a></p>

<p><a href="../archive/news7356.php">Rattling The Coppers</a></p>

<p><a href="../archive/news7357.php">Going Out On A High</a></p>

<p><a href="../archive/news7358.php">Dirty Sheets</a></p>

<p><a href="../archive/news7359.php">Mapuche Hunger Strikes</a></p>

<p><a href="../archive/news73510.php">Nationalist Express</a></p>

<p><a href="../archive/news73511.php">Gaza Defendant Appeal</a></p>

<p><a href="../archive/news73512.php">Taking The Ciss</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/735-michael-fish-lg.jpg" target="_blank">
													<img src="../images/735-michael-fish-sm.jpg" alt="Michael Fish" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 20th August 2010 | Issue 735</b></p>

<p><a href="news735.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HIGH PRESSURE FRONT</h3>

<p>
<p>
	<strong>AS THE CAMP FOR CLIMATE ACTION MOVES FROM WALES TO SCOTLAND</strong> </p>
<p>
	Climate Camp Cymru kicked off on Friday (13th) in South Wales and continued until Tuesday (17th) with an eviction forcing a change of site. </p>
<p>
	The camp ran into problems on Saturday (14th) afternoon when it was evicted from its site. A disproportionately large police force consisting of 10 riot vans accompanied by dogs, helicopters and mounted police, was called into action to remove just 30 activists from a field. </p>
<p>
	The field in Glyn-Neath chosen for the climate camp was also the site of a Roman hill fort. Although permission to use the site had been granted by the land-owner, local police showed a heart-warming concern for the environment by claiming that the climate campers would damage the ancient monument. </p>
<p>
	Conversely, Cadw, the government body responsible for the historic environment of Wales, did not perceive any threat to the historical site by the climate camp. They held talks with camp organisers and the police before the eviction. Once they had ascertained that no damage had been done, they were happy for the camp to go ahead, provided it was monitored. </p>
<p>
	Disregarding advice from the experts, the police decided that the climate campers, whose record for leaving sites exactly as they found them is well known, had to go. According to one camper, police threatened to arrest everyone on the site for aggravated trespass after the land-owner was persuaded by police to withdraw permission for use of the land. &ldquo;<em>There was an overbearing and unnecessary police presence, we were intimidated and criminalized</em>,&rdquo; said Penny Williams, one of the activists at the camp. </p>
<p>
	Police seemed somewhat less concerned with the environmental crimes being committed in the surrounding countryside by coal giants Celtic Energy, who own the nearby Selar and Nant-Helen opencast coal mines. Celtic Energy are seeking planning permission to expand the mine at Nant-Helen where around 450,000 tonnes of coal are dug from the ground each year. Climate campers had planned direct action against the company to highlight the destruction caused by the mining. </p>
<p>
	Despite the setbacks, climate campers managed to secure a second site on the Gower on Saturday night. Although many people had been discouraged by the earlier eviction, around 25 people regrouped on the new camp. </p>
<p>
	Early on Tuesday morning, two groups of activists occupied the Nant-Helen mine. Protesters said that they wanted to observe the scale of what was happening there and to show that they were still able to observe despite the earlier bullying by the police. They made their way to the bottom of the mine and took photographs of machinery before leaving. No police were involved. <br /> 
	&nbsp;Penny Williams said: <em>&ldquo;It was overwhelming to experience the phenomenal scale of the destruction of the landscape. It&rsquo;s just crazy to think that more opencast mines like this are being planned</em>. </p>
<p>
	&ldquo;<em>It is unjust that corporations like Celtic Energy can carry on these operations in the face of today&rsquo;s climate change. Our economic system is based on an addiction to fossil fuels and on maximising profit at the expense of the environment. We need to rebuild the system based around greener jobs and stop this unnecessary destruction</em>.&rdquo; </p>
<p>
	* See <a href="http://climatecampcymru.org" target="_blank">http://climatecampcymru.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(902), 104); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(902);

				if (SHOWMESSAGES)	{

						addMessage(902);
						echo getMessages(902);
						echo showAddMessage(902);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


