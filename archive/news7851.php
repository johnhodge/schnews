<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 785 - 26th August 2011 - Dale Farm: Fight the Power</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, dale farm, eviction" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 785 Articles: </b>
<b>
<p><a href="../archive/news7851.php">Dale Farm: Fight The Power</a></p>
</b>

<p><a href="../archive/news7852.php">Zad: Breaking Da Vinci Code</a></p>

<p><a href="../archive/news7853.php">Splinter Of Discontent</a></p>

<p><a href="../archive/news7854.php">Gaddafi Ducks Out</a></p>

<p><a href="../archive/news7855.php">Atos Shrugged</a></p>

<p><a href="../archive/news7856.php">Autonomist Under Attack</a></p>

<p><a href="../archive/news7857.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th August 2011 | Issue 785</b></p>

<p><a href="news785.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DALE FARM: FIGHT THE POWER</h3>

<p>
<p>  
  	<strong>CALLOUT AS EVERYONE GEARS UP FOR IMMINANT EVICTION ATTEMPT&nbsp; </strong>  </p>  
   <p>  
  	As the eviction date for Dale Farm is only a matter of days away, Basildon Council has announced that it will take the draconian step of cutting off all water and electricity to the site once the eviction notice period expires on midnight 31st August. This will leave sick, elderly, young, and pregnant residents without access to water or electricity. Amnesty International have condemned the removal of vital water and electricity in these circumstances, and asked their supporters to put pressure on the council to cease this serious violation of human rights. An injunction has been sought in consideration of two residents who are dependent upon a constant electricity supply for nebulisers, without access to which their lives are placed in serious jeopardy.  </p>  
   <p>  
  	Also, notices have gone up along Oak Road, adjacent to Dale Farm, saying that the road will be closed to all but residents from Friday, Sept. 2nd.&nbsp; Both ends of Oak Road will be blocked (blocking access via both Hardings Elm Road and Gardiners Lane North). Additionally, the lay by on the southern end of Oak Lane (leading on to the A127; by the white &lsquo;Basildon onion&rsquo; water tower) will be blocked. There will be a no stop zone on the footpaths on the A127 between A176 at Billericay and A132 at Wickford. Residents are feeling under siege, with children asking how many more nights they are going to be able to sleep in their beds. Dale Farm is a big site, so it should be possible to find routes in, but be advised that after Sept. 1, it will be harder to get in, and likely impossible to get vehicles in.  </p>  
   <p>  
  	* For more on what to expect at the eviction see <a href='../archive/news738.htm'>SchNEWS 738</a>  </p>  
   <p>  
  	<strong>CAMP CONSTANT: </strong>On Saturday, 27th August, join the launch of a solidarity and resistance camp for supporters of the Dale Farm community. Details of the current state of play, the roadblocks and the &lsquo;Big Weekend&rsquo;s&rsquo; programme of practical eviction resistance workshops, training for legal observers and human rights monitors, music, films, media sessions, sleeping arrangements and more at: <a href="http://dalefarm.wordpress.com" target="_blank">http://dalefarm.wordpress.com</a> <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1332), 154); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1332);

				if (SHOWMESSAGES)	{

						addMessage(1332);
						echo getMessages(1332);
						echo showAddMessage(1332);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


