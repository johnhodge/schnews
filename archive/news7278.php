<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - Hoto Finish</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, croatia, autonomous spaces" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<p><a href="../archive/news7275.php">Turkey Shoot</a></p>

<p><a href="../archive/news7276.php">The Itt Crowd</a></p>

<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>

<b>
<p><a href="../archive/news7278.php">Hoto Finish</a></p>
</b>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HOTO FINISH</h3>

<p>
<p>
	Croatian protesters have occupied a pedestrian street in Croatia&rsquo;s capital Zagreb and are demanding the mayor&rsquo;s resignation. </p>
<p>
	The occupation is around two weeks old and is the latest tactic in a two-and-a-half year campaign against a dodgy development by tycoon-owned HOTO Group. The development has the unconditional backing of city mayor Milan Bandic and was OK-ed by the Minister of Environmental Protection, Planning and Construction - unsurprising as she has shares in the construction company set to carry out the work. Spirits were high when SchNEWS visited the assortment of students, NGO representatives, disgruntled residents and dreadlocked activists camping out on Varsavska, a pedestrian street in the centre of Zagreb. </p>
<p>
	The latest stage of the campaign began early on Monday May 17th as contractors erected a metal fence around Varsavska - a public space soon to be privatised and turned into an access ramp for the private investor&rsquo;s underground car park. Straight away the entrance was blocked by activists, stopping any further work. At midday they were joined by several hundred city residents who hammered on the fence and eventually pulled it down and occupied the area. Now the fence has been used by protesters as the skeleton of a tent-like structure, over which they have draped clear tarpaulin. A long line of sleeping bags are neatly laid out inside. Activists are keen to stress the non-violent and alcohol-free nature of the space. </p>
<p>
	There is a huge amount of public support, and the protests have attracted numbers unheard of in Croatia. Local residents have been bringing food and hot drinks to support the occupation. On May 20th, 4,000 people marched to Zagreb City Council demanding the resignation of Mayor Bandic over the shady plans. In an effort to encourage him to &lsquo;pack his bags&rsquo; and find a new job, protesters helpfully piled up suitcases outside the council building. </p>
<p>
	The site was previously occupied earlier this year using shipping containers. That occupation lasted 10 days but was evicted forcefully by riot police in the middle of the night with 23 arrests (five ultimately charged, none convicted). The real casualty was the trojan horse - a giant wooden structure built by activists and presented to the City Administration as a symbol of all swindles and deception related to the project. The horse was brutally broken apart using cranes in a two-hour long eviction in the snow. </p>
<p>
	Complaints against the development include concerns about restricted emergency vehicle access, increased traffic and of course the loss of a well-loved hang-out area, several large trees and protected cultural heritage buildings in the old part of the city. There is now a belated investigation by the state anti-corruption office USKOK of potential criminal liability in issuing the permits for the construction of the &ldquo;lifestyle&rdquo; HOTO Centre, luxury flats and car park. This follows several months of corruption charges against Croatian politicians, in which the Vice President and Minister of Defence are still awaiting court decisions. </p>
<p>
	The protesters, gathering under the banner &ldquo;Right to a City&rdquo;, have vowed to continue their occupation of Varsavska for as long as is necessary to stop the project. They say, &ldquo;<em>this is not just about this street, this is a symbol of the fight against corruption in Croatia. The system is rotten</em>&rdquo;. </p>
<p>
	* For more (in Croatian) see <a href="http://pravonagrad.org" target="_blank">http://pravonagrad.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(824), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(824);

				if (SHOWMESSAGES)	{

						addMessage(824);
						echo getMessages(824);
						echo showAddMessage(824);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


