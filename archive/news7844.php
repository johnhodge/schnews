<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 784 - 19th August 2011 - Crap Justice of the Week</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, riots, david cameron, facebook, sentencing" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 784 Articles: </b>
<p><a href="../archive/news7841.php">The U.n. Holy Land</a></p>

<p><a href="../archive/news7842.php">Shock Tactics</a></p>

<p><a href="../archive/news7843.php">Getting The Humpback</a></p>

<b>
<p><a href="../archive/news7844.php">Crap Justice Of The Week</a></p>
</b>

<p><a href="../archive/news7845.php">Oxford Anti-social</a></p>

<p><a href="../archive/news7846.php">Chile: Education Riots</a></p>

<p><a href="../archive/news7847.php">Edl: You Can Never Telford</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th August 2011 | Issue 784</b></p>

<p><a href="news784.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CRAP JUSTICE OF THE WEEK</h3>

<p>
<p>  
  	<em><strong>&ldquo;This was about people showing indifference to right and wrong, people with a twisted moral code, people with a complete absence of self-restraint.&rdquo;</strong></em>  </p>  
   <p>  
  	Fast-forward ten years and Cameron&rsquo;s words might be a better description of the judicial lynch mob mentality taking hold in England&rsquo;s court-rooms right now. In what seems to be a sort of &lsquo;shock doctrine&rsquo; approach to criminal justice, the government have decided to wage &ldquo;all-out war&rdquo; on gangs and gang culture. If anything illustrates the ruling elite&rsquo;s fear of the mob it is this return to transportation-era justice. Five months for receiving a pair of shorts, six months for stealing water, six months for nicking ice cream, four f**kng years for organising a riot on Facebook that never happened. This uprising had our masters worried (one top cop has leaked that during a COBRA meeting a panicking Cameron was considering bringing in the army).  </p>  
   <p>  
  	This wave of vicious sentencing has been co-ordinated from the top. While cops were considering introducing tactics, like the use of baton rounds, from Northern Ireland onto the streets of the mainland the judiciary has taken a leaf out of the Stormont book and is in effect introducing a form of internment. The existence of the directive ...sorry &lsquo;guidance&rsquo; was revealed&nbsp; by the chair of the bench at Camberwell Green Magistrates Court, Novello Noades,as she sentenced one man to six months in prison. Don&rsquo;t forget that most of those headed for custody now are actually on remand &ndash; an astounding two thirds of all defendants have been remanded.  </p>  
   <p>  
  	Cruder weapons are set to be deployed as the principle of &lsquo;collective punishment&rsquo; is used. Tory-controlled Wandsworth council is trying to evict a family because one member has been accused of involvement. &ldquo;We wanted to get the ball rolling,&rdquo; said the council, nodding in the direction of the old fashioned &lsquo;innocent &lsquo;til proven guilty&rsquo; schtick by saying &ldquo;If he is acquitted we will end our eviction proceedings&rdquo;. Wandsworth Council and others have promised wide application of these powers.  </p>  
   <p>  
  	Today 25 protesters gathered outside the house of Ravi Govindia, head of Wandsworth Council, shouting &ldquo;Evict the Tories &ndash; not the poor&rdquo;. One arrest was made.  </p>  
   <p>  
  	For more <a href="http://noriotevictions.wordpress.com" target="_blank">http://noriotevictions.wordpress.com</a>  </p>  
   <p>  
  	Prison support groups are being set up around the country alongside the Anarchist Black Cross. Currently nearly 2000 have been nicked following the riots &ndash; with no doubt many more to follow as cops trawl through the CCTV.  </p>  
   <p>  
  	* <a href="http://bristolabc.wordpress.com/defendant-solidarity/" target="_blank">http://bristolabc.wordpress.com/defendant-solidarity/</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1328), 153); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1328);

				if (SHOWMESSAGES)	{

						addMessage(1328);
						echo getMessages(1328);
						echo showAddMessage(1328);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


