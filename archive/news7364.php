<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 736 - 27th August 2010 - War is Over (Killing Continues)</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, iraq, anti-war" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 736 Articles: </b>
<p><a href="../archive/news7361.php">Royal Bs</a></p>

<p><a href="../archive/news7362.php">Hovefields Eviction Threat</a></p>

<p><a href="../archive/news7363.php">The Fash Show</a></p>

<b>
<p><a href="../archive/news7364.php">War Is Over (killing Continues)</a></p>
</b>

<p><a href="../archive/news7365.php">Grow Heathrow Eviction Alert</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 27th August 2010 | Issue 736</b></p>

<p><a href="news736.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WAR IS OVER (KILLING CONTINUES)</h3>

<p>
<p>
	Crack open the champagne, brush the dust of the Stop The War Banners, and remember those chants from all those years ago. If you didn&#39;t hear it anywhere else you heard it here: The Iraq War is over. Or, to be more precise, the American war in Iraq is over. 1,000,000 dead Iraqis, 4,000 dead US soldiers, perhaps 3000 dead coalition and mercenary troops, 2 million refugees and entire cities in ruin; the Americans have left Iraq. Mostly. There is of course the small matter of some 50,000 soldiers that will remain for an indeterminate time in an advisory role, making sure that Iraq&#39;s death squads and torturers are up to scratch. </p>
<p>
	The Obama administration decided to jump the gun, leaving two weeks before their self-imposed deadline of August 31st 2010. The last combat division, the 4th Stryker brigade, 2nd Infantry Division, have driven out with their hundreds of vehicles from Camp Victory to Kuwait. The US strategy perfected in Iraq by General Petraeus has been to declare victory and get the hell out. The good general is soon to repeat this in Afghanistan. The Iraqi government has demanded a complete withdrawal of US forces by December 2011, and the US is likely to accede (they don&#39;t have much choice). </p>
<p>
	As it turns out, there was no war for oil. Or at least, a trillion dollars of war bought at best a few tens of millions of dollars of Iraqi oil. </p>
<p>
	The US has abandoned most of its bases in Iraq. Many of them were as large as cities, complete with malls, Burger Kings, Pizza Huts (TGI Friday was refused permission because it was considered too provocative to Muslim sentiment). Balad airbase was for a while second only to London Heathrow in its traffic. These garrisons of the US empire were for a while quite an emotional issue for people either for or against the &#39;New American Century.&#39; </p>
<p>
	And now they are the subject of one of the largest fire sales in recent history. Entire bases - blast walls, generators, housing units, are being sold at bargain basement prices to Iraqi middle men. US army issue prefab briefing rooms (complete with whiteboards and military maps en situ) are being sold on to Iraqi farmers who can use then to chill out whilst tending their sheep. </p>
<p>
	If controlling Iraq was the US/UK war aim, they failed abysmally. If the war aim was to cause complete and utter bloody chaos and devastation, then Messrs Bush and Blair can pat themselves on the back. Iraq has no functioning government - the divisive and sectarian system development by the US has meant that, following the elections, there has been a 4 month long deadlock. And while violence may have dropped from it&#39;s bloody peak, there is still not even the semblance of security. On Wednesday, around 50 people in seven cities were killed in a series of co-ordinated bomb blasts, blamed on Sunni militant groups aligned to al-Qaida. The attacks were the first to break into the consciousness of the media for a while but violence is a daily occurrence. The following is taken directly from Reuters Alertnet, on just one day- August 18th 2010, (based on police, military and interior ministry reports). Just an ordinary day in Iraq: </p>
<p>
	<strong>MADAEN</strong> - Gunmen in a speeding car opened fire at a government-backed Sahwa militia checkpoint, killing one and wounding two others in Madaen, 30 km (20 miles) south-east of Baghdad </p>
<p>
	<strong>BAQUBA</strong> - A sticky bomb attached to the car of a government-backed Sahwa militia leader killed him and wounded his brother when it exploded in eastern Baquba, 65 km (40 miles) north-east of Baghdad </p>
<p>
	<strong>MOSUL</strong> - Gunmen using silenced guns opened fire at an Iraqi military checkpoint, killing a soldier in western Mosul, 390 km (240 miles) north of Baghdad </p>
<p>
	<strong>MOSUL</strong> - A roadside bomb targeting an Iraqi army patrol killed one civilian and wounded two soldiers when it went off in eastern Mosul </p>
<p>
	<strong>MOSUL</strong> - Police found the body of a man with gunshot wounds to the head in the town of Bartila, west of Mosul </p>
<p>
	<strong>SADIYA</strong> - Gunmen stormed three houses, killing three people in the town of Sadiya, 80 km (50 miles) north-east of Baquba, the capital of Diyala province </p>
<p>
	<strong>TIKRIT</strong> - Two people were killed, including a policeman, and one civilian was wounded when a roadside bomb exploded in central Tikrit, 150 km (95 miles) north of Baghdad </p>
<p>
	<strong>BAGHDAD</strong> - A roadside bomb wounded four people in Palestine street in north-eastern Baghdad </p>
<p>
	<strong>BAGHDAD</strong> - A roadside bomb wounded two people in the eastern Baghdad district of Zayouna </p>
<p>
	<strong>MOSUL</strong> - Police found the body of an unidentified man with head and chest bullet wounds in eastern Mosul, 390 km (240 miles) north of Baghdad </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(917), 105); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(917);

				if (SHOWMESSAGES)	{

						addMessage(917);
						echo getMessages(917);
						echo showAddMessage(917);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


