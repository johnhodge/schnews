<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 663 - 23rd January 2009 - Decommission Accomplished</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edo, edo-mbm, itt, direct action, arms trade, smash edo, brighton, isreal, palestine, edo 9, raytheon, gaza, hellfire missile, bae" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 663 Articles: </b>
<b>
<p><a href="../archive/news6631.php">Decommission Accomplished</a></p>
</b>

<p><a href="../archive/news6632.php">Sentenced For Life</a></p>

<p><a href="../archive/news6633.php">Are You Being Served</a></p>

<p><a href="../archive/news6634.php">School Of Life</a></p>

<p><a href="../archive/news6635.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/663-edo-smashed-lg.jpg" target="_blank">
													<img src="../images/663-edo-smashed-sm.jpg" alt="Bloody hell Sarge, it looks like a bomb's hit the place!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 23rd January 2009 | Issue 663</b></p>

<p><a href="news663.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>DECOMMISSION ACCOMPLISHED</H3>

<p>
<b>AS EDO GETS SMASHED.  <br />
REALLY, REALLY SMASHED...</b> <br />
 <br />
<i>&#8220;Great Britain... will do everything we can to prevent arms trafficking which is at the root of some of the problems... I believe that will help get a solution to this crisis.&#8221; - Gordon Brown <br />
&#8220;I don&#8217;t feel I&#8217;m going to do anything illegal tonight, but I&#8217;m going to go into an arms factory and smash it up to the best of my ability so that it cannot actually work or produce munitions... [which] have been provided to the Israeli army so that they can kill children.&#8221;</i> - <b>Elijah Smith, one of the EDO 9.</b> <br />
 <br />
After five years of campaigning, die-ins, lock-ons, rooftop occupations, noise demos and the odd riot, it turns out that the Smash EDO campaign had been taking a fairly indirect route. In the early hours of last Saturday morning six activists turned up at the factory with a ladder and some hammers, and spent a night thoroughly &#8216;decommissioning&#8217; the arms factory from top to bottom.  <br />
 <br />
Outraged by the ongoing genocide in Gaza and the UK and US&#8217;s involvement in high-tech killing around the world, they explained in a pre-recorded video that the time for talking was over. In the pre-recorded words of Bob; &#8220;<i>The Israeli Defence Forces are guilty of war crimes in Gaza. EDO and many other arms manufacturers around the UK are aiding and abetting these humanitarian crimes and war crimes. The action we&#8217;ve taken is intended to hamper or delay the commission of war crimes and prevent this greater crime. The glorification of war and the mass production of arms and weapons is a sickness in the heart of those involved.</i>&#8221; <br />
  <br />
DCI Graham Pratt of Sussex Plod paid them the best compliment they could have got: <i>&#8220;Windows had been smashed and offices turned over in what I would describe as wanton vandalism, but with machinery and equipment so targeted that it could have been done with a view to bringing business to a standstill. The damage is significant and the value substantial.&#8221; </i> <br />
 <br />
Coherence and logic aren&#8217;t taught at cop school, so we&#8217;ll leave out the fact that targeting machinery and equipment in an arms factory by anti-arms protesters cannot be &#8216;wanton vandalism&#8217;, but we&#8217;ll let that one go. More to the point, &#8216;significant and substantial damage&#8217; has been caused to an arms company that&#8217;s been busy selling arms to Israel and ignoring public disgust at the genocide that&#8217;s been carried out in Gaza.  <br />
 <br />
This isn&#8217;t your run of the mill symbolic act of solidarity. The six decommissioners threw themselves in the path of the US-UK-Israeli juggernaut, and in doing so slowed it down by a measurable amount. The protesters are being charged with criminal damage to the tune of �250,000, but this is almost definitely an understatement. After all the Raytheon 9 (See <a href="../archive/news635.htm">SchNEWS 635</a>) were charged with �350,000 for chucking some computers out of an office window.  <br />
 <br />
But if you want to reduce their actions to numbers, consider these: a Hellfire missile costs around �25,000, and if you take EDO/police at face value that would mean 10 less Hellfires. But in reality the figure&#8217;s likely to be much higher, as the six were in there for over an hour. Of course, if EDO can&#8217;t get their kit back in working order quick enough to complete their contracts on time, their losses could be hefty indeed.  According to local eyewitnesses, the protesters did it in some style. They stuck the factory&#8217;s sound system on and blasted out tunes whilst they worked.  <br />
 <br />
Apparently a missile or bomb shaped object was seen flying out of one factory window. The jury&#8217;s out on whether that counts as collateral damage. <br />
 <br />
Eventually 30 police accompanied by a specialist forced entry unit broke through the barricades and were able to corner and arrest the decommissioners. <br />
 <br />
Alongside the six that broke in to the factory, another three protesters are being held in connection with the action. Initially eight of the nine were held on remand, but as of Thursday all but two are now out on strict bail conditions.  <br />
 <br />
The six had recorded &#8216;martyr-videos&#8217; of themselves explaining why they were prepared to decommission the factory. View it at <a href="http://www.indymedia.org.uk/media/2009/01//418836.mpg" target="_blank">www.indymedia.org.uk/media/2009/01//418836.mpg</a> <br />
 <br />
* See also <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>  <br />
 <br />
* The Raytheon 9 - During the Lebanon War in August 2006 nine protesters occupied Raytheon offices in Derry, Northern Ireland and smashed their way through an alleged �350,000 worth of computer gear. Their defence was that they were attempting to prevent war crimes, and in June 2008 were found unanimously not guilty on three counts of criminal damage by a jury in a Belfast court. <a href="http://www.raytheon9.org" target="_blank">www.raytheon9.org</a> <br />
 <br />
* PRISONER SUPPORT: Sent letters of support to Robert Altford (aka Tintin). Prisoner no. VP7552 and Elijah James Smith. Prisoner no. VP7551 - HMP Lewes, Brighton Rd, Lewes, BN7 1EA. <br />
 <br />
* Mayday Reclaim the Streets Against EDO MBM/ITT, May 4th - Reclaim The Streets style demo/carnival on Mayday - against Brightons bomb factory. <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
<b>ALSO UP IN ARMS</b> <br />
 <br />
BAE is another company playing its part in Israel&#8217;s Gaza massacre, as well as many other war crimes. This Thursday (22nd) six protesters blocked the entrance to BAE&#8217;s Newcastle premises for more than an hour by staging a &#8216;die-in&#8217;.  <br />
 <br />
Meanwhile in Warwick, last week a group of university students disrupted a BAE recruitment event by shouting down the speakers and distributing an alternative BAE careers guide. <br />
 <br />
Despite being subsidised by the British government, BAE makes more than �1 million per day from it&#8217;s weapons peddling &#8211; so much in fact that it can even afford to slip the odd billion to Saudi princes. <br />
 <br />
* <a href="http://www.indymedia.org.uk/en/2009/01/419750.html" target="_blank">www.indymedia.org.uk/en/2009/01/419750.html</a> <br />
 <br />
* <a href="http://weaponsoutofwarwick.wordpress.com" target="_blank">http://weaponsoutofwarwick.wordpress.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=edo-mbm&source=663">edo-mbm</a>, <a href="../keywordSearch/?keyword=itt&source=663">itt</a>, <a href="../keywordSearch/?keyword=direct+action&source=663">direct action</a>, <a href="../keywordSearch/?keyword=arms+trade&source=663">arms trade</a>, <a href="../keywordSearch/?keyword=smash+edo&source=663">smash edo</a>, <a href="../keywordSearch/?keyword=brighton&source=663">brighton</a>, <a href="../keywordSearch/?keyword=palestine&source=663">palestine</a>, <a href="../keywordSearch/?keyword=raytheon&source=663">raytheon</a>, <a href="../keywordSearch/?keyword=gaza&source=663">gaza</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(199);
			
				if (SHOWMESSAGES)	{
				
						addMessage(199);
						echo getMessages(199);
						echo showAddMessage(199);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>