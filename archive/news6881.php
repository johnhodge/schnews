<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 688 - 21st August 2009 - Stock Horror At Hls</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, hls, huntingdon life sciences, vivisection, direct action, shac, morgan stanley, bayer" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 688 Articles: </b>
<b>
<p><a href="../archive/news6881.php">Stock Horror At Hls</a></p>
</b>

<p><a href="../archive/news6882.php">Climate Of Anticipation  </a></p>

<p><a href="../archive/news6883.php">A Wales Of A Time</a></p>

<p><a href="../archive/news6884.php">Fash On Parade</a></p>

<p><a href="../archive/news6885.php">Straight From The P'smouth</a></p>

<p><a href="../archive/news6886.php">Back To Base</a></p>

<p><a href="../archive/news6887.php">Cease And Resist</a></p>

<p><a href="../archive/news6888.php">Smarter In Vestas</a></p>

<p><a href="../archive/news6889.php">Gross-ery Shop</a></p>

<p><a href="../archive/news68810.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/688-lolbunnie-lg.jpg" target="_blank">
													<img src="../images/688-lolbunnie-sm.jpg" alt="U Huminz iz Sick Weirdos" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 21st August 2009 | Issue 688</b></p>

<p><a href="news688.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STOCK HORROR AT HLS</h3>

<p>
<strong>AS BANKERS RUN SCARED FROM INVESTING IN ANIMAL TORTURE...</strong> <br />  <br /> <strong>Huntingdon Life Sciences (HLS)</strong>, the largest animal testing lab in Europe have had a huge setback this week as giant US investment bank Stanley Morgan pulled out their shares in HLS. <br />  <br /> Morgan Stanley divested their shares after just a few demos in the last few weeks by <strong>Win Animal Rights (WAR)</strong>. This victory follows Barclays&rsquo; dumping of HLS in May (See <a href='../archive/news677.htm'>SchNEWS 677</a>), and is part of a successful tactic of targeting investors, clients and other support companies &ndash; alongside continual direct action and demonstrations &ndash; which has brought HLS to the brink of collapse. <br />  <br /> Despite the <strong>National Extremism Tactical Coordination Unit (NETCU)</strong> - one of the UK&rsquo;s political police forces [see netcu.wordpress.com] - insisting that the <strong>Stop Huntingdon Animal Cruelty (SHAC)</strong> campaign has been crippled (See <a href='../archive/news655.htm'>SchNEWS 655</a>), activists have achieved one of their major campaign targets, the delisting of HLS from the <strong>New York Stock Exchange (NYSE)</strong>. <br />  <br /> CEO Andrew Baker is now looking to make HLS a &lsquo;privately held company&rsquo; meaning that they will no longer trade shares on stock markets. <br />  <br /> On August 1st, activists from WAR visited the homes of five senior Morgan Stanley executives, followed by a visit on the 6th to Morgan Stanley&rsquo;s New York HQ, plus more home visits. In the days after this Morgan Stanley &ndash; HLS&rsquo;s largest investor with a 5% stake &ndash; divested hundreds of thousands of LSR shares, leaving HLS/LSR valued below the $15 million required to be listed on the NYSE. <br />  <br /> In the past three months, after constant actions and demos in the UK and US, HLS/LSR have seen their two biggest institutional investors pull out. In late July a capital firm called H Partners pulled their &pound;11.2 million out, and in May, after months of campaign pressure, Barclays sold all their shares. Also that month Hartford Investment Management Company, Rice Hall James and Associates LLC and BNY Mellon also pulled out (whoever these anonymous suits are!). HLS is currently &pound;72 million in debt and revenue has dropped by a quarter in the past year. HLS are admitting that the international campaign by WAR and SHAC is having a tangible effect. <br />  <br /> HLS were forced to move their shares to the NYSE after years of constant campaigning forced them to pull their shares off the main trading platform of the London Stock Exchange (LSE) in 2001. The SHAC campaign had made the trading of HLS shares on the LSE virtually impossible. After HLS was left without a banker willing to offer them services the British government was forced to step in and bail them out awarding them banking facilities with the Bank of England. <br />  <br /> HLS have around 70,000 animals at their death lab near Cambridge, ranging from primates and dogs through to rodents and birds, killing 500 a day in cruel experiments mostly for the pharmaceutical industry. Protests continue every week against HLS &ndash; for more see <a href="http://www.war-online.org" target="_blank">www.war-online.org</a> and <a href="http://www.shac.net" target="_blank">www.shac.net</a> <br />  <br /> * To show the, er, diversity of campaigning against HLS, another group has bobbed up to help close the vivisectors, calling themselves <strong>Militant Forces Against HLS (MFAH)</strong>. Similar in tactics to the ALF, in the past few months they have been targeting HLS customers across western Europe, and on August 2nd burnt the hunting lodge of Novartis CEO Daniel Vasella. They have also carried out an arson attack on a Novartis sports centre in St. Louis, France, as well as attacks on execs of Novartis and Bayer. The Vice Chairman of Novartis in Duesseldorf had his house spray painted, and his Porsche covered in paint stripper and the tyres slashed. Similar attacks against five Bayer execs also recently happened across Germany. <br />  <br /> ** Campaigners are gearing up for a mass demo against <strong>Highgate Rabbit Farm</strong> at 12 noon, near Market Rasen, Normanby-By-Spital, Lincolnshire on Saturday September 26th after a successful camp held there last month (See <a href='../archive/news686.htm'>SchNEWS 686</a>). <a href="http://www.shac.net/OperationLiberation" target="_blank">www.shac.net/OperationLiberation</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=688">animal rights</a>, <a href="../keywordSearch/?keyword=bayer&source=688">bayer</a>, <a href="../keywordSearch/?keyword=direct+action&source=688">direct action</a>, <a href="../keywordSearch/?keyword=hls&source=688">hls</a>, <a href="../keywordSearch/?keyword=huntingdon+life+sciences&source=688">huntingdon life sciences</a>, <span style='color:#777777; font-size:10px'>morgan stanley</span>, <a href="../keywordSearch/?keyword=shac&source=688">shac</a>, <a href="../keywordSearch/?keyword=vivisection&source=688">vivisection</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(464);
			
				if (SHOWMESSAGES)	{
				
						addMessage(464);
						echo getMessages(464);
						echo showAddMessage(464);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>