<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 777 - 1st July 2011 - Greece: The Golden Fleecing</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, riots, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 777 Articles: </b>
<p><a href="../archive/news7771.php">Libya: Anti-nato Classes</a></p>

<p><a href="../archive/news7772.php">Africa House Evicted</a></p>

<p><a href="../archive/news7773.php">Simon Levin Rip</a></p>

<p><a href="../archive/news7774.php">A Ship Off The Ol'bloc</a></p>

<p><a href="../archive/news7775.php">Campaign For Real Bail</a></p>

<b>
<p><a href="../archive/news7776.php">Greece: The Golden Fleecing</a></p>
</b>

<p><a href="../archive/news7777.php">Peru - What A Scorcher</a></p>

<p><a href="../archive/news7778.php">Turn On The Waterways</a></p>

<p><a href="../archive/news7779.php">Sheikh-down</a></p>

<p><a href="../archive/news77710.php">Holding The Fortnum</a></p>

<p><a href="../archive/news77711.php">[headline On Strike]</a></p>

<p><a href="../archive/news77712.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st July 2011 | Issue 777</b></p>

<p><a href="news777.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREECE: THE GOLDEN FLEECING</h3>

<p>
<p>  
  	Greece kicked off with a 48-hour general strike on Tuesday (28th) ahead of the vote on the &pound;25bn austerity bill that finally passed on Wednesday (29th).  </p>  
   <p>  
  	The austerity bill is a prerequisite for additional loans from the EU and the IMF. And, sadly for the Greek people, those &lsquo;bail outs&rsquo; ultimately only add to the deficit, not reduce it. But the government signed up for it, by 155 votes to 138. The resultant austerity measures will include tax increases (with a &lsquo;solidarity levy&rsquo; of between 1% and 5% on all Greek wage earners), a lower tax-free threshold and VAT hoisted from 13% to 23%. Cuts will see public sector wages slashed by another 15%, 150,000 public sector jobs cleaved, reductions to social benefits and the retirement age raised to 65. That&rsquo;s one uncompromising package deal.  </p>  
   <p>  
  	Following the vote the streets became a battlefield as the people went head to head with the police at Syntagma square. Protesters set up barricades to block access to Parliament, with several fires blazing in the area. Tear gas, hand grenades and even rocks were used by police on protesters, with many non-violent demonstrators getting caught up in the fracas. Some protesters used Maalox, an antacid, to protect themselves against tear gas.  </p>  
   <p>  
  	Tactics deployed by the 4000 police included attempts to kettle demonstrators in alleyways and side streets so they could throw tear gas at them all the more successfully. In turn the protesters hurled chunks of marble and paving stones ripped from the ground.  </p>  
   <p>  
  	Reports from the Red Cross show that at least 500 people received medical care at the Metro station in Syntagma square, a hundred at the Gennimatas general hospital in Athens while at least 170 protesters visited the A&amp;E of Evangelismos hospital. There were 14 arrests and 29 people detained.  </p>  
   <p>  
  	In the city of Thessaloniki, a 600 strong demo marched through the streets towards the town hall. In Kozani, the occupation of the labour centre continued. On the island of Chios, the town hall was occupied by demonstrators. The list goes on. As Wednesday came to a close, riot police had their work leave recalled for the foreseeable future and were instructed to be on high alert. <br />  
  	Hundreds of people assembled outside Parliament again on Thursday (30th), as the key second vote was won - allowing the government to change laws necessary to implement the cuts more quickly. Violence once again ensued on the streets.  </p>  
   <p>  
  	And it is likely to continue, until the government acknowledge that this crisis was caused largely by businessmen&rsquo;s greed, tax evasion and widespread corruption &ndash; and it is making pensioners and&nbsp; poorly paid workers pay for it. That is why people are angry, forced to clean up a mess they didn&rsquo;t cause. &ldquo;<em>Whatever happens we will stay on and fight</em>,&rdquo; says Pavlos Antonopoulos, an activist schoolteacher.  </p>  
   <p>  
  	The &pound;350bn question now is will these measures ever be successfully implemented? A Greek default is still plausible. The government hope the flames of resistance will die out &ndash; but it won&rsquo;t if the Greek public have anything to do with it. How long before the military is sent to march on its own people?  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1268), 146); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1268);

				if (SHOWMESSAGES)	{

						addMessage(1268);
						echo getMessages(1268);
						echo showAddMessage(1268);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


