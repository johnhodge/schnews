<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 730 - 9th July 2010 - Bog Roll Of Honour</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, direct action, manchester" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 730 Articles: </b>
<p><a href="../archive/news7301.php">The Hole Truth</a></p>

<p><a href="../archive/news7302.php">Decommissioners All Out</a></p>

<b>
<p><a href="../archive/news7303.php">Bog Roll Of Honour</a></p>
</b>

<p><a href="../archive/news7304.php">Village Fate</a></p>

<p><a href="../archive/news7305.php">Uzbeks Against The Wall</a></p>

<p><a href="../archive/news7306.php">Besetting A President</a></p>

<p><a href="../archive/news7307.php">Raven's Law</a></p>

<p><a href="../archive/news7308.php">Gettin On Their Wiki</a></p>

<p><a href="../archive/news7309.php">The Paper Trail</a></p>

<p><a href="../archive/news73010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th July 2010 | Issue 730</b></p>

<p><a href="news730.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BOG ROLL OF HONOUR</h3>

<p>
<p>
	Protesters in Manchester have been acquitted after an action against peat bog extraction. The Chat Moss site near Salford was the target of an action on April 15th in protest against the environmental impact of removing these natural, ancient carbon sinks &ndash; 94% of which have already been destroyed in Britain. Activists locked onto a peat extraction digger and the lorry it was trying to load to halt activity at the site, and two were arrested and charged under the Public Order Act Section 4a for causing &lsquo;harassment, alarm or distress&rsquo; to the employees at the site. </p>
<p>
	After hearing from three peat employees about how &lsquo;harassed, alarmed and distressed&rsquo; they were, the magistrate didn&rsquo;t even ask to hear from the defence before acquitting the two, saying it was not more than an &lsquo;irritation&rsquo;. He also rejected a request for a restraining order on the pair stopping them returning to Chat Moss. Inside the court and outside were supporters from Save Our North West Greenbelt. </p>
<p>
	The peat extraction site at Chat Moss is owned by Peel Holdings, a powerful corporate investment firm in the north-west who also own shipping ports, airports, shopping centres and more, and have a large stake in UK Coal. Local public opinion is firmly against the Chat Moss extraction site, including the local MP and council, who are proposing a ban on future extraction and seeking to return parts of it back to wet mossland. </p>
<p>
	Chat Moss has been forming as a peat bog for up to 10,000 years. One part of it is an SSSI (Site of Special Scientific Interest), another is an EU Special Area of Conservation. It is also an archaeological site, with the discovery of &lsquo;bog bodies&rsquo; preserved for millennia. Peat bogs like Chat Moss lock carbon into the soil and soak up further carbon, and disrupting them has a great environmental impact: in the UK alone, peat extraction causes 3 million tonnes of CO2 emissions a year. Most of the peat from Chat Moss ends up in garden centres for potted-plant mulch. </p>
<p>
	* See &lsquo;Save our North West Green Belt and Green Spaces&rsquo; on Facebook </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(851), 99); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(851);

				if (SHOWMESSAGES)	{

						addMessage(851);
						echo getMessages(851);
						echo showAddMessage(851);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


