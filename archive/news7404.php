<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 740 - 24th September 2010 - Running Ham-Mock</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, shell, rossport, shell to sea, oil, climate change, direct action, ireland" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 740 Articles: </b>
<p><a href="../archive/news7401.php">Brussels Sprouts Camp</a></p>

<p><a href="../archive/news7402.php">Rewrite His-tory</a></p>

<p><a href="../archive/news7403.php">All Mex'd Up</a></p>

<b>
<p><a href="../archive/news7404.php">Running Ham-mock</a></p>
</b>

<p><a href="../archive/news7405.php">Stockholme Syndrome</a></p>

<p><a href="../archive/news7406.php">Good Chap On Trial</a></p>

<p><a href="../archive/news7407.php">Schnews In Brief</a></p>

<p><a href="../archive/news7408.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 24th September 2010 | Issue 740</b></p>

<p><a href="news740.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RUNNING HAM-MOCK</h3>

<p>
<p>
	More shenanigans at the end of the rainbow this week as those plucky campers in Rossport, Ireland (see <a href='../archive/news737.htm'>SchNEWS 737</a>) make more moves to stop Shell stealing their country&rsquo;s lucky charms. </p>
<p>
	At 7am on Monday (20th) nine activists swam out to a drilling platform. Two strung themselves up in a hammock just under the platform, flanked by another two in kayaks and the rest in the water. By 9am the kayakers and swimmers had all been dragged back to shore and one drill on the platform was put to work. </p>
<p>
	Following complaints over the health and safety of the two suspended demonstrators, the drill was stopped temporarily, but work carried on later while the two layabouts continued to swing in their not so relaxing surroundings for the next 12 hours. </p>
<p>
	Contractors&rsquo; sub-sea-quent peace and quiet didn&rsquo;t last long though as another 13 protesters waded out to a platform on Wednesday (22nd) and this time three amphibious activists set up in a hammock, stopping the drilling platform being moved to a new location. Equipped with water and food, the last SchNEWS heard is that they were still there; hopefully the wind and waves won&rsquo;t take their toll too soon. </p>
<p>
	The direct action coincides with the lengthy hearing into the project, where Shell representatives and &lsquo;expert&rsquo; witnesses have been facing a grilling from locals and activists about the proposed raw gas pipeline. </p>
<p>
	Others from the local area and Rossport Solidarity Camp managed to stop Shell big wigs getting onto their hotel-bound bus last Thursday (16th) after a long day in the hearing. An 80-page submission this Wednesday (22nd) by An Taisce has called for the already extended hearing to be adjourned due to shady tactics by Shell. It remains to be seen whether this will get the result they want. </p>
<p>
	*See <a href="http://www.shelltosea.com" target="_blank">www.shelltosea.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(947), 109); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(947);

				if (SHOWMESSAGES)	{

						addMessage(947);
						echo getMessages(947);
						echo showAddMessage(947);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


