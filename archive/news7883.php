<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 788 - 16th September 2011 - Brighton 9: Sticking it to the Manikin</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, ukuncut" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 788 Articles: </b>
<p><a href="../archive/news7881.php">World's Largest Arms Fail</a></p>

<p><a href="../archive/news7882.php">Frack Shit Up</a></p>

<b>
<p><a href="../archive/news7883.php">Brighton 9: Sticking It To The Manikin</a></p>
</b>

<p><a href="../archive/news7884.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7885.php">Nice One Giza</a></p>

<p><a href="../archive/news7886.php">De-fault Is All Greece's</a></p>

<p><a href="../archive/news7887.php">Edl: Tommy Chickens Out</a></p>

<p><a href="../archive/news7888.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th September 2011 | Issue 788</b></p>

<p><a href="news788.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRIGHTON 9: STICKING IT TO THE MANIKIN</h3>

<p>
<p>  
  	Activists from the Gandhian non-violent, non-smashing shit up direct action campaign UK Uncut are on trial for, er. smashing shit up. The 9 were arrested during an occupation at the Top Shop and Top Man on Western Road, Brighton (yes that Brighton; the local one that&rsquo;s not in Egypt or Turkey or Israel or Palestine or Mexico or the Mezzanine region of Colombia, but Brighton, where we live).  </p>  
   <p>  
  	They are all accused of causing 4 grands worth of damage to a manikin in Topshop. If this figure seems high its not because of a bullshitting claim by tax-dodging scum Topshop, its because the these particular shop manikins were carved from the single gigantic diamond that Topshop&rsquo;s owners could afford after keeping all the lorryloads of cash the UK government let them have.  </p>  
   <p>  
  	In what could be seen as a rematch of last years EDO Decommisioners&rsquo; trial (see <a href='../archive/news729.htm'>SchNEWS 729</a>), Kellys Solicitors will once again be facing-off against Steven Shay of the CPS. The trial is being held at the Brighton Magistrates for about two weeks from Wednesday 14th. Given that Shay couldn&rsquo;t even get convictions on a group who admitted over &pound;180,000 worth of damage to an arms factory, we&rsquo;re hoping he can&rsquo;t do any better with a much shakier case.  </p>  
   <p>  
  	Defendants claim that the damage to the shop was not caused by the speak-truth-to-power peaceniks but rather by a squad of thuggish coppers who barged in their size 10 magnum boots waving their truncheons about. The defendants are also up for superglueing themselves to the shop&rsquo;s windows, which the prosecution will no doubt argue took several sprays of windolene and several wipes with a cloth to remove.  </p>  
   <p>  
  	In light of the hammering that people have received after the riots, the defendants are understandably concerned that their protest will be met with punitive sentences. However, the sketchy evidence of the prosecution compares badly with the&nbsp; &lsquo;stand up and be counted&rsquo; attitude of the protesters. Supporters of the Brighton 9 are confident of victory. A friend of the 9 said &ldquo;<em>this is a politically motivated prosecution, the state clearly has a problem with UK Uncut&rsquo;s ability to make the headlines and are seeking to make examples through cases such as this and the Fortnum 145. Luckily for us their evidence is falling apart already, hopefully it&rsquo;ll be laughed out of court sooner rather than later</em>&rdquo;.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1358), 157); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1358);

				if (SHOWMESSAGES)	{

						addMessage(1358);
						echo getMessages(1358);
						echo showAddMessage(1358);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


