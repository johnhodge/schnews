<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 675 - 8th May 2009 - Going Cheap On Amazon</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, peru, direct action, indigenous people, aidesep, amazon river, alan garcia, free trade agreement, alberto pizango chota" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 675 Articles: </b>
<p><a href="../archive/news6751.php">Brighton Rocked</a></p>

<p><a href="../archive/news6752.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6753.php">May Day Media Watch</a></p>

<p><a href="../archive/news6754.php">Sick Note</a></p>

<b>
<p><a href="../archive/news6755.php">Going Cheap On Amazon</a></p>
</b>

<p><a href="../archive/news6756.php">In Beds With The Enemy</a></p>

<p><a href="../archive/news6757.php">Coal Caravan  </a></p>

<p><a href="../archive/news6758.php">Droning On</a></p>

<p><a href="../archive/news6759.php">Raven'saint No More</a></p>

<p><a href="../archive/news67510.php">Shells Bells</a></p>

<p><a href="../archive/news67511.php">Nine Lives</a></p>

<p><a href="../archive/news67512.php">Roller Balls</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 8th May 2009 | Issue 675</b></p>

<p><a href="news675.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GOING CHEAP ON AMAZON</h3>

<p>
Throughout the last month the Amazonian region of Peru has erupted in protest with as many as 8,000 people from 1,350 indigenous communities blockading key waterways and road routes in the isolated oil and gas region and staging marches, pickets and occupations in surrounding towns and cities. Coordinated by the National Organisation of the Amazon Indigenous People of Peru (AIDESEP), the communities have organised against environmentally damaging exploitation of resources and the failure of the government to recognise indigenous rights. &nbsp; <br />  <br /> While in the cities of Iquitos and Atalaya government buildings and banks have been picketed and airstrips occupied, in the jungle region of Napo-Curraray locals have blockaded one of the main Amazonian tributaries servicing the Anglo-French oil company Perenco, which has a monopoly on oil production and development rights in the region. Now a month old, the blockade of cables and canoes has been breached twice. In the first weeks of the action two boats, including one from Perenco, forced their way through and shots were fired at pursuing protesters. Earlier this week a Peruvian Navy gunboat also breached the barricade, causing a number of injuries and sinking canoes. Community leaders are said to be outraged at the action and have condemned the military intimidation. The situation remains tense with two more gunboats said to be circulating in the area.      &nbsp; <br />  <br /> Similar protests took place in August last year when thousands of indigenous people brought Peruvian hydrocarbon production to a complete stop by blocking crude pumping installations, shutting down the only crude pipeline in Peru. The protests came to an end when the Peruvian congress voted to repeal two of the laws that threatened indigenous territories.&nbsp; <br />  <br /> The repealed laws were part of a raft of legislation passed without a congressional vote by Peruvian President Alan Garcia as part of a Free Trade Agreement with the US. Many more laws remain on the statutes which indigenous organisations claim will not only lead to a profit driven destruction of the rainforest but also dispossess hundreds of indigenous communities whose land titles have not yet been formalised by the Peruvian government. <br />  <br /> Although AIDESEP leaders have met with the president of the Peruvian congress, Javier Vel&aacute;squez - in talks they described as &lsquo;fruitless&rsquo; - they are holding out for &lsquo;transparent&rsquo; negotiations, preferably with Garcia himself. The demand seems unlikely to be met though as Garcia, one of the few remaining adherents to US Free Trade Agreements left amongst Latin American leaders, met with Perenco chairman Francois Perrodo less than a fortnight ago to discuss further exploration and drilling. After Perrodo pledged to invest $2billion in the region the government passed a law which declared Perrenco&rsquo;s work a &lsquo;national necessity&rsquo;.    &nbsp; <br />  <br /> Despite the intimidation and Garcia&rsquo;s pandering to big business, the communities aren&rsquo;t backing down - in particular the Napo protesters who have signalled their intent to step up the campaign and begin blocking more tributaries. &ldquo;<em>This is not only about indigenous rights but also the basic human right to live in peace&rdquo; said AIDESEP president Alberto Pizango Chota. &ldquo;We&rsquo;re not seeking confrontation but to simply be allowed to protect our environment, our homes and our lives</em>.&rdquo;   &nbsp; <br />  <br /> * See also <a href="http://www.aidesep.org.pe," target="_blank">www.aidesep.org.pe,</a> <a href="http://www.survival-international.org" target="_blank">www.survival-international.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>aidesep</span>, <span style='color:#777777; font-size:10px'>alan garcia</span>, <span style='color:#777777; font-size:10px'>alberto pizango chota</span>, <span style='color:#777777; font-size:10px'>amazon river</span>, <a href="../keywordSearch/?keyword=direct+action&source=675">direct action</a>, <span style='color:#777777; font-size:10px'>free trade agreement</span>, <span style='color:#777777; font-size:10px'>indigenous people</span>, <span style='color:#777777; font-size:10px'>peru</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(321);
			
				if (SHOWMESSAGES)	{
				
						addMessage(321);
						echo getMessages(321);
						echo showAddMessage(321);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>