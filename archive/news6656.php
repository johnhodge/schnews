<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 665 - 6th February 2009 - Wildcat Flap</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, wildcat stirke, oil industry, lindsay oil refinery, globalisation, british national party, strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 665 Articles: </b>
<p><a href="../archive/news6651.php">Sheik Battle & Rule</a></p>

<p><a href="../archive/news6652.php">Bye-byelaw!  </a></p>

<p><a href="../archive/news6653.php">Dirty Tricks</a></p>

<p><a href="../archive/news6654.php">Fitted Up</a></p>

<p><a href="../archive/news6655.php">Tank In Your Tiger</a></p>

<b>
<p><a href="../archive/news6656.php">Wildcat Flap</a></p>
</b>

<p><a href="../archive/news6657.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 6th February 2009 | Issue 665</b></p>

<p><a href="news665.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>WILDCAT FLAP</H3>

<p>
Last week&#8217;s week wave of wildcat strikes affecting power stations, oil refineries, nuclear plants and other engines of Babylon - should have been a wet dream for the workerist left but was unfortunately complicated  by the adoption as a slogan of Gordon Brown&#8217;s  pre-credit crunch boast of &#8220;British Jobs for British Workers&#8221;. This was enough to throw sections of the left into disarray, with one tiny crypto-trotskyite splinter group Workers Liberty even organising a picket of UNITE&#8217;s London HQ.  <br />
 <br />
Protests at the TOTAL run  Lindsay oil refinery centred around subcontractors IREM deciding to employ Italian and Portuguese workers. No doubt IREM hoped to save a few quid by bypassing industry wide pay rates and bringing in cheaper labour. One of international capital&#8217;s main weapons against workers is the &#8216;race to the bottom&#8217; normally involving jobs being exported, like call-centres bring relocated to Bangladesh). But the problem with construction is that it&#8217;s impossible to export &#8211; if you want that power station built here you are going to need local labour.  <br />
 <br />
Of course the construction industry in the U.K has in recent years been remarkably multi-ethnic &#8211; with workers drawn from all across the whole of the EU and beyond. In the boom-time this really only affected unskilled labourers. But as the recession bites and work becomes scarcer competition kicks in higher up the skills ladder. And that is essentially what the dispute is about. <br />
 <br />
The picture on the ground was confused &#8211; firstly by the mainstream press who insisted that the strike was largely anti-foreigner rather than anti-boss. In fact BNP organisers were kicked off picket lines early on at Lindsay, with one worker commenting on strike web forum <a href="http://www.bearfacts.co.uk" target="_blank">www.bearfacts.co.uk</a> &#8220;<i>There&#8217;s nowt working class about the BNP and if the strike had been about anything else where they couldn&#8217;t stick their spin on it then they wouldn&#8217;t  be supporting it. The strike's anti-racist. It&#8217;s the bosses being racist and fascist. If the BNP turn up at our picket when we walk out then I know a few of us won&#8217;t be standing for it and will tell them to f**k off in no uncertain terms.&#8221;. Another South Wales refinery worker said &#8220;In the engineering industry, especially in oil and gas, the job takes you worldwide. Who can blame foreign workers, who also face threats of redundancy and unemployment, who just want to work and support their family? What happened to international worker solidarity?</i>&#8221;
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777'>wildcat stirke</span>, <span style='color:#777777'>oil industry</span>, <span style='color:#777777'>lindsay oil refinery</span>, <span style='color:#777777'>globalisation</span>, <a href="../keywordSearch/?keyword=british+national+party&source=665">british national party</a>, <span style='color:#777777'>strike</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(220);
			
				if (SHOWMESSAGES)	{
				
						addMessage(220);
						echo getMessages(220);
						echo showAddMessage(220);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>