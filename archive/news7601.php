<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 760 - 25th February 2011 - Cameron Waves Arms About </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, middle east, arms trade, libya, bahrain, student protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 760 Articles: </b>
<b>
<p><a href="../archive/news7601.php">Cameron Waves Arms About </a></p>
</b>

<p><a href="../archive/news7602.php">Ukba Minds Its Pease For New Sussex Detention Centre</a></p>

<p><a href="../archive/news7603.php">Rough Diamond</a></p>

<p><a href="../archive/news7604.php">Off Their Leeds</a></p>

<p><a href="../archive/news7605.php">Last Days Of The Taj</a></p>

<p><a href="../archive/news7606.php">Belgium: Migrants Revolt</a></p>

<p><a href="../archive/news7607.php">Bristol: A Hub Of Activity</a></p>

<p><a href="../archive/news7608.php">Airy (un)friendly</a></p>

<p><a href="../archive/news7609.php">Time To Bail</a></p>

<p><a href="../archive/news76010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/760-cameron-lg.jpg" target="_blank">
													<img src="../images/760-cameron-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 25th February 2011 | Issue 760</b></p>

<p><a href="news760.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CAMERON WAVES ARMS ABOUT </h3>

<p>
<p>
	<strong>IN MIDDLE EAST: ARMS FAIR IN LOVE AND WAR (TORN REGIONS)</strong> </p>
<p>
	As Egypt and its martial law swiftly falls down the mainstream rolling news agenda it has been replaced by even more dramatic events in Libya. While the Libyan people continue their life and death struggle against brutal dictator/deflated blow-up doll Ghaddafi, all these tottering dictatorships are causing a few awkward moments for the government and UK plc. On the one hand they like to throw out soundbites claiming how happy they are that these oppressed peoples are demanding the same freedoms and rights as we &lsquo;enjoy&rsquo; over here, and their hopes for &ldquo;an orderly transition&rdquo; - on the other hand they have to remain extremely quiet about how &lsquo;we&rsquo; have been supporting these same dictators for years, happily flogging them the arms they&rsquo;ve used to enforce their reigns of terror while siphoning off their resources. </p>
<p>
	In fact, in recent years North Africa and the Middle East have been especially targeted as hot postcodes by European arms sellers looking for new custom. Its been boom time for companies shipping weaponry to the dictators in Algeria, Tunisia, Libya and Morocco with sales quadrupling in the past five years, from Eur 375 to Eur 2bn last year. Ker-ching. </p>
<p>
	Britain has been keen to hustle its way to the front of the queue. In the past few months alone, the UK Trade &amp; Investment Defence &amp; Security Organisation (UKTI DSO) has been only too happy to approve the sale of weapons, tear gas and crowd control ammunition to both Libya and Bahrain. The UK has been more than keen to flog Ghaddafi the tools of terror, making Libya a UKTI/DSO &lsquo;priority market country&rsquo;, with the government making &lsquo;high level political interventions&rsquo; (presumably including releasing the Lockerbie bomber &ndash; who didn&rsquo;t actually do it but that&rsquo;s another story) all in support of arms sales and cosying up on the oil companies&rsquo; behalf. There&rsquo;s nothing wrong with a dictator &ndash; as long as he&rsquo;s a dictator you can do a deal with. </p>
<p>
	Just last year Ghaddaffi popped into British Harm Stores and ordered wall and door breaching projectile launchers, sniper rifles, crowd control ammunition, small arms ammunition (perhaps for the Eur 79m&rsquo;s worth of small arms supplied by Berlusconi in 2009?), tear gas and irritant ammunition. All handy when you want to crack down on any protest or resistance. The empty political rhetoric of an &lsquo;ethical&rsquo; arms policy, and that weapons are &ldquo;covered&rdquo; by assurances that they &ldquo;would not be used in human rights repression&rdquo;, would be laughable if it wasn&rsquo;t for an internet full of evidence of Gaddafi&rsquo;s violent repression. </p>
<p>
	Still, to an arms dealer, there&rsquo;s no such thing as a problem, only an opportunity. After a regime change, or during a bit of social turmoil, the &lsquo;market&rsquo; gets a shake up. Some profitable old trade relationships might be blown, but new potential customers replace them - you&rsquo;ve just got to get out there and meet &lsquo;em! </p>
<p>
	Cue David Cameron flying into Egypt, with the blood on the streets barely dry, to begin his whistle stop tour of the troubled region. He was accompanied by top executives from the UK&rsquo;s arms companies &ndash; like intrusive coffin-salesman coming round when the &lsquo;deceased&rsquo; hasn&rsquo;t actually quite kicked the bucket yet. </p>
<p>
	Whilst he did a nice PR job of popping into Kuwait to make an impassioned plea for his humanitarian desire to support its &lsquo;democracy&rsquo; by selling it the weapons to protect itself (from local aggressors that we most likely also arm), the real reason for the trip was for a top level boost to UK arms companies attending this week&rsquo;s International Defence Exhibition and Conference (IDEX), Abu Dhabi. </p>
<p>
	The region&rsquo;s largest arms fair &ndash; where 10% of the exhibitors were Brits - was the perfect showroom to drum up new business and make a killing &ndash; never mind the embarrassment of what&rsquo;s actually happening on the streets, where the customers are busy making killings of their own. </p>
<p>
	For more establishment figures tut-tutting their concern for the people fighting these regimes, waffling about supporting freedom in the Middle East, see: the media. For more on the UK being up to its bloody neck in supporting repression the world over, see: <a href="http://www,caat.org.uk" target="_blank">www,caat.org.uk</a> </p>
<p>
	* LSE students began a series of occupations on Tuesday (22nd) to protest ties between the University and the Libyan regime. So far, common rooms and the office of Director Howard Davies have been occupied. The University recently received &pound;300k out of &pound;1.5m awarded by the Gaddafi International Charity and Development Foundation, presumably for having taken the Colonel&rsquo;s son Saif as a student. The occupiers are calling for the university to reject the remaining cash, revoke Saif&rsquo;s alumni status and award the unspent &pound;300k on scholarships for Libyan students. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1115), 129); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1115);

				if (SHOWMESSAGES)	{

						addMessage(1115);
						echo getMessages(1115);
						echo showAddMessage(1115);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


