<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 789 - 23rd September 2011 - RIP: Troy Anthony Davis</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, usa" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 789 Articles: </b>
<p><a href="../archive/news7891.php">Flying The Flag</a></p>

<p><a href="../archive/news7892.php">Egypt: Sheikhing The System</a></p>

<p><a href="../archive/news7893.php">Too Late To Dale Out</a></p>

<p><a href="../archive/news7894.php">Glue-me Retail Outlook</a></p>

<p><a href="../archive/news7895.php">Dogs Of War</a></p>

<p><a href="../archive/news7896.php">Sparks Fly</a></p>

<b>
<p><a href="../archive/news7897.php">Rip: Troy Anthony Davis</a></p>
</b>

<p><a href="../archive/news7898.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd September 2011 | Issue 789</b></p>

<p><a href="news789.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RIP: TROY ANTHONY DAVIS</h3>

<p>
<p>  
  	Troy Anthony Davis was executed in the early hours of Thursday 22nd at a prison in Butts County, Georgia (No, not an appropriate time to look for a cheap laugh about &lsquo;Butts County&rsquo;).&nbsp; This was despite more than&nbsp; a million people (including the Pope, former US Pres. Jimmy Carter, former FBI Director William Sessions and Archbishop Desmond Tutu) signing a petition against the state-sanctioned murder.  </p>  
   <p>  
  	On 18th August 1989 two people were shot in Savannah, Georgia &ndash; one of whom (police officer Mark MacPhail) died. Several &lsquo;witnesses&rsquo; identified Davis as the shooter and, on 23rd August, he handed himself in.  </p>  
   <p>  
  	Despite no forensic evidence, no murder weapon and one of the witnesses admitting under cross-examination that he only identified Davis because of police threats made to him, Davis was found guilty and sentenced to death in August 1991.  </p>  
   <p>  
  	Davis continued to maintained his innocence; 7 of the 9 witnesses who testified against him changed their story or recanted, and jurors who found Davis guilty publicly said they now think he was innocent. Over the past 20 years numerous appeals were denied, mostly in attempts to save face: in the state of Georgia it is easier to execute a potentially innocent man than open a Pandora&rsquo;s box of police coercion, inadequate defence funding and a biased judiciary.  </p>  
   <p>  
  	On the day of Troy Davis&rsquo;s death, the prosecutor in his case said of the situation, &ldquo;all of it is exquisitely unfair&rdquo; &ndash; unfortunately he was referring to the media attention on his work, not the legal stitch up that robbed Davis of his life.  </p>  
   <p>  
  	Troy himself was more magnanimous; to the last he maintained his innocence, imploring MacPhail&rsquo;s family to &ldquo;look deeper into this case so that you really can finally see the truth.&rdquo; His final words were he saved for his executioners asking&ldquo;may God bless your souls.&rdquo; Amen.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1370), 158); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1370);

				if (SHOWMESSAGES)	{

						addMessage(1370);
						echo getMessages(1370);
						echo showAddMessage(1370);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


