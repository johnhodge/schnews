<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 682 - 3rd July 2009 - Squats Up: South West London Squat Round Up</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, squatting, ann keen mp, alan keen mp, kew bridge, tyting farm" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 682 Articles: </b>
<p><a href="../archive/news6821.php">Rumble In The Jungle</a></p>

<p><a href="../archive/news6822.php">Coup Blimey</a></p>

<p><a href="../archive/news6823.php">Don&#8217;t Coal Home  </a></p>

<p><a href="../archive/news6824.php">A Lot To Ansar For</a></p>

<p><a href="../archive/news6825.php">Island Mentality  </a></p>

<p><a href="../archive/news6826.php">Sez Who?</a></p>

<p><a href="../archive/news6827.php">Ich Bin Ein Burnin</a></p>

<p><a href="../archive/news6828.php">Lions And Tigers</a></p>

<b>
<p><a href="../archive/news6829.php">Squats Up: South West London Squat Round Up</a></p>
</b>

<p><a href="../archive/news68210.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd July 2009 | Issue 682</b></p>

<p><a href="news682.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUATS UP: SOUTH WEST LONDON SQUAT ROUND UP</h3>

<p>
<strong>KEEN SQUATTERS</strong> <br />  <br /> This week a house was squatted in West London owned by Ann and Alan Keen, Labour MPs up to their eyeballs in the expenses scandal. It had been empty for up to a year, with the Keens living at their posh pad nearer to Parliament - which they had claimed &pound;140,000 for over the last four years, as their &lsquo;second home&rsquo;. On top of that, Alan Keen had an office at the back of the house, which he was claiming &pound;250 monthly travel expenses to get to &ndash; even though it was in the same house they were calling their &lsquo;main residence&rsquo;. In Alan&rsquo;s so-called office, squatters found a dusty old computer lying around. <br />  <br /> While the old Bill visited last Saturday &ndash; doing nothing - the owners haven&rsquo;t started eviction proceedings yet. What they have done is letter dropped around the area with a notice saying that their house has been occupied by squatters, that it is empty because the builders went into liquidation, and work will start in two weeks and they will move back in the summer. Those letters are yet more taxpayers&rsquo; money down the drain. It&rsquo;s also suspected that the Keens haven&rsquo;t paid the broke builders, and squatters found the house as a partial building site. They are currently getting it liveable.&nbsp; <br />  <br /> The squat has had a lot of mainstream publicity, riding on the furore about MP expenses. The house is in the seat of Brentford &amp; Isleworth, where Ann Keen is the local MP. To gauge the true feelings of her constituency, the squatters are holding a public meeting outside the front gates of the house this Sunday (5th), 3pm. <br /> Also, tonight (3rd), is &lsquo;Having A Laugh At Your Expenses&rsquo;, a comedy benefit gig at the house featuring Mark Thomas, Attila The Stockbroker and Wil Hodgson. The money goes to refugee support. Starts 8pm, &pound;4. <br />  <br /> The house is at 38 Brook Rd South, Brentford, TW8 0NN, Site mob 07912078757 <br />  <br /> <strong>KEW BRIDGE</strong> <br />  <br /> The Kew Bridge squatted eco-community in Brentford, West London is going strong after being occupied on June 6th (See <a href='../archive/news679.htm'>SchNEWS 679</a>). The community has been set up in the middle of a urban landscape with benders, yurts, compost loo and garden to prove a model of what can be achieved. It will be used as a community garden and educational centre. The local response has been overwhelmingly positive.&nbsp; <br />  <br /> It was empty for 23 years and is currently owned by St Georges &ndash; whose pipe dream is to build an eight story 168-unit building, despite the community not wanting the development, and the plan being refused permission. There is a public consultation process two weeks from now. <br />  <br /> Events are being held all week: Wednesday nights 7pm-9m is workshops on a range of topics from bike generators to chanting, Tuesday and Saturday 11am-8pm are gardening days, Wednesday 11am-8m is community structure building. All are welcome. <br />  <br /> The place is not currently under direct threat of eviction, so let&rsquo;s hope they get the chance to get it fully up and running. Visit Kew Bridge Road, Brentford, TW8 0JF, site mob 07515166011. <br />  <br /> * See also <a href="http://www.facebook.com/reqs.php#/group.php?gid=88020757939" target="_blank">www.facebook.com/reqs.php#/group.php?gid=88020757939</a> <br />  <br /> <strong>TYTING FARM</strong> <br />  <br /> The squatted 118-acre Tyting Farm near Guildford, Surrey, was evicted this Monday after eight and a half weeks. This site was notable for being occupied by The Diggers in 1649 - exactly 360 years ago - and is near St Georges Hill, made famous by Gerard Winstanley&rsquo;s Digger community.&nbsp; <br />  <br /> On Monday a group of around 35 bailiffs and police arrived, and bailiffs began to evict the eleven occupants, gratuitously trashing and burning their benders and dwellings, and ripping plants out of their beds.&nbsp; <br />  <br /> The site was taken on April 26th, and was the setting for a Rainbow Gathering in early May. After two court adjournments &ndash; due to the defendants drawing on historical documents - a possession order was finally issued two weeks ago at Guildford County Court. <br />  <br /> The squatters&rsquo; case - that Tyting Farm is common land - was based on a 1915 covenant by then-owner Duke of Northumberland bequeathing the land to the people of London and Guildford, remaining on condition of sale. In 1942 Guildford Council acquired the land for its protection. Part of the building dates back to a chapel built in 1300. Either way, despite the eviction, public access remains, and in fact the site could be re-occupied. <br />  <br /> The on-the-case network of SW London/Surrey squatters have had an active few months, with the historic Ravens Ait island on the Thames near Kingston being occupied from February to May (See <a href='../archive/news675.htm'>SchNEWS 675</a>), as well as a cross-over with the West London lot occupying Kew Bridge and the Keen house. After a brief moment to recharge their batteries, look out for another squat in the area soon.&nbsp; <br />  <br /> * Site mob 07842137535 email <a href="mailto:savetytingfarm@googlemail.com">savetytingfarm@googlemail.com</a> <br />  <br /> * See <a href="http://www.tytingcommunityproject.org.uk," target="_blank">www.tytingcommunityproject.org.uk,</a> <a href="http://www.facebook.com/group.php?gid=80809116892" target="_blank">www.facebook.com/group.php?gid=80809116892</a> <br />  <br /> * For more about the history of Tyting Farm see <a href="http://www.savetytingfarm.com" target="_blank">www.savetytingfarm.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>alan keen mp</span>, <span style='color:#777777; font-size:10px'>ann keen mp</span>, <span style='color:#777777; font-size:10px'>kew bridge</span>, <a href="../keywordSearch/?keyword=squatting&source=682">squatting</a>, <span style='color:#777777; font-size:10px'>tyting farm</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(391);
			
				if (SHOWMESSAGES)	{
				
						addMessage(391);
						echo getMessages(391);
						echo showAddMessage(391);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>