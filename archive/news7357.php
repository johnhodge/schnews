<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 735 - 20th August 2010 - Going Out On A High</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, protest camp, highgate farm, vivisection" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 735 Articles: </b>
<p><a href="../archive/news7351.php">High Pressure Front</a></p>

<p><a href="../archive/news7352.php">Climate Camp Scotland</a></p>

<p><a href="../archive/news7353.php">Holy Waters</a></p>

<p><a href="../archive/news7354.php">Wrekin Havok</a></p>

<p><a href="../archive/news7355.php">Hammertime</a></p>

<p><a href="../archive/news7356.php">Rattling The Coppers</a></p>

<b>
<p><a href="../archive/news7357.php">Going Out On A High</a></p>
</b>

<p><a href="../archive/news7358.php">Dirty Sheets</a></p>

<p><a href="../archive/news7359.php">Mapuche Hunger Strikes</a></p>

<p><a href="../archive/news73510.php">Nationalist Express</a></p>

<p><a href="../archive/news73511.php">Gaza Defendant Appeal</a></p>

<p><a href="../archive/news73512.php">Taking The Ciss</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th August 2010 | Issue 735</b></p>

<p><a href="news735.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GOING OUT ON A HIGH</h3>

<p>
<p>
	After the success of last year&rsquo;s camp (see <a href='../archive/news686.htm'>SchNEWS 686</a>) at Highgate Farm in Lincolnshire - which breeds rabbits and ferrets for vivisection - another protest camp was held this week. Protesters occupied public land next to the farm but police said that due to previous protests and complaints from the Lamborghini-driving owner of the farm, Geoffrey Douglas, the camp would violate section 42 of the Criminal Justice and Police Act 2001 (brought out to stop home demos). </p>
<p>
	To get around this the protest camp was to be between the hours of 7am-7pm each day. Like the previous camp, the protest was non-violent. </p>
<p>
	On the second day police arrested two protesters for using a megaphone &ndash; charging them with &lsquo;contravening a direction given by a police officer&rsquo;, and banning them from Lincolnshire until the case goes to court! Then they banned the remaining protesters from going to Highgate Farm for 28 days &ndash; presumably under section 42 of the CJPA &ndash; it remains to be seen whether this was lawful. </p>
<p>
	So the camp is gone, but the farm continues &ndash; and would probably love more visits from animal rights campaigners. It&rsquo;s at Highgate Lane, Normanby-by-Spital, Market Rasen, LN8 2HQ. </p>
<p>
	For more call 07973722978 or 07973722978 or see <a href="http://www.closehighgatefarm.com" target="_blank">www.closehighgatefarm.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(908), 104); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(908);

				if (SHOWMESSAGES)	{

						addMessage(908);
						echo getMessages(908);
						echo showAddMessage(908);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


