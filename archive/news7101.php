<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 710 - 19th February 2010 - Gaza Defendants Hammered</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, israel, palestine, riots" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 710 Articles: </b>
<b>
<p><a href="../archive/news7101.php">Gaza Defendants Hammered</a></p>
</b>

<p><a href="../archive/news7102.php">Block 'n' A.w.e  </a></p>

<p><a href="../archive/news7103.php">Rossport: Lobster Plot</a></p>

<p><a href="../archive/news7104.php">Calais: Hangar-ing On</a></p>

<p><a href="../archive/news7105.php">Never Ending Tory  </a></p>

<p><a href="../archive/news7106.php">Heckler Put Down </a></p>

<p><a href="../archive/news7107.php">Holloway To Go</a></p>

<p><a href="../archive/news7108.php">Sdl: Jocks-trap</a></p>

<p><a href="../archive/news7109.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/710-aldermaston-lg.jpg" target="_blank">
													<img src="../images/710-aldermaston-sm.jpg" alt="I Heart Nukes" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 19th February 2010 | Issue 710</b></p>

<p><a href="news710.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GAZA DEFENDANTS HAMMERED</h3>

<p>
<strong>AS JUDGE-MENTAL DENNIS SENTENCES GAZA PROTESTERS</strong> <br />  <br /> The state has begun handing down vicious sentences to men accused of participation in the rioting in London that occurred during the weeks of protest against the Israeli attacks on Gaza (see <a href='../archive/news661.htm'>SchNEWS 661</a><a href='../archive/news.htm'></a>-<a href='../archive/news662.htm'>662</a>). Ten young men have been jailed for their role in protests demanding an end to Israel&rsquo;s invasion of Gaza early last year &ndash; and more are to follow in the coming weeks. <br />  <br /> In total, 91 were arrested, the vast majority young Muslim men and in most cases long after the event. Altogether 73 have been charged with violent disorder following the demonstrations that took place over the three weeks of Operation Cast Lead. Fifty have already pleaded guilty amid accusations of police intimidation and denial of access to solicitors. <br />  <br /> Ten have been sentenced so far, some for as long as 2 and a half years, despite recommendations for leniency by the probation service. Among those sentenced so far are two Palestinian men, one of whom had two cousins killed by the Israeli army during the offensive. <br />  <br /> All bar one were sentenced by the same judge (Judge Dennis) for the same reason, which he regurgitated in a carefully orchestrated speech. He said the custodial sentences showed  &ldquo;the public are entitled to look to the law&hellip; for a deterrent message&rdquo;. <br />   <br /> SchNEWS spoke to a monitor from the Legal Defence Monitoring Group who has been attending the trials. <br />   <br /> <em>&ldquo;The Judge has committed the defendants to jail because he can- there were no obstacles placed in his or the State&rsquo;s way. Indeed, the judge indicated his participation in the state&rsquo;s project when he told all the defendants at the plea hearings that he had viewed all the evidence from police witnesses and independent (!) footage from Sky News and on that basis it would be best that they pleaded guilty - even before waiting to hear their defence! <br />   <br /> &ldquo;There were no obstacles to the police being able to conduct themselves in this way. There were no pre-planned counter surveillance teams like FIT Watch. Virtually none of the protesters brought kits to mask up. There were very few volunteers handing out bust cards detailing protesters&rsquo; legal rights when threatened by police with violation of their liberty. There were no legal observers or parliamentary observers like at Climate Camp. <br />  <br /> &ldquo;If these had been available then the police would have found it more difficult during and after the protests. More people could have gone to the effort of submitting civil claims against the police for damages - a long and arduous process no doubt - and submitted civil claims as evidence of the general conduct of police on the day. <br />  <br /> &ldquo;There is no doubt that many of those accused have received appalling legal advice &ndash; pleading guilty without even seeing the evidence against them. They were perhaps just throwing placard sticks at fully armoured police &ndash; not realistically threatening them. <br />  <br /> &ldquo;At least some defence QCs have tried to explain why the protests arose and the context. Unfortunately Judge Dennis did not pick up on this. After all, on sentencing the Palestinian who lost two family members in Gaza, the Judge said he took into account the emotional impact upon the protester of losing his family&hellip;&ldquo;in Israel&rdquo; - before sentencing him to 18 months. <br />  <br /> &ldquo;Of those charged, very few come from self-proclaimed &lsquo;activist community&rsquo; , schooled in the state&rsquo;s tactics. Whilst this is intended to stop Muslims from protesting at all, we hope it teaches us all another lesson. The Metropolitan Police cannot and should not be trusted. There can be no negotiation. Instead of turning to the Police for justice we should be prepared to trust in ourselves to defend ourselves. Just like Climate Camp, these protests need more legal observers with video cameras taking comprehensive notes of what takes place. Only then will we be able to prevent this happening again. Without this, the police will be able to continue to selectively edit the evidence they produce in order to remove the context: their provocation of the crowd.&rdquo;</em> <br />  <br /> The main demonstration on 10th of January saw 100,000 people marched from Trafalgar Square towards West London on their way to the Israeli Embassy without any incidents of violence on their part. When they got near to Hyde Park, the police kettled and beat protesters under Hyde Park The protesters were also harassed when they left the bypass. Forward Intelligence Team (FIT) and other evidence gathering teams (EGT) thrust cameras in people&rsquo;s faces. Police stopped and searched people without having any suspicion of them committing an offence under Section 44 of the terrorism act, which has since been ruled illegal by the European Courts (see <a href='../archive/news705.htm'>SchNEWS 705</a>) <br />  <br /> In the months following the protests many were arrested in dawn raids on their houses after the police scoured hours of CCTV footage for snippets of violence. Whilst there was plenty of self-flagellation by the police following their handling of the G20 protests later on in the year, no such forensic examination of the much worse policing at the Gaza protests took place. The protesters were all Muslim, and for, the most part, all of working class immigrant origin. The state knows  it would be easy to use the media to portray them as anti-Semitic nutters and get away with provoking them. <br />   <br /> Further sentencing will take place on Friday 19th and Friday 26th  February from 10am at Isleworth Crown Court, west London, TW7 5LP (the nearest station is Isleworth). There will be solidarity rallies outside the court.  <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=710">gaza</a>, <a href="../keywordSearch/?keyword=israel&source=710">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=710">palestine</a>, <a href="../keywordSearch/?keyword=riots&source=710">riots</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(660);
			
				if (SHOWMESSAGES)	{
				
						addMessage(660);
						echo getMessages(660);
						echo showAddMessage(660);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


