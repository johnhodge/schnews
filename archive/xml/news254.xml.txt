<?xml version="1.0"?>
<issue>
<issueno>254</issueno>
<date>2000-04-07 00:00:00</date>
<wakeup>WAKE UP! WAKE UP! IT&apos;S YER HITTING THE ROOF</wakeup>
<article>
<headline>Blobby&apos;s House Party</headline>
<content>

<p><strong><q>The government is creating an army of middle-class
 Swampies. The protests against these new developments will make the
 Newbury Bypass campaign look like a teddy bears picnic</q><br />
 Thomas Newell, Estate Agent</strong></p>
 
 <p><strong>&quot;If it means lying down in front of bulldozers,
 I&apos;ll be there&quot;</strong><br />
 <strong>Damian Green, Conservative MP for Ashford</strong></p>
 
 <p>Estate Agents and Tory MP&apos;s threatening Direct Action? Surely
 not! But, strange as it may seem, something of a rebellion is brewing
 in the county towns and leafy suburbs of Southern England, brought
 about by the government&apos;s decision to build 43,000 new homes in
 the area every year until 2016; over 700,000 houses in a region of the
 country already groaning under the strain of massive over-development.
 Across the south-east, people are bracing themselves for the onslaught
 of housing developers and road-builders, and this time it seems that
 it won&apos;t just be yer usual eco-warrior types on the front
 line. Local opposition to greenbelt development has been steadily
 building over the past few years. In 1998, a planned greenfield
 housing scheme in <location><city>Peacehaven</city></location> was
 stopped after local people, including the mayor, teachers, families
 and pensioners threatened to &apos;lock on and be arrested&apos;
 (<backissue issueno="164">SchNEWS 164</backissue>). More recently,
 when a protest camp, set up to stop 66 luxury houses being build at
 <location><city>Hockley</city></location>, was surrounded by security
 barricades, locals stormed through with food and tat for the besieged
 protesters (<backissue issueno="249">SchNEWS 249</backissue>). These
 were isolated incidents; this time round the grassroots protests are
 likely to be much more widespread, and looking at the sheer scale of
 the proposed developments, it&apos;s easy to see why.</p>
 
 <p>For example, Ashford, a town with a population of 55,000, would
 almost treble in size to 150,000, while between Horsham and Crawley in
 West Sussex, an area of greenbelt 5 miles long and 2 miles wide would
 vanish under 45,000 new houses. It has been estimated by the <org>Council
 for the Protection of Rural England</org> (CPRE) that over 430 square
 kilometres of countryside in the south-east are under threat.</p>
 
 <p>You may well be wondering why these huge numbers of new houses are
 actually needed, when so many are standing empty or derelict. Well,
 it&apos;s that old chestnut &apos;predict and provide&apos;, once so
 beloved of the road planners, based on a completely false concept of
 household formation that takes no account of empty and under-occupied
 houses. Or, for that matter, second homes. There are roughly 250,000
 homeless households in Britain, and 224,000 second homes. As
 <person>George Monbiot</person> noted: <q>The similarity of the
 numbers is no coincidence.  Every time a second home is purchased,
 another family is shoved out of the housing market. Rich people from
 the cities turn up in villages and buy up the houses at prices that
 local people couldn&apos;t possibly afford.</q> It has also been
 estimated that up to 26,500 new homes could be provided each year by
 converting old commercial and office buildings and redeveloping
 existing housing.</p>
 
 <p>All this is blatantly ignored by the house and road building
 lobbies, who assume that a nice new house and a convenient road to the
 out-of-town superstore will keep the natives happy, and that any
 protests will be small scale and localized. Wrong! If and when these
 crazy schemes get the go ahead, then the developers and the government
 will find themselves up against a direct action movement far beyond
 anything they&apos;ve encountered before. In the words of Tony Burton
 of CPRE: &apos;The touchpaper has been lit and the fuse is burning. If
 he is not careful this is going to explode in <person>John
 Prescott</person>&apos;s face&apos;.</p>
 
 <p><em>Have other SchNEWS readers noticed a similarity between Mr. Blobby
 and Mr. Prescott?</em></p>

</content>
</article>

<article>
<headline>Home Alone</headline>
<content>

<p>There was nothing about empty properties in this week&apos;s green
 paper on housing, despite latest figures that show 765,000 empty homes
 in England, 90,000 of which are in the South East. (There was nothing
 in the green paper on second homes either).</p>
 
 <p>Also worth considering is that as available housing becomes scarce,
 deposits and rents go up, less landlords are willing to take housing
 benefit, and if you haven&apos;t got pots of money there could be
 trouble ahead. So who owns all these empties? Well, step forward the
 gov&apos;t, who as England&apos;s most wasteful landlord are hardly
 setting a positive example to anyone. Currently around one fifth of
 their properties are standing empty.</p>
 
 <p>But don&apos;t give up all hope. April 10th-14th is South East
 Action on Empty Homes. <org>Community Action on Empty Homes</org> (a
 project run by the <org>Empty Homes Agency</org>) are setting up a
 mock Estate Agents in <location><city>Guildford</city></location>
 (home of the Government housing office for the South East).  But the
 publicity stunt isn&apos;t aiming to directly find homes for
 people. When SchNEWS rang up to get a list of empty properties in
 Brighton, the EHA told us that they couldn&apos;t pass it on because
 <q>Justice? ran a squatters estate agents, and we can&apos;t encourage
 squatting.</q>! (SchNEWS <backissue issueno="64">64</backissue> &amp;
 <backissue issueno="65">65</backissue>)</p>
 
 <p>Positive solutions to housing problems do exist - squatting, housing
 co-ops/associations, eco-villages. Check out:</p>
 
 <ul>
 
 <li><org>Council for Protection of Rural England</org>, <phone>020 7976 6373</phone>
 <web>http://www.greenchannel.com/cpre/</web></li>
 
 <li><org>Empty Homes Agency</org>, <phone>020 7928 6288</phone>, email
 <email>caeh@eha.globalnet.co.uk</email></li>
 
 <li><org>URGENT</org>, Box HN, 111 Magdalen Rd, Oxford OX4 1RQ tel:
 <phone>01865 794800</phone> <web>http://www.urgent.org.uk/</web>
 Sustainable housing policies, info, advice.</li>
 
 <li><org>Advisory Service for Squatters</org>, 2 St Paul&apos;s Rd,
 London N1 2QN, <phone>020 7359 8814</phone>
 <web>http://www.squat.freeserve.co.uk/</web></li>
 
 <li><org>Radical Routes</org>, <phone>0113 262 9365</phone> Info etc
 on housing
 co-ops. <web>http://www.home.clara.net/carrot/rrpub/info.htm</web></li>
 
 <li><org>Groundswell</org>, 5-15 Cromer St., London, WC1H 8LS
 <phone>020 7713 2880</phone>
 <web>http://www.oneworld.org/groundswell/</web>. Part of the <org>National
 Homeless Alliance</org>, supporting self-help initiatives with homeless
 people and those living in poverty. &quot;Unless people experiencing
 poverty really begin to do something, nothing is going to
 change!&quot;</li>
 </ul>
 
 <p><org>Defend Council Housing</org> are an umbrella group
 fighting the mass transfer of council housing to private housing
 corporations. They are organising a series of Conferences across the
 country covering topics like &apos;alternatives to stock
 transfers&apos; and &apos;why is New Labour selling our homes?&apos;
 For details ring <phone>020 7254 2312</phone>
 <web>http://www.defendcouncilhousing.org.uk/</web></p>

</content>
</article>

<article>
<headline>Pick-Pockets</headline>
<content>

<p>Imagine this... you&apos;re doing your weekly shopping. You&apos;ve got
 just &#163;25 to spend, you can only spend it on designated items and if
 there should happen to be change left over the store will keep it
 themselves! An estimated 4,000 retailers have signed up to the asylums
 seekers&apos; voucher scheme, launched, ironically, on April Fool&apos;s
 Day as the <doc>Immigration and Asylum Act</doc> came into force. Asylum seekers
 are placed in a no-win situation, unable to work whilst awaiting
 application decisions, they are forced below the povery line with an
 income that&apos;s 30% below the minimum necessary for survival. Begging
 often remains the only option, yet that&apos;s not tolerated. Faced with
 such facts, claims that refugees are cheats who come to Britain for an
 easy life fail to stand up. The truth is, they have no choice. What
 would you do if your homeland had been destroyed, if you had witnessed
 the deaths of your family, if you lived in constant fear for your
 life?</p>
 
 <p>Let&apos;s have a look at some of the other measures introduced to
 deal with this so-called social menace. Refugees are now being
 re-housed around the country, often separated from their families and
 friends as part of the &apos;dispersal scheme&apos;; they face house
 and area &apos;curfews&apos; and are prohibited from leaving their
 proscribed accommodation for more than 7 days; lorry drivers are
 subject to &#163;2,000 fines for bringing in &apos;illegal
 immigrants&apos;. The company awarded the prestigious honour of
 distributing the vouchers is <org>Sodexho Pass</org>, a French
 organisation who were once an inoffensive little catering
 company. However, they soon turned their hands to better things and
 have accumulated a nice history....They now own shares in the
 <org>Corrections Corporation</org> who run some of Britain&apos;s
 prisons; in 1998 they ruled <org>Marriot</org>, an American company,
 with an iron hand until their activities were declared
 unconstitutional by the US Labour Relations Board; and were also
 active in strike breaking operations in New England hospitals. What a
 nice bunch! In fact, just the sort of people you&apos;d expect to be
 involved in such a scheme. <org>Oxfam</org> and <org>Save the Children
 Fund</org> have pulled out of the voucher scheme, describing it as a
 form of &apos;persecution&apos;.</p>
 
 <p><person>Charles Obinna</person>, a Nigerian asylum seeker currently
 being held in Haslar Detention Centre described his feelings on
 Britain&apos;s attitude to refugees, <q>I now find myself in a new
 world where everything is deception and beyond credability.</q> Asylum
 seekers are not out for an easy life, they are simply seeking a better
 one, and they certainly won&apos;t find it here.</p>
 
 <p><org>National Coalition of Anti-Deportation Campaigns</org>,
 <phone>0121 554 6947</phone>, <web>http://ncadc.demon.co.uk/</web></p>
 
 <ul>
 <li>Four truckers were fined a total of &#163;32,000 for bringing 16 people
 illegally through a port. The truckers became the first to fall foul of
 the new aslyum act.</li>
 
 <li>Under the new regulations toys will not be allowed to be exchanged
 for tokens, so people have started up a campaign to send toys to
 <org>National Asylum Support Service</org>, asking them to pass them it on to any
 support service for refugee children. Send the toys to National Asylum
 Support Service, Quest House, Cross Road, Croydon, Surrey CR9 6EL</li>
 
 <li>This Saturday the <org>National Front</org> are marching in
 <location><city>Margate</city></location>. Well, probably about 30 of
 them. A counter-demo is planned - meet Margate train station at 12
 noon.</li>
 </ul>
 
 <p>SchNEWS pleads guilty to stealing much of the info above from
 journalist <person>Nick Cohen</person>&apos;s articles. Check out his
 book &apos;Cruel Britannia&apos;.</p>

</content>
</article>

<article>
<headline>Mayday! Mayday!</headline>
<content>

<p>Monday May
 1st. Meet 11am Parliament Square for a bit of Guerilla Gardening!
 Followed by a free party at the Dome! Transport from Brighton. Tickets
 on sale next week.</p>

</content>
</article>

<article>
<headline>Inside SchNEWS</headline>
<content>

<p><person>Zoora Shah</person>, an Asian woman who received a 20 year
 sentence in 1993 for killing a man who had subjected her to ongoing
 sexual and physical abuse, has had her sentence reduced to 12
 years. During an appeal against her conviction, which was lost, Shah
 described how <person>Mohammed Azam</person> had beaten and raped her
 for 12 years, using her &apos;as a bed&apos; and becoming violent when
 she failed to bring him drugs from
 <location><country>Pakistan</country></location>. Azam was jailed for
 drug offences in 1984, during which time he allegedly encouraged his
 associates to visit Shah for sex. She told the appeal court how she
 had attempted to hire a hitman to kill him, but was compelled to take
 matters into her own hands when Azam began to show a sexual interest
 in her two teenage daughters.</p>
 
 <p>Campaign group <org>Southall Black Sisters</org> criticised the
 criminal justice system for failing to distinguish between those who
 killed from a &apos;position of power&apos; and those who did so out
 of desperation.  Southall Black Sisters, 59 Norwood Rd, Southall,
 Middlesex UB2 4DW, <phone>0208 5719595</phone>.</p>
 
 <ul>
 <li><event>Women&apos;s Wednesdays in Whitehall</event> is a weekly
 protest and picket organised by the <org>Wages for Work Campaign</org>
 to highlight women&apos;s forgotten work and call for a change. 1-2pm
 every wednesday opposite Downing Street. Contact <org>Crossroads
 Women&apos;s Centre</org>, <phone>0207482 2496</phone>,
 <web>http://womenstrike8m.server101.com</web></li>
 
 <li><person>Neill Chapman</person> was recently sentenced to 6 months
 inside for his part in the June 18th protests in the City of
 London. Letters of support to Neill Chapman FF4529, HMP Belmarsh,
 Western Way, Thamesmead, London SE28 0EB</li>
 
 </ul>

</content>
</article>

</issue>
