<?xml version="1.0"?>

<!--
 Stylesheet for generating a SchNEWS issue as XHTML from XML from database output
 Toby Champion tobych@gn.apc.org
 Version 0.1

 To Do:
-->

<!DOCTYPE xsl:stylesheet [
        <!--because XSL stylesheets are XML documents, all forms of
            XML file content minimization are available to be used-->
]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl"
                xmlns="http://www.w3.org/TR/REC-html40"
                result-ns="">

<xsl:template match="/">
 <html>
  <head>
   <title>SchNEWS <xsl:value-of select="//issueno"/></title>
  </head>
  <body>

  <!-- Issue headings -->

  <h1>SchNEWS <xsl:value-of select="//issueno"/>, <xsl:value-of select="//date"/></h1>
  <p><xsl:value-of select="//wakeup"/></p>

  <!-- Table of contents -->

  <h2>Contents</h2>
  <ul>
  <xsl:for-each select=".//article">
   <li>
    <xsl:value-of select="./headline"/>
    <xsl:if test=".//altheadline"> - <xsl:value-of select=".//altheadline"/></xsl:if>
   </li>
  </xsl:for-each>
  </ul>

  <hr/>

  <!-- Articles -->

  <xsl:apply-templates select="//article"/>

  <!-- End matter -->

  <p>End of SchNEWS <xsl:value-of select="//issueno"/></p>

  </body>
 </html>
</xsl:template>

<!-- Each article -->

<xsl:template match="article">

 <h2><xsl:value-of select=".//headline"/></h2>
 <xsl:if test=".//altheadline"><p><em><xsl:value-of select=".//altheadline"/></em></p></xsl:if>

 <xsl:apply-templates select=".//content"/>

 <!-- Organisations -->

 <xsl:if test=".//org">
  <p>Organisations:</p>
  <ul>
   <xsl:for-each select=".//org">
    <li><xsl:value-of select="."/></li>
   </xsl:for-each>
  </ul>
 </xsl:if>

 <!-- People -->

 <xsl:if test=".//person">
  <p>People:</p>
  <ul>
   <xsl:for-each select=".//person">
     <li><xsl:value-of select="."/></li>
   </xsl:for-each>
 </ul>
 </xsl:if>

 <!-- Places -->

 <xsl:if test=".//location">
  <p>Places:</p>
  <ul>
   <xsl:for-each select=".//location">
    <li><xsl:value-of select="."/></li>
   </xsl:for-each>
  </ul>
 </xsl:if>

 <!-- Events -->

 <xsl:if test=".//event">
 <p>Events:</p>
 <ul>
  <xsl:for-each select=".//event">
   <li><xsl:value-of select="."/></li>
  </xsl:for-each>
 </ul>
 </xsl:if>

 <!-- Documents & Legislation -->

 <xsl:if test=".//doc">
 <p>Documents &amp; Legislation:</p>
 <ul>
  <xsl:for-each select=".//doc">
   <li><xsl:value-of select="."/></li>
  </xsl:for-each>
 </ul>
 </xsl:if>

 <!-- External links -->

 <xsl:if test=".//a">
  <p>External links:</p>
   <ul>
   <xsl:for-each select=".//a">
    <li>
     <xsl:value-of select="."/>:
      <a><xsl:attribute name="href"><xsl:value-of
      select="@href"/></xsl:attribute><xsl:value-of select="@href"/></a>
    </li>
   </xsl:for-each>
  </ul>
 </xsl:if>

 <hr/>
</xsl:template>

<xsl:template match="p"><p><xsl:apply-templates/></p></xsl:template>
<xsl:template match="ul"><ul><xsl:apply-templates/></ul></xsl:template>
<xsl:template match="li"><li><xsl:apply-templates/></li></xsl:template>
<xsl:template match="br"><xsl:apply-templates/><br/></xsl:template>
<xsl:template match="em"><em><xsl:apply-templates/></em></xsl:template>
<xsl:template match="strong"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="h1"><h3><xsl:apply-templates/></h3></xsl:template>

<xsl:template match="a"><a><xsl:attribute name="href"><xsl:value-of
 select="@href"/></xsl:attribute><xsl:apply-templates/></a></xsl:template>

<xsl:template match="web"><a><xsl:attribute name="href"><xsl:value-of
 select="."/></xsl:attribute><xsl:value-of
 select="."/></a></xsl:template>

<xsl:template match="email"><a><xsl:attribute name="href">mailto:<xsl:value-of
 select="."/></xsl:attribute><xsl:value-of
 select="."/></a></xsl:template>

<xsl:template match="backissue"><a><xsl:attribute name="href"><xsl:value-of
 select="@issueno"/></xsl:attribute><xsl:apply-templates/></a></xsl:template>

<xsl:template match="org"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="person"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="location"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="event"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="doc"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="money"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="phone"><u><xsl:apply-templates/></u></xsl:template>
<xsl:template match="postaddr"><strong><xsl:apply-templates/></strong></xsl:template>
<xsl:template match="q"><em>&quot;<xsl:apply-templates/>&quot;</em></xsl:template>

<xsl:template match="sidebar"><table border="1"><tr><td><xsl:apply-templates/></td></tr></table></xsl:template>

</xsl:stylesheet>
