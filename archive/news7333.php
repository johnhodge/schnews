<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 733 - 30th July 2010 - Greece: Fuel on the Fire</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, economy, financial crisis, greece, imf, strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 733 Articles: </b>
<p><a href="../archive/news7331.php">Robot Wars</a></p>

<p><a href="../archive/news7332.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news7333.php">Greece: Fuel On The Fire</a></p>
</b>

<p><a href="../archive/news7334.php">Edo: Lend Me Your Smears</a></p>

<p><a href="../archive/news7335.php">Rossport: Not Bored Yet</a></p>

<p><a href="../archive/news7336.php">Ian Tomlinson Protests</a></p>

<p><a href="../archive/news7337.php">Earth First! Gathering</a></p>

<p><a href="../archive/news7338.php">Radical Media Roundup</a></p>

<p><a href="../archive/news7339.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th July 2010 | Issue 733</b></p>

<p><a href="news733.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREECE: FUEL ON THE FIRE</h3>

<p>
<p>
	<strong>NATIONWIDE STRIKES AS WORKERS OPPOSE AUSTERITY MEASURES</strong> </p>
<p>
	Greece has been left paralysed by strikes as fuel tanker drivers, air traffic controllers and parliamentary workers oppose austerity measures. </p>
<p>
	The fuel tanker drivers strike has had a massive impact during the four days it has been running. Stores shrunk so rapidly that within 24hrs of the strike, fuel reserves were almost exhausted and there were huge queues at petrol stations. On Tuesday (27th), the drivers announced a continuation of their strike, even though 80% of petrol stations across the country have shut due to lack of fuel and station owners have withdrawn their support. Fuel tanker drivers have now proposed an indefinite strike. </p>
<p>
	On Wednesday (28th), the PASOK government were forced to resort to emergency civil conscription powers, issuing an order that, in theory, means that every striking lorry driver will receive a personal letter calling them back to work; disobeying this could result in imprisonment. </p>
<p>
	This political conscription also allows the army to intervene and to replace the striking drivers in their duties, paving the way for the military to step in to distribute petrol. The truck drivers response has been defiant: after the announcement of civil conscription drivers pulled their trucks to blockade the oil refineries of Thessaloniki and Aspropyrgos (Athens). A group of striking lorry drivers clashed with the police outside the Ministry of Transport in Athens. </p>
<p>
	The drivers are far from alone in taking action. A two day strike by air traffic controllers was ruled caused long delays in Greek airports and left thousands of tourists stranded. </p>
<p>
	These events have devastated the already ailing tourist industry during the peak of summer season. Tourism accounts for 25% of the country&rsquo;s national economy and had been experiencing significantly lower numbers of tourists and monetary losses even before these latest developments. The hiring of academic staff has been reduced by 50%, with 7,000 job replacements lost. At the same time, the IMF committee is performing another general inspection of the Greek economy with rumours about further measures to be introduced by September that will slash private sector wages by 20%. </p>
<p>
	In recent weeks even parliament workers have been striking, surely causing immense joy to the Government - without the staff to vote, passing new laws will be a bit of a struggle. However, as dissatisfied as the general public are with government measures, they are reaching similar levels of discontent with the continuing waves of strikes as both are causing crippling repercussions to Plato&rsquo;s old stomping ground. Something has to give one way or the other. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(883), 102); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(883);

				if (SHOWMESSAGES)	{

						addMessage(883);
						echo getMessages(883);
						echo showAddMessage(883);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


