<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 731 - 16th July 2010 - Against the Grain</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gm foods, spain, eu, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 731 Articles: </b>
<b>
<p><a href="../archive/news7311.php">Against The Grain</a></p>
</b>

<p><a href="../archive/news7312.php">Boring Nonsense</a></p>

<p><a href="../archive/news7313.php">Gaza Defendants Appeal</a></p>

<p><a href="../archive/news7314.php">  On The Right Tracks</a></p>

<p><a href="../archive/news7315.php">Three Imprisoned In Iran</a></p>

<p><a href="../archive/news7316.php">Inside Schnews: Speak Activist Gets Ten Years</a></p>

<p><a href="../archive/news7317.php">  Outside Schnews: Joe Glenton Released</a></p>

<p><a href="../archive/news7318.php">No Oil Painting</a></p>

<p><a href="../archive/news7319.php">Fight Or Flight</a></p>

<p><a href="../archive/news73110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th July 2010 | Issue 731</b></p>

<p><a href="news731.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AGAINST THE GRAIN</h3>

<p>
<p>
	<strong>RESISTANCE STARTS AGAIN AS EU OPENS THE DOOR TO GM CROPS</strong> </p>
<p>
	This week sees GM firmly back in the spotlight. On Wednesday the EU took a huge step in pushing forward the genetic modification agenda by copping out of regulation and putting the decision on whether to grow GM or not back into the hands of national governments. The European Commission approved changes to the rules that may break the deadlock which has prevented any significant cultivation of GM crops in Europe. What does this mean for GM production in the UK and other nation-states? And where does this leave the resistance movement? </p>
<p>
	<strong>CORN STARS</strong> </p>
<p>
	Whatever happens, activists in Catalonia, Spain are showing the way forward. On Wednesday (14th) dozens of environmental campaigners dressed in the customary white bio-hazard suits sabotaged two experimental maize trials taking place in Catalonia. </p>
<p>
	In a communiqu&eacute; the crop-trashers stated &ldquo;<em>We destroyed Syngenta&rsquo;s open-air genetic experiment because we understand that this kind of direct action is the best way to respond to the fait accompli policy through which the Generalitat, the State and the bio-tech multinationals have been unilaterally imposing genetically modified organisms (GMOs) in our agriculture and our food</em>.&rdquo; </p>
<p>
	Syngenta are the third largest seed company in the world. Trials such as this are an important stepping stone for new GM crops to be tested before gaining EC approval. However the results of such trials, and the inevitable reassurances that the crops are safe, are more likely to serve the interests of profit-hungry corporations than those of nature or human health (see <a href='../archive/news319.htm'>SchNEWS 319</a>, <a href='../archive/news346.htm'>346</a>, <a href='../archive/news622.htm'>622</a>). </p>
<p>
	The activists stated their reasons as being the dangers of cross-contamination between GM and non-GM crops, citing the extinction of varieties of traditional wheat that has occurred since the expansion of GM in Catalonia, concluding that, &ldquo;<em>We fundamentally reject both GM crops and the techno-industrial capitalist society that makes them both possible and necessary. We therefore call for people to take the step to action to destroy their genetically modified crops and the social order perpetuated by those that promote them</em>.&rdquo; </p>
<p>
	It looks as if such resistance may be more important than ever since the EC policy change. The move was considered necessary to break the twelve-year deadlock within the EU on the approval of GM farming. In a nutshell, those pro-GM states such as Spain and the Netherlands will be able to increase production, whilst those traditionally opposed - such as Germany and Austria - will be able to keep their restrictions. </p>
<p>
	However, the change has been slated as &lsquo;not worth the paper it&rsquo;s written on&rsquo;. Although states now have increased power to ban GM within their own territories, authorisation will be easier to achieve at EU level. This effectively opens up European agriculture to more GM crops. </p>
<p>
	<img style="border:2px solid black;width: 300px; height: 210px; margin-left: 10px"  align="right"  alt="Maize Destroyers"  src="http://www.schnews.org.uk/images/731-maize-destroyers-sm.jpg"  /> <strong>SEEDY MIDDLE-AGED MEN</strong> </p>
<p>
	The EC&rsquo;s &lsquo;roadmap&rsquo; for the legislation proves that the supposed autonomy given to individual states is actually part of a play-off. Bans on specific GM varieties may be allowed after the trial process, but only in exchange for government&rsquo;s &lsquo;flexibility&rsquo; in allowing quick authorizations of the trials in the first place. The legislation has also left open the possibility that individual countries&rsquo; bans could be overturned by expensive bio-tech lawyers. This is all good news for the powerful bio-tech transnational corporations such as Syngenta, Monsanto and Dupont. Yet despite these gains, the pro-GM lobby is still griping that the policy removes choice for farmers &ndash; just not enough freedom to grow trademarked, potentially lethal, mutant moneyspinner crops it seems. </p>
<p>
	Of course UK Plc&rsquo;s new management (being a bit of an unnatural mutant hybrid itself) is quite keen on a bit of profitable tinkering with DNA. Mirroring New Labour&rsquo;s GM zeal, one of the ConDem&rsquo;s first moves in government was to authorize new GM potato trials by Leeds University - putting mutatoes (see <a href='../archive/news583.htm'>SchNEWS 583</a>) back on the menu. </p>
<p>
	Perhaps hoping that the &lsquo;Franken-food&rsquo; furore of the nineties had died down, and that those pesky eco-warriors may have hung up their scythes and shovels, the Leeds trial is the first to be authorized &ndash; with several more in the pipeline. A further potato trial is planned to take place near Norwich by the Sainsbury&rsquo;s Laboratory at the John Innes Centre. The Leeds trial is due to last three years, Norwich for two, beginning this year. The trials are partly funded through taxpayer money. A similar potato trial in Norfolk is estimated to have cost the taxpayer &pound;1.7m over the last ten years through research done by pressure group GM Freeze. </p>
<p>
	These new EU developments to push through GM production are sure to cause a resurgence in the wide public opposition to - and direct action movement against - GM crops. In the past decade GM trials in this country were made politically and practically impossible after almost every one of the 54 crop trials attempted in the UK since 2000 were sabotaged or destroyed (see <a href='../archive/news583.htm'>SchNEWS 583</a>). See you in the fields! </p>
<p>
	* The grid references of both trials&rsquo; locations can be found at <a href="http://www.defra.gov.uk/environment/quality/gm/regulation/documents/trials-rev100524.pdf" target="_blank">www.defra.gov.uk/environment/quality/gm/regulation/documents/trials-rev100524.pdf</a> </p>
<p>
	* See also <a href="http://www.gmfreeze.org" target="_blank">www.gmfreeze.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(859), 100); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(859);

				if (SHOWMESSAGES)	{

						addMessage(859);
						echo getMessages(859);
						echo showAddMessage(859);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


