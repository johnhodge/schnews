<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 782 - 4th August 2011 - CIA: We Do What We Haftar</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, libya, arab spring, cia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 782 Articles: </b>
<p><a href="../archive/news7821.php">From Cradle To Graves</a></p>

<p><a href="../archive/news7822.php">Is It The End Of The Line For Shell?</a></p>

<p><a href="../archive/news7823.php">Safe To Go Back In The Water?</a></p>

<p><a href="../archive/news7824.php">Brutality In A China Shop</a></p>

<b>
<p><a href="../archive/news7825.php">Cia: We Do What We Haftar</a></p>
</b>

<p><a href="../archive/news7826.php">What Not To Malware</a></p>

<p><a href="../archive/news7827.php">Schnews In Brief</a></p>

<p><a href="../archive/news7828.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th August 2011 | Issue 782</b></p>

<p><a href="news782.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CIA: WE DO WHAT WE HAFTAR</h3>

<p>
<p>  
  	The Arab spring continues apace this week, although with mixed results. In Egypt Mubarak and his sons were wheeled out (literally in Mubarak&rsquo;s case) for the first day of a trial for corruption and ordering the killing of protesters. As all eyes were on Cairo for a glimpse of the former dictator pleading not guilty, Syrian tanks were crushing dissent in Hama. Obviously President Bashar al-Assad had decided it would be a good day to bury bad news. William Hague, Foreign Secretary, showed his grasp of the facts explaining there would be no Libyan style intervention because &ldquo;Syria is not Libya&rdquo;.  </p>  
   <p>  
  	The latest twist in the Libyan war is the death of the former leader of the rebel army, Abdul Fatah Younis. According to the mainstream press Younis was killed on Thursday 28th July. He and two of his officers had been shot and their bodies burned, and the identity of the assailants is still debatable.  </p>  
   <p>  
  	However Jordan based Al Bawaba News (<a href="http://www.albawaba.com" target="_blank">http://www.albawaba.com</a>) had reported his death four days earlier, stating that he had been killed in &ldquo;mysterious circumstances&rdquo; around 14th July.  </p>  
   <p>  
  	Whoever is behind the killing (Gaddafi, the CIA, Al-Qaeda and the Transitional National Council are amongst the suspects) one man who has certainly gained from it is General Khalifa Belqasim Haftar.  </p>  
   <p>  
  	Younis and Haftar had been swapping the title of leader of the rebels for several months before Younis&rsquo;s death and the men were believed to have a mutual distrust and dislike for each other. With Younis out of the way, Haftar is likely to be unchallenged as the head of the army.  </p>  
   <p>  
  	While General Younis was a long-time ally of Gaddafi, helping him come to power in 1969 and remaining at his side right up to 2011 when he switched to the NATO backed rebels, General Haftar&rsquo;s background is much murkier (see <a href='../archive/news777.htm'>SchNEWS 777</a>). The US cannot be at all upset that the CIA&rsquo;s man is now secure as the head of the rebel army &ndash; just what they want as they seek to benefit from their active role in determining the conflict&rsquo;s outcome...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1315), 151); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1315);

				if (SHOWMESSAGES)	{

						addMessage(1315);
						echo getMessages(1315);
						echo showAddMessage(1315);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


