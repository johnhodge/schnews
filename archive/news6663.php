<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 666 - 13th February 2009 - The Apes Of Wrath</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, animal rights, hls, huntingdon life sciences, animal testing, mel broughton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 666 Articles: </b>
<p><a href="../archive/news6661.php">The Watchtower</a></p>

<p><a href="../archive/news6662.php">...from The Book Of Schrevalations</a></p>

<b>
<p><a href="../archive/news6663.php">The Apes Of Wrath</a></p>
</b>

<p><a href="../archive/news6664.php">My Bloody Valentine</a></p>

<p><a href="../archive/news6665.php">Sounding Off</a></p>

<p><a href="../archive/news6666.php">Keep It Spikey</a></p>

<p><a href="../archive/news6667.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 13th February 2009 | Issue 666</b></p>

<p><a href="news666.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>THE APES OF WRATH</H3>

<p>
<b>AS HLS PRIMATE ABUSE EXPOSED...AGAIN</b> <br />
 <br />
A few weeks after seven people from SHAC were sentenced from 4 to11 years in prison for &#8220;<b>Conspiracy to Blackmail</b>&#8221; after running a campaign to close down animal testing laboratory Huntingdon Life Sciences.  A video and report has been released that highlights why these people were campaigning against HLS. This is the seventh time in 12 years HLS have been exposed by undercover investigations. <br />
 <br />
The video and report by Animal Defenders International (not connected to SHAC) follows monkeys in small rusted cages in Vietnam on a 30 hour journey to Europe. The monkeys could not stand up in the cages and suffered illnesses after the long journey; high temperature and weight loss, cuts on their heads and faces, flaking skin and bruising. <br />
 <br />
On arrival at HLS an undercover worker in the labs saw shocking routine cruelty and injuries caused by poor quality housing and rough handling. Monkeys were routinely scraped and cut, one monkey lost its toes, another cut his hand open. One female monkey cut her arm and the vet stapled shut the wound, but when the monkey was returned to its cage it pulled out the staples; she was then re-caught and had the wound glued shut. The undercover worker said of one monkey, &#8220;<i>the chains holding a cage door shut had sharp edges and one pierced the cheek of a female monkey, leaving her unable to eat; as a result she was force-fed twice a day.</i>&#8221; <br />
 <br />
The procedures on the animals are themselves inherently cruel. Monkeys are forced to ingest substances being tested by having a tube thrust down their throats into their stomachs and the substance pumped in. This caused vomiting, diarrhoea, nosebleeds, skin disorders, excessive salivating and the immediate death of one monkey following dosing.  <br />
 <br />
In other experiments monkeys are restrained by being strapped into special chairs, one member of staff was bitten and subsequently grabbed the monkey&#8217;s head and twisted it violently. The stress caused monkeys to suffer rectal prolapses and they had to be replaced in the study. In an inhalation study, three monkeys died and another three collapsed but were revived, autopsies showed the monkeys had blackened lungs. <br />
 <br />
Of course all this is done &#8220;humanely&#8221; and is licensed by the Home Office. So of course HLS can&#8217;t be doing anything wrong and if you say they are, then hey, you&#8217;re a dangerous extremist. <br />
 <br />
Report and video at: <a href="http://www.shac.net/HLS/exposed/2008/" target="_blank">www.shac.net/HLS/exposed/2008/</a> <br />
 <br />
* The jury in the re-trial of animal rights activist Mel Broughton retired this week to consider its verdict. He is accused of conspiracy to commit arson and possession of articles with intent to destroy property, relating to attacks on Oxford University property in 2006. Oxford University is later this year opening an animal testing laboratory. The main evidence is low copy DNA from a matchstick used in a fuse of a bomb that did not ignite. The use of low copy DNA based on tiny pieces of DNA was described as &#8220;potentially misleading&#8221; by Dr Jamieson, a forensics expert for the defence. Also significant is that Thames Valley Police have a personal vendetta against Mel after being caught on tape saying they will be &#8220;waging a dirty war&#8221; him and &#8220;to persecute him&#8221;.  (<a href='../archive/news590.htm'>SchNEWS 590</a>).
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=animal+rights&source=666">animal rights</a>, <span style='color:#777777; font-size:10px'>animal testing</span>, <a href="../keywordSearch/?keyword=hls&source=666">hls</a>, <a href="../keywordSearch/?keyword=huntingdon+life+sciences&source=666">huntingdon life sciences</a>, <span style='color:#777777; font-size:10px'>mel broughton</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(224);
			
				if (SHOWMESSAGES)	{
				
						addMessage(224);
						echo getMessages(224);
						echo showAddMessage(224);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>