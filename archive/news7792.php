<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 779 - 15th July 2011 - Gaza Flotilla: Seige You Later</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, israel, palestine, freedom flotilla, greece" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 779 Articles: </b>
<p><a href="../archive/news7791.php">Rolling Out The Unwelcome Mat</a></p>

<b>
<p><a href="../archive/news7792.php">Gaza Flotilla: Seige You Later</a></p>
</b>

<p><a href="../archive/news7793.php">Don't Mansion It</a></p>

<p><a href="../archive/news7794.php">Not The Full English</a></p>

<p><a href="../archive/news7795.php">Inside Schnews</a></p>

<p><a href="../archive/news7796.php">Like It Or Lumpur It</a></p>

<p><a href="../archive/news7797.php">Rotten Fruit</a></p>

<p><a href="../archive/news7798.php">Lyons Led By Donkeys</a></p>

<p><a href="../archive/news7799.php">C'mon Bunny Light My Fire</a></p>

<p><a href="../archive/news77910.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th July 2011 | Issue 779</b></p>

<p><a href="news779.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GAZA FLOTILLA: SEIGE YOU LATER</h3>

<p>
<p>  
  	It&rsquo;s looking as if the planned flotilla to Gaza has run aground on Greek intransigence. Weeks after they were due to sail, the ships are still being held in Greek harbours. In one way it&rsquo;s a testament to the Israeli government&rsquo;s fear of the lifting of the Gazan siege that they&rsquo;ve gone to such lengths to prevent a a small amount of humanitarian aid reaching the Gaza Strip by sea.  </p>  
   <p>  
  	Since the Mavi Marmara debacle in 2010 (see <a href='../archive/news725.htm'>SchNEWS 725</a>) when Israeli commandos shot and killed nine activists and stormed the entire fleet to prevent it reaching port, the Israeli authorities have been desperate to avoid another high seas showdown.  </p>  
   <p>  
  	This time round tactics have been far sneakier. One boat at least has been the victim of mysterious sabotage. Pressure on the Greek government has led to an embargo on the flotilla leaving. The Greek and Israeli governments are strongly allied - carrying out joint military manoeuvres on a regular basis and with the Greeks buying Israeli military technology.  </p>  
   <p>  
  	Nine vessels, including two cargo ships, are still blocked in various Greek ports, while the Irish ship Saoirse is in a Turkish port undergoing repairs for alleged sabotage suffered two weeks ago. One U.S ship cheekily dubbed the &lsquo;Audacity of Hope&rsquo;, carrying 36 passengers, four crew and about 10 members of the media, attempted to run the blockade on the same day it was announced, and was seized by Greek commandos. The captain, John Klusmire, is being held in custody charged with &ldquo;trying to leave port without permission and of endangering the lives of passengers.&rdquo;  </p>  
   <p>  
  	Not all the obstruction has taken place on the water. The bizarre saga of &lsquo;Marc&rsquo; show the lengths the Israelis are prepared to go to. A month or so ago a Youtube <a href="http://www.youtube.com/watch?v=vhmBbGFJleU" target="_blank">http://www.youtube.com/watch?v=vhmBbGFJleU</a> video started doing the rounds. It purported to be the story of an LGBT activist who decided to get involved with the Freedom Flotilla as he felt they must be a cross between &lsquo;Che Guevara and Mother Teresa&rsquo;. He was then apparently asked to leave the flotilla on account of his sexuality. In fact the whole thing was a hoax - Marc is none other than Israeli actor Omer Gershon and the video nothing more than a blatant attempt at pinkwashing the occupation.  </p>  
   <p>  
  	Flotilla activists have vowed not to give up the struggle and are organising demos outside Greek embassies across the world.  </p>  
   <p>  
  	* To find a demo near you - <a href="http://gazafreedommarch.org/cms/en/Solidarity/events-map.aspx" target="_blank">http://gazafreedommarch.org/cms/en/Solidarity/events-map.aspx</a>  </p>  
   <p>  
  	* To follow events <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1277), 148); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1277);

				if (SHOWMESSAGES)	{

						addMessage(1277);
						echo getMessages(1277);
						echo showAddMessage(1277);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


