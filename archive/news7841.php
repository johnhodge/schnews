<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 784 - 19th August 2011 - The U.N. Holy Land</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, carmel agrexco, palestine, israel, west bank, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 784 Articles: </b>
<b>
<p><a href="../archive/news7841.php">The U.n. Holy Land</a></p>
</b>

<p><a href="../archive/news7842.php">Shock Tactics</a></p>

<p><a href="../archive/news7843.php">Getting The Humpback</a></p>

<p><a href="../archive/news7844.php">Crap Justice Of The Week</a></p>

<p><a href="../archive/news7845.php">Oxford Anti-social</a></p>

<p><a href="../archive/news7846.php">Chile: Education Riots</a></p>

<p><a href="../archive/news7847.php">Edl: You Can Never Telford</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th August 2011 | Issue 784</b></p>

<p><a href="news784.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE U.N. HOLY LAND</h3>


<p>  
  	The machine-gunning of an Israeli bus and the subsequent air-raids on Gaza represent another dangerous turn of the screw for Palestine. Anxious to rescue themselves from their own domestic discontents (see <a href='../archive/news781.htm'>SchNEWS 781</a>) another border war might be just what the Israeli authorities want.  </p>  
   <p>  
  	While it&rsquo;s the shocking attack on the Israeli bus that&rsquo;s grabbed the headlines, the slow grind of the occupation is the reality on the ground. Just in the last week there have been the usual assortment of never-ending attacks, raids, air strikes and killings, to name but a few of the daily realities each Palestinian has to endure. On Friday (12th) three children were injured during an anti-wall protest in the village of al-Ma&rsquo;sara, West Bank by Israeli troops. The children, Abada Brijiyah, 11, Osama Brijiyah, 9 and Hareth Brijiyah, 10, were assaulted with rifle butts and batons&nbsp; by the Israeli military as they approached the site of the wall. Many others were tear gassed and shot with rubber coated steel bullets. In the village of Bil&rsquo;in soldiers attacked internationals and Israelis during the&nbsp; weekly protest against the apartheid wall. Local villagers have held non-violent demonstrations every Friday for over 6 years in villages across the area. (see <a href='../archive/news673.htm'>SchNEWS 673</a>)  </p>  
   <p>  
  	In the early hours of the 16th August&nbsp; Israeli air strikes zeroed in on Zeitun, Gaza City and Khan Younis respectively. The trigger for this attack was a report from an Israeli policeman claiming a rocket was fired into the Israeli city of Beersheba. The raid on Zeitun resulted in three people being seriously wounded. At the same time Amin Talab Dabash, 38 years old, died after Israeli border police attacked him in East Jerusalem. According to one witness &lsquo;he was killed deliberately&rsquo;.&nbsp; The same day a 17 year old Palestinian was shot down dead by Israeli gunfire. The teenager was killed as he drew nearer the border with Israel, at a refugee camp in the central Gaza strip.  </p>  
   <p>  
  	Talks between Hamas and Israeli officials have been taking place in Cairo with Egyptians acting as a (sort of) impartial go between. A hoped for prisoner deal could free 1,000 Palestinians held in Israeli prisons in exchange for one Israeli soldier Gilad Shalit, held since 2006. At the moment at least 6,000 Palestinians are rotting in Israeli prisons, 219 sit in detention with no charge. Out of the detained, 44 have been residing there for over a quarter of a century. 299 of the prisoners have been there over 20 years. Further indignation comes from the ever decreasing access Palestinian prisoners have to education; 1800 Palestinians were ready to take their June examinations but were prohibited by the prison service.  </p>  
   <p>  
  	All facets of a prisoner&rsquo;s life are suppressed. Many in need of health care are simply denied it. Medicine, food parcels, clothing and blankets often don&rsquo;t seem to reach them. A spokesman for Wa&rsquo;ed, an organisation fighting for the rights of prisoners said, &ldquo;Israel has been preventing family visits to 750 Gazan prisoners for five years now.&rdquo; Last week Palestinians en route to see family members detained in Southern Negev prison were subjected to chants of &ldquo;No visits for Gilad, no visits for you&rdquo; from a group of fifty Israelis they passed.  </p>  
   <p>  
  	The latest addition to the jail house is Samer Allawi, a Palestinian journalist for Al Jazeera. Allawi was detained on Wednesday(10th) as he was leaving the West Bank to return to his post in Kabul. He has not been charged.  </p>  
   <p>  
  	The situation in the region is as dire as ever, yet the upper echelons in Palestine are pinning their hopes on gaining UN recognition for Palestinian statehood on 20th September. While a Palestinian state could prove a diplomatic headache for the Israeli government, what would the title of &lsquo;194th member of the United Nations&rsquo; really mean for the people of Palestine? What difference would it make? Israeli journalist Akiva Eldar, writing in Ha&rsquo;aretz had this to say, &ldquo;This time, too, Israel will accuse the Arabs of unilateral steps, ignore the United Nations, expand settlements in the West Bank, and build more neighbourhoods for Jews in East Jerusalem.&rdquo;  </p>  
   <p>  
  	<strong>THE ZION SLEEPS TONIGHT</strong>  </p>  
   <p>  
  	Meanwhile west of the Apartheid Wall in Israel itself a story of suppression and reaction is unfolding. At the beginning the discontent arose from the continuing rise of the cost of living and rocketing house prices as well as the 5th highest income inequality amongst developed countries. One in four Israelis lives below the poverty line (See <a href='../archive/news781.htm'>SchNEWS 781</a>). This month numbers of protesters have escalated and their demands are changing from outrage over the sky-high cost of living into calls for social justice, cheaper housing, education, healthcare, food and fuel. More than 300,000 protesters from different backgrounds took to the streets on the 11th of August, with rallies in Jerusalem, Holon and Haifa, as well as Lod, Hod Hasharon and Nazareth.  </p>  
   <p>  
  	This is the largest ever Israeli protest over socio economic issues. It seems Israel is having its Arab Spring, even if it&rsquo;s a bit of a late bloomer. On Saturday the 13th demonstrators were encouraged to head out of Jerusalem and Tel Aviv to support protests across the rest of the country. There was a call-out for demos in eighteen different cities. The government response was passive and watchful, and some protesters accused them of using delaying tactics. One spokesman told Israeli press &ldquo;We don&rsquo;t want a group that will just hold discussions over and over again for months.&rdquo; A tenuous promise of change in the future just doesn&rsquo;t cut it any more. The weekend protests attracted 75,000,&nbsp; huge in a country of just 6 million.  </p>  
   <p>  
  	Confrontations between hundreds of Israelis and police took place on the 16th as a 500 strong march made its way towards the Israeli Knesset in Jerusalem. Inside the Knesset sat the ruling elite plotting, erm... discussing the recent protests, as the protesters shouted &ldquo;We want social justice&rdquo;. The police were unable to restrain the demonstration and failed to keep it away from the parliament. Recent polls suggest that 80% of all Israelis are against the policies of PM Benjamin Netanyahu, with 88% supporting the protest movement. So far this movement has taken no position on the Occupation, with mainstream peace groups standing back so as not to de-rail the popular uprising. What implications it has for relations with the Arab world remain to be seen.  </p>  
   <p>  
  	<strong>UK ACTION: ROTTEN TO THE CORP</strong>  </p>  
   <p>  
  	Meanwhile back in&nbsp; London, pro-Palestinian activists on Tuesday(16th) dumped&nbsp; rotten fruit in the foyer of&nbsp; UK/US investment firm Apax Partners. The firm has major investments in Carmel Agrexco, Israel&rsquo;s largest exporter of agricultural produce, harvested from illegal Israeli settlements on Occupied Palestinian land.&nbsp; The protesters then held a boisterous picket outside the Apax offices, handing out information and chatting to passers-by. The solidarity action was part of the ongoing boycott, divestment and sanctions campaign to halt the&nbsp; Occupation.  </p>  
   <p>  
  	*for more <a href="http://electronicintifada.net/" target="_blank">http://electronicintifada.net/</a> and <a href="http://www.bigcampaign.org/" target="_blank">http://www.bigcampaign.org/</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1325), 153); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1325);

				if (SHOWMESSAGES)	{

						addMessage(1325);
						echo getMessages(1325);
						echo showAddMessage(1325);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


