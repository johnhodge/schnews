<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 650 - 10th October 2008 - End Of World Is Nigh</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, surveillance, privitisation, credit crunch, recession, depression, group 4, parkguard, police reform act, civil contingencies act, numberplate recognition, david cameron" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../schmovies/index-on-the-verge.htm"><img 
						src="../images_main/on-the-verge-banner-tor.jpg" 
						width="465px" 
						height="90px" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 650 Articles: </b>
<b>
<p><a href="../archive/news6501.php">End Of World Is Nigh</a></p>
</b>

<p><a href="../archive/news6502.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6503.php">Navy Blues</a></p>

<p><a href="../archive/news6504.php">Tata For Now</a></p>

<p><a href="../archive/news6505.php">It Doesn't Add Up</a></p>

<p><a href="../archive/news6506.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/650-bust-stop-issue-lge.jpg" target="_blank">
													<img src="../images/650-bust-stop-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 10th October 2008 | Issue 650</b></p>

<p><a href="news650.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>END OF WORLD IS NIGH</H3>

<p><b>..ON IMPOSSIBLE TO SECOND GUESS - BUT SCHNEWS TRYS ANYWAY</b></p>
<p>Another fifty issues under the belt and its time  for SchNEWS to once again take stock. Two years ago back in <a href="news550.htm">issue 550</a>, SchNEWS went all Mystic Meg (or Cassandra - for you ancient Greek fans) in our 'State of the Indignation' address. At the time the economy was bubbling along happily for those lucky enough to get on the housing ladder and there was little sign of popular discontent around the 'nuclear-powered police state' in the offing.</p>
<p>But we predicted that 'the cracks were already beginning to show' and suggested that the inevitable recession would represent a major political opportunity. And this downturn looks set to be a real corker!  </p>
<p>In hindsight of course it was bleedin' obvious that a debt-fuelled consumer boom based on the idea that that house prices would rise infinitely was a bubble ripe for bursting. And now falling house prices, dropping at records rates, are bringing down the economic house of cards - further fuelling the falls... Yer ever prophetic SchNEWS hit that nail firmly on the head with our truth-hammer.  Funnily the article was so persuasive that just two years later the economics correspondents of all the major newspapers are now united in their dismissal of the fools who were naive enough to believe that free market capitalism was ever considered a good idea.</p>
<p>But what's stepping into its place? Bigger monopolies and mergers are concentrating power into fewer hands and no doubt there will be vulture capitalists picking up the remains. As banks go under, the state turns a blind eye to flagrant abuse of anti-monopoly rules as Lloyds TSB takes over HBOS, turning into an uber-behemoth with its hands on everyone's houses and wallets. Most of the &pound;700 billion US bail out is to be handed straight to the small elite which caused this meltdown in the first place. </p>
<p>In the meantime the surveillance state shows no signs of receding. Not content with having the largest CCTV network in the world (one for every fourteen of us and counting!), the stealthy introduction of I.D cards, 28 days detention etc, Britain's ruling class now want the ability to instantly interrogate every single piece of data-transfer happening in the UK - that's every phone call, text message and e-mail - in real time. The Intercept Modernisation Programme is a &pound;12 billion scheme to spy on the entire telecommunications system. Current UK law requires a warrant is to intercept communications (or at least to produce the evidence in court) but that will change with the implementation of the new database. </p>
<p>Meanwhile, Scotland - traditional home of unpopular pilot schemes - is now hosting a national roll-out of Automatic Numberplate Recognition (ANPR). Around 450,000 number plates will be recorded every day and used to flag up the whereabouts of known suspects. The &pound;2.4m system will also help investigators build up evidence using a database of millions of car journeys. How soon before the scheme is rolled out across the whole of the UK?</p>
<p>This massive expansion of state surveillance will make ruling the country in the rocky times ahead easier for the power elite. They already have the framework of repression: tighter public order laws, terrorism legislation and the power to declare martial law (The Civil Contingencies Act, <a href="news437.htm">SchNEWS 437</a>) all passed with little resistance in a time of peace and little social strife.</p>
<p>The elite knew that the clock was ticking and have prepared accordingly. Increasing numbers of police have gone hand in hand with centralization of law enforcement.  The Police Reform Act 2002 gave the police the ability to hand out police powers to 'accredited' organisations.  Private security firms and council busybodies are now being issued with the right to issue fines for 'anti-social behaviour' - a slippery term  which  easily becomes a euphemism for state interference in every aspect of your life. For example, Parkguard - a firm which patrols parks and housing estates, now has powers in Hertfordshire and Essex to issue fixed penalty notices for anti-social behaviour and confiscate alcohol and tobacco.  </p>
<p>Even private companies like Group 4 now employ large numbers of people legally authorised to use &quot;Control & Restraint&quot; techniques to ensure the compliance of the awkward. Abuses within the Asylum detention system largely run by these companies are well documented. The existence of different 'members' of the police family gives the state  the option of playing one against the other - could the Community support brigade be inflated into a strong-arm squad to keep order on the streets if the police ever made good on their threat to strike?</p>
<p>So what should SchNEWS readers make of the current &quot;crisis&quot;? Clearly there is a lurch to more state control over the economy as the whole free market system unravels, with a serious potential shift to national socialism as states adopt a protectionist policy and take control of the economy and put up borders.</p>
<p>Capitalism's current decline wasn't bought about by anti-capitalist direct action, which itself had declined in the west in recent years - with more energy being put into the war on Iraq and the recent resurgence of eco-action around oil use and climate change. So have these shifts in the direct action movement been misguided? Not entirely - the War on Iraq and all the associated &quot;terror&quot; scares and repression still needs to be resisted. And climate change ain't gonna go away just yet - the government is still gonna dig up loads of coal as a quick and dirty solution to the energy crisis bought about by years of reliance on gas which the UK now has to import. Also the skills learned by the direct action movement in organisation, resistance to state power and a culture of self-reliance and cooperation should prove us in good stead in times of crisis. These skills and vision need to be shared with the wider population if they are not merely to remain part of a sub-culture getting by in the recession, while the majority of the population struggle with crippling debts and job insecurity.</p>
<p>How long the recession and how deep it hurts is anyone's guess, but is doubtless that, as ever, the poorest will be hit hardest. However, we are at least used it to some extent - the question is how will the consumer classes take it when their ivory-towered aspirational lifestyles grind to a halt? The shock might finally persuade them to think about something more radical than giving David Cameron a go...see you in 50 issues when we're eating our words (or each other).</p>

</div>


<br /><br />

		</div>
		
		
		
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>