<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 773 - 27th May 2011 - No Spain, No Gain</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, real democracy now, spain, georgia, greece, bristol" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">
<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 773 Articles: </b>
<b>
<p><a href="../archive/news7731.php">No Spain, No Gain</a></p>
</b>

<p><a href="../archive/news7732.php">Le Harve It Large</a></p>

<p><a href="../archive/news7733.php">Mort In A Storm</a></p>

<p><a href="../archive/news7734.php">Baton Charged</a></p>

<p><a href="../archive/news7735.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/773-homage-lg.jpg" target="_blank">
													<img src="../images/773-homage-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 27th May 2011 | Issue 773</b></p>

<p><a href="news773.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NO SPAIN, NO GAIN</h3>

<p>
<p>
  	<strong>AS MASS PROTESTS IN MADRID RAISE THE BAR FOR EUROPEAN ANTI-AUSTERITY ACTIONS</strong> <br />
  	 <br />
  	We&#39;ve got the Arab Spring &ndash; what about a European summer? It&#39;s a week since thousands of pro-democracy demonstrators pitched up in central Madrid (see <a href='../archive/news772.htm'>SchNEWS 772</a>) - and revolution fever is spreading across Europe like nits in a playground. With the Spanish sit-in still going strong &ndash; and intending to remain until at least the 29th - street demonstrations have also hit Greece, Georgia, and, er, Bristol. Protests are spreading to Italy, France, Portugal, Austria even German - could it be that last year&#39;s initial protests against austerity measures are maturing, one year on, into a broader demand for political reform?  </p>
   <p>
  	Dealing with consequences of similar protect-the-rich IMF-style austerity measures as many millions of others in Europe, the Spanish have been mobilising on the streets with mass protests and strikes for months (see <a href='../archive/news741.htm'>SchNEWS 741</a>). This general air of resistance has now spawned a sit-down protest in the capital city. Demands are vague and the whole thing has the feel of a forum rather than a movement with a solid blueprint for the future, but what unites them is an anger with pro-business politicians who all conspired in the banking boom, bust and bail-out.  </p>
   <p>
  	In Madrid&#39;s central city square of Puerta del Sol, the &ldquo;alternative village&rdquo; set up to protest the government ahead of last Sunday&#39;s elections&nbsp; &ndash; which saw huge losses for Socialist Prime Minister Zapatero &ndash; continues unabated. The &lsquo;&iexcl;Democracia Real YA!&rsquo; (Real Democracy Now!) camp now boasts ramshackle but egalitarian canteens, day-care facilities, a press office, a pharmacy and even a zone for &ldquo;feminist information&rdquo;. Those involved in the movement have said they&#39;ll proliferate into local groups and assemblies around the country to spread the revolutionary message.  </p>
   <p>
  	<strong><img style="margin-right: 10px; border: 2px solid black"  align="left"  alt=""  src="http://www.schnews.org.uk/images/773-spain-storypic.jpg"  /> IT&#39;S A VERY MADRID WORLD</strong>  </p>
   <p>
  	In London, a camp-in opposite the Spanish embassy in Belgrave Square has seen hundreds arrive for daily rallies since 18th May. The group, calling themselves Real Democracy Now London, state their main driving force is their &ldquo;outrage and weariness at the misuse of the Spanish democratic system by politicians; their lack of respect and their constant mistreatment of the general interests of the electorate&rdquo;. It&#39;s a statement which sums up the widespread malcontent across the continent nicely, if you replace &ldquo;Spanish&rdquo; with pretty much any alternative nationality.  </p>
   <p>
  	The camp plans to remain for several more days. In Brighton, which is lacking in embassies, a solidarity protest has rocked up in a tourist-gateway lawn opposite the pier.  </p>
   <p>
  	In Bristol, a hefty number descended on the city centre on Sunday (22nd)&nbsp; - but rather than only offering solidarity for their Spaniard comrades, they were hoping to spark a similar uprising in the UK.&nbsp; Demonstrators bore banners with memes-of-the-moment slogans such as &ldquo;Real Democracy Now&rdquo;, and &ldquo;They don&#39;t represent us&rdquo;.  </p>
   <p>
  	Meanwhile tens of thousands of Greeks turned out in Syntagma Square, Athens - and in other cities - for four days running, declaring their &ldquo;indignation&rdquo; at the government and replicating the Spanish calls for &ldquo;All politicians to go&rdquo;. With Greek politicians having spent most of their time recently fielding accusations of incompetence on the international stage, they&#39;re now facing a double-whammy of pressure (and should possibly learn to take a hint). The protests have so far been peaceful, which suggests, ironically, that during these most explicitly anarchic demonstrations so far, the feisty Greek anarchists themselves have temporarily cast their black masks to the back of the wardrobe and decided that petrol-bombing cars is all a bit 2010.  </p>
   <p>
  	In Georgia, things have been a bit more rough n&#39; repressed, as police clamped down on several hundred opposition protesters demanding the resignation of President Mikheil Saakashvili. Led by former speaker of parliament Nino Burdjanadze, the protests against Saakashvili&#39;s authoritarian leadership, repression of independent voices and the country&#39;s widespread poverty began last Saturday (21st). Five days later, to clear the way for a military procession marking Georgia&#39;s Independence Day, riot cops moved in to disperse the crowds using tear gas, rubber bullets and water cannons. A police officer and protester were killed during the brutal attack, and another 37 were injured.  </p>
   <p>
  	Smaller solidarity protest camps have now sprouted outside Spanish Embassies worldwide, and may yet catalyse bigger mobilisations as the locals realise that their situation is not so different from the Iberian one.  </p>
   <p>
  	In all countries, the credit for&nbsp; the mobilisation&nbsp; has gone to Facebook and Twitter - making the claim that the internet is a tool for democracy much more literal than previously implied. Nonetheless, if such antics continue and escalate, we wonder how long before the surveillance / censorship / repression side of social networking is pressed into action by worried elites.  </p>
   <p>
  	In the meantime, the disaffected of Europe have the shining example of Iceland before them. In 2009, facing their own banker-lead economic collapse, people power overthrew the government &ndash; see <a href='../archive/news664.htm'>SchNEWS 664</a> &ndash; and then stuck two fingers up to the &#39;financial community&#39; by defaulting on their debt. The country is now, far from crashing back to the dark ages as an international pariah,&nbsp; actually doing rather well and looks set to recover more quickly than either Ireland or Greece where&nbsp;&nbsp; massive IMF loans and bail outs - mainly given to pay straight back to their overseas creditors - were rammed down taxpayers throats along with swingeing austerity cuts.&nbsp;&nbsp; An Ice lesson for us all.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1230), 142); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1230);

				if (SHOWMESSAGES)	{

						addMessage(1230);
						echo getMessages(1230);
						echo showAddMessage(1230);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">
<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>
