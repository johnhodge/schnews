<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 660 - 19th December 2008 - Mama Mia... Here They Go Again  </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, madres de plaza de mayo, argentina, junta, buenos aires, kirchner" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 660 Articles: </b>
<p><a href="../archive/news6601.php">Euro Trashed 1: New Rome-antics</a></p>

<p><a href="../archive/news6602.php">Euro Trashed 2: Tracks Of My Tear Gas</a></p>

<p><a href="../archive/news6603.php">Euro Trashed 3: Riotschool Musical  </a></p>

<p><a href="../archive/news6604.php">Up On The Roof  </a></p>

<p><a href="../archive/news6605.php">Go Your Own Wey</a></p>

<p><a href="../archive/news6606.php">Shac Yer Booty</a></p>

<b>
<p><a href="../archive/news6607.php">Mama Mia... Here They Go Again  </a></p>
</b>

<p><a href="../archive/news6608.php">That's The Way We Lake It</a></p>

<p><a href="../archive/news6609.php">Coal The Whole Thing Off  </a></p>

<p><a href="../archive/news66010.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th December 2008 | Issue 660</b></p>

<p><a href="news660.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>MAMA MIA... HERE THEY GO AGAIN  </H3>

<p>
In April 1977 fourteen middle-aged women quietly walked round and round the pyramid at the centre of Plaza de Mayo in Buenos Aires - the site of government in Argentina. Capped in white shawls they walked round in ones and twos because to pause or to walk in larger groups would have contravened the anti-freedom of assembly laws of the military dictatorship. It was this group of mothers of 'disappeared' Argentines that five years later led the first Marcha de la Resistencia; one of the first large scale protests against the military government of the time and its kidnapping, torture and murder - policies which resulted in the 'disappearance' of an estimated 30,000 people. And it was this group, the Madres de Plaza de Mayo, that last week led the 28th Marcha de la Resistencia a protest that also included, unions, unemployed organisations and hospital workers. <br />
 <br />
The founding members first met back in the early days of the Junta in 1976 while, as mothers of 'disappeared' people who were still hoping for answers, haunted police stations, military barracks, churches and the Ministry of the Interior. After the first protest they met and grew in whatever ways were available, even when directly confronted with state terror - three of the founding members were kidnapped and tortured then dropped, alive, into the sea. By the fall of the dictatorship in 1983 they were one of the most famous and influential protest movements in Latin America and, as housewives who habitually carried tear gas remedies in their handbags, one of the most original. <br />
 <br />
With the return of democracy the Madres continued campaigning, protesting and marching. However, in '86 they split into two factions, the Linea Fundadora (Founding Line) who decided to work 'within the system' campaigning for legislation and the Asociación (Association) who took a much more radical approach. They became involved in and remain active in a number of social causes, often taking far-left and controversial stances. However they deemed the 2006 Marcha to be their last, stating that the marches were no longer necessary as the current President - Nestor Kirchner - was a 'friend of the Madres' and not indifferent to the victims of the 'Dirty War' as Argentines refer to it. But the Linea Fundadora continues, this year with three main demands: <br />
The first demand was for justice and the incarceration in common jails for the 'genocidal murderers, their accomplices and ideologues'. Although the impunity laws that previously protected members of the military from facing punishment were repealed by Kirchner in 2003, there is still considerable anger that the guilty are not treated as common criminals and frequently permitted to remain under house arrest in luxurious surroundings paid for by their stints in the deeply corrupt Junta.  <br />
 <br />
The second was for the restoration of the identities of the children snatched from 'disappeared' people. An estimated 500 children were taken from their soon to be murdered parents and placed in the hands of 'friendly' families, their identities erased.  So far 95 of these have been located. <br />
 <br />
The last was for the reappearance of a live Julio Lopez. In 2006 Lopez - a 77 year-old retired bricklayer who was incarcerated and tortured for three years in the late seventies - was testifying at the trial of one of his torturers, Miguel Etchecolatz.  Before taking the stand for the final time in what was the first trial after the repeal, he disappeared again and has not been seen since. His continued absence is marked by monthly marches and has become a symbol of the unrepentant power the military still excerices. <br />
 <br />
But these were by no means all of the demands. The list was long and covered everything from an end to illegitimate external debt, wealth redistribution and a new 'democratic and popular' media and communications law. The process of 'memory and justice' may finally have begun and the Madres by now may all qualify for free bus passes but they're still going and with an energy and dedication that often puts the young'uns to shame. <br />
 <br />
* <a href="http://www.madresfundadoras.org.ar" target="_blank">www.madresfundadoras.org.ar</a> <br />
* <a href="http://www.madres.org" target="_blank">www.madres.org</a> <br />
* <a href="http://www.womeninworldhistory.com/contemporary-07.html" target="_blank">www.womeninworldhistory.com/contemporary-07.html</a> (in English)
</p>

</div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(175);
			
				if (SHOWMESSAGES)	{
				
						addMessage(175);
						echo getMessages(175);
						echo showAddMessage(175);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>