<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 670 - 27th March 2009 - --- In The Brown Stuff ---</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gordon brown, financial crisis, credit crunch, banking, g20 summit, royal bank of scotland, northern rock" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.g-20meltdown.org/"><img 
						src="../images_main/g20-meltdown-banner.jpg"
						alt="G20 - Meltdown in the City"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 670 Articles: </b>
<p><a href="../archive/news6701.php">Apocolypse Wow</a></p>

<p><a href="../archive/news6702.php">Met Yer Match!</a></p>

<p><a href="../archive/news6703.php">Luton Vanguard</a></p>

<b>
<p><a href="../archive/news6704.php">--- In The Brown Stuff ---</a></p>
</b>

<p><a href="../archive/news6705.php">Right Shred Fred</a></p>

<p><a href="../archive/news6706.php">Edo Inside Schnews</a></p>

<p><a href="../archive/news6707.php">Huon Cry</a></p>

<p><a href="../archive/news6708.php">Famous Fife</a></p>

<p><a href="../archive/news6709.php">Saabotage</a></p>

<p><a href="../archive/news67010.php">Raven Mad</a></p>

<p><a href="../archive/news67011.php">Well Acquitted</a></p>

<p><a href="../archive/news67012.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 27th March 2009 | Issue 670</b></p>

<p><a href="news670.htm">Back to the Full Issue</a></p>

<div id="article">

<h3><div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 IN THE BROWN STUFF 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div></h3>

<p>
<span style="font-size:13px; font-weight:bold">AS SchNEWS LOOKS AT HOW BAD THE FINANCIAL MESS REALLY IS</span> <br />
 <br />
With every one of London&#8217;s police forces set to be involved in a 7 million quid policing operation it appears Neo-Labour and their business friends are getting ready to batten down the hatches for the anti-G20 protests which are due to grind London to a halt next Wednesday and Thursday. With such a large scale operation it&#8217;s clear that the police are expecting more than yer usual band of anarcho-agitators to join in the fun. More and more people have simply had enough of the hypocrisy which lies behind the decision to bail out the banking system &#8211; aware that it amounts to little more than redistribution of wealth from the poor to the rich on an unprecedented scale.  <br />
 <br />
As if you need reminding &#8211; the European and North American economies are in free fall with a downturn that could be on a par with the Great Depression of the 1930s. Or worse. It&#8217;s a crisis that&#8217;s been caused by a debt-fuelled consumer, corporate and banking boom relying on unswerving confidence in the freedom of the market. At his Mansion House dinner speech in June 2006 the then Chancellor, Gordon Brown, told his mates in the City that their &#8220;<i>dynamism has led innovation with the most modern instruments of finance &#8230; making London a success story which sends out a message to the whole global economy.</i>&#8221; <br />
 <br />
The bankers in his audience were, however, already aware that a bonus driven culture had encouraged their staff to flog unaffordable loans to low and middle income households. Now they were looking for ways to shift these dodgy loans onto other investors. But no one volunteers to buy such worthless investments so the debts were packaged up with better loans into &#8216;Special Purpose Vehicles&#8217; (SPVs) in a process known as &#8216;securitisation&#8217; (aka fraud). <br />
 <br />
When borrowers began to default on the loans, shareholders knew that their investments were in trouble. But they had no idea how much &#8216;toxic debt&#8217; there was because securitisation hid the true content of the investment - so a selling frenzy ensued. Because the banks had been buying these SPVs on a grand scale, no one really knew how much the big financial institutions were worth - if they were worth anything at all. More than three quarters of the cash raised by Northern Rock to fund its 125% mortgage product came from securitisation and suddenly the pot ran dry. The subsequent fall of big institutions like Royal Bank of Scotland and the Halifax showed the reality: the banking system that Gordon Brown was lavishing so much praise upon just two years previously was now bankrupt. <br />
 <br />
�No one will lend to an insolvent company and, with the bank&#8217;s retail strategies based  on borrowing  in at 5% and lending at 8% - the loans dried up. Up to two thirds of the UK economy is based on consumer spending and, all of a sudden, would be shoppers had no money. At the beginning of this year the UK economy shrank at its fastest rate in more than a quarter of a century. � <br />
 <br />
<table width='250px' align='right'><tr><td><div style='width:250px; font-size: 13px; font-family:times new roman; font-style: italic ; padding:18px; color: #777777; text-align:center'>The decision to bail out the banking system amounts to a redistribution of wealth from the poor to the rich on an unprecedented scale.</div></td></tr></table>You would think that the best response to such a crisis would be to try something new. But no, the G20 is planning to solve the problem of a debt ridden economy by, er&#8230; borrowing more money. A lot of it. The cost of bailing out the financial sector is already �1.3 trillion and rising. That&#8217;s money UK plc doesn&#8217;t have, so the Chancellor has been raising money by selling government bonds or &#8216;gilts&#8217; &#8211; basically promises to repay investors high rates of interest but not for decades to come. But the debt will need repaying and with the UK&#8217;s AAA credit rating in doubt, some loans will be called in sooner rather than later. And if people are claiming the dole rather than paying taxes the only way to find the cash for the repayments will be to cut public expenditure. <br />
 <br />
�The �1.3 trillion bail out is, by coincidence, the same amount of cash that UK consumers owe to the banks. So why not simply pay off all the money we owe? Of course that would be sending out the &#8216;wrong message&#8217; &#8211; despite the government being more than willing to let negligent bankers pick up bonuses and pensions worth millions of pounds. <br />
 <br />
Meanwhile the sectors hit first by the downturn are those occupied by the lowest paid workers in the construction and service industries. OK, so some former city high-fliers have also got the boot, but it&#8217;s a lot easier to ride it out with a nice posh house and a robust savings account than it is on �60.50 a week Job Seekers Allowance. <br />
 <br />
This is from the government that, for all its anti-poverty talk, spent the entire housing benefit budget in just two hours propping up failing banks (see <a href='../archive/news647.htm'>SchNEWS 647</a>). �1.3 trillion is the equivalent of three year&#8217;s government expenditure. It&#8217;s a huge amount of cash and cuts will have to be made if the debt is to be repaid. City types are unlikely to be losing much sleep over the condition of the local hospital or school when they have private medical care and private education at their disposal. Those who have made all the costly mistakes will lose the least when public services are cut. The borrowers they drowned in debt will pay the real price of the downturn. <br />
 <br />
�Capitalism has always been an efficient tool for the redistribution of wealth from the poor to the rich and this bail out is more of the same. During the booms of the 1980s and 1990s the gap between rich and poor accelerated and this has not been helped by a taxation system which favours the wealthy &#8211; 100,000 of the biggest earners pay no tax at all (the non-domiciles). Last week documents revealed that Barclays has managed to avoid paying more than �1bn in corporation tax while RBS has now admitted the previous existence of a department focused entirely on &#8216;structured trades&#8217;, or tax evasion as it&#8217;s more commonly known. These trades included nifty practices such as backing both sides of a gamble speculating on asset prices. When the no-risk bets got accounted for, the losing side of the deal could be charged against taxable UK income and all winnings transferred into offshore tax free account. Sweet.   <br />
 <br />
Quick, give &#8216;em more money, these are the guys we can trust to sort the mess out! <br />
 <br />
When the UK government raises the cash to pay for the money it borrowed to give to the banks, it will cut the services most used by the worse off in our society. It will raise the greatest proportion of the taxes to repay that debt from the lowest paid in the country. In one year, an unemployed builder will pick up a state handout of just over three grand. Fred Goodwin who presided over large scale fraud at the Royal Bank of Scotland (which has just recorded the largest loss in UK corporate history) will get more than 700 grand courtesy of the UK tax payer. No wonder people are angry.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>banking</span>, <a href="../keywordSearch/?keyword=credit+crunch&source=670">credit crunch</a>, <a href="../keywordSearch/?keyword=financial+crisis&source=670">financial crisis</a>, <a href="../keywordSearch/?keyword=g20+summit&source=670">g20 summit</a>, <a href="../keywordSearch/?keyword=gordon+brown&source=670">gordon brown</a>, <span style='color:#777777; font-size:10px'>northern rock</span>, <a href="../keywordSearch/?keyword=royal+bank+of+scotland&source=670">royal bank of scotland</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(260);
			
				if (SHOWMESSAGES)	{
				
						addMessage(260);
						echo getMessages(260);
						echo showAddMessage(260);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>