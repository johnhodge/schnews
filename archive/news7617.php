<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 761 - 4th March 2011 - Lopp-Sided in Calais</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, calais, immigration" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 761 Articles: </b>
<p><a href="../archive/news7611.php">Penny For The Guy Ropes</a></p>

<p><a href="../archive/news7612.php">Crap Eviction Of The Week</a></p>

<p><a href="../archive/news7613.php">That'll Learn 'em</a></p>

<p><a href="../archive/news7614.php">Saying No To Nato</a></p>

<p><a href="../archive/news7615.php">Bonus Culture Uncut</a></p>

<p><a href="../archive/news7616.php">Blatant Liberty</a></p>

<b>
<p><a href="../archive/news7617.php">Lopp-sided In Calais</a></p>
</b>

<p><a href="../archive/news7618.php">Panama Moments</a></p>

<p><a href="../archive/news7619.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th March 2011 | Issue 761</b></p>

<p><a href="news761.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LOPP-SIDED IN CALAIS</h3>

<p>
<p>  
  	SchNEWS got the low-down this week from Calais Migrant Solidarity activists about the latest developments in Calais in light of police crack downs, CRS strikes and the soon-to-be-introduced fascist security law &lsquo;LOPPSI II&rsquo;.  </p>  
   <p>  
  	The new year started with a tragedy as two Palestinians jumped into a sewer to escape arrest, and never emerged. &ldquo;<em>There appears to be no investigation into their disappearance and the police are being incredibly vague about the incident</em>,&rdquo; one reports.  </p>  
   <p>  
  	Later in the month CRS riot cops staged a one-week strike to protest the cutting of their numbers - but far from being good news for the persecuted migrants, the CRS made twice as many arrests the week before for fear of falling short of their quota.  </p>  
   <p>  
  	The rolling programme of raids, harassment and beatings also continued, but mass arrests at a food distro site at the beginning of February suggested that the CRS were trying a change of tack. Activists report overhearing cop-talk to the affect that squat raids weren&rsquo;t being effective enough. <br />  
  	A week later two Palestinians were seriously injured falling from the first floor of a building to escape police. The cops told the media that the men were fleeing from a fire. The next day, another man was rushed to hospital after a similar incident.  </p>  
   <p>  
  	The new Africa House is on eviction alert over the coming months, and activists and migrants are in need of tents, tarps, clothes and other materials to help prepare. There is also a wider concern over the affect of the new &lsquo;LOPPSI II&rsquo; law on the migrant population. The law is a multifaceted attack on civil liberties in France, but the worry in the Calais jungles is the increased powers for the deportation of migrants accused of acting or speaking against the state.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1131), 130); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1131);

				if (SHOWMESSAGES)	{

						addMessage(1131);
						echo getMessages(1131);
						echo showAddMessage(1131);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


