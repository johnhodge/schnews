<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 748 - 18th November 2010 -   City Subs Picket Shame</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, journalism, workers struggles, media, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 748 Articles: </b>
<p><a href="../archive/news7481.php">Complete Hissy Fit</a></p>

<b>
<p><a href="../archive/news7482.php">  City Subs Picket Shame</a></p>
</b>

<p><a href="../archive/news7483.php">Degrees Of Resistance</a></p>

<p><a href="../archive/news7484.php">France: Screws Loose</a></p>

<p><a href="../archive/news7485.php">Love To Haiti</a></p>

<p><a href="../archive/news7486.php">There Will Be Mud</a></p>

<p><a href="../archive/news7487.php">New Feature: Dyer Warnings</a></p>

<p><a href="../archive/news7488.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 18th November 2010 | Issue 748</b></p>

<p><a href="news748.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>  CITY SUBS PICKET SHAME</h3>

<p>
<p>
	At SchNEWS we are accustomed to cranking out the news every week with the ever present spectre of financial ruin breathing down our necks. For our comrades in the mainstream media, however, it seems like a state of affairs they&rsquo;re not quite as comfortable with. </p>
<p>
	Whilst the Paxman-flummoxing BBC strikes over plans to chainsaw staff pensions grabbed all the headlines (at least the ones written by scab labour), local news-hounds around the country have also been taking action over mass redundancies and plans to put the squeeze on those that remain. </p>
<p>
	In Brighton, staff at local rag the Argus launched a feeble attempt to challenge SchNEWS&rsquo; position as the most radical purveyors of news in the Brighton and Hove area. </p>
<p>
	Yesterday (18th) saw the first day of a 48-hour strike over plans to sack the entire subs desk and ship operations to Southampton. Whilst we have no love for the team that brought us such classic Smash EDO-based headlines as &ldquo;SHAMEFUL&rdquo;, the move is another blow to the concept of a local newspaper. As SchNEWS went to print the Argus had yet to mention the strike in its own pages. </p>
<p>
	The Argus is owned by Newsquest &ndash; the mini-me local version of Murdoch&rsquo;s evil national empire. Itself owned by the US&rsquo;s largest publisher, Newsquest owns 17 Dailies and over 200 weeklies - and titles across the country are set to be hit by the latest round of cutbacks. They have already announced plans for mass-sackings at the Bradford Telegraph &amp; Argus, Ilkley Gazette, Wharfedale &amp; Airedale Observer and the Keighley News. The journalists then have the option of scrabbling for a reduced number of replacement jobs on far worse terms and conditions. </p>
<p>
	Earlier in the month, 75% of the editorial staff at the Southampton Daily Echo staged a 48-hour walkout after a pay freeze since June 2008, the closure of the final salary pension scheme and the scrapping of loyalty payments. </p>
<p>
	Whether it affects the quality of the drivel many local rags churn out remains to be seen - SchNEWS has been produced by scab labour and agency temps since the editorial staff went on strike in 1996 and no one has ever noticed. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1014), 117); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1014);

				if (SHOWMESSAGES)	{

						addMessage(1014);
						echo getMessages(1014);
						echo showAddMessage(1014);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


