<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 707 - 29th January 2010 - Citizen's Arrest Of The Week</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tony blair, war crimes, chilcot, game show, bounty hunters, iraq" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 707 Articles: </b>
<p><a href="../archive/news7071.php">Lanarky In Action</a></p>

<b>
<p><a href="../archive/news7072.php">Citizen's Arrest Of The Week</a></p>
</b>

<p><a href="../archive/news7073.php">Edl: Gone To Potteries</a></p>

<p><a href="../archive/news7074.php">Calais: Gimme Shelter</a></p>

<p><a href="../archive/news7075.php">Festival Clampdown: Toking The Piss</a></p>

<p><a href="../archive/news7076.php">Crawley: No Boarders</a></p>

<p><a href="../archive/news7077.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th January 2010 | Issue 707</b></p>

<p><a href="news707.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CITIZEN'S ARREST OF THE WEEK</h3>

<p>
<strong><u>For War Crimes...</u></strong> <br />  <br />  You probably wouldn&rsquo;t need much encouragement to try and perform a citizen&rsquo;s arrest on Britain&rsquo;s greatest living War Criminal, Tony Bliar. But, just in case you are the kind of money-grabbing mercenary who does, someone has had the genius idea of offering hard cash to anyone who can collar the mad vicar. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/707-Blair-Wanted-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/707-Blair-Wanted-sm.jpg"  alt='Wanted'  /></a></td></tr></table>They have set up a website called <a href="http://www.arrestblair.org" target="_blank">www.arrestblair.org</a> and invited donations to offer up as bounty to anybody who can stand hand to shoulder with the Great One and invite him to accompany them to the nearest police station to await trial for his crimes against humanity.&nbsp; <br /> A popular idea or have people &lsquo;moved on&rsquo; as Blair prepares for his smarm offensive in front of the Chilcot Whitewash? Within two days they had over &pound;9000 quid in the kitty. Not quite the billions spent on murdering up to a million in Iraq, but not to be sniffed at (SchNEWS is already hard at work cooking up a masterplan to bag the cash). <br />   <br /> One quarter of whatever is in the pot is available to everybody who manages it - like a lucrative reality gameshow (when to strike?! Let that pot build up, but don&rsquo;t wait too long!) - with the added bonus of leaving &lsquo;man of the people&rsquo; Blair feeling hounded in his homeland and in the news for all the wrong reasons. <br />       <br /> For a full briefing on your mission, should you choose to accept it, go to <a href="http://www.arrestblair.org" target="_blank">www.arrestblair.org</a> <br />   <br /> This article will self destruct in 5,4,3... <br />  <br /> *For those chomping at the bit to administer some justice, Blair will appear before Chilcot and chums today. Stop The War are pushing ahead with plans for a demo on the green in front of the QEII despite police opposition.  <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>bounty hunters</span>, <span style='color:#777777; font-size:10px'>chilcot</span>, <span style='color:#777777; font-size:10px'>game show</span>, <a href="../keywordSearch/?keyword=iraq&source=707">iraq</a>, <span style='color:#777777; font-size:10px'>tony blair</span>, <span style='color:#777777; font-size:10px'>war crimes</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(635);
			
				if (SHOWMESSAGES)	{
				
						addMessage(635);
						echo getMessages(635);
						echo showAddMessage(635);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


