<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 754 - 14th January 2011 - Keep It Civil</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, austerity, legal aid" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 754 Articles: </b>
<p><a href="../archive/news7541.php">Fight Them On The Beeches</a></p>

<p><a href="../archive/news7542.php">Call Out Corner</a></p>

<b>
<p><a href="../archive/news7543.php">Keep It Civil</a></p>
</b>

<p><a href="../archive/news7544.php">That's End-detainment</a></p>

<p><a href="../archive/news7545.php">Ema-dness</a></p>

<p><a href="../archive/news7546.php">Roughed Up Diamond</a></p>

<p><a href="../archive/news7547.php">Bonneville Rendevous</a></p>

<p><a href="../archive/news7548.php">Bastard Offspring</a></p>

<p><a href="../archive/news7549.php">Extinguisher Not Let Off</a></p>

<p><a href="../archive/news75410.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th January 2011 | Issue 754</b></p>

<p><a href="news754.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>KEEP IT CIVIL</h3>

<p>
<p>
	Dishevelled cigar-chomping fatty Ken Clarke&rsquo;s first foray into the world of swingeing cuts resulted in the surprisingly liberal plan to cut the prison population. In case anyone thought the old Tory rogue was going, err, rogue in the twilight of his career though, the &lsquo;No&rsquo; Justice minister&rsquo;s latest swipe with the spending sythe, is aimed squarely at the poor and unfortunate. Clarke&rsquo;s plans to reform civil legal aid will leave hundreds of thousands of vulnerable people facing destitution, debt, homelessness and miscarriages of justice with no access to legal recourse. </p>
<p>
	In an assault that makes Thatcher&rsquo;s attempts to cut legal aid begin to look mild, Clarke&rsquo;s plans aim to reduce the number of legal aid cases by half a million a year - moving entire legal areas out of the reach of yer averagely financed Joe. On the chopping block are debt advice, social welfare, employment, personal injury, clinical negligence and areas of housing. </p>
<p>
	The Tories claim the move will put an end to litigation culture. However in reality, it is those fighting evictions, unfair dismisal, non-payment of wages and police victimisation who will find themselves helpless, while chancers out for the &lsquo;compo&rsquo; can try their hand with &lsquo;no win no fee&rsquo; sharks &ndash; the Tories&rsquo; Big Society taking up the slack. </p>
<p>
	The right-wing press has merrily jumped on the bandwagon, hailing it as a blow to the &lsquo;fatcat&rsquo; lawyer parasites sucking tax-payers dry. Yet the truth is legal aid lawyers tend to operate in the one corner of the profession that doesn&rsquo;t exist to live it up on the backs of other people&rsquo;s misery. The average wage for a legal aid brief is &pound;25,000, a pittance in comparison to the fortunes to be leeched out of the private sector. Many of these lawers are now staring at unemployment as law centres and firms offering free advice close. Those that cling on to their jobs can expect a 10% cut in fees. </p>
<p>
	The prosecution rests: see the Facebook campaign &lsquo;Say no to legal aid cuts in the South West&rsquo;, or drop Camer-Leg and cronies a line and tell &lsquo;em what you think at <a href="http://www.justice.gov.uk/consultations/legal-aid-reform-151110.htm," target="_blank">www.justice.gov.uk/consultations/legal-aid-reform-151110.htm,</a> or better still, turn out for the demo outside the South-West offices of the Legal Services Commission: Queens Square, Bristol, 7/02/11 from 12 noon. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1058), 123); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1058);

				if (SHOWMESSAGES)	{

						addMessage(1058);
						echo getMessages(1058);
						echo showAddMessage(1058);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


