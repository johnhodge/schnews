<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 767 - 15th April 2011 - Taking Libya-Ties</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, libya, arab spring, africa, nato" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 767 Articles: </b>
<b>
<p><a href="../archive/news7671.php">Taking Libya-ties</a></p>
</b>

<p><a href="../archive/news7672.php">Southern Climes</a></p>

<p><a href="../archive/news7673.php">Ed-f Off</a></p>

<p><a href="../archive/news7674.php">Mexico: Grave Situation</a></p>

<p><a href="../archive/news7675.php">Benefit Of Doubt</a></p>

<p><a href="../archive/news7676.php">Social Centre Plus</a></p>

<p><a href="../archive/news7677.php">Inside Schnews</a></p>

<p><a href="../archive/news7678.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th April 2011 | Issue 767</b></p>

<p><a href="news767.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TAKING LIBYA-TIES</h3>

<p>
<p>  
  	<strong>AS SCHNEWS GETS AN EYEWITNESS REPORT FROM LIBYA</strong>  </p>  
   <p>  
  	Medyan Daireh, a Brighton-based Al Jazeera journalist who has previously covered conflicts in Iraq, Palestine, Afghanistan, Turkey and Kosovo (see <a href="../features" target="_blank">www.schnews.org.uk/features</a>) has just returned from a stint in Libya. He covered the uprising against Colonel Gadaffi, spent weeks living with the rebels and visited all of the rebel-controlled areas. SchNEWS took the opportunity to get his views on the rebels&rsquo; struggle and the situation in Libya:  </p>  
   <p>  
  	<em><strong>Who are the rebels? Are they different factions or are they a unified group? How much popular support do they have?</strong></em>  </p>  
   <p>  
  	The people who started the revolution in Libya are called the &lsquo;Youth coalition of February 17th&rsquo;. The rebels are first of all civilians; such as doctors, workers, engineers, solicitors, teachers, imams. They are from all classes within Libyan society. They do not have a clear political direction because of the political repression to which they&rsquo;ve been subjected for the last forty-two years. Gaddafi, and sometimes the Western press, accuses the rebels of being part of Al Qaida. There is no Al Qaida there, and no other groups, only rebels.  </p>  
   <p>  
  	There are various different groups of rebels, but they are mainly divided not on political lines, but rather because of lack of communications. All the people in Libya support the rebels and help them in practical terms in all possible ways. Some Libyans even gave the rebels their cars. The resources of the revolutionaries and of the National Council are very limited, but people support them as much as they can.  </p>  
   <p>  
  	<strong><em>How are they organising? Are they only organising militarily, or politically and institutionally as well? Is it centralised in Benghazi or dispersed?</em></strong>  </p>  
   <p>  
  	The rebels are very different and the leaders find it difficult to organise them. Being a fairly recent group (two months old), they still lack organisation, in spite of the new leadership trying to improve the situation. Most lack any military training and weapons among them are scarce.  </p>  
   <p>  
  	There is also another issue - you have the rebel groups and the military council. The latter were former members of Gaddafi&rsquo;s army, but left the Colonel and joined the revolution. They have military experience and radio devices to communicate. Both groups hold talks and work together. Just now I was told by the Military Council that both groups are under one leadership.  </p>  
   <p>  
  	They have also agreed to strictly prevent the very young and the very old joining the fighting. The Libya National Council is the institutional face of the rebels, and is supported by all rebels. Benghazi is the centre, and represents the leadership. Other towns have committees of the council.  </p>  
   <p>  
  	<em><strong><a href="../images/767-medyan-lg.jpg" target="_blank"><img style="border:2px solid black;width: 300px; height: 242px; margin-left: 10px"  align="right"  alt=""  src="http://www.schnews.org.uk/images/767-medyan-sm.jpg"  /> </a>What is life like in the rebel-controlled areas? How far has the state receded and what has replaced it? How does this compare with life in Gaddafi-controlled areas?</strong></em>  </p>  
   <p>  
  	The rebel-controlled areas enjoy good security. Up to now the people do not trust the police because of their role against the revolution. At the moment the National Council is setting up a new police force with a new name and new uniform, to make them more acceptable to people.  </p>  
   <p>  
  	Since the beginning of the revolution the schools have been closed. Most work places are not functioning. Some towns, constantly shelled and attacked by the Gaddafi&rsquo;s forces, have in large part been abandoned. However, food, medicines, and now even petrol are ready available. The Libyan National Council is replacing the state. Ordinary citizens have organized themselves to provide civil services on a voluntary basis. They are strongly united in everything. By contrast, in the Gaddafi- controlled area you have long queues to get basic food and petrol.  </p>  
   <p>  
  	<em><strong>How do they feel about the NATO intervention? What are their feelings about outside help in general?</strong></em>  </p>  
   <p>  
  	When the UN resolution for the no flight zone was passed, it was midnight in Libya. People could not sleep from the relief and kept celebrating. However the initial enthusiasm was greatly reduced by NATO&rsquo;s perceived insufficient bombing of Gaddafi&rsquo;s forces. This lack of NATO determination hindered the rebels&rsquo; advance. Moreover, the rebels lack proper weaponry and ammunition, some of them having only binoculars or a sword. In spite of this, as I witnessed, they have still managed to conquer towns like Ajdabiya. The rebels want more serious NATO attacks on Gaddafi&rsquo;s forces, more weapons and better communication. Gaddafi, on the other hand, has very sophisticated weapons and &lsquo;grad&rsquo; rocket launchers.  </p>  
   <p>  
  	<strong><em>How much control over the country does Gaddafi retain? What remains of his state institutions and security forces?</em></strong>  </p>  
   <p>  
  	Gaddafi still controls Tripoli and some areas in the west of the country. He also faces internal dissent that is only starting to shape now, even in the capital. His army is mainly made up of mercenaries from different countries such as Niger, Chad, Mali, Colombia, Mexico, and other poor countries. I have met some soldiers captured by the rebels. Some of them told me that they have been in Gaddafi&rsquo;s army for three or four years. The CIA and other secret organizations in Europe must have known this for quite a while but have kept silent.  </p>  
   <p>  
  	<strong>How strong is his military? Who are they and how have they been acting?</strong>  </p>  
   <p>  
  	I was present during the Ajdabiya battle. Gaddafi&rsquo;s forces had organised a siege at the entrance of the town. I witnessed an official from Ajdabiya approaching the rebels and announcing to them that Gaddafi&rsquo;s forces were ready to surrender. The rebels refused to accept because of dissent among Gaddafi&rsquo;s forces. The following day at 11am, 140 men from the rebels attacked Gaddafi&rsquo;s forces and surrounded them in spite of the rebels&rsquo; poor weaponry, such as kalashnikovs, RPG-7s and some heavy machine guns. By 3pm Gaddafi&rsquo;s army had run away. Many soldiers fled without their tanks and weapons and without taking the injured with them. The rebels captured four soldiers, one Libyan and three from Chad. The ground was full of weapons and military uniforms left behind. Gaddafi&rsquo;s forces fight from a distance but seem not to relish it face to face.  </p>  
   <p>  
  	<em><strong>What about the role NATO is playing? What do you think their intentions and objectives are?</strong></em>  </p>  
   <p>  
  	Many believe NATO&rsquo;s participation is insincere. The rebels believe that NATO is deliberately prolonging the conflict whilst they&rsquo;d like to finish it quickly. In spite of this, the rebels are ready for a long fight. They are the youth which started the revolution for a free and independent Libya.  </p>  
   <p>  
  	They don&rsquo;t favour having any NATO soldiers or other foreign intervention on the ground. They have rejected even volunteer fighters from the Arab world. They told me that they have been patient for 42 years, so they can fight for longer.  </p>  
   <p>  
  	<em><strong>How do you think the conflict will develop?</strong></em>  </p>  
   <p>  
  	I think that it will be a long struggle, because the rebels insist that they do not want any foreign intervention on the ground. NATO meanwhile is searching for somebody to back for leadership who will support American and Western objectives in the region, but the rebels are unwilling to accept any such compromise forced on them.  </p>  
   <p>  
  	According to what I saw on the ground, I can&rsquo;t believe that NATO is looking to get rid of Gaddafi unless they find a suitable replacement.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1180), 136); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1180);

				if (SHOWMESSAGES)	{

						addMessage(1180);
						echo getMessages(1180);
						echo showAddMessage(1180);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


