<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 739 - 17th September 2010 - Scottish Open Cast Mining: What's Happendon?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, happendon wood, action camp, coal" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 739 Articles: </b>
<p><a href="../archive/news7391.php">Gauling Behaviour</a></p>

<p><a href="../archive/news7392.php">Tesco Check-out</a></p>

<p><a href="../archive/news7393.php">Cut To The Chase: Anti-austerity Coalition Launched</a></p>

<p><a href="../archive/news7394.php">Fire To The Prisons</a></p>

<p><a href="../archive/news7395.php">Calais: Channel Hoping</a></p>

<b>
<p><a href="../archive/news7396.php">Scottish Open Cast Mining: What's Happendon?</a></p>
</b>

<p><a href="../archive/news7397.php">Fash Mobs</a></p>

<p><a href="../archive/news7398.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th September 2010 | Issue 739</b></p>

<p><a href="news739.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCOTTISH OPEN CAST MINING: WHAT'S HAPPENDON?</h3>

<p>
<p>
	Early on Sunday (12th) evening, a group of activists descended on Happendon wood, in South Lanarkshire, to occupy an area that is likely to be mined by Scottish Coal. Around 30 activists have spent a busy week setting up camp and preparing infrastructure for what could be a long-term occupation. The camp already boasts a two-storey communal building and kitchen, shelters and defences. </p>
<p>
	Campers say the atmosphere is positive and energetic. They report that the daily visits of police have so far been chilled. Scottish Coal and land-leasers Douglas &amp; Amgus Estates (Lord Home) have yet to make an appearance. Given that last year&rsquo;s Mainshill camp just down the road survived for seven months and took a five day eviction and 45 arrests to clear (see <a href='../archive/news708.htm'>SchNEWS 708</a>), there must be a lot of head scratching going on at Scottish Coal HQ. </p>
<p>
	The Happendon Wood Action Camp is part of the longer campaigns of Coal Action Scotland, and the local community, who resist coal mining expansion in the Douglas Valley area - already one of the most heavily mined in Europe. Scottish Coal has not yet publicly declared plans to mine the area. However, planning permission for mixed use development on the land has been applied for by the Scottish Resources Group. As there is recoverable coal on site, the area will have to be mined before development - not doing so would be illegally &lsquo;sterilising the nations assets&rsquo;. </p>
<p>
	The camp is not only focussed on protecting the woodland from development through direct action, but also on acting as a base for community and activist resistance against new fossil fuel production in the region. </p>
<p>
	Happendon Wood is 30 miles south of Glasgow. The camp encourages visitors, helpers and material/financial donations. See <a href="http://coalactionscotland.org.uk/?page_id=1974" target="_blank">http://coalactionscotland.org.uk/?page_id=1974</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(941), 108); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(941);

				if (SHOWMESSAGES)	{

						addMessage(941);
						echo getMessages(941);
						echo showAddMessage(941);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


