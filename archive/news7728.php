<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 772 - 20th May 2011 - Mound Zero</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, guerilla gardening, community garden, climate camp, brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT June 1st 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 772 Articles: </b>
<p><a href="../archive/news7721.php">A Bit Of Hows Yer Intifada?</a></p>

<p><a href="../archive/news7722.php">Angel Grinders</a></p>

<p><a href="../archive/news7723.php">Bell End?</a></p>

<p><a href="../archive/news7724.php">Madrid Fer It</a></p>

<p><a href="../archive/news7725.php">It Makes No Census</a></p>

<p><a href="../archive/news7726.php">Mexico: Poetic Justice</a></p>

<p><a href="../archive/news7727.php">No Stokes Without Fire</a></p>

<b>
<p><a href="../archive/news7728.php">Mound Zero</a></p>
</b>

<p><a href="../archive/news7729.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th May 2011 | Issue 772</b></p>

<p><a href="news772.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MOUND ZERO</h3>

<p>
<p>  
  	Brighton community garden The Mound was evicted by force in in the early hours of Wednesday morning (May 18th).&nbsp; Around twenty heavies and eight coppers turned up, taking the opportunity to swoop on the site whilst most of the gardeners were out of town visiting another community garden at Heathrow. &nbsp;  </p>  
   <p>  
  	By the time the The Mound collective rushed back to Brighton, the site was heavily guarded and the bulldozers were already in full swing.&nbsp; There was one arrest around as a protester tried to scale the walls to resist the eviction.&nbsp; He was cuffed and dragged down the street by cops just as more supporters started to arrive.  </p>  
   <p>  
  	The guerilla gardeners stood together, with signs and placards, and watched as &lsquo;The Mound&rsquo; signage was tossed aside. The bailiffs then spent twenty paid minutes literally polishing the Hargreaves &lsquo;To Let&rsquo; sign.&nbsp; Well, if its gonna last another ten years, folks, it really does need to sparkle...  </p>  
   <p>  
  	<strong>* Meanwhile Lewes Climate Camp</strong> (see <a href='../archive/news769.htm'>SchNEWS 769</a>) also faces eviction having lost their legal battle to remain in the disused grounds of a former special needs school &ndash; St Anne&rsquo;s - handily located under the windows of the county council offices. Can it be mere coincidence that both county and district councils became Tory controlled the week before?  </p>  
   <p>  
  	The camp&rsquo;s original plan to stay a week was torn up after garnering healthy local support and finding out the council had designs on selling the site for prime development. A new occupation called STAND (St Anne&rsquo;s Diggers) was started. Three weeks in, the well organised and family-friendly site is still going strong and taking to the trees ahead of next Wednesday&rsquo;s expected eviction attempt.  </p>  
   <p>  
  	There will be an open meeting on Sunday afternoon for people supporting their attempts to stay and eventually hand over the precious open space to Lewes residents. See <a href="http://www.climatecamp.org.uk" target="_blank">www.climatecamp.org.uk</a> <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1228), 141); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1228);

				if (SHOWMESSAGES)	{

						addMessage(1228);
						echo getMessages(1228);
						echo showAddMessage(1228);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


