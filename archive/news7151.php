<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 715 - 26th March 2010 - A Twist Of Fete</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, festivals, police, cambridge" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 715 Articles: </b>
<b>
<p><a href="../archive/news7151.php">A Twist Of Fete</a></p>
</b>

<p><a href="../archive/news7152.php">Thimble Pleasures</a></p>

<p><a href="../archive/news7153.php">Met With Undue Force</a></p>

<p><a href="../archive/news7154.php">New Deal Or No Deal</a></p>

<p><a href="../archive/news7155.php">Gimme Fife</a></p>

<p><a href="../archive/news7156.php">Edl: Bolton Blunderers</a></p>

<p><a href="../archive/news7157.php">Ee By Heckler</a></p>

<p><a href="../archive/news7158.php">Speak-ing Out</a></p>

<p><a href="../archive/news7159.php">Titnore: Jack Out The Box</a></p>

<p><a href="../archive/news71510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/715-strawberry-fair-lg.jpg" target="_blank">
													<img src="../images/715-strawberry-fair-sm.jpg" alt="A Midsummer Night's Mare" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 26th March 2010 | Issue 715</b></p>

<p><a href="news715.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A TWIST OF FETE</h3>

<p>
<strong>STRAWBERRY FAIR CANCELLED AFTER POLICE INTERVENTION...</strong> <br />  <br /> <strong>&ldquo;<em>It&rsquo;s rapidly becoming obvious that this is a nationwide issue - somehow the police have got it into their heads that music and dancing are bad for society. They&rsquo;ve used the licensing laws as a weapon.</em>&rdquo; - Strawberry Fair organiser</strong> <br />  <br /> In what is fast becoming a summer tradition, yet another gathering has fallen victim to party-pooping police tactics. The UK&rsquo;s last free open-air festival has been cancelled under police pressure. Strawberry Fair, in Cambridge, viewed by many as the starting gun for the festival season, has been stopped in its tracks after 37 straight years. Some (on Facebook at least) are vowing that some kind of festival will go ahead anyway. <br />  <br /> The fair had actually been granted a full licence on March 2nd by Cambridge city council, despite police objections. However the cops used their discretionary powers to appeal the decision to the magistrates court. A final decision on the outcome wouldn&rsquo;t have been made until just a few weeks before the event. According to Justin Argent, chairman of the organising committee &ldquo;<em>The timetable for the appeal means we will not know whether the fair can go ahead as planned until far too late in the day. We do not want to pass this risk on to the supportive suppliers, traders and artists whose livelihoods would be severely damaged by a last-minute cancellation</em>.&rdquo; Regular SchNEWS readers will remember the problems that followed the last minute cancellation of last year&rsquo;s Big Green Gathering (see <a href='../archive/news685.htm'>SchNEWS 685</a>). <br />  <br /> The council weighed in with a spirited denunciation of the police. Jennifer Liddle, chairman of the city council&rsquo;s licensing committee, said, &ldquo;<em>It is a great shame an unelected and unaccountable police force decided to ignore that decision and lodge an appeal. Not only is their decision to try to ban Strawberry Fair undemocratic, it has cost an enormous amount of money</em>&rdquo;. Even local MP David Howarth chipped in, &ldquo;<em>It should be left to democratically elected councillors to make decisions like these that affect our city, not unelected police officers</em>.&rdquo; <br />   <br /> In previous years the festival was conducted under the council&rsquo;s general entertainments licence for Midsummer Common. This year however the committee had to apply for a separate licence, which opened up the process to police intervention. Cambridge police assembled a legal team, including  London based barrister Elliot Gold and Avon and Somerset Police&rsquo;s Inspector Ian Ross, who fronted the Big Green Clampdown. They presented a 75 page dodgy dossier, which claimed that &ldquo;music and dancing led to drug taking&rdquo; (in our rhythmically challenged experience it&rsquo;s actually the other way round), and so consequently the event encouraged anti-social behaviour. <br />   <br /> Also submitted was an unwittingly hilarious DVD, that mainly consists of soft focus shots of circles of cross-legged people who may or may not be skinning up. The highlight reel also includes a few voyeuristic shots of public urination and some lardy bouncers beating someone up. The DVD was published on Cambridge Police&rsquo;s website, but hastily taken down after a public outcry. <br />   <br /> <strong>FAIR PLAY?</strong> <br />  <br /> Strawberry Fair is an entirely volunteer run non-profit event. Justin Argent, Chair of Strawberry Fair said, &ldquo;<em>As a voluntary group we don&rsquo;t have access to the kinds of legal resource or the public finance used by Cambridgeshire Police in their objections to the license. It is going to be an very tough task for us to prepare for the appeal hearing both in time and money</em>&rdquo;. She added, &ldquo;<em>Gaining a secure license that will allow the Fair to continue for many years to come has to be the top priority for the Strawberry Fair committee. The police appeal of the decision made by Cambridge City Council means that we now have to put all our efforts into fighting that appeal, rather than into the detailed preparations for the 2010 Fair which should now be taking place, and we are not prepared to compromise either the appeal or the Fair itself</em>&rdquo; <br />  <br /> Weirdly, at the end of last year&rsquo;s fair the police told the local paper that they were &lsquo;not aware of any major incidents at the event&rsquo;.  Now the party line is &ldquo;<em>Over the years, the event has attracted a large number of people - many from outside Cambridge - who see it as nothing more than an excuse to drink to excess and engage in illegal drug use, whilst at the same time becoming involved in anti-social behaviour disturbing to other fairgoers and city residents</em>.&rdquo; <br />  <br /> So why the sudden change of tune? According to one organiser, &ldquo;<em>They didn&rsquo;t have serious issues until we decided we weren&rsquo;t going to pay them - this year they just wanted a nominal fee of &pound;1,500.  But that was set to change and rise to potentially thousands of pounds a year. Our attitude was that as the festival was free for all the people of Cambridge, there shouldn&rsquo;t be any charge by the police. As soon as we refused to pay this process of licence, objections started. We&rsquo;ve no idea what their ultimate agenda is but it&rsquo;s clear they don&rsquo;t want the fair to continue in its present form</em>&rdquo;. <br />  <br /> Organisers also foresaw problems with policing at this year&rsquo;s fair if the police ban had been refused. &ldquo;<em>Even if the police lost their appeal there is no reason to suppose that would be the end of their campaign and a fair this year would have a pretty much unbearable and unmanageable police presence, and the real possibility of court cases for the license holder. We are but one of a number of events that are bearing the brunt of an overzealous police force, and last summer proved that even established, gated events (Big Green, Thimbleberry), with almost non-existent crime rates are not immune from this</em>.&rdquo; <br />  <br /> Festival organisers have stressed that they feel that an unauthorised gathering might be unhelpful to the continuance of the fair in future years. However one told us &ldquo;<em>We don&rsquo;t want to encourage it, but its clear that people do value the event possibly more than they value the organising team behind it. Strawberry is really important to people in Cambridge</em>&rdquo; <br />  <br /> Organisers are calling out for donations to help fund the fight against the appeal so Strawberry Fair has the chance to officially go ahead next year. Meanwhile &ndash; check out the unofficial group on Facebook: They&rsquo;ve cancelled Strawberry fair - I&rsquo;ll still turn up <br />  <br /> * See <a href="http://www.strawberry-fair.org.uk/" target="_blank">www.strawberry-fair.org.uk/</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=cambridge&source=715">cambridge</a>, <a href="../keywordSearch/?keyword=festivals&source=715">festivals</a>, <a href="../keywordSearch/?keyword=police&source=715">police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(706);
			
				if (SHOWMESSAGES)	{
				
						addMessage(706);
						echo getMessages(706);
						echo showAddMessage(706);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


