<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 682 - 3rd July 2009 - Coup Blimey</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, honduras, manuel zelaya, latin america, coup" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 682 Articles: </b>
<p><a href="../archive/news6821.php">Rumble In The Jungle</a></p>

<b>
<p><a href="../archive/news6822.php">Coup Blimey</a></p>
</b>

<p><a href="../archive/news6823.php">Don&#8217;t Coal Home  </a></p>

<p><a href="../archive/news6824.php">A Lot To Ansar For</a></p>

<p><a href="../archive/news6825.php">Island Mentality  </a></p>

<p><a href="../archive/news6826.php">Sez Who?</a></p>

<p><a href="../archive/news6827.php">Ich Bin Ein Burnin</a></p>

<p><a href="../archive/news6828.php">Lions And Tigers</a></p>

<p><a href="../archive/news6829.php">Squats Up: South West London Squat Round Up</a></p>

<p><a href="../archive/news68210.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd July 2009 | Issue 682</b></p>

<p><a href="news682.htm">Back to the Full Issue</a></p>

<div id="article">

<div style='font-weight:bold; font-size:13px'><div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 AN INDEPTH LOOK AT THIS WEEK&#8217;S COUP D&#8217;ETAT IN HONDURAS 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div></div>

<h3>COUP BLIMEY</h3>

<p>
<strong></strong> <br /> <font face="courier new"><em>&ldquo;Imagine if I had proposed a real reform? They would have executed me on the spot.&rdquo;</em></font> - <strong>Manuel Zelaya, deposed President of Honduras</strong> <br />  <br /> Early in the morning of Sunday the 28th dozens of soldiers stormed the Honduran presidential residence, disarming and beating the guards before seizing the pyjama clad president, Manuel Zelaya, and escorting him to a waiting plane destined for Costa Rica. Within hours protesters were out on the streets confronting the military and the coup was being condemned by every international government and organisation from the US to the UN. <br />  <br /> By the time of the operation, the military had been surrounding the residence for several days in response to a chain of events that began when Zelaya proposed a popular consultation to determine whether November&rsquo;s presidential poll &ndash; in which Zelaya can&rsquo;t stand due to the one term limit &ndash; should include a referendum on whether to elect an assembly to re-write the constitution. The Supreme Court declared the consultation illegal and ordered the police and military to not distribute ballot boxes, threatening those that did with 8&ndash;12 year prison sentences for &lsquo;abuse of authority&rsquo;. The army sided with the Supreme Court and confiscated the ballot boxes and election materials. In retaliation Zelaya fired the head of the military&rsquo;s Joints Chiefs of Staff, General Romeo V&aacute;squez. The Honduran Congress then promptly began an investigation into Zelaya, not only over his &lsquo;administrative actions&rsquo; but also into his mental health. <br />  <br /> Not to be intimidated, Zelaya led a caravan of 25&ndash;30,000 (claimed Radio Es De Lo Menos &ndash; an independent Honduran radio station present on the caravan, while Associated Press said &ldquo;dozens&rdquo;) to military bases to retrieve the boxes and distribute them in time for the Sunday poll. With everything set up for the poll the military moved in.&nbsp; <br />  <br /> As far as our lazy and ignorant media are concerned, ambivalence prevails. Even as the White House (yes, that White House &ndash; supporter of just about every Latin American military coup since independence) condemned the coup in surprisingly unambiguous terms - labelling it a coup, stating that they only recognise Zelaya as president, suspending joint military operation with the Honduran army and threatening prompt action with the Organisation of American States (OAS). Weird, it&rsquo;s going to take some years to get used to a president who uses diplomacy. The media has been happy to follow the line of the Honduran right-wing press - framing it as a simple case of deposing a leftist megalomaniac who was in the process of a power grab. Even the most rudimentary analysis (a SchNEWS speciality) demonstrates this clearly isn&rsquo;t the case. <br />  <br /> Zelaya was elected in 2005, not as a populist leftist, but as the candidate of one of the two main parties - the Liberals - who over the years had carved up power between themselves and the Conservative party and kept politics a strictly elite business. Zelaya, however, broke ranks, first by joining Hugo Chavez&rsquo;s Bolivarian Alternative for the Americas (ALBA), a grouping of Left leaning Latin American states committed to alternative strategies of cooperation and development (i.e without Uncle Sam&rsquo;s strings attached dollar), and then by implementing various pro-poor policies such as raising the minimum wage by 60% (i.e. about $1.20 - most live on less than $2 a day). <br />  <br /> The current crisis was triggered when Zelaya, with just a few months remaining of his one term, proposed a referendum which would have asked the Honduran people one, yes or no, question; &ldquo;<em>Do you think that the November 2009 general elections should include a fourth ballot in order to make a decision about the creation of a National Constitutional Assembly that would approve a new Constitution?</em>&rdquo; Even though Zelaya did not once mention presidential terms and the proposed assembly would have convened after the end of his term before probably spending several years bitterly debating reform which may or may not have included mention of term limits, this was quickly reported in the Honduran right-wing media as being an attempt to secure the ability to extend his rule. This idea, which was only ever stated as opinion, was seized on by &ldquo;cut-n-paste&rdquo; hacks (step up those liberal giants The Guardian and The New York Times) and repeated until it became an accepted truth. <br />  <br /> Like in most of Latin American, the Honduran constitution is an elite stitch-up designed to ensure that no matter who the people elect, real structural change is impossible and power remains concentrated in the hands of the wealthy and powerful. As the current coup government claim, the constitution does state that seven of its 379 articles are not subject to reform. One of these articles refers to term limits and states that anyone who proposes changes can be removed and be disqualified from office for ten years. However, what Zelaya proposed was the election of an assembly to completely re-write - not reform - the constitution, and the issue of term limits would have been for them to decide. Nevertheless, following the Supreme Court judgement, Zelaya changed what would have been a binding consultation to non-binding, effectively an opinion poll, nothing more. <br />  <br /> The constitution also states that it is necessary to gain a two thirds majority of a specially convened Congress to depose of the head of the army. Which raises the question of what sort of constitution allows the immediate removal of an elected president but requires a massive action to remove the head of the army? No need to ask where power really lies. <br />  <br /> <strong>BANANA REPUBLIC</strong> <br />  <br /> Since Zelaya was deposed and replaced by the military backed President of Congress Roberto Micheletti, a fellow Liberal, Honduras has been in lock-down. After imposing a curfew from 9pm and 6am, the coup regime immediately set about clamping down on both resistance and anyone reporting on it. <br />  <br /> Protests began within hours of the coup, with supporters of the president, grass-roots social movements and anyone not up for living under an junta, pouring onto the streets to demonstrate against the regime. On the first day of the coup protesters set up barricades of burning tyres in the streets outside the presidential residence to restrict the military&rsquo;s movements and prevent the arrival of reinforcements. Protests have since broken out around the country with marches, blockades of major transit routes and strikes, including a nationwide teachers strike. The police and military have responded with tear gas, rubber bullets, water cannons and live ammunition, resulting in a large number of injuries and mass arrests. <br />  <br /> It&rsquo;s been impossible to determine the scale of the protests due to the severe repression of any media reporting anything other than the coup government line. Journalists on the streets face violent harassment and gunshots. Independent and pro-Zelaya media have essentially been shut down, with a number of raids on local TV and radio stations, foreign channels such as CNN (yes that CNN) and Venezuela&rsquo;s Telesur being blocked, as well as arrests of both local and international journalists. Pro-coup media, however, have operated unimpeded. <br />  <br /> In attempts to quell the escalating resistance the coup leadership has stepped up the repression. On Wednesday, Micheletti passed an emergency decree that ironically stripped Hondurans of a number of constitutional rights, including the right to protest, freedom of assembly, freedom of association, freedom of movement, freedom from unwarranted search, seizure and arrest and the rights of due process while under arrest. <br /> Under pressure from both the streets and the international community, the military is now also facing dissent from within its own ranks with at least two battalions heeding the protesters chants that &ldquo;<em>Soldiers, you are part of the people</em>&rdquo; and refusing to participate. <br />  <br /> Having postponed his original return, scheduled for Thursday (2nd), in order to wait for the expiration of an OAS ultimatum for the return of democracy, Zelaya now plans to return to Honduras on Saturday (4th). While Micheletti has promised to arrest him the &ldquo;moment he sets foot on Honduran soil&rdquo;, social movements and supporters are preparing to meet the President to provide an escort and protection. <br />  <br /> In the current political environment in Latin America, &lsquo;80s-style military coups are as out of fashion as shoulder pads and Wham, a fact that even the less than squeaky-clean US administration recognises. While Zelaya is far from a revolutionary man of the people, his relatively minor challenge to the ruling oligarchy has shown just how difficult it still is to effect genuine change. However, it seems that the Honduran powers that be might just have backed themselves into a corner that it will be difficult to bully their way out of.&nbsp; <br />  <br /> * For extensive coverage see <a href="http://www.narconews.com" target="_blank">www.narconews.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>coup</span>, <span style='color:#777777; font-size:10px'>honduras</span>, <a href="../keywordSearch/?keyword=latin+america&source=682">latin america</a>, <span style='color:#777777; font-size:10px'>manuel zelaya</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(384);
			
				if (SHOWMESSAGES)	{
				
						addMessage(384);
						echo getMessages(384);
						echo showAddMessage(384);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>