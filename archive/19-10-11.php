<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
require_once '../functions/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 781 - 29th July 2011 - And Finally</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

	<?php require "../inc/leftBarMain.php"; ?>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

<div style='text-align: right; width: 75%; margin-left: 25%; padding-bottom: 5px; font-weight: bold; border-bottom: 1px solid #bbbbbb' >
	<a href='news792.php'>Issue 792</a> - 
	<span style='font-size: 15px'>Special Update</span> - 
	19th October 2011</div>

<h3>Occupy Everywhere!</h3>

<p><b>AS CITIES AROUND THE UK JOIN THE GLOBAL RESISTANCE TO CORPORATE GREED</b>

<p>In the last few days approximately 1,000 cities across the world have joined in the #Occupy movement sparked off by Occupy Wall Street. As the Occupy Wall Street protests neared their second month camped up next to the Mamon of world finance, movements all around the world have copied the OWS model-  with "Occupy" protest camps springing up in city centres all over the world. United by common tactics and the common slogan, "We are the 99%", people are taking the fight against capitalism to the belly of the beast.

<p>Saturday the 15th  of October saw occupations take place all over the UK. The biggest and most headline-grabbing is, of course, "Occupy The Stock Exchange"- still going strong with several hundred people camped outside St Paul's Cathedral. But it's by no means the only one, and at the moment there are at least seven UK cities where the "Occupy" movement has successfully taken and held city centres.
 
<p>This is a movement of all of us! And it needs all of us to help if it's going to win. If you are willing and able to camp out- come along to your closest occupation. If you can't, they still need your help. They need tents, kitchen equipment, general protest tat (ie- rope and gaffa tape) food, and especially warm clothing, blankets and sleeping bags- it's getting colder and they don't plan to leave anytime soon.

<p>For more information on occupations follow the links below. For up to the minute updates, follow @SchNEWS on Twitter, where we're tweeting and re-tweeting info about the UK and world occupations.

<p>More Occupations are planned, and there may be others that we've missed, so below is not necessarily the complete list. Please <a href='mailto:mail@schnews.org.uk'>email</a>/tweet/phone us with updates.

<p>&nbsp;

<p><b>London- Occupy the Stock Exchange</b>
<br />Location: St Paul's Cathedral, EC4M 8AD
<br />Twitter: @OccupyLSX
<br /><a href='http://occupylondon.org.uk/' target='_BLANK'>http://occupylondon.org.uk/</a>

<p>&nbsp;

<p><b>Nottingham</b>
<br />Location: Old Market Square
<br />Twitter: @OccupyNotts
<br /><a href='mailto:occupynottingham@hotmail.co.uk'>occupynottingham@hotmail.co.uk</a>
<br /><a href='http://nottingham.indymedia.org.uk/articles/2076' target='_BLANK'>http://nottingham.indymedia.org.uk/articles/2076</a>
<br />Facebook: <a href='http://www.facebook.com/pages/Occupy-Nottingham/176572135759014?sk=wall' target='_BLANK'>http://www.facebook.com/pages/Occupy-Nottingham/176572135759014?sk=wall</a>

<p>&nbsp;

<p><b>Bristol</b>
<br />Location: College Green
<br />Twitter: @OccupyBristolUK
<br /><a href='http://www.bristol.indymedia.org.uk/article/706047' target='_BLANK'>http://www.bristol.indymedia.org.uk/article/706047</a>
<br />Facebook: <a href='https://www.facebook.com/pages/Occupy-Bristol/156092691150973?sk=info' target='_BLANK'>https://www.facebook.com/pages/Occupy-Bristol/156092691150973?sk=info</a>

<p>&nbsp;

<p><b>Manchester</b>
<br />Location: Peace Gardens, near St.peters square
<br />Twitter: @OccupyMCR
<br /><a href='http://www.occupymanchester.org/' target='_BLANK'>http://www.occupymanchester.org/</a>
<br />Facebook: <a href='http://www.facebook.com/OccupyMCR' target='_BLANK'>http://www.facebook.com/OccupyMCR</a>

<p>&nbsp;

<p><b>Norwich</b>
<br />Haymarket NR2 1QD
<br />Twitter: @OccupyNorwich
<br /><a href='http://occupynorwich.tumblr.com/' target='_BLANK'>http://occupynorwich.tumblr.com/</a>
<br />Facebook: <a href='http://www.facebook.com/pages/Occupy-Norwich/291233767570027' target='_BLANK'>http://www.facebook.com/pages/Occupy-Norwich/291233767570027</a>

<p>&nbsp;

<p><b>Newcastle</b>
<br />Twitter: @OccupyNewcastle
<br /><a href='http://occupynewcastle.org/' target='_BLANK'>http://occupynewcastle.org/</a>

<p>&nbsp;

<p><b>Edinburgh</b>
<br />Twitter: @OccupyEdinburgh
<br />Facebook: <a href='http://www.facebook.com/Occupyedinburgh' target='_BLANK'>http://www.facebook.com/Occupyedinburgh</a>

<p>&nbsp;

<p>Also check out <a href='www.occupybritain.co.uk' target='_BLANK'>www.occupybritain.co.uk</a> and <a href='http://www.occupytogether.org/' target='_BLANK'>http://www.occupytogether.org/</a> - two websites that are trying to connect the movements around Britain and around the world.



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


