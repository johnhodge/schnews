<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 684 - 17th July 2009 - Good Plan, 'stan</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, afghanistan, mujahideen, taliban, resource wars" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 684 Articles: </b>
<b>
<p><a href="../archive/news6841.php">Good Plan, 'stan</a></p>
</b>

<p><a href="../archive/news6842.php">Crap Search Of The Week</a></p>

<p><a href="../archive/news6843.php">Call Off The Hounds!</a></p>

<p><a href="../archive/news6844.php">Dow And Nowt</a></p>

<p><a href="../archive/news6845.php">Va-va-boom!</a></p>

<p><a href="../archive/news6846.php">Coup Stark</a></p>

<p><a href="../archive/news6847.php">Calais Bruise Crews</a></p>

<p><a href="../archive/news6848.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/684-trinny-susannah-lg.jpg" target="_blank">
													<img src="../images/684-trinny-susannah-sm.jpg" alt="What not to War" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 17th July 2009 | Issue 684</b></p>

<p><a href="news684.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GOOD PLAN, 'STAN</h3>

<p>
<strong> <br /> AS THE BODY COUNT MOUNTS, WE LOOK AT THE UNWINNABLE WAR IN AFGHANISTAN</strong> <br />  <br /> There seems to be a real sense of surprise that eight British soldiers were killed in 24 hours, during the &lsquo;fiercest fighting yet in Afghanistan&rsquo;. The Taliban have changed their strategies from massed wave attacks to the hit-and-run guerilla insurgency that stopped the US occupation dead in its tank-tracks in Iraq.&nbsp; <br />  <br /> Whilst the patriotic coverage of &ldquo;Our Boys&rdquo; is not surprising, the fact that the Taliban can fight back isn&rsquo;t either. After all they the product-of / heirs-to the Mujahideen that defeated the Soviets in the 80s. Afghanistan is called &lsquo;the graveyard of empires&rsquo; for good reason. The British were defeated there (just like in Iraq) in the heyday of the British Empire. In 1842, Afghans killed over 16,000 British soldiers and auxiliaries in a single battle. The Brits remained bogged down in Afghanistan for decades, eventually giving up in the 1920s. The Russians tried a few decades later. They quit after ten years with 14,000 dead on their side. <br />  <br /> Since then Afghanistan has been in a near-constant state of war. The Taliban years meant a kind of harsh peace for many, although their brand of knuckle-headed Islam only really held any real appeal to the 2 million shellshocked survivors in the refugee camps. <br />  <br /> It&rsquo;s hard to guess at the numbers of dead in the latest US-UK phase of their war. Human Rights Watch has managed to put together some figures; their minimum estimates (including only known, recorded deaths) suggest something like 1,000 every year - almost certainly a gross undercount. <br />  <br /> With such high stakes it&rsquo;s obvious that this war is very important to Western leaders and NATO generals. Considering that the invasion was basically an act of revenge in the first place, it&rsquo;s worth taking a moment to ponder why &lsquo;our boys&rsquo; are still being sent to die nearly eight years after 9-11. <br />  <br /> <strong>KABULSHIT</strong> <br />  <br /> But first we need to be clear about is what the war is not about. It&rsquo;s not about oil. Or pipelines. Or bases. It&rsquo;s not about the War on Terror&trade; (sorry, Overseas Contingency Operation). It&rsquo;s not about the war on drugs.&nbsp; <br />  <br /> Yes, there is a school of thought that says that the war in Afghanistan is part of a grand plan to control the world&rsquo;s energy resources - gas and oil especially. Some claim Afghanistan is perfectly placed to take gas from central Asia on to Europe, bypassing dodgy states like Russia and Iran. But these schemers&rsquo; plans involve routing gas through such dangerous and unpredictable territory as northern Pakistan, Kurdistan and Afghanistan - a bad idea from the start, hugely expensive and impossible to defend. <br />  <br /> A strongly worded communiqu&eacute; from any one of a dozen rebel groups in the area is enough to send energy prices soaring. And anyway, this is only one of at least three mega pipeline projects crisscrossing Central Asia.&nbsp; <br />  <br /> Russia, China, Iran, Turkmenistan, Pakistan and India have all been investing heavily in pipelines which don&rsquo;t need either America or Afghanistan. <br />  <br /> OK, Afghanistan does have one multi-billion dollar export - opium. But a side effect of  toppling the Taliban caliphate in 2001 was that opium cultivation and production skyrocketed. About the only nice thing the UN ever said about the Taliban was about their success in eradicating opium. But in the lawless post-invasion environment, Afghans now produce some 90% of the world&rsquo;s supply with the anti-drug Taliban showing their pragmatic side - needing cash to fund their insurgency, they encourage and then tax opium farming. A clever plan would be to buy opium directly from farmers (distributing processed opiates to hospitals maybe) but this has been vetoed by the Americans, who can&rsquo;t be seen to be soft on drugs.&nbsp; <br />  <br /> So if the Afghan war is about anything, it&rsquo;s about pride. The US (and us) can&rsquo;t be seen to be defeated by a ragtag group of peasants and refugees - because when you look at the globe, there are a lot of people that match that description and the last ten years have shown that it&rsquo;s exactly these types of forces that can defeat modern armies. &lsquo;Our&rsquo; forces can&rsquo;t win, and they can&rsquo;t retreat; the only solution chickenhawk&nbsp;politicians suggest is to throw more young lads into the line of fire and drop more explosives on more people, escalating the conflict. It&rsquo;s worth recalling the suggestion of a US general during the last years of the Vietnam War: &ldquo;Can&rsquo;t we just declare victory and leave?&rdquo;&nbsp; <br />  <br /> Much more than Israel&rsquo;s attacks on Palestinians, Afghanistan is as clear a case of complicity from the UK government as you could get. The attacks are ordered by our politicians with the active help of British corporate backers. Take EDO/ITT for example: while they don&rsquo;t like to admit they make cash supplying components to the Israelis, they are proud to admit they make entire bomb racks for British warplanes, used to murder farmers and shepherds in Afghanistan. EDO&rsquo;s bomb racks are specifically designed to be used with the Paveway bombs regularly used in &lsquo;<strong>close air support&rsquo; missions</strong>&rsquo; to &lsquo;<strong>facilitate reconstruction and the extension of government authority</strong>&rsquo; - i.e. bombing missions to you and me.&nbsp; <br />  <br /> And EDO must be rubbing their hands as the UK government makes galvanising claims that the Afghan war is &lsquo;showing signs of success&rsquo; - despite that fact that after eight years the Taliban now control something like 40% of the country. Even areas theoretically under Coalition/&lsquo;Afghan Government&rsquo; control often have to sway back to Taliban law at night, when the US/UK military are back at their bases. <br />  <br /> But it&rsquo;s been difficult to get people on to UK streets to protest the war in Afghanistan lately. Understandable as most folks are reluctant to be seen supporting the Taliban, undeniably amongst the most humourless bunch of bastards you could ever hope not to meet.&nbsp; <br />  <br /> Another reason may be that, much more than Iraq, there just isn&rsquo;t much media coverage from the other side of Afghanistan. Very few images of Afghan dead permeate the mass media filters. Apart from the odd Channel Four special, and Robert Fisk in the Independent, what we see is what we&rsquo;re spoonfed - squaddies in rough terrain doing a tough job, alternately doling out sweet to local kids and lobbing mortars at enemy positions. <br />  <br /> When the Trots are the most active force agitating against the war, we&rsquo;ve really taken our eye off the ball.&nbsp; <br />  <br /> <strong>Information About Afghanistan</strong> <br />  <br /> * <a href="http://www.tomdispatch.com" target="_blank">www.tomdispatch.com</a> (US alternative news source that&rsquo;s kept focussed throughout)  <br />  <br /> * <a href="http://www.atimes.com" target="_blank">www.atimes.com</a>  (Asia Times Online - their reporter, Syed Saleem Shahzad, seems to have really good sources within the Taliban) <br />  <br /> <strong>Information For Action</strong> <br />  <br /> * <a href="http://www.antimilitaristnetwork.org.uk" target="_blank">www.antimilitaristnetwork.org.uk</a>  <br />  <br /> * <a href="http://www.stopwar.org.uk" target="_blank">www.stopwar.org.uk</a> (reluctantly)  <br />  <br /> * <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />  <br />   <strong>EDO are exhibiting in an Unmanned Aerial Vehicle</strong> military conference on July 22nd-23rd. Anyone wanting to give them a warm welcome should get themselves down to the Celtic Manor Resort Hotel in Newport on the 22nd and 23rd of July. <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=afghanistan&source=684">afghanistan</a>, <span style='color:#777777; font-size:10px'>mujahideen</span>, <span style='color:#777777; font-size:10px'>resource wars</span>, <span style='color:#777777; font-size:10px'>taliban</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(403);
			
				if (SHOWMESSAGES)	{
				
						addMessage(403);
						echo getMessages(403);
						echo showAddMessage(403);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>