<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 701 - 27th November 2009 - Buy Buy Cruel World</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, consumerism, credit crunch" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 701 Articles: </b>
<b>
<p><a href="../archive/news7011.php">Buy Buy Cruel World</a></p>
</b>

<p><a href="../archive/news7012.php">You Can Bank On Itt</a></p>

<p><a href="../archive/news7013.php">Mainshill Eviction Alert</a></p>

<p><a href="../archive/news7014.php">Losing The Magic</a></p>

<p><a href="../archive/news7015.php">Gang Banging</a></p>

<p><a href="../archive/news7016.php">Uni'd A Miracle</a></p>

<p><a href="../archive/news7017.php">Mersey Beatings</a></p>

<p><a href="../archive/news7018.php">Schnews In Brief</a></p>

<p><a href="../archive/news7019.php">Nervous Wrex</a></p>

<p><a href="../archive/news70110.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/701-buy-nothing-day-lg.jpg" target="_blank">
													<img src="../images/701-buy-nothing-day-sm.jpg" alt="Buy Nothing Day Special Offer" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 27th November 2009 | Issue 701</b></p>

<p><a href="news701.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BUY BUY CRUEL WORLD</h3>

<p>
<strong>AS SchNEWS HOPES THE ANTI-CONSUMERIST MESSAGE WILL FINALLY GAIN PURCHASE</strong> <br />  <br /> With the government plumping for the &lsquo;bail now, pay later&rsquo; option to paper over the gaping cracks in the global financial system, consumer spending is once again creeping up. While this may be good news for Tesco and Poundland, a return to a rampaging consumer society ravaging the world&rsquo;s resources doesn&rsquo;t sound too good to us here in awkward squad corner. Which is why we&rsquo;re once again welcoming the return of the premier annual direct inaction event <strong>Buy Nothing Day</strong> on November 28th. <br />  <br /> Started by Adbusters in the early 90s, Buy Nothing Day has grown into a worldwide movement that encourages people to go cold turkey on our collective shopping addiction for 24 hours. As well as getting folks to take a break from racking up the debts to upgrade their lives and create self-worth through i-phones and i-shoes, Buy Nothing Day aims to spread the anti-consumerist message and get people to think about the effects of unrestrained consumption on the environment and the developing world. You&rsquo;d think after the credit crunch / debt meltdown &lsquo;n&rsquo; all that, efforts might be less in vain this year but you&rsquo;ll probably have to keep the pressure up for the other 364 days too. <br />  <br /> Maybe one day, after the next great collapse, SchNEWS will launch our own &lsquo;Buy Something Day&rsquo; when it&rsquo;ll be OK to take a break from toiling on yer community food garden and scavenging landfill sites for computer spares and actually purchase some well needed bit of kit you&rsquo;ve saved all year for... <br />    <br /> Meanwhile, you can participate in Buy Nothing Day by doing nothing more than sitting in a darkened squat reading anti-capitalist diatribes all day (or even in a cosy semi watching the X-Factor), and for those willing to spread the anti-consumerist message about a bit there are a host of events taking place all over the country. <br />   <strong> <br /> <font size="3">* Some of the Buy Nothing Day events happening around the country on Saturday November 28th include...</font></strong> <br />  <br /> <strong>Birmingham:</strong> The 8th annual Santa Parade will be trundling through Birmingham city centre from noon. For details email <a href="mailto:joe@birminghamfoe.org.uk">joe@birminghamfoe.org.uk</a> <br />  <br /> <strong>Brighton:</strong> Feeling the crunch when it comes to your Christmas list? Head down to The Cowley Club to join in free gift making workshops including rug weaving, candle dipping and a book exchange, starting off at 11am. <br />   <br /> <strong>Bristol:</strong> If you really can&rsquo;t kick the consumption habit there will be Free Shop in Bristol. Swing by to pick up yer gratis goodies or stop for a chat and a bite to eat, at the centre of Broadmead from 11am. <br />  <br /> <strong>Cardiff:</strong> Help clean up after capitalism with an organised litter pick along the Taff Trail and surrounding areas between 12.30pm and 3.30pm. Meet outside 89 Corporation Road, Grangetown, CF11 7AQ at 12.00 for a 12.30 start. See <a href="http://cardiffdigs.blogspot.com/2009/11/buy-nothing-day-litter-pick.html" target="_blank">http://cardiffdigs.blogspot.com/2009/11/buy-nothing-day-litter-pick.html</a> <br />  <br /> <strong>Falmouth, Cornwall:</strong> Kernow Action Now! a new Cornwall direct action group will be launching what will become a regular Free Shop from 1pm outside the old Woolworths in Falmouth. <br />  <br /> <strong>Leeds:</strong> Another Free Shop along with a craft workshop and lusty renditions of alternative Christmas carols. From 11am to 4pm at Briggate in Leeds city centre. <br />  <br /> <strong>Lincoln:</strong> Anti-capitalist crazies My Dad&rsquo;s Strip Club are out to do three events in three days. Following <em>Bite the Hand</em>, a comedic demo held yesterday, today sees <em>From Dirty Cash to Clean Green</em> as MDSC try and navigate a way out of the climate chaos through drawing and performance. 5.30-7.30pm at Healthy Hub caf&eacute;, Beaumont Fee. Saturday&rsquo;s finale is <em>Good Shit Doesn&rsquo;t Have to Cost the Earth</em> as MDSC will be out coercing unsuspecting shoppers into breaking their addiction to mass-produced cheap-shit. Meet for a report back at the Dog and Bone pub 1pm sharp. See <a href="http://www.mydadsstripclub.com" target="_blank">www.mydadsstripclub.com</a> <br />  <br /> <strong>London: </strong>Islington&rsquo;s Green Living Centre is hosting a craft workshop using waste materials including newspaper origami and making picture frames from old CD cases. 222 Upper Street. For more info email  <a href="mailto:sustainabilitycentres@islington.gov.uk">sustainabilitycentres@islington.gov.uk</a> <br />  <br /> <strong>Sheffield: </strong>A hoard of zombie shoppers will be lurching around Meadowhall shopping centre in search for sweet, tasty consumer brains. Bring friends, costumes, signs and blood, lots and lots of blood (fake please, people). Meet by the statue in the main concourse at 1pm. Search Facebook for &lsquo;zombie flashmob sheffield&rsquo; to get more details. <br />  <br /> <strong>South Derbyshire:</strong> We won&rsquo;t pretend to understand this one despite our up and running Twitter page and hundreds of devoted followers but apparently... conkertu.com will serve as the social media hub for sustainability, with an annual programme of eight lifestyle events to meet, tweet and stream. See twitter.com/conkertu twitter.com/zerocredit_uk twitter.com/philcampbell twitter.com/visitconkers <br />   <br /> * For more info see <a href="http://www.buynothingday.co.uk" target="_blank">www.buynothingday.co.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>consumerism</span>, <a href="../keywordSearch/?keyword=credit+crunch&source=701">credit crunch</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(584);
			
				if (SHOWMESSAGES)	{
				
						addMessage(584);
						echo getMessages(584);
						echo showAddMessage(584);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>