<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 732 - 23rd July 2010 - Action Aid - By George</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, supermarkets, sweatshops, workers struggles" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 732 Articles: </b>
<p><a href="../archive/news7321.php">Getting Away With Murder</a></p>

<p><a href="../archive/news7322.php">The Democratic Process</a></p>

<p><a href="../archive/news7323.php">Banking On The Camp</a></p>

<p><a href="../archive/news7324.php">Aisle Be Back</a></p>

<p><a href="../archive/news7325.php">Hurndall Killer Released</a></p>

<p><a href="../archive/news7326.php">Lewes Road Digs In</a></p>

<b>
<p><a href="../archive/news7327.php">Action Aid - By George</a></p>
</b>

<p><a href="../archive/news7328.php">Sabs: Rover And Out</a></p>

<p><a href="../archive/news7329.php">Inside Schnews</a></p>

<p><a href="../archive/news73210.php">Calais: Out To Lunch</a></p>

<p><a href="../archive/news73211.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd July 2010 | Issue 732</b></p>

<p><a href="news732.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ACTION AID - BY GEORGE</h3>

<p>
<p>
	Campaign group Action Aid has targeted Asda stores this week in an effort to highlight the Walmart-owned chain&rsquo;s appalling record of exploiting sweatshop labour in Asia. </p>
<p>
	Actions have taken place in major cities up and down the country; campaigners have been putting secret messages into the pockets of Asda clothing describing the corporation&rsquo;s treatment of their garment workers, as well as collecting 2p from Asda shoppers as they enter the store. This amount is calculated as how much Asda would need to raise its price on a &pound;4 t-shirt to guarantee their workers earn enough to feed, clothe and educate their families (assuming they gave that 2p to the workers of course). A small price when you consider the &pound;45 million profit each day that the Walmart group makes. </p>
<p>
	Anyone who finds a secret message in the garment they&rsquo;ve bought can send it to Action Aid to claim a campaign t-shirt and enter a prize draw for other ethical clothing items. They will also be invited to send a letter to Asda bosses demanding fair treatment for workers making Asda clothes. </p>
<p>
	The group have promised to continue with their campaign every month until Asda increases the wages of overseas workers. Action Aid claims that even Asda&rsquo;s own surveys demonstrate that their workers aren&rsquo;t paid enough to sustain themselves or their families. They have formed a coalition with Asia Floor Wage ( a group dedicated to improving pay conditions for garment workers across Asia) to persuade Asda to re-evaluate their pay structures. </p>
<p>
	At the moment, Asda pays its factory employees in Bangladesh an average of &pound;6.70 in a working week of 48 hours. Asia Floor Wage calculates the basic living wage needed in Bangladesh at &pound;25.90 a week. This could easily be achieved through tiny increases of final retail prices but it seems that profit-greedy capitalists at the top simply prefer their workforces to live in poverty. </p>
<p>
	While some high street retailers have promised to meet more &lsquo;ethical&rsquo; targets by 2015, Asda still lags far behind other retail chains - even Primark we wonder? - in both word and action. </p>
<p>
	* See <a href="http://www.actionaid.org.uk" target="_blank">www.actionaid.org.uk</a> to find out more about the campaign. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(876), 101); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(876);

				if (SHOWMESSAGES)	{

						addMessage(876);
						echo getMessages(876);
						echo showAddMessage(876);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


