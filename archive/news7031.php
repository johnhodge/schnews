<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 703 - 11th December 2009 - Once You Cop You Just Can't Stop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, direct action, copenhagen, denmark" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 703 Articles: </b>
<b>
<p><a href="../archive/news7031.php">Once You Cop You Just Can't Stop</a></p>
</b>

<p><a href="../archive/news7032.php">Not Waving But Frowning</a></p>

<p><a href="../archive/news7033.php">Inside Schnews</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/703-COP15-lg.jpg" target="_blank">
													<img src="../images/703-COP15-sm.jpg" alt="COP15 - With this lot in charge we're brickin' it!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th December 2009 | Issue 703</b></p>

<p><a href="news703.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ONCE YOU COP YOU JUST CAN'T STOP</h3>

<p>
<font size="3"><strong>SchNEWS' </strong><strong>GUIDE TO VISITING THE COP15 PROTESTS IN <st1:country-region w:st="on"><st1:place w:st="on">DENMARK</st1:place></st1:country-region>.</strong></font><strong>..</strong> <br />  <br /> Going to COP15 at the weekend? Wondering which pair of socks to bring? A space-hopper and a peace flag or a gas-mask and a bag of transit wheel nuts? Ask no longer as SchNEWS brings you our prepare-for-action guide to the week long climate protest in Denmark, a country which professes itself to be a 'key developer of climate solutions'. Let's hope they're right. Read on for yer essential what's-what: border checks, police tactics, and probably most importantly, what will the weather be like? <br /> See <a href="http://www.schnews.org,uk" target="_blank">www.schnews.org,uk</a> or click 'full article' <br /> <font size="2" style="font-size: 11pt"><b> <br /> WHAT'S BEEN GOING ON SO FAR? <br />  <br /> </b></font>The 50,000 strong 'wave' protest in Trafalgar Square on the 5th has continued into its first week as Climate Camp organisers pledge to remain there for the rest of the conference. For those that can't make it to the continent, the place to be is central London with your warm clothes, sleeping bag and tent. And all yer mates of course. <br /> Over the choppy North Sea, successful demos have already been held, although police have been carrying out raids on several of the key locations within the community set up to deal with the influx of global demonstrators. Convergence centres and sleeping spaces have been hit, but reports coming through are of firm, but not aggressive, police throwing their weight around (and we're all used to that) and seizing a few tools (because cordless power drills are obviously dangerous weapons of the revolution). No arrests have been made as yet, and police have been keeping their batons to themselves <br /> <font size="2" style="font-size: 11pt"><b> <br /> WHAT SHOULD I BRING?</b></font> <br />  <br /> Firstly, enough clothes to wrap up warm. Snow is forecast for the beginning of next week with temperatures dropping just below freezing at night, with so-called highs of 1 or 2&deg;c during the day. Thus a good sleeping bag and plenty of blankets are a must. Finally, a good mood and plenty of colourful clothes to brighten up the chilly Danish weather are the only other requirements! Kitchens and workshops are already fully equipped, but obviously if you have special dietary requirements beyond mere veganism, bring yer own tucker to make sure you can protest with a full belly. No one likes a hungry anarchist. <br /> <font size="2" style="font-size: 11pt"><b> <br /> SHOULD I BE SCARED OF THE BORDER CHECKS?</b></font> <br />  <br /> No. Until now the potential 48hr detention for people crossing the border has not been used by immigration officers, and all they've been doing is checking passports and luggage thoroughly. Advice is to NOT bring anything to give them an excuse to detain you, i.e. tools, stuff that can be directly linked to use in actions (like bike locks, surface-to-air missiles etc.), clothing specifically designed for covering the face, or anything sharp. Six Swedish students and a teacher were refused entry on the Sweden-Denmark border today (10th) 'cos one of the group had been previously arrested for a minor offence which had not yet gone to trial.  However, others passing into the country that way have encountered no problems since. <br /> <font size="2" style="font-size: 11pt"><b> <br /> OK, I'VE GOT THROUGH THE BORDER, I'VE ARRIVED IN COPENHAGEN, NOW WHAT?</b></font> <br />  <br /> Go to one of the many info points set up across the city to get your activists handbook and find out where to rest your weary head. Sleeping spaces have been set up with basic facilities (heating, toilets etc.) and there is a suggested donation to help cover the costs. In your handbook you should find all the info you need to know including numbers for legal advice, medic centres and how to get fully involved. <br /> <font size="2" style="font-size: 11pt"><b> <br /> SO WHAT HAPPENS IF I GET BUSTED? <br /> </b></font> <br /> Although mass arrests at protests are quite normal for Denmark, people are rarely charged with anything, the usual procedure is just to be detained. Foreigners can be held for up to 12 hours on suspicion, 72 hours if you were caught in the act. In both cases they must tell you what you are being detained for, and after the time is up they must put you in front of a judge and charge you, or let you go. The only thing that might cause you a bit more trouble is masking up, which is illegal. Maximum penalty for this is a fine. If arrested, you MUST tell the police your name, birthday, and address and that's it. You have the right to: <br />  <br /> - know your charge and time of arrest&nbsp; <br /> - remain silent <br /> - not sign anything or stand to your charge <br /> - make necessary phone calls (although police can make these on your behalf) <br /> - have access to medical care, medicine and legal advice <br /> <font size="2" style="font-size: 11pt"><b> <br /> ANYTHING ELSE?</b></font> <br />    <br /> Have a good time! Stay safe and sensible, and don't forget what this is all about. Fire-bombing delegates. No seriously, time for this planet is running out, and those idiots in power need a big urgent shove in the right direction... <br />  <br /> For more info about action timetables, locations of info points etc. see: <a href="../archive/news700.php" target="_blank">http://www.schnews.org.uk/archive/news700.php</a> <br />  <br /> Up to date info about what's going on in Copenhagen: <a href="http://indymedia.dk" target="_blank">http://indymedia.dk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=703">climate change</a>, <a href="../keywordSearch/?keyword=copenhagen&source=703">copenhagen</a>, <a href="../keywordSearch/?keyword=denmark&source=703">denmark</a>, <a href="../keywordSearch/?keyword=direct+action&source=703">direct action</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(605);
			
				if (SHOWMESSAGES)	{
				
						addMessage(605);
						echo getMessages(605);
						echo showAddMessage(605);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>