<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 716 - 2nd April 2010 - Eire We Go Again</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, oil, climate change, rossport, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 716 Articles: </b>
<b>
<p><a href="../archive/news7161.php">Eire We Go Again</a></p>
</b>

<p><a href="../archive/news7162.php">Down To The Wire</a></p>

<p><a href="../archive/news7163.php">Sand Storms</a></p>

<p><a href="../archive/news7164.php">Out To Launch</a></p>

<p><a href="../archive/news7165.php">No Expense Bared</a></p>

<p><a href="../archive/news7166.php">More Than Fair</a></p>

<p><a href="../archive/news7167.php">Hard Copy From Honduras</a></p>

<p><a href="../archive/news7168.php">Ukba Go Channel Hopping</a></p>

<p><a href="../archive/news7169.php">All's Well That Nz Well</a></p>

<p><a href="../archive/news71610.php">Story To Tel</a></p>

<p><a href="../archive/news71611.php">Un-irving</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/716-bp-lg.jpg" target="_blank">
													<img src="../images/716-bp-sm.jpg" alt="Beyond being beyond petroleum" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 2nd April 2010 | Issue 716</b></p>

<p><a href="news716.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EIRE WE GO AGAIN</h3>

<p>
<strong>THE LATEST FROM THE ANTI-SHELL PROTESTS IN ROSSPORT...</strong> <br />  <br /> The Irish state&rsquo;s determined persecution of protesters against Shell&rsquo;s experimental pipeline started to unravel last week as 25 of the 27 standing trial had their cases withdrawn or dismissed. <br />  <br /> They did however manage to stitch up campaigner Niall Harnett, who received three five-month sentences after being roughed up by Gardai in a court building. In a farcical turn of events, after failing in the prosecution of a Gardai for assault, they then prosecuted Niall. <br /> The prosecution managed to convince the judge that Niall assaulted three Gardai at once, including an inspector and a sergeant and that no less than seven Gardai managed to cram into the space between the doors with Niall to witness this extraordinary event. Niall stated in court, &ldquo;<em>I am not prepared to apologise for standing up to them, because they are bullies</em>.&rdquo; Niall has lodged an appeal, but if unsuccessful he will serve five months as the sentences run concurrently. <br />  <br /> This legal farce follows a year of sinister Rossport related incidents with elements worthy of the most gratuitously paranoid potboiler (see <a href='../archive/news673.htm'>SchNEWS 673</a>). First news arrived that one of the security guards who worked for Shell had been shot dead in Bolivia and that he was part of a terrorist group that aimed to start a civil war there. Within weeks it became clear that he was not alone, that in fact several security guards from the Shell compound had travelled to Bolivia with him where some were arrested and others are wanted for questioning. <br />   <br /> The environmental prize winner Willie Corduff was beaten by masked men while conducing a peaceful overnight sit in at the Shell compound. Then Pat O&rsquo;Donnel&rsquo;s boat was boarded and sunk by more masked men, an event the Gardai failed to investigate for weeks. Finally, as Shell&rsquo;s pipe lying ship the Solitaire arrived off the coast two Irish Navy gun boats were deployed alongside a Air Force spotter plane, 300 Gardai and 200 Shell private security. All to watch, surround and repress no more than one hundred Shell to Sea campaigners. Rumours abounded of unmarked military jeeps and the place was crawling with secret police men. A huge media smear campaign swung into action seeking to paint the Shell to Sea campaigners as crazed terrorists despite the facts showing such types were on the other side. <br />   <br /> Following the court victories over 100 supporters showed up outside the gates of Castlerea Prison to protest the continuing imprisonment of Shell to Sea colleague Pat O&rsquo;Donnell, serving seven months in prison on trumped-up chances following months of state-sponsored harassment (see <a href='../archive/news710.htm'>SchNEWS 710</a>). Pat&rsquo;s ordeal has continued in prison. He has been refused a transfer to Loughan House, where a more open regime applies and visiting Pat at Castlerea Prison has been difficult for his family. <br />  <br /> Despite the scale of official repression of the campaign, it has achieved major victories with the experimental pipeline delayed by 10 years from 2003 to 2013. In what has become known as the Great Oil &amp; Gas Giveaway, the terms of energy exploration licenses have been changed so that energy companies now pay no royalties and very little tax. <br />  <br /> Millions have been spent on a huge PR offensive to win hearts and minds of key opinion makers. Paid hacks have poured out slanders about those who continue to resist Shell, while the few journalists who have dared question such stories have been targeted for abuse. <br />   <br /> * See <a href="http://www.shelltosea.com" target="_blank">www.shelltosea.com</a> and <a href="http://www.indymedia.ie/article/96167" target="_blank">www.indymedia.ie/article/96167</a> <br /> </span>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=716">climate change</a>, <a href="../keywordSearch/?keyword=direct+action&source=716">direct action</a>, <a href="../keywordSearch/?keyword=oil&source=716">oil</a>, <a href="../keywordSearch/?keyword=rossport&source=716">rossport</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(716);
			
				if (SHOWMESSAGES)	{
				
						addMessage(716);
						echo getMessages(716);
						echo showAddMessage(716);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


