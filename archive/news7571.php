<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 757 - 4th February 2011 - Cut to the Quick...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, students, benefits, cuts, students, austerity, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 757 Articles: </b>
<b>
<p><a href="../archive/news7571.php">Cut To The Quick...</a></p>
</b>

<p><a href="../archive/news7572.php">High Street Tax Maniacs</a></p>

<p><a href="../archive/news7573.php">Mubarak's Attacks Causing Chaos In Cairo</a></p>

<p><a href="../archive/news7574.php">Don't Mention The Wall</a></p>

<p><a href="../archive/news7575.php">Positive Schnews</a></p>

<p><a href="../archive/news7576.php">Behind The Irony Curtain</a></p>

<p><a href="../archive/news7577.php">On The Hoof</a></p>

<p><a href="../archive/news7578.php">Grow To Like It</a></p>

<p><a href="../archive/news7579.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/757-green-lg.jpg" target="_blank">
													<img src="../images/757-green-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 4th February 2011 | Issue 757</b></p>

<p><a href="news757.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CUT TO THE QUICK...</h3>

<p>
<p>
	<strong>AS THOUSANDS TAKE TO THE STREETS OF LONDON AND MANCHESTER</strong> </p>
<p>
	<strong>Thousands of protesters ran the police ragged in a hyperactive day of protest at the latest national demonstration against fees and cuts in London on Saturday (29th).</strong> </p>
<p>
	Up to 10,000 people marched through the city towards Parliament Square, taking the route agreed with police. However, the crowd showed little interest in hanging around for speeches at the designated end point and most pushed on towards Milbank Towers &ndash; scene of the birth of the student protest movement in November (see <a href='../archive/news747.htm'>SchNEWS 747</a>). They were met by a boarded up Tory HQ and a thick line of police. For a short while tension bubbled as teenagers swarmed up the front steps and the crowd jostled the police lines while delivering a rousing chorus of &ldquo;Tory scum&rdquo;. </p>
<p>
	There was to be no repeat of November&rsquo;s ruckus though, and, as the crowd began to get jittery about the possibility of a kettle, shouts went up to head to the Egyptian embassy to show solidarity with protesters calling for the end of Mubarak&rsquo;s regime (see <a href='../archive/news756.htm'>SchNEWS 756</a>). </p>
<p>
	The march then split into several large groups, most beginning the long (long) trek to the embassy. At the embassy a large, boisterous crowd gathered to hurl abuse at the autocratic Arab dictator, dance to a sound system and warm themselves by fires of swappie placards. Again police threatened to kettle, but the crowd stayed mobile and several splinter groups, each with hundreds of protesters, moved off in different directions. </p>
<p>
	Police attempts to follow the crowd were foiled by the blocking of vans and rapid dispersals and police were eventually reduced to wearily trailing behind as the crowds seized control of the roads. One group sprung a kettle on cop-cycles that were trying to follow them, surrounding three motorbikes and hurling smokebombs into the mix for good measure. </p>
<p>
	While one group returned to Parliament Square for a quick sit in, a number of others, several hundred strong, took to roving around the consumer&rsquo;s paradise around Oxford Street. With energy levels kept high by sound systems and the Sounds of Resistance Samba band, the packs of protesters targeted tax-dodgers Vodafone, Topshop, Boots and Dorothy Perkins. Time and again stores around the city were forced to pull down the shutters and plead for police protection as the crowds descended, bawling &ldquo;Pay your tax&rdquo;. Rapid police deployments prevented things getting too wild and the crowds quickly moved on, ever fearful of kettles. </p>
<p>
	With the huffing coppers reduced to trying keep pace with the antsy youth, protesters took control of the streets, bringing traffic grinding to a halt with their relentless marching and flying road blocks. All throughout the evening the cry of &ldquo;Whose streets? Our streets!&rdquo; rang loud and true. </p>
<p>
	<strong>AARON-EOUS</strong> </p>
<p>
	A simultaneous union/student organised demo in Manchester saw the final nail driven into the coffin of simpering politician wannabe, NUS President Aaron Porter. </p>
<p>
	After being collared by a group of protesters wanting answers for his utterly disgraceful lack of support for the student movement, Porter tried to make an escape. However, he was clocked by hundreds of fuming protesters who began yelling &ldquo;Students, workers here us shout, Aaron Porter sold us out&rdquo; and &ldquo;Aaron Porter, we know you, you&rsquo;re a fucking Tory too&rdquo; (the Daily Mail and the Torygraph reported this as anti-Semitic chants of &ldquo;you&rsquo;re a Tory Jew&rdquo;. Reports from those in the crowd and near Porter all fervently deny this and the reports seem to be entirely based on the almost certainly dickey hearing of one photographer.) </p>
<p>
	Witnesses reported that a &ldquo;sobbing&rdquo; Porter was then escorted away by police and private security. NUS vice-president Shane Chowan took Porter&rsquo;s place as a pre-march speaker and was promptly heckled throughout his speech before being pelted by assorted grocery items. </p>
<p>
	Around 4,000 people attended the Manchester protest. A group of about 500 broke away from the main march, forced their way through police lines and headed for the city centre. As the protesters marched through the city streets there were clashes with police, who tried to restore order through the time-tested methods of batons, horses and kettles. Eventually the breakaway groups were confined in kettles, from which they were released one by one after coppers forced them to give their details. There were 20 arrests. </p>
<p>
	While neither protest could match last year&rsquo;s action for numbers or all out action, they served a timely reminder that the movement ignited by the Tory/Lib attack on education is alive and kicking. With thousands of young folk now seasoned in protest tactics and with a healthy distrust of politicians and the police, the fight ain&rsquo;t over yet. <a href="http://</p>
<p>
	*www.anticuts.com" target="_blank"></p>
<p>
	*www.anticuts.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1087), 126); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1087);

				if (SHOWMESSAGES)	{

						addMessage(1087);
						echo getMessages(1087);
						echo showAddMessage(1087);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


