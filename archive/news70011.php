<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 700 - 20th November 2009 - Out Of Their League</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, english defence league, sdl, scotland, nottingham, far-right, anti-fascists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 700 Articles: </b>
<p><a href="../archive/news7001.php">Good Cop Bad Cop</a></p>

<p><a href="../archive/news7002.php">Tamil Torture</a></p>

<p><a href="../archive/news7003.php">Not A Leg To Stand On</a></p>

<p><a href="../archive/news7004.php">Walls Of Steel</a></p>

<p><a href="../archive/news7005.php">Nae To Nato</a></p>

<p><a href="../archive/news7006.php">Blackboard Rumble</a></p>

<p><a href="../archive/news7007.php">Crasbo Selecta</a></p>

<p><a href="../archive/news7008.php">Schnews In Brief</a></p>

<p><a href="../archive/news7009.php">Truck Or Treat</a></p>

<p><a href="../archive/news70010.php">Taking The Cissbury</a></p>

<b>
<p><a href="../archive/news70011.php">Out Of Their League</a></p>
</b>

<p><a href="../archive/news70012.php">French Letter</a></p>

<p><a href="../archive/news70013.php">Atomic Acquitten</a></p>

<p><a href="../archive/news70014.php">R . I . P Ivan Khutorskoy</a></p>

<p><a href="../archive/news70015.php">...and Finally... Milestone Or Millstone?</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th November 2009 | Issue 700</b></p>

<p><a href="news700.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>OUT OF THEIR LEAGUE</h3>

<p>
After their disastrous attempt at rallying Welsh racists (See <a href='../archive/news696.htm'>SchNEWS 696</a>, <a href='../archive/news697.htm'>697</a>), the EDL&nbsp; scrambled over Hadrian&rsquo;s Wall last Saturday (14th) to try their hand at instigating racial hatred in Scotland. The Scottish Defence League assembled in a Glasgow pub and were quickly surrounded by police. Hundreds of Antifascists gathered outside waiting for police to release the racists on an unsuspecting Scottish public.  <br />  <br /> However, before that could happen, the UAF contingent of the crowd broke away and marched off to a nearby Glasgow Green to have a natter about how important it is keep fascists off the streets. With only around 50 people left to actually try and keep fascists off the streets, the afternoon lock-in came to an end and the SDL and were let out. Tightly kettled by the police, they marched about 200 metres up the road then stood around singing popular Scottish nationalist ditties Rule Britannia and God Save the Queen.  <br />  <br /> After 20 minutes, the SDL marched back down the road and were put onto buses taking them away. After they had departed a demonstration of over a thousand people returned from Glasgow Green, ready to confront the spectre of fascism only to have completely missed the fash themselves. <br />  <br /> Later some right wing nutters were arrested after doing Nazi salutes near the train station, and an Asian guy was also arrested at the scene for breach of the peace. <br />  <br /> <font face="courier new">* The EDL are set to rear their ugly heads in Nottingham on December 5th. On the same day soldiers from the Second Battalion Mercian regiment will be marching through town to receive a salute in Nottingham&rsquo;s central Market Square. There are concerns the EDL will try and hijack the event to garner support. While the EDL have yet to announce their meeting point, Notts Stop the BNP will be gathering outside the Royal Centre at 10am, while UAF have stated that they will be holding a rally at 10am in the Market Square. Autonomous antifascists are being urged to act independently and stay mobile to try and avoid being kettled in and reducing the antifascist presence to confined chanting.</font>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-fascists&source=700">anti-fascists</a>, <a href="../keywordSearch/?keyword=english+defence+league&source=700">english defence league</a>, <a href="../keywordSearch/?keyword=far-right&source=700">far-right</a>, <a href="../keywordSearch/?keyword=nottingham&source=700">nottingham</a>, <a href="../keywordSearch/?keyword=scotland&source=700">scotland</a>, <span style='color:#777777; font-size:10px'>sdl</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(579);
			
				if (SHOWMESSAGES)	{
				
						addMessage(579);
						echo getMessages(579);
						echo showAddMessage(579);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>