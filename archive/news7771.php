<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 777 - 1st July 2011 - Libya: Anti-Nato Classes</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, libya, middle east, nato" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 777 Articles: </b>
<b>
<p><a href="../archive/news7771.php">Libya: Anti-nato Classes</a></p>
</b>

<p><a href="../archive/news7772.php">Africa House Evicted</a></p>

<p><a href="../archive/news7773.php">Simon Levin Rip</a></p>

<p><a href="../archive/news7774.php">A Ship Off The Ol'bloc</a></p>

<p><a href="../archive/news7775.php">Campaign For Real Bail</a></p>

<p><a href="../archive/news7776.php">Greece: The Golden Fleecing</a></p>

<p><a href="../archive/news7777.php">Peru - What A Scorcher</a></p>

<p><a href="../archive/news7778.php">Turn On The Waterways</a></p>

<p><a href="../archive/news7779.php">Sheikh-down</a></p>

<p><a href="../archive/news77710.php">Holding The Fortnum</a></p>

<p><a href="../archive/news77711.php">[headline On Strike]</a></p>

<p><a href="../archive/news77712.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/777-Libya-lg.jpg" target="_blank">
													<img src="../images/777-Libya-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 1st July 2011 | Issue 777</b></p>

<p><a href="news777.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LIBYA: ANTI-NATO CLASSES</h3>

<p>
<p>  
  	<strong>AS SCHNEWS ANALYSES THE NEW GREAT GAME SHAPING THE LIBYA CONFLICT</strong>  </p>  
   <p>  
  	<strong>&ldquo;<em>The Euro-American attack on Libya has nothing to do with protecting anyone; only the terminally naive believe such nonsense. It is the West&rsquo;s response to popular uprisings in strategic, resource-rich regions of the world and the beginning of a war of attrition against the new imperial rival, China.</em>&rdquo; - John Pilger - award-winning journalist and lefty fire-brand</strong>  </p>  
   <p>  
  	The bombs are still falling on Tripoli in yet another British intervention on behalf of Arab human rights in the Middle East (the 46th since 1945). What started out as a U.N backed campaign&nbsp; supposedly to protect anti-Gaddafi elements from a massacre has turned into reckless use of air-power to oust Gadaffi from power. And yet the UK peace movement is virtually nowhere to be seen. It was only in May that UK troops were supposed to be withdrawing from Iraq and the end of the battle in Afghanistan has just been signalled - yet here we are, at it again.  </p>  
   <p>  
  	The same justifications for war as ever have been rolled out - accusations of war crimes against Gaddafi&rsquo;s forces - including allegations of systematic rape which have been made by senior US politicians and publicised throughout the western media. However while both Amnesty and Human Rights Watch have found evidence of multiple war crimes by both sides, neither organisation has found any evidence for the claims. When the dust settles will these allegations end up in the dustbin of history, next to the stories of Iraqi troops pulling babies out of incubators in Kuwait in the 1991 Gulf War? Those stories, pushed by senior U.S politicians to justify military action turned out to have been crafted by P.R agency Hill Knowlton. And then of course we have the lies used to drag us into the disastrous Iraq conflict - and yet nothing seems to have been learned. The very same journals that were determinedly &lsquo;anti-war&rsquo; in 2003 are now swallowing the propaganda whole and pondering &lsquo;the sensitive politics of humanitarian intervention&rsquo;.  </p>  
   <p>  
  	Does SchNEWS really need to hammer home the real nature of this conflict?....It&rsquo;s the oil, dummies. Oil reserves in Libya are the largest in Africa and the ninth largest in the world with 41.5 billion barrels&nbsp; as of 2007.&nbsp; Libya is considered a highly attractive oil area due to its low cost of oil production (as low as $1 per barrel at some fields), and proximity to European markets. Also due to the country&rsquo;s erratic poltical course large chunks of the country remain unexplored. As an independent actor Gaddafi could be courted, hence Tony Blairs rather smarmy visit in 2004. However as ever with dictators he was showing worrying signs of waywardness - Prior to the uprising China was the third-largest buyer of Libyan crude behind Italy and France, a month into the uprising and Gadaffi was promising to re-negotiate all contracts away from western corporations.  </p>  
   <p>  
  	Nato is now into&nbsp; the third month of its bombing campaign - which includes aerial attacks on cities and&nbsp; the use of unmanned drones. Effectively the coalition has placed itself as the air arm of&nbsp; the &lsquo;rebels&rsquo;. Interestingly, this time, although the U.S is still heavily involved, it is taking a backseat to the British and French. It was S.A.S troops who were caught on the ground back in May - and only this week it was confirmed that the French have been dropping arms to the rebel forces. It&rsquo;s becoming clear that the mass popular uprising has become a proxy war. That isn&rsquo;t to say that some of those who are fighting aren&rsquo;t doing so to overthrow Gadaffi for legitimate reasons. But the rebels are loosely organised and the factions most likely to receive western military aid - and therefore most likely to pre-dominate - are those most likely to be amenable to a pro-Western sentiment.  </p>  
   <p>  
  	The man in charge of the military wing of the &ldquo;pro-democracy rebels&rdquo; is Colonel Khalifa Haftar who, according to a study by the US Jamestown Foundation, set up the Libyan National Army in 1988 &ldquo;with strong backing from the Central Intelligence Agency&rdquo;. Funnily enough Colonel Haftar&rsquo;s home from home for the last twenty years has been Langley in Virginia - base of the CIA. Other senior figures in the movement were until recently part of Gadaffi&rsquo;s regime - so a victory for the rebel forces would hardly be the clean slate one might hope for after the overthrow of a dictator. There are already indications that the rebels might not be quite the lilywhite freedom fighters portrayed in the mainstream press, with evidence of atrocities carried out against sub-Saharan (i.e black) Africans in the rebel-held areas.  </p>  
   <p>  
  	<strong>A BAD TRIPOLI</strong>  </p>  
   <p>  
  	Plans have already been drawn up for the division of power in post-Gaddafi Iraq. According to the Guardian, the proposals have been drafted by the Department for International Development, all in the best possible interests of the Libyan people naturally. The 50 page document, which is hoped to be ratified at the forthcoming meeting of the &lsquo;Libyan contact group&rsquo; in Istanbul in mid-July, only deals with the possibility of regime change in Libya, effectively boxing the dictator into a corner, to fight or die.  </p>  
   <p>  
  	Largely unreported have been the Gadaffi regime&rsquo;s overtures for a negotiated ceasefire. At the end of May, Libyan prime minister Al-Baghdadi al-Mahmou-di wrote to Britain, France and other governments proposing an immediate ceasefire monitored by the United Nations and the African Union, unconditional talks with rebel forces, an amnesty for all fighters, compensation for victims of the civil war and the formation of a new constitution for a &ldquo;radically different&rdquo; Libya. This offer was rejected out of hand by the NATO powers.  </p>  
   <p>  
  	Increasingly reports of civilian deaths in Tripoli are filtering through. The familiar language of targeted attacks against military tactics, used in every &lsquo;intervention&rsquo; is surfacing again, casting a smokescreen over the indiscriminate nature of aerial bombardment. This war carries strong echoes of the NATO&rsquo;s &lsquo;humanitarian intervention&rsquo; in Serbia in 1999. The reasons given at the time -&nbsp; prevention of ethnic cleansing by Serbian forces - were a stage managed exaggeration: The ethnic cleansing in fact massively intensified after the bombing. Civilian targets were hit repeatedly in Belgrade. The much feted Kosovan Liberation Army turned out to be a mafia organisation that indulged in its own bouts of ethnic cleansing once in power.  </p>  
   <p>  
  	The Arab Spring, a genuinely radical moment in history initially full of optimism, is moving into a dangerous new phase as the Western elites and regional powers seek to manipulate the outcome. In Egypt the state remains intact despite the change of regime and the bringing of criminal charges against Mubarak, with clashes between protesters and riot police taking place yet again in the iconic Tahrir square. Meanwhile in Bahrain, home of the U.S 5th fleet and satellite kingdom of Saudi Arabia&nbsp; - the repression enthusiastically backed by the West is claiming civilian victims. The Middle East is caught in the balance of the new cold war - China vs the U.S.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1263), 146); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1263);

				if (SHOWMESSAGES)	{

						addMessage(1263);
						echo getMessages(1263);
						echo showAddMessage(1263);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


