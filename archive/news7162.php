<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 716 - 2nd April 2010 - Down To The Wire</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, internet, neo-liberalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 716 Articles: </b>
<p><a href="../archive/news7161.php">Eire We Go Again</a></p>

<b>
<p><a href="../archive/news7162.php">Down To The Wire</a></p>
</b>

<p><a href="../archive/news7163.php">Sand Storms</a></p>

<p><a href="../archive/news7164.php">Out To Launch</a></p>

<p><a href="../archive/news7165.php">No Expense Bared</a></p>

<p><a href="../archive/news7166.php">More Than Fair</a></p>

<p><a href="../archive/news7167.php">Hard Copy From Honduras</a></p>

<p><a href="../archive/news7168.php">Ukba Go Channel Hopping</a></p>

<p><a href="../archive/news7169.php">All's Well That Nz Well</a></p>

<p><a href="../archive/news71610.php">Story To Tel</a></p>

<p><a href="../archive/news71611.php">Un-irving</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd April 2010 | Issue 716</b></p>

<p><a href="news716.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DOWN TO THE WIRE</h3>

<p>
<p style="margin-bottom: 0cm"><strong>AS ANTI-DOWNLOADING LEGISLATION REACHES BRITAIN  <br />  <br /> <em>'...Then they came for the illegal downloaders, but I wasn't an illegal downloader so I did nothing...' Oh hang on &ndash; yes I am.</em></strong> <br />  <br /> Next week, April 6th, the British parliament votes on the Digital Economy Bill. Don't expect much news coverage of this because on this same day Brown will formally dissolve parliament before the election, and due to arcane rules this bill can be nodded through in the 'wash up' on this day without having to go to a debate. It just may be a swansong for the people that brought you the Terrorism Act and a record amount of other draconian legislation during 13 lucky years of Neo-Labour. <br />  <br /> This Bill features a section which targets illegal downloading, pandering to big media corporations who are moaning about losing revenue because of it. Internet service providers (ISPs) will be legally obliged to divulge the details of copyright infringers and ordered to block access to websites which allow infringements &ndash; eg &ndash; bit-torrent sites or online file repositories like Rapidshare. ISPs also face large fines for failing to act against P2P downloaders. Users who have received multiple copyright infringement letters can be temporarily suspended. Public Wi-Fi outlets may fall foul if somebody illegally downloads using their network, and this free service may become less common. <br />  <br /> This part of the DE Bill is being supported by UK Music - led by has-been musician Feargal Sharkey - who supposedly represents the interests of the entire music industry. But in reality, when it comes to music, it's media corporations rather than artists who have most vested interests in the crack-down on illegal downloading. Especially the majority of smaller selling, less well known artists.  <br />  <br /> Free Software leader Richard Stallman claims that due to the way the record industry operates, smaller artists don't get paid adequately, and he advocates other ways to pay them &ndash; including allowing people to directly pay the artist a donation, having downloaded the music for free. The corporate-controlled record industry is well known to be on the slide, and many musicians are now bypassing the mainstream system and distributing their works on websites, often for free, and asking for donations. If you were to directly pay an independent artist &pound;1 for a track, it'd be more than they would get if you bought the single. <br />  <br /> But while media corporations support the Bill, other internet corporations are not happy about it. These include those who make their money from the internet itself, including Yahoo, Google, Facebook and others. And ISP companies like BT and Talk Talk don't like it, because the onus is on them to do the policing, and anyway they make money from internet traffic no matter what it is. One survey shows that 96% of 18 to 24-year-olds have illegally downloaded music on their music player, and a hefty proportion of web traffic is downloads. <br />  <br /> There has been a campaign against the Bill, led by the Open Rights Group, who have organised several demos and a petition of over 12,000 demanding a real debate. Last Wednesday (24th March), 300 demonstrated outside parliament, then yesterday, April 1st, ORG held another 'flashmob' demo of 35 people where they delivered 'disconnection' notices to the three major party headquarters, followed by a demo outside the London offices of UK Music.  <br />  <br /> ORG told SchNEWS today that it's not a fore-gone conclusion that the Bill will be passed, and urged a continued campaign to lobby MPs to not vote for it. (See <a href="http://www.openrightsgroup.org" target="_blank">www.openrightsgroup.org</a>) Another group, 38 Degrees, have been raising cash they will use for full-page newspaper ads to be run on April 6th (See <a href="http://38degrees.org.uk" target="_blank">http://38degrees.org.uk</a>). <br />  <br /> But whether or not the tried n' tested 'n' often failed methods of mainstream protest work, don't go thinking that the golden age of downloading is over just yet. Last year a similar law was introduced in France, known as Hadopi, with a similar raft of legislation. And guess what the result is &ndash; illegal file sharing has gone up anyway. Though they have yet to disconnect peer-to-peer (P2P) users as they now legally can, a recent report suggests that P2P is down but most of these users have gone on to illegal media streaming sites and getting stuff from download sites like Rapidshare (both of which aren't illegal in Hadopi laws). This example shows that legislation will always be playing catch-up to internet use, and file sharing will no doubt remain a moving target for law makers. (See <a href="http://arstechnica.com/tech-policy/news/2010/03/piracy-up-in-france-after-tough-three-strikes-law-passed.ars" target="_blank">http://arstechnica.com/tech-policy/news/2010/03/piracy-up-in-france-after-tough-three-strikes-law-passed.ars</a>)  <br />  <br /> New Zealand also introduced something similar in early 2009 with Section 92A of the Copyright (New Technologies) Amendment Act &ndash; but this was section was repealed after lobbying from ISPs and an  international internet campaign, New Zealand Internet Blackout (See <a href="http://creativefreedom.org.nz/blackout.html" target="_blank">http://creativefreedom.org.nz/blackout.html</a>).  <br />  <br /> <strong>HARD ACT TO FOLLOW</strong> <br />  <br /> It could be that countries like France, NZ, Japan and Britain introducing such laws are just rehearsals for a bigger, international trade law, the Anti-Counterfeiting Trade Agreement (ACTA). While it is still some distance from becoming finalised, meetings began in 2007 led by the US, the European Commission and Japan, with other countries joining in later.  The only information we have about the private talks are documents which have been leaked.  <br />  <br /> It builds on the TRIPS agreement (See <a href='../archive/news420.htm'>SchNEWS 420</a>) which is about regulating and enforcing copyright, patents, trademarks and other so-called 'intellectual property'. The pretext is to cut down on the global trade of 'counterfeit' goods, including unlicensed generic medicines and illegal file sharing. Like TRIPS and other US / European neo-liberal efforts to control the world, the idea is to roll ACTA out to countries like Russia, China and Brazil, dragging them into line with US-based copyright laws. Representatives on the advisory committees reads like a who's who of media, pharmaceutical and biotechnology corporations - is if there was any doubt about whose interest ACTA serves. <br />  <br /> A major emphasis with ACTA is an clampdown on copyright-enfringing internet activity akin to the DE Bill, including forcing ISPs to provide information about suspected illegal downloaders without a warrant and making it easier for the record industry to shut down bit-torrent sites such as Pirate Bay. Another part of ACTA could see searches of laptops, music players and phones at international borders for illegal media files, leading to fines and possibly devices being confiscated. The US lobby in ACTA hopes to see new rules which will make DRM*-breaking software &ndash; and DRM-cracked media files &ndash; illegal, causing a great increase in DRM-usage by media and computer corporations. * (DRM: Digital Restrictions Management - measures to prevent copying or control use of files, disks and devices &ndash; see <a href="http://www.defectivebydesign.org" target="_blank">www.defectivebydesign.org</a>)  <br />  <br /> As well as this, ACTA also targets 'counterfeit' medicine - in other words giving drug monopolies more power over companies who make unlicensed generic drugs, which will affect health standards in many countries. <br />  <br /> Round 8 of the ACTA talks will be this month, April 12th-16th in Wellington New Zealand. A counter-conference will be held on April 10th (See <a href="http://publicacta.org.nz" target="_blank">http://publicacta.org.nz</a>).  <br />  <br /> * See also <a href="http://www.eff.org/issues/acta" target="_blank">www.eff.org/issues/acta</a> <a href="http://www.laquadrature.net/en" target="_blank">www.laquadrature.net/en</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>internet</span>, <a href="../keywordSearch/?keyword=neo-liberalism&source=716">neo-liberalism</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(717);
			
				if (SHOWMESSAGES)	{
				
						addMessage(717);
						echo getMessages(717);
						echo showAddMessage(717);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


