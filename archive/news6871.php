<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 687 - 14th August 2009 - Fash Get The Brum Rush</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, birminghan, english defence league, unite against fascism, casuals united, fascism, anti-fascists, bnp, red white and blue festival" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 687 Articles: </b>
<b>
<p><a href="../archive/news6871.php">Fash Get The Brum Rush</a></p>
</b>

<p><a href="../archive/news6872.php">From Russia With Hate</a></p>

<p><a href="../archive/news6873.php">Notes From A Small Island  </a></p>

<p><a href="../archive/news6874.php">Celt In Action</a></p>

<p><a href="../archive/news6875.php">Ciggy Stubbed Out?</a></p>

<p><a href="../archive/news6876.php">Bad Korea Choice</a></p>

<p><a href="../archive/news6877.php">Greek Tragedy  </a></p>

<p><a href="../archive/news6878.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/687-swastika-dustbin-lg.png" target="_blank">
													<img src="../images/687-swastika-dustbin-sm.png" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 14th August 2009 | Issue 687</b></p>

<p><a href="news687.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FASH GET THE BRUM RUSH</h3>

<p>
<strong>AS ANTI-FASCISTS CLASH WITH THE FAR RIGHT IN BIRMINGHAM CITY CENTRE</strong> <br />  <br /> Last Saturday, business-as-usual shopping at the Bull Ring in central Birmingham was brought to a halt as sporadic street fighting broke out between a burgeoning new section of the far right in Britain &ndash; the English Defence League (EDL) &ndash; there to demonstrate against &lsquo;Islamic fundamentalism&rsquo; (read: any Muslims), and anti-fascists alongside the Unite Against Fascism (UAF) umbrella group. The mainstream media obligingly printed pictures of white lads being beaten up by Asians, fuelling the fires of racial hatred. Around 400 anti-fascists &ndash; mostly local youths - did a good job of kicking the arses of some 150 EDL bandwagoners. In total 35 were arrested and three injured. <br />  <br /> The English Defence League, and Casuals United, are supposedly non-BNP, non-NF multi-racial groups united against Islamic fundamentalism and Sharia Law in Britain. But a handful aside, all the faces on their side of the demo were white. Casuals United are the product of a recruitment drive on the football terraces &ndash; a common practise in eastern Europe, with footy supporters seen to be patriotic and with a gang mentality. Despite maintaining the charade that they aren&rsquo;t linked to the BNP, the EDL were started by a BNP member, are fronted by known Combat 18 members, and you couldn&rsquo;t slide a rizla through the gap between the BNP and EDL agendas, apart from the EDL trying to draw British Afro-Caribbeans into the Islamophobic mix. They say the UAF is a government backed campaign to target them &ndash; admittedly it has support from Labour MPs as well as SWP involvement - but the fact is that regardless of the UAF, anti-fascists have actual broad public support, and the EDL/BNP don&rsquo;t. <br />  <br /> The rise of the EDL this year has followed demonstrations and scuffles at several Military parades in Luton and other towns between far right nationalists versus Asian and white anti-fascists (See <a href='../archive/news670.htm'>SchNEWS 670</a>, <a href='../archive/news671.htm'>671</a>). In Luton the National Front demonstrated on April 13th, and on May 24th the EDL demonstrated there &ndash; both times mosques and other properties were attacked in Muslim-heavy areas of town. Far-right groups are planning to return in larger numbers over the August bank holiday at the Harrow mosque in North West London, and are threatening another demo in Manchester in early October. <br />  <br /> The public presence of British racist nationalists and xenophobes continues to increase &ndash; as is typical in times of economic hardship - however the up-for-it anti-fascist movement is also growing stronger but needs increased support, particularly out on the street. <br />  <br /> <strong>* Mobilisation against BNP&rsquo;s Red, White and Blue festival</strong> &ndash; August 15th - against the BNP as they try to have their celebration of island-mentality xenophobia. Meet at Market Pl Codnor, Ripley, Derbyshire DE5 at 9am followed by a National Rally at 11am, see <a href="http://nobnpfestival.wordpress.com" target="_blank">http://nobnpfestival.wordpress.com</a> <br />  <br /> * See these websites <a href="http://www.uaf.org.uk" target="_blank">www.uaf.org.uk</a> <a href="http://jasonnparkinson.blogspot.com" target="_blank">http://jasonnparkinson.blogspot.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-fascists&source=687">anti-fascists</a>, <span style='color:#777777; font-size:10px'>birminghan</span>, <a href="../keywordSearch/?keyword=bnp&source=687">bnp</a>, <span style='color:#777777; font-size:10px'>casuals united</span>, <span style='color:#777777; font-size:10px'>english defence league</span>, <a href="../keywordSearch/?keyword=fascism&source=687">fascism</a>, <span style='color:#777777; font-size:10px'>red white and blue festival</span>, <span style='color:#777777; font-size:10px'>unite against fascism</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(456);
			
				if (SHOWMESSAGES)	{
				
						addMessage(456);
						echo getMessages(456);
						echo showAddMessage(456);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>