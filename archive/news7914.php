<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 791 - 7th October 2011 - Squat a Waste of Time</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 791 Articles: </b>
<p><a href="../archive/news7911.php">Nhs Direct Action</a></p>

<p><a href="../archive/news7912.php">Welling Up</a></p>

<p><a href="../archive/news7913.php">Angel Delight</a></p>

<b>
<p><a href="../archive/news7914.php">Squat A Waste Of Time</a></p>
</b>

<p><a href="../archive/news7915.php">Core Values</a></p>

<p><a href="../archive/news7916.php">Factory Finish?</a></p>

<p><a href="../archive/news7917.php">First We Take Manhattan</a></p>

<p><a href="../archive/news7918.php">'cos I'm Worthing It</a></p>

<p><a href="../archive/news7919.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th October 2011 | Issue 791</b></p>

<p><a href="news791.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUAT A WASTE OF TIME</h3>

<p>
<p>  
  	The government&rsquo;s consultation on squatting ended on Wednesday (5th), so if you were planning on appealing to Tory government&rsquo;s more compassionate side it&rsquo;s too late &ndash; not that you would have had much luck anyway.  </p>  
   <p>  
  	There was a late push from a number of squatting groups to try and get responses from those more sympathetic to the movement, but given the government&rsquo;s attitude to squatters (the consultation was aimed at &ldquo;victim[s] of squatting&rdquo;) they are unlikely to pay much attention. The process was a box ticking exercise from a government who have already vowed to &ldquo;end squatters rights&rdquo;.  </p>  
   <p>  
  	Meanwhile a new report from the Centre for Regional Economic and Social Research at Sheffield Hallam University found that &ldquo;the popular characterisation of squatters bears little resemblance to reality... squatting and homelessness are inextricably linked&rdquo; and that &ldquo;criminalising squatting will result in the criminalisation of homeless people&rdquo;.  </p>  
   <p>  
  	The report recommended not creating new offences, but instead devoting research and assistance towards those who are forced to squat. Not that any of this has had any effect on Grant Shapps, Crispin Blunt or Mike Weatherly the leading cheerleaders of the anti-squat lynch mob.  </p>  
   <p>  
  	All three were the focus of criticism in a letter to the Grauniad on 25th September signed by a 160 lawyers. The thrust of their argument is that the case for a new squatting law is based upon lies in the mainstream press, and that misleading statements from the government was adding to this. They want ministers to &ldquo;correct any statements they have made which are likely to have confused the public&rdquo; in order to allow an &ldquo;informed factual discussion rather than a response based on sensationalist misrepresentation&rdquo;.  </p>  
   <p>  
  	Obviously that sort of thing wouldn&rsquo;t appeal to Mike Weatherly&rsquo;s foamy mouthed constituents so he got his response in the next day. Unfortunately, he completely ignored most of the points in the lawyers&rsquo; letter and instead called them &ldquo;out of touch&rdquo; and suggested they were acting out of self interest. This view was echoed by Grant Shapps who Tweeted - &ldquo;These lawyers are sadly out of touch for believing that taking &ldquo;a few days&rdquo; to clear squatters is a reasonable outcome!&rdquo;, when asked if he knew that squatting a home was already a criminal offence, there was silence.  </p>  
   <p>  
  	It is to this background of reasoned debate being shutdown with populist soundbites that any new law will be announced, though with National Union of Students, Unite and others talking about opposing the legislation, they might not get it all their own way.  </p>  
   <p>  
  	* For more see <a href="http://squashcampaign.org" target="_blank">http://squashcampaign.org</a> <a href="http://nearlylegal.co.uk" target="_blank">http://nearlylegal.co.uk</a> &amp; <a href="http://squatter.org.uk." target="_blank">http://squatter.org.uk.</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1381), 160); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1381);

				if (SHOWMESSAGES)	{

						addMessage(1381);
						echo getMessages(1381);
						echo showAddMessage(1381);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


