<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 673 - 24th April 2009 - Summit Of The Un-americas</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, barack obama, summit of the americas, trinidad, chavez, evo morales, fidel castro, cuba, nafta, plan colombia, plan mexico, zapatistas, latin america" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 673 Articles: </b>
<p><a href="../archive/news6731.php">Quick Fix</a></p>

<p><a href="../archive/news6732.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news6733.php">Summit Of The Un-americas</a></p>
</b>

<p><a href="../archive/news6734.php">And Then We Take... Bil&#8217;in</a></p>

<p><a href="../archive/news6735.php">Brum Punch</a></p>

<p><a href="../archive/news6736.php">Tiger Tiger  Burning Bright</a></p>

<p><a href="../archive/news6737.php">First Past The Post</a></p>

<p><a href="../archive/news6738.php">Passing The Baton</a></p>

<p><a href="../archive/news6739.php">Rossport In A Storm</a></p>

<p><a href="../archive/news67310.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 24th April 2009 | Issue 673</b></p>

<p><a href="news673.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SUMMIT OF THE UN-AMERICAS</h3>

<p>
<p>For Barack Obama the G20 Summit was something of a festival of sycophancy as world leaders rubbed up against him like cats in heat, eager for some of that shiny, hopeful magic to stick to them and play well to the audiences at home. Two weeks later though, at the Summit of the Americas, Obama was facing a different proposition altogether.  <br />
     <br />
    Gone were the gushing platitudes and hearty backslapping, replaced instead with 50 minute diatribes on &lsquo;Yankee Imperialism&rsquo;, homie handshakes with American bogeymen, and allegations that America was meddling and plotting assassinations in other countries. <br />
     <br />
    The summit was held in Port of Spain, Trinidad, where it has been deeply controversial. Problems began months before the summit when Beetham Gardens, a run down area of government housing, was walled off from the rest of the capital. <br />
     <br />
    Beetham came into existence 35 years ago when a large squatters community was transformed by government financed construction of concrete homes with running water, indoor plumbing, electricity and even mail delivery. But neglected thereafter, it has since been plagued by poverty and crime. In what the government declared to be nothing more than a touch of &lsquo;beautification&rsquo;, the 5ft wall completely separated the community from the rest of the city. <br />
     <br />
    Squatters living on land near a railway line due to transport the arriving heads of state were other casualties of the tarting up process. While some had lived in the area for over thirty years, they were handed eviction notices by the Land Settlement Agency - despite many of them holding &lsquo;Certificates of Comfort&rsquo; which should have protected them from eviction. Even after angry protests the residents say they were offered no serious alternatives despite promises. <br />
     <br />
    As the summit approached, the Trinidad and Tobagan government took further steps to protect the incoming leaders from reality. With the summit costing billions of dollars to put on, a number of local unions and social movements were planning large demonstrations to highlight the hypocrisy of such extravagant spending in the face of far more pressing issues like high food prices, inflation, crime, corruption and health. However, all applications for demos were rejected, essentially banning all protest. The Truth Drummers &ndash; a coalition of several farming, civil rights and environmental groups - were told they had permission to stage a drumming demo but as soon as they began were ordered to disperse under threat of arrest, facing off with armed riot police. Another member of the group was arrested the day before the demo on charges of &lsquo;placarding&rsquo;, with bail set at $17,000. <br />
     <br />
    The alternative &lsquo;Peoples Summit&rsquo; was also repressed. All attempts to gain permission to hold the event in Port of Spain were turned down flat and the counter-summit was finally staged at the University of the West Indies, some distance outside of the city. Although this meant it didn't have the highly visible presence that the organisers had been hoping for, the three day event was still attended by hundreds of locals and internationals. <br />
     <br />
    The mainstream summit began with a now famous handshake as Venezualan president Hugo Chavez told Obama &ldquo;I want to be your friend&rdquo; and later slipped him a copy of Eduardo Galeanos famous history of imperial exploitation of Latin America, The Open Veins of Latin America. After the conference Chavez hailed a &lsquo;new era&rsquo; and announced the restoration of US-Venezuelan ambassadors. Diplomatic relations had been broken off in September in solidarity with Bolivian president Evo Morales who&rsquo;d expelled a top US diplomat for aiding opposition groups in inciting violence (Morales, subject of a recent thwarted murder attempt, was reported to have confronted Obama about American meddling in Bolvian politics and covert plots). But despite the thaw in relations Chavez still refused to back the final declaration of the summit. <br />
     <br />
    This was mostly in protest at the continued exclusion of Cuba from the gathering &ndash; a subject several other leaders raised with Obama. Despite the improvement in US-Cuban relations after the US relaxed travel restrictions and Cuban leader Raul Castro declared he was willing to talk about &lsquo;everything&rsquo;, the Big Brother of the Castro family &ndash; Fidel - declared the summit to be nothing more than &lsquo;delirious dreams&rsquo;. <br />
     <br />
    Chavez was not alone in refusing to sign the final declaration. In fact, Trinidad Prime Minister Patrick Manning was the only one that did. Aside from Cuba, absentees also cited issues that were off the agenda - such as a serious discussion of the economic crisis and more local issues like the growth of the biofuels industry. Also of concern was the Obama&rsquo;s refusal to renegotiate NAFTA (North American Free Trade Area) or other free trade agreements. <br />
     <br />
    The rise of the current generation of leftist Latin American leaders has been paralleled by a rise in social movements at the grassroots level. But for those movements there were a number of pressing issues which barely got a mention. One such was the increasing militarisation of Colombia and Mexico in the name of the &lsquo;War on Drugs&rsquo;. <br />
     <br />
    While Obama has won plaudits for stalling a Free Trade deal with Colombia until it improves its horrendous human rights record, he has also backed the continuation of &lsquo;Plan Colombia&rsquo; which supplies billions of dollars of military equipment and support to the Colombian army. The plan has completely failed in its stated intention of ridding Colombia of the scourge of cocaine with coca production actually on the increase since its inception. While the American policy of spraying coca crops with industrial Roundup has caused untold environmental damage, the coca growers simply moved deeper into the cover of the jungle. Meanwhile the militarisation has reduced violence in major population centres (without tackling the underlying causes) but has pushed an ever expanding conflict deeper into rural areas, with accompanying human rights abuses. <br />
     <br />
    Mexico, with its exploding drug war, looks set to follow a similar path. A similar agreement, &lsquo;Plan Mexico&rsquo; - also known as the &lsquo;Merida Initative&rsquo; - began in 2008, a year in which over 6000 people were killed in the drug war. In a pre-summit visit to Mexico, Obama backed the plan and asserted his commitment to help fight the &lsquo;War on Drugs&rsquo;. <br />
     <br />
    For the indigenous rights movements across the continent, which continue to grow regardless of this latest summit circus, the main struggles are around recognition and land rights but also against environmental degradation, foreign exploitation of resources and the militarisation of their territories. As well as the continuation of the Zapatistas in Mexico there have been large-scale movements in Ecuador and Colombia (<a href='../archive/news656.htm'>SchNEWS 656</a>) and smaller protest movements wherever there is a sizeable indigenous population. <br />
     <br />
    Obama&rsquo;s opening statement that &ldquo;I have a lot to learn and I&rsquo;m very much looking forward to listening&rdquo; may be welcome relief after the Bush years. But with so many key issues off the table, and a number of Latin American countries refusing to cave in to US pressure, it might just be that Hilary Clinton&rsquo;s commentary on proceedings that she &ldquo;...thought the cultural performance was fascinating,&rdquo; may be more revealing. <br />
     <br />
    * See also <a href="http://mamaradio.blogspot.com" target="_blank">http://mamaradio.blogspot.com</a> <br />
    </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=barack+obama&source=673">barack obama</a>, <a href="../keywordSearch/?keyword=chavez&source=673">chavez</a>, <span style='color:#777777; font-size:10px'>cuba</span>, <a href="../keywordSearch/?keyword=evo+morales&source=673">evo morales</a>, <span style='color:#777777; font-size:10px'>fidel castro</span>, <a href="../keywordSearch/?keyword=latin+america&source=673">latin america</a>, <span style='color:#777777; font-size:10px'>nafta</span>, <span style='color:#777777; font-size:10px'>plan colombia</span>, <span style='color:#777777; font-size:10px'>plan mexico</span>, <span style='color:#777777; font-size:10px'>summit of the americas</span>, <span style='color:#777777; font-size:10px'>trinidad</span>, <span style='color:#777777; font-size:10px'>zapatistas</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(296);
			
				if (SHOWMESSAGES)	{
				
						addMessage(296);
						echo getMessages(296);
						echo showAddMessage(296);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>