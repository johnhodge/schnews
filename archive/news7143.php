<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 714 - 19th March 2010 - Netan-yahoo Sucks</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, palestine, israel, occupation" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 714 Articles: </b>
<p><a href="../archive/news7141.php">Tit Top</a></p>

<p><a href="../archive/news7142.php">Bristols At Dawn</a></p>

<b>
<p><a href="../archive/news7143.php">Netan-yahoo Sucks</a></p>
</b>

<p><a href="../archive/news7144.php">Shrop Till They Drop</a></p>

<p><a href="../archive/news7145.php">Thai Dyed</a></p>

<p><a href="../archive/news7146.php">Sus-sexin' It Up</a></p>

<p><a href="../archive/news7147.php">Acquit While You're A-head</a></p>

<p><a href="../archive/news7148.php">Bloody Bankers</a></p>

<p><a href="../archive/news7149.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th March 2010 | Issue 714</b></p>

<p><a href="news714.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NETAN-YAHOO SUCKS</h3>

<p>
The inverse law of political spiel states that, in order to get to the truth, simply reverse any statement made by any politician. So, when Obama says &ldquo;There&rsquo;s no crisis in Israel-US relations&rdquo; you know something weird is going down. <br />   <br /> Side by side with the all-over-the-news spat between Satans great and small, the much maligned and sidelined Palestinians of the West Bank and Jerusalem seem to be kicking off in quite some fury. Hamas try to claim this as theirs - declaring a &lsquo;Day of Rage&rsquo; after the rioting had already started. From England, we wonder if the Palestinians can fit as much Rage as we had during the &lsquo;Summer of Rage&rsquo; into one day. Cities and towns across the west bank have errupted in street fighting between stone throwing shabab (yoof) and heavily armed IDF soldiers. <br />   <br /> The Old City of Jerusalem has been closed to Palestinians on Israeli orders, and the main checkpoint controlling movement from Jerusalem to the West Bank has gone from being a transport bottleneck to an IDF live fire range. It looks like there&rsquo;s a chance that the oft called &lsquo;Third Intifada&rsquo; may be kicking off. <br />   <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/714-Israel-Clinton-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/714-Israel-Clinton-sm.jpg"  /></a></td></tr></table>Israel&rsquo;s relentless settlement drive in Palestinian East Jerusalem has been the immediate cause of all this. In the last month the Israeli state announced plans to demolish 200 Palestinian homes in Silwan, &lsquo;restored&rsquo; synagogues in the Old City (an important religious centre for three wrong religions) and then announced that they&rsquo;d be building an extra 1,600 homes on stolen Palestinian land. <br />   <br /> Strangely, this last part didn&rsquo;t just annoy the Palestinians. The latest round of international hoo-ha was sparked off after this latest settlement building announcement was made while the US Vice President Joe Biden was in town. Biden, &lsquo;Israel&rsquo;s best friend in Washington&rsquo;, was preparing to be wined and dined by PM Netanyahu when the announcement was made. This is just not how the game is played by the seasoned political hypocrites in the Washington-Tel Aviv nexus. The way it&rsquo;s supposed to be is that the Israelis reluctantly agree to some &lsquo;painful concessions&rsquo; to stop colonising (for a week or so when the US is in town) so that &lsquo;peace talks&rsquo; can take place. As soon as the secretary of state, or VP or whoever, has gone - taking CNN and Fox with them - Israel starts building again and then blames the Arabs when peace talks fail. <br />  <br /> Netenyahu&rsquo;s coalition of rightists, ultra-rightists and the religious right have decided that they don&rsquo;t need any of these long established niceties - going out of their way to humiliate their patron and provider the US of A as publicly as possible.  Their &lsquo;apology&rsquo; didn&rsquo;t seem to do too much good - claiming it was a mistimed briefing by low ranking Jerusalem official. As one former CIA analyst put it, that&rsquo;s like saying &ldquo;Sorry I punched you on Monday. It shouldn&rsquo;t have happened on Monday.&rdquo; <br />  <br /> Quite why they would do so has been the subject of much discussion. Could it be that Israel is sending a message to the US that it&rsquo;s perfectly happy with unilateral actions, and that this latest show is just testing the waters for an attack on Iran? Just as plausible is that Netanyahu&rsquo;s Likud government sees the Republican party as its natural ally and wants to take Obama down a peg or two. Or, over the next month or so there&rsquo;ll be the usual Israeli &lsquo;climbdown&rsquo;, followed by more settlements and a pointless peace process that serves to take the heat off the Obama administration for a year or so. <br />  <br /> One things for sure, the combination of US affront and Palestinian anger mean that for the first time in a long while the US is seeing something a little like the rest of the world has long been used to seeing - Israeli injustice met with Palestinian righteous anger. <br />  <br /> * See <a href="http://palsolidarity.org" target="_blank">http://palsolidarity.org</a> <a href="http://www.awalls.org" target="_blank">www.awalls.org</a> <a href="http://www.maannews.net/eng" target="_blank">www.maannews.net/eng</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=israel&source=714">israel</a>, <a href="../keywordSearch/?keyword=occupation&source=714">occupation</a>, <a href="../keywordSearch/?keyword=palestine&source=714">palestine</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(699);
			
				if (SHOWMESSAGES)	{
				
						addMessage(699);
						echo getMessages(699);
						echo showAddMessage(699);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


