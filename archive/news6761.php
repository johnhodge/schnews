<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 676 - 22nd May 2009 - Final Frontiers</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, immigration, no borders, deportation, calais, detention centre, colnbrook" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 676 Articles: </b>
<b>
<p><a href="../archive/news6761.php">Final Frontiers</a></p>
</b>

<p><a href="../archive/news6762.php">Not A Blind Brit Of Difference</a></p>

<p><a href="../archive/news6763.php">War That Broke The Tamils' Back</a></p>

<p><a href="../archive/news6764.php">Now We're Rolling</a></p>

<p><a href="../archive/news6765.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/676-startTrek-lg.jpg" target="_blank">
													<img src="../images/676-startTrek-sm.jpg" alt="They're forcing you to go back to Vulcan Spock, but why?!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 22nd May 2009 | Issue 676</b></p>

<p><a href="news676.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FINAL FRONTIERS</h3>

<p>
<p><strong>AS WE BOLDLY TAKE A SCHNEWS CRUISE TO CALAIS</strong> <br />  <br /> &ldquo;<em>Please tell your government and your media that we are here, then I&rsquo;m sure they will help us. There is a family; a young Kurdish girl only five-years-old in the Jungle. The police came and gassed the family, she is very weak. I am OK, but I am worried for her, please go back and tell them that we are here</em>.&rdquo; - <strong>Iraqi Kurd in Calais.&nbsp; <br /> </strong> <br /> A humanitarian crisis caused by British immigration policy is taking place just over the channel in Calais. Thousands of people in search of a new life in the area are risking their lives to enter the UK and are only surviving with the &lsquo;illegal&rsquo; solidarity of local people. The situation has worsened dramatically since the forced closure of the Red Cross&rsquo;s Sangatte centre. <br />  <br /> A team from the No Borders network recently visited Calais &ndash; in advance of the No Borders Action camp (23-30th June). The following are edited extracts from their report: <br />  <br /> &ldquo;<em>Following a scare campaign by the right wing press, in 2002 an agreement between UK and French governments shut the Sangatte Red Cross centre. The centre provided shelter for 2000 people and its closure pushed the situation for migrants in Calais to crisis point. Large groups from Afghanistan, Eritrea, Iraq, Sudan and Palestine gather there before attempting to cross the channel, by clinging on to the underside of lorries or attempting to walk the length of the tunnel. <br />  <br /> &nbsp;Speaking to some of the migrants we learned how up to 1000 people are living in woods near  the ferry port known as &lsquo;The Jungle&rsquo;, all waiting for a chance to travel to the UK. Police regularly destroy or even burn their temporary structures and  tear gas their tents. More repression seems imminent with French immigration minister Eric Besson stating he wants an &lsquo;exclusion zone&rsquo; for migrants in the region</em>.&rdquo; - <strong>NBN <br /> </strong> <br /> Across the globe millions are on the move - responding to war, climate change or economic needs. The rich world&rsquo;s response is to throw up a network of fences, cameras and checkpoints to &lsquo;manage&rsquo; this movement. One bottleneck for migrants is Calais &ndash; the final hurdle before entering Blightly. The UKs borders now extend to this portion of France (for the first time since 1558 for you history buffs). &nbsp; <br />  <br /> So why are migrants heading here? Nobody&rsquo;s coming here looking for a &lsquo;soft touch&rsquo; no matter what you read in the Sun. The fact is that thanks to our globe-spanning imperialist past we have had an impact on countries all around the world. English is the global language while many British polices and investments abroad are responsible for forced migration. Waging war in the Middle East, exporting weapons to military regimes and supporting large infrastructure projects, such as dams, oil and gas pipelines and mines have all provoked a mass exodus of people. Dams alone, many backed by Britain cash, have displaced an estimated 80 million since the Second World War.&nbsp; <br />  <br /> The tightening of border controls across  Europe and in the UK has created a de-facto underclass across the Union &ndash; people without states or rights or access to vital infrastructure such as health care. While genuinely free movement is feared by our dark masters, &lsquo;managed&rsquo; migration can serve their interests very nicely &ndash; cheap black market labour undermining legal workers status. <br />  <br /> This process of &lsquo;managed migration&rsquo; involves a network of detention centres which incarcerates refused asylum seekers and refugees, picks up people on entry to the UK or those who&rsquo;ve overstayed their visas. These people are detained for weeks, months and even years. Some are currently being held in detention indefinitely. At any one time there are around 3,000 people detained under immigration powers in this country. With the detention centres run privately this means there are plenty of fat contracts out there for companies wanting to get in on the refugee detention game. <br />  <br /> <strong>BORDERLINE INSANITY</strong> <br />  <br /> The suffering inflicted on migrants in Calais is just one example of a brutal system whose apparent purpose is to deter others from coming but which also creates the conditions for an illegal workforce that undermines the rights of all workers. Calais is just one example of the hi-tech EU borders regime that has resulted in the death (many at sea) of at least 14,000 migrants in the last twenty years, according to a recent press review.&nbsp; <br /> The EU policy of the &ldquo;free movement of persons&rdquo; within its borders has gone hand in hand with an attempt to build &lsquo;Fortress Europe&rsquo;; externalising EU borders into Africa and Asia with EU border guards patrolling the Mediterranean, Libya and off the West Coast of Africa, courtesy of the Frontex borders agency. Also via the European Neighbourhood Policy where countries from the Ukraine all the way round the Mediterranean to Morocco are now paid by the EU to do its migration prevention work for it.&nbsp; <br />  <br /> Although most internal European borders were removed by the 1990 Schengen Convention. But while the Schengen agreement was a mechanism for allowing the &lsquo;free movement&rsquo; of certain people within certain EU states, it has always gone hand in hand with an increase in internal borders and social controls, such as SIS (Schengen Information System). Another recent manifestation of Schengen policy has been the creation of Frontex, an armed transnational border police that works almost exclusively outside EU territory.&nbsp; <br />  <br /> And meanwhile the UK has continued to ferociously police its borders, nowhere more so than in trying to stop people coming in from Calais, leaving people there between the proverbial rock and a hard place. As NBN report: <br />  <br /> "<em>French law specifically forbids aiding &lsquo;illegal immigrants&rsquo;, punishable by up to five years in prison and a &pound;25,000 fine. Despite this, we met with two local humanitarian groups who distribute free food to migrants - these volunteers risk arrest daily simply by feeding people. <br />  <br /> One friend who has lived with the migrants in Calais for several months explained to us how the night-time routine involves numerous midnight escape attempts. Nearby truck stops are patrolled by gangsters, who wander the night with knives out, wearing Balaclavas and demanding ransom from any migrant wishing to pass. Just like on the Greek, Italian and Turkish borders the non-existence of any legal entry for asylum-seekers has produced a market of illegal transport facilities, a prospering business at the EU-borders. Border controls have produced this market while criminalising the migrants. Draconian penalties against &ldquo;people trafficking&rdquo; drives up transportation prices making the market more attractive to criminal elements, as well as making a secure arrival at the aimed destination impossible.</em>&rdquo;&nbsp; <br />  <br /> No Borders is a transnational network of autonomous groups who advocate freedom of movement and equality for all, but more than that it is a political position; seeing borders as systemic to a world order that is supremely unjust and is structured towards massive inequality, acting as a useful control to divide a social hierarchy of legal/illegal, documented/undocumented, citizens/non-citizens. No Borders means rejecting this categorisation and calls for unity between exploited people against the rich and powerful. <br />  <br />  <br />  <br /> No Borders are holding a transnational demo for freedom of movement and the abolition of migration controls in Calais on the 27th of June. <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> <br />  <br />  <br />  <br /> *<strong>Activists from the Stop Deportation Network blockaded Colnbrook detention centre</strong> on 12th May in a last-ditch attempt to prevent the mass deportation of around 45 people to Iraqi Kurdistan. The six activists efwfectively blocked the entrance to the detention centre for four hours after which time they were cut from their lock-ons and arrested for obstruction of the highway. Tinsley House, a detention centre near Gatwick has also been targeted in recent months (see <a href='../archive/news669.htm'>SchNEWS 669</a>).</p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>calais</span>, <span style='color:#777777; font-size:10px'>colnbrook</span>, <a href="../keywordSearch/?keyword=deportation&source=676">deportation</a>, <span style='color:#777777; font-size:10px'>detention centre</span>, <a href="../keywordSearch/?keyword=immigration&source=676">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=676">no borders</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(329);
			
				if (SHOWMESSAGES)	{
				
						addMessage(329);
						echo getMessages(329);
						echo showAddMessage(329);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>