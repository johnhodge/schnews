<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 788 - 16th September 2011 - Nice One Giza</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, palestine, egypt" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 788 Articles: </b>
<p><a href="../archive/news7881.php">World's Largest Arms Fail</a></p>

<p><a href="../archive/news7882.php">Frack Shit Up</a></p>

<p><a href="../archive/news7883.php">Brighton 9: Sticking It To The Manikin</a></p>

<p><a href="../archive/news7884.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news7885.php">Nice One Giza</a></p>
</b>

<p><a href="../archive/news7886.php">De-fault Is All Greece's</a></p>

<p><a href="../archive/news7887.php">Edl: Tommy Chickens Out</a></p>

<p><a href="../archive/news7888.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th September 2011 | Issue 788</b></p>

<p><a href="news788.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NICE ONE GIZA</h3>

<p>
<p>  
  	Egyptian protesters have proven themselves to be a feisty bunch of late &ndash; not satisfied with kicking out their dictator, they sent the Israeli ambassador scurrying back across the Sinai Desert.  </p>  
   <p>  
  	Hundreds of pro-Palestinian activists separated from the much larger demo against military rule in Tahrir Square and converged on Israel&rsquo;s embassy building in Giza, on the bank of the Nile. The protesters clearly meant business - arriving with sledgehammers and setting to work demolishing the reinforced concrete walls.&nbsp; Egyptian security were completely overwhelmed, and seemed either unable or unwilling to stop the determined mob. Once inside the compound protesters scaled the walls, chucking out thousands of Hebrew language documents from the upper floors.  </p>  
   <p>  
  	The prize for most impressive act goes to a chap by the name of Ahmed Elshahat, aka &ldquo;Flagman&rdquo; who scaled the 22 stories of the building to tear down the Israeli flag. His award &ndash; a cartoon him in action by Brazilian cartoonist Carlos Latuff, (and no doubt a personal audience with the Egyptian military&rsquo;s finest interrogators). An Egyptian flag was put in place of the Israeli one.  </p>  
   <p>  
  	Egypt doesn&rsquo;t seem to be in any hurry to invite the official representative of colonial Zionism back. Egyptian activists can pat themselves on the back again for sorting out things out by themselves on the streets rather than waiting for their useless leaders to do anything.  </p>  
   <p>  
  	Israel is rapidly losing friends in the region, even Jordan doesn&rsquo;t seem safe anymore, where the Israelis withdrew their ambassador on Thursday ahead of a massive demo. For decades Israeli leaders could rely on dictatorships to strike deals with them, whilst arrogantly cawing that they were the &lsquo;only democracy in the middle east.&rsquo; The Middle East is looking a lot more democratic these days, and democratic opinion is very, very pro-Palestinian.  </p>  
   <p>  
  	After losing their ambassador to Turkey last week (see <a href='../archive/news787.htm'>SchNEWS 787</a>) and the Egyptian one this week, Netenyahu and his cabinet of racist lunatics must be in a state of panic as Turkish foreign minister Davolgulu meets up with the Egyptian government to discuss the new Middle Eastern order. The rumours flying around are that Hamas may relocate to Egypt (maybe they can use the now empty Israeli embassy building), and that the next flotilla to Gaza will be protected by Turkish warships. And if the Zionists were hoping that they might find a friend in the new NATO-loving Libyan government, they should think again &ndash; Hamas leader Ismael Haniyah personally congratulated the leaders of Libya&rsquo;s rebel government, who in turn stated publicly that &ldquo;Palestine is the most important issue for the Libyan people&rdquo;.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1360), 157); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1360);

				if (SHOWMESSAGES)	{

						addMessage(1360);
						echo getMessages(1360);
						echo showAddMessage(1360);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


