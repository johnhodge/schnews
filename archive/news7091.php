<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 709 - 12th February 2010 - Slung Out On Yer Frontier</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, calais, no borders, immigration, asylum seekers" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 709 Articles: </b>
<b>
<p><a href="../archive/news7091.php">Slung Out On Yer Frontier</a></p>
</b>

<p><a href="../archive/news7092.php">Carmel Agrexco : Slay It With Flowers</a></p>

<p><a href="../archive/news7093.php">I-ran Away</a></p>

<p><a href="../archive/news7094.php">Serco-mplicit  </a></p>

<p><a href="../archive/news7095.php">Cold Turkey  </a></p>

<p><a href="../archive/news7096.php">West Bank Story </a></p>

<p><a href="../archive/news7097.php">Flying In The Face</a></p>

<p><a href="../archive/news7098.php">Sus-sex Appeal</a></p>

<p><a href="../archive/news7099.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th February 2010 | Issue 709</b></p>

<p><a href="news709.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SLUNG OUT ON YER FRONTIER</h3>

<p>
<p class="MsoNormal"><strong>SchNEWS ASKS YOU AND WHOSE GENDARMERIE? AS FRENCH COPS CRACKDOWN ON MIGRANT WELFARE CENTRE</strong> <br />  <br /> <strong>On Sunday afternoon French riot police smashed their way in to the recently opened migrant solidarity centre in a Calais warehouse (see <a href='../archive/news708.htm'>SchNEWS 708</a>), arresting the activists inside and trashing the migrants&rsquo; possessions. Several activists were injured in the raid while outside the centre, police beat migrants with batons.</strong> <br />  <br /> Following the closure of a night shelter offering temporary respite to migrants from the freezing conditions, they were again at the mercy of the French CRS riot cops&rsquo; destructive frenzy (see <a href='../archive/news707.htm'>SchNEWS 707</a>). The night shelter had been opening sporadically when temperatures dropped below the requisite -2C but closed down last week. <br />  <br /> Two solidarity groups, No Borders and SoS - Soutien aux Sans-papiers - last week legally rented an empty hangar in Calais to create an activist and migrant space. <br />   <br /> According to Marie Chautemps, &ldquo;The Kronstadt Hangar was opened as a direct challenge to the winter of repression that the migrants in Calais have faced since their &lsquo;jungle&rsquo; communities were destroyed in a cruel PR stunt, back in September (see <a href='../archive/news692.htm'>SchNEWS 692</a>). With the authorities blocking any attempts to create a place for migrants to shelter from constant police harassment, or from the bitterly cold winter, the Kronstadt Hangar was set up in the name of common human respect as well as resistance to an increasingly fascist EU border policy.&rdquo; <br />  <br /> <strong>BESSON AND ON</strong> <br />  <br /> The French authorities were never going to put up with such a visible display of solidarity, however legally acquired, and ordered an illegal eviction. Before the eviction of the &lsquo;jungle&rsquo; last September, French immigration minister Besson had vowed to make Calais an immigrant free zone by the end of the year. Despite missing this target, the numbers of migrants dropped from around 1,500 to a few hundred over the winter. Under pressure from the UK authorities to keep migrants away from channel ports, there was no let up on harassment and property destruction by his CRS henchmen. It is the policies of the same UK authorities and their French NATO allies in Afghanistan that many of these migrants are fleeing from. <br />  <br /> However, until Saturday the police had only kept a close eye on the centre, threatening to arrest any migrants who entered. Migrants weren&rsquo;t told about the hangar until Saturday evening when around 100 from Afghanistan, Iran, Kurdistan, Eritrea, and Ethiopia came. Defying the police blockade on the street outside, the migrants started chanting, &ldquo;Freedom! Freedom!&rdquo; and pushed against the police line. With activists from the inside joining in, a path was created for the migrants to run into the hangar. <br />   <br /> Those inside agreed on the collective nature of the organisation of the space through daily meetings and immediately set to translating a statement about the hangar into the languages of all users. Despite a police siege, the mood was upbeat on Saturday night with people chatting, listening to folk music, drinking tea, writing and drawing. Tea, blankets, sleeping bags, basic mattresses were provided and a safe, secure, although chilly night was had by all. <br />  <br /> <strong>HANGARS ON</strong> <br />  <br /> When the migrants tried to return from receiving food the following day, police prevented them from re-entering. As numbers began to dwindle, more and more police arrived and the remaining migrants were told to leave or be arrested. All left the hangar but remained nearby shouting &ldquo;<strong>You fought for us, we&rsquo;ll fight for you!</strong>&rdquo;. With the media kept well away from the hangar, the police smashed their way in with a battering ram and arrested around 12 activists. <br />   <br /> The CRS then roamed the streets chasing and arresting migrants, while others managed to get away   as activists hindered their efforts with decoys. The activists arrested were later released, only allowed to return to the centre to collect their belongings. Migrant&rsquo;s possessions had been stolen along with activists&rsquo; equipment such as camera film, phones, house keys and bank cards. The doors were welded shut. <br />  <br /> The police repression couldn&rsquo;t extinguish the links that had been forged between solidarity activists and migrants who actively participated in and appreciated attempts to create a common space. This latest initiative shows how difficulties like language barriers and the high turnover of activists and migrants can be overcome to increase resistance to the racist border regime.. <br />  <br /> *For more see <a href="http://www.calaismigrantsolidarity.wordpress.com" target="_blank">www.calaismigrantsolidarity.wordpress.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=709">asylum seekers</a>, <a href="../keywordSearch/?keyword=calais&source=709">calais</a>, <a href="../keywordSearch/?keyword=immigration&source=709">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=709">no borders</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(651);
			
				if (SHOWMESSAGES)	{
				
						addMessage(651);
						echo getMessages(651);
						echo showAddMessage(651);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


