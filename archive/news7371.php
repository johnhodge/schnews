<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 737 - 3rd September 2010 - Out of Their League</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, english defence league, racism, anti-fascists, far right" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 737 Articles: </b>
<b>
<p><a href="../archive/news7371.php">Out Of Their League</a></p>
</b>

<p><a href="../archive/news7372.php">Brighton Defence League</a></p>

<p><a href="../archive/news7373.php">Grounded At Heathrow</a></p>

<p><a href="../archive/news7374.php">Marx And Sparks</a></p>

<p><a href="../archive/news7375.php">Shell Corrib Pipeline Under Attack</a></p>

<p><a href="../archive/news7376.php">On The Calais Beat</a></p>

<p><a href="../archive/news7377.php">Get Charter</a></p>

<p><a href="../archive/news7378.php">  Truth Or Dairy?</a></p>

<p><a href="../archive/news7379.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd September 2010 | Issue 737</b></p>

<p><a href="news737.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>OUT OF THEIR LEAGUE</h3>

<p>
<p>
	<strong>IS THE EDL WAVE BREAKING AS THE &lsquo;BIG ONE&rsquo; FLOPS?</strong> </p>
<p>
	It was supposed to be &lsquo;The Big One&rsquo; - that&rsquo;s how the EDL were billing their Bradford rally - a climactic moment in their campaign against &lsquo;radical Islam&rsquo;. According to puff pieces released on Youtube before the event, there were supposed to be 5,000 leaguers descending on the Yorkshire town on Saturday 28th August. The EDL had warned women and children not to be present and one flyer bore the slogan &lsquo;Burn, baby, burn&rsquo;. A much smaller rally by the National Front in 2001 had sparked three days of rioting (see <a href='../archive/news313.htm'>SchNEWS 313</a>) in Oldham which saw over 200 men incarcerated for periods of up to five years. Bradford braced itself. In the end a mere 700-800 EDLers were on display. Two days later, a march by the English Nationalist Alliance in Brighton saw no more than fifty fascists show up and their march was only forced through the city&rsquo;s streets by Sussex Police with batons and dogs. </p>
<p>
	Is this the beginning of the end for the EDL? Before Bradford the EDL looked to be on a roll. Nothing succeeds like success and for a while the League looked unopposed by anybody except the state. Rallies in Bolton and Dudley had attracted possibly up to two thousand. They had held rallies in Manchester, Aylesbury, Nottingham, Newcastle, Stoke and elsewhere. </p>
<p>
	Attempts to spread to Wales and Scotland were met with disinterest but in northern England at least they appeared to be getting stronger. Organised opposition from the left had largely come from the ineffectual Socialist Worker&rsquo;s Party (SWP) front Unite Against Fascism (UAF). A few anarchos and direct action types were making regular pilgrimages to the EDL demos but not in the numbers required to make any difference. Often the Muslim community, under pressure from the police, were able to keep their young men off the streets. In Bolton police action was directed at anti-fascists, within even the UAFs Weyman Bennett getting lifted for &lsquo;conspiracy to commit violent disorder&rsquo;. </p>
<p>
	Apart from the set-piece demos, followers began branching out under the EDL flag, placing pigs&rsquo; heads on mosques and beginning to turn up to small meetings of Indymedia, the Anarchist Federation and the UAF. </p>
<p>
	<strong>NORTHERN LIGHTS OUT</strong> </p>
<p>
	Bradford was different - from early in the day a mixed group of travelling anti-fascists and local Muslim youth began congregating in the area where the EDL were to be confined (their march had been banned on request of the local authority but a static demonstration was permitted). In the event the EDL were bussed into a waste-ground, mostly surrounded by plywood hoarding (the intended site of a new shopping centre). Bradford is shaped like a bowl with the centre at the bottom and the town and its estates looking down on all sides. The EDL were, fittingly, at the lowest point in town. Anyone outside the perimeter had a birds-eye view of proceedings. </p>
<p>
	At midday there were already 150 EDL inside the cordon, leading to fears that the apocalyptic scenarios promised were about to unfold. Two EDLers even felt confident enough to give a press conference outside their pen. The fact that one of them was the EDLs only asian member confused issues for a few minutes but they were soon chased back with shouts of &ldquo;Nazi Scum - off our streets&rdquo;. </p>
<p>
	By 2pm however, the advertised start time for their demo, it became clear that they had achieved a very low turn out - estimates vary from six to eight hundred. Meanwhile the UAF confined themselves to a cordon five hundred yards away in Exchange Square. Cops established a double ring around the EDL rally facing inwards and outwards. The EDL rushed towards a gap in the hoardings screened only by a couple of police vans and began hurling their racist abuse. &ldquo;Muhammed is a paedo&rdquo;, &ldquo;Who the fuck is Allah?&rdquo;. Soon stones, smoke flares and half empty cans of lager were being hurled at the more thinly spread anti-fascist crowd. The smoke flares were hurled back. Police baton charges drove anti-fascists back and it was clear that the EDL were getting the same rough treatment. </p>
<p>
	The afternoon continued in a three-way struggle with the police until, as the main body of anti-fascists were being pushed up the street by mounted police, the less lardy members of the EDL finally hit on the idea of scaling the fence. The rear of the EDL&rsquo;s pen was totally unguarded and around 150 of them escaped over the hoarding - intent on heading into Bradford and making good on their threats. </p>
<p>
	On the other side hundreds of anti-fascists broke away from the confrontation with the police and headed around the block towards the Forster Square retail park. Shouts of &lsquo;Allahu Akbar&rsquo; mixed with cries of &lsquo;Antifascista&rsquo;. There was some hesitation when the EDL were first encountered but they soon broke and ran, some of their more hapless members taking a pasting before police with dogs intervened and rescued them. Chased back into their pen the EDL tamely got back onto their coaches and were driven along the motorway under police escort. By this time the Muslim youth from the surrounding estates were in town in large numbers looking for any stray leaguers. </p>
<p>
	<strong>SOON TO BE RELEGATED?</strong> </p>
<p>
	So where next for the EDL? It is true that they have bounced back from worse defeats than this; their first two outings in Birmingham, for example, mainly involved a bit of sieg-heiling and a lot of kissing the pavement. But they were able to turn these into a call-out by painting the conflict as a race war - beleaguered Englishmen surrounded by radical jihadists. They may yet turn it around, after all the mainstream press provides ample fodder for the belief that the UK is under siege by Islamic hordes. The EDL feed off of the kind of patriotism needed to ensure that the domestic population backs Our Boys. </p>
<p>
	The EDL is not a monolithic body, it is a loose coalition operating on the basis of a lowest common denominator, a belief in the need to prevent the victory of Sharia law in the UK- a goal so easily achievable it&rsquo;s a wonder they don&rsquo;t all stay at home. Some of them are die-hard white supremacists, some are football hooligans just up for a ruck, others are disenchanted BNP activists and there&rsquo;s even an increasingly diminishing number of law-abiding flag wavers. With their taste for bespoke merchandise the EDL are obviously a cash-cow for someone. The &lsquo;leadership&rsquo; may want to pretend that they have a very narrow focus on &lsquo;militant Islam&rsquo; but the rank and file are simply there for the &lsquo;muzzie-bashing&rsquo; and racist taunts are a regular feature of their mobilisations. The contradictions in a movement always show up the moment that a coalition hits a stumbling block and for now at least the EDL seem to be indulging in a fair amount of finger pointing and mutual recrimination. Hilariously, on the front page of their website they&rsquo;re trying to blame the anti-police violence on infiltrators from Combat 18. </p>
<p>
	The waving of Israeli flags may have started as simple device for winding up the opposition, but now Zionist organisations have become increasingly involved with the EDL, linked by a hatred of Islam. EDL types have showed up to support Zionists outside Ahava (see <a href='../archive/news704.htm'>SchNEWS 704</a>), target of the Israeli boycott campaign. Once again in their analysis of what went wrong in Bradford they accuse their opponents of being anti-Israel. Which is probably true but begs the question of why the EDL care so much about the affairs of a nation in the Middle East. </p>
<p>
	At the moment, apart from rumour-mongering on their forums, the EDL do not have any more set-pieces on their calendar. They might go for Leicester next or maybe even back to Luton where it all began.They are failing to grow and anti-fascists, who were mostly stunned by the rapid growth of the far-right movement have shown that together with locals it is possible to counter-act the EDL even on their days of action. At the moment the league are down but not out... time perhaps to finish the job. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(919), 106); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(919);

				if (SHOWMESSAGES)	{

						addMessage(919);
						echo getMessages(919);
						echo showAddMessage(919);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


