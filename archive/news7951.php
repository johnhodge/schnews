<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 795 - 4th November 2011 - Dedicated Followers of Fascism</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 795 Articles: </b>
<b>
<p><a href="../archive/news7951.php">Dedicated Followers Of Fascism</a></p>
</b>

<p><a href="../archive/news7952.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7953.php">Squat Next?</a></p>

<p><a href="../archive/news7954.php">Extraordinairy Petition</a></p>

<p><a href="../archive/news7955.php">Bring 'em To Book</a></p>

<p><a href="../archive/news7956.php">Hearts Of Oakland</a></p>

<p><a href="../archive/news7957.php">Quake Up Call</a></p>

<p><a href="../archive/news7958.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th November 2011 | Issue 795</b></p>

<p><a href="news795.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DEDICATED FOLLOWERS OF FASCISM</h3>

<p>
<p>  
  	<strong>AS EDL VISIT BRUM BUT FAIL TO STRAIGHTEN OUT THE KINKS IN THEIR STRATEGY</strong>  </p>  
   <p>  
  	On Saturday 29th October 2011 the English Defence League (EDL) paid their third visit to Birmingham. Previous excursions ended in running battles through the streets (see <a href='../archive/news687.htm'>SchNEWS 687</a>) The march was the EDL&rsquo;s second big mobilisation this year, although not hyped as much as their September outing in Tower Hamlets (see <a href='../archive/news787.htm'>SchNEWS 787</a>).  </p>  
   <p>  
  	In the end the EDL&rsquo;s numbers were relatively low - around the 300-400 mark, in contrast to a turnout of 3000 in Luton in Februay (<a href='../archive/news758.htm'>SchNEWS 758</a>) and around 850 in Tower Hamlets earlier this year. This could be the EDL on a downward slide.  </p>  
   <p>  
  	EDL facebookers are moaning that the demo format (i.e. turn up in a randomly chosen town, stand around in a car park listening to coked-up egotists chuntering on about Sharia Law before the cops push you back ont o your bus) is getting stale and numbers in attendance are dwindling.  </p>  
   <p>  
  	SchNEWS travelled up with some mates from the anti-fascist network and watched the first half hour of the EDL demo before cops forcibly cleared away onlookers.  </p>  
   <p>  
  	The fash met up in Wetherspoons, Walkabout and O&rsquo;Neill&rsquo;s on Birmingham&rsquo;s Broad Street and marched a hundred yards down the road to Centenary Square where the police had set up a pen for them complete with portaloos and a tinny sound system. George Crosses and a Union Jack were displayed alongside an Israeli and American flag.&nbsp; One banner, confusingly, read: &ldquo;Gary Glitter would love Sharia Law, but the kids don&rsquo;t&rdquo;.  </p>  
   <p>  
  	The stage was set for the EDL leadership to say their piece. The first speaker (who&rsquo;s name we didn&rsquo;t catch) hit a bum-note by resigning on the spot. A passer-by began sieg-heiling the EDL which caused a crowd surge with around twenty EDL clashing with police in riot gear. Beer cans, fireworks and a rock were thrown. However, this attempt to break out of the cordon was unsuccessful.  </p>  
   <p>  
  	The break-away group was then berated from the podium and told that this was exactly what &ldquo;the Islamists wanted&rdquo;. At this point the EDL started fighting each other. Nice one lads!  </p>  
   <p>  
  	All in all, the EDL&rsquo;s Birmingham outing was a fairly desultory effort, compared to previous efforts some Leaguers were able to break out of police cordons and go on the rampage. This may be down to the leadership losing their bottle in the face of&nbsp; continual prosecutions of organisers in cases of public disorder. In a way the EDL has the worst of both worlds as it lacks a formal organisation but is clearly controlled by a small cabal responsible for its public face, media strategy and liaison with the police. With that cabal effectively bailed away from demos (Stephen Lennon / Tommy Robinson&rsquo;s stint in Bedford gaol has served as a warning) the EDL is slightly rudderless with the hooligan hardcore being told off from the podium for being overenthusiastic but nobody else wanting to join in &lsquo;cos of all the drunken aggro.  </p>  
   <p>  
  	However, the fact this was not a good day for the EDL should not be a cause for complacency. The fact remains that hundreds of racists were able to gather, virtually unopposed in Britain&rsquo;s second largest city, close to majority Asian and Muslim areas in a way that has not been seen for a generation.  </p>  
   <p>  
  	The opposition, such as it was, came from a coalition of Unite Against Fascism and local groups who organised a &lsquo;Unity&rsquo; event in Chamberlain Sq few hundred metres from Centenary Square. The crowd numbered no more than 200-300 throughout the day. Local Conservative and Labour politicians took to the podium alongside &lsquo;community leaders&rsquo;. The only break from this dreary monotony came when Martin Smith from Love Music Hate Racism took the stage and announced that he &ldquo;didn&rsquo;t feel like playing music today&rdquo;, expressed his anger at the poor turnout and asked why we couldn&rsquo;t listen to some real anti-fascists. A few brave souls ventured towards the EDL lines but were firmly dealt with by the overwhelming police presence.  </p>  
   <p>  
  	The cops were in fact in control the whole day forming lines around both the EDL rally and the counter demo, having that morning put fencing around the Occupy Birmingham site preventing entry. Filter cordons were set up&nbsp; throughout the city centre and those attending the counter demo were searched under Section 60 of the Public Order Act. Everything was heavily stewarded.  </p>  
   <p>  
  	At around 3pm a breakout was attempted from the Unity rally. Around a hundred youth marched toward a gap in police lines, only to be held back by Unity stewards. Unwillingness to shove the stewards out of the way gave the police time to form lines. At 4pm, after the EDL coaches had begun to leave, another breakout was tried, this time successful, but was unable to confront the EDL.  </p>  
   <p>  
  	Across the country cops have put into place a blueprint to deal with EDL demos. Local councils demand that&nbsp; fascist and anti-fascist demonstrations are both restricted, the area is swamped with police who use Forward Intelligence Teams to target known anti-fascists and EDL leaders and to ensure both sides are controlled by stewards. In the case of the antis&nbsp; police procure stewards from local mosques or Muslim organisations.  </p>  
   <p>  
  	The result, at the moment, is a stalemate which often allows the EDL to demonstrate unopposed in city centres around the UK.  </p>  
   <p>  
  	The fact that elements within the EDL are becoming frustrated with these set piece demonstrations&nbsp; sets new challenges for anti-fascism in the UK. The EDL and its fellow travellers have on the streets and online begun to turn their anger on the left. They&rsquo;ve made noises in this direction promising a &lsquo;ring of steel&rsquo; around the royal wedding for instance and condemning protesters against student fees.&nbsp;&nbsp; UAF meetings and stalls have been attacked and last Saturday night a group attacked the Occupy Newcastle site. A fair bit of the bluster from Casuals United and their ilk is just that &ndash; prominent threats were made against an anti-fascist benefit in Dalston last week but nothing came of it. However there is a willingness to have a pop at soft targets, small meetings and street stalls.  </p>  
   <p>  
  	As the austerity measures bite and the EDL&rsquo;s anti-Islam rhetoric loses its bite there will be an attempt to re-orientate the Leaguers towards attacks on organised working class resistance. Anti-fascism will have to become part of every activist&rsquo;s arsenal.  </p>  
   <p>  
  	* <a href="http://antifascistnetwork.wordpress.com" target="_blank">http://antifascistnetwork.wordpress.com</a> <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1412), 164); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1412);

				if (SHOWMESSAGES)	{

						addMessage(1412);
						echo getMessages(1412);
						echo showAddMessage(1412);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


