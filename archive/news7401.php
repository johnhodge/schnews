<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 740 - 24th September 2010 - Brussels Sprouts Camp</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, immigration, direct action, calais, no borders" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 740 Articles: </b>
<b>
<p><a href="../archive/news7401.php">Brussels Sprouts Camp</a></p>
</b>

<p><a href="../archive/news7402.php">Rewrite His-tory</a></p>

<p><a href="../archive/news7403.php">All Mex'd Up</a></p>

<p><a href="../archive/news7404.php">Running Ham-mock</a></p>

<p><a href="../archive/news7405.php">Stockholme Syndrome</a></p>

<p><a href="../archive/news7406.php">Good Chap On Trial</a></p>

<p><a href="../archive/news7407.php">Schnews In Brief</a></p>

<p><a href="../archive/news7408.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 24th September 2010 | Issue 740</b></p>

<p><a href="news740.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRUSSELS SPROUTS CAMP</h3>

<p>
<p>
	<strong>NO BORDERS HEADS TO BELGIUM TO FIGHT EU IMMIGRATION POLICIES</strong> </p>
<p>
	No Borders Camp 2010 in Brussels kicks off this Saturday (25th) til the 3rd October, and plenty are converging on that part of the continent in an effort to create a world where no one is illegal. Among the objectives of the camp are the denouncing of European migration policy; showing the links between this policy and the structures of capitalism and repression; the blocking of Brussels&rsquo; deportation system and the organisation of an autonomous safe space for the voices of migrants and activists to be heard. </p>
<p>
	<strong>WHERE WILL IT BE?</strong> </p>
<p>
	The camp plans to take space at TOUR&amp;TAXIS, a complex in the heart of Brussels with green areas and buildings usually used for conferences and events. There&rsquo;s plenty of room for upwards of 1,000 people and finding it is pretty easy once you&rsquo;re in the city - check out the travel advice on the website. There&rsquo;s also a section on an anarchopedia website to find travelling partners or arrange liftshares. A decentralised info point will be set up at G&eacute;su, an abandoned monastery which is squatted by around 130 people; from there you can find out what&rsquo;s going on at the camp. </p>
<p>
	<strong>HOW DOES IT ALL WORK?</strong> </p>
<p>
	Different working groups have been responsible for different aspects of the camp and will continue to do so on site, although all decision are made by consensus. The program group co-ordinates proposals for workshops and affinity group actions, and sets locations and times for activities during the camp; the finance group and logistics group take care of money and materials. Support will be provided by the legal and medical teams. There is a dedicated Big Demo working group to plan for the mass action on the 2nd October, but self-organised and autonomous actions are actively welcomed during the week too. A mobilisation group will be on hand to assist. The process group will be going round making sure that every proposal is running smo othly and nothing gets forgotten. Food will be cooked on site and organised by the kitchen team. Participation in these working groups is open to everyone at the camp. </p>
<p>
	A general assembly will be held every morning during the camp for decisions to be made, issues to be raised, and for briefings from the different working groups to be given. Workshops will also run throughout the week with a different theme each day: European migration policy, detention &amp; deportations, capitalism &amp; migration, the struggle of migrants, and the militarisation of borders, all culminating in the mass demo day on the 2nd, then debrief the day after. There will be photo exhibitions and a cinema showing informative films while other spaces in the city will host debates, gigs and cabaret. </p>
<p>
	There is a general policy of no photos or filming on site, even for campers. Journalists will only be allowed into the area at allotted times, escorted, and if you&rsquo;re in shot, you&rsquo;ll be asked for your permission before they take any pictures or film. A dedicated media team will be taking care of this. </p>
<p>
	<strong>WHAT DO I NEED?</strong> </p>
<p>
	Whatever camping gear will make you comfy, and if you&rsquo;re happy to leave stuff like tents and sleeping bags behind at the camp after you leave, it will go to migrants and activists who are camping out permanently in Calais and Brussels and desperately need it. </p>
<p>
	The logistics group also have a long wishlist of tat and equipment which you can find on the website, so have a read through before you leave in case you happen to be the one who has 300m of plumbing hose lying around in your back garden. </p>
<p>
	<strong>AND THE COPPERS?</strong> </p>
<p>
	Generally aren&rsquo;t that bad, and the legal team will be on hand, but there are a few things to watch out for.. </p>
<p>
	Belgian police like to break cameras so although its not forbidden to film them or take pictures, make sure you&rsquo;re doing it out of arms reach. </p>
<p>
	It&rsquo;s an offence to cover your face with masks, scarves or make-up in Brussels so be prepared to get arrested and have a fine slapped on you if you can&rsquo;t overcome your shyness. </p>
<p>
	Obviously don&rsquo;t bring anything that can be considered a weapon (even instructions for an oragami crowbar) and if they find drugs on you (even cannabis) they&rsquo;ll arrest and/or fine you. </p>
<p>
	The Belgian police do use pepper spray, horses and sometimes water cannons for &lsquo;riot control&rsquo;. </p>
<p>
	If you&rsquo;re a foreigner, the police can hold you for a maximum of 24 hours without charge. The clock starts from the moment they grab you &ndash; not when you get to the police station or are put in a van. </p>
<p>
	You don&rsquo;t have to give your name at any point during your Belgian custodial experience, but if you don&rsquo;t, it&rsquo;s likely you&rsquo;ll be held for the maximum possible time while they try and find out who you are. Staying anonymous won&rsquo;t stop you from getting charged and sentenced if the police are determined to follow through on an arrest. </p>
<p>
	As they arrest you they have to tell you what it&rsquo;s for, and if you are released without charge, you&rsquo;ll be asked to sign a register that details who the arresting officer was, why they took you into custody, and the times of your arrest and release. If the details aren&rsquo;t complete or you don&rsquo;t think they&rsquo;re right, don&rsquo;t sign it. </p>
<p>
	If they do charge you, and haven&rsquo;t released you after 24hrs, it mean that a judge has issued an arrest warrant and a trial will follow. The police can hold you like this for up to 5 days, and you only get the right to a lawyer once the arrest warrant has been issued. </p>
<p>
	As standard, keep schtum and decline to answer any questions or make any statements, and don&rsquo;t sign a written statement or &lsquo;proces verbaal&rsquo; (PV) as its known in Belgium. </p>
<p>
	You have the normal rights to a translator, food and hygiene supplies and medical aid. </p>
<p>
	If they try and deport you at any point, they&rsquo;ll have to get you to sign a deportation order. Don&rsquo;t do it. You&rsquo;re protected as an EU citizen so tell them you&rsquo;re appealing on the basis of EU law. </p>
<p>
	<strong>ANYTHING ELSE?</strong> </p>
<p>
	Spread the word and get channel-hopping. Feel free to keep SchNEWS updated on how its going via the article &lsquo;comments&rsquo; feature on our website, or by email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a> </p>
<p>
	* <a href="http://www.noborderbxl.eu.org/?lang=en" target="_blank">http://www.noborderbxl.eu.org/?lang=en</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(944), 109); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(944);

				if (SHOWMESSAGES)	{

						addMessage(944);
						echo getMessages(944);
						echo showAddMessage(944);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


