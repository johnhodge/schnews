<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 721 - 7th May 2010 - Wurzel Damage</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bristol, eco-village, eviction" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 721 Articles: </b>
<p><a href="../archive/news7211.php">A Smashing Defence</a></p>

<p><a href="../archive/news7212.php">Kant Pay, Won't Pay</a></p>

<p><a href="../archive/news7213.php">Got It Sussed</a></p>

<b>
<p><a href="../archive/news7214.php">Wurzel Damage</a></p>
</b>

<p><a href="../archive/news7215.php">Deportation Iran</a></p>

<p><a href="../archive/news7216.php">Inside Schnews</a></p>

<p><a href="../archive/news7217.php">Bita Sweet</a></p>

<p><a href="../archive/news7218.php">Meltdown Town</a></p>

<p><a href="../archive/news7219.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th May 2010 | Issue 721</b></p>

<p><a href="news721.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WURZEL DAMAGE</h3>

<p>
Bristol eco-village (<a href='../archive/news719.htm'>SchNEWS 719</a>) was dramatically evicted today as bailiffs charged onto the site, toppling tripods and booting out the occupying community. <br />  <br /> On Wednesday (5th) at 9am, the site project manager arrived flanked by workers from a construction company and 15 hired heavies from Constant &amp; Co (notorious bastards  &ndash; <a href='../archive/news669.htm'>SchNEWS 669</a>).  A crowd of around 40 supporters quickly formed to join the nine villagers on site, two of whom had scaled tripods. Local residents were among the first to show, one even parking his car across the main site access while villagers were dragged out of the area. <br />  <br /> A JCB and cherry picker arrived and 5 people instantly climbed on to the cherry picker, causing the owner to promptly agree to leave. Despite efforts to prevent the JCB getting on site by climbing on to it (the digger driver responded by simply driving at anyone who came near), it pushed a mound of earth up against a building occupied by the remaining villagers, allowing the thugs in attendance to climb up and forcibly remove them from the roof. <br />  <br /> One occupant who was on a tripod on the roof received the heaviest-handed treatment. Bailiffs started to pull the poles down with him still on top, thus crushing his body between the metal. They then went on to sit on him while he was still trapped to prevent anyone filming or seeing what was going on. He was packed off in an ambulance before being released later that day with severe bruising, leg ligament damage, crutches and painkillers. <br />  <br /> The local community now plan to hold talks with the council and the owners to negotiate the site becoming a green space rather than the proposed industrial units. The eco-village organisers have pledged to help the residents in any way they can to keep the space green. <br />  <br /> The next Bristol eco-village is not far away with a site already picked out and a swoop planned for on Saturday (15th). Meet outside the Hippodrome at 11am to join the movement. <br />  <br /> * See the Facebook group: Bristol Eco Village - the Urban Centre for Alternative Technology. <br />
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(767), 90); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(767);

				if (SHOWMESSAGES)	{

						addMessage(767);
						echo getMessages(767);
						echo showAddMessage(767);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


