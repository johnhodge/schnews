<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 751 - 10th December 2010 - WikiLeaks: Hacktion stations</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, wikileaks, direct hacktion, julian assange, leaks" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 751 Articles: </b>
<p><a href="../archive/news7511.php">For Whom The Fee Tolls</a></p>

<p><a href="../archive/news7512.php">Anti-tax Dodgers High Street Bonanza</a></p>

<p><a href="../archive/news7513.php">A Hellas Of A Time</a></p>

<b>
<p><a href="../archive/news7514.php">Wikileaks: Hacktion Stations</a></p>
</b>

<p><a href="../archive/news7515.php">Schews In Brief</a></p>

<p><a href="../archive/news7516.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th December 2010 | Issue 751</b></p>

<p><a href="news751.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WIKILEAKS: HACKTION STATIONS</h3>

<p>
<p>
	<a href="../images/751-assange-lg.jpg"><img style="border:2px solid black;width: 300px; height: 197px; margin-right: 10px"  align="left"  alt=""  src="http://www.schnews.org.uk/images/751-assange-300.jpg"  /> </a>A real (but largely virtual) war is being fought over control of information and your right to hear the truth - especially any truth unpalatable as far as the ruling elites are concerned. In their guise of &lsquo;The Authorities&rsquo;, the nod for a full on assault on WikiLeaks was given sometime ago; a not-so-covert campaign to shut the website shut down and trace and punish those responsible is well underway. </p>
<p>
	Despite founder Julian Assange being arrested in the UK this week (and wouldn&rsquo;t you like to be a fly on the wall in that interview room?!), the website itself has proved harder to conquer, and a mass direct &lsquo;hacktion&rsquo; campaign of unprecedented size has booted itself up to help fight back. </p>
<p>
	Following the release of the 250,000 US diplomatic cables, and the constant drip-feed of awkward news stories flowing since, the diplomatic world has been flapping in panic. The nut-o-sphere of right-wing media and front-of-house politicians continue to froth in helpless rage at the constant parade of behind-the-scenes dirty laundry. </p>
<p>
	But backroom US government departments, and their proxies, have been stepping up their efforts by leaning heavily on anyone with connections to the web&rsquo;s whistle-blowers. </p>
<p>
	Last week, Amazon pulled the plug on its hosting of the site, while the Wikileaks domain name was also yanked off-line by Everydns.net (hosting was switched to mirror sites in Europe). </p>
<p>
	Funding was also targeted, with PayPal freezing funds and Mastercard and Visa suspending all payments to Wikileaks. Swiss bankers PostFinance shut down accounts containing defence funds for Wikileaks and founder Julian Assange. The site has also been subject to a continued barrage of Distributed Denial of Service (DDoS) attacks from &lsquo;sources unknown&rsquo;. </p>
<p>
	The only possible point to all this is presumably petty revenge and as a warning to les autres; the file containing the source data has been posted and torrent-downloaded by many thousands of people all over the planet. There&rsquo;s no keeping this embarrassing cat in the (diplomatic) bag. </p>
<p>
	With Wikileaks under siege, hacktavist group &lsquo;Anonymous&rsquo; launched &lsquo;Operation Payback&rsquo; - widely distributing software that can launch DDoS&rsquo;s of their own. This week they took aim at an array of targets including PayPal, Amazon, Sarah Palin, Visa and Mastercard (whose website was paralysed, shutting down its ability to process transactions for a time). </p>
<p>
	&lsquo;Anonymous&rsquo; describes itself as &ldquo;an anonymous, decentralised movement that fights against censorship and copywrong&rdquo;. In the past they have targeted Kiss&rsquo;s Gene Simmons for his attacks on filesharing and sinister culty weirdos the Church of Scientology. </p>
<p>
	One member said, &ldquo;If we let WikiLeaks fall without a fight then governments will think they can just take down any sites they disagree with as they wish.&rdquo; Although there has been no let up in the attacks, he said the group would now focus on methods to support Wikileaks such as mirroring the site. </p>
<p>
	&lsquo;Payback&rsquo; has also targeted the Swedish prosecution authority in protest over the strange affair of Julian Assange&rsquo;s sex-offence charges. </p>
<p>
	The allegations against Assange concern two women he had consensual sex with, one of whom accuses him of deliberately breaking a condom, the other of them having non-consensual sex after consensual sex. </p>
<p>
	The original charges of rape were thrown out last August when Swedish authorities found they were unfounded. However, as Assange was gaining renown as the public face of Wikileaks, the case was reopened last November. Two days after the release of the first diplomatic cables, the Swedish authorities issued an Interpol alert, leading to his arrest in the UK &ndash; and denial of bail. He now faces extradition to Sweden. </p>
<p>
	Protesters declaring their support for Assange gathered outside Westminster Magistrates Court for Assange&rsquo;s bail hearing on Tuesday (7th). Demos have been called against Assange&rsquo;s extradition for Monday (13th), outside the Swedish Embassy at 16.00, and for Tuesday (14th) outside Westminster Magistrates Court. </p>
<p>
	*To get a slice of the hacktion see <a href="http://www.indymedia.org.uk/en/2010/12/470215.html" target="_blank">http://www.indymedia.org.uk/en/2010/12/470215.html</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1037), 120); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1037);

				if (SHOWMESSAGES)	{

						addMessage(1037);
						echo getMessages(1037);
						echo showAddMessage(1037);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


