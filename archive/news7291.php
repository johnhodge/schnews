<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 729 - 2nd July 2010 - They Think It's All Over... ITT is Now!!!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edo, smash edo, edo decommissioners, direct action, arms trade, anti-war, brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 729 Articles: </b>
<b>
<p><a href="../archive/news7291.php">They Think It's All Over... Itt Is Now!!!</a></p>
</b>

<p><a href="../archive/news7292.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7293.php">Village People</a></p>

<p><a href="../archive/news7294.php">Community Guarding</a></p>

<p><a href="../archive/news7295.php">Calais Update</a></p>

<p><a href="../archive/news7296.php">Atenco Prisoners Free</a></p>

<p><a href="../archive/news7297.php">Fin Tuned Tactics</a></p>

<p><a href="../archive/news7298.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd July 2010 | Issue 729</b></p>

<p><a href="news729.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THEY THINK IT'S ALL OVER... ITT IS NOW!!!</h3>

<p>
<p>
	<strong>&ldquo;It&rsquo;s a real victory for the anti-war movement, The jury were presented with the facts and they supported our motivations. If people in Britain knew the truth away from media manipulations they would all support our actions&rdquo; - <em>Ornella Sabeine, EDO Decommissioner.</em></strong> </p>
<p>
	After a nail-biting twenty-four hour hiatus, the jury came to decision on the Decommissioners Case (see <a href='../archive/news721.htm'>SchNEWS 721</a> ) - 100% Not Guilty! Six of the seven defendants Tom Woodhead, Bob Nicholls, Ornella Saibene, Harvey Tadman, Simon Levin and Chris Osmond came smiling out of Hove Trial Centre at the end of a gruelling three and half week trial. Elijah Smith was remanded in custody for another offence. Five were acquitted on Wednesday afternoon, the other two Chris and Elijah had to wait until this morning. They had waited eighteen months for this moment. </p>
<p>
	The jury decisions were all completely unanimous, an indication perhaps of the depth of feeling ignited by the evidence presented of war atrocities committed in Gaza during Operation Cast Lead. </p>
<p>
	It was on the night of the 16th January 2009 that six activists broke into EDO MBM&rsquo;s manufacturing facility on Home Farm Rd Brighton (see <a href='../archive/news663.htm'>SchNEWS 663</a>). For an hour they wreaked havoc with hammers. Filing cabinets and computers were hurled from top-floor window. Machinery was also sabotaged. One of the reasons that the six had so much time in the factory was ironically that Sussex police saw a bomb in the car-park and cordoned off the area for specialists to arrive. The &lsquo;bomb&rsquo; was in fact a dummy, a prop for EDO to display at trade fairs, precision guided out of an upstairs window by the decommissioners. </p>
<p>
	The six had pre-recorded videos to be posted on Indymedia after the action (see <a href="http://www.youtube.com/watch?v=mfa8R2AxUFg" target="_blank">www.youtube.com/watch?v=mfa8R2AxUFg</a>). In his video Elijah Smith said &ldquo;I don&rsquo;t feel I&rsquo;m going to do anything illegal tonight, but I&rsquo;m going to go into an arms factory and smash it up to the best of my ability so that it cannot actually produce munitions and these very dirty bombs that have been provided to the Israeli army so that they can kill children. The time for talking has gone too far. I&rsquo;m not a writer, I&rsquo;m just a person from the community and I&rsquo;m deeply disgusted&rdquo;. <br /> 
	&nbsp;The jury requested to view those videos again before they made their historic decision ...and they obviously liked what they saw. </p>
<p>
	In effect the trial was turned around: it was EDO MBM in the shape of managing director Paul Hills who found themselves in the dock. Laughably they had come to court intending to pass themselves off as a company primarily manufacturing in-flight entertainment equipment. He was presented with a dossier of evidence painstakingly built up over the years by campaigners, which pointed firmly at the company&rsquo;s complicity in war crimes. </p>
<p>
	Paul Hills was due to be giving evidence again on Thursday after accusing a campaigner of intimidating him as a witness at the regular weekly noise demo. Funnily enough Hills wasn&rsquo;t available at the time and the court heard that he was flying to the U.S. Perhaps we should spare a thought for Paul standing right now in front of ITTs board explaining how a bunch of goddam two-bit limey punks were able to smash his factory up with hammers and walk out of court smiling. </p>
<p>
	The answer is that Paul Hill&rsquo;s evidence was more full of holes than his factory&rsquo;s windows. Here&rsquo;s just a few edited highlights of five days of his cross-examination. </p>
<p>
	Hills revealed that the company have owned the rights to the main bomb rack used on Israeli F-16s - the VER-2 - since 1998. He admitted removing website evidence of his company&rsquo;s dealings with Israel as early as 2004, the date of the first protests. He admitted having interfered with the crime scene, retrieving debris and papers, before police photographers arrived. He claimed to have police permission but no police statement backed him up. There has been speculation that &pound;189,000 is actually an underestimate of the damage caused and that more controversial evidence may have been spirited away. After being warned at one stage by the judge that he was at risk of perjuring himself if he contradicted evidence he&rsquo;d produced in earlier court cases, crucially he ended by admitting that anyone looking at the evidence presented to him in court would form the reasonable belief that his company was involved in arms sales to Israel. </p>
<p>
	It was this that the defendants needed to convince the jury of - that there was an obvious link between this factory and the bombardment of Gaza. </p>
<p>
	<a href="../images/729-decommissioners-2-lg.jpg" target="_blank"><img style="border:2px solid black;width: 300px; height: 224px; margin-right: 10px"  align="left"  alt="EDO Decommissioners"  src="http://www.schnews.org.uk/images/729-decommissioners-2-sm.jpg"  /> </a>A witness, Sharyn Lock, provided the background necessary for the jury to understand the full scope of the horror then unfolding in Gaza. Now a trainee midwife, in 2009 she was a human-rights volunteer in Al-Quds hospital, Gaza City. She was in the Gaza strip for the whole of Operation Cast Lead, and able to show footage of a missile strike on the hospital, just metres from the maternity ward. The jury also saw news reports of the white phosphorus attacks on the UNWRA compound, which incinerated much-needed food and medicine. Sharyn closed her evidence by saying she had no doubt that those who armed the Israeli Air Force &lsquo;had the blood of children on their hands&rsquo;. </p>
<p>
	After hearing of the verdict she told SchNEWS &ldquo;Brilliant news. I am so proud not only of the eight UK civilians who risked their liberty to protect fellow civilians whom they may never meet - but also of the jury who recognised that it is everyone&rsquo;s responsibility to uphold international law, even if that means decommissioning the weapons.&rdquo; </p>
<p>
	By the time this went to print the news of the verdict was spreading. Right-wing nutjob and Daily Mail journalist Melanie Phillips was first in with her considered opinion on &lsquo;the ignorance and bigotry of the judge&rsquo;. The Israeli ambassador was crying foul saying that it was &lsquo;not a great era of the British system&rsquo; (sic) - according to Israeli news source he was reportedly &lsquo;furious&rsquo; at the judges &lsquo;blatant anti-semitic stand&rsquo;. Even David Icke put it up on his website. </p>
<p>
	On the more positive side, the decommissioners were congratulated by Noam Chomsky, who said &ldquo;I would like to express my respect and admiration for those who are undertaking non-violent resistance to oppose British participation in Israel&rsquo;s cruel crimes in Gaza&rdquo;. </p>
<p>
	So what next for Smash EDO? Unable to extract any usable quotes from the after-verdict party at the Community Garden, SchNEWS spoke to an increasingly bleary eyed Andrew Beckett, press spokesman for the campaign: &ldquo;When we first started banging pots and pans outside the factory back in 2004, we never believed we&rsquo;d get anywhere like this. EDO must be reeling, their dirty laundry is now flapping out there for the whole world to see. We&rsquo;re not to going to let up the pressure on this factory - watch this space&rdquo;. </p>
<p>
	* See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> * </p>
<p>
	* <a href="http://decommisioners.wordpress.com" target="_blank">http://decommisioners.wordpress.com</a> * </p>
<p>
	* View these SchMOVIES of the trial: The opening day of the trial - <a href="http://schnews.clearerchannel.org/snippet-decoms-day-one.flv" target="_blank">http://schnews.clearerchannel.org/snippet-decoms-day-one.flv</a> * The day of the first verdicts - <a href="http://schnews.clearerchannel.org/special-report.flv" target="_blank">http://schnews.clearerchannel.org/special-report.flv</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(841), 98); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(841);

				if (SHOWMESSAGES)	{

						addMessage(841);
						echo getMessages(841);
						echo showAddMessage(841);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


