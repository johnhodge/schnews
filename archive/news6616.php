<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 661 - 9th January 2009 - Gaza Gameplan</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, gaza, palestine, genocide, ehud barak, tzipi livni, lebanon, hezbollah, idf, hamas, fatah, mahmud abbas" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 661 Articles: </b>
<p><a href="../archive/news6611.php">Eyewitness : Rafah</a></p>

<p><a href="../archive/news6612.php">Eyewitness : Gaza</a></p>

<p><a href="../archive/news6613.php">Global Outrage</a></p>

<p><a href="../archive/news6614.php">Gaza Relief Boat Rammed</a></p>

<p><a href="../archive/news6615.php">Ceasefire? What Ceasefire?</a></p>

<b>
<p><a href="../archive/news6616.php">Gaza Gameplan</a></p>
</b>

<p><a href="../archive/news6617.php">Media War</a></p>

<p><a href="../archive/news6618.php">Elementary My Dear Watson</a></p>

<p><a href="../archive/news6619.php">Inside Schnews</a></p>

<p><a href="../archive/news66110.php">Outside Schnews</a></p>

<p><a href="../archive/news66111.php">Shac Verdict</a></p>

<p><a href="../archive/news66112.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th January 2009 | Issue 661</b></p>

<p><a href="news661.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>GAZA GAMEPLAN</H3>

<p>
<b>Having promised 'a bigger shoah' (Hebrew for Holocaust) against Gaza last March</b> (by Israeli Deputy Defense Minister Matan Vilnai), the Israeli state seems to be keeping its promise. The tally of innocent dead and schools, clinics and homes destroyed might make you think that these latest and worst round of atrocities in Palestine are destruction for destruction's sake. <br />
 <br />
Just what are the Israeli's aims here? If you're watching the BBC or other equally compliant media, you'll hear that they are bombing Gaza to "stop the rockets." Gordon Brown, George Bush, whoever repeat the same line. Utter bollocks. The Hamas-Israel ceasefire stopped the rockets, and Israel broke the terms of the ceasefire by refusing to end the siege (see <a href="../archive/news617.htm">SchNEWS 617</a>). <br />
 <br />
The excuse, as much as one is needed, is the Palestinian armed groups' firing of rockets into Israeli territory. To date, the Qassam rockets, "stupid backyard cherrybomb rockets that couldn't hit the ground if gravity didn't help out", have claimed around 20 lives since the first one was fired eight years  back.  <br />
 <br />
Even if the Israelis really wanted to stop the Qassams, any military pundit worth his salt would tell you that this invasion won't work. A Qassam rocket requires no high tech parts and can be assembled in any garage or workshop. So unless the IDF plan on destroying every building in Gaza (which admittedly they might) it wont work. <br />
 <br />
If this 'war' is really an election campaign, then it's hard to imagine a bloodier one. But, if short term electoral goals are all that counts, the plan is working as far as the Israeli Labor Party is concerned. Their popularity shot up some 30-40% in the first week of the bombing. It's a chilling thought about the Israels national psyche that the route to the Knesset is through a sea of Palestinian blood. <br />
 <br />
But there's no business like Shoah-business when it comes to Israeli elections. The big three parties of the Israeli political scene are competing with each other for the genocide vote in the upcoming elections. <br />
 <br />
The frontrunners for the February elections - Ehud Barak (Labor Party boss and Defence Minister), Tzipi Livni (top dog of the ruling far-right-of-centre Kadima) and Benyamin Netenyahu (Likudnik psychopath) are competing for votes amongst an electorate that's been programmed to hate/fear Arabs. In order to win the election, they're trying to outdo each other in appearing tough on terror. What this means for the Palestinians is another genocidal war against a civilian population. <br />
 <br />
When they tried it on in Lebanon in 2006 the mighty child killers of the Israeli Defence Force were forced to run back across the border with their tails between their legs by the guerilla tactics of Hezbollah (see <a href="../archive/news552.htm">SchNEWS 552</a>). Anxious to repair the damage to their self esteem, the Israeli military and political elites have now sent the IDF into Gaza, acting in true bully tradition and picking on the weakest target they could find - the prison by the sea that is Gaza. <br />
 <br />
But for those on the ground it seems inconceivable that this amount of bloodshed can really be caused for such narrow electoral gain? This is one of the worst massacres of Palestinians since '48.  <br />
 <br />
Norman Finkelstein puts Israel's war aims this way: "<i>The goals of the Israeli government, it seems to me, are pretty clear. Number one, Israel wants to re-establish what it calls its 'deterrence capacity'. That's a technical term the Israelis use. It basically means to restore the fear of Israel among the Arabs in the region. After the defeat inflicted by Hezbollah, and the inability of Israel to launch an attack on Iran, it was almost inevitable that they would then target Hamas, because Hamas is also defying the Israeli will. According to the Israeli papers, Barak was planning the attack already before the last ceasefire, and they were just waiting for a provocation from the Palestinians.</i>" <br />
 <br />
There are other games being played here though. Diplomatic cover has been provided by the Americans as a last gift to the Middle East from the Bush Administration. Hamas has been a sore point with  the US since they arm-twisted anti-Hamas Palestinian President Mahmud Abbas into holding elections- which his Fatah party then promptly lost.  <br />
 <br />
The American game plan can be seen in the diplomatic manoeuvres being played out in the UN. Having blamed Hamas for the violence, the quisling Palestinian president has been calling for a ceasefire that includes international peacekeepers- ie. Egyptian, British and French troops (no doubt alongside other loyal US stooges such as Kuwait) to enforce the peace. <br />
 <br />
An international monitoring force on the Green Line (aka 'the border') is exactly what the Palestinians have been asking for from the international community for decades now. If it was armed and empowered to repel Israeli incursions, and if it was extended to the West Bank it might even form the basis for a lasting peace (fat chance of that under Pax Americana). <br />
 <br />
But what seems to be suggested here, along with these dubious suggestions of a 'humanitarian corridor' is that an international force will patrol inside Gaza, looking for tunnels and Palestinian weapons. Equally worrying, what's being mooted is that this 'humanitarian corridor' would be governed by by Mahmud Abbas and his Fatah goons, effectively reasserting PLO control inside Gaza under UN guard. <br />
 <br />
This would be music to Israel's ears. It would mean that the UN, EU and the rest of the international community would be directly complicit in the Occupation. Pre-empting this, the Gaza-based Popular Resistance Committees have said that any international troops would be treated as enemies invading the area. It's hard to see any country volunteering its services as policeman of Gaza. <br />
 <br />
In the forefront of every Gazan's mind however, is that the attacks are a preparation for transfer. After all, most of the population of Gaza are refugees from what's now known as Israel. The Zionist ideology has no place for Palestinians anywhere in historic Palestine, and they would like nothing better than for Gaza to just sink into the sea, or its people to leave.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=israel&source=661">israel</a>, <a href="../keywordSearch/?keyword=gaza&source=661">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=661">palestine</a>, <a href="../keywordSearch/?keyword=genocide&source=661">genocide</a>, <a href="../keywordSearch/?keyword=idf&source=661">idf</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(185);
			
				if (SHOWMESSAGES)	{
				
						addMessage(185);
						echo getMessages(185);
						echo showAddMessage(185);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>