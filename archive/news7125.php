<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 712 - 5th March 2010 - Edo: Sitting On Defence</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, iraq, anti-militarism, barclays" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 712 Articles: </b>
<p><a href="../archive/news7121.php">School Of Hard Knocks</a></p>

<p><a href="../archive/news7122.php">Honduras: Fnr-i-p</a></p>

<p><a href="../archive/news7123.php">Headless Morse-man</a></p>

<p><a href="../archive/news7124.php">Migrant Strikes</a></p>

<b>
<p><a href="../archive/news7125.php">Edo: Sitting On Defence</a></p>
</b>

<p><a href="../archive/news7126.php">Calais: A Safe Harbour?</a></p>

<p><a href="../archive/news7127.php">Ukba'stards</a></p>

<p><a href="../archive/news7128.php">A Different Bail Game</a></p>

<p><a href="../archive/news7129.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 5th March 2010 | Issue 712</b></p>

<p><a href="news712.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDO: SITTING ON DEFENCE</h3>

<p>
Good news for the EDO decommissioners (See <a href='../archive/news663.htm'>SchNEWS 663</a>) as the judge agreed to allow the factory-smashing defendants a prevention of war crimes defence. Judge Kemp took little more than a morning to decide in their favour (the hearing had been scheduled for 3 days in Lewes Crown Court), refusing to listen to the prosecution&rsquo;s attempt to prevent the war-crimes argument. <br />   <br /> This was just as well for the six modern day Luddites who went into the factory to smash it with hammers (having videoed themselves explaining why they were going to smash it up). Without a &lsquo;preventing the killing of civilians by war crimes&rsquo; defence, it would be a very short trial indeed. <br />   <br /> Obviously this somewhat unusual even handedness from a British judge has left the prosecution seeing red. Determined to prevent the defendants from justifying their actions in front of a jury, the prosecution is taking their argument to the court of High Court to overturn the decision. <br />   <br /> Chloe Marsh, Smash EDO&rsquo;s press spokesperson said, &ldquo;<em>This was an attempt by the authorities to shut down a fair trial for the defendants. There are important questions to be answered about this company&rsquo;s complicity in war crimes. We welcome the judge&rsquo;s decision and condemn these underhand attempts to prevent a jury from hearing the full facts of the case</em>.&rdquo; <br />  <br /> <strong>* To mark the seventh anniversary of the start of the Iraq war</strong>, join the Smash EDO picket outside our local Barclays Bank, on North Street, Brighton at 12pm on Wednesday March 17th. (Barclays have made a shed-load of money off the Iraq War and ITT-EDO, in case anyone needed the dots joining). <br />  <br /> * Following this, from 3pm-6pm at the <strong>Home Farm Road Factory</strong> on the same day there&rsquo;s a  &lsquo;Horrors of War&rsquo; noise demo planned. Bring noise making equipment, bloody clothes, coffins. <br />   <br /> Just because British troops have left and US troops are on their way out doesn&rsquo;t mean the Iraq war has ended. For the relatives of the 12 killed just today (a good day by Iraqi standards) in attacks directly attributable to the chaos caused by the invasion and occupation, things are a still a long way from OK. <br />   <br /> * See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> and <a href="http://decommissioners.co.uk" target="_blank">http://decommissioners.co.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-militarism&source=712">anti-militarism</a>, <a href="../keywordSearch/?keyword=barclays&source=712">barclays</a>, <a href="../keywordSearch/?keyword=iraq&source=712">iraq</a>, <a href="../keywordSearch/?keyword=smash+edo&source=712">smash edo</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(683);
			
				if (SHOWMESSAGES)	{
				
						addMessage(683);
						echo getMessages(683);
						echo showAddMessage(683);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


