<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 672 - 17th April 2009 - Too Cruel For School</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, ratcliffe-on-soar, coal, climate change, direct action, nottingham, e.on, kingsnorth, sumac centre, sneiton dale" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 672 Articles: </b>
<p><a href="../archive/news6721.php">Easy, Tiger...</a></p>

<p><a href="../archive/news6722.php">Day Of Anger</a></p>

<p><a href="../archive/news6723.php">Pathological Liars</a></p>

<p><a href="../archive/news6724.php">Right To Roma</a></p>

<b>
<p><a href="../archive/news6725.php">Too Cruel For School</a></p>
</b>

<p><a href="../archive/news6726.php">W-hacked In Jail</a></p>

<p><a href="../archive/news6727.php">Mallets Mallet</a></p>

<p><a href="../archive/news6728.php">Ray Of Blight   </a></p>

<p><a href="../archive/news6729.php">Getting On Our Tits</a></p>

<p><a href="../archive/news67210.php">Fools Gold</a></p>

<p><a href="../archive/news67211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th April 2009 | Issue 672</b></p>

<p><a href="news672.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TOO CRUEL FOR SCHOOL</h3>

<p>
<span style="font-size:13px; font-weight:bold">AS NOTTINGHAM COPS GO OVER THE TOP IN CLIMATE CLASSROOM</span> <br />
 <br />
In the very early hours of Easter Monday114 environmental activists were arrested in Nottingham on suspicion of conspiring to shut down the Ratcliffe-on-Soar coal fired power station for a prolonged period. <br />
 <br />
As was widely reported in the mainstream media, over 200 police raided a school in Sneiton Dale, Nottingham where the protesters were staying, arresting everybody inside. Cops apparently had some &#8216;intelligence&#8217; about the massive protest and had cancelled officers&#8217; leave in planning the raid. They say they found &#8216;specialist&#8217; equipment in the school that could have been used to disrupt the power station, including &#8216;equipment that could be used to tie people to machinery&#8217; (the humble bicycle D-lock?) <br />
 <br />
All those arrested were taken to various police stations in the area and released on bail in the evening without being charged. All police stations in the area were at maximum capacity, and locals were outraged that the operation involved so many officers, who they said were normally so hard to find when you needed one! <br />
 <br />
Could it be that the police wanted to avoid a repeat of the court case when Greenpeace activists were arrested at Kingsnorth Power Station in 2007 (run by E.ON, who also run Ratcliffe) &#8211; and successfully argued during a trial that they were acting lawfully to prevent damage caused by climate chaos (See <a href='../archive/news646.htm'>SchNEWS 646</a>). This way the Police nipped the protest in the bud, and can avoid any further legal scrutiny for E.ON by just dropping the charges after. <br />
 <br />
During another action in 2007, Ratcliffe was invaded by local climate activists locking onto the conveyor belts that take coal up to be burned (See <a href='../archive/news583.htm'>SchNEWS 583</a>). This power station emits more carbon dioxide than several developing countries put together, and is one of the worst sources of carbon emissions in the UK. <br />
 <br />
Police caused massive damage throughout the school, despite the activists only using one room in an annexe. They decided to search the entire building and smash all doors down &#8211; despite someone from the school with keys turning up, who was told to go away. The school is now closed till further notice &#8211; nice job by the boys in blue trashing kids education. <br />
 <br />
After the arrests, three vanloads of coppers in full gear arrived at the Sumac Centre, one of the addresses of the arrestees, which they presented a search warrant for.  A room upstairs was searched, and some papers and a computer taken. Other addresses saw police cruising the street, and they continued to monitor the Sumac Centre. <br />
 <br />
While some are inclined to speculate wildly about how the police found out about the alleged plan for the action, SchNEWS suggests that playing the blame game is the perfect way to help the cops destroy a movement, while solidarity and some sensible thinking will go a long way to successful actions. <br />
 <br />
* See also <a href="http://www.indymedia.org.uk/en/regions/nottinghamshire" target="_blank">www.indymedia.org.uk/en/regions/nottinghamshire</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=672">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=672">coal</a>, <a href="../keywordSearch/?keyword=direct+action&source=672">direct action</a>, <span style='color:#777777; font-size:10px'>e.on</span>, <a href="../keywordSearch/?keyword=kingsnorth&source=672">kingsnorth</a>, <a href="../keywordSearch/?keyword=nottingham&source=672">nottingham</a>, <span style='color:#777777; font-size:10px'>ratcliffe-on-soar</span>, <span style='color:#777777; font-size:10px'>sneiton dale</span>, <span style='color:#777777; font-size:10px'>sumac centre</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(288);
			
				if (SHOWMESSAGES)	{
				
						addMessage(288);
						echo getMessages(288);
						echo showAddMessage(288);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>