<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 680 - 19th June 2009 - Yarl&#8217;s Wood Hunger Strikers Attacked  </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, immigration, yarl's wood, serco, deportation, human rights" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 680 Articles: </b>
<p><a href="../archive/news6801.php">Cops, Lies And Videotape</a></p>

<b>
<p><a href="../archive/news6802.php">Yarl&#8217;s Wood Hunger Strikers Attacked  </a></p>
</b>

<p><a href="../archive/news6803.php">Clean Sweep</a></p>

<p><a href="../archive/news6804.php">Ofog Of War</a></p>

<p><a href="../archive/news6805.php">Darkest Peru</a></p>

<p><a href="../archive/news6806.php">Aldermast-off</a></p>

<p><a href="../archive/news6807.php">Guerilla Garden To Stay</a></p>

<p><a href="../archive/news6808.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 19th June 2009 | Issue 680</b></p>

<p><a href="news680.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>YARL&#8217;S WOOD HUNGER STRIKERS ATTACKED  </h3>

<p>
A mass hunger strike by families detained at Yarl&rsquo;s Wood refugee detention centre in Bedfordshire has been met with violent assaults on men, women and children by security guards working for Serco - who manage the prison on behalf of the UK Border Agency. The detainees started the hunger strike on Monday and since Tuesday prison managers have stopped detainees from speaking to visitors and reporters, and denied them internet access. <br />  <br /> Having collectively prevented the deportation of a family on Tuesday, all the detained families left their rooms to stage a sit-in in the corridors on Wednesday. At about 4pm, between 30 and 40 Serco goons waded in, using excessive force, to remove all men involved in the protest, separating them from the women and children. Two women, who had their clothes ripped off, were also violently removed. In the struggle, one of the women&rsquo;s child, aged 19 months, fell from her back and was stepped on by a guard, and her husband was also injured. A guard was reportedly seen filming one of the semi-naked women. Another detainee who had spoken to the press and put out a statement calling for support for the hunger strike was also violently assaulted. Doctors have not been allowed access to examine the casualties, some of which are reportedly severely injured. <br />  <br /> One of the women detainees, who had been snatched from Newcastle on Friday, reported &ldquo;<em>I have never ever seen such violence. They were beating the men like they were animals. They say if we dare to go back into the corridor they will spray us all over [with pepper spray]. We need your help from outside. We don&rsquo;t have any rights in here. We need your support from outside</em>.&rdquo; <br />  <br /> By the end of Wednesday, at least two men, two women and two children had been removed from the wing and at the time of writing their whereabouts is still unknown.&nbsp; <br />  <br /> Shamelessly, the UK Border Agency have issued a press release saying &ldquo;<em>Officers have separated a small number of detainees from the general population at Yarl&rsquo;s Wood Immigration Removal Centre. This was conducted by staff trained in conflict resolution... This separation was conducted with the utmost sensitivity and there have been no injuries to detainees</em>.&rdquo; These lies were echoed by mainstream media reports. <br />  <br /> Many Yarl&rsquo;s Wood detainees, including pregnant women, have medical conditions but don&rsquo;t receive proper medical attention, whilst many children are ill and suffer anxiety, crying and screaming all night. <br />  <br /> A lively demo with street theatre in solidarity with the hunger strikers and against immigration prisons was held outside outside Government Offices North East in Newcastle on Wednesday with another one planned for Saturday. A solidarity protest called for by No Borders London will be held today (19th) outside Serco&rsquo;s London offices at 22 Hand Court, Holborn, WC1V 6JF. <br />  <br /> * See <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a> <br />  <br /> * Also last Friday (12th), detainees rioted and lit a fire at the new Brook House detention centre at Gatwick Airport (See <a href='../archive/news669.htm'>SchNEWS 669</a>). <br />  <br /> <strong>No Border Camp</strong> <br /> <em>Calais, June 23rd-29th</em> <br />  <br /> This week of protests calls for the freedom of movement for all, an end to borders and to all migration controls - as well as highlighting the realities of the situation in Calais and Northern France for migrants. It will build links between migrants support groups, challenge the authorities on the ground, and protest against the increased repression of migrants and local activists alike.  <br />  <br /> There will be a big demo on Saturday 27th. <br /> &nbsp; <br /> * See <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=deportation&source=680">deportation</a>, <a href="../keywordSearch/?keyword=human+rights&source=680">human rights</a>, <a href="../keywordSearch/?keyword=immigration&source=680">immigration</a>, <span style='color:#777777; font-size:10px'>serco</span>, <span style='color:#777777; font-size:10px'>yarl's wood</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(366);
			
				if (SHOWMESSAGES)	{
				
						addMessage(366);
						echo getMessages(366);
						echo showAddMessage(366);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>