<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 673 - 24th April 2009 - Quick Fix</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, terrorists, bob quick, hicham yezza, students, muslims, newspapers, police, war on terror" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 673 Articles: </b>
<b>
<p><a href="../archive/news6731.php">Quick Fix</a></p>
</b>

<p><a href="../archive/news6732.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6733.php">Summit Of The Un-americas</a></p>

<p><a href="../archive/news6734.php">And Then We Take... Bil&#8217;in</a></p>

<p><a href="../archive/news6735.php">Brum Punch</a></p>

<p><a href="../archive/news6736.php">Tiger Tiger  Burning Bright</a></p>

<p><a href="../archive/news6737.php">First Past The Post</a></p>

<p><a href="../archive/news6738.php">Passing The Baton</a></p>

<p><a href="../archive/news6739.php">Rossport In A Storm</a></p>

<p><a href="../archive/news67310.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/673-obama-chavez-lg.jpg" target="_blank">
													<img src="../images/673-obama-chavez-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 24th April 2009 | Issue 673</b></p>

<p><a href="news673.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>QUICK FIX</h3>

<p>
<p><strong>AS TERROR SUSPECTS ARE RELEASED WITHOUT CHARGE.. <br />
      </strong> <br />
      Unsurprisingly enough, the twelve terror suspects arrested and detained without charge over Easter have been released. Never charged, these innocent men have been freed from the clutches of the anti-terror plod into the unwelcoming arms of the UK Borders Agency where Jacqui Smith&rsquo;s Home Office plots and schemes to have them deported back to Pakistan on the grounds that they are a &lsquo;<strong>security threat</strong>.&rsquo; <br />
         <br />
        The lawyer for the twelve, Mohammed Ayubsaid said &ldquo;<em>Our clients have no criminal history, they were here lawfully on student visas and all were pursuing their studies and working part-time. They are neither extremists nor terrorists. Their arrest and detention has been a serious breach of their human rights. As a minimum they are entitled to an unreserved apology</em>.&rdquo; <br />
         <br />
        Having been arrested slightly earlier than planned due to the &lsquo;balls up&rsquo; of Assistant Commissioner for Groundless Scaremongering, Bob Quick (the guy snapped whilst waving his &lsquo;top secret&rsquo; papers), the press worked themselves into a frenzy over a politician&rsquo;s incompetence putting British lives at risk (as if they ever would). Headlines such as &lsquo;Scramble to find the Easter bomb factory&rsquo; (The Times) gleefully leapt onto the front pages with a familiar absence of fact. Bob Quick&rsquo;s just retired on a fat pension and the ensuing hoo-ha knocked Ian Tomlinson&rsquo;s death off the front pages &ndash; nice work!! <br />
         <br />
        These &lsquo;anti terrorist operations&rsquo; are really joint exercises by the police and the media. The fact that the men had no previous convictions and didn&rsquo;t appear on any terror/crime/subversive watchlists anywhere was spun as to make them appear even more dangerous - &lsquo;clean skins&rsquo; in the spooks&rsquo; vocabulary - &ldquo;<em>highly trained, professional killers whose blameless backgrounds provide not the slightest clue as to their true, evil intent</em>&rdquo; (to quote the Terrorgraph). So lack of evidence is used as proof of wrongdoing. Looks like the people who spun the Iraq War are still getting paid. <br />
         <br />
        The twelve innocent men - who&rsquo;ve been threatened at gunpoint, imprisoned without charge and are now facing deportation - are just the latest in a long litany of repression justified as security. From the Forrest Gate shooting (no man shot by cops, no charges made), the Ricin plot (no ricin, no plotting ever happened) and the arrest of Nottingham student Hicham Yezza (downloading public domain documents - see <a href='../archive/news668.htm'>SchNEWS 668</a>), the pattern repeats time and time again - high profile arrests, persecution of immigrant communities, &lsquo;<strong>Britain under attack</strong>&rsquo; headlines. <br />
         <br />
        Using the flimsy excuse that the men were in the UK on student visas, the government wants universities to vet foreign nationals against state watch-lists and act as part of the state surveillance network against (Muslim) foreigners. Fortunately, British Universities have stood up to attempts to co-opt them Syrian-style into the state repression machinery, for now. <br />
         <br />
        The supposed &lsquo;<strong>threat to national security</strong>&rsquo; pretext for their deportation forms part of the government&rsquo;s latest strategy, &lsquo;Contest 2&rsquo; , as elaborated in The United Kingdom&rsquo;s Strategy for Countering International Terrorism. This emphasises threats from domestic terrorist threats, due to &lsquo;an extremist violent ideology associated with Al Qaeda&rsquo;. <br />
         <br />
        The policy emphasises sources of terrorism in political views. The prevention strategy will challenge &lsquo;views which fall short of supporting violence and are within the law, but which reject and undermine our shared values and jeopardise community cohesion.&rsquo; To counter that threat, Contest 2 justifies greater use of &lsquo;non-prosecution actions&rsquo;, hence various special powers to impose punishment without trial, such as house arrest, asset-freezing and deportation. <br />
         <br />
        First they came for the Muslims..... <br />
         <br />
        </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>bob quick</span>, <span style='color:#777777; font-size:10px'>hicham yezza</span>, <span style='color:#777777; font-size:10px'>muslims</span>, <a href="../keywordSearch/?keyword=newspapers&source=673">newspapers</a>, <a href="../keywordSearch/?keyword=police&source=673">police</a>, <a href="../keywordSearch/?keyword=students&source=673">students</a>, <span style='color:#777777; font-size:10px'>terrorists</span>, <a href="../keywordSearch/?keyword=war+on+terror&source=673">war on terror</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(295);
			
				if (SHOWMESSAGES)	{
				
						addMessage(295);
						echo getMessages(295);
						echo showAddMessage(295);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>