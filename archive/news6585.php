<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 658 - 5th December 2008 - Somali-argh</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, sirius star, somalia, puntland, pirates, gulf of aden, ethiopia, al shabab, islamic courts, blackwater" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 658 Articles: </b>
<p><a href="../archive/news6581.php">Koch-a-hoop</a></p>

<p><a href="../archive/news6582.php">Whack-a-mole</a></p>

<p><a href="../archive/news6583.php">Od'd On Edo</a></p>

<p><a href="../archive/news6584.php">Get It E.on</a></p>

<b>
<p><a href="../archive/news6585.php">Somali-argh</a></p>
</b>

<p><a href="../archive/news6586.php">  Tactical Sabbatical</a></p>

<p><a href="../archive/news6587.php">Spit And Polish</a></p>

<p><a href="../archive/news6588.php">Schnews In Brief</a></p>

<p><a href="../archive/news6589.php">And Finally 658</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 5th December 2008 | Issue 658</b></p>

<p><a href="news658.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>SOMALI-ARGH</H3>

<p>
<b>FROM PENZANCE TO PUNTLAND AS SchNEWS TAKES TO THE HIGH SEAS OF SOMALI PIRACY</b> <br />
 <br />
The four horsemen of civil war, occupation, famine and epidemic disease are barely enough to warrant attention from the worlds press, but, capture a ship-load of oil and suddenly the whole world's crying Somalia! Something ought to be done about the problems in Somalia. By 'something must be done,' of course, governments businesses and their loyal hangers-on (known locally as the free press) mean of course that the scourge of modern day piracy off the Horn of Africa) must be defeated by military force as it threatens the free flow of goods (especially oil) between the Middle East and Europe (via that old colonial hub, the Suez Canal).  <br />
 <br />
Free trade-friendly regimes around the world are demanding that the Somali government step in and act decisively against the pirates. One slight problem though. there is no Somali government. Well, to be more accurate, there are anywhere between six and several dozen Somali 'governments' all in various stages of war and mutual non-recognition. There was something like a Somali government, which briefly controlled Mogadishu and most of Somalia, known as the Islamic Courts Union. They were destroyed by joint US and Ethiopian fire-power in late 2006. The latest spike in piratical activity (as well as the latest round of civil war and insurgency) pretty much dates from back then (see <a href="../archive/news567.htm">SchNEWS 567</a> & <a href="../archive/news585.htm">SchNEWS 585</a> ).  <br />
 <br />
Somalia's pirates mostly operate out of the semi-autonomous Puntland region, which lies in the relatively sparsely populated north of the country. De-facto independent since 1991, Puntland has remained (mostly) free of political violence, more or less staying out of  the country's 17 year long civil war. When countries such as Britain aren't idiotically pushing for sanctions against the all but non-existent central government, they're demanding that Puntand (one of the world's poorest and weakest states) stamp down on the pirates themselves.  <br />
 <br />
Realpolitik-savvy types point out that tax on the pirates is now the Puntaland authority's main source of cash, and that the pirate gangs have access to more fire-power than the government has - and they aren't afraid to use it.  <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0"><a href="../images/658-sirius-star-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/658-sirius-star-sm.jpg"  /></a></td></tr></table>Dig a little deeper than the foreign policy wonks dare to go, you find that the Puntlanders tell a slightly different story. They tell of big-business fishing trawlers riding roughshod over their traditional fishing grounds, overfishing and intimidating locals who tried to fish in their own waters. <br />
 <br />
Just as Somali fishermen in small boats found themselves unable to compete against these giant fish factories, they found that the combination of small boats, rope ladders and Kalashnikovs (none of which are in short supply in coastal Somalia) were just the ticket for getting a bit of 'compensation' from the trawlers. From boarding fishing vessels it was just a skip and a jump to hijacking the massive cargo ships that ply the route between the Mediterranean, Middle East and South Asia.  <br />
 <br />
With no central authority to stop them, these modern day motley crews have gotten bolder and bolder as they operate on the high seas - further and further from the Somali coastline, now striking targets in the Indian Ocean and the Gulf of Aden. This year they've capture headlines with the vessels that they've taken. Back in September they captured a Ukrainian vessel full of tanks and rocket launchers, and then they topped even that a few weeks back with the largest ship ever captured by pirates, the 318,000 ton Saudi oil supertanker, the Sirrus Star. Having captured 1% of Saudi Arabia's oil output, they got to watch the value of their cargo increase as oil prices jumped up in fear.  <br />
 <br />
Not only have Western governments promised to bring their warships to face the pirates (whose little boats are anyway much more agile than those of the British, Russian or US navies), but psychotic Muslim killing mercs Blackwater have threatened to put their oar in, offering to send their ship - the McArthur and its crew of ex-navy SEALS to patrol the Gulf of Aden. Meanwhile, the pirates continue to benefit from the complete and utter chaos in the horn of Africa on the edge of the worlds' most important shipping lanes.  <br />
 <br />
<b>SHABAB RANKS</b> <br />
 <br />
The Ethiopian government, which invaded Somalia to oust the Islamic Courts two years ago (alongside the largely fictitious Transitional Federal Government) has been fighting (ie - losing) an Iraq-style insurgency ever since. Looking for a way out, the Ethiopians have been desperately trying to do deals with some of their erstwhile enemies, the Alliance for the Re-liberation of Somalia. <br />
 <br />
As far as an intervention to quell a fundamentalist movement goes, the invasion of Somalia has become an even greater failure than the invasion of Afghanistan or Iraq. Whilst the Ethiopians try to peel off the 'moderate' insurgents, the Islamic Courts Militias are taking over town after town, and now directly control most of the south of the country. Their former military wing- Al Shabab (trans: 'The Youth') seem to have split off and are championing an even stricter form of religious intolerance than the original religious intolerance of the Islamic Courts. Clashes between the two groups have been reported. The Islamists effectively run a 'night government' of Mogadishu - passing judgements, training soldiers and collecting taxes under the noses of the Ethiopians and the Transitional Government.  <br />
 <br />
As the Ethiopians plan to withdraw (perhaps the Ethiopian president has Bush's old  'Mission Accomplished' banner to wave somewhere), they're desperately seeking any semblance of non-enemy authority to hand over to. As they pull back from outlying districts, these areas are straight away recaptured by the Islamists, who bring with them their brand of peace at the cost of conformity to a strict Islam that's alien to Somalian tradition (although, just as was the case in 2006, Somalis often see this as preferable to the desperation that has been forced on them for the best part of a generation - see <a href="../archive/news567.htm">SchNEWS 567</a>). <br />
 <br />
Meanwhile, the UN warns that the population of Somalia faces 'total destitution' next year if the political and military situation does not get better next year. As the Ethiopian government has just announced that it will delay troop withdrawals, and the West is much more concerned about the passage of Saudi oil and plastic Chinese  crap past Somalia's shores, it looks like 2009 has the same mix of occupation and desperation that 2007 and 2008 had.  <br />
 <br />
* For up to date info on Somalia, check out the Mogadishu-run Shabelle Media Network (when they're not being shut down by the government that is). <a href="http://shabelle.net" target="_blank">http://shabelle.net</a>
</p>

</div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(158);
			
				if (SHOWMESSAGES)	{
				
						addMessage(158);
						echo getMessages(158);
						echo showAddMessage(158);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>