<?php

//	Test inputs
if (!isset($print) || !$print || !isset($id))	{

	echo "Exiting";
	exit;

}



//	Get Issue Data
$conn		=	mysqlConnect();
$sql		=	"	SELECT	issue, headline, date, wake_up_text, disclaimer
					FROM issues
					WHERE id = $id	";
$rs			=	mysqlQuery($sql, $conn);

if (mysqlNumRows($rs, $conn) != 1)	exit;

$issue 		=	mysqlGetRow($rs);

$issue_num		=	$issue['issue'];
$headline		=	stripslashes($issue['headline']);
$date			=	date("D, j F Y", strtotime($issue['date']));
$wake_up_text	=	stripslashes($issue['wake_up_text']);
$disclaimer		=	stripslashes($issue['disclaimer']);











//	Output Buffer
$output		=	array();

$output[]	=	"<p><i>$wake_up_text</i></p>";
$output[]	=	"<h1>SchNEWS Issue $issue_num | $date</h1>";

//	ARTICLES
$sql		=	"	SELECT headline, story, keywords
					FROM articles
					WHERE issue = $id
					ORDER BY story_number ASC	";
$rs			=	mysqlQuery($sql, $conn);
while ($article		=	mysqlGetRow($rs))	{

	$headline		=	stripslashes($article['headline']);
	$story			=	str_replace(array('<br />', '<p>', '<br>'), "\r\n", $article['story']);
	$story			=	nl2br(strip_tags($story, '<strong><em><b><i>'));
	$keywords		=	strtolower(stripslashes($article['keywords']));

	$output[]		=	"<br /><br /><br />";
	$output[]		=	"<h2>$headline</h2>";
	$output[]		=	"<p>$story</p>";
	//$output[]		=	"<p>Keywords: $keywords</p>";

}



$output[]		=	"<br /><br /><br />";
$output[]	=	"<p align='right'><i>$disclaimer</i></p><br />";


echo implode("\r\n", $output);





?>