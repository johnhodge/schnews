<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 672 - 17th April 2009 - W-hacked In Jail</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, oaxaco, mexico, david venegas, juan martinez moreno, brad will" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 672 Articles: </b>
<p><a href="../archive/news6721.php">Easy, Tiger...</a></p>

<p><a href="../archive/news6722.php">Day Of Anger</a></p>

<p><a href="../archive/news6723.php">Pathological Liars</a></p>

<p><a href="../archive/news6724.php">Right To Roma</a></p>

<p><a href="../archive/news6725.php">Too Cruel For School</a></p>

<b>
<p><a href="../archive/news6726.php">W-hacked In Jail</a></p>
</b>

<p><a href="../archive/news6727.php">Mallets Mallet</a></p>

<p><a href="../archive/news6728.php">Ray Of Blight   </a></p>

<p><a href="../archive/news6729.php">Getting On Our Tits</a></p>

<p><a href="../archive/news67210.php">Fools Gold</a></p>

<p><a href="../archive/news67211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th April 2009 | Issue 672</b></p>

<p><a href="news672.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>W-HACKED IN JAIL</h3>

<p>
Back in 2006 what started as a teachers strike in Oaxaca, Mexico, escalated into a full blown rebellion against the local power structure that lasted for five months. After enduring regular beatings, arrests and a number of disappearances and murders, the uprising was finally brought to an end by a massive military and police operation involving thousands of personnel backed by armoured vehicles and helicopters (see <a href='../archive/news567.htm'>SchNEWS 567</a>). But that wasn&#8217;t the end of the matter for those who have continued to face persecution for their involvement.  <br />
 <br />
One of the leaders of the movement, David Venegas, was arrested in 2007 and charged with possession of illegal drugs with intent to supply, sedition, conspiracy, arson, attacks on transit routes, rebellion, crimes against civil servants, dangerous attacks, and resisting arrest. Venegas beat all but the drug charge and was released on bail eleven months later. He maintains that the drugs were planted on him hours after he had been taken into custody. The case against him is so dubious that at least half of the police who participated in his arrest refuse to testify against him. <br />
 <br />
The verdict in this case was due at the start of April but, in what has been described as &#8220;harassment by delay&#8221;, it has been twice postponed. The first postponement came when Venegas failed to arrive 15 minutes early for the hearing of the process of &#8216;identification&#8217; &#8211; a process nobody had bothered to inform him about beforehand. The latest delay came on April 13th when the judge stated that he needed more time to read the dossier information. A date for the verdict has not yet been set. The process has sparked a number of demonstrations by supporters who claim the delays are part of a campaign to prevent Venegas participating in the social movement. <br />
 <br />
Venegas is not the only participant of the 2006 uprising to face persecution. Last October police arrested Juan Martinez Moreno for the murder of Indymedia journalist Brad Will (see <a href='../archive/news653.htm'>SchNEWS 653</a>). Moreno remains in prison despite a recent injunction from the Federal Judiciary declaring his imprisonment pending trial to be unconstitutional due to irregularities in the case, (irregularities that we presume include the existence of photographic and video evidence that clearly depict the real killers, a motley crew including the ex-mayor of Oaxaca and two policeman). <br />
 <br />
* <a href="http://narcosphere.narconews.com/notebook/kristin-bricker/2009/04/oaxaca-david-venegas-verdict-delayed-again" target="_blank">http://narcosphere.narconews.com/notebook/kristin-bricker/2009/04/oaxaca-david-venegas-verdict-delayed-again</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>brad will</span>, <span style='color:#777777; font-size:10px'>david venegas</span>, <span style='color:#777777; font-size:10px'>juan martinez moreno</span>, <span style='color:#777777; font-size:10px'>mexico</span>, <span style='color:#777777; font-size:10px'>oaxaco</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(289);
			
				if (SHOWMESSAGES)	{
				
						addMessage(289);
						echo getMessages(289);
						echo showAddMessage(289);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>