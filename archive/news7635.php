<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 763 - 18th March 2011 - Charity Begins Abroad</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, detention, g4s, deportation, migrants" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 763 Articles: </b>
<p><a href="../archive/news7631.php">Nuclear Power: Going Ciritical</a></p>

<p><a href="../archive/news7632.php">Clegg-stravaganza</a></p>

<p><a href="../archive/news7633.php">Dale Farm: 28 Days Later</a></p>

<p><a href="../archive/news7634.php">Corrie Anniversary</a></p>

<b>
<p><a href="../archive/news7635.php">Charity Begins Abroad</a></p>
</b>

<p><a href="../archive/news7636.php">Shopping Stewards</a></p>

<p><a href="../archive/news7637.php">Schnews Benefit</a></p>

<p><a href="../archive/news7638.php">Berk-ing Mad</a></p>

<p><a href="../archive/news7639.php">What Comes A Mound</a></p>

<p><a href="../archive/news76310.php">Losing His Wragg</a></p>

<p><a href="../archive/news76311.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th March 2011 | Issue 763</b></p>

<p><a href="news763.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CHARITY BEGINS ABROAD</h3>

<p>
<p>  
  	It&rsquo;s hard to make the imprisonment of innocent children palatable, but getting a children&rsquo;s charity on board in the whole exercise is one amazing bit of spin. After Cleggo&rsquo;s dramatic U-turn on his promise to &ldquo;end the shameful practice&rdquo; of locking up kids with new-style &lsquo;pre-departure accommodation&rsquo; such as the one planned for Pease Pottage, East Sussex (see <a href='../archive/news760.htm'>SchNEWS 760</a>), now Barnado&rsquo;s is to help oversee the &lsquo;welfare&rsquo; side. Presumably the &lsquo;welfare&rsquo; aspect of sending home families who, in many cases, fear for their lives will be one big cuddly-toy elephant in the room.  </p>  
   <p>  
  	Talking of deportation &ndash; the death of Angolan deportee Jimmy Mubenga during forced deportation last October is back in the spotlight. Security firm G4S (see <a href='../archive/news746.htm'>SchNEWS 746</a>), whose guards are thought to have caused Mubenga&rsquo;s death by suffocation through their violent restraining techniques, may face corporate manslaughter charges. There has only ever been one case of a company being convicted of this, so if you&rsquo;re holding your breath this time you might go the same way as G4S&rsquo;s victims. The company earns over &pound;600m from the government for services and, as the cuts axe keeps falling, they have been contracted for even more - including the private sector replacement for the scrapped youth unemployment schemes. And although G4S was swiftly removed from its deportation duties after the incident, it still runs three immigration removal centres.  </p>  
   <p>  
  	It seems the government likes to keep their (corporate) friends close, and their (campaigning) enemies even closer.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1146), 132); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1146);

				if (SHOWMESSAGES)	{

						addMessage(1146);
						echo getMessages(1146);
						echo showAddMessage(1146);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


