<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 691 - 17th September 2009 - Coup De Twat</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, honduras, latin america, direct action, coup" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 691 Articles: </b>
<p><a href="../archive/news6911.php">Bloody Poor Show</a></p>

<p><a href="../archive/news6912.php">Harrowing Ordeal</a></p>

<p><a href="../archive/news6913.php">Turning On The Mains</a></p>

<p><a href="../archive/news6914.php">Wind Turbulence</a></p>

<p><a href="../archive/news6915.php">Haze Of Smoke</a></p>

<p><a href="../archive/news6916.php">Back To Base-ics</a></p>

<b>
<p><a href="../archive/news6917.php">Coup De Twat</a></p>
</b>

<p><a href="../archive/news6918.php">Are You Experienced?</a></p>

<p><a href="../archive/news6919.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 17th September 2009 | Issue 691</b></p>

<p><a href="news691.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COUP DE TWAT</h3>

<p>
Last Tuesday (15th) Hondurans celebrated 188 years of freedom from the tyranny of Spanish imperial rule. The day also marked 80 days since that freedom was revoked in a military coup which deposed elected president Manuel Zelaya (See <a href='../archive/news682.htm'>SchNEWS 682</a>). <br />  <br /> Since June 28th, when the Honduran military escorted Zelaya out of the country at gunpoint, an increasingly organised resistance has been met with increasingly vicious repression. Action against the coup has taken place daily, with street protests, blockades of major transit routes and national general strikes involving over 30,000 teachers and lorryloads of police. The newly formed National Resistance Front (FOMH), representing more than 50,000 teachers, the country&rsquo;s three largest unions, and the Popular Bloc, which groups more than 10 trade unions representing over 30,000 public employees, has coordinated opposition to the coup. <br />  <br />  The Organisation of Community and Ethnic Development (ODECO), representing Honduras&rsquo; Afro-Honduran community, has also been active in demanding the return of democracy, spurred on by the blatant racism of the new regime which recently appointed disgraced former presidential candidate Rafael Pineda Ponce as &lsquo;minister of government&rsquo;. Pineda-Ponce dropped out of the 2001 campaign following comments in which he referred to Afro-Hondurans as &ldquo;monkeys hanging from trees&rdquo;. <br />  <br /> The military response has been brutal. They have killed dozens of protesters and detained over 3,500 people for their roles in demonstrations, while there have also been numerous reports of horrific rapes of women protesters. <br />  <br /> The coup regime remains internationally isolated. The US government has suspended millions of dollars in aid and last weekend revoked the US visas of leading members of the  regime and the Honduran judiciary, vocal supporters of the coup. Aid money however, has continued to filter through, not least from the Millennium Challenge Corporation, a taxpayer funded agency set up to promote free trade style development boasting Hilary Clinton on its board of directors. <br />  <br /> Internationally brokered talks between Zelaya and coup &lsquo;president&rsquo; Roberto Micheletti resulted in a proposed compromise which Zelaya eagerly accepted, despite clauses which would have reduced his powers and prevented him from even discussing the constitutional reform which led to the coup in the first place. Micheletti however, refused any agreement which would lead to the return of Zelaya, instead preferring to hang on for November elections. <br />  <br /> The coup regime could well come to regret their hardened stance. A weak and ineffectual Zelaya, grateful for the restoration of his presidential trappings, could have proved easy pickings in internationally recognised elections with all the usual opportunities for ballot stuffing and voter intimidation. Instead, they face the prospect of a sham poll, not internationally recognised and boycotted by a determined and organised opposition with a list of demands which already goes far beyond the return of Zelaya and includes an assembly for constitutional reform &ndash; the very thing they were so desperate to prevent in the first place. <br />  <br /> * See <a href="http://hondurasresists.blogspot.com" target="_blank">http://hondurasresists.blogspot.com</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=coup&source=691">coup</a>, <a href="../keywordSearch/?keyword=direct+action&source=691">direct action</a>, <a href="../keywordSearch/?keyword=honduras&source=691">honduras</a>, <a href="../keywordSearch/?keyword=latin+america&source=691">latin america</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(499);
			
				if (SHOWMESSAGES)	{
				
						addMessage(499);
						echo getMessages(499);
						echo showAddMessage(499);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>