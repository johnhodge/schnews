<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - Bad Medicine... No Remedy For UK Herbalists</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, alternative medicine" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>

<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>

<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>

<b>
<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>
</b>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<p><a href="../archive/news75511.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BAD MEDICINE... NO REMEDY FOR UK HERBALISTS</h3>

<p>
<p>
	UK users of hundreds of herbal remedies are set to be criminalised from May 1st this year as the government implements rules drawn up to comply with the 2004 EU Traditional Herbal Medicinal Products Directive (THMPD - see <a href='../archive/news697.htm'>SchNEWS 697</a>). It will make all unlicensed herbal remedies and/or supplements illegal for sale over the counter. </p>
<p>
	Assessing the rationale for doing this is more difficult than trying to puzzle out how on earth a homoeopathic remedy could actually work. On the one hand our overlords are concerned about safety and rip-off merchants. We have a completely unregulated market in people flogging any old plant-based potion (or indeed exotic animal extract &ndash; see your local Chinese herbalist) with vague promises of untested healing properties &ndash; and to a customer probably blinded by symptoms and desperate to believe in its powers, if only to benefit from a self-administered placebo effect. Yes, you can see how the unscrupulous might benefit from this - and that certain products could be harmful in longer-term ways, unrelated to the &lsquo;treatment&rsquo; area. </p>
<p>
	On the other hand, big pharma has long been pressurizing for just this kind of legislation &ndash; well, all these people trusting in nature&rsquo;s own cheap unbranded remedies, synthesised without the need for a billion dollar research lab and industrial plant, but merely passed down from generation to generation over centuries and even millennia &ndash; thus passing it&rsquo;s own giant randomised trial. Better to ban it all and trust in big pharma to keep us all safe, for a handsome profit of course. (Er, thalidomide anyone? Prozac? - or Chamomile tea and a spliff &ndash; you decide!) </p>
<p>
	<strong>NOT A CODEX MOMENT</strong> </p>
<p>
	There is an inter-country UN / WHO body that negotiates agreed standards for plant and mineral extracts for human ingestion. It&rsquo;s called Codex Alimentarius, which should surely get conspiracy hunters salivating. It&rsquo;s unaccountable meta-government, as delegations from counties supposedly thrash out voluntary agreements over food product safety and then the individual nations voluntarily abide by them. With the World Trade Organisation then recognising its findings, the Codex position quickly establishes itself as the dominant one. </p>
<p>
	As long ago as 1996 a German delegation (uninfluenced by any the large German pharamaceutical countries, no doubt) proposed no herb, vitamin or mineral should be sold for preventive or therapeutic reasons, and that supplements should be reclassified as drugs. The proposal was agreed. But protests at the time, and since, halted implementation - until recently. But the great thing about powerful interests &ndash; they can afford to sweat it out and play the long game, chipping away until resistance is spent. </p>
<p>
	<strong>WORT A CALAMITY</strong> </p>
<p>
	But either way, the EU&rsquo;s Food Supplements Directive (which closely mirrors the Codex Alimentarius Guidelines) are like using an axe where a scalpel is needed. In order to become licensed, a product must now go through an expensive process similar to those for industrial scale man-made drugs, costing up to &pound;120,000. Just about feasible for some widely taken Holland &amp; Barret type products like Echanecia, but impossible for thousands of small scale producers of a wide range of more esoteric traditional herbs and flowers sought by the more-committed herbalist. </p>
<p>
	And, worse, the Coalition government seems strangely unwilling to even allow UK herbalists the compromise built into its laws by the EU. This allows statutorily regulated herbal practitioners to continue prescribing unlicensed products. This solution is interesting cos it still allows people to be exposed to all the health risks so feared &ndash; but at least creates some kind of bureaucratic record of who to blame / attempt to punish after the event. A (wanted?) side-effect is that this approach forces everyone involved to &lsquo;join the system&rsquo;, needing to register as legit and jump through various taxing, legal, financial and social hoops - and, thus, ultimately be much more under control by the ruling elites. </p>
<p>
	Whilst elements of the EU&rsquo;s model leave a sour taste in the mouth, and bring an artificial level of complexity into an essentially simple transaction (&ldquo;Have you got any of&nbsp;<natural product=""> that I as a sane, free thinking member of the human race has decided to ingest/apply in the belief it will benefit me?&rdquo;, &ldquo;Yes&rdquo;, &ldquo;OK great thanks, cos I&rsquo;d really like some. Let me give you something to compensate you for your trouble in providing it to me...&rdquo;) it does at least offer some reassurance to those worried about malicious quackery and leaves potion-punters the chance to source their particular poison from a reputable source &ndash; although at slightly higher prices to pay for all the regulation, naturally.</natural> </p>
<p>
	<natural product="">But that&rsquo;s all hypothetical here as the Coalition (and Labour before them) have bizarrely delayed plans to introduce a statutory herbal practitioner register. This means just a few weeks until a vast range of natural everyday plants and other ingredients for herbal remedies and products become illegal to cultivate in an unpoliceable, unworkable prohibition.</natural> </p>
<p>
	<natural product="">A coalition of herbalists has er, flowered, all committed to fighting the legal jackboots trampling all over their livelihoods, but the clock is ticking and come what May? Anyone purchasing or selling herbal remedies could be a criminal and punters will be left meeting black-market dealers in dodgy pub car parks trying to score a wrap of Valerian and a ten bag of Pokeweed...</natural> </p>
<p>
	<natural product="">Learn more at: <a href="http://www.naturalnews.com/030873_EU_directive_medicinal_herbs.html#ixzz1AG1RpBEk&nbsp;</natural>" target="_blank">http://www.naturalnews.com/030873_EU_directive_medicinal_herbs.html#ixzz1AG1RpBEk&nbsp;</natural></a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1069), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1069);

				if (SHOWMESSAGES)	{

						addMessage(1069);
						echo getMessages(1069);
						echo showAddMessage(1069);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


