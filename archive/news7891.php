<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 789 - 23rd September 2011 - Flying the Flag</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, right to protest, nick clegg" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 789 Articles: </b>
<b>
<p><a href="../archive/news7891.php">Flying The Flag</a></p>
</b>

<p><a href="../archive/news7892.php">Egypt: Sheikhing The System</a></p>

<p><a href="../archive/news7893.php">Too Late To Dale Out</a></p>

<p><a href="../archive/news7894.php">Glue-me Retail Outlook</a></p>

<p><a href="../archive/news7895.php">Dogs Of War</a></p>

<p><a href="../archive/news7896.php">Sparks Fly</a></p>

<p><a href="../archive/news7897.php">Rip: Troy Anthony Davis</a></p>

<p><a href="../archive/news7898.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd September 2011 | Issue 789</b></p>

<p><a href="news789.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FLYING THE FLAG</h3>

<p>
<p>  
  	<strong>AS BANNER-DROP PROTESTER JAILED DURING LIB-DEM CONFERENCE</strong>  </p>  
   <p>  
  	For dropping a banner off a bridge outside the this week&rsquo;s Libdem conference, one protester is now serving 10 days on remand at HMP Birmingham. Ed Bauer and two fellow activists from the&nbsp; National Campaign Against Fees and Cuts (NCARC) unfurled a banner with the straight and to the point message: &ldquo;<em><strong>Traitors Not Welcome: Hate Nick Clegg Love NCAFC</strong></em>&rdquo;.  </p>  
   <p>  
  	The three were nicked immediately afterwards on the flimsy pretext that &ldquo;debris fell into Broad Street creating a hazard to pedestrians and motorists&rdquo;. The worst they and their mates expected was to be nicked and maybe spend the night in the cells. Instead, after all spent a weekend in custody, Ed was singled out and sent straight to prison by the disctrict judge.  </p>  
   <p>  
  	Because Ed is also awaiting trial for the Fortnam and Mason occupation (although not breaking any bail conditions), the judge said that he could not be satisfied that he would not commit further offenses, obviously reckoning him a protester needing to be made an example of. An act which would under normal circumstances would maybe attract an on the spot fine has has become the pretext for detention without trial.  </p>  
   <p>  
  	Michael Chessum of the NCAFC and National Union of Students National Executive Committee said, &ldquo;<em>It is appalling that students taking part in peaceful protest are being victimised in this way. It is ludicrous that anyone would be remanded in custody for a minor traffic charge &ndash; and it&rsquo;s clear that the behaviour of the police and the court is an attempt to intimidate and muzzle protests against the Liberal Democrats&rsquo; betrayal of education. Whether it&rsquo;s kettles, intimidation, or tactical charges &ndash; it is becoming increasingly difficult for students and young people to say that they have a meaningful right to protest</em>.&rdquo;  </p>  
   <p>  
  	Friends and supporters have been totally shocked by the courts&rsquo; decision. But, post riots, this has become the norm. Unfurling a banner is exactly the sort of right enshrined in the Human Rights Act, plain old freedom of expression without any intention to disrupt, let alone damage or steal.  </p>  
   <p>  
  	And it was Nick Clegg the arch-traitor himself who was banging on about the importance of the human rights act during his boring conference speech, while Ed languisihes behind bars for trying to exercise that right to critisise the Liberal Democrats.  </p>  
   <p>  
  	And Ed, in fact, was only doing what he had been elected to do as Vice President of Education for&nbsp; Birmingham University&rsquo;s Guild of Students (a posh name for their Student Union). As fellow protester Alice Swift explains, &ldquo;<em>He was elected on the back of promising to be a campaigning Vice President of Education and to actively fight against the unfair and unnecessary rise in tuition fees. Students like me voted for Ed to have a voice in the debate surrounding the attack on our education and it would be a disgrace if University management were to sack an elected student representative. I would be distraught to see him sacked and I know so many more students would be too</em>.&rdquo;  </p>  
   <p>  
  	A solidarity group has been formed on Facebook- Banner Drops Are Not A Crime, and Twitter&nbsp; #BannerNotACrime. Their spokesperson said this to ScNEWS:  </p>  
   <p>  
  	&ldquo;<em>The district judge felt that it is the place of the courts to prevent people from engaging in criticism of their own government by locking them up, before trial. It is a disgusting attack on civil liberties, the right to free speech and peaceful protest.</em>&rdquo;  </p>  
   <p>  
  	A solidarity demo for Ed and and his co-defendants will be held outside Birmingham Magistrates Court, where they are due to appear in back this coming Monday the 26th between 3 and 6pm, on Commercial Street.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1364), 158); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1364);

				if (SHOWMESSAGES)	{

						addMessage(1364);
						echo getMessages(1364);
						echo showAddMessage(1364);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


