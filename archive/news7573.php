<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 757 - 4th February 2011 - Mubarak's Attacks Causing Chaos in Cairo</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, egypt, protest, riots, mubarak" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 757 Articles: </b>
<p><a href="../archive/news7571.php">Cut To The Quick...</a></p>

<p><a href="../archive/news7572.php">High Street Tax Maniacs</a></p>

<b>
<p><a href="../archive/news7573.php">Mubarak's Attacks Causing Chaos In Cairo</a></p>
</b>

<p><a href="../archive/news7574.php">Don't Mention The Wall</a></p>

<p><a href="../archive/news7575.php">Positive Schnews</a></p>

<p><a href="../archive/news7576.php">Behind The Irony Curtain</a></p>

<p><a href="../archive/news7577.php">On The Hoof</a></p>

<p><a href="../archive/news7578.php">Grow To Like It</a></p>

<p><a href="../archive/news7579.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th February 2011 | Issue 757</b></p>

<p><a href="news757.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MUBARAK'S ATTACKS CAUSING CHAOS IN CAIRO</h3>

<p>
<p>
	Protests in Egypt have taken a violent turn this week as Mubarak&rsquo;s heavies were sent out on to the streets to provoke peaceful demonstrators, giving the military no choice but to wade in. After a small crowd of Mubarak supporters managed to get into Tehrir Square, scenes of utter chaos ensued. </p>
<p>
	Pro-Mubarak demonstrators on camels and horses charged at the gathered crowds and volleys of stones quickly started to fly between the warring factions. Police and the army then had sufficient excuse to step in and attempt to separate the groups, using tear gas and rubber bullets. Violence in the capital Cairo has been escalting and continues as we go to press. </p>
<p>
	Until now, the resistance movement has seemed to emerge as a peaceful, non-hierarchical, grassroots campaign with no clear leadership or autocratic direction. The government supporters however have been suspiciously well-equipped with professionally sign painted banners, organised transport and ready for a fight. Reports have been made of people being ordered to demonstrate for Mubarak under threat of losing their wages, and Vodafone were apparently &lsquo;forced&rsquo; to send out random pro-government propaganda texts. </p>
<p>
	This is the latest in a string of attempts by the troubled president to regain control, which have included imposing curfews, pulling the plug on internet and mobile phone networks, stopping public transport and appointing ex-Military Inteligence CIA-toruture camp stooge Omar Suleiman to vice-president to oversee the strongarm dirty tricks campaign. </p>
<p>
	While rolling coverage of the dissent in Egypt has been top of the agenda of all major news media over the last week, SchNEWS was hoping to get our own exclusive eye-witness account from an intrepid reporter who missioned out there to get a slice of the action earlier this week (in solidarity, not as a regime-change goal hanger). </p>
<p>
	However, having just stepped out of the airport, our man on the inside was instantly kidnapped by Egyptian police, who have seemingly been rounding up any foreign-looking types and journalists and detaining them - &lsquo;informally&rsquo;. </p>
<p>
	Mainstream jouno&rsquo;s of many nationalities have been stopped, blocked or detained by plain clothes thugs, with suffering beatings and worse. With disinformed fear of foreigners spreading wildly, anyone too Western-looking is now a target, with eye-witness accounts all over the tweet-a-sphere. Thankfully our plucky wanderer has now been released and is &lsquo;relatively unharmed&rsquo;. </p>
<p>
	The Egyptian Health Ministry reports 13 people dead and over 1300 injured, but who knows how close to the real figures these estimates are. The unrest shows no sign of stopping &ndash; by the time you read this all hell could well have broken loose with hundreds of thousands due out on the streets for Friday&rsquo;s protests, dubbed the &lsquo;Day of Departure&rsquo; by the protesters. Mubarak and company&rsquo;s desperate ploy to cling on to power by stirring up chaos and bloodshed, before trying to ride in with the military to suppress it all means it does look like its all going to turn even uglier </p>
<p>
	* See All Media </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1089), 126); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1089);

				if (SHOWMESSAGES)	{

						addMessage(1089);
						echo getMessages(1089);
						echo showAddMessage(1089);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


