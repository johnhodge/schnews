<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 741 - 1st October 2010 - Tek The Biscuit</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, festivals, squatting, raves, wales" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 741 Articles: </b>
<p><a href="../archive/news7411.php">Unrest For The Wicked</a></p>

<p><a href="../archive/news7412.php">No Borders Update: Bruss Versus Them</a></p>

<p><a href="../archive/news7413.php">Edls On Wheels</a></p>

<p><a href="../archive/news7414.php">Schnewsflash: Ecuador</a></p>

<p><a href="../archive/news7415.php">Pump Action</a></p>

<p><a href="../archive/news7416.php">Cheeky Minks</a></p>

<p><a href="../archive/news7417.php">Inca-hoots</a></p>

<p><a href="../archive/news7418.php">Siege Of Gaza: Update</a></p>

<b>
<p><a href="../archive/news7419.php">Tek The Biscuit</a></p>
</b>

<p><a href="../archive/news74110.php">Happen-stance</a></p>

<p><a href="../archive/news74111.php">Amsterdam-busters</a></p>

<p><a href="../archive/news74112.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st October 2010 | Issue 741</b></p>

<p><a href="news741.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TEK THE BISCUIT</h3>

<p>
<p>
	Today (1st) sees the preliminary hearing in Haverfordwest Magistrates Court for those arrested and charged during the bust of the UK Teknival in Pembrokeshire this May (see <a href='../archive/news727.htm'>SchNEWS 727</a>) &ndash; and the numbers answering charges has risen dramatically from six to as many as seventeen. </p>
<p>
	The case has been deemed too complex for the local magistrates to deal with alone, so it&rsquo;s now going before a district judge &lsquo;for direction&rsquo;. What this means exactly for those charged (apart from yet another long and expensive journey to Wales) isn&rsquo;t yet clear, but a top UK defence lawyer has warned that Haverfordwest is a &lsquo;kangaroo court&rsquo;, and says, &lsquo;there&rsquo;ll be no justice there&rsquo;. </p>
<p>
	Justice is looking pretty thin on the ground for one of the defendants in particular, who will have to appear in court without any legal representation whatsoever. It turns out that the local Haverfordwest solicitor he&rsquo;d got to help fight his case neglected to process his claims for legal aid in time, despite them being submitted months ago. Effectively this means that his case can&rsquo;t go to a decent solicitor who is an expert in the field. Without fair representation, it has much less chance of being thrown out of court. </p>
<p>
	With already draconian licensing laws &ndash; and worsening - it&rsquo;s becoming harder to hold gatherings and create festivals and entertainment away from the confines of the commercial mainstream. </p>
<p>
	Those on charges face heavy fines or even imprisonment &ndash; and were already plunged into great debt by costs incurred when the Teknival was busted and closed down, wirh gear confiscated and vehicles impounded. The main trial is planned for November. Watch this space for developments and join the Facebook group &lsquo;Drop the Charges over UKTek&rsquo; to show your support. </p>
<p>
	* This Saturday (2nd) there will be a big benefit night in Bristol to raise funds to support the UKTek arrestees. A mass of 15 soundsystems will join forces at the Lakota, 6 Upper York St, Bristol, for at night of thumping dance music from jungle and techno to trance, 10pm-6am, &pound;10 on door. </p>
<p>
	* See <a href="http://www.facebook.com/event.php?eid=141577695869973&amp;ref=ts" target="_blank">www.facebook.com/event.php?eid=141577695869973&amp;ref=ts</a> <a href="http://www.dalerave6.co.cc" target="_blank">www.dalerave6.co.cc</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(959), 110); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(959);

				if (SHOWMESSAGES)	{

						addMessage(959);
						echo getMessages(959);
						echo showAddMessage(959);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


