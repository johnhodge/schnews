<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 751 - 10th December 2010 - For Whom the Fee Tolls</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, students, cuts, austerity, riots" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 751 Articles: </b>
<b>
<p><a href="../archive/news7511.php">For Whom The Fee Tolls</a></p>
</b>

<p><a href="../archive/news7512.php">Anti-tax Dodgers High Street Bonanza</a></p>

<p><a href="../archive/news7513.php">A Hellas Of A Time</a></p>

<p><a href="../archive/news7514.php">Wikileaks: Hacktion Stations</a></p>

<p><a href="../archive/news7515.php">Schews In Brief</a></p>

<p><a href="../archive/news7516.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th December 2010 | Issue 751</b></p>

<p><a href="news751.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FOR WHOM THE FEE TOLLS</h3>

<p>
<p>
	<strong>THE DEBT GENERATION FIGHTS BACK AS PARLIAMENT VOTES ON FEES</strong> </p>
<p>
	The wave of student protests that sprang forth so vehemently on 11/11/10 faced its day of reckoning this Thursday (9th) as MPs huddled in Parliament to decide on the future of education. Chaos was on the menu as the tuition fees bill passed with a majority of 21 votes. </p>
<p>
	The crowds started congregating at 12 noon by the University of London Union in Malet Street. The march stalled to hear speeches and bold declarations such as &ldquo;We will not be detained, constrained and kettled again!&rdquo; just before heading towards Parliament Square. </p>
<p>
	At 12.45 the march crossed the starting line, and police tried to nip in early by containing the protesters at the first turn, helped by stewards linking arms to control the crowd - but alas the kids gave them the two finger salute and ran through the whole lot. &ldquo;<em>Today we won&rsquo;t be kettled because today we are prepared</em>,&rdquo; as one protester put it. </p>
<p>
	There were scores of police lining each street and batches of riot police waiting round each corner. Cops tried to run in front and form a line but the youth ran faster and by 1.15 they had descended on Trafalgar square in numbers, accompanied by the sounds of a salsa band. </p>
<p>
	Protesters were stopped there but soon managed to break through. The FIT team was shoved out the way by a rebellious posse. At this point billows of coloured smoke surged through the air as the demonstrators released flares and bangers. The feisty atmosphere became charged as people reached the corner by Westminster Abbey. The harris fencing surrounding the green got ripped apart and waves of the disgruntled started cascading onto the legion of riot police protecting the finishing line, batons at the ready. </p>
<p>
	Hundreds more police started approaching the square and each road leading off from it was filled with a dozen riot vans. Flares, sticks, snooker balls and paint balls flew across the square, placards were set on fire as students hollored &ldquo;Fuck the cuts!&rdquo; The protesters started using barriers to break through police lines. Scuffles erupted, leaving one officer with a bloody nosy while another got sent to hospital with a broken leg. By 3.45pm a containment was in place with peaceful demonstrators allowed to leave the space, but for many that was easier said than done. </p>
<p>
	As the afternoon wore on a large sound system was in place and the party started really bouncing &ndash; to a backdrop of hardcore, protesters and riot police having it out on the street. The focus then turned towards the Commons as voting time approached. In the distance an outline of police horses could be seen standing ominously in wait as dusk set in. The police line flowed back and forth as people kept trying to break through their lines. </p>
<p>
	After the vote, angry protesters rampaged through the government quarter, smashing telephone booths, vandalizing statues and breaking government building windows. </p>
<p>
	By 6pm reports were coming through of many people getting injured as police tactics took a turn towards the oppressive. Police marched through docile crowds and shoved them violently backwards. The horses were released towards the crowd. Two demonstrators were knocked unconscious, another demonstrator on a wheelchair was dragged away by police; minutes later the wheelchair was seen lying empty. A medical steward passed on he treated at least 10 serious head injuries from marchers being hit with police batons. </p>
<p>
	The next generation of possible voters then turned their attention to the Treasury building, home to the Chancellor George Osborne, and let loose. While contained inside the square, students started using concrete blocks and metal poles to smash windows of the building on Great George Street before the riot crew got in and reclaimed the property. Windows of the supreme court were also put in. </p>
<p>
	Protests spread to the West End with demonstrators breaking shop windows in Oxford Street. Police eventually surrounded a group of around 150-200 people in Trafalgar Square, where fires were lit, graffiti daubed on statues and missiles thrown at riot police. They then moved on towards Marble Arch, attacking two Vodafone stores along the way. </p>
<p>
	Elsewhere, at around 7.30pm, an angry mob attacked a vehicle they thought contained a mediocre Tory candidate; instead they managed to hit the negative media coverage jackpot by coming face to face with Prince Charles and his bit on the side. </p>
<p>
	The stately window was cracked and the royal buggy was splashed with paint. Cue the media&rsquo;s defining image. </p>
<p>
	By 9pm the protest seemed to be dwindling. Police attempted to clear Parliament square by reopening Westminster Bridge to the South Bank. They had apparently been letting protesters out one by one but felt the area (perhaps unsurprisingly) was not clearing quickly enough. The police made 22 arrests in total with scores injured. There were also protests in Leeds, Sheffield, Edinburgh, Liverpool, Belfast, Brighton, Manchester and Bristol, with school pupils joining students. </p>
<p>
	* See <a href="http://www.anticuts.com" target="_blank">www.anticuts.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1034), 120); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1034);

				if (SHOWMESSAGES)	{

						addMessage(1034);
						echo getMessages(1034);
						echo showAddMessage(1034);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


