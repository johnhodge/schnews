<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 766 - 8th April 2011 - Party & Protest April Roundup</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://brightonmayday.wordpress.com" target="_blank"><img
						src="../images_main/765-brighton-mayday-banner.png"
						alt="Mayday Mass Party & Protest Brighton 30th April 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 766 Articles: </b>
<p><a href="../archive/news7661.php">House Demolition</a></p>

<p><a href="../archive/news7662.php">Rich Pickings</a></p>

<p><a href="../archive/news7663.php">Sick To The Gills</a></p>

<p><a href="../archive/news7664.php">Caught Off Gard</a></p>

<b>
<p><a href="../archive/news7665.php">Party & Protest April Roundup</a></p>
</b>

<p><a href="../archive/news7666.php">Zero Eviction Day</a></p>

<p><a href="../archive/news7667.php">Getting Their Fill</a></p>

<p><a href="../archive/news7668.php">Quaking In Their Boots</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 8th April 2011 | Issue 766</b></p>

<p><a href="news766.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PARTY & PROTEST APRIL ROUNDUP</h3>

<p>
<p>  
  	<strong><em>The Party &amp; Protest listings are what SchNEWS is all about: getting people off their arses and becoming involved. Here&rsquo;s a round-up for April...</em></strong>  </p>  
  <p style="text-align: center"> 
  	<strong>AUSTERITY / ANTI CUTS</strong>  </p>  
   <p>  
  	&nbsp;* Following the successful occupation the week before,<strong> Trafalgar Square</strong> will be re-taken for 24 hours from 6.30pm on April 9th <a href="http://occupytrafalgarsquare.wordpress.com" target="_blank">http://occupytrafalgarsquare.wordpress.com</a>  </p>  
   <p>  
  	* In protest against<strong> Housing Benefit </strong>cuts and Westminster Council criminalising sleeping on the street or handing out free food, there will be a demo at Westminster City Hall, 64 Victoria Street, London, SW1E 6QP on April 14th 5pm-9pm. <a href="http://benefitclaimantsfightback.wordpress.com" target="_blank">http://benefitclaimantsfightback.wordpress.com</a>  </p>  
   <p>  
  	<strong>* UK Uncut demos continue all this month</strong> &ndash; targeting tax dodging by corporations and the rich, estimated to cost the state $95bn a year. April 9th - <strong>Bath, Bristol, Manchester, Nottingham, Sheffield, Tonbridge, Tunbridge Wells, York</strong>; April 14th &ndash;<strong> London</strong>; April 16th &ndash; <strong>Edinburgh, Glasgow, London;</strong> April 23rd - <strong>Burnley, Newcastle, Sheffield</strong> (TBC); April 30th &ndash; <strong>Brighton</strong> (see below), <strong>Leeds</strong>; May 1st &ndash;<strong> Oxford</strong>. For full details see <a href="http://www.ukuncut.org.uk/actions/list" target="_blank">www.ukuncut.org.uk/actions/list</a>  </p>  
   <p>  
  	* On the <strong>3rd National Day Of Protests Against Benefits Cuts</strong>, April 14th: <strong>Leeds</strong> meet 11am at Leeds train station for 11.30am demo at Atos Origin, 1 Whitehall, LS1 4HR then to A4e, Zicon House, Wade Lane, LS2 8DD <a href="http://www.facebook.com/event.php?eid=155593464493862" target="_blank">www.facebook.com/event.php?eid=155593464493862</a> &ndash; <strong>Brighton</strong> 2pm-5pm Churchill Sq <a href="http://brightonbenefitscampaign.wordpress.com" target="_blank">http://brightonbenefitscampaign.wordpress.com</a> &ndash; <strong>London</strong> protest against the Daily Mail and their knee-jerk reactionary politics, in this case their lies about people on benefits. 2pm-5pm, Daily Mail Headquarters, Young Street, London, W8 5TT <a href="http://benefitclaimantsfightback.wordpress.com" target="_blank">http://benefitclaimantsfightback.wordpress.com</a> &ndash; <strong>Bristol</strong> 12pm-5pm Bristol Government Buildings, Flowers Hill, Bristol BS4 5LA <a href="http://www.bristolanticutsalliance.org.uk" target="_blank">www.bristolanticutsalliance.org.uk</a> - <strong>Poole</strong> outside Jobcentre, Dear Hay Lane, Poole, 12pm-2pm <a href="http://www.facebook.com/event.php?eid=161332900587762" target="_blank">www.facebook.com/event.php?eid=161332900587762</a>  </p>  
   <p>  
  	<strong>* Mayday Brighton: mass party &amp; protest</strong>, April 30th 12 noon &ndash; location TBA. Not just a demo against the cuts, bringing together UK Uncut, as well as Critical Mass, No Borders, South Coast Climate Camp, Anti-Fascists, Smash EDO, Hunt Sabs and more &ndash; going for a whole range of targets. <a href="http://brightonmayday.wordpress.com" target="_blank">http://brightonmayday.wordpress.com</a>  </p>  
  <p style="text-align: center"> 
  	<strong>ANTI WAR / ARMS TRADE</strong>  </p>  
   <p>  
  	* The <strong>Faslane Peace Camp</strong> is holding a grand re-opening with a weekend of gardening, workshops and of course some protesting over the weekend April 8th-10th. Faslane Peace Camp, Shandon Helensburgh, G84 8ND <a href="http://www.facebook.com/pages/Faslane-Peace-Camp/10143429717" target="_blank">www.facebook.com/pages/Faslane-Peace-Camp/10143429717</a>  </p>  
   <p>  
  	<strong>* Shut Down Heckler &amp; Koch</strong> will picket the H&amp;K office in Nottingham on April 11th 4pm at the gates of Easter Park, Lenton Lane, Nottingham NG7 2PX (regular event held on second Monday of every month) <a href="http://www.shutdownhk.org.uk" target="_blank">www.shutdownhk.org.uk</a>  </p>  
   <p>  
  	* <strong>Disarm DSEi</strong> will confront the <strong>Counter Terror Expo</strong> - an event showcasing the latest equipment used by states to spy on, restrict and repress citizens, supplying despotic regimes the world over &ndash; April 19th 12 noon at Grand &amp; National Hall, Olympia, Hammersmith Road, W14 8UX <a href="http://www.dsei.org/counter-terror-2011" target="_blank">www.dsei.org/counter-terror-2011</a>  </p>  
   <p>  
  	* <strong>Smash EDO Noise Demo</strong> - every Wednesday 4pm-6pm outside EDO on Home Farm Road, Brighton BN1 9HU <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>  </p>  
  <p style="text-align: center"> 
  	<strong>ANIMAL RIGHTS</strong>  </p>  
   <p>  
  	* Demo against the<strong> Grand National</strong> and horseracing in general, where nearly 400 horses are killed annually. Meet April 9th 11.30am outside main entrance to Aintree Racecourse, Merseyside on A59, opposite train station. <a href="http://www.faace.co.uk" target="_blank">www.faace.co.uk</a>  </p>  
   <p>  
  	*<strong> National Rally for Animals in Laboratories,</strong> calling for end to all animal experiments. 12 noon, April 16th, Whitworth Park, Manchester <a href="http://www.wdail.org" target="_blank">www.wdail.org</a>  </p>  
  <p style="text-align: center"> 
  	<strong>POLICE / SURVEILLANCE</strong>  </p>  
   <p>  
  	&ldquo;<strong>Standing up to Surveillance&rdquo; - A Network for Police Monitoring Conference</strong>. With increased state surveillance in protest groups and minority communities &ndash; eg filming people at demos, police databases, infiltration into protest movements and more - this conference will hear from people personally and politically affected by this. April 17th, 10.30am-5pm at Rich Mix, 35&ndash;47 Bethnal Green Road, London E1 6LA. &pound;5/&pound;10 <a href="http://standinguptosurveillance.wordpress.com" target="_blank">http://standinguptosurveillance.wordpress.com</a>  </p>  
  <p style="text-align: center"> 
  	<strong>COMMUNITY ACTION</strong>  </p>  
   <p>  
  	<strong>* Squatathon</strong> - Tory plans to make squatting illegal could make 25,000 people homeless in London alone. There is currently around 250,000 empty properties in Britain - with more to come as the financial crisis bites. April 17th, 10am-1pm &ndash; location TBA see <a href="http://www.facebook.com/event.php?eid=154118774647735" target="_blank">www.facebook.com/event.php?eid=154118774647735</a>  </p>  
   <p>  
  	*<strong> Common Place Social Centre </strong>in Leeds is closing down after six years. There will be a weekend of events April 23rd-24th including a gig on Saturday night and films on Sunday. Plus - a &lsquo;What&rsquo;s Next?&rsquo; meeting at The Common Place 5pm 17th April. 23-25 Wharf St, Leeds, LS2 7EQ <a href="http://www.thecommonplace.org.uk" target="_blank">www.thecommonplace.org.uk</a>  </p>  
   <p>  
  	*<strong> Critical Mass</strong> - reclaiming city centres from cars with a mass bike-ride, April 29th and every last Friday of the month - typically at around 5pm. For locations see <a href="../pap/guide.php#crit-mass" target="_blank">www.schnews.org.uk/pap/guide.php#crit-mass</a> but check local site to confirm.  </p>  
  <p style="text-align: center"> 
  	<strong>ECOLOGICAL DIRECT ACTION</strong>  </p>  
   <p>  
  	* <strong>Titnore Camp </strong>tat-down weekend. The five year old camp has successfully saved the woodland in West Durrington, West Sussex from the axe. Climbers particularly welcome. All weekend April 9th-10th. <a href="http://www.protectourwoodland.co.uk" target="_blank">www.protectourwoodland.co.uk</a>  </p>  
  <p style="text-align: center"> 
  	<strong>NUCLEAR</strong>  </p>  
   <p>  
  	* <strong>Sizewell Nuclear Power Station</strong> will be targeted with a weekend &ndash; April 22nd-25th &ndash; of workshops, skill-shares, woodland and beach walks, vegan food and demo on the 23rd 12noon. Marking the 25th anniversary of Chernobyl. Sizewell IP16 4UE - on Suffolk coast 8 miles from the A12. <a href="http://www.sizewellcamp.org.uk" target="_blank">www.sizewellcamp.org.uk</a>  </p>  
  <p style="text-align: center"> 
  	<strong>ROYAL WEDDING</strong>  </p>  
   <p>  
  	<strong>Not The Royal Wedding Street Party</strong> &ndash; celebrating people-power rather than inherited privilege, with food, stalls and entertainment &ndash; April 29th, from 11.30am, Earlham Street, London WC2 <a href="http://www.republic.org.uk" target="_blank">www.republic.org.uk</a> Look on Party &amp; Protest closer to the date for other events and protests during the royal wedding.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1176), 135); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1176);

				if (SHOWMESSAGES)	{

						addMessage(1176);
						echo getMessages(1176);
						echo showAddMessage(1176);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


