<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 753 - 7th January 2011 - Bolivia: Losing the Morales Highground </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bolivia, evo morales" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 753 Articles: </b>
<p><a href="../archive/news7531.php">Csi Palestine</a></p>

<p><a href="../archive/news7532.php">Reading And Rioting</a></p>

<p><a href="../archive/news7533.php">In Pod We Trust</a></p>

<p><a href="../archive/news7534.php">Lean Dean Fighting Machine</a></p>

<b>
<p><a href="../archive/news7535.php">Bolivia: Losing The Morales Highground </a></p>
</b>

<p><a href="../archive/news7536.php">Getting Off Soot Free</a></p>

<p><a href="../archive/news7537.php">Holloway Hootenanny</a></p>

<p><a href="../archive/news7538.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th January 2011 | Issue 753</b></p>

<p><a href="news753.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BOLIVIA: LOSING THE MORALES HIGHGROUND </h3>

<p>
<p>
	When the government of Evo Morales declared its Christmas present to the Bolivian people would be an 70-80% leap in fuel prices, the public&rsquo;s furious response made him glad he had kept the receipt. </p>
<p>
	The backlash began when a strike by bus and taxi drivers set to have their livelihoods savaged by the price hike brought La Paz shuddering to a halt. The counter movement gathered pace as demonstrators took to the streets in La Paz and El Alto, Cochabamba, Santa Cruz, Potosi and Oruro. The protests quickly turned raucous as fuming demonstrators showered police and government buildings with stones and erected burning barricades. In the clashes that followed, 15 coppers were injured as police tried to clear the streets with tear gas. </p>
<p>
	Morales and chums&rsquo; plan was to put an end to the fuel subsidies which put a 6 year freeze on prices in the poverty wracked country. The government claimed that not only did the subsidies cost them $380 billion a year but also that every year $150 billion worth of fuel was smuggled out of the country and flogged in neighbouring countries where prices are much higher. However, the measures would have had a disastrous impact on the millions of Bolivians living below or around the poverty line as transport and food costs were set to rocket. The announcement of the plan had an immediate impact with runs on supermarkets and banks. </p>
<p>
	With more strikes and protests on their way and unions and social movements about to wade in, Morales addressed the people - many of them the same feisty bunch that set him on his way to power in the first place. There was no condemnation of a violent minority or strikers holding the country to ransom and no &lsquo;we&rsquo;re in it together&rsquo; economic-necessity rhetoric. Instead, Morales announced &ldquo;We have decided to obey the people&rdquo; and revoked the law. Whether the sudden change of heart was down to a Road to Damascus epiphany of the damage the measures would cause, or fear of being run out of town like so many of his neo-liberal predecessors, the U-turn chalked up another victory for people power in the gutsy Andean nation. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1052), 122); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1052);

				if (SHOWMESSAGES)	{

						addMessage(1052);
						echo getMessages(1052);
						echo showAddMessage(1052);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


