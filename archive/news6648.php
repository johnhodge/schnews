<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 664 - 30th January 2009 - Upping The Auntie  </title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bbc, gaza, palestine, israel, london, manchester, forward intelligence team, fitwatch, maria gallestegui, hunger strike, parliament square, socpa, downing st, sussex university" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 664 Articles: </b>
<p><a href="../archive/news6641.php">Mner Mner...</a></p>

<p><a href="../archive/news6642.php">Iceland Meltdown</a></p>

<p><a href="../archive/news6643.php">Bolivian It Up</a></p>

<p><a href="../archive/news6644.php">Inside Guantanamo</a></p>

<p><a href="../archive/news6645.php">Exclusive: Omar Deghayes Speaks To Schnews</a></p>

<p><a href="../archive/news6646.php">Big It Up  </a></p>

<p><a href="../archive/news6647.php">Outfoxed</a></p>

<b>
<p><a href="../archive/news6648.php">Upping The Auntie  </a></p>
</b>

<p><a href="../archive/news6649.php">Bayer 'eck</a></p>

<p><a href="../archive/news66410.php">Tas-mania</a></p>

<p><a href="../archive/news66411.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th January 2009 | Issue 664</b></p>

<p><a href="news664.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>UPPING THE AUNTIE  </H3>

<p>
In the latest twist to the Propaganda war, the BBC have refused to air the Disaster Emergencies Committee&#8217;s Gaza appeal. The humanitarian appeal for cash to rebuild the shattered homes and infrastructure of the Gaza Strip was knocked back on the grounds that the beeb must &#8216;retain their impartiality&#8217;. They&#8217;ve got caught out this time though. Channel 4, ITV and Channel 5 have all agreed to show the joint appeal put out by such dangerous loony-left groups (not!) as Oxfam, Save the Children, Help the Aged and the British Red Cross. This has left the BBC high and dry, trying to defend a stance that&#8217;s even more pro-Israeli than the government&#8217;s - a pretty difficult task by itself.  <br />
 <br />
On Monday 26th a Stop the War protest outside the BBC in London managed to storm their offices. Unseen by the vanload of Plod stationed nearby, twenty people managed to get inside the lobby, demanding to see the Director General. He was unavailable for comment. Things got even sillier when a BBC camera crew were thrown out by BBC security for trying to report it. <br />
 <br />
* On the Gaza march last Saturday, the Forward Intelligence Team (FIT) were blocked by Fitwatch activists, as well as many other people  supporting them to join in obstructing police cameras. The crowd blocked cop photographers all day and rendered their &#8220;intelligence&#8221; gathering operation largely useless. Police reacted by targeting two Fitwatch activists who were nicked and held till Monday when the police tried to have them remanded (but failed). They now face charges of  obstructing police and assault / Section 4 (threatening behaviour). For more info on resisting police intrusion see <a href="http://www.fitwatch.blogspot.com" target="_blank">www.fitwatch.blogspot.com</a> <br />
 <br />
* In the face of zero mainstream press coverage, Maria Gallestegui is into the third week of her hunger strike at Parliament Square in solidarity with the people of Gaza (See <a href="../archive/news662.htm">SchNEWS 662</a>). This week she staged a one-woman protest inside the gates of Downing St &#8211; which is legal because she&#8217;d registered her intentions with the police prior to the event in accordance with SOCPA laws (remember that one for next time eh). Standing outside the door of No.10 as cabinet members came and went, she demanded, in her own words "they they lift the blockade... (bring) an end to the arms sale to Israel... and (hold) a war crimes tribunal". Hear her commentary at <a href="http://www.indymedia.org.uk/media/2009/01//420495.mp4" target="_blank">www.indymedia.org.uk/media/2009/01//420495.mp4</a> <br />
 <br />
* Sussex University students are claiming victory after a week of occupying a lecture theatre (See <a href="../archive/news663.htm">SchNEWS 663</a>) - they have reached a mutual agreement with university management to support Palestinians in their plight. The elated group say they are &#8216;reviving the role of educational institutions in effecting local and international social change&#8217;. The university has agreed to review its ethical investment policy, create new scholarships for Palestinians and pass surplus educational materials to universities in Palestine. See <a href="http://sussexoccupation.blogspot.com" target="_blank">http://sussexoccupation.blogspot.com</a>  <br />
 <br />
* On other campus protests continue: In Bradford on Jan 27th 80 students occupied the boardroom of their university following a rally on campus, calling for the university to take measures in solidarity with Gaza including a boycott of Israeli goods. For more student actions see <a href="http://www.indymedia.org.uk" target="_blank">www.indymedia.org.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=664">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=664">palestine</a>, <a href="../keywordSearch/?keyword=israel&source=664">israel</a>, <a href="../keywordSearch/?keyword=london&source=664">london</a>, <a href="../keywordSearch/?keyword=manchester&source=664">manchester</a>, <a href="../keywordSearch/?keyword=socpa&source=664">socpa</a>, <a href="../keywordSearch/?keyword=sussex+university&source=664">sussex university</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(211);
			
				if (SHOWMESSAGES)	{
				
						addMessage(211);
						echo getMessages(211);
						echo showAddMessage(211);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>