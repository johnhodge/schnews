<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 738 - 10th September 2010 - Lewes Road: Forked off</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, community garden, autonomous spaces, guerilla gardening, brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 738 Articles: </b>
<p><a href="../archive/news7381.php">Caravandals</a></p>

<b>
<p><a href="../archive/news7382.php">Lewes Road: Forked Off</a></p>
</b>

<p><a href="../archive/news7383.php">Hit The Road, Shac</a></p>

<p><a href="../archive/news7384.php">Jock's Trapped</a></p>

<p><a href="../archive/news7385.php">Blair Rich Project</a></p>

<p><a href="../archive/news7386.php">Alder Not Wiser</a></p>

<p><a href="../archive/news7387.php">Sky Larks</a></p>

<p><a href="../archive/news7388.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th September 2010 | Issue 738</b></p>

<p><a href="news738.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>LEWES ROAD: FORKED OFF</h3>

<p>
<p>
	Despite the hoardings having gone up and the plants, pots and garden furniture occupying a skip in the razed forecourt, the Lewes Road Community Garden leaves a lasting legacy for residents and the wider community and may even rise again &ndash; so claimed die-hard locals and guerrilla gardeners at a heavily policed demo outside the site on Wednesday (8th) night. </p>
<p>
	The day before (7th) at 2am &ndash; fifteen or more bailiffs backed by a dozen Sussex plod evicted the occupied site after the third time of trying. As well as it being a snide tactic, barging on site in the early hours, the High Court writ contained errors - wrong planning numbers - and the occupant, in the garden office, a legal squatter was forcibly removed. Which all adds up to an illegal eviction as far as we can work out m&rsquo;Lud, leaving aside the obvious noise regulations shat on. </p>
<p>
	One local resident told SchNEWS: <em>&ldquo;I&rsquo;ve never sworn so much at the police and not got arrested. The noise went on from 2-3am until 5. And what for? To make way for a Tesco &ndash; brilliant!&rdquo;</em> </p>
<p>
	On Wednesday Tesco bosses met up with Brighton MP Caroline Lucas. Nice one Caroline - &lsquo;horse&rsquo; and &lsquo;bolted&rsquo; come to mind. And no it&rsquo;s not true she wasn&rsquo;t spotted leaving the meeting with a Tesco halloumi cheese and rocket wrap in her pocket despite strong rumours. Apparently they - Tesco - are going to &lsquo;mull it over for two weeks&rsquo; before deciding business as usual i.e let&rsquo;s fuck up another community. </p>
<p>
	The Co-op employing more than 20 workers and standing bang next door faces certain closure while a Spar over the road and smaller Turkish shop and other African and Asian greengrocers further down are looking down the same barrel. </p>
<p>
	Meanwhile garden campaigners &ndash; who took over the site in May 2009 after it had lain derelict as a former Esso garage for five years (see <a href='../archive/news680.htm'>SchNEWS 680</a>) - have made an official complaint to Sussex Police about the eviction and are planning to take the case back to the High Court. </p>
<p>
	During it&rsquo;s tenure the garden became a meeting space for residents without gardens, a play area for kids, a community compost centre and an outdoor venue for films, acoustic music and yoga among other things. </p>
<p>
	* See <a href="http://lewesroadcommunitygarden.webs.com" target="_blank">http://lewesroadcommunitygarden.webs.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(929), 107); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(929);

				if (SHOWMESSAGES)	{

						addMessage(929);
						echo getMessages(929);
						echo showAddMessage(929);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


