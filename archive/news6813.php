<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 681 - 26th June 2009 - You&#8217;re Nicked Sunshine!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, stonehenge, summer solstice, fitwatch, police, wiltshire, english heritage" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 681 Articles: </b>
<p><a href="../archive/news6811.php">It&#8217;s Just Not Cricket</a></p>

<p><a href="../archive/news6812.php">Borderline Insanity</a></p>

<b>
<p><a href="../archive/news6813.php">You&#8217;re Nicked Sunshine!</a></p>
</b>

<p><a href="../archive/news6814.php">A Turd In The Pool  </a></p>

<p><a href="../archive/news6815.php">Schools Not Out</a></p>

<p><a href="../archive/news6816.php">Playing Solitaire  </a></p>

<p><a href="../archive/news6817.php">Tarred With A Brush  </a></p>

<p><a href="../archive/news6818.php">Big Issue  </a></p>

<p><a href="../archive/news6819.php">Kilt In Action</a></p>

<p><a href="../archive/news68110.php">Bleeding Harts  </a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th June 2009 | Issue 681</b></p>

<p><a href="news681.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>YOU&#8217;RE NICKED SUNSHINE!</h3>

<p>
<strong>AS POLICE CLAMP DOWN ON STONEHENGE SOLSTICE CELEBRATIONS</strong> <br />  <br /> Revellers attending this year&rsquo;s solstice celebrations at Stonehenge last weekend reported a massive police presence. Bolstered by tooled-up private security (presumably employed by English Heritage), police took control of the gathering but, in the words of one attendee, &ldquo;<em>If the Babylon are going to start bringing their shit to Stonehenge again, and act in an intimidating, intrusive manner as they did this year, we need to treat it as a serious threat to our right to gather peacefully</em>.&rdquo; <br />  <br /> &ldquo;<em>[There were] Hundreds of coppers in total; at the entrance to the stones, the exit from the car park and patrolling all over the site supported by considerable numbers of private security contractors with stab vests, handcuffs and sniffer dogs at the entrance to the stones</em>&rdquo;. <br />  <br /> To totally destroy any remnants of sacred energy, Wiltshire police continually buzzed the site with an unmanned drone copter, which filmed the crowd below. Also a police helicopter hovered over the car park at low altitude for about an hour from around 10am. There were 37 arrests and three hundred searches with sniffer dogs. <br />  <br /> As one told us &ldquo;<em>There was nothing free about it</em>&rdquo;. Another commented on Indymedia &ldquo;<em>Let&rsquo;s bring Fitwatch tactics to the party next year, give out some flyers encouraging people to challenge the policing of the event, and, if possible, find a way to knock that fucking drone out of the sky. If the pigs are going to police Stonehenge as if it was a demo, it&rsquo;s because they rightly see such free and anarchic gatherings as a threat to their authority. To preserve the spirit of the summer solstice, I really think it&rsquo;s time for us to organise against them</em>.&rdquo; <br />  <br /> <strong>* SchNEWS recommends Andy Worthington&rsquo;s book Stonehenge: Celebration and Subversion</strong>, featuring the history of spiritual and counter-cultural activity around the Stones going from 19th century pagan and Druidic gatherings through to the Stonehenge free festivals, the Battle Of The Beanfield, and more recently.  <br />  <br /> See <a href="http://www.andyworthington.co.uk/2009/06/20/its-25-years-since-the-last-stonehenge-free-festival&nbsp;" target="_blank">www.andyworthington.co.uk/2009/06/20/its-25-years-since-the-last-stonehenge-free-festival&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>english heritage</span>, <a href="../keywordSearch/?keyword=fitwatch&source=681">fitwatch</a>, <a href="../keywordSearch/?keyword=police&source=681">police</a>, <span style='color:#777777; font-size:10px'>stonehenge</span>, <span style='color:#777777; font-size:10px'>summer solstice</span>, <span style='color:#777777; font-size:10px'>wiltshire</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(375);
			
				if (SHOWMESSAGES)	{
				
						addMessage(375);
						echo getMessages(375);
						echo showAddMessage(375);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>