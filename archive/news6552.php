<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 655 - 14th November 2008 - Lack Cluster Excuse</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, arms trade, military bases, lakenheath, socpa, israel, lebanon, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://edinantimil.livejournal.com/"><img 
						src="../images_main/anti-war-gathering-banner.png" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 655 Articles: </b>
<p><a href="../archive/news6551.php">Fifth Columnist</a></p>

<b>
<p><a href="../archive/news6552.php">Lack Cluster Excuse</a></p>
</b>

<p><a href="../archive/news6553.php">A Brum Do</a></p>

<p><a href="../archive/news6554.php">X-ray Of Hope</a></p>

<p><a href="../archive/news6555.php">Clarion Up The Arm's Fair</a></p>

<p><a href="../archive/news6556.php">And Finally 655</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th November 2008 | Issue 655</b></p>

<p><a href="news655.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>LACK CLUSTER EXCUSE</H3>

<p>
On the morning of October 1st, 2006, a group of activists broke into the Lakenheath air base. Having monitored Israeli jets on their way to Lebanon they became aware of the presence of cluster bombs - the recorded victims of which are 98% civilian - at the US military base in Suffolk.<br />
<br />
Aware also that the MOD has no jurisdiction on US bases - in fact they&#8217;re not even allowed to enter without what&#8217;s known as a &#8216;Bloody Good Reason&#8217; - they decided to cut their way through the fences and chain themselves to the gates of the Special Munitions Store to prevent the cluster bombs (or any other explosive) being loaded onto waiting F-15s destined for the Middle East.<br />
<br />
They than rang the MOD to inform them that war crimes were taking place and encouraged them to pop over and take a look.<br />
<br />
Two years on and they are now preparing to stand trial at Ipswich Crown Court having been prosecuted under SOCPA section 128, as well as for criminal damage. <br />
<br />
SOCPA, the Serious Organised Crime and Police Act, first came into action in 2005 (see <a href="news483.htm">SchNEWS 483</a> and every t&#8217;other week since) and the &#8216;Lakenheath 8&#8217; were among the first to be prosecuted under it.<br />
 <br />
While admitting to being pleasantly surprised at becoming officially designated &#8216;seriously organised&#8217;, spokeswoman and accused Mel Harrison was rather less enamoured with the label criminal. Section 128 refers to trespassing on certain sites designated by the Secretary of State &#8211; sometimes royally-owned Crown land or places chosen in the interests of &#8216;national security&#8217;. <br />
<br />
The maximum sentence is a year in the nick and a �5000 fine. <br />
<br />
Fortunately it doesn&#8217;t state whose national security it should be in the interest of &#8211; and many of the designated sites are US military bases.  The official reason for this is to protect them from terrorist attack, however, as Harrison points out, terrorists generally willing to blow themselves to little pieces for the cause are unlikely to be deterred by the threat of a year in jail and a fine.  Instead, in Harrison&#8217;s opinion, the real targets of the legislation appear to be peace activists. <br />
<br />
Given the recent victories in court for protesters successfully using the 'Stopping War Crimes' defence, (see <a href="news589.htm">SchNEWS 589</a>, <a href="news646.htm">646</a> on how Kingsnorth climate campaigners and the B52 Two - who broke into RAF Fairford in March 2003 - won their cases), it will be interesting to see how the Lakenheath defendents fair when the trial begins on the first of December&#8230;<br />
<br />
* See also <a href="http://www.lakenheathaction.org" target="_blank">www.lakenheathaction.org</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=655">arms trade</a>, <a href="../keywordSearch/?keyword=socpa&source=655">socpa</a>, <a href="../keywordSearch/?keyword=israel&source=655">israel</a>, <a href="../keywordSearch/?keyword=direct+action&source=655">direct action</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(121);
			
				if (SHOWMESSAGES)	{
				
						addMessage(121);
						echo getMessages(121);
						echo showAddMessage(121);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>