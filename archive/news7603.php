<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 760 - 25th February 2011 - Rough Diamond</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, brighton, sussex, alternative media, rough music, internet, surveillance" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 760 Articles: </b>
<p><a href="../archive/news7601.php">Cameron Waves Arms About </a></p>

<p><a href="../archive/news7602.php">Ukba Minds Its Pease For New Sussex Detention Centre</a></p>

<b>
<p><a href="../archive/news7603.php">Rough Diamond</a></p>
</b>

<p><a href="../archive/news7604.php">Off Their Leeds</a></p>

<p><a href="../archive/news7605.php">Last Days Of The Taj</a></p>

<p><a href="../archive/news7606.php">Belgium: Migrants Revolt</a></p>

<p><a href="../archive/news7607.php">Bristol: A Hub Of Activity</a></p>

<p><a href="../archive/news7608.php">Airy (un)friendly</a></p>

<p><a href="../archive/news7609.php">Time To Bail</a></p>

<p><a href="../archive/news76010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 25th February 2011 | Issue 760</b></p>

<p><a href="news760.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROUGH DIAMOND</h3>

<p>
<p>
	Brighton&rsquo;s lippy young upstart of a radical newsletter, Rough Music, has been ruffling some feathers on SchNEWS&rsquo; home patch. Since 2005, Rough Music has been sticking it to local profiteering parasites, thuggish cops and corrupt politicians. Now it appears their efforts have provoked a backlash as a shadowy figure is trying to shut the plucky pamphleteers down. </p>
<p>
	The first inkling of trouble came when RM received an email from Nominet &ndash; who regulate &lsquo;.org.uk&rsquo; domain names &ndash; demanding proof of identification for the registered owner of their website, one Mr ahem, Rough Music. Having misplaced the id of Mr Music, the domain name roughmusic.org.uk was left to die and the site moved to the new domain of &lsquo;roughmusic.org&rsquo;. </p>
<p>
	But, like so many of the world&rsquo;s persecuted, RM was not to find peace and security in its new home. Soon, a new email dropped into the inbox from the regulators of their new domain, this time including the whole correspondence that led to the enquiry. RM was being pursued by one Tomas J Stehlik who was determined to get his hands on the personal details of the &ldquo;illegal group&rdquo; behind the monthly-ish newsletter &ndash; so that legal action could be taken by his clients. </p>
<p>
	Having traced Stehlik via his company Stehlik IT Services (&ldquo;We take the SH out of IT&rdquo;), RM rang up to try and trace the sinister figures lurking behind Stehlik. While Stehlik kept schtum on the identity of his client(s), his comments suggested the mystery foe was someone who has appeared in regular RM feature Wanker&rsquo;s Corner. &ldquo;<em>You would not use such language about your children even if they committed some not very nice things</em>,&rdquo; the priggish Stehlik told RM. </p>
<p>
	A barely coherent, toothless, brew-stained spokesmunter for RM emerged out his k-hole long enough to mumble at SchNEWS, &ldquo;<em>We don&rsquo;t know who these moneybags scum are, but they won&rsquo;t keep us quiet. Whoever it is has picked a fight with the wrong Brighton-based scurrilous radical newsletter, the wankers</em>.&rdquo; </p>
<p>
	RM is one of a gaggle of local newsletters, including the Worthing Pork Bolter, the Hereford Heckler and the Manchester Mule, pluggin the gap left by a vapid excuse for a regional news industry. Unlike sycophantic advertorial packed local rags, these independent voices are willing to stand up to profit and power in a time when few else in the media will. And with more and more newspapers slashing costs by dumping local staff and even outsourcing production (see <a href='../archive/news748.htm'>SchNEWS 748</a>), they remain true local voices. And it is anonymity that lets them do it. Yes, it might not mesh with the wiki-era of transparency and freedom of information, but with Britain&rsquo;s ludicrous libel laws tailor-made to help the rich and powerful silence their critics, there is no other option. So, in solidarity with RM, SchNEWS would like to state for the record, the following people are grade-A wankers: Tomas J Stehlik, Mike Holland, Simon Fanshawe, Tony Mernagh, Steve Harmer Strange, and many more... </p>
<p>
	Check out the latest sizzling edition of RM for the whole story, a round-up of the suspects and more details of the shady habits of Tomas J Stehlik at: <a href="http://www.roughmusic.org" target="_blank">www.roughmusic.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1117), 129); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1117);

				if (SHOWMESSAGES)	{

						addMessage(1117);
						echo getMessages(1117);
						echo showAddMessage(1117);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


