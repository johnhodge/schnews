<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 785 - 26th August 2011 - Gaddafi Ducks Out</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, libya" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 785 Articles: </b>
<p><a href="../archive/news7851.php">Dale Farm: Fight The Power</a></p>

<p><a href="../archive/news7852.php">Zad: Breaking Da Vinci Code</a></p>

<p><a href="../archive/news7853.php">Splinter Of Discontent</a></p>

<b>
<p><a href="../archive/news7854.php">Gaddafi Ducks Out</a></p>
</b>

<p><a href="../archive/news7855.php">Atos Shrugged</a></p>

<p><a href="../archive/news7856.php">Autonomist Under Attack</a></p>

<p><a href="../archive/news7857.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th August 2011 | Issue 785</b></p>

<p><a href="news785.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GADDAFI DUCKS OUT</h3>

<p>
<p>  
  	<strong>&ldquo;<em>Any future defense secretary who advises the president to again send a big American land army into Asia or into the Middle East or Africa should have his head examined</em>,&rdquo; Robert Gates, US Defense Secretary (and clearly from the Princess Bride school of geopolitics), February 2011.</strong>  </p>  
   <p>  
  	Instead of a big land army, NATO sends in the drones and bombers to help a local uprising overthrow the world&rsquo;s bad guy de jour.  </p>  
   <p>  
  	And it looks pretty much game over for both Gaddafi&rsquo;s career as Brother Leader, as well as the unique, bizarre political experiment that was the Great Socialist People&rsquo;s Libyan Arab Jamahiriya (as Libya was/is officially known). Having promised to fight to the last, it looks like the Lady Gaga of dictators snuck out by the back door of his presidential palace to beat a hasty &lsquo;tactical withdrawal&rsquo; to the desert, where fighting still rages. Events are unfolding quickly (too quickly for a weekly newsletter to hope to keep up with) so perhaps its time to take a look back, at the highs, the lows, and the downright weird during the 42 years of Gaddafi&rsquo;s rule over Libya.  </p>  
   <p>  
  	Unlike the revolutions in Egypt and Tunisia, material conditions weren&rsquo;t such a major factor in the rebellion. Under Gaddafi, Libya enjoyed one of the highest incomes-per-person in the whole of Africa. As a country with large reserves of high quality oil conveniently situated across the Med from major European consumers, making money was never really difficult. But the big difference between Gaddafi and say, Equatorial Guinea or Nigeria, is that Gaddafi did a fair bit of redistribution; funding education, health, roads and other mega projects. The Colonel was never shy to wield a stick, but there were quite a few carrots dangled under the citizens&rsquo; noses as well.  </p>  
   <p>  
  	And with only 6m inhabitants, a little redistribution went a long way.  </p>  
   <p>  
  	Gaddafi has always been one of the world&rsquo;s more entertaining leaders (from afar; from within Libya his &lsquo;quirks&rsquo; probably don&rsquo;t seem so funny). When the Colonel first rose to power in a military coup he was young, semi-literate officer. After being taught to write, he penned his &lsquo;Green Book&rsquo;- a weird fusion of Anarchism, Stalinism, Islam and Arab Nationalism. Gaddafi explained that representative democracy was a sham, and that party politics and politicians seek only to further their power (sounds fine so far). His solution was to abolish all political parties, and do away with formal leadership titles. Because of this Gaddafi has never held any formal position in Libya. He just rules the country with an iron fist is all.  </p>  
   <p>  
  	Gaddafi tried his hand at pan arab nationalism. After the defeat of Egypt and Syria in the Yom Kippur War, he boasted that if only they&rsquo;d let him in on the invasion they&rsquo;d have conquered Israel in a day. Shunned a role in arab politics, he then decided to rebrand himself as a world revolutionary, funding Basque separatists, Irish nationalists, and pretty much anyone who claimed anti-imperialist credentials.  </p>  
   <p>  
  	In his final years he metomorphasised into a Pan African. His main contribution to African unity was to involve himself in most of the continent&rsquo;s wars;&nbsp; from Liberia to Congo, Gaddafi used conflicts abroad to sell weapons and increase his influence. Just like us really.  </p>  
   <p>  
  	However, just like merry old England, racism against black Africans &lsquo;coming over here&rsquo; is rife in much of Libya, and this helped fan the rumours of foreign mercenaries in Gaddafi&rsquo;s army. The black men that were paraded in front of tv cameras by the rebels later turned out to be undocumented farm labourers seeking work. To date no proof of Gaddafi&rsquo;s use of foreign mercs has surfaced. The same can&rsquo;t quite be said of the rebels- as well as US / UK / French spies active on the ground, south african mercenaries have been witnessed by journalists. Of course, the rebels&rsquo; air force is NATO, hardly a homegrown Libyan phenomena.  </p>  
   <p>  
  	Despite the enthusiasm and popular support for the rebels and rebellion, the chances of a NATO proxy war working out for the best are still fairly slim (see <a href='../archive/news777.htm'>SchNEWS 777</a>). If history is any guide the Libyans have several years of bloody chaos to look forward to, with no guarantee that their new government will be any freer or more democratic than the last. A weak and divided Libya could mean easy pickings for Western oil companies to exploit. Or there&rsquo;s a chance that the Islamist fighters NATO armed and trained will attack the West. Again.  </p>  
   <p>  
  	<a href="../images/785-Riot-lg.jpg" target="_blank"><img style="margin-right: 10px; border: 2px solid black"  align="left"  alt=""  src="http://www.schnews.org.uk/images/785-Riot-md.jpg"  /> </a>  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1335), 154); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1335);

				if (SHOWMESSAGES)	{

						addMessage(1335);
						echo getMessages(1335);
						echo showAddMessage(1335);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


