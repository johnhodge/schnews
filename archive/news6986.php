<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 698 - 6th November 2009 - Bordering On Insanity</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, calais, no borders, refugees, asylum seekers, france" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://natowc.noflag.org.uk/"><img 
						src="../images_main/nato-pa-09-banner.png"
						alt="Smash NATO"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 698 Articles: </b>
<p><a href="../archive/news6981.php">Post Apocalypse   </a></p>

<p><a href="../archive/news6982.php">A Far Right Palava</a></p>

<p><a href="../archive/news6983.php">Lights Out</a></p>

<p><a href="../archive/news6984.php">Fuc-toff</a></p>

<p><a href="../archive/news6985.php">Court Of Carmel-ot</a></p>

<b>
<p><a href="../archive/news6986.php">Bordering On Insanity</a></p>
</b>

<p><a href="../archive/news6987.php">Lawless-cruttenden</a></p>

<p><a href="../archive/news6988.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 6th November 2009 | Issue 698</b></p>

<p><a href="news698.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BORDERING ON INSANITY</h3>

<p>
With winter approaching, the situation in Calais is increasingly desperate. Since the destruction of the Pashtun &lsquo;jungle&rsquo; in September (See <a href='../archive/news692.htm'>SchNEWS 692</a>) French police have continued destroy camps and evict squats, often hauling migrants into Coquelles detention centre before dumping them on the outskirts of Calais days later. <br />  <br /> Without the comparative safety and community of the basic shelters built in the &lsquo;jungles&rsquo;, the mainly young (often very young &ndash; 10-13 years old) migrants have resorted to sleeping under bridges, which the cops then clear, destroying tents and blankets. The police have now also taken to spraying irritant chemicals over an area once it has been cleared. The Prefect of Calais (cop in charge of the operations), Pierre de Bousquet, said last week &ldquo;I<em>t is not the right of the police to destroy blankets and clothes</em>&rdquo;, yet that is exactly what they have been doing. <br />  <br /> <strong>No Borders</strong> remain active in the area. Recent actions have included chaining the entrance to Coquelles detention centre before dawn raids; flyposting Calais centre with photos of police violence and harassment that can go by unseen; banner drops on motorway bridges surrounding Calais; and dying the town hall fountain red. <br />  <br /> Benjamin, recently part of a hunger strike with eight other Iranians and two No Borders activists (See <a href='../archive/news694.htm'>SchNEWS 694</a>) said, &ldquo;<em>I like No Borders, they treat us with respect. The police have got worse recently, and I am scared to be returned to Greece. If I get sent to Iran I will die</em>.&rdquo; <br />  <br /> More importantly, activists have been distributing desperately needed tents and blankets. The French state has transformed such simple offers of help into political acts through their use of legislation making it illegal to help migrants. <br />   <br /> One activist said, &ldquo;<em>When we stand together to resist a raid, work together to occupy buildings, stop deportations to &lsquo;safe&rsquo; third countries like Greece or Italy, or challenge charter flights to Iraq or Afghanistan, when we take action or hold demonstrations, this is when we are strong</em>&rdquo;. <br />   <br /> Two vans of tents and blankets from Cardiff and Bristol No Borders groups are leaving for Calais today (6th). A second Bristol van is going on November 26th. (<a href="http://bristolnoborders.wordpress.com" target="_blank">http://bristolnoborders.wordpress.com</a>). <br />   <br /> There have also been benefit gigs held around the UK which are vital to support migrant solidarity work in Calais. The next is on Friday 21st November at the Hare and Hounds, Brighton <br />  <br /> * For more information see <a href='../archive/news676.htm'>SchNEWS 676</a>, <a href="http://www.calaismigrantsolidarity.wordpress.com" target="_blank">www.calaismigrantsolidarity.wordpress.com</a> <a href="http://www.calais9.wordpress.com" target="_blank">www.calais9.wordpress.com</a> UK contact number 07534 008380. Calais office (from UK) 00 33 6 34 81 07 10 <br />  <br /> * Donations to &lsquo;Calais Migrant Solidarity&rsquo;, Unity Trust Bank, sort code 086001 account number 20233983 <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=698">asylum seekers</a>, <a href="../keywordSearch/?keyword=calais&source=698">calais</a>, <a href="../keywordSearch/?keyword=france&source=698">france</a>, <a href="../keywordSearch/?keyword=no+borders&source=698">no borders</a>, <a href="../keywordSearch/?keyword=refugees&source=698">refugees</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(556);
			
				if (SHOWMESSAGES)	{
				
						addMessage(556);
						echo getMessages(556);
						echo showAddMessage(556);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>