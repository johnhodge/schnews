<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 779 - 15th July 2011 - Rolling Out the Unwelcome Mat</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, legal aid, immigration, asylum seekers" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 779 Articles: </b>
<b>
<p><a href="../archive/news7791.php">Rolling Out The Unwelcome Mat</a></p>
</b>

<p><a href="../archive/news7792.php">Gaza Flotilla: Seige You Later</a></p>

<p><a href="../archive/news7793.php">Don't Mansion It</a></p>

<p><a href="../archive/news7794.php">Not The Full English</a></p>

<p><a href="../archive/news7795.php">Inside Schnews</a></p>

<p><a href="../archive/news7796.php">Like It Or Lumpur It</a></p>

<p><a href="../archive/news7797.php">Rotten Fruit</a></p>

<p><a href="../archive/news7798.php">Lyons Led By Donkeys</a></p>

<p><a href="../archive/news7799.php">C'mon Bunny Light My Fire</a></p>

<p><a href="../archive/news77910.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th July 2011 | Issue 779</b></p>

<p><a href="news779.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROLLING OUT THE UNWELCOME MAT</h3>

<p>
<p>  
  	<strong>NO ASYLUM HERE AS SCHNEWS MOURNS CUTS TO MIGRANT LEGAL AID</strong>  </p>  
   <p>  
  	<strong>The closure of the Immigration Advisory Service (IAS)</strong> on Monday (11th), following new legislation denying legal aid for all immigration cases, has left hundreds of migrants and asylum seekers without legal support.  </p>  
   <p>  
  	The service had been running for 35 years from London and regional offices, with 300 employees. At the time of the shock closure - which staff discovered via notices on doors when they turned up for work - there were 650 active cases. Claimants are now being told to ask courts for their hearings to be delayed and to find themselves alternative legal representation.  </p>  
   <p>  
  	The charity&rsquo;s collapse into administration is the latest assault on migrants facing deportation, stuck in a legal system designed to encourage failure. Three quarters of asylum claims are refused by the Home Office &ndash; but of those who can appeal, a quarter of the rash and politically-motivated decisions are overturned at Independent Tribunal. For the government, it&rsquo;s easier to just shut &lsquo;em up and send &lsquo;em packing than follow through on their hollow commitments to access to justice and human rights.  </p>  
   <p>  
  	In June 2010 the then biggest refugee legal service, Refugee and Migrant Justice, was forced to close despite being owed &pound;1.8million by the government in legal aid dough for concluded cases. Amongst the thousands of asylum seekers left out in the cold were hundreds of children alone in the UK and trafficking victims.  </p>  
   <p>  
  	RMJ and the Immigration Advisory Service have not only achieved justice for thousands of people and their families but have often embarrassed the government and the UKBA in the process. The IAS recently won an injunction to stop &lsquo;failed&rsquo; asylum seekers from being deported back to Iraq.  </p>  
   <p>  
  	The slashing of legal aid for migration, refugee and asylum cases is part of the government&rsquo;s <strong>Legal Aid, Sentencing and Punishment of Offenders Bill</strong>. The fast-tracked bill also includes making family, welfare, employment, clinical negligence and housing cases illegible (well, consultation and due process ain&rsquo;t the Coalition&rsquo;s style &ndash; althought they might just u-turn into it later if put under enough pressure).  </p>  
   <p>  
  	The Legal Services Commission, which oversees legal aid provision, says on its website it&rsquo;s &ldquo;<em>committed to playing its part in delivering these reforms, to ensure there is a sustainable legal aid scheme for the future, targeted at those who need help most</em>.&rdquo; Except those being forced back to torture and persecution, harassed at work, denied access to their children or made homeless, apparently.  </p>  
   <p>  
  	Not content with allowing essential legal services for migrants to fall through the net, the government&rsquo;s latest &lsquo;crackdown&rsquo; on allowing people in to the country will be based on &ndash; yep! - how much money they&rsquo;ve got in the bank.  </p>  
   <p>  
  	New Home Office proposals would enforce a minimum income threshold for anyone wanting to sponsor a spouse, partner or dependent to come to the UK. If your loved one earns less than &pound;5,000 a year, you&rsquo;re not comin&rsquo; in. The government is now in the fourth phase of its plans to cut immigration to below 100,000 a year. As well as the income limits, the White Paper also proposes the probation period for spouses and partners to be upped from two years to five, and encourages people to send money abroad to elderly relatives rather than bring them into the country.  </p>  
   <p>  
  	Even the convention on human rights isn&rsquo;t safe. Ministers are scheming to change the wording on article 8 of the document, which concerns the right to family life. That change would mean that even husbands and wives who have been recognised as genuine would be liable for deportation if they&nbsp; were found to be living &lsquo;illegally&rsquo; in the UK.  </p>  
   <p>  
  	* Always plenty of immigration status reports at <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1276), 148); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1276);

				if (SHOWMESSAGES)	{

						addMessage(1276);
						echo getMessages(1276);
						echo showAddMessage(1276);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


