<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 723 - 21st May 2010 - Thailand: Red With Blood</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, thailand, financial crisis" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 723 Articles: </b>
<p><a href="../archive/news7231.php">All That Romains...</a></p>

<b>
<p><a href="../archive/news7232.php">Thailand: Red With Blood</a></p>
</b>

<p><a href="../archive/news7233.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7234.php">Village Greens</a></p>

<p><a href="../archive/news7235.php">Lost In Transnational</a></p>

<p><a href="../archive/news7236.php">Nama-ed And Shamed</a></p>

<p><a href="../archive/news7237.php">Genoa A Good Lawyer?</a></p>

<p><a href="../archive/news7238.php">Brim And Proper</a></p>

<p><a href="../archive/news7239.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st May 2010 | Issue 723</b></p>

<p><a href="news723.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THAILAND: RED WITH BLOOD</h3>

<p>
Mounting oppression is sweeping Thailand as, at the time of going to print (20th May), a state of emergency has been declared across 23 provinces, mainly in the north of the country - the main Red Shirt stronghold. <br />   <br /> Thailand has been convulsed by a bitter struggle between the nation&rsquo;s elite and its disenfranchised poor, played out in protests that have paralysed Bangkok for weeks and now threaten to envelope the country. <br />   <br /> The protesters&rsquo; fortified camp in central Bangkok was attacked by the army on Wednesday (19th) morning. By evening, fires were ablaze across the city. There were over 1,000 Red Shirts (made up of the rural and urban poor) hiding inside the protest area, distrustful of government offers to ship them to safety. Fourteen were killed that day as the army moved in to suppress protests, with the bodies of six protesters shot by troops found inside a temple designated as a refuge for women and children. Two months ago saw the Red Shirt opposition movement stage relatively peaceful, ritualistic actions (see <a href='../archive/news714.htm'>SchNEWS 714</a>). <br />   <br /> However, the total number of deaths since the protests began are now reported to be at around 82 with over 800 injured. <br />  <br /> Despite recent bloodthirsty government tactics to quell it, the insurgency continues, with Red Shirts holding rallies across the rest of the country. In the northern territory, town halls have been occupied and burned to a cinder. Banks, the stock exchange and media outlets have been some of the targets of rioters, furious at their mistreatment. There have been reports that on &ldquo;Red Radio&rdquo;, in defiance of ceasefire call by Red-Shirt leaders, a call out was made that &ldquo;if you pass by a bank, burn it down&rdquo;. <br />   <br /> This recent chapter was set in motion after the Government of Abhisit Vejjajiva - as supported by the Yellow Shirt movement (a sect of Thais opposed to former leader Thaksin Shinawatra and his allies) came to power in 2008 following a parliamentry vote engineered by the military. The Red Shirts movement began as supporters of Thaksin (a former policeman and brief owner of Manchester City Football Club). Thaksin was deposed in a military coup in 2006 and convicted in absentia for corruption. <br />  <br /> In past years, similar rebellions have been calmed by Thailand&rsquo;s 82-year-old monarch, Bhumibol Adulyadej, who has reigned since 1946 - making him the world&rsquo;s longest serving current head of state and the longest-reigning monarch in Thai history. But as the country suffers through its worst political crisis in decades, the king has disappointed &lsquo;his subjects&rsquo; by saying nothing that might calm the turmoil, as he did in 1973 and 1992 when - with a few quiet words - he halted eruptions of political blood letting. The geriatric and ailing king is no longer in a position to influence events as his power fades. <br />  <br /> This time the divisions in society have become too deep and the anger too hot to reconcile with a few words. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=financial+crisis&source=723">financial crisis</a>, <span style='color:#777777; font-size:10px'>thailand</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(784);

				if (SHOWMESSAGES)	{

						addMessage(784);
						echo getMessages(784);
						echo showAddMessage(784);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


