<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 657 - 28th November 2008 - Dedicated Followers Of Fascism</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, british national party, enoch powell" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://veggieclimatemarch.50webs.com/"><img 
						src="../images_main/vegan-climate-march-banner.jpg"
						alt="Vegan Climate March, Dec 6th 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 657 Articles: </b>
<p><a href="../archive/news6571.php">Frozen Assets</a></p>

<p><a href="../archive/news6572.php">Radio Active</a></p>

<p><a href="../archive/news6573.php">Agenda Bender</a></p>

<p><a href="../archive/news6574.php">Free Wheelers</a></p>

<p><a href="../archive/news6575.php">Pointing The Minga</a></p>

<p><a href="../archive/news6576.php">Bruges Crew</a></p>

<b>
<p><a href="../archive/news6577.php">Dedicated Followers Of Fascism</a></p>
</b>

<p><a href="../archive/news6578.php">Schnews In Brief</a></p>

<p><a href="../archive/news6579.php">657 And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th November 2008 | Issue 657</b></p>

<p><a href="news657.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>DEDICATED FOLLOWERS OF FASCISM</H3>

<p>
BNP's Annus Horribilis continues... After last week's leak of their entire membership list on the web (See <a href="../archive/news656.htm">SchNEWS 656</a>), the flak has continued... <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* In our previous issue we linked to a website publishing the membership list, but this has since been withdrawn due to personal threats of violence to the webmaster. But don't worry you haven't missed out, it is still online at <a href="http://www.wikileaks.org" target="_blank">www.wikileaks.org</a> <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* This week the BNP were evicted from their merchandise warehouse. Rented by Excalibur - the trading arm of the BNP - it stored such lovely merchandise as replica military medals, Enoch Powell t-shirts, 'Great White' records and Union Jack mugs. Their ex-landlord made this statement: "<i>Evans Easyspace was aware of renting a property to Excalibur, but were not aware of its links to the BNP. We have now terminated their agreement and they are moving out at the end of November.</i>" In response the BNP have announced that they actually decided to move to better premises. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* While the BNP membership list revealed a litany of police, prison officers, soldiers and even vicars, it also contains two paedophiles - who were jailed last week for sexually abusing two fourteen-year-old girls. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* In desperation over the disclosure of their membership list the BNP has been forced into hiding behind the much-hated Human Rights Act. They've also been firing off dubious legal threats to the likes of Indymedia. The letter sent to Indymedia by Lee John Barnes LLB (hons), from the "BNP Legal Affairs Unit", threatens to take legal action, on the basis of theft, data protection and contempt of court, unless the list of members is removed. For more of LJB's hilarious antics check out <a href="http://weloveleebarnes.blogspot.com" target="_blank">http://weloveleebarnes.blogspot.com</a> <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* This week, all but two of the thirty-three anti-fascists arrested at the BNP's Red White and Blue Festival in August this year (See <a href="../archive/news643.htm">SchNEWS 643</a>) have had their charges dropped. Over twenty were arrested for violent disorder after the group tried to blockade a road into the fascist's knees-up. Others were arrested for failing to comply with police directions on the mass demonstration. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
* For more Fashwatch info see <a href="http://www.searchlightmagazine.com" target="_blank">www.searchlightmagazine.com</a> , <a href="http://www.antifa.org.uk" target="_blank">www.antifa.org.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=british+national+party&source=657">british national party</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(151);
			
				if (SHOWMESSAGES)	{
				
						addMessage(151);
						echo getMessages(151);
						echo showAddMessage(151);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>