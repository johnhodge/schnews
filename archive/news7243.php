<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 724 - 28th May 2010 - Cops Circle The Square</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, democracy village, brian haw, protest camp" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 724 Articles: </b>
<p><a href="../archive/news7241.php">Flight Brigade</a></p>

<p><a href="../archive/news7242.php">Schmovies In Brief</a></p>

<b>
<p><a href="../archive/news7243.php">Cops Circle The Square</a></p>
</b>

<p><a href="../archive/news7244.php">War-sore</a></p>

<p><a href="../archive/news7245.php">Pan Handled</a></p>

<p><a href="../archive/news7246.php">H&k: A Kirk In The Teeth</a></p>

<p><a href="../archive/news7247.php">Homes Alone</a></p>

<p><a href="../archive/news7248.php">Bp: Block In The Pipeline</a></p>

<p><a href="../archive/news7249.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th May 2010 | Issue 724</b></p>

<p><a href="news724.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COPS CIRCLE THE SQUARE</h3>

<p>
Nick &lsquo;Extra&rsquo; Clegg made much of civil liberties during his &lsquo;big bang&rsquo; damp squib policy launch last week. <br />  <br /> However, it was an altogether more traditional Tory approach actually on the streets this week (25th) as cops evicted the Democracy Village protest camp at Parliament Square (see <a href='../archive/news722.htm'>SchNEWS 722</a>). Well you can&rsquo;t have the hoy polloi mooching around looking untidy when the Queen&rsquo;s coming for the State opening of Parliament. Not even when her speech itself promised the &ldquo;return of the right to peaceful protest.&rdquo; <br />  <br /> It seems cops sprung into action at the behest of London Mayor Boris Johnson, who&rsquo;d clearly been rifling through Red Ken&rsquo;s dusty old filing cabinets before finding some old GLA bye-laws to use to say bye bye to the camp which had successfully made the square home since May 1st.&nbsp;  <br />  <br /> But clearing away the anti-Afghan war protesters wasn&rsquo;t enough - police also arrested and detained more permanent parliament resident Brian Haw, and his right hand fellow anti-Iraq war protester, Barbara Tucker.&nbsp;  <br />  <br /> Despite Brian having previously been arrested numerous times for no good reason - as well as having his possessions illegally seized and trashed - before later being repeatedly vindicated in court (see <a href='../archive/news609.htm'>SchNEWS 609</a>, <a href='../archive/news607.htm'>607</a>), cops were not at all hesitant when Brian asked to see a warrant and tried to film them after they turned up first thing in the morning demanding to search his tent.&nbsp;  <br />  <br /> Despite Brian currently being on crutches, cops weren&rsquo;t slow to roughly seize him for not getting out of the way quick enough &ndash; thus &lsquo;obstructing police&rsquo; and they carted him and Barbara off for a pointless day of discomfort in the cells. Both were held for just over 24 hours before being unconditionally bailed til July. They are now safely back in the Square &ndash; but at least her maj&rsquo; didn&rsquo;t haven&rsquo;t to glimpse him from her car window as she went by... <br /> Video of Brian&rsquo;s arrest: <a href="http://www.youtube.com/watch?v=cXjL5bNkV1I" target="_blank">www.youtube.com/watch?v=cXjL5bNkV1I</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>brian haw</span>, <a href="../keywordSearch/?keyword=democracy+village&source=724">democracy village</a>, <a href="../keywordSearch/?keyword=protest+camp&source=724">protest camp</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(794);

				if (SHOWMESSAGES)	{

						addMessage(794);
						echo getMessages(794);
						echo showAddMessage(794);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


