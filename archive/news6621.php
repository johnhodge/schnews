<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 662 - 16th January 2009 - Information For Action</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, genocide, israel, palestine, gaza, ism, jenny linnel, tubas, smash edo, edo-mbm, raytheon, heckler & koch, boeing, thales, caterpillar, carmel agrexco, stop the war coalition, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pap/index.htm"><img 
						src="../images_main/662-banner.png"
						alt="UK Nationwide Demos Against the Attack on Gaza - Saturday January 17th"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 662 Articles: </b>
<b>
<p><a href="../archive/news6621.php">Information For Action</a></p>
</b>

<p><a href="../archive/news6622.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6623.php">Frank Fort</a></p>

<p><a href="../archive/news6624.php">Spirited Away</a></p>

<p><a href="../archive/news6625.php">The Rushing Revolution</a></p>

<p><a href="../archive/news6626.php">Greec-ing The Wheels Of Power</a></p>

<p><a href="../archive/news6627.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/662-raytheon-lg.jpg" target="_blank">
													<img src="../images/662-raytheon-sm.jpg" alt="Raytheon Roof-o-meter" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 16th January 2009 | Issue 662</b></p>

<p><a href="news662.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>INFORMATION FOR ACTION</H3>

<p>
<b>WITH CASUALITIES MOUNTING IN GAZA AND NO SIGN OF A MEANINGFUL CEASEFIRE IN SIGHT, ASKS: WHAT CAN YOU DO ABOUT IT FROM BRITAIN?</b> <br />
 <br />
<b>Go To Palestine</b> <br />
 <br />
In recent years many British activists have done exactly this. Admittedly going to Gaza would be very difficult and dangerous right now, though there are several British activists currently there, including Jenny Linnel and Ewa Jasciewicz, whose dramatic reports featured in last week's SchNEWS. International activists often work with the <b>ISM</b>, a Palestinian-led group that does non-violent direct action, human shield and other support work (see <a href="../archive/news544.htm">SchNEWS 544</a> , <a href="http://palsolidarity.org" target="_blank">http://palsolidarity.org</a>). A group from Brighton has also sent several delegations in the past year to the Tubas region, helping Palestinians whose lives are under threat from the encroaching illegal Israeli settlements and army (See <a href="../archive/news584.htm">SchNEWS 584</a> , <a href="../archive/news609.htm">SchNEWS 609</a> , <a href="../archive/news652.htm">SchNEWS 652</a> , <a href="http://www.brightonpalestine.org" target="_blank">www.brightonpalestine.org</a>). <br />
 <br />
For those unable to do this, there are numerous ways in Britain to vent your anger against the massacre in Gaza. <br />
 <br />
<b>Target The Arms Trade</b> <br />
 <br />
There is a building movement of campaigns focusing on arms companies in Britain. These groups arose from the anti-war movement in 2003, and borne out of frustration that 1.5 million people on the streets of London and a host of other huge demos didn't change government policy. There are arms corporations who have manufacturing and offices in this country, and virtually all have a role in arming Israel - though wherever the arms are being deployed, they need to be stopped. Currently the most active campaigns in Britain include <b>Smash EDO</b> (<a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a>) - in the process of closing down EDO-MBM/ITT in Brighton, who make bomb release parts for the Paveway missile used in Israel; <b>Smash Raytheon</b> (<a href="http://raytheonout.wordpress.com" target="_blank">http://raytheonout.wordpress.com</a>) - building a campaign against the Bristol base of the US arms giant who make the radar for the F-16 bombers used on Gaza, plus bombs and missiles such as the GBU-39 bunker-buster - as the rooftop occupation goes into its sixth week (See <a href="../archive/news661.htm">SchNEWS 661</a>). In Nottingham, the main warehouse of <b>Heckler & Koch</b> has become a target (<a href="http://nottsantimilitarism.wordpress.com" target="_blank">http://nottsantimilitarism.wordpress.com</a>). <br />
 <br />
<b>* Smash EDO Demos - Noise demos</b> continue every Wednesday between 4-6pm at the EDO-MBM/ITT factory on Home Farm Road, Brighton. There will be monthly themed demos throughout the spring, starting next week  <br />
 <br />
** On January 21st  <b>"Naming the Dead" in Gaza</b> outside EDO - with an accompanying die-in. <br />
 <br />
<b>Other military hardware companies selling their wares to Israel: Boeing</b> - who make Apache helicopters and hellfire missiles and <b>Thales</b> who make Watchkeeper drones can both be found side by side with Raytheon at Bristol Business Park, Frenchay; Caterpillar - whose armoured bulldozers are used to flatten Palestinian homes - Head Office, Mansour House, 188, Bath Road, Slough, Berkshire UK, SL1 3GA. There have been actions against Caterpillar - see <a href="../archive/news351.htm">SchNEWS 351</a> , <a href="../archive/news446.htm">SchNEWS 446</a> , <a href="../archive/news454.htm">SchNEWS 454</a> ; <b>Israel Military Industries</b>, Eurotaas (EUTA) Ltd, 12 York Gate, London NW1 4QS  <br />
 <br />
<b>* Demo, Jan 19th at UAV Engines</b> - a company owned by Israeli defence contractor Elbit systems who build engines for military aircraft used by Israel. Their premises is at Lynn Lane, Shenstone, Litchfield, WS14 0DT. Meet 11.15am at Shenstone Railway Station. <br />
 <br />
<b>Boycott Israeli Goods</b> <br />
 <br />
There is a list of shops in this country who sell Israeli goods, and Israeli companies doing business here. One company who are the target for an ongoing campaign is <b>Carmel Agrexco</b>, based in Hayes Middlesex. CA are 50% Israeli owned, import produce grown on illegally settled Palestinian land, and have been the target of many actions in the past five years including a 12 hour blockade last October (for just some see <a href="../archive/news571.htm">SchNEWS 571</a> , <a href="../archive/news585.htm">SchNEWS 585</a> , <a href="../archive/news600.htm">SchNEWS 600</a> , <a href="../archive/news609.htm">SchNEWS 609</a> , <a href="../archive/news620.htm">SchNEWS 620</a> , <a href="../archive/news637.htm">SchNEWS 637</a> , <a href="../archive/news649.htm">SchNEWS 649</a> )  <br />
 <br />
<b>Valentines Day</b> is a busy time for Carmel as they flog Israeli-grown flowers across Europe, and the Boycott Israeli Goods Campaign is calling for actions in the weeks leading up to this, Feb 1st-14th. Organise your own actions and see <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a> <br />
 <br />
<b>* Boycott companies that have links with Israel:</b> Coca Cola, Burger King, Starbucks, l'Oreal, McDonalds, Johnson & Johnson, Est�e Lauder, Disney, and Nestle all donate/actively support the Israeli Government. Waitrose, Tesco, Sainsburys and Marks and Spencers stock goods from illegal settlements. Check the first 3 numbers on the barcode: 729 is from Israel. <a href="http://www.inminds.com/boycott-israel.php#companies" target="_blank">www.inminds.com/boycott-israel.php#companies</a>  <br />
 <br />
<b>* Do a supermarket action</b> - Get a group together, and go around with a large trolley and fill it with goods made in Israel, then leave it in the middle of the aisle with a big note saying "I was gonna buy these goods until I realised they are from stolen Palestinian lands and using slave Palestianian labour. Stop the Gaza massacre!" <b>In Brighton there is a week of supermarket actions</b>: Sat 17th, 12pm-2pm - Waitrose, Western Road; Mon 19th 5-6pm - Somerfield, London Road; Tues 5-6pm - Sainsburys, New England Qtr; Weds 5-6pm - Tescos, New Church Rd, Hove; Thurs 5-6pm - Somerfield, St James St; Fri 5-6pm - Marks & Spencer, Western Rd. <br />
 <br />
<b>* Other companies involved with Israel - Veolia,</b> a privatised contractor involved in amongst other things recycling, were forced to withdraw a project for a tramway in East Jerusalem which their subsidiary company Luas was involved in after protests from the Irish Palestinian Solidarity Campaign. <a href="http://electronicintifada.net/v2/article5723.shtml." target="_blank">http://electronicintifada.net/v2/article5723.shtml.</a> <b>Ahava - Dead Sea Cosmetics</b> - 39 Monmouth Street, Covent Garden, London, WC21T 9DD  <br />
 <br />
<b>* For more info about boycotting Israel see <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a>  <br />
 <br />
 <br />
Street Demonstrations Continue</b> <br />
 <br />
When the latest attack began on December 27th, demos took off around the country. Many British cities have seen big street demos already, but there's another round this Saturday (17th).  <br />
 <br />
<b>London</b> - the third Saturday city centre demo in a row -  2pm at Trafalgar Square.  <a href="http://<br />
www.stopwar.org.uk" target="_blank"><br />
www.stopwar.org.uk</a>  <br />
 <br />
<b>** Nottingham</b> - 10.30am, Forest Recreation Ground, marching to the Council House.  <a href="http://<br />
www.nottmagainstwar.org.uk" target="_blank"><br />
www.nottmagainstwar.org.uk</a>  <br />
 <br />
<b>** Birmingham</b> - 12 noon, Victoria Square  <br />
 <br />
<b>** Sheffield</b> - 12 noon, outside Sheffield Town Hall.  <a href="http://<br />
www.sheffieldpsc.org.uk" target="_blank"><br />
www.sheffieldpsc.org.uk</a>  <br />
 <br />
<b>** Liverpool</b> - 1pm outside the bombed out church at the top of Bold St  <a href="http://<br />
www.indymedia.org.uk/en/regions/liverpool" target="_blank"><br />
www.indymedia.org.uk/en/regions/liverpool</a>  <br />
 <br />
<b>** Leeds</b> - 12.30pm at Art Gallery for march through city centre. Bring shoes and placards.  <a href="<br />
http://lcaw.co.uk" target="_blank"><br />
http://lcaw.co.uk</a>  <br />
 <br />
<b>** Cardiff</b> - 1pm, Nye Bevan Statue, Queen Street  <br />
 <br />
<b>** Exeter</b> - 12.30pm outside Odeon Cinema, Sidwell Street  <br />
 <br />
<b>** Bath</b> - 12.30pm, Abbey Courtyard,  <a href="http://<br />
www.bathactivistnetwork.blogspot.com" target="_blank"><br />
www.bathactivistnetwork.blogspot.com</a>  <br />
 <br />
<b>** Newport</b> - 1.30pm at the old Kwik Save Car Park in Pill, marching to John Frost Square  <a href="<br />
http://southwalesanarchists.org" target="_blank"><br />
http://southwalesanarchists.org</a> <br />
 <br />
<b>* Gaza Vigil At Parliament Square</b> - Support the ongoing the 24hr vigil which has been going on since January 8th. Maria Gallastegui has been on hunger strike at the vigil since the 12th.  <a href="http://<br />
www.vigilforgaza.net" target="_blank"><br />
www.vigilforgaza.net</a>  <br />
 <br />
<b>* Daily demos outside the Israeli Embassy</b>, 5-7pm. 2 Palace Green, London, W8. <br />
 <br />
<b>* Brighton Gaza Vigil</b> - daily, 1-2pm, 5-6pm, at the Clocktower <br />
 <br />
<b>* Live video link with Gaza</b> and public meeting - 7.30pm, 16th January, Community Base South Wing, 113 Queens Road, Brighton  <br />
 <br />
<b>Actions this week</b> <br />
 <br />
* The biggest of the street demos across the country last weekend was in London, where 100,000 people marched on the Israeli Embassy last Saturday in freezing weather - the largest ever demonstration in support of the Palestinians in Britain. <a href="http://www.indymedia.org.uk" target="_blank">www.indymedia.org.uk</a> <br />
 <br />
<b>* Starbucks in Whitechapel</b> was firebombed on Monday night (12th) in an apparently anti-Israel action. First rocks were thrown through the windows and later a petrol bomb was tossed in... <br />
 <br />
* This Wednesday, activists stormed the offices of the <b>British Israel Communications and Research Centre (BICOM)</b> in Great Portland St, a lobby group playing a key role in Israel's public relations operation, downplaying the number of Palestinian dead and manipulating the news agenda. The office was temporarily shut down as photos of civilian casualties from Gaza were pasted onto computer screens, phone lines were cut off and leaflets distributed highlighting the the human cost of Israeli aggression.  <br />
Email <a href="mailto:londonpalsol@googlemail.com">londonpalsol@googlemail.com</a> <br />
 <br />
* The Brunei Gallery at the <b>School for Oriental and African Studies (SOAS)</b> was occupied this Tuesday, after the Uni gave the MoD space to display photos glorifying colonialism, but then gave into demands to ban the military on campus. The exhibition was closed, and the students are using the gallery all week for Palestinian solidarity. <a href="http://www.soassolidarity4gaza.blogspot.com" target="_blank">www.soassolidarity4gaza.blogspot.com</a> <br />
 <br />
* On Tuesday (12th) <b>Brighton</b> saw a daring banner drop on the precarious West Pier - although nothing like as precarious as life in the Gaza strip of course, a region about as big as the coastal strip from Worthing to Seaford but with three times the population. <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> <br />
 <br />
<b>Real News About Gaza</b> <br />
 <br />
 <a href="http://www.palsolidarity.org" target="_blank">www.palsolidarity.org</a>  <br />
* <a href="http://www.indymedia.org.uk" target="_blank">www.indymedia.org.uk</a>  <br />
* <a href="http://electronicintifada.net" target="_blank">http://electronicintifada.net</a>  <br />
* <a href="http://www.maannews.net/en" target="_blank">www.maannews.net/en</a>  <br />
* <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a>  <br />
* <a href="http://talestotell.wordpress.com" target="_blank">http://talestotell.wordpress.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=genocide&source=662">genocide</a>, <a href="../keywordSearch/?keyword=israel&source=662">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=662">palestine</a>, <a href="../keywordSearch/?keyword=gaza&source=662">gaza</a>, <a href="../keywordSearch/?keyword=ism&source=662">ism</a>, <a href="../keywordSearch/?keyword=tubas&source=662">tubas</a>, <a href="../keywordSearch/?keyword=smash+edo&source=662">smash edo</a>, <a href="../keywordSearch/?keyword=edo-mbm&source=662">edo-mbm</a>, <a href="../keywordSearch/?keyword=raytheon&source=662">raytheon</a>, <a href="../keywordSearch/?keyword=heckler+%26+koch&source=662">heckler & koch</a>, <a href="../keywordSearch/?keyword=carmel+agrexco&source=662">carmel agrexco</a>, <a href="../keywordSearch/?keyword=direct+action&source=662">direct action</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(192);
			
				if (SHOWMESSAGES)	{
				
						addMessage(192);
						echo getMessages(192);
						echo showAddMessage(192);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>