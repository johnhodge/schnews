<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 694 - 9th October 2009 - Swooper Troopers</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, ratcliffe-on-soar, coal, direct action, kingsnorth, e.on, nottingham" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 694 Articles: </b>
<b>
<p><a href="../archive/news6941.php">Swooper Troopers</a></p>
</b>

<p><a href="../archive/news6942.php">Brimar Republic</a></p>

<p><a href="../archive/news6943.php">Calais-ing It Bare</a></p>

<p><a href="../archive/news6944.php">Constant Threat At Dale Farm</a></p>

<p><a href="../archive/news6945.php">Madchester Revisited</a></p>

<p><a href="../archive/news6946.php">Young Turks</a></p>

<p><a href="../archive/news6947.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/694-chim-chim-cher-ee-lg.jpg" target="_blank">
													<img src="../images/694-chim-chim-cher-ee-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 9th October 2009 | Issue 694</b></p>

<p><a href="news694.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SWOOPER TROOPERS</h3>

<p>
<strong>POWER TO THE PEOPLE AS DEVELOPMENTS  FEEL CLIMATE PROTEST PRESSURE</strong> <br />  <br /> Phew, what a scorcher of a summer it&rsquo;s been for climate change campaigners in Britain - with three climate camps, protest camps and direct action targeting both open-cast coal mining and coal-fired power stations. And it&rsquo;s not over yet as next weekend (October 17th-18th) sees <strong>The Great Climate Swoop</strong> at the Ratcliffe-on-Soar coal-fired power station near Nottingham. <br />   <br /> This mass action is a follow up to the Climate Camp Swoop on the City of London back in August (See <a href='../archive/news689.htm'>SchNEWS 689</a>), and is buoyed by recent news that the development of the <strong>Kingsnorth coal plant</strong> &ndash; the site of the 2008 Camp For Climate Action (see <a href='../archive/news642.htm'>SchNEWS 642</a>) has been shelved (Who said direct action was ineffective?). <br />  <br /> The Great Climate Swoop will descend upon Ratcliffe power station, the third biggest polluting plant in the country. Ratcliffe is represented as one the bastions of the new &lsquo;clean coal&rsquo; industry by its corporate owners and government investors, by virtue of their  promise that technology to &lsquo;capture&rsquo; and &lsquo;store&rsquo; carbon might be possible sometime in the future. <br />   <br /> The technology to trap and store carbon is unproven (unless of course you just leave it in the ground where it is already of course trapped and stored) - and the government and power stations themselves admit that &lsquo;clean coal&rsquo; power plants are unlikely to be available for decades, if at all. <br />   <br /> Coal is now the biggest growing source of carbon emissions, rising some 20,000 times faster than at any point in the Earth&rsquo;s billion year history. And the UK is the world&rsquo;s biggest per capita emitter of fossil fuel carbon dioxide, followed closely by the US and Germany. There are currently plans for eight new coal-power stations to be built in the UK, all on the promise of &lsquo;clean coal&rsquo;. <br />  <br /> The Swoop is going to involve a convergence on the main gates of Ratcliffe at 12 noon, where a camp will be held for 24 hours. Bring warm clothes, tents and enough food for a day. There will be four blocks, including a mass demo at the main gates and others who will take direct action within the plant. Affinity groups from all around the country are set to arrive, but if you are coming independently the website gives all details including a map &ndash; see <a href="http://www.thegreatclimateswoop.org" target="_blank">www.thegreatclimateswoop.org</a> <br />  <br /> * Transport: The Big Lemon Bus is taking people from Brighton to The Swoop on Friday Oct 16th , 6pm, from Old Steine, returning Sunday. Bus tickets are &pound;35 (&pound;25 concession), to cover costs, and are available at the Brighton Peace and Environment Centre or through Big Lemon Bus website. For other transport see <a href="http://climatecamp.org.uk/actions/climate-swoop-2009/transport" target="_blank">http://climatecamp.org.uk/actions/climate-swoop-2009/transport</a> <br />  <br /> ** Over the last few months there has been a bombardment of action confronting the fossil fuel industry &ndash; particularly the mining and burning of coal &ndash; including: the Camps For Climate Action (<a href="http://www.climatecamp.org.uk" target="_blank">www.climatecamp.org.uk</a>) in Scotland, Wales and London as well as continued protests at Mainshill Protest Camp in Scotland (See <a href='../archive/news692.htm'>SchNEWS 692</a>), plus actions at Ffos-y-Fran opencast coal mine in Wales (See <a href='../archive/news692.htm'>SchNEWS 692</a>). Elsewhere, there&rsquo;s been a 1,500-strong mass action at a coal-fired power plant in Copenhagen (See <a href='../archive/news693.htm'>SchNEWS 693</a>) as well as a demo at Hazlewood coal-fired power station near Melbourne, which is the dirtiest coal plant in the world (See <a href='../archive/news691.htm'>SchNEWS 691</a>, <a href="http://www.switchoffhazelwood.org" target="_blank">www.switchoffhazelwood.org</a>). <br />   <br /> In Germany a couple of weeks ago the courts revoked permission for a new coal fired power station in Datteln on environmental grounds. The E.ON plant was half way to completion and would have become Europe&rsquo;s largest coal-fired power station. Despite the German energy corporation being busy building power stations all over Europe &ndash; including Kingsnorth and Ratcliffe-on-Soar &ndash; it goes to show E.ON can be stopped, even on their home ground. <br />  <br /> And it was more bad news for E.ON as this week their plans for a new coal power station at Kingsnorth were scrapped. On Wednesday (7th Oct) E.ON threw in the towel and stopped the development of the first new coal-fired station to be built in the UK for 30 years. This is a massive victory for environmental campaigners who have been taking continuous action against the plans. <br />    <br /> With Kingsnorth on the back burner, next stop Ratcliffe-on-Soar&hellip; and then on to Copenhagen for the COP15 Climate Conference, December 7th-18th see <a href="http://www.climate-justice-action.org" target="_blank">www.climate-justice-action.org</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=climate+change&source=694">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=694">coal</a>, <a href="../keywordSearch/?keyword=direct+action&source=694">direct action</a>, <a href="../keywordSearch/?keyword=e.on&source=694">e.on</a>, <a href="../keywordSearch/?keyword=kingsnorth&source=694">kingsnorth</a>, <a href="../keywordSearch/?keyword=nottingham&source=694">nottingham</a>, <a href="../keywordSearch/?keyword=ratcliffe-on-soar&source=694">ratcliffe-on-soar</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(519);
			
				if (SHOWMESSAGES)	{
				
						addMessage(519);
						echo getMessages(519);
						echo showAddMessage(519);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>