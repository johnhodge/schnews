<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 744 - 22nd October 2010 - The Big Libcon Cuts: To the Manor Osbourne</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, austerity, tory" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.anarchistbookfair.org.uk" target="_blank"><img
						src="../images_main/an-bkfr-2010-banner.jpg"
						alt="Anarchist Bookfair 2010, London, October 23rd"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 744 Articles: </b>
<p><a href="../archive/news7441.php">Crude But Refined</a></p>

<p><a href="../archive/news7442.php">Huntington Eviction Alert</a></p>

<b>
<p><a href="../archive/news7443.php">The Big Libcon Cuts: To The Manor Osbourne</a></p>
</b>

<p><a href="../archive/news7444.php">France: Will You Still Need Me, Feed Me, When I'm 62?</a></p>

<p><a href="../archive/news7445.php">Chev It Up Ya Prs</a></p>

<p><a href="../archive/news7446.php">Who Ya Gonna Coal?</a></p>

<p><a href="../archive/news7447.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd October 2010 | Issue 744</b></p>

<p><a href="news744.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE BIG LIBCON CUTS: TO THE MANOR OSBOURNE</h3>

<p>
<p>
	Thousands have marched, a government department has been broken into, and a Scottish branch of Lloyds TSB has been occupied by pissed-off pensioners...It must be the (OK, somewhat quiet compared to France) start of the anti-cuts revolt! </p>
<p>
	Wednesday&rsquo;s Spending Review announced, as expected, that the plan is to further punish the public and the workers for the elite&rsquo;s fuck ups. Osborne announced nearly 20% cuts in public expenditure, clobbering welfare, social housing, public sector workers yada yada. The axing package &ndash; already dubbed as this generation&rsquo;s Poll Tax - was acknowledged, even by government-friendly think tanks, as passing the burden of the cuts mainly to the poorest in society. </p>
<p>
	The Age of Austerity (TM) is characterised by a lurch to the right under cover of crisis. Remove safety nets, make workers leaner and &lsquo;hungrier&rsquo;, then privatise and hand everything over to corporations to run and profit from. And if an insufficient profit is to be made, the public purse is always there for a bailout. Just make sure the cops are tooled up enough to keep order when the market inevitably &lsquo;fails to deliver&rsquo;. While cooked up on the playing fields of Eton, the after-taste is definitely more American in flavour. </p>
<p>
	For those who know that the real crisis has yet to begin to play out, resistance is in the air. Is it an opportunity to wake up the masses before they sleepwalk-on-ice-factor-idol themselves into the biggest change in the &lsquo;social settlement&rsquo; between rich and poor since the Second World War? </p>
<p>
	Protests took place in many UK cities on Wednesday evening, including Cardiff, Newcastle, Sheffield, Barnsley, Cambridge, Southampton, Bolton, Luton and London. In Dorchester protesters marched in white boiler suits and masks to represent the &lsquo;faceless&rsquo; nature of workers in the firing line. </p>
<p>
	The London turn-out was the largest with 3000 people marching on Westminster. Most then packed into the central hall, whilst hundreds waited outside. The focus was on the half million public sector job losses and the 41% cut in university teaching budgets, amongst more general anti-slashing anger. </p>
<p>
	Notably absent from the throngs of trade unionists and student activists was leader of the opposition Ed Milliband, who had promised earlier in the month that he would &ldquo;definitely&rdquo; attend. <br /> 
	If he was worried about associating with misbehaving members of the public he needn&rsquo;t have worried. The main march passed peacefully. It was only several hours later, at around 8pm, that any minor mischief occurred when 12 protesters broke into the government Department of Business, Innovation and Skills in Whitehall. Three were arrested for criminal trespass, while others avoided the nick by leaving when asked. Immediate arrests do tend to put a dampener on these high-profile occupations... </p>
<p>
	On Thursday in Glasgow a few unlikely suspects continued the disobedience. Eleven members of Citizens United group, including several pensioners, have occupied a Lloyds TSB branch in the city centre. The group entered the building shouting &ldquo;No Cuts! No Cuts!&rdquo;, and, at the time of writing, are still there. </p>
<p>
	Admirable, yes, but it looks as though it will take until at least the weekend for the resistance to pick up steam. A series of protests across the country are planned for Saturday, including in Cardiff at the City Hall, Edinburgh which kicks off at 11am at East Market Street, and Sheffield outside the Town Hall at 12.30pm. </p>
<p>
	MadPride, the mental health system survivors group, are taking a creative approach for their demo planned at Speakers Corner, Hyde Park for the October 26th at 1pm. Mental health service users will re-enact the opening chapter of Foucault&rsquo;s &lsquo;Discipline and Punish&rsquo;, and a life-size effigy of a Con-Dem politician will be publicly hung, drawn and quartered. The day will also be a &lsquo;medication strike&rsquo;, with participants refraining from taking medications and any use of the mental health services for 24 hours. </p>
<p>
	As the reality of the cuts sink in, will this relatively calm start gain significant momentum? The seeds of nationwide unrest have been scattered but to reach European levels it will need a outbreak of &ldquo;We&rsquo;re all in it together&rdquo;... </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(982), 113); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(982);

				if (SHOWMESSAGES)	{

						addMessage(982);
						echo getMessages(982);
						echo showAddMessage(982);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


