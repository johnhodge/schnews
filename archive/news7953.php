<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 795 - 4th November 2011 - Squat Next?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 795 Articles: </b>
<p><a href="../archive/news7951.php">Dedicated Followers Of Fascism</a></p>

<p><a href="../archive/news7952.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news7953.php">Squat Next?</a></p>
</b>

<p><a href="../archive/news7954.php">Extraordinairy Petition</a></p>

<p><a href="../archive/news7955.php">Bring 'em To Book</a></p>

<p><a href="../archive/news7956.php">Hearts Of Oakland</a></p>

<p><a href="../archive/news7957.php">Quake Up Call</a></p>

<p><a href="../archive/news7958.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 4th November 2011 | Issue 795</b></p>

<p><a href="news795.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUAT NEXT?</h3>

<p>
<p>  
  	AS HOPES OF STOPPING LEGAL AID BILL REST ON A LORD&rsquo;S PRAYER <br />  
  	&nbsp;  </p>  
   <p>  
  	Unsurprisingly last Tuesday&rsquo;s vote to criminalise squatting (see <a href='../archive/news794.htm'>SchNEWS 794</a>) went the government&rsquo;s way by a fairly sizeable margin. Clause 26 of the Legal Aid bill creates the new offence of squatting in a residential building - and was passed by 283 to 13. An amendment excluding premises that had been vacant for over six months was also defeated.  </p>  
   <p>  
  	The night before the vote Squatters Housing Action Group (SHAG) staged a demo in London with a march and bike ride converging on parliament. The intention was to have a picnic then sleep outside for the night, the Met had other ideas though and used oppressive SOCPA legislation to harass, kettle and ultimately move on the protesters. Police were being particularly petty &ndash; pushing people over, nicking bikes and arresting 12 people &ndash; on an otherwise entirely peaceful action.  </p>  
   <p>  
  	The bill now heads off to the Lords where it should get a bit more of a going over. Squatters are unlikely to receive much sympathy from the majority of the landed gentry, but they do tend to be a little more cantankerous than their elected counterparts. Although the hurriedly pushed through amendment was a kick in the teeth for the homeless and vulnerably housed, it&rsquo;s far from over yet. Assuming the bill does get duffed up by the oldies, it&rsquo;ll head back to the Commons once again, buying a bit more time.  </p>  
   <p>  
  	The question now is &ndash; where to take the fight next? Lobbying the Lords, setting up info-shops and doing media interviews might help, but it&rsquo;s probably too little too late. Many squatters choose to live outside the &lsquo;system&rsquo; so appealing to it to save them is alien to many. Squattastic, the regularish London squat meeting, will be held at 2-4 Tufnell Park Road, N2 0DL from 2pm to discuss the legislation and the response to it.  </p>  
   <p>  
  	In Brighton there will be a demo in support of squatting on Saturday 5th, forming up at Victoria Gardens from 11am and marching to an undisclosed location at midday. Victoria Gardens is also home to Occupy Brighton, who have condemned the move to criminalise and fully support the march. Bring soundsystems, banners and ire.  </p>  
   <p>  
  	* For the latest happenings see <a href="http://network23.org/snob" target="_blank">http://network23.org/snob</a> and <a href="http://www.squashcampaign.org" target="_blank">www.squashcampaign.org</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1414), 164); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1414);

				if (SHOWMESSAGES)	{

						addMessage(1414);
						echo getMessages(1414);
						echo showAddMessage(1414);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


