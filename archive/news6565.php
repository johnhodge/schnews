<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 656 - 21st November 2008 - Snap Crackle Cop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, photography, direct action, prevention of terrorism act, police, national union of journalists, jaqui smith, counter-terrorism bill, netcu" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://edinantimil.livejournal.com/"><img 
						src="../images_main/anti-war-gathering-banner.png" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 656 Articles: </b>
<p><a href="../archive/news6561.php">Minga The Merciless</a></p>

<p><a href="../archive/news6562.php">Witness Appeal Of The Week</a></p>

<p><a href="../archive/news6563.php">So Solidarity Crew</a></p>

<p><a href="../archive/news6564.php">Fruity Number</a></p>

<b>
<p><a href="../archive/news6565.php">Snap Crackle Cop</a></p>
</b>

<p><a href="../archive/news6566.php">Rail Against The Machine</a></p>

<p><a href="../archive/news6567.php">Rnc U L8r</a></p>

<p><a href="../archive/news6568.php">3 Reichs And Yer Out</a></p>

<p><a href="../archive/news6569.php">T'rubble At Mill (rd)</a></p>

<p><a href="../archive/news65610.php">Off With Their Arms</a></p>

<p><a href="../archive/news65611.php">Identity Politics</a></p>

<p><a href="../archive/news65612.php">And Finally 656</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st November 2008 | Issue 656</b></p>

<p><a href="news656.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>SNAP CRACKLE COP</H3>

<p>
As we reported some months back, there have been worrying developments in the way police have been focussed lately on cracking down on people daring to take photographs (or video) of the world around them. Numerous cases have come to light around the country of police stopping and searching budding paparazzi and even forcing them to delete the contents of their cameras, despite it being technically legal to take pictures in most public places (see our handy guide &#8211; <a href="../archive/news638.htm">SchNEWS 638</a>). <br />
 <br />
A couple of weeks ago a 15-year old schoolboy on a geography field trip was stopped by police under Section 44 of the Terrorism Act for taking a photograph of Wimbledon train station as part of his of GCSE course. Community Support Officers forced him to give his details and sign a form or face arrest (legal note: You do not have to give your details under and stop and search, despite what lies the police will say and never sign any police notes). <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0"><a href="../images/656-camera-lg.jpg" target="_blank"><img style="border:2px solid black" src="../images/656-camera-sm.jpg"  /></a></td></tr></table>Last week the police stopped four students from Kingston University from filming an interview with the anti-war Parliament Square protesters as part of their MA in Film Making. The police approached the students and told them they would need a permit from the Council to film. Brian Haw from the Parliament Square peace camp filmed this incident, but the police curiously didn&#8217;t stop him from filming! Later the students returned with a letter from the University course director explaining their work and that it was not for commercial purposes and the students were covered by the University&#8217;s insurance. But police would still not let the students film and when challenged refused to check with superiors. <br />
 <br />
The students finally managed to film a week later and were accompanied by a member of the National Union of Journalists just in case the police tried to interfere again &#8211; which they didn&#8217;t. <br />
The increasing uncertainty over what constitutes a crime in UK law has seen a huge rise in potential police power as vague laws are increasingly &#8216;interpreted&#8217; in an arrest-now-argue-about-it-later police philosophy. (Or in the case of the murder of Jean Charles Menezes a shoot-now-cover-it-up-later policy, see <a href="../archive/news647.htm">SchNEWS 647</a>). <br />
 <br />
This is not just SchNEWS&#8217;s own polarised view of it - Home Secretary Jacqui Smith exposed the situation perfectly when she wrote to the Secretary-General of the National Union of Journalists earlier this year to clarify that there is &#8220;<i>no legal restriction on photography in public places...</i>&#8221;, but &#8220;<i>Decisions may be made locally to restrict or monitor photography in reasonable circumstances. That is an operational decision for the officers involved based on the individual circumstances of each situation</i>&#8221;. <br />
 <br />
And if that&#8217;s not clear enough for you, things could be about to get even more blurry. The Counter-Terrorism Bill is currently winging its way through the Lords on its way to the Statute book. Section 75 will make it an offence to &#8220;<i>elicit or attempt to elicit information about members of the armed forces, intelligence services or policemen, where this information could be of use to a terrorist.</i>&#8221; This is punishable by up to ten years in the slammer, and it is up to the accused to prove that you had a reasonable excuse for holding the information. If you had looked at the recently leaked list of BNP members you could potentially be up for terrorism charges as the act even covers ex-coppers, soldiers and spooks.  <br />
 <br />
How broadly this could be interpreted in the field is left open &#8211; any picture of a copper could be of use to a terrorist, even if just as source material for making their own fake police uniform for use in a daring bomb plot.  <br />
 <br />
To what extent this will be used against protesters is yet to be seen, but with animal rights protesters already openly viewed as terrorists and other political activists increasingly criminalised, this could be the latest blunt tool used by police for harassment of activists. <br />
In future making notes of police misbehaviour and taking their photos and collar numbers, despite being a perfectly legal (and sensible) thing to do, could warrant arrest. NETCU Watch point out: &#8220;<i>None of this has ever been used in order to use violence against the police, let alone terrorism, but we can hazard a guess that they might use this proposed legislation against us.</i>&#8221; <br />
 <br />
It is highly likely that websites highlighting the political work of the police will be affected as the legislation also applies to Internet Service Providers, meaning they will have a legal duty to remove all sites perceived to fall under this offence. FitWatch say &#8220;<i>This legislation not only attempts to stifle our ability to hold the police force to account for their actions, but also attacks the principles of open publishing on the Internet. It must be resisted.</i>&#8221; FitWatch are calling for a day of mass action the day the legislation is passed (expected early 2009) by means of a mass publishing of information about police officers. The idea is to get hold of a piece of info about a cop, or a photo or video - and publish it on Flickr, Youtube, your blog, website, myspace/facebook, wherever you want. <br />
 <br />
* See <a href="http://www.fitwatch.blogspot.com" target="_blank">www.fitwatch.blogspot.com</a> and <a href="http://www.netcu.wordpress.com" target="_blank">www.netcu.wordpress.com</a> <br />
 <br />
** Report and picture of Smug Ignorant Pig at <a href="http://www.indymedia.org.uk/en/2008/11/412849.html" target="_blank">www.indymedia.org.uk/en/2008/11/412849.html</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=656">direct action</a>, <a href="../keywordSearch/?keyword=police&source=656">police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(137);
			
				if (SHOWMESSAGES)	{
				
						addMessage(137);
						echo getMessages(137);
						echo showAddMessage(137);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>