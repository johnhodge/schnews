<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 782 - 4th August 2011 - Brutality in a China Shop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, china, migrants, workers rights, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 782 Articles: </b>
<p><a href="../archive/news7821.php">From Cradle To Graves</a></p>

<p><a href="../archive/news7822.php">Is It The End Of The Line For Shell?</a></p>

<p><a href="../archive/news7823.php">Safe To Go Back In The Water?</a></p>

<b>
<p><a href="../archive/news7824.php">Brutality In A China Shop</a></p>
</b>

<p><a href="../archive/news7825.php">Cia: We Do What We Haftar</a></p>

<p><a href="../archive/news7826.php">What Not To Malware</a></p>

<p><a href="../archive/news7827.php">Schnews In Brief</a></p>

<p><a href="../archive/news7828.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th August 2011 | Issue 782</b></p>

<p><a href="news782.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRUTALITY IN A CHINA SHOP</h3>

<p>
<p>  
  	In China, over the last few months, ever greater dissent has been raising its balaclava&rsquo;d head - to the consternation of the authoritarian rulers. They continue to respond with swift retribution and brutality. But will they will continue to be able to contain it? Despite their attempts to maintain the &lsquo;bamboo curtain&rsquo; of internet control, dissent finds its way around and news of the masses&rsquo; resistance continues to leak out. &nbsp;  </p>  
   <p>  
  	Last week, hundreds of people rioted in the streets of the southwest Guizhou province over the death of a disabled street vendor. Three chengguan officers, two men and a woman, reportedly beat the one-legged fruit seller to death over an issue to do with his unlicensed stall.  </p>  
   <p>  
  	The chengguan are a municipal security force charged with enforcing local laws; everything from sanitation and pollution control to work safety, planning, and &lsquo;city appearance&rsquo;. It&rsquo;s their treatment of street vendors however that have earned them an infamous reputation. Beatings have become so commonplace that a new turn of phrase has been coined in Mandarin; to say &lsquo;Don&rsquo;t be too chengguan&rsquo;&nbsp; is an admonishment for bullying or terrorising someone.&nbsp; &nbsp;  </p>  
   <p>  
  	The most recent unrest in Guizhou saw crowds of people throwing stones at the police and attacking&nbsp; chengguan vehicles. The authorities deployed teargas and water cannons to break up the protest, and images of bloodied and bruised demonstrators have spread all over the internet. The riot continued late into the evening of Tuesday 26th July and reportedly left 30 protesters and 10 police officers injured. Six chengguan officers have apparently been taken in for questioning over the death.  </p>  
   <p>  
  	Two months ago, on 10th June,&nbsp; a three day riot exploded in Zenchang, an industrial city in the southern Guangdong province. Another dispute sparked by the chengguan, who wrestled a street vendor and his pregnant wife to the ground. The area has a huge migrant worker population, who usually get the blunt end of the law enforcement&rsquo;s truncheons and the thinnest wedge of China&rsquo;s economic boom. Thousands of them took the streets, attacking police with bottles, bricks and stones, and torching police and fire vehicles. Over 1,000 troops were deployed with teargas and water cannons. Many protesters were arrested, and six were given jail sentences in early July for their part in the demonstration.  </p>  
   <p>  
  	In April, more migrant workers in Guangdong clashed with police during a protest over unpaid wages, and in Lichuan, Hubei, thousands stormed government headquarters after a local politician who had spoken out against police corruption died in custody. Mongolia has also seen its largest street protests in 20 years after a herder was killed by authorities whilst trying to stop coal trucks trespassing on pastoral grasslands.  </p>  
   <p>  
  	Although accurate data over the extent of civil dissent and its severity is hard to come by, even conservative state estimates counted more than 90,000 mass incidents in 2006 alone, with increases in following years. The Chinese domestic security budget was raised to 624.4bn yuan (&pound;60bn) in 2011, an increase of nearly 14% from the previous year.  </p>  
   <p>  
  	While supporting the so-called Arab Spring is all the rage for the western powers, with intervention in countries where ideological capital or control of resources seems up for grabs, it&rsquo;s all self-interest &ndash; they have no such desire to do or even say anything about China.  </p>  
   <p>  
  	How could they, when they know they&rsquo;ll soon be queuing up, caps in hand, to beg them to bailout their bankrupt system. We&rsquo;re moving to a world where the largest, richest superpower props up and de-facto owns increasingly huge chunks of the world&rsquo;s assets &ndash; rather like America in the 20th century, only one notch up on the police state, overt censorship and complete indifference to what anyone else thinks about it stakes...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1314), 151); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1314);

				if (SHOWMESSAGES)	{

						addMessage(1314);
						echo getMessages(1314);
						echo showAddMessage(1314);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


