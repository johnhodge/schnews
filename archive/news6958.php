<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 695 - 16th October 2009 - Just Deserts</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, western sahara, africa, morocco, resource grab" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 695 Articles: </b>
<p><a href="../archive/news6951.php">La La La Bomba</a></p>

<p><a href="../archive/news6952.php">Kicking Up A Stink</a></p>

<p><a href="../archive/news6953.php">Harverster Loons</a></p>

<p><a href="../archive/news6954.php">Quids Pro Quo</a></p>

<p><a href="../archive/news6955.php">Art Attack</a></p>

<p><a href="../archive/news6956.php">Fash In The Pan</a></p>

<p><a href="../archive/news6957.php">695 Schnews In Brief</a></p>

<b>
<p><a href="../archive/news6958.php">Just Deserts</a></p>
</b>

<p><a href="../archive/news6959.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th October 2009 | Issue 695</b></p>

<p><a href="news695.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>JUST DESERTS</h3>

<p>
In one of Earth's most inhospitable landscapes, tens of thousands of indigenous people live under military rule in annexed territories, a colonial power having taken control of the region's abundant natural resources, launched waves of subsidised immigrant settlement and built a series of bloody big walls to keep out hundreds of thousands living in refugee camps in a mine-filled no-man's land. Welcome to Africa's last colony: Western Sahara. <br />  <br /> With such a story to tell you might have thought you'd have heard more about it but the media landscape on the subject is, well pretty desert-like. <br />  <br /> Last week the UN's Fourth Committee (Special Political and Decolonization) meeting in New York heard petitions from academics, politicians, human rights campaigners and, that increasingly rare breed, indigenous Saharawi's who wanted to impress upon the international community the urgent need for action in their country. <br />  <br /> Decades of dithering, weak resolutions and hands off treatment has following the bitter war for control between Morocco, Algeria, Mauritania and the home-grown Polisario Front - seeking self-government and independence &ndash; that was sparked after Spain finally pulled out of Europe's last colonial occupation in 1975. <br />  <br /> After its other neighbours eventually dropped out for one reason or another, Moroccan armed forces eventually prevailed (helped along by shedloads of Western military kit), with human rights abuses galore against the Polisario, more or less completing the job in the early 1990s. Morrocco built walls and fortifications, shipped settlers in and native Saharawis out, they now rule the area to such an extent that the Moroccan government is the largest single employer and the Kingdom refers to the country merely as 'the Southern states'. Large numbers of displaced Saharawis have been left living in squalid refugee camps in Algeria with others eeking out life in the area to the East of the Moroccan walls, a mine-filled no-man's land they optimistically call the 'free zone', where the Polisario have maintained their existence as the political representatives of the refugees and a guerilla force still clinging on in the teeth of the program of armed suppression and incremental cultural eradication. <br />  <br /> Despite pointedly refusing to recognise the Moroccan claims to sovereignty, the rest of the World has pretty much left them to get on with it, and shown plenty of willingness to business with them while the UN has tried and failed to get anywhere with numerous failed attempts to get Morocco to agree to engage in any sort of negotiations that don't ultimately leave them in charge. <br />  <br /> But why would anybody be especially interested in this militarily insignificant wasteland of sand, scrub and searing heat in the day and chilling sub zero nights? <br />  <br /> Well it's all about the money of course, stoopid! WS has one of the planet's largest deposits of  phosphates, a key ingredient in the fertilisers that modern farming relies on. <br /> This is what kept Spain in WS so long, and Spanish mining interests did deals with Moroccan counterparts to ensure their finger remained in the pie after their official withdrawal. The first thing Morroco did was secure the phosphate mines, eventually building a wall around the entire main mining town, Bou Craa, now almost entirely inhabited by Moroccan mining employees. <br />  <br /> To ensure the smooth flowing of the profits, they have in fact built the longest conveyor belt in the World (over a 100km long &ndash; you should see their version of the Generation Game). Morocco exports at least half of the world's phosphates, dealing internationally with almost every country, in a hugely lucrative trade. <br />  <br /> Whilst the WS mine only currently constitutes a small fraction of this, there are large known deposits to extract there, and with scientists now fearful of 'peak phosphate', as limited resources decline under the vastly increasing demand, the appeal of plundering WS is unlikely to diminish any time soon. Not mention the lure of valuable uranium, iron, and titanium deposits all ready for the exploitin'. Ever willing to make a buck, Morocco's even made a small packet exporting Saharan sand to the Canary Islands to beef up the beaches ready for the European holidaymakers jetting over for a bit of winter sun. <br />  <br /> Also making it worth the colonisation is the potential for oil and gas development. It is suspected there are stashes of these to be found, and Morocco actually signed exploration deals with Total (one of SchNEWS favourite oil companies, see <a href='../archive/news611.htm'>SchNEWS 611</a>) and an US firm, Kerr-Mcgee in the early 2000s, which only stalled after a concerted campaign by pressure groups and NGO's led to the firm's shareholders getting jittery and they both eventually pulled out. Presumably, as energy becomes more and more important globally, there will be further attempts to make such deals politically palatable again. <br />  <br /> And there's always other fish in the sea. Load of them in fact; the western coast of WS is teeming with relatively undepleted stocks of fish &ndash; which Europe is extremely keen on, it's fishermen having laid waste to so many of the other oceans of the world. In fact, the EU has recently done a deal to open up some of the waters to them, so it's unlikely they'll want to rock the boat any time soon. <br />  <br /> Meanwhile, the Saharawi people have been left largely deserted in the desert by the West. While Venezuela's Hugo Chavez, Nambia's President Pohamba and South Africa's Jacob Zuma all spoke on the issue at the UN General Assembly last month, more than a mere oasis of global support and pressure for action is going to be needed to sway Morocco from its course. And even then what price that it will be the powerful resource-hungry businesses that muscle in to make sure any resolution puts their interests above those of any remaining beleaguered native inhabitants. <br />  <br /> On August 5th, six Sahrawi student advocates returning from a fact-finding mission from the refugee camps, and heading for a conference on conflict resolution in the UK, were stopped and arrested by Moroccan police at the Mohamed V Airport in Casablanca. Whisked off they were detained in unknown locations before being released under heavy surveillance with injuries including a broken ankle, cuts, lacerations and and severe bruising. Fears for their safety remain and any imminent threat of awareness-raising presence over here safely averted by the Moroccan authorities for the time being. <br />  <br /> Today campaigners around the world will mark the 34th anniversary of the International Court of Justice Ruling &ndash; the UN's first ignored proclamation of the region's right to self rule. <br />  <br /> See <a href="http://www.freesahara.ning.com" target="_blank">www.freesahara.ning.com</a> and <a href="http://www.upes.org" target="_blank">www.upes.org</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=africa&source=695">africa</a>, <span style='color:#777777; font-size:10px'>morocco</span>, <span style='color:#777777; font-size:10px'>resource grab</span>, <span style='color:#777777; font-size:10px'>western sahara</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(533);
			
				if (SHOWMESSAGES)	{
				
						addMessage(533);
						echo getMessages(533);
						echo showAddMessage(533);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>