<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 777 - 1st July 2011 - Turn On The Waterways</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, romani" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 777 Articles: </b>
<p><a href="../archive/news7771.php">Libya: Anti-nato Classes</a></p>

<p><a href="../archive/news7772.php">Africa House Evicted</a></p>

<p><a href="../archive/news7773.php">Simon Levin Rip</a></p>

<p><a href="../archive/news7774.php">A Ship Off The Ol'bloc</a></p>

<p><a href="../archive/news7775.php">Campaign For Real Bail</a></p>

<p><a href="../archive/news7776.php">Greece: The Golden Fleecing</a></p>

<p><a href="../archive/news7777.php">Peru - What A Scorcher</a></p>

<b>
<p><a href="../archive/news7778.php">Turn On The Waterways</a></p>
</b>

<p><a href="../archive/news7779.php">Sheikh-down</a></p>

<p><a href="../archive/news77710.php">Holding The Fortnum</a></p>

<p><a href="../archive/news77711.php">[headline On Strike]</a></p>

<p><a href="../archive/news77712.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st July 2011 | Issue 777</b></p>

<p><a href="news777.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TURN ON THE WATERWAYS</h3>

<p>
<p>  
  	The struggle is intensifying for travelling boat dwellers trying to protect their homes and way of life. Changes currently &lsquo;in the pipeline&rsquo; for the waterways authorities include stricter implementation of rules regarding boats without moorings. This targets a marginalised sector of the population, Bargee Travellers - from Romany families who have been on the canals for generations, to &lsquo;New Wave Travellers&rsquo; looking to for the quay to a better life. These measures will leave thousands vulnerable to homelessness &ndash; needlessly shunted on to the housing lists and dole queue.  </p>  
   <p>  
  	The government is making canal overseers British Waterways a charity and wants to give it more power in the process &ndash; including the ability to force entry, stop and search and make subordinate legislation. While the prospect of unregulated quasi-police jumping aboard your vessel uninvited is daunting, it&rsquo;s the latter that&rsquo;s causing the most concern for the 6,000-10,000 people living on boats trundling up and down the canals.  </p>  
   <p>  
  	British Waterways has spent years trying to get those &ldquo;continuously cruising&rdquo; to cough up and settle down. The body tried to criminalise boaters without moorings when the 1995 British Waterways Act was being debated &ndash; pushing for a &pound;1000 fine for living as a traveller on the waterways.  </p>  
   <p>  
  	Parliament stopped this on human rights grounds. It included in the final Act a &lsquo;14 day&rsquo; rule on stopping in one place but never stated just how far a boat has to travel between moorings. British Waterways has since waged a harassment campaign against boat travellers, threatening in letters to &ldquo;remove and demolish&rdquo; homes it considers to have contravened the made-up-as-it-goes-along rule.  </p>  
   <p>  
  	Making people homeless is not the usual remit for charitable bodies, something which has been brought up by boat dwellers during the government&rsquo;s &ldquo;consultation&rdquo; period.  </p>  
   <p>  
  	* For more info see <a href="http://www.gypsy-traveller.org/where-you-live/boat-dwellers" target="_blank">www.gypsy-traveller.org/where-you-live/boat-dwellers</a> and <br />  
  <a href="http://	www.bargee-traveller.org.uk" target="_blank">	www.bargee-traveller.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1270), 146); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1270);

				if (SHOWMESSAGES)	{

						addMessage(1270);
						echo getMessages(1270);
						echo showAddMessage(1270);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


