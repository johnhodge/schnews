<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 687 - 14th August 2009 - Bad Korea Choice</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, south korea, ssangyong, workers struggles, occupation" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 687 Articles: </b>
<p><a href="../archive/news6871.php">Fash Get The Brum Rush</a></p>

<p><a href="../archive/news6872.php">From Russia With Hate</a></p>

<p><a href="../archive/news6873.php">Notes From A Small Island  </a></p>

<p><a href="../archive/news6874.php">Celt In Action</a></p>

<p><a href="../archive/news6875.php">Ciggy Stubbed Out?</a></p>

<b>
<p><a href="../archive/news6876.php">Bad Korea Choice</a></p>
</b>

<p><a href="../archive/news6877.php">Greek Tragedy  </a></p>

<p><a href="../archive/news6878.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th August 2009 | Issue 687</b></p>

<p><a href="news687.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BAD KOREA CHOICE</h3>

<p>
Earlier this month, a 77 day worker occupation of South Korean car manufacturer Ssangyong came to an ugly end when 100 commandos and 300 riot police stormed the factory, resulting in a pitched battle with the militant strikers. The aftermath has seen mass arrests and prosecutions of union members and workers. <br />  <br /> The sit-in began in late May when the struggling Ssanyong, under bankruptcy protection, announced plans to make 2,646 workers redundant. While over 1,600 workers left the company voluntarily, over a thousand others opposed the move and around 800 of them occupied the factory&rsquo;s paint shop in protest. <br />  <br /> Despite worker offers to reduce hours and participate in temporary layoffs, the stand-off remained until the August 5th when the state moved in. In the ensuing chaos, strikers attempted to hold the police at bay with fire bombs, hand made cannons, catapults and strategically lit fires, but were eventually overran by police lowered onto the roof in modified shipping containers and shimmying down ropes suspended from helicopters. <br />  <br /> Some 64 people involved in the occupation have since been arrested including all of the core union leaders. A number also face investigation for acts of violence against police officers and the company&rsquo;s security personnel. <br />  <br /> Prosecutors and police have even raised the spectre of reds under the bed during the investigation, claiming, &ldquo;Outside forces with a strongly pro-communist character were involved in the strike&rdquo; and that &ldquo;ideological documents&rdquo; were found suggesting that there were plans to &ldquo;establish a military committee&rdquo;, charges that have yet to be substantiated in any way.&nbsp; <br />  <br /> It is also noticeable that all the attention has been focused on the actions of the union while ignoring the illegal and violent acts perpetrated by the company and the police. There has been no mention of any police facing any charges for the indiscriminate assaults on fallen union members during the raid. Moreover, although company employees were booked without detention, none have been arrested for attacking members of civic groups with pieces of wood in front of the factory&lsquo;s main gate.&nbsp; <br />  <br /> Police are also doing nothing about the fact that the company violated the Fire Services Act by cutting off electricity and water for fire hydrants, the Emergency Medical Service Act by preventing medical personnel from entering or exiting the occupied factory, and denying basic human rights by blocking individuals from bringing the workers water and food. <br />  <br /> Civic groups and labour organizations have been unanimous in criticising the hard-line and anti-labour stance of the prosecutors and police and warn that more strikes could result from the witch-hunts.&nbsp; <br />  <br /> <font face="courier new"><strong><font size="3">Disarm DSEi 2009</font></strong> <br />  <br /> Destroy the Banks! Destroy the Investors! Destroy the Arms Trade! DSEi - the world&rsquo;s largest arms fair - is back on at the ExCel Centre, Docklands, East London, September 8th-11th. Demo on 8th at a location in London - <a href="http://www.dsei.org" target="_blank">www.dsei.org</a> <br />  <br /> August 16th - DSEi Public Meeting - Stop the arms before they get to DSEi Arms Fair. Call for actions at local arms dealers/factories in fortnight running up to the DSEi Arms Fair at the ExCel Centre. 2pm, Calthorpe Arms, Grays Inn Rd, London, WC1X</font> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=occupation&source=687">occupation</a>, <span style='color:#777777; font-size:10px'>south korea</span>, <span style='color:#777777; font-size:10px'>ssangyong</span>, <a href="../keywordSearch/?keyword=workers+struggles&source=687">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(461);
			
				if (SHOWMESSAGES)	{
				
						addMessage(461);
						echo getMessages(461);
						echo showAddMessage(461);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>