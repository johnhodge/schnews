<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 749 - 25th November 2010 - New Kids on the Black Block</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, austerity, students, brighton, universities" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 749 Articles: </b>
<b>
<p><a href="../archive/news7491.php">New Kids On The Black Block</a></p>
</b>

<p><a href="../archive/news7492.php">Money For Old Europe</a></p>

<p><a href="../archive/news7493.php">Adertorial Politics</a></p>

<p><a href="../archive/news7494.php">Dear Reader....</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/749-Student-protest-lge.jpg" target="_blank">
													<img src="../images/749-Student-protest-sm.jpg" alt="Brighton Students Keen to Shwo Off Results of Self-Directed Learning..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Thursday 25th November 2010 | Issue 749</b></p>

<p><a href="news749.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NEW KIDS ON THE BLACK BLOCK</h3>

<p>
<p>
	<strong>AS BRITAIN&rsquo;S SCHOOLCHILDREN AND STUDENTS TAKE TO THE STREETS</strong> </p>
<p>
	The second wave of student protest this week saw thousands walkout of their classrooms and on to the streets all over the country. </p>
<p>
	Whilst events in London were amply covered &ndash; albeit traditionally skewed - by the mainstream media, SchNEWS was braving it on the frontline at the Brighton demo where around 3,000 students marched through the city centre. </p>
<p>
	The demo began around 2pm when around 1000 school-bunkers loitering in Dyke Road Park were joined by groups that had walked out from local universities and colleges (as well as the obligatory Trot paper sellers and anarchist infiltrators). The atmosphere was jovial but determined, with a higher proportion of face paint and animal costumes than at your average demo. A sound system added to the carnivalesque atmosphere. </p>
<p>
	<strong>HIJACK-A-NORY</strong> </p>
<p>
	Once the crowd had made their way into town they made it clear they were not going to stop on command. Or, as Chief Superintendent Graham Bartlett put it, after protesters &ldquo;<em>peacefully followed the agreed route...a group of protesters who were unconnected with the original march seemed to have hijacked the protest</em>&rdquo; - keeping to the official, national narrative of a small minority of disruptive troublemakers and conveniently ignoring the fact that the vast majority then went on to target several key buildings throughout the city. </p>
<p>
	Most demonstrators were clear on what they think is at stake if the rise in fees goes ahead. Typical was,&ldquo;<em>They&rsquo;re unfair, and it&rsquo;s sad. Students will have to seriously think about whether they can afford to go to university. Your university days are supposed to broaden your horizons, and a lot of people in the future are going to miss out on that</em>.&rdquo; </p>
<p>
	One Brighton University student commented: &ldquo;<em>They don&rsquo;t think cuts through, they just do them and don&rsquo;t think about the consequences</em>.&rdquo; </p>
<p>
	A first attempt to occupy Grand Parade, a Brighton Uni building conveniently located 10 metres from the official end point of the protest, was bungled with the main bloc telegraphing its intentions thus allowing staff and police to block and lock the doors. Luckily enough (like an educational version of Starbucks), there is another campus building just down the road which several agitating undergrads were able to occupy, draping their fabric signs over the balcony and at the window. </p>
<p>
	<strong>A TRIP TO THE PRIORY</strong> </p>
<p>
	Meanwhile, another bid for occupation was made at the Priory House council building next to the Town Hall. Several students made it inside but were removed by police shortly after. </p>
<p>
	The police kettled around 200 demonstrators outside the Hall in Bartholomew square and brought out the dogs. Various overheard conversations suggested that the teenage protesting newbies were shocked and angered by the police&rsquo;s repressive tactics. Some protesters managed to make an escape from the containment through a side alley before a few scuffles ensued as cops moved in to block it off. </p>
<p>
	Another splinter group had made their way to the police station, where a clash took place between a number of protesters and some particularly violent police; clearly shaken and surprised that they should be a target for protesters they had spent the past few hours manhandling. A hysterical mother looked on as her 15-year-old son was aggressively bundled to the ground. He was eventually released on police bail on suspicion of assault police, presumably for the damage done to police boots and batons inflicted by his head during the arrest. </p>
<p>
	In a final bid to turn the next generation against them, the police then formed another kettle in the North Laine area around a group that appeared to largely consist of confused under-18s. Meanwhile about 40 protesters stormed a branch of (tax-dodging) Vodafone. Riot cops cleared the store, only for them to cross the road and budget-ransack Poundland for out of date biscuits and plastic crap. </p>
<p>
	Overall six people were arrested, five of them school students between the ages of 15-16. The occupation of Pavilion Parade, Brighton Uni&rsquo;s humanities building, is still (Thursday night) ongoing. Numbers are fluid, but approximately 40 students are maintaining a constant presence in one lecture theatre; screening films, holding meetings and entertaining the local media. A few NUS reps brought food but, when pressed, sat awkwardly on the fence unwilling to either upset the rabble or contradict their glorious leader Aaron Porter (NUS president and Labour activist) who accused student groups of &ldquo;<em>naively and opportunistically [aligning] themselves with the anarchists</em>&rdquo;. </p>
<p>
	<strong>MASS = ENERGY</strong> </p>
<p>
	With similar, and more destructive, scenes of &lsquo;kid-power&rsquo; breaking out in the capital and around the country this week, it has all left the police somewhat bemused. Used to painting all &lsquo;trouble&rsquo; as down to the agitation of a few bad (cl)eggs, this new wave of protesters doesn&rsquo;t fit their &lsquo;domestic extremism&rsquo; mould. </p>
<p>
	In Brighton, cops made remarks to seasoned activists - who&rsquo;d merely turned up cos it seemed like something interesting might be going on - which clearly implied they thought it was all orchestrated by them, and how sick it was to be using the kids like that. If only! This has been a genuine grassroots mass uprising of the type that never seemed that likely until recently. Not to mention the sparsely attended SchNEWS campaign table at fresher fairs in recent years... </p>
<p>
	The deep anger and readiness to express it is all bubbling up from previously more &lsquo;mainstream&rsquo; apolitical youth, but it&rsquo;s not all that surprising; these kids are realising they are victims of such large-scale financial and environmental generational theft that they now have so little to lose they may as well really go for it. </p>
<p>
	How long before the authorities realise that they now have a genuine mass movement on their hands? One which no amount of attempted marginalising, illegal and violent policing and empty soundbites on &lsquo;fairness&rsquo; is going to suppress. (Sadly, they&rsquo;re bound to give all those things a good go in the meantime.) </p>
<p>
	The second day of action is planned for Tuesday 30th November. In the meantime keep a watchful eye on <a href="http://www.educationactivistnetwork.wordpress.com" target="_blank">www.educationactivistnetwork.wordpress.com</a> <a href="http://</p>
<p>
	www.conventionagainstfeesandcuts.wordpress.com" target="_blank"></p>
<p>
	www.conventionagainstfeesandcuts.wordpress.com</a> and <a href="http://www.anticuts.com" target="_blank">www.anticuts.com</a> for further fun and games. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1021), 118); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1021);

				if (SHOWMESSAGES)	{

						addMessage(1021);
						echo getMessages(1021);
						echo showAddMessage(1021);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


