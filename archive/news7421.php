<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 742 - 8th October 2010 - Frontier Law</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, no borders, brussels, police brutality, immigration, refugees" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 742 Articles: </b>
<b>
<p><a href="../archive/news7421.php">Frontier Law</a></p>
</b>

<p><a href="../archive/news7422.php">Red Leicester</a></p>

<p><a href="../archive/news7423.php">It's In Their Make-up</a></p>

<p><a href="../archive/news7424.php">Cut It Out!</a></p>

<p><a href="../archive/news7425.php">Fur Crying Out Loud</a></p>

<p><a href="../archive/news7426.php">Dam Squatters</a></p>

<p><a href="../archive/news7427.php">Itt's Hammertime</a></p>

<p><a href="../archive/news7428.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 8th October 2010 | Issue 742</b></p>

<p><a href="news742.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FRONTIER LAW</h3>

<p>
<p>
	<strong>EYEWITNESS REPORT FROM NO BORDERS DEMONSTRATION IN BRUSSELS</strong> </p>
<p>
	The No Borders camp in Brussels last week persevered in getting their message across by means of various direct actions despite widespread arbitrary arrests and shocking police violence, including physical and sexual abuse in police custody. At least 500 mostly &lsquo;preventative&rsquo; arrests took place, and 14 people were seriously injured. Here&rsquo;s a day by day report... </p>
<p>
	<strong>SATURDAY 25th</strong> </p>
<p>
	The camp officially began on the site of an old railway station, now a huge development called &ldquo;Tour and Taxis&rdquo;. Workshops also took place at Gesu, a large squatted monastery housing migrant families and activists, a thirty minute walk from camp. </p>
<p>
	<strong>SUNDAY 26th</strong> </p>
<p>
	The first demonstration kicked off at 2pm with a visit to an allegedly closed detention centre near Nossegem Station. The noise demo was in commemoration of Semira Adamu, killed in 1998 by police deporting her. </p>
<p>
	Police used horses, a water cannon and a barbed-wire fence to block protesters&rsquo; access to the &lsquo;closed&rsquo; centre, where migrants were visible at windows, apparently trying to communicate. </p>
<p>
	Those coming by train from the camp were blocked, frisked by police and had their cars searched. 100 people were kettled for an hour, then released on condition of bag searches and forced to show their faces to a video camera. Police violence escalated during the demonstration at the centre, where a girl lying on the floor was kicked in the head by police and hospitalised. A bottle was thrown and hit a cop on the cheek. In retaliation, police grabbed a man from the crowd at random, beat him so badly his eardrum burst, and arrested him. All 17 arrestees were later released without charge. </p>
<p>
	<strong>WEDNESDAY 29th</strong> </p>
<p>
	A Trades Union demo of 100,000 people marched against austerity cuts and ECOFIN (see <a href='../archive/news741.htm'>SchNEWS 741</a>). A space at the back of the demo was pre-arranged and cleared for No Borders to join the march in their own anti-capitalist block. </p>
<p>
	Determined not to allow No Borders to join the demo, police swept Brussels, arresting anyone suspected of being a camper. Mass &ldquo;preventative arrests&rdquo; clocked up to around 350. Inside the big police station at Ixelles many people were detained together in a large hall and later moved into cells. Inside people chanted and resisted with many removing their plastic handcuffs and damaging cells. Toilets and sinks were ripped off and bricks removed from walls. Police were unable to close some of the doors due to damage. </p>
<p>
	Using the distraction of the demo to their advantage, 30 activists blockaded a Frontex (the EU border security agency) conference for an hour before being removed by police. </p>
<p>
	<strong>THURSDAY 30th</strong> </p>
<p>
	Several autonomous actions took place, including BP&rsquo;s headquarters which was blockaded, plus The Royal Palace Hall, Ministry of Foreign Affairs and Greek Embassy spray-painted with slogans like &ldquo;You have blood on your hands - No Borders&rdquo;. </p>
<p>
	A planned direct-action &ldquo;game&rdquo; was cancelled due to the mass arrests the previous day. Many people seemed physically and mentally exhausted after a traumatic time in police custody. Many were held for 12 hours without food, water or charges. </p>
<p>
	Later, two women from camp were picked up by police while walking in the direction of the Gesu squat. While in custody they were forced to strip in front of male officers. One woman refused and had clothes physically ripped off her. They were later released, again without charge, in a highly distressed state. </p>
<p>
	<strong>FRIDAY 1st</strong> </p>
<p>
	The day of a planned demo. Police put a ban on any gathering of more than five people around Central Station from 3pm-6am. Police vans and water-cannons were at several locations near camp and few made it out without arrest. </p>
<p>
	In retaliation for the mass arrests and sexual violence, 30 people attacked a police station, breaking windows and doors, letting off smoke bombs and starting a fire. Police vans were also attacked with Molotovs. Six unrelated people caught up in the resulting turmoil were arrested. </p>
<p>
	According to an eyewitness, they were heavily mistreated inside the station with beatings, kickings, spittings and verbal abuse lasting several hours. At least one of the arrested was visibly injured and constantly asking for a doctor. Dozens of police were present; the abuse took place in front of the office of the head of the police station. </p>
<p>
	Despite the repression, several other actions took place: anti-Frontex banners and flyering at the airport, Frontex windows and doors smashed and smoke bombs were let off, locks glued at the International Organisation for Migration (IOM). A building of Sodexho - the French hotel and catering company with a 50% share in Corrections Corporation of Australia and UK Detention Services, had windows smashed and oil was spilled over another Sodexho building. Steria, the company that designed the Eurodac fingerprinting database - leading to thousands of migrants a year being deported - had its windows smashed and &ldquo;Smash Eurodac&rdquo; spray-painted across it. The Italian Embassy had excrement thrown over it. </p>
<p>
	<strong>SATURDAY 2nd</strong> </p>
<p>
	The official No Borders demonstration, negotiated with police in advance. Numbers started small due to more police repression, but swelled to around 1200 over the day. By 1.20pm the center of Molenbeek, a popular neighbourhood with lots of migrants, was reportedly under siege. Riot police patrolled with dogs and many streets were blocked. </p>
<p>
	Meanwhile, three activists visited a police recruitment event at the Commensurate in Ixelles, Brussels to question the abusive, sexist and corrupt behaviour of police. One of the activists had been arrested getting on a bus to the union demo three days earlier. He was held for almost 24 hours with no food or water, strip-searched, humiliated and his wallet containing 250 Euros was &ldquo;lost&rdquo; by police. A female activist confronted police about the sexual abuse of women arrestees that week. The public event was well attended and activists were dragged out by flustered police in front of a shocked audience and held for over 8 hours, again without food or water. Their dictaphone was taken and its memory erased. </p>
<p>
	Later, a man and woman out walking near camp were stopped by police. The woman was told she was going to be raped before being bundled into a van by five policemen and her hood pulled down over her head. She was released soon after on the other side of camp, highly distressed. </p>
<p>
	Back on site, the owner of Tour and Taxis had rented out half the camp space inside the big train station to a &ldquo;Brazilian dance party&rdquo; which was not part of No Borders. Heras fences were erected dividing the space in two. A knife fight in the audience meant police were called and took this opportunity to enter the camp in full riot gear with batons raised and dogs on leads, causing panic. Fortunately the camp&rsquo;s police liaison team, along with Tour and Taxis own security, persuaded them to leave without incident. </p>
<p>
	Throughout the week activists have been working on finding accommodation for a Roma family with six children and another on the way who were staying at the camp. The search continues. </p>
<p>
	The camp legal team and lawyers are appealing for testimonies and complaints of mistreatment and preventative arrests by police. They can be contacted at <a href="mailto:juridixnbc@vluchteling.be.">juridixnbc@vluchteling.be.</a> Other activists are also looking into taking action from their home countries. </p>
<p>
	Contact your local No Borders group for details. </p>
<p>
	* See <a href="http://www.noborderbxl.eu.org" target="_blank">www.noborderbxl.eu.org</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(964), 111); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(964);

				if (SHOWMESSAGES)	{

						addMessage(964);
						echo getMessages(964);
						echo showAddMessage(964);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


