<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 692 - 24th September 2009 - Junglist Missive</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, asylum seekers, immigration, calais, no borders" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 692 Articles: </b>
<b>
<p><a href="../archive/news6921.php">Junglist Missive</a></p>
</b>

<p><a href="../archive/news6922.php">Ffos'n' & Fightin'</a></p>

<p><a href="../archive/news6923.php">Tesco Alfresco</a></p>

<p><a href="../archive/news6924.php">Sean Free</a></p>

<p><a href="../archive/news6925.php">Co-mutiny Service</a></p>

<p><a href="../archive/news6926.php">Serb Your Enthusiasm</a></p>

<p><a href="../archive/news6927.php">Coal D-locks</a></p>

<p><a href="../archive/news6928.php">Glam Rocked</a></p>

<p><a href="../archive/news6929.php">Zelaya Cake</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/692-jungle-mania-lg.jpg" target="_blank">
													<img src="../images/692-jungle-mania-sm.jpg" alt="Jungle Mania" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Thursday 24th September 2009 | Issue 692</b></p>

<p><a href="news692.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>JUNGLIST MISSIVE</h3>

<p>
<strong>REPORTS FROM CALAIS AS POLICE EVICT THE JUNGLE MIGRANT CAMP</strong> <br />  <br /> In a move that left home secretary Alan Johnson &ldquo;delighted&rdquo;, the long predicted clearance of the migrant camp in Calais (see <a href='../archive/news682.htm'>SchNEWS 682</a>, <a href='../archive/news684.htm'>684</a>, <a href='../archive/news685.htm'>685</a>) took place on Tuesday (22nd). Around 500 French riot police moved into the &ldquo;jungle&rdquo; at 7.30am, bulldozing tents and seizing the migrants. <br />  <br /> At dawn that morning the remaining migrants of the camp had gathered around a fire where they waited for police with banners in English and Pastu, one proclaiming: &ldquo;<em>We need shelter and protection. We want asylum and peace. Jungle is our home</em>&rdquo;. <br />  <br /> No Borders activists confronted the advancing police, at one stage using a length of rope to form a human shield around a group of frightened teens. The police cut the rope and rushed in, knocking the activists and migrants to the ground, resulting in the dramatic footage that graced the national press. <br />   <br /> After the police had left, one activist described the scene: &ldquo;<em>The Afghan-Pashtun jungle has been completely razed - it&rsquo;s like a road-building site</em>.&rdquo; <br />  <br /> In total police arrested 278 migrants, 132 of them minors. Those of them that have been released have described how police took away their money, phones and clothes. <br />  <br /> Although some have been sent as far away as Toulouse, a third of the migrants have already been returned to the streets of Calais. They now have no possessions, nothing to eat and nowhere to stay. No Borders activists are remaining in the area, with one saying &ldquo;<em>We&rsquo;re busy trying to help people rebuild the camps - we&rsquo;ve brought loads of tents and tarps over from the UK</em>.&rdquo; <br />  <br /> Another described the situation: &ldquo;[it] <em>seems to be calming down now &ndash;  with less and less police visible &ndash; but our work load has been ramped up because of the situation these people have been left in. <br />  <br /> There are seven or eight other camps around Calais but at the moment there doesn&rsquo;t seem to be a move towards evicting them, which suggests that the whole thing was a P.R stunt</em>.&rdquo; <br />  <br /> The situation in Calais, a short ferry hop away, remains one issue where British activists can make a real difference to a horrendous situation perpetuated by our government. One activist summed it up, saying &ldquo;<em>What we&rsquo;re doing here makes a huge difference, when we&rsquo;re there and shit happens, police back off a little, but because the journalists are interested in what we do and they can legally film the cops, it can calm the situation right down &ndash; we&rsquo;re making it a story</em>&rdquo;. <br />  <br /> There have been solidarity demos in Wales, Manchester, London and Brighton. <br />  <br /> * For information on going to Calais ring (from UK) 00 33 63481071 or email <a href="mailto:calaisolidarity@gmail.com">calaisolidarity@gmail.com</a> <br />  <br /> * For updates <a href="http://see:www.calaismigrantsolidarity.wordpress.com" target="_blank">see:www.calaismigrantsolidarity.wordpress.com</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=asylum+seekers&source=692">asylum seekers</a>, <a href="../keywordSearch/?keyword=calais&source=692">calais</a>, <a href="../keywordSearch/?keyword=immigration&source=692">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=692">no borders</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(502);
			
				if (SHOWMESSAGES)	{
				
						addMessage(502);
						echo getMessages(502);
						echo showAddMessage(502);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>