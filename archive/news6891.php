<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 689 - 4th September 2009 - Common People?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, camp for climate action, climate change, tar sands, london, bp, shell" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.rts.gn.apc.org/"><img 
						src="../images_main/rtf-05-banner.jpg"
						alt="Reclaim the Future"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 689 Articles: </b>
<b>
<p><a href="../archive/news6891.php">Common People?</a></p>
</b>

<p><a href="../archive/news6892.php">Factory Finish</a></p>

<p><a href="../archive/news6893.php">Herculean Task</a></p>

<p><a href="../archive/news6894.php">Circus Freaks</a></p>

<p><a href="../archive/news6895.php">Hill Fought</a></p>

<p><a href="../archive/news6896.php">Inside Schnews</a></p>

<p><a href="../archive/news6897.php">Dsei-ing With Death</a></p>

<p><a href="../archive/news6898.php">Brum Punch</a></p>

<p><a href="../archive/news6899.php">Arm-a-geddon Myself Some Of That</a></p>

<p><a href="../archive/news68910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/689-Hell-centre-lg.jpg" target="_blank">
													<img src="../images/689-Hell-centre-sm.jpg" alt="Hell Centre" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 4th September 2009 | Issue 689</b></p>

<p><a href="news689.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COMMON PEOPLE?</h3>

<p>
<strong><font size="2">AS SCHNEWS REPORTS BACK FROM THE CLIMATE CAMP AT BLACKHEATH</font></strong> <br />  <br /> After the famous scuffling of Kingsnorth camp last year (see <a href='../archive/news641.htm'>SchNEWS 641</a>) and the attacks on the Bishops gate gathering at the G20 (see <a href='../archive/news672.htm'>SchNEWS 672</a>), the <strong>2009 Camp for Climate Action on Blackheath Common</strong> in South London was almost suspiciously cop-free, despite the best efforts of extremist pro-police factions within the camp to invite them on site. <br />   <br /> On Wednesday 26th August, groups of campers converged on the common from different meet-up points in London to occupy the site, which was swiftly enclosed in security fencing and covered with tents and marquees. The complex consensus decision making process of organising the camp ran pretty smoothly &ndash; perhaps it&rsquo;s more suited to day-to-day camp business than to planning mass actions as in previous years. The camp was divided into neighbourhoods with their own kitchens and daily meetings that sent two &lsquo;spokes&rsquo; to carry the neighbourhoods&rsquo; decisions to a general site meeting. <br />   <br /> The main contentious subject was people from the police liaison group wanting to invite the police on site, even after the police said they didn&rsquo;t need to and camp consensus had already been made against it (on the other hand wearing a black hoody and mumbling the words to Harry Roberts when yer too pissed to stand up doesn&rsquo;t a revolution make either). <br />  <br /> With the absence of mass action and police hassle, the atmosphere was more relaxed than in previous years, with the emphasis on the organisation of the site as an experimental autonomous community, workshops, a series of smaller actions, and planning for the &lsquo;Swoop&rsquo; mass action in October, as well as preparation for the climate summit in Copenhagen coming up in December. <br />  <br /> <strong><font size="2">OVER THE HEDGE</font></strong> <br />  <br /> It would be an understatement to describe the policing as hands-off. Even the invasion of the hallowed capitalist ground of Canary Wharf was only observed from a safe distance by a handful of sulky goons from the Met. Campers were prevented from invading the Canary Wharf building itself by an unbreachable line of journalists, then moved on to a demo outside Barclays Bank, funders of arms trade and climate criminals. The march was joined by an extremely drunk hedge funder who insisted on standing at the front shouting, &ldquo;<em>I&rsquo;m right here with you guys... what is it you believe in?</em>&rdquo; He was invited back to the camp but wandered off during a lengthy consensus meeting about whether to continue pointlessly blocking three cars on a minor road. Nice one JH, hope your boss doesn&rsquo;t see the photos! <br />  <br /> <strong><font size="2">BOTTOM OF THE BARREL</font></strong> <br />  <br /> Despite this year&rsquo;s Climate Camp&rsquo;s focus on equipping its supporters with the knowledge and the skills to perform direct action in the future, rather than mobilising the entire camp to act during the week, many autonomous affinity groups and neighbourhoods took it upon themselves to venture out into the city of London and target some of the biggest climate criminals. <br />  <br /> One of the lucky recipients was every 4x4 driver&rsquo;s best friend, BP. The company is one of the biggest businesses involved in the extraction of oil from the tar sands of Alberta, Canada (See <a href='../archive/news644.htm'>SchNEWS 644</a>). It takes up to 5 litres of water to produce 1 litre of usable petrol from the tar sands and the extraction process can produce up to five times as much CO2 as regular oil. <br />  <br /> Increasing rates of rare cancers and respiratory diseases have been reported among the native peoples living alongside the river, as well as high levels of arsenic and other metals found in the groundwater due to leaking from the toxic &lsquo;tailing ponds&rsquo; produced as a waste product from the process. These ponds are so large they can be seen from space and the impact on the local ecosystem is disastrous. <br />  <br /> On Tuesday (1st) the protest march started outside the National Portrait Gallery, which BP has been financing as part of a &lsquo;greenwash&rsquo; strategy in an attempt to clean up its image. Campaigners then moved to the Canadian embassy which seems to have little concern over climate change, pulling out of the Kyoto treaty and acting all-systems-go for tar sands extraction. Before moving on to Shell&rsquo;s London headquarters the march arrived at BP&rsquo;s main office and managed to give them a taste of what it was like to have your doorstep polluted by oil, and fittingly, as the protesters finished up at Shell, the &lsquo;S&rsquo; from their sign spontaneously dropped from the building, leaving it reading &ldquo;<strong>Hell Centr</strong>e&rdquo;. The bricks and mortar obviously have more of a conscience than the planks sitting inside. <br />  <br /> Aside from this action other groups organised themselves to demonstrate against other worthy targets. Monday (31st) kicked off the action with a flashmob at City Airport against the proposed expansion, and saw a protest outside HM Treasury. <br />  <br /> As well as the tar sands gathering, on 1st September activists from the camp met at Endelman PR, the company responsible for the greenwashing campaign of E.On, one of the biggest enthusiasts for building new coal fired power stations in the UK. Another group blockaded RBS and managed to gain entrance and superglue themselves inside the mega-bank&rsquo;s offices. <br />  <br /> Wednesday (2nd) saw campaigners get into the Dept. for Energy and Climate Change, blocking the entrance with a canoe while others delivered a message to E.On letting them know that their Ratcliffe power station had been named for the action in October. An organisation distinguishing themselves from Climate Camp called Don&rsquo;t Build Kingsnorth hung a banner and went inside the buildings of  Laing O&rsquo;Rourke, to hand out leaflets and talk to staff. This mob is one of the construction companies bidding for the contract to build the controversial coal-fired energy plant. <br />  <br /> <strong><font size="2">CLASS ACT?</font></strong> <br />  <br /> So was Climate Camp London 2009 a success? Many of the more direct action focused campaigners felt frustrated with the separation of action planning from workshops on education and training, and the postponing of mass action until October. Climate Camp seems to have gained a serious foothold in mainstream media culture, a huge turnaround from the victimisation and violence protesters have suffered in previous years. Is it the case that this is a positive development as more people learn about the issues and public becomes more radical, or does it represent a softening of Climate Camp&rsquo;s stance? <br />   <br /> What remains to be seen is whether the newcomers  this year will have been inspired enough by the process to pull their fingers out and move for direct action in the coming months. <br />   <br /> The biggest danger Climate Camp now faces is that it will turn into a purely intellectual exercise, and anyone who has been involved with the more serious side of the campaign knows that climate chaos protests are not all about painting banners and making straw bale benches. Ultimately the point of direct action is that when intellectualism fails, (and it has up until now, think governments refusing to put policy in line with scientific recommendations), it's about getting in there and fighting back, to make life difficult for those at the top. We don't want to get too middle class now do we..? <br />  <br /> * See <a href="http://www.climatecamp.org.uk/" target="_blank">www.climatecamp.org.uk/</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>bp</span>, <a href="../keywordSearch/?keyword=camp+for+climate+action&source=689">camp for climate action</a>, <a href="../keywordSearch/?keyword=climate+change&source=689">climate change</a>, <a href="../keywordSearch/?keyword=london&source=689">london</a>, <a href="../keywordSearch/?keyword=shell&source=689">shell</a>, <span style='color:#777777; font-size:10px'>tar sands</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(474);
			
				if (SHOWMESSAGES)	{
				
						addMessage(474);
						echo getMessages(474);
						echo showAddMessage(474);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>