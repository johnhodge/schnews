<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 758 - 11th February 2011 - EDL Go To Hell, Well Luton</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, racism, luton, anti-fascists" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 758 Articles: </b>
<p><a href="../archive/news7581.php">The Withdrawal Method</a></p>

<b>
<p><a href="../archive/news7582.php">Edl Go To Hell, Well Luton</a></p>
</b>

<p><a href="../archive/news7583.php">Egypt: Tahrir We Go Again</a></p>

<p><a href="../archive/news7584.php">Greece: Hunger Strikes</a></p>

<p><a href="../archive/news7585.php">Greece: Trash Talk</a></p>

<p><a href="../archive/news7586.php">Harper Scarper</a></p>

<p><a href="../archive/news7587.php">Shopped Cops Dropped</a></p>

<p><a href="../archive/news7588.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th February 2011 | Issue 758</b></p>

<p><a href="news758.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDL GO TO HELL, WELL LUTON</h3>

<p>
<p>
	The English Defence League held their biggest demo to date in Luton on Saturday 5th There were estimates of up to three thousand in attendance. However in a marked contrast to earlier outings, there was little drunken violence and the vast majority were on their coaches and away by the time the whistle blew at four p.m. There are reports that two houses were targeted later that night but there was no EDL mass break-out. </p>
<p>
	Bedfordshire police are estimated to have spent &pound;800,000 on putting over a thousand cops on the street. The EDL were, in what is now becoming common practice, allowed a short march while anti-fascists, under the auspices of Unite Against Fascism were held in a static demonstration in Park Square. The rest of Luton seems to have stayed at home for the day and the streets in the town centre were deserted </p>
<p>
	There were scuffles at the train station early in the morning as EDL and UAF mingled on the platforms but no outright kick-offs. Around two hundred anti-fascists occupied the train station entrance until forced to move by the cops. UAF types later broke out of their kettle and running through back streets made it to the Bury Park area. </p>
<p>
	Previous EDL efforts in Luton, billed by the far-right group as &lsquo;where it all began&rsquo;, saw a rampaging mob smashing windows in what is effectively Luton&rsquo;s Muslim quarter in Bury Park.(see <a href='../archive/news670.htm'>SchNEWS 670</a>). This time round in response the Muslim youth were out in their hundreds ready to repel any attack. Shopkeepers defiantly kept their premises open. Although the atmosphere was nervous at first, the united response when one car- driver chose to shout &lsquo;EDL&rsquo; at the mass (and then presumably drove straight to the nearest panel-beaters), made it clear that any incursion would be steadfastly resisted. </p>
<p>
	Stewards, mostly recruited through local mosques, attempted to control the crowd and conform to the police plan of keeping anti-racists and the Muslim/Asian community apart but the majority of the crowd welcomed those who had come to stand with them. </p>
<p>
	Hot on the heels of their biggest demo to date, the EDL - in the shape of Tommy Robinson/Steven Yaxley-Lennon -, announced that they&rsquo;re going to become a political party. Ignoring the fact that Britain already has it&rsquo;s fair share of ballot-box Nazis, the pro-police tanning salon-owner Robinson, increasingly seen to be the EDLs leader, is determined to split the far-right vote. Intoxicated with his Newsnight appearance, he announced that Nick Griffin is washed up and that if he (Tommy) appeared on Question Time, then he&rsquo;d have 10,000 supporters outside. </p>
<p>
	This news was carried uncritically in a front-page puff-piece in the Daily Star who have, under the auspices of editor and pornographer Richard Desmond, decided to become the EDLs flag-bearers. Quite why the one-time owner of Asian Babes magazine wants to throw his weight behind the EDL is anyone&rsquo;s guess. </p>
<p>
	Meanwhile not all the rank and file are happy with Tommy&rsquo;s meteoric rise - there are unconfirmed reports that he had to be escorted away by police after being punched by another leaguer. Apparently they weren&rsquo;t too happy at his telling them to &lsquo;fucking calm down&rsquo; on the megaphone. </p>
<p>
	Meanwhile the EDL have announced their next outing - Birmingham on March 19th. SchNEWS spoke to one anti-fascist who said, &ldquo;The EDL bandwagon is a dangerous one - at the moment they have laid claim to a single emotive issue - Islamic radicalism. They use this to push a broader racist agenda, one that attacks all racial minorities. They had a big turn-out today but remember that they&rsquo;ve been building for this since November - when they were out every few weeks the turn outs were much lower. They can and must be resisted.&rdquo; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1097), 127); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1097);

				if (SHOWMESSAGES)	{

						addMessage(1097);
						echo getMessages(1097);
						echo showAddMessage(1097);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


