<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 676 - 22nd May 2009 - War That Broke The Tamils' Back</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tamil tigers, sri lanka, war, human rights, rajapaksa, white phosphorous, refugee camp, prabhakaran" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 676 Articles: </b>
<p><a href="../archive/news6761.php">Final Frontiers</a></p>

<p><a href="../archive/news6762.php">Not A Blind Brit Of Difference</a></p>

<b>
<p><a href="../archive/news6763.php">War That Broke The Tamils' Back</a></p>
</b>

<p><a href="../archive/news6764.php">Now We're Rolling</a></p>

<p><a href="../archive/news6765.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd May 2009 | Issue 676</b></p>

<p><a href="news676.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WAR THAT BROKE THE TAMILS' BACK</h3>

<p>
Following battles which have raged since late last year (see <a href='../archive/news672.htm'>SchNEWS 672</a>, <a href='../archive/news665.htm'>665</a>), the Sri Lankan government declared victory in its 26 year war against the separatist Liberation Tigers of Tamil Eelam &ndash; the Tamil Tigers - this Tuesday (19th). After squeezing the remaining rebels into an ever smaller area the declaration came after the death of the Tigers&rsquo; leader Velupillai Prabhakaran whose corpse was paraded on TV the same day. <br />  <br /> The announcement followed weeks of intensifying fighting with rapidly escalating casualty figures. The area designated by the Sri Lankan army as a &lsquo;safety zone&rsquo; for escaping civilians has been shelled repeatedly in attacks which included the repeated shelling of several hospitals, killing hundreds of wounded civilians. The Sri Lankan army denies attacking the area despite massive evidence that they have repeatedly used heavy artillery, air strikes, cluster bombs and white phosphorous on Tamil civilians. Health workers in the area estimate that up to 15,000 people, including 2000 children, have died in the last few months of fighting. <br />  <br /> Tamils fleeing the conflict area have come come under fire from both the Sri Lankan army and LTTE fighters. Around a quarter of a million of those that have made it out have been shepherded into heavily policed government internment camps. On Wednesday the Sri Lankan government announced that the majority of the refugees will remain in the camps, where conditions are said to be desperate, for up to two years. Thousands of former LTTE fighters, some as young as fourteen, are being held in &lsquo;rehabilitation centres&rsquo; while the &lsquo;hardcore&rsquo; rebels are being interrogated in a secure camp in the South. In the Sinhalese  areas of the country ethnic Tamils are now also being detained with increasing regularity. <br />  <br /> The Tamil diaspora meanwhile has continued to express its outrage at the actions of the Sri Lankan army and the silence of the international community, with protests from South Africa to the US. In London, demonstrations have continued throughout the week with thousands of Tamils protesting in and around Parliament Square. With the protests barely acknowledged in the national press the police have been free to respond in an unrestrained fashion. Last week a blockade of Westminster Bridge was ended when police were said to have seized the front-line protesters &ndash; children in pushchairs &ndash; and removed them to nearby police vans, forcing the parents to follow. <br />  <br /> In his victory address to parliament Sri Lankan president Mahinda Rajapaksa stated the country had been  &ldquo;liberated from terrorism&rdquo; and declared a national holiday. &ldquo;<em>Our intention was to save the Tamil people from the cruel grip of the LTTE</em>.&rdquo; he said. &ldquo;<em>We all must now live as equals in this free country</em>,&rdquo;. With hundreds of thousands of suffering and bereaved Tamils trapped in razor wired refugee camps and the Tamil diaspora still demanding justice, Rajapaksa&rsquo;s triumphalism seems as misplaced as Bush&rsquo;s &ldquo;<strong>Mission Accomplished!</strong>&rdquo; moment...&nbsp; <br />  <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>human rights</span>, <span style='color:#777777; font-size:10px'>prabhakaran</span>, <span style='color:#777777; font-size:10px'>rajapaksa</span>, <span style='color:#777777; font-size:10px'>refugee camp</span>, <a href="../keywordSearch/?keyword=sri+lanka&source=676">sri lanka</a>, <a href="../keywordSearch/?keyword=tamil+tigers&source=676">tamil tigers</a>, <span style='color:#777777; font-size:10px'>war</span>, <span style='color:#777777; font-size:10px'>white phosphorous</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(331);
			
				if (SHOWMESSAGES)	{
				
						addMessage(331);
						echo getMessages(331);
						echo showAddMessage(331);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>