<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 671 - 3rd April 2009 - Storming The Ramparts</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, g20 summit, direct action, london, squatting, convergence centre, ramparts, earl st, fit team, social centres" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 671 Articles: </b>
<p><a href="../archive/news6711.php">Bank Statement</a></p>

<p><a href="../archive/news6712.php">Crapping Arrest Of The Week</a></p>

<p><a href="../archive/news6714.php">Boiling Kettle</a></p>

<p><a href="../archive/news6715.php">Tents Affair</a></p>

<b>
<p><a href="../archive/news6716.php">Storming The Ramparts</a></p>
</b>

<p><a href="../archive/news6717.php">Reaching The Summit</a></p>

<p><a href="../archive/news6718.php">Ravens Art</a></p>

<p><a href="../archive/news6719.php">Spanners In The Works</a></p>

<p><a href="../archive/news67110.php">Nato Summit  Strasbourg</a></p>

<p><a href="../archive/news67111.php">Creepy Crawley</a></p>

<p><a href="../archive/news67112.php">Mal Maison</a></p>

<p><a href="../archive/news67113.php">Lady Of The Lakenheath</a></p>

<p><a href="../archive/news67114.php">Edo Briefs</a></p>

<p><a href="../archive/news67115.php">Right Off</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd April 2009 | Issue 671</b></p>

<p><a href="news671.htm">Back to the Full Issue</a></p>

<div id="article">

<div style='font-weight:bold; font-size:13px'><div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 G20 Summit Eyewitness Report 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div></div>

<h3>STORMING THE RAMPARTS</h3>

<p>
Two London squat centres were raided yesterday (Thursday) as police took a swipe at the infrastructure of the G20. The long term squatted social centre Ramparts and the G20 convergence centre at  Street in the City were both raided on Thursday at midday in coordinated operations.  <br />
 <br />
The police had kept Earl Street residents awake for much of Wednesday night, banging on the doors and windows and switching their sirens on outside. In the morning they had been searching anyone coming or going and demanding people's names and addresses.  <br />
SchNEWS eyes on the ground at Earl St told us "<i>At around midday I heard 'they're coming in, they're coming in,' we all ran upstairs to the second level, people were getting panicked, some people split to the front of the room, others of us stayed where we were and decided to sit down with our hands in the air.</i>"  <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 280" width="280px"><a href="../images/671-g20-3-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/671-g20-3-sm.jpg"  /></a></td></tr></table>As dozens of police battered their way through the door, 30-40 activists congregated on the top floor and "sat in a circle with our arms in the air to show the police our non-violent intentions" However, cops in full riot gear brandishing taser guns forced them face down with hands on the floor. People were brought out handcuffed one by one and sat on the pavement whilst police searched the building. Two were taken to hospital with head injuries inflicted by the police and two others were arrested.  <br />
 <br />
Other cops cordoned off the street while supporters of the detainees kept up a lively protest denouncing the illegal police actions. They were released after giving their details two hours later. At hand during the raid was the cops' latest toy, the urban military-style armoured Ford F450 vehicle. Although police allowed people in one by one to get their possessions, they emptied the building and started ripping up the floorboards. <br />
 <br />
Meanwhile over at RampArts, it was party time for the FIT throughout Thursday morning as a large number of bobbies gathered to stop and search all traffic in and out of the well known London squat. Anyone leaving the building that morning were pressed for information and filmed with glee by the FIT eager to make ID's and arrests. Shortly after midday, as Earls Street was busted open, bobbies in balaclavas smashed their way in through the roof, assaulting everyone in their way as batons and tasers were drawn. With an embedded ITN camera team in tow, they screamed at everyone to get down on the ground, jumping on them and handcuffing them, firing off insults and taunts like "<i>one of you croaked last night</i>". After checking IDs four were arrested including two who had been injured during the raid. The others were released and the centre remains open, see <a href="http://www.therampart.wordpress.com" target="_blank">www.therampart.wordpress.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>convergence centre</span>, <a href="../keywordSearch/?keyword=direct+action&source=671">direct action</a>, <span style='color:#777777; font-size:10px'>earl st</span>, <span style='color:#777777; font-size:10px'>fit team</span>, <a href="../keywordSearch/?keyword=g20+summit&source=671">g20 summit</a>, <a href="../keywordSearch/?keyword=london&source=671">london</a>, <span style='color:#777777; font-size:10px'>ramparts</span>, <span style='color:#777777; font-size:10px'>social centres</span>, <a href="../keywordSearch/?keyword=squatting&source=671">squatting</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(274);
			
				if (SHOWMESSAGES)	{
				
						addMessage(274);
						echo getMessages(274);
						echo showAddMessage(274);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>