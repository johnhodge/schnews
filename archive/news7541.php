<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 754 - 14th January 2011 - Fight Them on the Beeches</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, environment, deforestation, cuts" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 754 Articles: </b>
<b>
<p><a href="../archive/news7541.php">Fight Them On The Beeches</a></p>
</b>

<p><a href="../archive/news7542.php">Call Out Corner</a></p>

<p><a href="../archive/news7543.php">Keep It Civil</a></p>

<p><a href="../archive/news7544.php">That's End-detainment</a></p>

<p><a href="../archive/news7545.php">Ema-dness</a></p>

<p><a href="../archive/news7546.php">Roughed Up Diamond</a></p>

<p><a href="../archive/news7547.php">Bonneville Rendevous</a></p>

<p><a href="../archive/news7548.php">Bastard Offspring</a></p>

<p><a href="../archive/news7549.php">Extinguisher Not Let Off</a></p>

<p><a href="../archive/news75410.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th January 2011 | Issue 754</b></p>

<p><a href="news754.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FIGHT THEM ON THE BEECHES</h3>

<p>
<p>
	<strong>YOUR COUNTRY NEEDS YEW AS TORIES PLAN TO CUT NATIONAL FORESTS</strong> </p>
<p>
	Last week (see <a href='../archive/news753.htm'>SchNEWS 753</a>) we covered the protests against the sell-off of the Forest of Dean. But the implications of the Public Bodies Bill for the Forestry Commission&#8239; go a lot further than that.&#8239; Essentially the Tory&rsquo;s are planning (in time-honoured fashion) to flog off the family silver and privatise forests up and down the country. The sale is intended to raise &pound;2bn - less than half of one years tax avoidance by Vodafone. </p>
<p>
	At a recent parliamentary select committee DEFRA Minister Jim Paice said: &ldquo;<em>We wish to proceed with very substantial disposal of public forest estate, which could go to the extent of all of it.</em>&rdquo; The Forestry Commission currently controls around one million hectares. Ministers have said they &lsquo;<em>hoped the woodland would be bought by community groups and charities</em>&rsquo; but would also &lsquo;<em>be willing to sell the land to private UK or foreign companies</em>.&rsquo; </p>
<p>
	Well as we know, hope ain&rsquo;t much good against profit and already the investors are sniffing round looking at chipping the woods cheap bio-fuel for example. So now&rsquo;s the moment to fight the legions of Saruman before the UK&rsquo;s forests disappear up the chimney. </p>
<p>
	Crucially the forests are expected to be flogged off without any requirement to uphold the Forestry Commission&rsquo;s current commitment to replace rows of conifers with native broadleaf trees such as oak, beech, ash and lime. Defra is reluctant to reduce the sale value of the forests by adding conditions over the types of tree that must be planted. The Woodland Trust (<a href="http://lwww.woodlandtrust.org.uk" target="_blank">lwww.woodlandtrust.org.uk</a>) said that the value of each hectare would be cut by a third if any future owner was required to plant native trees and not just focus on maximising the profits. Sue Holden, the trust&rsquo;s chief executive, said: &ldquo;Ancient woodland is our richest and most fragile habitat, our equivalent of the rainforest. [Selling the sites] with no means of securing their restoration would mean a massive opportunity would be lost, probably forever.&rdquo; </p>
<p>
	<strong>ASH FOR CASH</strong> <br /> 
	&nbsp; <br /> 
	&nbsp;Ironically it&rsquo;s actually the UN&rsquo;s international year of the forest - and as the world slowly wakes up to the necessity of green cover just to keep us breathing, then Maggie&rsquo;s little helpers are reaching for the chainsaw. Of course, once you&rsquo;ve bought yer own little leafy glade then you&rsquo;ll still need planning permission before you can shout &lsquo;TIMBER!&rsquo; - but don&rsquo;t worry &lsquo;cos amongst the Tory policies is a promise to take a felling axe to red-tape of planning permission. </p>
<p>
	Of course it&rsquo;s not every new owner who&rsquo;s going to chop down the trees - some will want to fence them off and use them as handy little tax loophole. In the UK, profits and gains derived from commercial forestry are beyond the scope of taxation. This means although no allowances are available on expenditure, commercial forestry in the UK is free from income tax, capital gains tax (other than gains on the underlying land as opposed to the timber crop), corporation tax and, if held for more than two years, inheritance tax. - KER-CHING! (nicked from <a href="http://www.investment.co.uk" target="_blank">www.investment.co.uk</a>). </p>
<p>
	<strong>CEDER WRITING ON THE WALL</strong> </p>
<p>
	So, perhaps the trees and squirrels or whatever could carry on much as before - just under different ownership. But you won&rsquo;t see that because as the first thing private owners are likely to do is stick up a massive fence and a keep-out sign. </p>
<p>
	The Forestry Commission ain&rsquo;t perfect but there does at least seem to be some commitment to the idea of public ownership and access. Right-to-roam legislation only applies to &lsquo;freehold&rsquo; rather &lsquo;leasehold&rsquo; woodlands, meaning that new owners, depending on the terms of the sale, will be able to restrict access. And of course any time that a more profitable alternative reared its head they&rsquo;d have total freedom to change their minds and shut off access. </p>
<p>
	The abstract idea of &lsquo;selling forests to developers is bad&rsquo; is gathering weight nationally, but the fact that it means that patch of trees just outside town where you take yer dog for a walk is only just starting to trickle through. The hugely encouraging protest last week in the Forest of Dean (see <a href='../archive/news753.htm'>SchNEWS 753</a>) has made a good start, but there are still only a small number of local groups organising against the sell off. The Public Bodies Bill, which proposes the corporate takeover of woodland is currently at committee stage in the House of Lords, which basically means it&rsquo;s passed through the initial stages of debate and now amendments are being made. </p>
<p>
	It&rsquo;s still got to go through the House of Commons, so the time to sign the petition, hassle yer MP, and more importantly, build yerself a treehouse equipped with sab tools and D locks is now. <a href="http://</p>
<p>
	*www.saveourforests.co.uk" target="_blank"></p>
<p>
	*www.saveourforests.co.uk</a> <a href="</p>
<p>
	*http://www.facebook.com/pages/Save-Britains-Forests/157828020924281" target="_blank"></p>
<p>
	*http://www.facebook.com/pages/Save-Britains-Forests/157828020924281</a> </p>
<p>
	*Local campaigns we know about: Staffordshire: <a href="http://www.savecannockchase.org.uk/" target="_blank">http://www.savecannockchase.org.uk/</a> | Exeter: <a href="http://www.eclipse.co.uk/exeter/haldon/" target="_blank">http://www.eclipse.co.uk/exeter/haldon/</a> | Nottinghamshire: <a href="http://www.facebook.com/group.php?gid=8192305118" target="_blank">http://www.facebook.com/group.php?gid=8192305118</a> | East Anglia: <a href="http://www.fotfp.org.uk/fotfp_fc_campaign.shtml" target="_blank">http://www.fotfp.org.uk/fotfp_fc_campaign.shtml</a> | Gloucestershire: <a href="http://www.savetheforestofdean.co.uk/" target="_blank">http://www.savetheforestofdean.co.uk/</a> </p>
<p>
	*The petition: <a href="http://www.38degrees.org.uk/page/s/save-our-forests" target="_blank">http://www.38degrees.org.uk/page/s/save-our-forests</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1056), 123); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1056);

				if (SHOWMESSAGES)	{

						addMessage(1056);
						echo getMessages(1056);
						echo showAddMessage(1056);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


