<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 713 - 12th March 2010 - Sea Shepherd Roundup : Whaling Banshees</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, sea shepherd, direct action, whaling" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 713 Articles: </b>
<p><a href="../archive/news7131.php">Sussex, Lies And Videotape</a></p>

<p><a href="../archive/news7132.php">Schcoop! - No Cutbacks For Sussex Fat Cats</a></p>

<b>
<p><a href="../archive/news7133.php">Sea Shepherd Roundup : Whaling Banshees</a></p>
</b>

<p><a href="../archive/news7134.php">A-fence-is Weapon</a></p>

<p><a href="../archive/news7135.php">Hungry For Reform</a></p>

<p><a href="../archive/news7136.php">Hey Joe</a></p>

<p><a href="../archive/news7137.php">Coal-amity</a></p>

<p><a href="../archive/news7138.php">Tesco A No Go</a></p>

<p><a href="../archive/news7139.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th March 2010 | Issue 713</b></p>

<p><a href="news713.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SEA SHEPHERD ROUNDUP : WHALING BANSHEES</h3>

<p>
This week Sea Shepherd completed its most ambitious and effective campaign yet against illegal Japanese whaling in the Southern Ocean Whale Sanctuary. The three month mission - called Operation Waltzing Matilda - was the stuff of epic seafaring adventures, which included having a $1.5 million boat rammed and sunk by a Japanese whaler, a mid-sea collision, and one protester being detained aboard a whaler to be taken to Japan. The campaign managed to stop any whaling for 33 days of the three month whaling season. <br />   <br /> Previous years stopped the killing of half the whaler&rsquo;s quotas, but this year will definitely better those results and cost the whalers tens of millions in profit. <br />  <br /> This year Sea Shepherd started with three ships - the Steve Irwin, the Bob Barker - a former Norwegian whaling boat, which was initially able to sail incognito - , the hi-tech trimaram interceptor boat the Ady Gil, and a helicopter. On January 6th the Ady Gil was rammed by the harpoon vessel Shonan Maru 2, cutting it in half. The six crew members made a narrow escape with their lives (See <a href='../archive/news705.htm'>SchNEWS 705</a>). <br />   <br /> The captain of the Ady Gil, a New Zealander called Peter Bethune, later responded by riding a jet-ski under the cover of darkness and boarding the Shonan Maru 2 with the intention of issuing a citizens arrest warrant to its captain for the destruction of the Ady Gil and attempted murder of its crew, and slapping them with an invoice for the cost of the ship. He managed to remain on the ship overnight undetected, then approached the bridge of the ship in the morning. Bethune was then taken prisoner and, unbelievably, the harpoon ship was taken out of the whaling operation and sent back to Japan to deliver him to the authorities. It will soon arrive in Japan. Sea Shepherd have a legal team ready to represent Bethune. Every effort to sabotage the whalers amounts to more whales being saved and to tie up a whaling vessel like this is a great result for the campaign. Already the Shonan Maru 2 and another harpoon vessel, the Yushin Maru 3, were taken out of whaling roles and used as security for the larger giant floating whale abattoir, the ship Nisshin Maru. <br />  <br /> After losing the Ady Gil, it became harder to track the movements of the whalers in the vast ocean, but, as the Bob Barker was able to masquerade as a Norwegian whaler, it was used to catch the whalers by surprise. Together with the Steve Irwin it was able to get in behind the Nisshin Maru to prevent harpooners loading their catches. There were running water cannon fights as well as volleys of rotten butter stink bombs. Sea Shepherd protesters also sprayed red paint over the &lsquo;research&rsquo; signs which the whalers had painted on their sides (cos of course the Japanese are killing hundreds of giant whales for research purposes only). At one point the Yushin Maru 3 rammed the Bob Barker causing a one-metre gash, which was later repaired. <br />   <br /> Events unfold on the weekly reality tv documentary, Whale Wars, on Animal Planet cable channel - see <a href="http://animal.discovery.com/tv/whale-wars" target="_blank">http://animal.discovery.com/tv/whale-wars</a> <br />  <br /> * See <a href="http://www.seashepherd.org" target="_blank">www.seashepherd.org</a> <br />  <br /> * For a great first-hand account see <a href="http://www.indymedia.org.uk/en/2010/03/447043.html" target="_blank">www.indymedia.org.uk/en/2010/03/447043.html</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=713">direct action</a>, <a href="../keywordSearch/?keyword=sea+shepherd&source=713">sea shepherd</a>, <a href="../keywordSearch/?keyword=whaling&source=713">whaling</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(690);
			
				if (SHOWMESSAGES)	{
				
						addMessage(690);
						echo getMessages(690);
						echo showAddMessage(690);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


