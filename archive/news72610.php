<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 726 - 11th June 2010 - Edl Offside In Cardiff</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, wales, cardiff, antifa, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 726 Articles: </b>
<p><a href="../archive/news7261.php">Slick Publicity</a></p>

<p><a href="../archive/news7262.php">And So Itt Begins</a></p>

<p><a href="../archive/news7263.php">Calais Eviction Alert</a></p>

<p><a href="../archive/news7264.php">For Who's Benefit?</a></p>

<p><a href="../archive/news7265.php">Chilli Reception</a></p>

<p><a href="../archive/news7266.php">Hell And High Water</a></p>

<p><a href="../archive/news7267.php">Striking Blow</a></p>

<p><a href="../archive/news7268.php">Blowing A Raspberry</a></p>

<p><a href="../archive/news7269.php">Gas-tly Business</a></p>

<b>
<p><a href="../archive/news72610.php">Edl Offside In Cardiff</a></p>
</b>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th June 2010 | Issue 726</b></p>

<p><a href="news726.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDL OFFSIDE IN CARDIFF</h3>

<p>
After a total shambles in South Wales last autumn (see <a href='../archive/news696.htm'>SchNEWS 696</a>), the English Defence League attempted to hold a demo in Cardiff last Saturday (5th). Called under the &ldquo;Welsh Defence League&rdquo; name and heavily promoted by the mentally unstable nutjob Jeff Marsh*, the event was being talked up as a show of strength from the WDL who had predicted that they would be adding Cardiff City&rsquo;s notorious Soul Crew hooligans to the ranks of their followers. <br />  <br /> 

	As has been the case in other parts of the UK the UAF took it upon themselves to prop up the ailing careers of Labour MPs and confuse anti-EDL efforts by calling a march on the other side of the city. <br />  <br /> 

	Militant anti-fascists (largely from local Anarchist groups) gathered at the EDL appointed protest pen before their arrival. After a few hours Police managed to get militants away from the area and force them into the UAF rally that had gathered two blocks away to listen to speeches. UAF stewards welcomed their arrival by declaring them &ldquo;as bad as the WDL&rdquo; for aiming for confrontation. <br />  <br /> 

	However these stewards got increasingly worried when a number of other groups arrived, first a group of skinheads who were verbally abused for being &ldquo;nazis&rdquo; before the crowd spotted their S.H.A.R.P. (SkinHeads Against Racial Prejudice) insignia. Then another large group of men, this lot in their mid-30s in designer gear led by a well known local hooligan. As some sections of the crowd moved away, a minibus decked out in England flags drove passed and stopped at a red light. At this point all the recent arrivals showed that you don&rsquo;t need a purple placard to &ldquo;smash the EDL&rdquo;, just some well placed fists. <br />  <br /> 

	Over the next few hours the &pound;250,000 police operation was stretched to its fullest to get the EDL out of the city alive, with several organised groups attacking their mobile police pen from all sides. The distinct inability to get more than a dozen Welsh people on their demo of about 120 meant most of the EDL were displaying England flags and singing anti-welsh songs, repeatedly singing that we were &ldquo;not English anymore&rdquo; (I wasn&rsquo;t aware we ever were). With that attitude they were never going to get much of a positive reaction on a rugby international day! <br />  <br /> 

	There were four arrests, all of militant anti-fascists (some of whom have bail restrictions to not to associate with the UAF - as if!!!). In the aftermath of the day a number of Cardiff City footy fans (who the EDL have now &ldquo;declared war&rdquo; on) have started a Cardiff ANTIFA group see <a href="http://www.facebook.com/group.php?gid=123227311047223" target="_blank">www.facebook.com/group.php?gid=123227311047223</a> <br />  <br /> 

	* For an insight into the minds of the &lsquo;opposition&rsquo;, and a piss-yerself laugh, see Jeff Marsh&rsquo;s account of the day at <a href="http://casualsunited.webs.com/cardiffdemo.htm" target="_blank">http://casualsunited.webs.com/cardiffdemo.htm</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=antifa&source=726">antifa</a>, <span style='color:#777777; font-size:10px'>cardiff</span>, <a href="../keywordSearch/?keyword=direct+action&source=726">direct action</a>, <a href="../keywordSearch/?keyword=edl&source=726">edl</a>, <a href="../keywordSearch/?keyword=wales&source=726">wales</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(816);

				if (SHOWMESSAGES)	{

						addMessage(816);
						echo getMessages(816);
						echo showAddMessage(816);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


