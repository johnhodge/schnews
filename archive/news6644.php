<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 664 - 30th January 2009 - Inside Guantanamo</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, barack obama, guantanamo, geneva convention, cia, rendition, black sites, bagram, afghanistan, moazzam begg, war on terror" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.bigcampaign.org/index.php?mact=Calendar,cntnt01,default,0&cntnt01event_id=9&cntnt01display=event&cntnt01detailpage=73&cntnt01return_id=103&cntnt01returnid=73"><img 
						src="../images_main/663-carmel-agrexco-banner.jpg"
						alt="Boycott Israeli Goods"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 664 Articles: </b>
<p><a href="../archive/news6641.php">Mner Mner...</a></p>

<p><a href="../archive/news6642.php">Iceland Meltdown</a></p>

<p><a href="../archive/news6643.php">Bolivian It Up</a></p>

<b>
<p><a href="../archive/news6644.php">Inside Guantanamo</a></p>
</b>

<p><a href="../archive/news6645.php">Exclusive: Omar Deghayes Speaks To Schnews</a></p>

<p><a href="../archive/news6646.php">Big It Up  </a></p>

<p><a href="../archive/news6647.php">Outfoxed</a></p>

<p><a href="../archive/news6648.php">Upping The Auntie  </a></p>

<p><a href="../archive/news6649.php">Bayer 'eck</a></p>

<p><a href="../archive/news66410.php">Tas-mania</a></p>

<p><a href="../archive/news66411.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th January 2009 | Issue 664</b></p>

<p><a href="news664.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>INSIDE GUANTANAMO</H3>

<p>
<i>&#8220;You don&#8217;t stick a knife in a man&#8217;s back nine inches and then pull it out six inches and say you&#8217;re making progress.&#8221; </i> <br />
- <b>Moazzam Begg, British ex-Guantanamo detainee quoting Malcolm X.</b>  <br />
 <br />
Within his first week of office Obama has taken significant steps to undo some of the damage caused by his predecessor, issuing three Executive Orders: one to close Guantanamo within a year, one to review existing detainee policies and a third to close CIA run detention centres (otherwise known as &#8216;secret prisons&#8217; or &#8216;black sites&#8217;) and curtail the use of torture. But the orders still leave many legal black holes open, which could leave much of the extensive network of renditions and detention centres unscathed. <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 208" width="208px"><a href="../images/664-gitmo-hilton-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/664-gitmo-hilton-sm.jpg"  alt='If this is the Hilton I'd hate to see the Travel Lodge'  /></a></td></tr></table><b>What the Executive Orders do include: </b> <br />
 <br />
The first directive is aimed at removing detainees from Guantanamo either to their place of residence or a country which will accept them. If refused entry, the US will review whether they can be put on trial. In the mean time, military commissions against those currently facing prosecution have been halted and the Geneva Convention reinstated, ending the use of torture inside Guantanamo. <br />
 <br />
The second order is to set out a review of policy options for apprehension, detention, trial, transfer or release of detainees, which has to be completed in 180 days. The last order commissions a review of the military&#8217;s interrogation guidelines, requires all departments and agencies to provide the International Committee of the Red Cross access to detainees and orders CIA prisons to be closed. <br />
 <br />
An additional a presidential memorandum has also been issued to review the detention of Ali Saleh Hahlah al-Marri &#8211; the only &#8216;enemy combatant&#8217; to be detained on US soil. He is currently being held in South Carolina. <br />
 <br />
<b>What the Executive Orders exclude: </b> <br />
 <br />
No mention is made of detention centres at US military bases such as Bagram in Afghanistan, which currently holds 600 hundred detainees, neither about prisoners held by foreign forces or private military companies.  <br />
 <br />
Reprieve, an organisation of human rights lawyers, say the US holds one hundred times the number of prisoners currently held in Guantanamo, in both CIA and military prisons across the world. The status of &#8216;proxy detention&#8217; is also hazy. A joint statement issued by campaigners says, &#8220;<i>the Executive Order leaves open the possibility for the CIA to use detention facilities on a short-term or transitory basis, or to use foreign-controlled facilities to detain and interrogate individuals</i>.&#8221;  <br />
 <br />
The orders also do not include an end to rendition flights - covert transportation of detainees to places where they are tortured to obtain information - nor do they clearly outline how detainees will be returned safely. If released back to their country of origin some face torture and death. Moazzam Begg says Guantanamo was &#8220;like the Hilton&#8221; compared to other places in which he was incarcerated. <br />
  <br />
There is also no mention of redress for the prisoners that have been falsely accused, taken from their homes, families and livelihoods and abused for years, or whether those responsible  will be held accountable &#8211; beginning with those at the very top. It is beyond doubt that international law and human rights have been totally violated on many counts. <br />
 <br />
In Guantanamo alone, over 800 people have been detained, 500+ released without a single charge brought against them and only 38 granted a trial at all (all which have now been put on hold &#8211; and these were dodgy military courts in any event). The overwhelming majority of detainees in Guantanamo were picked up by bounty hunters and sold to US forces for a reward.  <br />
 <br />
The names, fates, and whereabouts of all individuals the US has secretly detained are still unknown.  <br />
 <br />
Of course, everything cannot be achieved all at once. Rectifying eight years of the Bush administration cannot be achieved overnight, even if you are Obama. But there is a danger that in the wash of excitement over closing Guantanamo everything else will be forgotten. The campaign is far from over.  <br />
 <br />
<b>EU/UK response:</b> <br />
 <br />
This week, taking Obama&#8217;s lead, the EU has entered negotiations about allowing some foreign national detainees entry. However they have yet to obtain full details of the detainees&#8217; cases from the US, have yet to decide how to deal with evidence obtained through torture (the foundation of most cases) and still have to haggle through the varying judicial systems and asylum and refugee regulations of each country.  <br />
 <br />
To clear the minefield, the EU is expected to set up a &#8216;clearing house&#8217; in collaboration with the US &#8211; allotting inmates to countries who will take them. It is likely that deals will be struck between governments to bypass the inconvenience of the judicial process and the embarrassment if detainees are found not guilty.  <br />
 <br />
Britain has refused to take any foreign nationals. So far 13 detainees with British status have been returned. David Miliband in a statement earlier this week says the UK has &#8220;done its bit&#8217;&#8217;. At least four detainees with British links still remain in Guantanamo: Binyam Mohamed, Shaker Aamer, Ahmed Belbacha and Saaid Farhi. <br />
 <br />
* &#8216;Two Sides &#8211; One Story: Guantanamo from both sides of the wire&#8217; - a speaking tour is currently crossing the country bringing together ex-detainees including Moazzam Begg and an ex-prison guard at Guantanamo. 11th Jan &#8211; 4th Feb. See <a href="http://www.cageprisoners.com" target="_blank">www.cageprisoners.com</a> <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
For more info see  <a href="http://www.reprieve.org.uk" target="_blank">www.reprieve.org.uk</a> , <a href="http://www.andyworthington.co.uk" target="_blank">www.andyworthington.co.uk</a> <a href="http://<br />
www.save-omar.org.uk" target="_blank"><br />
www.save-omar.org.uk</a> , <a href="http://www.guantanamo.org.uk" target="_blank">www.guantanamo.org.uk</a> , <a href="http://www.cageprisoners.com" target="_blank">www.cageprisoners.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=barack+obama&source=664">barack obama</a>, <a href="../keywordSearch/?keyword=guantanamo&source=664">guantanamo</a>, <a href="../keywordSearch/?keyword=afghanistan&source=664">afghanistan</a>, <a href="../keywordSearch/?keyword=war+on+terror&source=664">war on terror</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(207);
			
				if (SHOWMESSAGES)	{
				
						addMessage(207);
						echo getMessages(207);
						echo showAddMessage(207);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>