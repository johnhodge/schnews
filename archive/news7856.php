<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 785 - 26th August 2011 - Autonomist Under Attack</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bristol, alternative media" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 785 Articles: </b>
<p><a href="../archive/news7851.php">Dale Farm: Fight The Power</a></p>

<p><a href="../archive/news7852.php">Zad: Breaking Da Vinci Code</a></p>

<p><a href="../archive/news7853.php">Splinter Of Discontent</a></p>

<p><a href="../archive/news7854.php">Gaddafi Ducks Out</a></p>

<p><a href="../archive/news7855.php">Atos Shrugged</a></p>

<b>
<p><a href="../archive/news7856.php">Autonomist Under Attack</a></p>
</b>

<p><a href="../archive/news7857.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th August 2011 | Issue 785</b></p>

<p><a href="news785.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AUTONOMIST UNDER ATTACK</h3>

<p>
<p>  
  	In a blatant assault on activist media, police raided the home of one of the editors of a local Bristolian newspaper, the Autonomist. Police are desperate to link them to recent disorder, and figure that even if they can&rsquo;t find any evidence, damaging the homes of members of the collective and confiscating their computers will put them out of business.  </p>  
   <p>  
  	Whilst the police warrant justifies the seizure of rocks and white paint, it also extends to &lsquo;clothing&rsquo; and &lsquo;literature.&rsquo; It goes without saying that these items are only found in the hands of those that indulge in what Avon and Somerset police refer to as &ldquo;domestic extremisim&rdquo; (no- that&rsquo;s not our typo, this time). If anyone suspects anyone of possessing any of these things, we advise them to contact the police immediately.  </p>  
   <p>  
  	Luckily, living as we do in the modern age, the Autonomist is still online and still functioning. It&nbsp; advertises itself as &ldquo;the only local newspaper not funded by Banksy&rdquo;, and has not been pulling its punches. While they report on acts of sabotage against corporate &amp; state targets, it has been their coverage of the riots that has earned them the full on enimity of the police.  </p>  
   <p>  
  	The newsletter is given out for free and mainly written by homeless people. Collective member Lucy Parsons says &ldquo;The seizure of phones, computers, and paperwork relating to the production of the Autonomist just as we start to compile the September edition is a clear, worrying, and damaging attack on journalistic independence. The demonisation of those who report the news as &ldquo;domestic extremists&rdquo;, and the willingness to use violence to silence them, does not fill us with confidence in the police or the future of liberty in this country. Regardless, we will continue to produce the Autonomist, using computers at the library if we have to, and you can expect the September issue at the turn of the month.&rdquo; Spoken like a true radical newsletter editor...  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1337), 154); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1337);

				if (SHOWMESSAGES)	{

						addMessage(1337);
						echo getMessages(1337);
						echo showAddMessage(1337);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


