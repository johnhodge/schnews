<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 683 - 10th July 2009 - Hans Off!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, repression, china, urumchi, xinjiang, student protest, students" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 683 Articles: </b>
<p><a href="../archive/news6831.php">Bobbies On The Bleat</a></p>

<b>
<p><a href="../archive/news6832.php">Hans Off!</a></p>
</b>

<p><a href="../archive/news6833.php">Won&#8217;t Take It Zelaya-n Down  </a></p>

<p><a href="../archive/news6834.php">Oh, The Humanity</a></p>

<p><a href="../archive/news6835.php">Inside Schnews</a></p>

<p><a href="../archive/news6836.php">Nice Tosia, To  See You Nice...  </a></p>

<p><a href="../archive/news6837.php">Call Out! Keen To Squat!   </a></p>

<p><a href="../archive/news6838.php">Climate Campaigns Run Hot</a></p>

<p><a href="../archive/news6839.php">Digging In  </a></p>

<p><a href="../archive/news68310.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th July 2009 | Issue 683</b></p>

<p><a href="news683.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HANS OFF!</h3>

<p>
In the most serious Chinese government massacre since Tiananmen Square 20 years ago, hundreds of people were killed on Sunday (5th) in Urumchi, capital of East Turkestan - or Xinjiang, as the ethnically Uighur province has been known since it was occupied by the &lsquo;People&rsquo;s Liberation Army&rsquo; 60 years ago. <br />  <br /> On Sunday, students organised a protest against the Chinese authorities&rsquo; inaction on the mob killing and beating of Uighurs at a toy factory in the southern Guangdong province two weeks ago, and the increasing racial discrimination against the Muslim Uighurs across China. In the earlier attack, the police didn&rsquo;t show up for three hours as the Uighurs were being beaten, resulting in at least three fatalities. <br />  <br /> The march in Urumchi initially passed peacefully, with the student protesters even waving Chinese national flags to appease the security forces. It didn&rsquo;t work however. Paramilitary police arrived, heavily armed and in position to confront the marchers before they even reached the central square. Police initially tried to disperse the crowd with tear gas. When this failed they shot into the crowd with live ammo and hunted down groups of protesters well into the evening. According to eyewitnesses, seventeen demonstrators were crushed by armoured vehicles near the university. While it appears that most of the several hundred dead and over a thousand injured were Uighurs, there were also Han Chinese victims as, following the repression of the march, many Uighurs vented their anger in attacks on the Han and their property. <br />  <br /> In response some 1,500 Uighur students were subsequently rounded up. Many are still being held and are being threatened with execution for being responsible for the deaths during the Sunday clashes. Protests were also violently repressed in other East Turkestan cities such as Kashgar, with reports of dozens killed. <br />  <br /> Security forces flooded the province&rsquo;s cities on Monday, fearing a larger uprising in a region simmering with ethnic tensions, where the indigenous Uighirs have been resisting the extermination of their culture and language for decades. With the world&rsquo;s media in town, the police acted with restraint to control groups of Han Chinese who went on the rampage on Monday against Uighur businesses, and Uighur women protesting against the detention of their relatives on Tuesday. Many Uighur have since tried to flee the area, scared for their lives. <br />  <br /> Complementing the city lock-downs by the security forces, the authorities imposed a communications blackout with almost all internet connections and phone lines to the outside world cut off, even for the world&rsquo;s media, who were forced to rely on official press releases and video footage of the clashes. The Chinese authorities presented the Uighurs as the instigators and main aggressors in the violence &ndash; only Han Chinese victims were shown and Uighurs were shown attacking property. None of the 156 dead claimed by the authorities have been named, nor has their ethnicity been revealed. Rebiya Kadeer, Uighur political prisoner for six years from 1999 until 2005, and currently in exile in the US, was blamed by authorities for masterminding the protests. <br /> The massacre in Urumchi resembles the brutal crackdown of protests in Tibet in March 2008 when over 200 Tibetans were killed by police followed by a massive cover up by the authorities. But while this provoked the righteous fury of western liberalism - spurred on by the sage noddings of spiritual mentors such as the Dali Lama and Richard Gere - the Uighurs, being Muslims, are less likely to be sanctified by the western press. Video footage has since emerged however, of brutality by the security forces towards protesters. As reports painting a distinctly different picture to the official version of events begin to emerge from East Turkestan, it looks like this situation is also one the Chinese government will struggle to keep covered up for long.&nbsp; <br />  <br /> * For more visit <a href="http://www.uyghurcongress.org" target="_blank">www.uyghurcongress.org</a> <a href="http://www.uhrp.org" target="_blank">www.uhrp.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>china</span>, <a href="../keywordSearch/?keyword=repression&source=683">repression</a>, <span style='color:#777777; font-size:10px'>student protest</span>, <a href="../keywordSearch/?keyword=students&source=683">students</a>, <span style='color:#777777; font-size:10px'>urumchi</span>, <span style='color:#777777; font-size:10px'>xinjiang</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(394);
			
				if (SHOWMESSAGES)	{
				
						addMessage(394);
						echo getMessages(394);
						echo showAddMessage(394);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>