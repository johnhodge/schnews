<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 655 - 14th November 2008 - Fifth Columnist</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, netcu, animal rights, observer, mark townsend, nick denning, journalism, sean kirtley, mel broughton, vivisection" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://edinantimil.livejournal.com/"><img 
						src="../images_main/anti-war-gathering-banner.png" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 655 Articles: </b>
<b>
<p><a href="../archive/news6551.php">Fifth Columnist</a></p>
</b>

<p><a href="../archive/news6552.php">Lack Cluster Excuse</a></p>

<p><a href="../archive/news6553.php">A Brum Do</a></p>

<p><a href="../archive/news6554.php">X-ray Of Hope</a></p>

<p><a href="../archive/news6555.php">Clarion Up The Arm's Fair</a></p>

<p><a href="../archive/news6556.php">And Finally 655</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/655-eco-terror-lg.jpg" target="_blank">
													<img src="../images/655-eco-terror-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 14th November 2008 | Issue 655</b></p>

<p><a href="news655.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>FIFTH COLUMNIST</H3>

<p>
<b>AS THE OBSERVER TROTS OUT POLICE LINE OVER ECO-TERRORISM</b><br />
<br />
<i>&#8220;Of course I don&#8217;t have a f*cking agenda. I&#8217;m a national newspaper journalist &#8211; why would I have an agenda?&#8221;</i> - <b>Mark Townsend, Observer journalist.</b><br />
<br />
The Observer launched a broadside this week against the environmental movement. Headlined &#8220;<b>Police warn of growing threat from eco-terrorists</b>&#8221;- handily sub-headed, &#8220;<b>Fear of deadly attack by lone maverick as officers alert major firms to danger of green extremism</b>&#8221; - the article goes on to allege that &#8220;<i>Officers are concerned that a &#8216;lone maverick&#8217; eco-extremist may attempt a terrorist attack aimed at killing large numbers of Britons.</i>&#8221; Of course not one shred of evidence is referred to in the thinly disguised puff-piece for the National Extremism Tactical Co-ordination Unit (NETCU).The words of an unnamed police source are all it takes to generate the spectre of carbon-neutral suicide bombings coming to a city-centre near you.<br />
<br />
Apparently, &#8220;<i>The unit is currently monitoring blogs and internet traffic connected to a network of UK climate camps and radical environmental movements under the umbrella of Earth First! ... A senior source at the unit said it had growing evidence of a threat from eco-activists. &#8216;We have found statements that four-fifths of the human population has to die for other species in the world to survive. There are a number of very dedicated individuals out there and they could be dangerous to other people.&#8217;</i>&#8221;<br />
<br />
The piece was written by one Mark Townsend (environmental journalist of the year and co-incidentally author of a mildy-amusing tome entitled &#8220;Fifty ways to fuck the planet&#8221;) and the mysterious Nick Denning, who doesn't even work at the Observer.<br />
<br />
When SchNEWS contacted Mark at his desk, at first he seemed defensive and grew increasingly aggressive as the interview went on. When asked if he&#8217;d just regurgitated a police press release he said, &#8220;<i>You don&#8217;t know anything about NETCU mate &#8211; they don&#8217;t just stick out press releases</i>&#8221;. Which is strange given that the unit comment very publicly on the work they do &#8211; just check out their website <a href="http://www.netcu.org.uk" target="_blank">www.netcu.org.uk</a> for a few examples of the spin they put on stories involving protestors. They also don&#8217;t disguise their political purpose. As it says on their site, &#8220;<i>We support the business and academic sectors, providing a centralised source of information, advice, guidance and liaison on strategies to withstand domestic extremist attacks.</i>&#8221;<br />
<br />
Now baby-eating anarchist scare-stories are nothing new in the world of the left liberal media. Anyone remember the samurai sword-wielding nihilists it was reported were going to be at large on Mayday 2001 &#8211; not to mention the swathes of fiction released around the time of the Gleneagles G8 (see <a href="news503.htm">SchNEWS 503</a>). More recently of course we had the much publicised discovery of a &#8216;weapons cache&#8217; near this year&#8217;s Climate Camp. As far as the political police are concerned the media are just another weapon in the fight against domestic unrest &#8211; and for their journalist-dupes  truth balance and fact-checking just don&#8217;t come into  it. When we asked Mark where the claim that Earth First! advocates the disappearance of 80% of the population had come from he said, &#8220;<i>I don&#8217;t know - they [NETCU] said they had seen them in blogs</i>&#8221; - How&#8217;s that for speaking truth to power? <br />
<br />
So why the sudden appearance of the article? It contains little that could be described as news, just a load of cobbled together wild-eyed speculation. One answer is that concern for NETCU jobs in the face of the credit crunch has triggered a search for a new enemy and a broader remit. How convenient that now the animal rights (AR) movement is in &#8216;disarray&#8217;, a new target hovers into view. <br />
<br />
<b>OFF WITH HIS SUBHEAD</b><br />
<br />
Another possible explanation is that the growing movement against climate change has got the state more worried than we realise, and the idea is to spread fear amongst activists that they are being heavily watched. At the moment campaigners are generally regarded in a positive light and public support is absolutely crucial for successful defiance of the state. Just look at how lightly anti-GM activists and peace protestors are treated by the authorities compared to their animal rights counterparts. Perhaps the time has come to drive a wedge between environmental activists and the general public, and of course the best way to do this is with the emotive issue of &#8216;violence&#8217;. Are we observing the beginning of a smear campaign? <br />
<br />
So far it&#8217;s AR that has felt the full weight of state-orchestrated demonisation. Despite the fact that the movement has never been responsible for a single death they are routinely described as &#8216;terrorists&#8217; or &#8216;extremists&#8217;. A political climate has been created which enables the state to crack down hard. New criminal offences are drafted targeting the movement and people are imprisoned simply for organising demos (Sean Kirtley &#8211; see <a href="news634.htm">SchNEWS 634</a>). The SOCPA legislation (sec 145) banning demos aimed at disrupting &#8216;contractual obligations&#8217; currently only protects &#8216;animal research&#8217; organisations. A who&#8217;s who of UK media organisations have lined up to take a pop at the AR movement &#8211; Dispatches, Panorama and all the main papers have parroted the police line that AR is full of dangerous violent fanatics with an irrational belief system. High profile waves of arrests make the front page, so do the convictions, but news of acquittals languish in the back pages. The actual cause that the AR movement is fighting for receives virtually no media examination. The industrial-scale use of animals for food and vivisection is one of the great hidden evils of our lifestyle &#8211; rather than confront that, it&#8217;s obviously best to throw those that confront it into prison.<br />
<br />
The opening of the new Oxford animal lab, albeit two years late and millions over budget, is now being hailed as a victory by vivisectionists. Of course the very fact that they were able to achieve this much is because of a huge mobilisation on the part of the state. Back in <a href="news590.htm">SchNEWS 590</a> we described how Thames Valley Police accidentally taped themselves saying that they were &#8216;going to wage a dirty war&#8217; against SPEAK, with one commander adding that, &#8220;We&#8217;re going to prosecute the shit out of them.&#8221; Publicity surrounding the tape didn&#8217;t prevent the arrest and prosecution of Mel Broughton, a prominent spokesperson for the campaign (see <a href="news616.htm">SchNEWS 616</a>). Last week he was acquitted of possessing an explosive substance - packets of sparklers - with intent. Two other charges led to hung jury. <br />
<br />
Despite this, the prosecution demanded a re-trial and Mel was remanded in prison with a trial date to be fixed &#8216;some time next year&#8217;. NETCUs attempts to take &#8216;ringleaders&#8217; of the AR off the streets by fair means or foul continues.<br />
<br />
We don&#8217;t know what agenda Townsend&#8217;s actually working to but it&#8217;s been clear for a while that the police want an increased ability to deal with all forms of dissent. Public demonisation is a key plank in the strategy. We&#8217;ll leave the last words to John Curtin, long term AR campaigner: &#8220;<i>We used to be regarded as Robin Hood figures for what we did &#8211; rescuing animals from a life of torture in laboratories &#8211; and now we&#8217;re terrorists &#8211; the same people doing the same things.</i>&#8221;<br />
<br />
* Original article is at <a href="http://www.guardian.co.uk/environment/2008/nov/09/eco-terrorism-earth-first-elf " target="_blank">www.guardian.co.uk/environment/2008/nov/09/eco-terrorism-earth-first-elf</a><br />
 <br />
** NETCU Watch <a href="http://www.netcu.wordpress.com" target="_blank">www.netcu.wordpress.com</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=655">direct action</a>, <a href="../keywordSearch/?keyword=animal+rights&source=655">animal rights</a>, <a href="../keywordSearch/?keyword=vivisection&source=655">vivisection</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(120);
			
				if (SHOWMESSAGES)	{
				
						addMessage(120);
						echo getMessages(120);
						echo showAddMessage(120);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>