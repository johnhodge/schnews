<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 652 - 24th October 2008 - Ciba Punks</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, ciba vision, hls, huntingdon life science, shac, hampshire, shac trial, blackmail, conspiracy to blackmail, animal rights, vivisection, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.peasantsrevolt.org/"><img 
						src="../images_main/peasants-revolt-banner.jpg" 
						width="465" 
						height="90" 
						border="0" 
						alt="On The Verge - The Smash EDO Campaign Film - made by SchMOVIES - is out!" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 652 Articles: </b>
<b>
<p><a href="../archive/news6521.php">Ciba Punks</a></p>
</b>

<p><a href="../archive/news6522.php">Fully Ocupied</a></p>

<p><a href="../archive/news6523.php">Meet Swedish Balls</a></p>

<p><a href="../archive/news6524.php">Defra-nately Maybe</a></p>

<p><a href="../archive/news6525.php">Worcester Source</a></p>

<p><a href="../archive/news6526.php">Yer Numbers Up</a></p>

<p><a href="../archive/news6527.php">And Finally 652</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/652-ciba-lg.jpg" target="_blank">
													<img src="../images/652-ciba-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 24th October 2008 | Issue 652</b></p>

<p><a href="news652.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>CIBA PUNKS</H3>

<p>
<b>IN THE VIRTUAL REALITY OF THE CRACKDOWN ON ANIMAL RIGHTS PROTESTS</b><br />
<br />
The British Govt&#8217;s attempts to smash the animal rights movement have long been catalogued in SchNEWS over the years, but this week it turned from the pernicious to the ridiculous as Police stretched their already tenuous use of the word &#8216;blackmail&#8217; with regards to animal rights protest... <br />
<br />
Last Tuesday (14th) some activists did a banner drop protesting against Ciba Vision, a subsidiary of Novartis, from an overpass over the motorway near Ciba Vision&#8217;s premises at Hedge End, near Southampton. Ciba Vision make contact lenses, and are a client of vivisection lab Huntingdon Life Sciences (HLS). The protest was just outside the exclusion zone which limits protests near the premises of a list of companies associated with HLS as named on the injunction against SHAC (See <a href="news581.htm">SchNEWS 581</a>). The two were not arrested, and only had their details taken by police for a possible court summons.<br />
<br />
But things turned serious last Friday at 10am, when police kicked the door down at the house they were staying. With a search warrant regarding Tuesday&#8217;s banner-drop, 25 coppers arrived - including Hampshire Police&#8217;s two &#8216;animal rights specialists&#8217;, Martin Foster and Andrew Tester - and some plain-clothes CID to do the interviewing. The two banner-droppers were arrested for criminal damage and conspiracy to blackmail &#8211; because, the police said, they had used spraypaint to make the banner, and that spraypaint had been used in other actions against HLS targets previously! Their clothes were confiscated for forensic analysis, to see if they could be linked to a recent &#8216;home visit&#8217; on the manager of a HLS client which involved paintstripper on his car and other nasty surprises. As one of them said, &#8220;<i>It&#8217;s as though police think we&#8217;re the only people in Hampshire with spray cans</i>&#8221;.<br />
<br />
This was the third time this same household had been raided in the past year, and police also took a computer, owned by another resident, who is currently one of the defendants in the upcoming SHAC trials (see below). A total of six computers, three printers and ten mobile phones have been taken from this household during these raids.<br />
<br />
How can a banner drop in a public place &#8211; for all intents and purposes a legal, non-violent action - blow out into a possible charge of &#8216;conspiracy to blackmail&#8217;? With the evidence at hand this case would get laughed out of court - that is unless the police try to pin something on them &#8211; but this is just the latest chapter in the British Govt&#8217;s tenacious and heavy handed campaign to smash animal rights activism in this country.<br />
<br />
Said one of the arrestees - &#8220;<i>If they take what is legal protest, and use it to arrest people, it forces protesters underground because if they put their faces to actions &#8211; even legal ones &#8211; they risk having their house raided and being dragged into court cases with possible prison sentences. The Police are the biggest recruiters for the ALF (the name used for covert, anonymous animal rights actions).</i>&#8221;<br />
<br />
&#8220;<i>Any animal rights activist saying &#8216;I will continue to protest until they stop&#8217; is now seen as tantamount to blackmail. But what is happening to SHAC and other animal rights groups is going to happen to other types of campaigns.</i>&#8221;<br />
 <br />
<b>TRIAL & ERROR</b><br />
<br />
Rumbling in the background to this weeks incident is the first of two SHAC trials, which is currently into it&#8217;s third week and is due to last over four months, ending in January. The second trial begins then, but it may be affected by the results of the first. Eight have been charged - two with blackmail and the others with &#8216;conspiracy to blackmail&#8217;. Since then, four are on remand and three have pleaded guilty.<br />
<br />
But what is all this &#8216;blackmail&#8217; thing about? Since 2001 SHAC have published the names of companies doing business with HLS, and initially sent them polite letters saying, &#8216;Do you know what HLS do? Please stop dealing with them.&#8217; Since then there have been demos and ALF-style actions against some of them.<br />
<br />
It turns out that some of the companies involved did stop working with HLS. Many did so after being visited by police, who told their management that now they were on the SHAC website they needed to beef up security - warning staff of dire threats to life and limb and advising them to check under their cars for bombs. These companies include those making cages, other torture equipment, and supplying transport and other services to HLS, as well as clients using HLS&#8217;s &#8216;research&#8217; on animals.<br />
<br />
The police and prosecution are alleging that, by hosting the names of companies that work with Huntingdon Life Sciences, SHAC are giving a nod and a wink to ALF-style attacks. Yes covert attacks have taken place against HLS related targets &#8211; including paintstripper on cars, phonecalls and letters to directors&#8217; homes and more &#8211; and people have been prosecuted for various offences in the past &#8211; but these attacks against HLS (and associated companies) are not being linked to the SHAC defendants in this trial. Instead re-publishing public information and writing to companies about animal abuse inside HLS&#8217; labs is made to look like a scene from a Mafia movie where the don says &#8220;<i>...we wouldn&#8217;t want any nasty accidents to happen to you on your way back from the animal murdering lab, would we now.</i>&#8221;<br />
<br />
This trial is just the latest court case involving animal rights activists, coming after the recent Sequani trials which saw Sean Kirtley banged up for four years for running a website (See <a href="news634.htm">SchNEWS 634</a>). In May 2007 police made 32 arrests across the UK in &#8216;Operation Achilles&#8217;. 700 police were involved as homes and animal sanctuaries were raided, with police seizing computers, mobile phones and cash in what a popular weekly direct action newsletter described as a &#8216;fishing expedition&#8217; (See <a href="news586.htm">SchNEWS 586</a>).<br />
<br />
But while HLS are being protected by the bootboys and laws of the British state, they are in fact in a tenuous state. They took a loan from the British Govt in 2001, and are battling to repay it. Novartis are being targeted because they are a major client of HLS, which HLS would sorely miss, and their status on the New York Stock Exchange is another weak point for them. If they get kicked off the NYSE before 2011 &#8211; as has happened in the past due to the international efforts of SHAC - they will go into liquidation. <br />
<br />
That animal rights activists are portrayed in Britain as &#8216;extremists&#8217; or &#8216;terrorists&#8217; is deeply ironic seeing as they are about protecting animals from torture.<br />
<br />
* See also <a href="http;//www.shac.net" target="_blank">www.shac.net</a>
</p>

</div>


<br /><br />

		</div>
		
		
	
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>