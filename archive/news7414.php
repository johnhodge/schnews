<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 741 - 1st October 2010 - SchNEWSflash: Ecuador</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, latin america, ecuador, coup" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 741 Articles: </b>
<p><a href="../archive/news7411.php">Unrest For The Wicked</a></p>

<p><a href="../archive/news7412.php">No Borders Update: Bruss Versus Them</a></p>

<p><a href="../archive/news7413.php">Edls On Wheels</a></p>

<b>
<p><a href="../archive/news7414.php">Schnewsflash: Ecuador</a></p>
</b>

<p><a href="../archive/news7415.php">Pump Action</a></p>

<p><a href="../archive/news7416.php">Cheeky Minks</a></p>

<p><a href="../archive/news7417.php">Inca-hoots</a></p>

<p><a href="../archive/news7418.php">Siege Of Gaza: Update</a></p>

<p><a href="../archive/news7419.php">Tek The Biscuit</a></p>

<p><a href="../archive/news74110.php">Happen-stance</a></p>

<p><a href="../archive/news74111.php">Amsterdam-busters</a></p>

<p><a href="../archive/news74112.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st October 2010 | Issue 741</b></p>

<p><a href="news741.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCHNEWSFLASH: ECUADOR</h3>

<p>
<p>
	This Thursday morning (30th) the government of Ecuador fell foul of a coup attempt by an alliance of military, police and right-wing politicos. President Raefael Correa is currently being held in a military hospital while rebel soldiers have taken control of the runway in Quito airport and the controls most important military airbase. </p>
<p>
	Trouble started at around 9am, when a thousand police took to the streets demonstrating against cuts to the bonuses they receive in addition to their wages and terms of promotions. It quickly became obvious that this was little more than a pretext to cause right-wing chaos when soldiers occupied the airport. </p>
<p>
	Ecuador has been governed by left-leaning, green-tinged Rafael Correa and his Alianza PAIS party since 2007. As well as declaring Equador&#39;s international debt illegitimate as it was incurred by corrupt and unrepresentative regimes, he has aligned Ecuador with fellow Latin lefties in Venezuela, Bolivia and Brazil. Correa also won praise from environmentalists when last year he became the only leader in the world to pledge to leave his country&#39;s oil reserves in the ground (where they shouldn&#39;t harm any rainforests, atmospheres or other such natural resources that most world leaders treat as expendable). </p>
<p>
	But, as followers of 21st century Latin American politics will no doubt find familiar, right-wing interests associated with the old regimes began to plot in earnest after Correa&#39;s government passed through changes to the country&#39;s constitution (see SchNEWS re Honduras, Venezuela) allowing the president to rule by decree and call new presidential elections if Congress became stuck at an impasse. Given the blocking power of Equador&#39;s right, this was seen as necessary to push through reforms aimed at building &#39;socialism for the 21st century&#39; (just like his buddy Hugo Chavez). </p>
<p>
	Congress had been debating major cuts to Ecuador&#39;s state services, cuts which have been opposed by Correa and his party. When Correa threatened to use his constitutional powers to break the deadlock, his political enemies, staged their coup. The coup seems to have coalesced around former leader Lucio Gutierrez, who has has publicly called for the dissolution of parliament and new elections, placing the blame for the crisis on Correa and his &#39;abusive and corrupt government&#39;. </p>
<p>
	Showing more balls than pretty much any leader the UK has ever had, Correa confronted protesting cops and when they responded with tear gas and violence, injuring El Presidente in the process, Correa (in what maybe the speech of his life) shouted: </p>
<p>
	&ldquo;<em>If you want to kill the president, he&#39;s here, kill me if you think you can, kill me if you have the courage... If you want to destroy the country, go ahead and destroy it, but this presidente isn&#39;t going anywhere</em>.&rdquo; <br /> <a href="http://
	&nbsp;www.elpais.com/videos/internacional/quieren/matar/matenme/elpepuint/20100930elpepuint_1/Ves/" target="_blank">
	&nbsp;www.elpais.com/videos/internacional/quieren/matar/matenme/elpepuint/20100930elpepuint_1/Ves/</a> (in Spanish, obviously) </p>
<p>
	At the time of writing, Correa was still being held in the military hospital. He was being treated for the injuries he sustained from the police at the palace, including tear gas inhilation and an injury to his leg which had to be operated on. </p>
<p>
	The head of the armed forces has publicly stated that the coup attempt did not emanate from the top, and that the armed forces remain loyal to the government. It remains to be seen what the military actually does over the next hours/days. A state of emergency has been declared throughout the country, and border crossings and airports have been closed. </p>
<p>
	Chaos has taken hold of the capital, as around 4,000 police have taken to the streets in support of the coup. Widespread looting has taken place, and the only police not striking/revolting are those guarding the jails. It has been reported that either soldiers or police tried to kidnap Correa in the hospital where he was recovering. </p>
<p>
	Police in military uniforms have blocked streets with burning tyres and have been attempting to suppress pro-government demos. The cops also invaded the Asemblea Nacional (congress), chucking out the elected ministers and chaining the doors shut. </p>
<p>
	Equador&#39;s last two presidents were forced out of office by popular protest, but Correa&#39;s government has enjoyed widespread support during its 2 &frac12; years in office. As cops and protesters face off throughout the country, Equador&#39;s future hangs in the balance. This is Latin America&#39;s third coup attempt in Latin America in less than ten years, and, looking at the examples of Honduras and Venezuela, the key to the survival of progressive governments is the population&#39;s willingness to take to the streets. Given the Equadorian people&#39;s ability to overthrow illegitimate regimes (and the fact that the army has declared its support for the president) for now it looks hopeful that Equador&#39;s elected government is staying put. For now. </p>
<p>
	* See also <a href="http://telesurtv.net," target="_blank">http://telesurtv.net,</a> <a href="http://www.jornada.unam.mx," target="_blank">www.jornada.unam.mx,</a> <a href="http://www.elpais.com," target="_blank">www.elpais.com,</a> and <a href="http://english.aljazeera.net" target="_blank">http://english.aljazeera.net</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(955), 110); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(955);

				if (SHOWMESSAGES)	{

						addMessage(955);
						echo getMessages(955);
						echo showAddMessage(955);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


