<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 717 - 9th April 2010 - Counter Strike</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, anti-war, arms trade, london, dsei" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 717 Articles: </b>
<b>
<p><a href="../archive/news7171.php">Counter Strike</a></p>
</b>

<p><a href="../archive/news7172.php">Stan By Your Man</a></p>

<p><a href="../archive/news7173.php">Calloo, Calais</a></p>

<p><a href="../archive/news7174.php">Colombia: Crude Boys</a></p>

<p><a href="../archive/news7175.php">Cementing Friendship</a></p>

<p><a href="../archive/news7176.php">Schnews In Brief</a></p>

<p><a href="../archive/news7177.php">Going Ballistic</a></p>

<p><a href="../archive/news7178.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/717-hung-parliament-lg.jpg" target="_blank">
													<img src="../images/717-hung-parliament-sm.jpg" alt="Election Forecast Shocker" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 9th April 2010 | Issue 717</b></p>

<p><a href="news717.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COUNTER STRIKE</h3>

<p>
<strong>AS REPRESSION INC ARRIVES IN LONDON TO PEDDLE ITS WARES.....</strong> <br />  <br /> Roll up, roll up, one and all for the &lsquo;Counter Terror&rsquo; Expo 2010. Kicking off at London&rsquo;s Kensington Olympia on April 14th, the two day event will showcase the surveillance and &lsquo;security&rsquo; technologies of 250 companies hoping for a slice of the paranoia dollar. Also on the Big Brother bandwagon are NATO, the MoD and other representative associations from the police, military and  private security industry. There are several good reasons to show up at 12pm, Wednesday 14th April (at Olympia Way W14) and counter the terror expo.&nbsp; <br />  <br /> The event is being organised by DSEi&rsquo;s party planner Clarion, several arms companies are  getting involved, including big names with existing counter campaigns such as BAE, Lockheed Martin and General Dynamics, jumping at the chance to talk a lot about keeping everybody safe, whilst generating vast profits from maiming and killing in Afghanistan, Iraq, Palestine and elsewhere. <br />   <br /> <strong>COUNTERING CORPORATE CONTROL</strong> <br />  <br /> The security companies involved in this industry are pushing hard to normalise the idea that not only is literally nowhere safe, public spaces are now in fact inherently dangerous, and can only be rescued with the application of hardcore security technologies. Everywhere must be fortified with security apparatus, and, naturally, as the  number of fortified places increase, the more vulnerable the regular spaces become to everything we&rsquo;re supposed to be hysterically afraid of. <br />   <br /> Much of the marketing produced by these companies pushes the line that once the hospital has a cutting edge retinal scanning entry system, the shopping centre with its bog standard CCTV would be foolish not to keep up. The security companies exhibiting at Counter Terror 2010 are unashamedly milking this &lsquo;logic&rsquo;, offering the sort of fully integrated biometric and surveillance systems previously available only to high security prisons and top secret military installations, to schools, hospitals, shops and university halls of residence. <br />   <br /> It&rsquo;s an interesting time for invasive and downright bizarre &lsquo;security&rsquo; inventions. Now being &lsquo;secure under the watchful eyes&rsquo; isn&rsquo;t just morally and legally mandatory; it&rsquo;s cool, glamorous even. Now that security systems are, as phrased by G4S &ldquo; no longer a choice... but a necessity&rdquo;, the industry is becoming fully competitively commercialised, and there are so many shiny oppression machines to choose from. Mostly, these are designed to be appealing to buyers as either exclusive luxury items, or very cool pieces of military grade hardware. <br />   <br /> Biometric technologies are definitely the next big thing for the repression industry. Indeed many companies appear to be treating biometrics as the tools with which to conquer the final frontier of complete control; once the passwords and pin codes are in the body itself, that&rsquo;s it, job done. <br />   <br /> With big money being pumped in to R&amp;D, there are a lot of deeply unsettling technological advances that will be put on show at the Olympia for potential buyers. <br />   <br /> These include; scanners to measure hand size and cross reference that with iris scans, entrance systems using key cards that match up to data from retinal scans, a scanner that recognises the vein pattern in your fingertip (because finger prints just aren&rsquo;t foolproof enough any more), as well as exciting ideas for where the industry might go next, including entrance ways that recognise your unique body odour. Seriously. <br />   <br /> The companies responsible for the repackaging of this invasive oppression are grimly excited by the idea of everyone bumbling around in these Minority Report like future worlds, with G4S even claiming to be able to &ldquo;in some cases pre-empt crime&rdquo;. <br />   <br /> It&rsquo;s perhaps worth noting the extent to which these technologies are entering the private marketplace. Sure, there&rsquo;s big business at stake for the companies that get the government contracts, but in fact the marketing is overwhelmingly targeted at potential private commercial buyers. In the wake of highly unsuccessful government attempts to introduce mandatory ID cards, the big security corporations are introducing an even more repressive set of systems via the private sector. <br />   <br /> Government initiatives might be, in theory, subject to at least some legal and human rights regulation, have to be conscious of losing votes, but if your office wants to take your iris scan to let you go to work, well, that&rsquo;s treated as their business. Whether we get state ID cards or not might be irrelevant if we end up living in a patchwork of corporatised spaces, each one demanding some new biological data of us whenever we want to come in or out.  <br /> &nbsp; <br /> <strong>COUNTERING CORPORATE PROFIT</strong> <br />  <br /> With over 200 hundred exhibitors, there&rsquo;s a lot going on, but particular highlights from the world of private security include G4S, L3 and Human Recognition Systems. <br />   <br /> Truly a bastion of injustice profiteering, G4S (formerly Group 4) is worth opposing wherever it goes; amongst many, many other things, the company manages private prisons in the US, runs security at UK police custody centres, does pre-deployment training for the US army and is responsible for innumerable incidences of abuse against migrants in privately run migration prisons in the UK, including Yarl&rsquo;s Wood and Tinsley House (See <a href='../archive/news709.htm'>SchNEWS 709</a> and passim). The company also owns 70% of an Israeli security firm called Hashmira; a company profiting heavily from the body scanners and other security technologies it is supplying for Israel&rsquo;s apartheid wall (see <a href='../archive/news566.htm'>SchNEWS 566</a>) <br />  <br /> On top of this, Hashmira also &ldquo;violates its employees&rsquo; rights on a regular basis&rdquo;, as stated by the attorney representing Israeli workers&rsquo; rights association KavLaOved. Despite being sued almost annually since 2005 for legal infringements such as sexual discrimination, non-payment of pensions and breach of labour laws, the company has had its operating license continually renewed in government, and last year settled a contract to supply private security at Israeli prisons and custody centres, to the tune of &pound;1.14 million. <br />   <br /> L3 shot to fame last year with its unbelievable $165million contract to sell full body airport security  scanners to the US government. It&rsquo;s also a huge arms company. <br />  <br /> Human Recognition Systems is a UK company that advertises itself as being a member of &ldquo;global consortia&rdquo; to develop national ID programmes. Involved in an ever growing number of governmental and privately commissioned  projects internationally, the company has also got involved in the London 2012 Olympics, cataloguing the hand and iris measurements of 8,000 workers at one construction site. Chief executive of HRS, Neil Norman, thinks that this system is ideal &ldquo;for the typical worker&rdquo;,(but presumably not for him). <br />  <br /> HRS is also heading the drive to turn every university into an ever more elitist, dissent free, business school. Partnered with something called the Capital Values Group, HRS is  engaged in a far flung mission to transform university halls across the world into high-tech fortresses, converting disused buildings into swish student residences with 24 hour security staff and surveillance, and, of course, biometric entry systems. Considering the fact that in the UK, and elsewhere, students and faculty are practically at war with management over cuts in courses, pay, resources and basic protest rights, HRS is looking at a substantial market for security systems created specifically for universities. <br />   <br /> So, whether it&rsquo;s to protest increased state surveillance and corporate control, highlight the not-so-nice activities of arms companies proclaiming to keep us all safe, or to demonstrate against the vast profits that are being reaped off endless government and media manipulation of fear, come to Kensington on April 14th and show that we&rsquo;re watching them, watching us. <br />   <br /> * See <a href="http://www.dsei.org," target="_blank">www.dsei.org,</a> <a href="http://www.corporatewatch.org" target="_blank">www.corporatewatch.org</a> or go to <a href="http://www.counterterrorexpo.com" target="_blank">www.counterterrorexpo.com</a> for a full exhibitor listing straight from the horse&rsquo;s mouth.  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-war&source=717">anti-war</a>, <a href="../keywordSearch/?keyword=arms+trade&source=717">arms trade</a>, <a href="../keywordSearch/?keyword=dsei&source=717">dsei</a>, <a href="../keywordSearch/?keyword=london&source=717">london</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(727);
			
				if (SHOWMESSAGES)	{
				
						addMessage(727);
						echo getMessages(727);
						echo showAddMessage(727);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


