<?php
	require '../storyAdmin/functions.php';
	require '../functions/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS Back Issues - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<META name=description content="Feature article by SchNEWS the free weekly direct action newsheet from Brighton, UK. Going back to issue 100, some available as PDF" />
<META name=keywords content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../archiveIndex.css" type="text/css" />
<link rel="stylesheet" href="../issue_summary.css" type="text/css" />

<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>

</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>

			<?php

				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";

			?>


</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<?php

			//	Include Left Bar
			require '../inc/archiveIndexLeftBar.php';

		?>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->>

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<p><font face="Arial, Helvetica, sans-serif" size="2"><b>
			<a href="../index.htm">Home</a> &#124; BACK ISSUES<br /> </b></font><font face="Arial, Helvetica, sans-serif" size="1">PDF
			(<a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html" target="_blank">Acrobat</a>)
			are available for all these issues. Read the <a href="../pages_menu/pdf_notes.html">notes
			on PDF files</a> for help, especially with printing.</font></p><p><font face="Arial, Helvetica, sans-serif"><a href="../search.htm"><b>SchNEWS
			SEARCH FACILITY</b></a></font></p><h3 align="left">

			<font face="Arial, Helvetica, sans-serif"><b>Issues 701 - 750</b></font></h3>


			<div id="backIssues">



<div class="backIssue">
<table width='100%'><tr><td>
<a href="news750.htm"><img src='../images/750 FIFA.150.JPG' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news750.htm">SchNEWS 750, 2nd December 2010</a></b>  <font size="1">Click <a href="pdf/news750.pdf">HERE</a> for PDF version</font>
<br />
<b>Fee Foes' Fine Fun</b> -
	Day X2, the second national day of action against education reforms, saw a whirlwind of protest around the country. From teach-ins to storming the town hall, walk-outs to battling police; students, school children and anti-cuts activists ensured the momentum of the student rebellion continues to build.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news749.htm"><img src='../images/749-Student-protest-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news749.htm">SchNEWS 749, 25th November 2010</a></b>  <font size="1">Click <a href="pdf/news749.pdf">HERE</a> for PDF version</font>
<br />
<b>New Kids on the Black Block</b> -
	The second wave of student protest this week saw thousands walkout of their classrooms and on to the streets all over the country. Whilst events in London were amply covered &ndash; albeit traditionally skewed - by the mainstream media, SchNEWS was braving it on the frontline at the Brighton demo where around 3,000 students marched through the city centre.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news748.htm"><img src='../images/748-wills-kate-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news748.htm">SchNEWS 748, 18th November 2010</a></b>  <font size="1">Click <a href="pdf/news748.pdf">HERE</a> for PDF version</font>
<br />
<b>Complete Hissy Fit</b> -
	Still smarting from the failure to contain the student rioters in central London, the Met are peevishly taking out their anger against an old enemy. On Monday evening the hosts of the FIT WATCH website were asked to close it down on the grounds of it &ldquo;being used to undertake criminal activities&rdquo;. In a letter from the MET, the company Justhost.com were warned that the website was being used in an attempt &ldquo;to pervert the course of justice&rdquo;. The DI demanded that the site be taken down for &lsquo;twelve months&rsquo;. Deciding caution was the better part of valour, Justhost decided to take it down.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news747.htm"><img src='../images/747-student-riot-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news747.htm">SchNEWS 747, 11th November 2010</a></b>  <font size="1">Click <a href="pdf/news747.pdf">HERE</a> for PDF version</font>
<br />
<b>Losing Their Faculties</b> -
	Finally, perhaps, the wave is cresting... The fightback against the vicious Tory cuts programme has so far been restricted to placard waving demos and petitions. Not any more &ndash; the placards are still there but now they&rsquo;re being used to build bonfires in front of a windowless Tory HQ. And they say young people don&rsquo;t take an interest in politics.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news746.htm"><img src='../images/746-vodafone-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news746.htm">SchNEWS 746, 4th November 2010</a></b>  <font size="1">Click <a href="pdf/news746.pdf">HERE</a> for PDF version</font>
<br />
<b>The Vodafone-y War</b> -
	In the past week as many as 21 Vodafone stores have been hit with direct action. The phone giant have become a widespread target for the national anti-cuts campaign following their multi-billion government-sanctioned tax-dodging shenanigans.&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news745.htm"><img src='../images/745-mark-kennedy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news745.htm">SchNEWS 745, 29th October 2010</a></b>  <font size="1">Click <a href="pdf/news745.pdf">HERE</a> for PDF version</font>
<br />
<b>Serious Organised Crime</b> -
	The six defendants in the second SHAC trial were hammered with vicious sentences this week. The harshest was the six years handed down to 53-year-old Sarah Whitehead. Sentences for the other defendants Tom Harris, Nicole Vosper, Jason Mullan, Nicola Tapping and Alfie Fitzpatrick ranged from a two year suspended sentence for the youngest, Alfie, to three and a half years for Nicole Vosper. All the defendants also copped lengthy ASBOs, which will prevent them from any further participation in animal rights activism.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news744.htm"><img src='../images/744-A-team-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news744.htm">SchNEWS 744, 22nd October 2010</a></b>  <font size="1">Click <a href="pdf/news744.pdf">HERE</a> for PDF version</font>
<br />
<b>Crude but Refined</b> -
	Utmost secrecy, activist goodie bags and sheer determination shut down the the UK&rsquo;s busiest oil refinery last week in one of the most well-planned actions the climate justice movement has seen so far. Stilt walkers, a samba band and around 500 activists blockaded the road to Croydon oil refinery in Essex on Saturday, stopping an estimated 400,000 gallons of oil getting to London&rsquo;s petrol stations.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news743.htm"><img src='../images/743-hammertime-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news743.htm">SchNEWS 743, 15th October 2010</a></b>  <font size="1">Click <a href="pdf/news743.pdf">HERE</a> for PDF version</font>
<br />
<b>Hammering home the Point</b> -
	The &lsquo;ITT&rsquo;s Hammertime&rsquo; Smash EDO demo in Brighton on Wednesday (13th) ended with over fifty arrests, after police made it clear they were going to protect the arms makers at any cost. The demonstration planned to lay siege to Brighton&rsquo;s premier weapons factory for the day, but police repression and a newly developed policy for preventative arrests put paid to the ambitious plans to push the factory out of town. With both a section 60 and highly restrictive section 14 in place, police were given even greater powers, which they gleefully exercised...

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news742.htm"><img src='../images/742-europe-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news742.htm">SchNEWS 742, 8th October 2010</a></b>  <font size="1">Click <a href="pdf/news742.pdf">HERE</a> for PDF version</font>
<br />
<b>Frontier Law</b> -
	The No Borders camp in Brussels last week persevered in getting their message across by means of various direct actions despite widespread arbitrary arrests and shocking police violence, including physical and sexual abuse in police custody. At least 500 mostly &lsquo;preventative&rsquo; arrests took place, and 14 people were seriously injured. Here&rsquo;s a day by day report...
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news741.htm"><img src='../images/741-spain-strikes-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news741.htm">SchNEWS 741, 1st October 2010</a></b>  <font size="1">Click <a href="pdf/news741.pdf">HERE</a> for PDF version</font>
<br />
<b>Unrest For The Wicked</b> -
	Mass protests and strikes have broken out across the whole of Europe this week as the reality of already imposed and still pending austerity cuts becomes clear. Across the EU, rallies were held in thirteen capital cities and in Spain a general strike saw millions take action.&nbsp;On Wednesday (29th) around 100,000 representatives of the European trade union movement, including German miners and Polish shipbuilders, brought Brussels to a standstill to protest against the forthcoming savage spending cuts. The message &ldquo;We will not pay for their crisis&rdquo; is now resounding across Europe.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news740.htm"><img src='../images/740-van-damme-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news740.htm">SchNEWS 740, 24th September 2010</a></b>  <font size="1">Click <a href="pdf/news740.pdf">HERE</a> for PDF version</font>
<br />
<b>Brussels Sprouts Camp</b> -
	No Borders Camp 2010 in Brussels kicks off this Saturday (25th) til the 3rd October, and plenty are converging on that part of the continent in an effort to create a world where no one is illegal. Among the objectives of the camp are the denouncing of European migration policy; showing the links between this policy and the structures of capitalism and repression; the blocking of Brussels&rsquo; deportation system and the organisation of an autonomous safe space for the voices of migrants and activists to be heard.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news739.htm"><img src='../images/739-pope-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news739.htm">SchNEWS 739, 17th September 2010</a></b>  <font size="1">Click <a href="pdf/news739.pdf">HERE</a> for PDF version</font>
<br />
<b>Gauling Behaviour</b> -
	Weak to begin with, France&rsquo;s attempts to deny that recent mass expulsions of Roma people were racist have been dealt a blow after a Ministry of Interior circular ordering evacuation of camps of Roma, as &ldquo;a matter of priority&rdquo; was leaked. From mid-August to early September this year, approximately 1000 Roma were deported from France and 128 Roma camps dismantled.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news738.htm"><img src='../images/738-hovefields-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news738.htm">SchNEWS 738, 10th September 2010</a></b>  <font size="1">Click <a href="pdf/news738.pdf">HERE</a> for PDF version</font>
<br />
<b>Caravandals</b> -
	The Hovefields Gypsy/Traveller site in Essex with 50-60 inhabitants has been evicted this week. At the time of writing, a group of these families are still on the road without anywhere to stop, having been also evicted from two other sites they tried to move on to, all within 24 hours. In fact it is illegal for them to stop anywhere as a group, as they are more than six live-in vehicles.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news737.htm"><img src='../images/737-blair-mein-kampf-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news737.htm">SchNEWS 737, 3rd September 2010</a></b>  <font size="1">Click <a href="pdf/news737.pdf">HERE</a> for PDF version</font>
<br />
<b>Out of Their League</b> -
	It was supposed to be &lsquo;The Big One&rsquo; - that&rsquo;s how the EDL were billing their Bradford rally - a climactic moment in their campaign against &lsquo;radical Islam&rsquo;. According to puff pieces released on Youtube before the event, there were supposed to be 5,000 leaguers descending on the Yorkshire town on Saturday 28th August. The EDL had warned women and children not to be present and one flyer bore the slogan &lsquo;Burn, baby, burn&rsquo;.&nbsp;In the end a mere 700-800 EDLers were on display...

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news736.htm"><img src='../images/736-climate-camp-derek-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news736.htm">SchNEWS 736, 27th August 2010</a></b>  <font size="1">Click <a href="pdf/news736.pdf">HERE</a> for PDF version</font>
<br />
<b>Royal BS</b> -
	Camp for Climate Action 2010 finished this week having shut down operations at the RBS Global Headquarters, disrupting works at their administration building and closing numerous branches around Edinburgh&#39;s city centre. Activists also targeted Cairn Energy and Forth Energy, companies that had received huge wads of cash from the bank for not-so-environmentally-friendly projects.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news735.htm"><img src='../images/735-michael-fish-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news735.htm">SchNEWS 735, 20th August 2010</a></b>  <font size="1">Click <a href="pdf/news735.pdf">HERE</a> for PDF version</font>
<br />
<b>High Pressure Front</b> -
	Climate Camp Cymru kicked off on Friday (13th) in South Wales and continued until Tuesday (17th) with an eviction forcing a change of site. The camp ran into problems on Saturday (14th) afternoon when it was evicted from its site. A disproportionately large police force consisting of 10 riot vans accompanied by dogs, helicopters and mounted police, was called into action to remove just 30 activists from a field.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news734.htm"><img src='../images/734-festival-van-gogh-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news734.htm">SchNEWS 734, 6th August 2010</a></b>  <font size="1">Click <a href="pdf/news734.pdf">HERE</a> for PDF version</font>
<br />
<b>Grassroots Struggle</b> -
	Yet another independent festival has been cancelled after a concerted campaign by bureaucrats, nimbys and police. The Grassroots Festival was a small volunteer-run event due to take place in Cambridgeshire in early September. Organisers had lined up three days of revelry, from poetry to Drum &lsquo;n&rsquo; Bass and culminating in a communal banquet replete with juggling waiters.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news733.htm"><img src='../images/733-Drone-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news733.htm">SchNEWS 733, 30th July 2010</a></b>  <font size="1">Click <a href="pdf/news733.pdf">HERE</a> for PDF version</font>
<br />
<b>Robot Wars</b> -
	Want to keep fighting an unwinnable war while keeping down the home team casualty figures? You need remote-control warfare. Drones or UAVs (Unmanned Aerial Vehicles) are increasingly becoming the face of modern imperial war. Remotely piloted Predator, Reaper and now Avenger drones mean that the world&rsquo;s great powers can unleash destruction from afar without risking a single soldiers life. &nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news732.htm"><img src='../images/732-whitewash-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news732.htm">SchNEWS 732, 23rd July 2010</a></b>  <font size="1">Click <a href="pdf/news732.pdf">HERE</a> for PDF version</font>
<br />
<b>Getting Away With Murder</b> -
	&ldquo;After 16 months of waiting, to hear nothing is being done is a complete joke. Today they gave us no hope. This experience has broken our family apart. The DPP has told us there was an unlawful act, yet no charges are to be brought. This is no justice - everyone has failed us.&rdquo; - Ian Tomlinson&rsquo;s son Paul King&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news731.htm"><img src='../images/731-syngenta-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news731.htm">SchNEWS 731, 16th July 2010</a></b>  <font size="1">Click <a href="pdf/news731.pdf">HERE</a> for PDF version</font>
<br />
<b>Against the Grain</b> -
	This week sees GM firmly back in the spotlight. On Wednesday the EU took a huge step in pushing forward the genetic modification agenda by copping out of regulation and putting the decision on whether to grow GM or not back into the hands of national governments. The European Commission approved changes to the rules that may break the deadlock which has prevented any significant cultivation of GM crops in Europe. What does this mean for GM production in the UK and other nation-states? And where does this leave the resistance movement?

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news730.htm"><img src='../images/730-shell-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news730.htm">SchNEWS 730, 9th July 2010</a></b>  <font size="1">Click <a href="pdf/news730.pdf">HERE</a> for PDF version</font>
<br />
<b>The Hole Truth</b> -
	There is still plenty of ire left in Ireland as campaigners ready themselves for another summer of action against Shell and their plans to despoil the coast of County Mayo with a new gas pipeline. The project is already a decade late and three times over budget; pretty impressive for a small community fighting one of the biggest multinationals in the world.&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news729.htm"><img src='../images/729-decommissioners-1-150.png' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news729.htm">SchNEWS 729, 2nd July 2010</a></b>  <font size="1">Click <a href="pdf/news729.pdf">HERE</a> for PDF version</font>
<br />
<b>They Think It's All Over... ITT is Now!!!</b> -
	After a nail-biting twenty-four hour hiatus, the jury came to decision on the Decommissioners Case - 100% Not Guilty! Six of the seven defendants Tom Woodhead, Bob Nicholls, Ornella Saibene, Harvey Tadman, Simon Levin and Chris Osmond came smiling out of Hove Trial Centre at the end of a gruelling three and half week trial. Elijah Smith was remanded in custody for another offence. Five were acquitted on Wednesday afternoon, the other two Chris and Elijah had to wait until this morning. They had waited eighteen months for this moment.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news728.htm"><img src='../images/728--osborne-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news728.htm">SchNEWS 728, 25th June 2010</a></b>  <font size="1">Click <a href="pdf/news728.pdf">HERE</a> for PDF version</font>
<br />
<b>Cutting 'N' Running</b> -
	So &lsquo;austerity&rsquo; took shape this week as George &lsquo;where were you hiding during the election campaign&rsquo; Osborne went for broke on Tuesday with his hard-hitting &lsquo;fuck the poor&rsquo; budget. Yes, taxes &amp; VAT are going up and almost every type of benefit is being slashed (and tied into being available for whatever work they deem you fit for), but hey! it&rsquo;s not all bad news - corporation tax is being reduced!

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news727.htm"><img src='../images/727-avatar-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news727.htm">SchNEWS 727, 18th June 2010</a></b>  <font size="1">Click <a href="pdf/news727.pdf">HERE</a> for PDF version</font>
<br />
<b>Raving Madness</b> -
	Around 2,500 partygoers descended on Dale Aerodrome in Wales last May bank holiday for the 2010 UK Teknival, only to be met with a massive police response. Police broke up the party on the first day, arresting 17 people in the process. Four remain on police bail and six have been charged.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news726.htm"><img src='../images/726-world-cup-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news726.htm">SchNEWS 726, 11th June 2010</a></b>  <font size="1">Click <a href="pdf/news726.pdf">HERE</a> for PDF version</font>
<br />
<b>Slick Publicity</b> -
	In response to America&rsquo;s largest ever environmental disaster, Tony Hayward, CEO of BP, said, &ldquo;I&rsquo;d like my life back.&rdquo; We&rsquo;re sure he does, as would the eleven workers who died on BP&rsquo;s Deepwater Horizon oil rig when it exploded 40 miles off the Louisiana coast, in the Gulf of Mexico, on April 20th.But you have to sympathise with Tony. The fact that BP failed to test the strength of the cement in the well, despite knowing that the casing was &ldquo;the riskier of two options&rdquo; and that it &ldquo;might collapse under pressure&rdquo; can&rsquo;t be held against him. Even though the same disregard for safety killed 15 and injured 170 BP workers in the Texas Oil Refinery explosion of 2005.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news725.htm"><img src='../images/725-israeli_navy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news725.htm">SchNEWS 725, 4th June 2010</a></b>
<br />
<b>Israel Attacks Freedom Flotilla</b> -
	The shocking news of the assault by Israeli Navy commandos on the Gaza flotilla has swept across the world, bringing the violent and complex conflict that is Israel and Palestine to the forefront of the global media. The flotilla was in international waters approaching the Gaza Strip, which has been blockaded by Israel for the last three years. The six ships of the freedom flotilla were carrying aid supplies and a multinational collective of people who oppose the conflict centred around Gaza.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news724.htm"><img src='../images/724-bp-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news724.htm">SchNEWS 724, 28th May 2010</a></b>  <font size="1">Click <a href="pdf/news724.pdf">HERE</a> for PDF version</font>
<br />
<b>Flight Brigade</b> - Migrant rights activists across the UK are readying themselves for next week&rsquo;s  European Week of Action Against the Deportation Machine. Campaigners are calling out for a week of protest and direct action targeting the government agencies and private companies responsible for shipping migrants out of Blighty and back to the desperate situations they risked their lives to flee, often subjecting them to violent abuse on their way
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news723.htm"><img src='../images/723-romania-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news723.htm">SchNEWS 723, 21st May 2010</a></b>  <font size="1">Click <a href="pdf/news723.pdf">HERE</a> for PDF version</font>
<br />
<b>All That Romains...</b> - Welcome to the age of austerity. Romania has erupted in protests as harsh measures continue to grow and waves of discontent flood Europe. Romania&rsquo;s capital, Bucharest, stood still for a day on Wednesday (19th) as ferocious cuts cripple the Romanian populace. The city was  brought to a halt as an estimated 50,000 protesters marched through the streets in the largest of the demonstrations seen since the cuts were announced.
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news722.htm"><img src='../images/722-jake-the-clegg-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news722.htm">SchNEWS 722, 14th May 2010</a></b>  <font size="1">Click <a href="pdf/news722.pdf">HERE</a> for PDF version</font>
<br />
<b>Squaring Up To 'Em</b> - Only four days into the brave new Libservative era and the population is getting into the national coalition-building mood - a rag-tag rainbow alliance of the disaffected is set to pitch up in Parliament Square this Saturday (15th). But will it be a strong, stable protest in the national interests?
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news721.htm"><img src='../images/721-greece-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news721.htm">SchNEWS 721, 7th May 2010</a></b>  <font size="1">Click <a href="pdf/news721.pdf">HERE</a> for PDF version</font>
<br />
<b>A Smashing Defence</b> - In the early hours of 17th January 2009 six people broke in to the EDO/ITT weapons&rsquo; components factory in Moulsecoomb, Brighton, and caused hundreds of thousands of pounds worth of damage to the production line. On May 17th nine people will stand trial accused of &lsquo;conspiracy to commit criminal damage&rsquo;. Three were arrested outside the factory. The trial is expected to last for ten weeks at Hove Crown Court. The decommissioners will be running the defence that their actions were legally justified as they were &ldquo;acting to prevent a greater crime&rdquo;.&nbsp;
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news720.htm"><img src='../images/720-greek.150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news720.htm">SchNEWS 720, 30th April 2010</a></b>  <font size="1">Click <a href="pdf/news720.pdf">HERE</a> for PDF version</font>
<br />
<b>Crash of the Titans</b> - Greece is further troubled as government officials get ready to announce IMF imposed wage cuts  that will resonate across the country&rsquo;s entire public sector. Emergency demonstrations were held across the country (Thursday 29th April) in protest against the cuts by grass-roots trade unions, leftist and anarchist organisations.
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news719.htm"><img src='../images/719-david-cameron-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news719.htm">SchNEWS 719, 23rd April 2010</a></b>  <font size="1">Click <a href="pdf/news719.pdf">HERE</a> for PDF version</font>
<br />
<b>Giving the Green Finger</b> - Bristol eco-village set up camp this week with huge support from local people and activists from all around the country. The off-grid, urban community kicked off last Saturday (17th) on a patch of waste land in St Werburghs - previously notorious for fly tipping, arson attacks and hard drug users.
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news718.htm"><img src='../images/718-cameron-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news718.htm">SchNEWS 718, 16th April 2010</a></b>  <font size="1">Click <a href="pdf/news718.pdf">HERE</a> for PDF version</font>
<br />
<b>Tar-Mageddon</b> - After two weeks of direct action, the &ldquo;BP Fortnight of Shame&rdquo; concluded yesterday (15th) with protests outside the oil giant&rsquo;s AGM and a rooftop occupation in Brighton. Throughout the country, activists targeted BP over its plans to extract oil from the Alberta tar sands in Canada (see SchNEWS 716) in protests which peaked last Saturday (10th) when hundreds of climate activists in London, Brighton, Oxford and Cambridge descended on BP garages for the &ldquo;Party at the Pumps&rdquo; .
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news717.htm"><img src='../images/717-hung-parliament-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news717.htm">SchNEWS 717, 9th April 2010</a></b>  <font size="1">Click <a href="pdf/news717.pdf">HERE</a> for PDF version</font>
<br />
<b>Counter Strike</b> - Roll up, roll up, one and all for the &lsquo;Counter Terror&rsquo; Expo 2010. Kicking off at London&rsquo;s Kensington Olympia on April 14th, the two day event will showcase the surveillance and &lsquo;security&rsquo; technologies of 250 companies hoping for a slice of the paranoia dollar. Also on the Big Brother bandwagon are NATO, the MoD and other representative associations from the police, military and  private security industry.
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news716.htm"><img src='../images/716-bp-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news716.htm">SchNEWS 716, 2nd April 2010</a></b>  <font size="1">Click <a href="pdf/news716.pdf">HERE</a> for PDF version</font>
<br />
<b>Eire We Go Again</b> - The Irish state&rsquo;s determined persecution of protesters against Shell&rsquo;s experimental pipeline started to unravel last week as 25 of the 27 standing trial had their cases withdrawn or dismissed. They did however manage to stitch up campaigner Niall Harnett, who received three five-month sentences after being roughed up by Gardai in a court building.&nbsp;
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news715.htm"><img src='../images/715-strawberry-fair-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news715.htm">SchNEWS 715, 26th March 2010</a></b>  <font size="1">Click <a href="pdf/news715.pdf">HERE</a> for PDF version</font>
<br />
<b>A Twist of Fete</b> - In what is fast becoming a summer tradition, yet another gathering has fallen victim to party-pooping police tactics. The UK&rsquo;s last free open-air festival has been cancelled under police pressure. Strawberry Fair, in Cambridge, viewed by many as the starting gun for the festival season, has been stopped in its tracks after 37 straight years.
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news714.htm"><img src='../images/714-titnore-150.png' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news714.htm">SchNEWS 714, 19th March 2010</a></b>  <font size="1">Click <a href="pdf/news714.pdf">HERE</a> for PDF version</font>
<br />
<b>Tit Top</b> - Well blimey. Against all the odds it looks like Titnore Woods has been saved from the developers&rsquo; evil clutches. The decision made by Worthing Council&rsquo;s planning committee to reject the development plans for the new housing estate on the ancient woodland site on Monday (15th) had everyone shouting with joy and dancing in the public gallery.&nbsp;
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news713.htm"><img src='../images/713-sussexUni-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news713.htm">SchNEWS 713, 12th March 2010</a></b>  <font size="1">Click <a href="pdf/news713.pdf">HERE</a> for PDF version</font>
<br />
<b>Sussex, Lies and Videotape</b> - Following last week&rsquo;s &lsquo;Stop The Cuts&rsquo; demonstration and occupation of the senior management&rsquo;s offices six Sussex University students had been suspended and excluded from campus. The six had been &lsquo;positively identified&rsquo; following Sussex Police&rsquo;s thuggish altercations on campus....
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news712.htm"><img src='../images/712-sussexUni-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news712.htm">SchNEWS 712, 5th March 2010</a></b>  <font size="1">Click <a href="pdf/news712.pdf">HERE</a> for PDF version</font>
<br />
<b>School of Hard Knocks</b> - Students at Sussex University came into violent conflict with riot cops on Wednesday (3rd) as they occupied the executive nerve centre of their university. Several weeks in the planning by Stop the Cuts campaign, around 80 students rushed the &lsquo;fortified&rsquo; Sussex House building with a supporting demo outside of around 300 people, all in protest against the proposed cuts to university funding...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news711.htm"><img src='../images/711-300-poster-150.JPG' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news711.htm">SchNEWS 711, 26th February 2010</a></b>  <font size="1">Click <a href="pdf/news711.pdf">HERE</a> for PDF version</font>
<br />
<b>Greek Fire</b> - Greece has once again been rocked by protests, strikes and civil unrest as Eurozone austerity measures kick in... plus, far-right losers the SDL are seen off the streets of Edinburgh, Colombian riot police attack protesting workers demanding better conditions at BP owned oil company, Tree hugging tramps declare war on Tesco, Protests are called as more Gaza defendants are banged up, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news710.htm"><img src='../images/710-aldermaston-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news710.htm">SchNEWS 710, 19th February 2010</a></b>  <font size="1">Click <a href="pdf/news710.pdf">HERE</a> for PDF version</font>
<br />
<b>Gaza Defendants Hammered</b> - As Judge-mental Dennis hands down vicious sentences to London Gaza protesters.... plus, 700 activists converge on Aldermaston nuclear facility for mass blockade, Irish Shell protester jailed for seven months, after last week's illegal eviction activists re-establish migrant solidarity centre, Cameron's cuddly conservatives promise a crackdown on travellers and squatters, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news709.htm"><img src='../images/709-carmel-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news709.htm">SchNEWS 709, 12th February 2010</a></b>  <font size="1">Click <a href="pdf/news709.pdf">HERE</a> for PDF version</font>
<br />
<b>Slung Out On Yer Frontier</b> - SchNEWS asks you and whose gendarmerie? As French cops crackdown on migrant welfare centre... plus, activists take on companies importing goods from illegally occupied Palestinian territories, Iranian anti-regime campaigner Bita Ghaedi goes into the fourth week of hunger strike in her fight against deportation, Turkish anti-militarists set to stand trial for &ldquo;alienating people from military service&rdquo; and &ldquo;praising crime and criminals&rdquo;, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news708.htm"><img src='../images/708-medyan-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news708.htm">SchNEWS 708, 5th February 2010</a></b>  <font size="1">Click <a href="pdf/news708.pdf">HERE</a> for PDF version</font>
<br />
<b>Eyewitness Afghanistan</b> - From Kemp Town to Kabul, as SchNEWS interviews Al Jazeera journalist Medyan Dairieh about his take on the war... plus, Honduras' 'return to democracy' sees the coup leaders holding on to power and upping the repression, Kingsnorth Climate Campers win in the High Court against unlawful police stop and searches, No Borders and SoS Soutien open an autonomous safe space in Calais, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news707.htm"><img src='../images/707-mainshill-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news707.htm">SchNEWS 707, 29th January 2010</a></b>  <font size="1">Click <a href="pdf/news707.pdf">HERE</a> for PDF version</font>
<br />
<b>Lanarky in Action</b> - As the National Eviction Team move in on Mainshill Solidarity Camp... plus, they finally get around to putting a price on Blair's head, the English Defence League embark on a racist rampage through Stoke, French police evict a night shelter and destroy the makeshift camp erected by migrants in protest, a coalition of No Borders activists and local nimbys help derail plans for a new deportation centre in Crawley, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news706.htm"><img src='../images/706-Obama-Haiti-xs.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news706.htm">SchNEWS 706, 22nd January 2010</a></b>  <font size="1">Click <a href="pdf/news706.pdf">HERE</a> for PDF version</font>
<br />
<b>Haiti: The Aftershock Doctrine</b> - As the rescue effort winds down SchNEWS looks at how the international aid effort is being subverted by military and corporate power... plus, Monday&rsquo;s Smash EDO demo forced the Brighton bomb makers to shut up shop, the EDL&rsquo;s racist roadshow is moving on to Stoke, Mainshill solidarity camp in South Lanarkshire is gearing up for an eviction date, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news705.htm"><img src='../images/705-wild-at-heart-vsm.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news705.htm">SchNEWS 705, 15th January 2010</a></b>  <font size="1">Click <a href="pdf/news705.pdf">HERE</a> for PDF version</font>
<br />
<b>Wild at Heart</b> - Our comrades at SMASH EDO have announced the location of their Remember Gaza demo... plus, Gaza convoy finally allowed in after being held at port and attacked by Egyptian police, legal observer arrested at Smash EDO Carnival Against the Arms Trade finally overturns charges, European Court rules Section 44 stop 'n search illegal, the Christmas period has seen no let up for migrants in the squats and camps of Calais, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news704.htm"><img src='../images/704-earth-accord-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news704.htm">SchNEWS 704, 20th December 2009</a></b>  <font size="1">Click <a href="pdf/news704.pdf">HERE</a> for PDF version</font>
<br />
<b>Not a Hopenhagen</b> - As World Leaders dither inside, SchNEWS looks to the thousands of dissenters outside the Copenhagen Climate Conference.... plus, the Camp for Climate Action activists target Canada for wanting to exploit the oil their climate koshing tar sands, as much of Iraqs's oil finally got auctioned off last week was was mission accomplished for the US?, a prominent member of the EDL is charged with soliciting murder and using threatening, abusive or insulting words, Israeli goverment irked as UK warrant for war crimes issued to foreign ministe, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news703.htm"><img src='../images/703-COP15-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news703.htm">SchNEWS 703, 11th December 2009</a></b>
<br />
<b>SchNEWS Guide to Copenhagen</b> - Due to too many people being away (probably) burning babylon, being sick or generally too lazy, there is no print issue this week. But we didn't want you not gettin' any of yer regular fix and wanted to give you the lowdown on going to Copenhagen, so have patched together this 'blink and you'll miss it' Climate Conference special edition for the web only (where special means er, compact.)
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news702.htm"><img src='../images/702-dubai-pear-shaped-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news702.htm">SchNEWS 702, 4th December 2009</a></b>  <font size="1">Click <a href="pdf/news702.pdf">HERE</a> for PDF version</font>
<br />
<b>If You're Not Swiss-ed Off...</b> - Ten years after Seattle, thousands congregate in Geneva to resist world trade organisation meeting... plus, 25 years after the Bhopal disaster and still no justice for the victims, protests in Wales over biomass plant at Port Talbot, Sussex students protest at staff cuts, Lithuanian national is imprisoned over London G20 protests, we gloat as the capitalist edifice of Dubai crumbles, and more...
</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news701.htm"><img src='../images/701-buy-nothing-day-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news701.htm">SchNEWS 701, 27th November 2009</a></b>  <font size="1">Click <a href="pdf/news701.pdf">HERE</a> for PDF version</font>
<br />
<b>Buy Buy Cruel World</b> - SchNEWS hopes the anti-consumerist message will finally gain purchase with international 'Buy Nothing Day' tomorrow... plus, the Target Barclays campaign is launched, opposing their investment in the arms trade, protest camp against opencast coal mine in Scotland is under threat of eviction, protest camp by the sacked Vesta workers on the Isle Of Wight is evicted, and more...
</p>
</td></tr></table>
</div>



			</div>




			<p align="left">&nbsp;</p><p align="justify"><font face="Arial, Helvetica, sans-serif"><a name="backissues"></a><font size="2"><b><font size="4">Back
			Issues...</font></b></font></font></p>
			<hr />
            <p><font face="Arial, Helvetica, sans-serif"><a href="index-601-650.htm"><b><font size="2">Issues 601-650 <br />
  August 2007- October 2008 </font></b></a></font></p>
            <hr /> <p><font face="Arial, Helvetica, sans-serif"><a href="index-551-600.htm"><b><font size="2">Issues
			551-600<br /> July 2006 - August 2007</font></b></a></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif"><a href="index-501-550.htm"><b><font size="2">Issues
			501-550<br /> June 2005 - July 2006</font></b></a></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-451-500.htm"><b>Issues
			451 - 500<br /> April 2004 - June 2005</b></a></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-401-450.htm"><b>Issues
			401 - 450<br /> April 2003 - April 2004</b></a></font></p><hr /> <font face="Arial, Helvetica, sans-serif" size="2"></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-351-401.htm"><img src="../images_main/peacederesistanceweb.jpg" width="100" height="145" align="right" border="1" alt="SchNEWS - Peace De Resistance" /><b>Issues
			351-401<br /> April 2002 - April 2003</b></a><br> <b>These issues are published
			in the book 'Peace de Resistance':<br> </b>'As the Forces of Darkness gather for
			a new attack on Iraq, they're shaken by an unexpected explosion of resistance.
			US airbases are invaded; military convoys are blockaded; schoolkids stage mass
			walkouts from Manchester to Melbourne; roads are occupied, embassies besieged,
			and cities all over the world are brought to a standstill by the biggest mass
			demonstrations ever seen. Read about the efforts to sabotage the war machine that
			the corporate media ignored: not just the mass demos, but Tornado-trashing in
			Scotland, riots across the Middle East, and train blockades in Europe. Featuring
			SchNEWS 351-401, this book also covers many of the past year's other stories from
			the rebel frontlines, including GM crop-trashing, international activists in Palestine,
			and the 10th anniversary of Castlemorton. All that plus loads of new articles,
			photos, cartoons, subverts, satire, a comprehensive contacts list, and more&#133;'
			<br /> </font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>PLUS
			INCLUDES FREE CD-ROM by BEYOND TV featuring footage of many of the actions reported
			in the book, satire plus other digital resources</b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>&quot;Peace
			de Resistance&quot;<br /> </b>ISBN 09529748 7 8 <br /> &pound;7.00 inc. p&amp;p
			direct from us.<br /> Contact SchNEWS for distribution details<b><br /> <a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-301-350.htm" target="index-301-350.htm"><img src="../images_issues/sotw.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Of The World" /></a><b><font size="3"><a href="index-301-350.htm"><font size="2">Issues
			301-350 <br /> April 2001 - April 2002</font></a><br /> </font><b>These issues
			are published in the book 'SchNEWS Of The World':<br> </b></b>The temperature
			goes up a few notches as the earth's climate has its hottest year on record -
			and the corporate carve-up lifts its tempo in the paranoia and madness of September
			11th. Behind the haze Argentina goes into meltdown. Israel reoccupies Palestine
			causing a second Intifada. Hundreds of thousands come out on the street in Genoa,
			Quebec, Gothenburg, Barcelona, Brussels against globalised institutions. Even
			larger numbers fight for their land and livelihood against neo-liberalism in South
			Africa, India, South America and the rest of the global south. <br /> These 50
			issues plus 200 other pages of articles, cartoons, photos, graphics, satire, and
			a comprehensive contact database are all together in<b>...<br /> SchNEWS Of The
			World</b> .<br /> 300 pages, ISBN 09529748 6 X, &pound;4 inc. p&amp;p direct from
			us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b></font></p><hr />
			<p><font face="Arial, Helvetica, sans-serif" size="2"><img src="../images_issues/yearbook2001.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Squall Yearbook 2004" /><b><b><font size="3"><a href="index-251-300.htm"><font size="2">Issues
			251-300 <br /> March 2000 - April 2001</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchNEWS/Squall Yearbook 2001':</b></b><br> The
			Zapatistas march into Mexico City, thousands disrupt the World Bank meeting in
			Prague, Churchill gets an anarchist make-over: from Bognor to Bogota, Dudley to
			Delhi, and Kilburn to Melbourne, resistance has become as global as the institutions
			of capitalism. This was a year full of stories of people at the frontline of struggles
			worldwide, and creating sustainable solutions to the corporate carve-up of the
			planet. These 50 issues of SchNEWS along with the best of Squall magazine, plus
			loadsa photos, cartoons, satirical graphics, subverts, and a comprehensive contacts
			database are available together in....<br /> <b>SchNEWS/Squall Yearbook 2001</b>
			<br /> 300 pages, ISBN 09529748 4 3, BARGAIN - &pound;3.00 inc. p&amp;p direct
			from us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b>
			</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><img src="../images_issues/schquall.gif" width="100" height="144" align="right" border="1" alt="SchQUALL" /><b><font size="3"><a href="index-201-250.htm"><font size="2">Issues
			201-250<br /> February 1999 - March 2000</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchQUALL': <br> </b></b>Genetically modified
			crops get trashed, animal rights, freemasons, June 18th '99 international day
			of action against capitalism, Exodus Collective, Cuba, the November 30th '99 Battle
			for Seattle, climate change, parties and festivals, indigenous peoples' resistance
			to multinational corporations, the privatisation of everything in sight, crap
			arrests of the week, prisoner support and much more&#133;the full lowdown on the
			direct action movement in Britain and abroad! <br /> These 50 issues of SchNEWS
			plus the best of Squall magazine, top photographs, cartoons, subverts and much
			more are available together in ...<b><br /> SchQUALL - SchNEWS and Squall Back
			To Back</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/survival.gif" width="100" height="143" align="right" border="1" alt="SchNEWS Surviva Guide" /></b><font size="3"><a href="index-151-200.htm"><font size="2">Issues
			151-200 <br /> January 1998 - January 1999 </font></a></font></b><br /> <b>These
			issues are published in the book 'Survival Handbook': </b></b><br> Read all about
			it! Genetic crop sites get a good kicking; streets reclaimed all over the world;
			docks occupied in protest of death at work; protestors rude about multinational
			corporations' plans for world domination... <br /> SchNEWS gives you the news
			the mainstream media ignores. Tells you where to party and protest. Tries not
			to get all po-faced about what's going down in the world. <br /> These 50 SchNEWS
			issues are together with a set of 'survival handbook' stylee articles to help
			you survive into the new millenium... plus photos, cartoons, a comprehensive database
			of nearly five hundred grassroots organisations and more in... <br /> <b>SchNEWS
			Survival Handbook</b><br /> <b>SOLD OUT</b> - You might be able to find a copy
			in your local radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/annual_small.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Orange Book" /></b><font size="3"><a href="index-101-150.htm"><font size="2">Issues
			101-150<br /> December 1996 - January 1998</font></a></font><br /> These issues
			are published in the book 'SchNEWS Annual':</b></b><br> &quot;The McLibel trial
			ends, Manchester Airport second runway protest camp goes mad, eviction at Fairmile,
			the GANDALF Trial, anti-nuclear demos in Germany, the massive Reclaim The Streets
			that took back Trafalgar Square... New Labour breaking each and every promise
			they ever made.<br /> These 50 issues are together with loads more pics, graphics
			and articles in...<br /> <b>SchNEWS Annual </b><br /> 230 pages, ISBN: 0-9529748-1-9,
			&pound;BARGAIN - &pound;2.00 inc. p&amp;p direct from us.<b><br /> <b><a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/round.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Black Book" /><font size="3"><a href="index-051-100.htm"><font size="2">Issues
			51-100 <br /> December 1995 - November 1996</font></a></font></b><br> <b>These
			issues are published in the book 'SchNEWS Round':</b> <br /> </b>Direct Action
			takes over England... M41 motorway gets occupied in huge Reclaim The Streets party...
			The Third Battle Of Newbury... plus the national SchLIVE Tour, the Liverpool Dockers
			go 'direct action', resistance to Jobseekers Allowance, Exodus Collective, Trident
			Ploughshares women go free after smashing Hawk Jet, Selar Opencast Mine protest
			camp, Fairmile - A30 protest camp, Wandsworth Eco-village, the Squatter's Estate
			Agency...SchNEWS publishes the inside story from the activists themselves! <br />
			The 50 issues of SchNEWS featuring all this stuff, plus 60 extra pages of photos,
			cartoons, more articles plus the 1996 activists database is available in...</font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>SchNEWS
			Round</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><p></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/reader.gif" width="101" height="142" align="right" border="1" alt="SchNEWS Reader" /><font size="3"><a href="index-001-50.htm"><font size="2">Issues
			1-50 <br /> November 1994 - November 1995</font></a></font><br /> These issues
			are published in the book 'SchNEWS Reader':</b></b><br> Where It All Began...<br />
			From the pilot edition edition right through the first year of SchNEWS featuring
			the first of many Crap Arrest Of The Weeks, the Criminal Justice Act 'Arrestometer',
			and a running commentary of resistance as the sections of the Criminal Justice
			Act come in attacking Travellers, Squatters, Hunt Sabs, Ravers, Protesters, Footie
			Fans and freedom in general. In the news was... Shoreham Live Animal Export protests,
			the end of the No-M11 Link Rd campaign, Pollok Free State anti-M77 protest in
			Glasgow, eviction of Stanworth Valley 'village in the trees' near Blackburn against
			the building of the M65, opencast mining protest camps in South Wales and much
			more...<br /> These historic 50 issues of SchNEWS, plus loadsa cartoons by Kate
			Evans and more are available in...<b><br /> </b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b><b>SchNEWSreader</b><br />
			<b>SOLD OUT</b> </b>Rare as hen's teeth. If you missed out you can read most of
			the SchNEWS issues here - and if you really want to see em as they were you can
			download scans of them below....</font></p><p><img src="../images_main/spacer_black.gif" width="100%" height="10" /></p></td></tr>
			<tr> <td valign="bottom" width="10" height="2">&nbsp;</td><td valign="top" width="468" height="2"><img src="../images_main/spacer.gif" width="1" height="10" /><br />
			<font face="Arial, Helvetica, sans-serif" size="2"><font size="1">SchNEWS, c/o
			Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> Press/Emergency Contacts: +44 (0) 7947 507866<br />Phone: +44 (0)1273
			685913<br /> email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a></font></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><font size="1">@nti copyright
			- information for action - copy and distribute!</font></font></p>



</div>




<div class='mainBar_right'>

		<?php

		//	Latest Issue Summary
		require "../inc/currentIssueSummary.php";

		?>

</div>






<div class="footer">

		<?php

			//	Include Footer
			require "../inc/footer.php";

		?>

</div>








</div>




</body>
</html>




