<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 772 - 20th May 2011 - A Bit of Hows Yer Intifada?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, palestine, israel, intifada" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT June 1st 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 772 Articles: </b>
<b>
<p><a href="../archive/news7721.php">A Bit Of Hows Yer Intifada?</a></p>
</b>

<p><a href="../archive/news7722.php">Angel Grinders</a></p>

<p><a href="../archive/news7723.php">Bell End?</a></p>

<p><a href="../archive/news7724.php">Madrid Fer It</a></p>

<p><a href="../archive/news7725.php">It Makes No Census</a></p>

<p><a href="../archive/news7726.php">Mexico: Poetic Justice</a></p>

<p><a href="../archive/news7727.php">No Stokes Without Fire</a></p>

<p><a href="../archive/news7728.php">Mound Zero</a></p>

<p><a href="../archive/news7729.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 20th May 2011 | Issue 772</b></p>

<p><a href="news772.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A BIT OF HOWS YER INTIFADA?</h3>

<p>
<p>  
  	<strong>AS SCHNEWS EXAMINES THE NEW WAVE OF PALESTINIAN RESISTANCE</strong>  </p>  
   <p>  
  	A mass non-violent (or at least unarmed) resistance movement is on the move in Palestine. Inspired by the events in neighbouring Arab countries but drawing on decades of resistance - the Third Intifada may be here. The waves of the Arab movement are beginning to lap at the Israeli shore.  </p>  
   <p>  
  	Al-Nakba (literally &lsquo;the catastrophe&rsquo;) day is traditionally the annual focus for Palestinian resistance. On March 15th, a day after Israel&rsquo;s independence day, Al-Nakba day marks the moment in 1948 when the creation of the Jewish state meant exile for the 700,000 Palestinians within its new borders. Displaced by terror, they are still recognised by the U.N as the world&rsquo;s longest term refugees. Some are in the West Bank, many more are spread in the Palestinian diaspora in neighbouring Arab countries.  </p>  
   <p>  
  	This year&rsquo;s Al-Nakba day was infused with the spirit of the Arab Spring. This time round&nbsp; masses of Palestinians marched, unarmed, towards the Israeli border, demanding the right of return. In three separate episodes during the day - on the Syrian border with the Golan Heights, on the Lebanese border and on the border with the Gaza Strip, thousands crossed or attempted to cross the border&nbsp; &mdash;those marching were met with live gun fire. At least a dozen Palestinians died. Many more, most of them young men, were injured.  </p>  
   <p>  
  	A volunteer with the Jordan Valley solidarity movement told SchNEWS: &ldquo;<em>There was a general buzz around this year&rsquo;s Nakba day - posters went up across the West Bank, thousands joined Facebook groups calling for mass demos - there were large demonstrations in Jordan two days before</em>&rdquo;.  </p>  
   <p>  
  	Within the West Bank the largest demonstration was at the Qu&rsquo;alandia checkpoint, which chokes off movement between Ramallah and East Jerusalem. According to one eyewitness - a volunteer with the International Solidarity Movement - &ldquo;<em>Thousands turned up in the morning; there was a constant barrage of teargas and rubber-coated bullets. Three or four ambulances were constantly shuttling back and forth - there was no way we were going to force our way through but our intention was plain, people stayed for thirteen hours. The shebab (youth) lit tyres and hurled fireworks - we&rsquo;re expecting more checkpoint demos tomorrow</em>.&rdquo;  </p>  
   <p>  
  	Meanwhile the border fences were breached in the Golan heights, a long disputed territory between Israel and Syria. Despite several protesters being shot dead,&nbsp; hundreds poured through the broken fence and into nearby village Majdal Shams. Similar scenes occurred in Lebanon. In part these scenes played into the hands of Syria&rsquo;s leadership, who are gunning down protesters (over 850 in the last two months) in an effort to resist the same tide that swept Mubarak out of power. Certainly it seems likely that Assad&rsquo;s henchmen were involved in facilitating the protests in an effort to deflect attention from the own crimes.  </p>  
   <p>  
  	While these mobilisations outside the borders owe a great deal to the new spirit seizing the Arab world, the West Bank and Gazan unarmed resistance movement was already well established. The advent of the Popular Committees - grassroots social movements in the villages in the West bank - has been significant. Our ISM contact told us, &ldquo;<em>This could be the start of something bigger. Year on year the village demos have been getting bigger. More villages are creating Popular Committees every year</em>.&rdquo; In villages like Bi&rsquo;lin (see <a href='../archive/news673.htm'>SchNEWS 673</a>) every Friday mass demonstrations of villagers and internationals brave teargas and rubber bullets to attack the separation barriers that cross their land and divide them from their livelihoods. In some cases they&rsquo;ve achieved remarkable successes, but at a high price. Many have been killed, others maimed with organisers liable to be seized and incarcerated under the Israeli system of administrative detention. &nbsp;  </p>  
   <p>  
  	In the Jordan Valley north of Jerusalem, a population massively reduced from its 1967 high of 300,000 is struggling to survive. Here, according to one volunteer with the Jordan Valley Solidarity campaign, &ldquo;We are building to resist the Occupation&rdquo; and they mean this literally. Jordan Valley communities are surrounded by networks of settlements and settler-only roads. The army and civil authorities maintain tight control over what can be built. One act of resistance is the building of schools in Area C &ndash; an area inside the West Bank under full Israeli security control. Side by side, Internationals and Palestinians build these basic institutions of community and effectively dare the army to demolish them.  </p>  
   <p>  
  	In fact, you could say that the Palestinians were the pioneers of non-violent resistance in the Arab World &ndash; the first intifada (1987-93) originally consisted largely of mass protests and strikes, only later giving way to the armed struggle of the PLO. The second intifada (2000-05) was conducted largely by armed groups - resistance was conducted paramilitary style in an era of suicide bombing and rocket fire from Gaza. Eventually the Palestinian Authority were co-opted into being Israel&rsquo;s watchmen (see <a href='../archive/news627.htm'>SchNEWS 627</a>) (papers revealed to Al-Jazzera showed just how thoroughly the PA sold out the people they were supposed to represent in order to cling onto any vestige of power)&nbsp; and the armed struggle ceased to be effective.&nbsp; Perhaps now the spirit of village resistance is spreading into the towns; the upsurge at Qu&rsquo;alandia checkpoint was the biggest in recent years.  </p>  
   <p>  
  	Only two years ago it looked as if Palestine was going to end up in the model of the rest of the Arab World - rule by a handful of corrupt strongmen on behalf of more powerful outside agencies. But now the people are on the move. Israel may find itself as a theocratic, racially divided state surrounded by democratic(ish) Arab regimes. The risings in Tunisia and Egypt have proved to be a real game changer, leaving the West and Israel struggling to play catch-up. Obama has suddenly found a few billion down the back of the sofa to shower on the Arabs, in an effort to make them forget about the dictatorships foisted on them in the past with enthusiastic US backing. But it isn&rsquo;t Uncle Sam who&rsquo;s making history in the Middle East any more - it&rsquo;s the Arab people.  </p>  
   <p>  
  	*Blog of a Palestinian village in resistance <a href="http://www.iraqburin.wordpress.com/" target="_blank">www.iraqburin.wordpress.com/</a> <br />  
  	International Solidarity Movement&nbsp; palsolidarity.org <br />  
  	Jordan Valley Solidarity <a href="http://www.jordanvalleysolidarity.org" target="_blank">www.jordanvalleysolidarity.org</a> <br />  
  	Brighton Tubas&nbsp; <a href="http://www.brightonpalestine.org." target="_blank">www.brightonpalestine.org.</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1221), 141); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1221);

				if (SHOWMESSAGES)	{

						addMessage(1221);
						echo getMessages(1221);
						echo showAddMessage(1221);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


