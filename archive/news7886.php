<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 788 - 16th September 2011 - De-Fault is all Greece's</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, recession, cuts" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 788 Articles: </b>
<p><a href="../archive/news7881.php">World's Largest Arms Fail</a></p>

<p><a href="../archive/news7882.php">Frack Shit Up</a></p>

<p><a href="../archive/news7883.php">Brighton 9: Sticking It To The Manikin</a></p>

<p><a href="../archive/news7884.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7885.php">Nice One Giza</a></p>

<b>
<p><a href="../archive/news7886.php">De-fault Is All Greece's</a></p>
</b>

<p><a href="../archive/news7887.php">Edl: Tommy Chickens Out</a></p>

<p><a href="../archive/news7888.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th September 2011 | Issue 788</b></p>

<p><a href="news788.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DE-FAULT IS ALL GREECE'S</h3>

<p>
<p>  
  	On Saturday (10th) the Greek PM George Papandreou was due to outline the government&rsquo;s fiscal policy for the year during the Thessaloniki International Exhibition. This resulted in a plethora of demonstrations being called by groups such as the square movement, the socialist union PAME, trade unions GSEE, ADEDY, local football fans (whose team had been relegated due to financial difficulties) and yer protest happy black block.  </p>  
   <p>  
  	On the actual day over 6,000 police were deployed in the city of Thessalonika. There were 32 people detained before demonstrations had even begun, numbers reached triple figures by the day&rsquo;s end.  </p>  
   <p>  
  	According to the Occupied London blog there were at least 10,000 people demonstrating across the city. Scuffles ensued throughout the day between protesters and police with tear gas always at the ready. In Athens 400 people had gathered in Syntagma square in solidarity with Thessaloniki. Police attacked the static demonstration, injuring 15 people in the process. Later on that evening two motorcycle cop units were attacked in the anarchist stronghold district of Exarcheia with Molotov cocktails and stones. The cost of policing amounted to over 2 million Euro (that is 1.74 million pounds to you and me).  </p>  
   <p>  
  	School started for high school students across Greece on 12th. School books for each subject are meant to be distributed to students for free but this year thanks to the cuts books won&rsquo;t be available until late November. The 50% reduction of public transport costs for students has been slashed to none. Political speeches have been banned from all high schools as the government fears further occupations. Meanwhile over 300 university departments are being occupied by students protesting against the new reform bill. The bill in question will privatise higher education, bring in tuition fees and introduce university managers.  </p>  
   <p>  
  	The European Parliament gathered in Strasbourg for its chin wag session this Wednesday 14th. The EU commission made it clear it had &ldquo;no intention of placing Greece under stewardship&rdquo;, in plain non-politician speak that means &ldquo;we are placing Greece under stewardship&rdquo;. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1361), 157); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1361);

				if (SHOWMESSAGES)	{

						addMessage(1361);
						echo getMessages(1361);
						echo showAddMessage(1361);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


