<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - You Never Can Tel Aviv</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, tel aviv" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>

<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>

<b>
<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>
</b>

<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<p><a href="../archive/news75511.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>YOU NEVER CAN TEL AVIV</h3>

<p>
<p>
	Around 15,000 indigent Israelis poured onto the streets of Tel Aviv last Saturday (15th) to demonstrate against the government&rsquo;s attacks on civil and human rights, and its increasingly racist agenda. </p>
<p>
	Chanting &ldquo;Jews and Arabs refuse to be enemies&rdquo; and &ldquo;We will fight the regime of darkness,&rdquo; the marchers lambasted the government of Binyamin Netanyahu and his collaborators in the Labour party. </p>
<p>
	Unrest has been growing in Israel over the increasingly racist atmosphere fuelled by extremist Rabbis calling for a ban on Jews selling property to Arabs, and the recent law passed by the Knesset requiring all Israeli Arabs to swear an oath of allegiance to Israel as a &lsquo;Jewish and democratic state&rsquo;. </p>
<p>
	The match to the tinderbox moment came earlier this month when the Knesset, the Israeli parliament, passed a bill to set up a parliamentary commission to investigate the funding of civil and human rights groups. The move was condemned by opponents as &ldquo;McCarthyite&rdquo; and described by one dissenting minister as having a &ldquo;whiff of fascism&rdquo;. </p>
<p>
	The protesters marched from Meir Park, in front of the headquarters of far-right coalition members the Likud, to the Tel Aviv Museum, where they staged a rally. Afterwards scuffles with the police broke out and riot police cleared the area. There were three arrests. </p>
<p>
	The rally was orgsmash edoanised by activists from over 50 Jewish and Arab human rights and left-wing organisations who marched under the banner of the &ldquo;Democratic Camp&rsquo;. One of the organisers, former Knesset speaker Avraham Burg told the press, &ldquo;Israel is not a democracy any more. Technically it is, but the foundations of democracy &ndash; liberty, equality &ndash; are under threat. The rabbinical fatwas and political harassment are red lights. If we don&rsquo;t stand up now, tomorrow it will be too late.&rdquo; He added, &ldquo;[The movement] is not yet big, but it&rsquo;s a beginning. This is the launching pad for the future political generation of Israel&rdquo;. </p>
<p>
	Amongst the marchers were activists proclaiming solidarity with Israeli activist Jonathan Pollak, who was recently handed a three month prison sentence for his part in a 2008 Critical Mass bike ride protesting Israel&rsquo;s blockade of Gaza. </p>
<p>
	Jonathan is a renowned activist in the Popular Struggle Coordination Committee (PSCC), a grassroots movement that campaigns against the Israeli occupation and a founding member of Anarchists Against the Wall, who join the weekly Palestinian demos against the separation wall. </p>
<p>
	On 31st of January 2008, Jonathan was among 30 Israeli activists on a protest bike ride through Tel Aviv. After he was spotted by plain-clothes police, he was singled out and arrested. He was then accused of incitement and being the leader of the event. After his arrest, the protest was allowed to continue unmolested. </p>
<p>
	He was tried at a Magistrates court in December and convicted on charges of illegal assembly. On sentencing, Jonathan said, &ldquo;<em>I will go to prison wholeheartedly and with my head held high. It will be the justice system itself, I believe, that will need to lower its eyes in the face of the suffering inflicted on Gaza&rsquo;s inhabitants</em>&rdquo;. </p>
<p>
	You can send letters of support to Jonathan to Hermon Prison, NSWING, PO BOX 4011, MAGHAR 14930, Israel. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1068), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1068);

				if (SHOWMESSAGES)	{

						addMessage(1068);
						echo getMessages(1068);
						echo showAddMessage(1068);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


