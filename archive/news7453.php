<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 745 - 29th October 2010 - EDL: Down the Rabbi Hole</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, fascism, racism, uaf, anti-fascist, israel" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 745 Articles: </b>
<p><a href="../archive/news7451.php">Serious Organised Crime</a></p>

<p><a href="../archive/news7452.php">First Cuts Not The Deepest</a></p>

<b>
<p><a href="../archive/news7453.php">Edl: Down The Rabbi Hole</a></p>
</b>

<p><a href="../archive/news7454.php">Somerset Sabs Attacked</a></p>

<p><a href="../archive/news7455.php">Cuts Both Ways</a></p>

<p><a href="../archive/news7456.php">Naples: Load Of Rubbish</a></p>

<p><a href="../archive/news7457.php">Snogfest Vs Nazi Nonce</a></p>

<p><a href="../archive/news7458.php">The Us Itt Parade</a></p>

<p><a href="../archive/news7459.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th October 2010 | Issue 745</b></p>

<p><a href="news745.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDL: DOWN THE RABBI HOLE</h3>

<p>
<p>
	Rabbi, surfer, Tea Party activist, aficionado of aviator sunglasses. Senatorial candidate for California, supporter of fascistic English street armies: Nachum Shiffren is a deeply confusing (or maybe confused) man. </p>
<p>
	On Monday (25th), the rabid rabbi teamed up with far-right goons the EDL to march through London to show their opposition to &ldquo;Islamic extremism&rdquo; and solidarity with the state of Israel. Over 200 EDLers showed up to join Shiffren in a stroll past the Israeli embassy. They were met by a mixed bag of up to 100 counter-protesters. </p>
<p>
	In a rambling speech, the Shiff attacked Muslims &ndash; whom he described as &ldquo;dogs&rdquo; - politicians, lefties, Jews who didn&rsquo;t buy into his message of hate...in fact just about everyone except fascists and racists. At one point he broke into a fist-shaking explosion of Hebrew after telling the baffled leaguers, &ldquo;You won&rsquo;t understand what I&rsquo;m about to say but you will feel my meaning.&rdquo; At least the good rabbi understands the level of debate the EDL operate on... </p>
<p>
	The smattering of useful idiots that make up the EDL Jewish Division were also on hand to defend the EDL. However, leader Roberta Moore&rsquo;s speech came to an abrupt end when a cheeky anti-fascist shorted their PA with water. Enraged EDLers chased the mystery person down the street but he/she managed to evade capture. </p>
<p>
	The unlikely coalition of the rabbi and the racists was condemned by Jewish groups across the country. Amongst the counter-demo were a number of &ldquo;Jews Against the EDL&rdquo; protesters, including radical Jewish group Jewdas, Jews for Justice for Palestinians and Jewish orthodox anti-zionists. The Board of Deputies of British Jews denounced Shiffren&rsquo;s relationship with the EDL as an attempt by the group to play minorities off against each other. </p>
<p>
	* Thursday last week (21st), charges levelled at an anti-fascist protester were dropped just 24 hours before he was scheduled to stand trial. Alan Clough was preparing to defend himself against charges of threatening behaviour following last March&rsquo;s EDL demo in Bolton (see <a href='../archive/news715.htm'>SchNEWS 715</a>), which saw over 60 arrests &ndash; almost all of them UAF or counter-protesters. The last minute U-turn came after footage previously hidden away on youtube and in the deep dark vaults of Granada TV which clearly showed a copper punching Clough in the face moments before the 63-year-old was arrested. After witnessing the assault on an innocent man deserving of his free bus pass, a disappointed CPS spokesman said: &ldquo;<em>Having viewed footage received from the defence team, we felt that there was no longer a realistic prospect of securing a conviction</em>.&rdquo; Shame. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(989), 114); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(989);

				if (SHOWMESSAGES)	{

						addMessage(989);
						echo getMessages(989);
						echo showAddMessage(989);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


