<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 661 - 9th January 2009 - Global Outrage</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, protest, gaza, israel, genocide, raytheon, edo-mbm, london, stop the war coalition, tel aviv" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 661 Articles: </b>
<p><a href="../archive/news6611.php">Eyewitness : Rafah</a></p>

<p><a href="../archive/news6612.php">Eyewitness : Gaza</a></p>

<b>
<p><a href="../archive/news6613.php">Global Outrage</a></p>
</b>

<p><a href="../archive/news6614.php">Gaza Relief Boat Rammed</a></p>

<p><a href="../archive/news6615.php">Ceasefire? What Ceasefire?</a></p>

<p><a href="../archive/news6616.php">Gaza Gameplan</a></p>

<p><a href="../archive/news6617.php">Media War</a></p>

<p><a href="../archive/news6618.php">Elementary My Dear Watson</a></p>

<p><a href="../archive/news6619.php">Inside Schnews</a></p>

<p><a href="../archive/news66110.php">Outside Schnews</a></p>

<p><a href="../archive/news66111.php">Shac Verdict</a></p>

<p><a href="../archive/news66112.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th January 2009 | Issue 661</b></p>

<p><a href="news661.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>GLOBAL OUTRAGE</H3>

<p>
Protests and peace vigils around the world began the day after the bombing of Gaza began on December 27th. While the Stop The War Coalition were straight into organising, they weren't the only ones, and many at the demos weren't prepared to let STW stewards call the shots.  <br />
 <br />
Significantly the crowds at many Europe-wide demos were made up of Muslim/Arab people. Also present in numbers were yer organised lefties, and various anarchist types also braving the winter cold to join in. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:75%; align:left" align="left">&nbsp;</div> <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/661-03-raytheon-rooftop-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/661-03-raytheon-rooftop-sm.jpg"  alt='Protesters have been occupying the rooftop of Raytheon, Bristol, since December 9th.'  /></a></td></tr><tr><td align='center'><div style="text-align: center; font-size:7px; color:#444444; border-bottom:solid 1px #666666; margin-bottom:6px; padding-bottom: 6px; width:275">Protesters have been occupying the rooftop of Raytheon, Bristol, since December 9th.</div></td></tr></table><h3>Anti-Arms Trade Campaigns</h3> <br />
 <br />
For anti-militarism campaigns, it's mostly been business as usual as they continued to target companies profiting from this and every other war - such as <b>EDO-MBM/ITT in Brighton and Raytheon in Bristol</b> - but obviously this issue is highlighted during such a bloodthirsty military adventure. <br />
 <br />
The most notable recent anti-arms trade effort - and particularly impressive during the winter cold-snap - is the continued occupation by three protesters of the roof at Raytheon in Bristol. This occupation began on December 9th, well before the Gaza attack, and has been supported by several demos at the premises under the banner 'Smash Raytheon' (See <a href="../archive/news660.htm">SchNEWS 660</a>). Raytheon is a US arms giant who specialise in missiles, and will be raking it in during the Israeli attacks. <br />
 <br />
In fact the Bristol Business Park, Frenchay, has become a bit of a protest hot-spot, as across the road from Raytheon is Boeing, another corporation at the heart of the US war machine. Boeing's range of aircraft reads like a who's who of death machines, including the infamous Apache helicopter gunships which are currently terrorising Gaza, plus F15 fighter jets, Hellfire missiles and more. On New Years Eve ten windows were smashed at Boeing's Bristol premises. <br />
 <br />
* There are two <b>Rooftop Solidarity</b> demos this week at <b>Raytheon</b> - January 8th at 2.30pm, and Saturday the 10th, at 12.30pm. Come and lend support as the rooftop dwellers go into their fifth (freezing) week. For transport to both demos from central Bristol meet at the top of Picton St, off Stokes Croft, at the designated start times. For more see <a href="http://raytheonout.wordpress.com" target="_blank">http://raytheonout.wordpress.com</a> and <a href="http://bristol.indymedia.org" target="_blank">http://bristol.indymedia.org</a> <br />
 <br />
* All week (5th-10th) <b>Bristol</b> daily peace vigil, weekdays 5pm-6pm, Saturday 3pm-4pm at the 'Centre', opposite the Hippodrome. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:75%; align:left" align="left">&nbsp;</div> <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/661-03-london-demo-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/661-03-london-demo-sm.jpg"  alt='Another Israeli flag burns outside Downing St, Jan 4th'  /></a></td></tr><tr><td align='center'><div style="text-align: center; font-size:7px; color:#444444; border-bottom:solid 1px #666666; margin-bottom:6px; padding-bottom: 6px; width:275">Another Israeli flag burns outside Downing St, Jan 4th</div></td></tr></table><h3>Protests in Britain since the attack started</h3> <br />
 <br />
* <b>Saturday 28th December</b> and Sunday 29th saw quick responses to the Israeli aggression. Around 2000 showed up outside the Israeli embassy in <b>London</b> to express their outrage, and the police utterly failed to keep the demonstrators inside their protest pen. Despite the best efforts of the Stop the War organisers the demos were definitely of your up-fer-it variety. One STW organiser was heard saying "there's just too many people here." As well as plenty of argy bargy with the police, the scene of a copper's hat in flames provided warmth and cheer on a cold winter's night. <br />
 <br />
* <b>On Friday Jan 2nd</b> thousands marched through <b>London</b>, ending with bloody clashes at the <b>Israeli Embassy</b>. Protesters threw shoes at 10 Downing St, and one firework exploded at the iron gates to Downing St.  <br />
 <br />
* <b>There was a hastily called international day of action on Saturday the 3rd and British cities came out in force...</b> The biggy was <b>London</b> with 50,000-odd, while 4,000 came out in <b>Manchester</b>, 3000 in <b>Edinburgh</b>, plus hundreds turned out at many places, including <b>Brighton, Portsmouth, Lancaster, Liverpool, Newcastle, and Exeter</b>. In <b>Birmingham</b> demonstrators tried to storm the Town Hall, but the SWP managed to defuse the momentum that the protest had. <br />
 <br />
* <b>Last Sunday (4th)</b>, there was a demo outside the Israeli Embassy on Kensington High St, London. Then on Monday saw demos in Edinburgh, Glasgow, and later than night in Brighton pixies painted the road outside their local bomb factory, EDO-MBM/ITT, writing "EDO kills kids in Gaza for cash" on Home Farm Rd outside EDO. <br />
 <br />
* <b>On Friday (9th)</b> there's a demo in <b>Bristol</b> - march from Easton to the city centre. Assemble 2pm Stapleton Road (by the Railway Bridge) <a href="http://bristol.indymedia.org" target="_blank">http://bristol.indymedia.org</a>  <br />
 <br />
* <b>Mass London Demo</b> against attack on Gaza - this Saturday (10th). Meet 12.30pm at Speakers Corner, Hyde Park, march to Israeli Embassy, High St. Kensington. <a href="http://www.stopwar.org.uk" target="_blank">www.stopwar.org.uk</a> <br />
 <br />
* Also on <b>Saturday</b> (10th) in <b>Leeds</b> there's a peace vigil - 12.30pm, Leeds Art Gallery, The Headrow; vigil in Wrexham meet 12 noon at Queens Square, Wrexham. <br />
 <br />
* In <b>London</b> there are daily protests this week at the <b>Israeli Embassy</b>, Kensington High Street, W8, from 5.30pm-7pm (tube - High Street Kensington).  <br />
 <br />
* In <b>Brighton</b> on Sunday 11th, Palestine Solidarity Campaign march and rally, 1pm, Palmeira Sq. <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:75%; align:left" align="left">&nbsp;</div> <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/661-03-london-demo2-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/661-03-london-demo2-sm.jpg"  alt='Another Israeli flag burns outside Downing St, Jan 4th'  /></a></td></tr><tr><td align='center'><div style="text-align: center; font-size:7px; color:#444444; border-bottom:solid 1px #666666; margin-bottom:6px; padding-bottom: 6px; width:275">Another Israeli flag burns outside Downing St, Jan 4th</div></td></tr></table><h3>International Protests</h3> <br />
 <br />
<b>Spontaneous protests erupted in city centres across the world after the attack began, often targeting the local Israeli Embassy. This is just a snapshot of some of the events from January 3rd:</b> <br />
 <br />
* <b>In Israel:</b> Last Saturday, as the Israeli army began its bloody ground offensive in Gaza, some 10,000 descended on <b>Tel Aviv</b> to protest against their government's war crimes. The police tried to stop the demo, saying it would be attacked by right-wing rioters (well they got that bit of it right!). Police also demanded that the organisers prevented the hoisting of Palestinian flags, but this was overturned by a petition to a High Court judge. <br />
 <br />
The far-right thugs did their best to break up the peace demo. While police kept the two sides apart during the demo, at the end they disappeared, allowing the right wingers to attack the remainder of the marchers. Peace protesters managed to take refuge in a building, but the right wing nutters attempted to break in, threatening to 'finish them off'. Eventually police came but stood back as the thugs lurked on the street to intimidate. <br />
 <br />
* But this wasn't the biggest demo in Israel. In the northern town of <b>Sakhnin</b> some 50,000+ took to the streets in unprecedented numbers - mostly Palestinian citizens of Israel. Around 1,000 later left there in a convoy of coaches to join the Tel Aviv demo. <a href="http://gush-shalom.org" target="_blank">http://gush-shalom.org</a> <br />
 <br />
* An Israeli military airbase at <b>Sde Dov</b> was blockaded with a 'die-in' by nineteen activists from <b>Anarchists Against the Wall</b>. All were arrested and kept in custody while one minor was held under house arrest. Police are threatening to prosecute them as a criminal group conspiring to commit more serious sabotage.  <br />
 <br />
* <b>Across the Middle East and the Muslim world there have been large protests</b>. Last Friday, after prayers, large demos erupted including 10,000 in <b>Djakarta</b>, plus <b>Tehran, Istanbul, Cairo, Amman, Damascus, Kabul, Khartoum</b>, the northern Sinai city of el-Arish - Egypt's closest city to Gaza, and many others. In <b>Aden, Yemen</b>, protesters broke into the Egyptian consulate protesting against Cairo's non-response to Israel's offensive. Within the West Bank street battles and demos kicked off, including a large demo in <b>Ramallah</b>. <br />
 <br />
* <b>European cities</b> saw large turn-outs including 20,000 in <b>Paris</b>, 10,000 in <b>Amsterdam</b>, and others. There were marches across the <b>US</b> but apart from several thousand in <b>New York</b> they didn't recall the heady days of the Iraq Invasion. Most <b>Australian</b> cities held marches, with <b>Melbourne</b> turning out 6,000. In <b>Oaxaca, Mexico</b>, demonstrators from the local social movement marched on the US consulate, but were attacked by police and shot at with tear gas, with 19 violently arrested, and many beaten and robbed while in the lock-up. <br />
 <br />
But as has said countless times, it's not just about making up the numbers on the city A-B marches. In Britain there is a building movement against arms industry targets - which has grown out of the 2003 anti-war movement. In recent years successful campaigns have hit EDO-MBM/ITT in Brighton, Raytheon in Bristol, Heckler & Koch in Nottingham, as well as ongoing peace camps like Faslane in Scotland. The politicians aren't listening, so the companies who supply the war machine are legitimate and effective targets for action. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:25%; align:left" align="left">&nbsp;</div> <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/661-03-london-demo-ad.png" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/661-03-london-demo-ad.png"  /></a></td></tr></table> <br />
 <br />
<table align="center" width="50%" style="padding:10px; border: double 4px black"><tr><td align="center"> <br />
<b>For more info see</b> <a href="<br />
http://electronicintifada.net" target="_blank"><br />
http://electronicintifada.net</a>  <a href="<br />
http://www.maannews.net/en" target="_blank"><br />
http://www.maannews.net/en</a> <a href="<br />
http://www.freegaza.org" target="_blank"><br />
http://www.freegaza.org</a> <a href="<br />
http://www.palsolidarity.org" target="_blank"><br />
http://www.palsolidarity.org</a> <a href="<br />
http://talestotell.wordpress.com" target="_blank"><br />
http://talestotell.wordpress.com</a> <br />
 <br />
</td></tr></table>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=661">direct action</a>, <a href="../keywordSearch/?keyword=protest&source=661">protest</a>, <a href="../keywordSearch/?keyword=gaza&source=661">gaza</a>, <a href="../keywordSearch/?keyword=israel&source=661">israel</a>, <a href="../keywordSearch/?keyword=genocide&source=661">genocide</a>, <a href="../keywordSearch/?keyword=raytheon&source=661">raytheon</a>, <a href="../keywordSearch/?keyword=edo-mbm&source=661">edo-mbm</a>, <a href="../keywordSearch/?keyword=london&source=661">london</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(182);
			
				if (SHOWMESSAGES)	{
				
						addMessage(182);
						echo getMessages(182);
						echo showAddMessage(182);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>