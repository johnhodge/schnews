<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

	incrementIssueHitCounter(45);

	if (isset($_GET['print']) && $_GET['print'] == 1)	{

		$print 	=	true;
		$id		=	45;
		require "../archive/show_print_issue.php";
		exit;

	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 678 - 5th June 2009 - 24 YEARS ON, SchNEWS REVISITS THE BATTLE OF THE BEANFIELD...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, protest, hill of tara, digger diving, battle of soldiers hill, palestine, holy land foundation, hamas, arms trade, forward intelligence team, police, rossport, shell, direct action, protest camp, civil liberties, socpa, brian haw, parliament square, protest camp, g8, l&#8217;aquila, anti-globalisation, summit hopping, italy, deportation, british midland, iran, stonehenge, savernake forest, police brutality, free party, new age travellers, police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>

<style type="text/css">
<!--
.style1 {font-weight: bold}
-->
</style>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.london.noborders.org.uk/calais2009" target="_blank"><img
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news677.htm">SchNEWS 677, 29th May 2009</a></b><br />
<b>Mosquito Bite</b> - The M&iacute;skito people on the coast of Nicaragua have broken away and declared themselves a new nation, in defiance of Daniel Ortega's government... plus, Bury Hill Wood, part of the Abinger Forest, Surrey is under threat from oil exploration by Europa Oil and Gas, plans are afoot to squat land in the Hammersmith area of London and turn it into an eco-village, things are getting harder for vivisection lab Huntingdon Life Sciences, as major shareholder Barclays bank pulls out investment, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news676.htm">SchNEWS 676, 22nd May 2009</a></b><br />
<b>Final Frontiers</b> - No Borders special as SchNEWS reports on the humanitarian crisis in Calais caused by British immigration policy... plus, will the belated withdrawal of British backing affect the Colombian military and their reign of violence, the Sri Lankan government claims victory over the Tamil Tigers but we question at what price, anti-Trident protesters have occupied a new site at the Rolls Royce Rayensway hi-tech manufacturing facility in Derby, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news675.htm">SchNEWS 675, 8th May 2009</a></b><br />
<b>Brighton Rocked</b> - As Smash EDO Mayday mayhem hits the streets of Brighton we report from the mass street party and demonstration against the arms trade, war and capitalism... plus, the bike-powered Coal Caravan concludes its three week tour of the North Of England, a US un-manned Predator drone accidently bombs the village of Bala Baluk, killing up to 200 innocent civilians, RavensAit, the squatted island on the Thames near Kingston, is finally evicted this week, the Nine Ladies protest camp, having won its ten-year battle to stop the quarrying of Stanton Moor in Derbyshire, has finally tatted down and finished, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news674.htm">SchNEWS 674, 1st May 2009</a></b><br />
<b>May the Fourth be With You</b> - With Smash EDO's Mayday Mayhem about to hit Brighton, here's some useful information covering the day.... plus, RBS&nbsp;are seeking &pound;40,000 in compensation from a seventeen year old girl who damaged one computer screen and keyboard at the G20 demo last month, the Metropolitan Police are forced to admit they assaulted and wrongfully arrested protesters during a 2006 demo at the Mexican Embassy in London, the IMF/World Bank meeting in Washington DC is met with three days of protests,&nbsp;one London policeman is sacked for admitting that the police killed someone while another is merely disciplined for saying that he 'can't wait to bash up some long haired hippys', and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news673.htm">SchNEWS 673, 24th April 2009</a></b><br />
<b>Quick Fix</b> - The twelve students arrested this month as alleged 'terrorists' have been released without charge - like many 'anti-terrorist operations' in Britain, this one becomes a joint operation between the police and the media... plus,&nbsp;a protester in the West Bank town of Bil'in is killed by the Israel military while protesting against the 'Apartheid Wall',&nbsp;Birmingham squatters prevent two separate community squats in the city from being evicted on the same day, the bloodshed continues in Sri Lanka,&nbsp;a coalition hasformed called The United Campaign Against Police Violence, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news672.htm">SchNEWS 672, 17th April 2009</a></b><br />
<b>Easy, Tiger...</b> - A civil war is escalating in Sri Lanka between the Army and the separatist rebels of the Tamil Tigers, with the death-toll mounting... plus, pro-democracy demonstrations in Egypt face overwhelming repression in last weeks planned 'Day Of Anger', the truth about the murder of Ian Thomlinson by police at the G20 protests is coming out, Mexican authorities get revenge by framing leaders of 2006's mass movement for social and political change, one of the so-called EDO 2 receives sentence for Raytheon roof-top protest, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news671.htm">SchNEWS 671, 3rd April 2009</a></b><br />
<b>G20 SUMMIT EYEWITNESS REPORT</b> - As the G20 Summit takes place, we look at what the leaders are discussing, along with an eyewitness account of the G20 protests around the Bank Of England on Wednesday April 1st.... plus, a report from day two of the protests, on the day when the G20 Summit began at the ExCel entre in the East London Docklands, the squatted island at Raven's Ait, on the Thames near Surbiton, is under threat from eviction and calling for help, groups of sacked workers who are feeling the direct consequences of the financial crash are protesting, and re-occupying their work places, peace campaigner Lindis Percy causes traffic chaos outside the USAF airbase at Lakenheath in Suffolk, and more....</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news670.htm">SchNEWS 670, 27th March 2009</a></b><br />
<b>G20 SUMMIT SPECIAL</b> - SchNEWS looks how deep the financial problems are for the banks and the British Govt, and how they won't learn from their errors. We ask the question: the crash which looked inevitable in 1999 has happened, so we look to how we can survive this.... plus, ex-Royal Bank Of Scotland boss Fred 'The Shred' Goodwin, has his home and car attacked by demonstrators, Horfield Prison in Bristol is surrounded by a noise demo in solidarity with Elija Smith, one of the EDO Decommissioners, Climate change campaigners stop work at Muir Dean open-cast coal mine in Scotland, and more...</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news669.htm">SchNEWS 669, 20th March 2009</a></b><br />
<b>Stand Up To Detention</b> - As the British Home Secretary opens another detention centre for 'failed' asylum seekers, No Borders protests around the country highlighting the plight of those victims of the UK asylum system.... plus, the sixth anniversary of the murder in Gaza of US activist Rachel Corrie is tragically marked by the shooting of another US activist, the largest Gypsy and Traveller community in Britain is seriously under threat of being evicted this year, Smash EDO hold an all night demo outside the Brighton factory of EDO-ITT, and more.....</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">	<table border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
							<tr>
								<td>
									<div align="center">
										<a href="../images/678-beanfield-lg.jpg" target="_blank">
											<img src="../images/678-beanfield-sm.jpg" alt="The Battle of the Beanfield, June 1st, 1985" border="0" >
											<br />click for bigger image
										</a>
									</div>
								</td>
							</tr>
						</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th June 2009 | Issue 678</b></p>
<p><b><i>WAKE UP!! IT'S YER RIDING THE SITE-GEIST....</i></b></p>

<h3><i>SchNEWS</i></h3>

<p><font size="1"><a href="../archive/pdf/news678.pdf" target="_blank">PDF Version - Download, Print, Copy and Distribute!</a></font></p>
<p><font size="1"><a href='../archive/news678.php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>
<p>
<b>Story Links : </b> <a href="../archive/news6781.php">Convoy Polloi</a>  |  <a href="../archive/news6782.php">The Rich Get Richter</a>  |  <a href="../archive/news6783.php">Tara For Now  </a>  |  <a href="../archive/news6784.php">Deportation Alert</a>  |  <a href="../archive/news6785.php">Holy Unjustified</a>  |  <a href="../archive/news6786.php">Photo Finish</a>  |  <a href="../archive/news6787.php">Rebels Without A Causeway</a>  |  <a href="../archive/news6788.php">Brian Of Britain</a> 
</p>


<div id="article">

<h3>CONVOY POLLOI</h3>

<p>
<p><strong>24 YEARS ON, SchNEWS REVISITS THE BATTLE OF THE BEANFIELD...</strong> <br />  <br /> <em>&ldquo;What I have seen in the last thirty minutes here in this field has been some of the most brutal police treatment of people that I&rsquo;ve witnessed in my entire career as a journalist. The number of people who have been hit by policemen, who have been clubbed whilst holding babies in their arms in coaches around this field, is yet to be counted. There must surely be an enquiry after what has happened today.&rdquo;</em> - <strong>Ken Sabido ITN journalist.</strong>&nbsp; <br />  <br /> Twenty four years have passed since the defining moment of the Thatcher government&rsquo;s assault on the traveller movement - the Battle of the Beanfield. On June 1st 1985 a convoy of vehicles set off from Savernake Forest in Wiltshire towards Stonehenge, with several hundred travellers on their way to setting up the 14th Stonehenge Free Festival. But this year English Heritage, who laughably were legally considered the owners of the Stonehenge Sarsen circle (built several thousand years before by god knows who), had secured an injunction against trespass naming 83 people. This was considered legal justification enough for a brutal assault on the entire convoy. What followed was a police riot and the largest mass arrest in British history. <br />  <br /> As the Convoy made its way to the Stones the road was blocked with tonnes of gravel and it was diverted down a narrow country lane, which was also blocked. Suddenly a group of police officers came forward and started to break vehicle windows with their truncheons. Trapped, the convoy swung into a field, crashing through a hedge. <br />  <br /> For the next four hours there was an ugly stalemate. The Convoy started trying to negotiate, offering to abandon the festival and return to Savernake Forest or leave Wiltshire altogether. The police refused to negotiate and told them they could all surrender or face the consequences. <br />  <br /> At ten past seven the &lsquo;battle&rsquo; began. In the next half hour, the police operation &ldquo;<em>became a chaotic whirl of violence</em>.&rdquo; Convoy member Phil Shakesby later gave his account of the day: <br />  <br /> &ldquo;<em>The police came in [to the grass field] and they were battering people where they stood, smashing homes up where they were, just going wild. Maybe about two-thirds of the vehicles actually started moving and took off, and they chased us into a field of beans.&nbsp; <br />  <br /> By this time there were police everywhere, charging along the side of us, and wherever you went there was a strong police presence. Well, they came in with all kinds of things: fire extinguishers and one thing and another. When they&rsquo;d done throwing the fire extinguishers at us, they were stoning us with these lumps of flint</em>.&rdquo;&nbsp; <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/678-henge-84-stones-herb-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/678-henge-84-stones-herb-sm.jpg"  /></a></td></tr></table>By the end of the day over four hundred were under arrest and dispersed across police stations around the whole of the south of England. Their homes had been destroyed, impounded and in some cases torched. <br />  <br /> <strong>THE VAN GUARD?</strong> <br />  <br /> In today&rsquo;s surveillance society Britain it is seems inconceivable that festivals like the Stonehenge Free Festival ever happened. At their height these gatherings attracted 30,000 people for the solstice celebration - 30,000 people celebrating and getting on with it without any need for the state or its institutions. The festivals themselves were just the highpoint of a year-round lifestyle of living in vehicles. As one traveller said at the time, &ldquo;<em>The number of people who were living on buses had been doubling every year for four years. It was anarchy in action, and it was seen to be working by so many people that they wanted to be a part of it too</em>.&rdquo;&nbsp; <br />  <br /> Having seen off the miners strike &ndash; the first casualties in the plan to re-order Britain according to  neo-liberal economics (or as it was known locally - Thatcherism), the state turned its force on a more subtle threat. This time not people fighting for jobs and a secure place in the system but people who rejected that system outright. Although prejudice against travellers was nothing new, the traditional &lsquo;ethnic&rsquo; travelling minority represented no significant threat to the status quo that couldn&rsquo;t be dealt with by local authorities. But to many of the millions left unemployed by the Thatcher revolution, life on the road looked increasingly appealing. This was inconvenient for a state determined that conditions for the unemployed be miserable enough to spur them into any form of low-paid work. <br />  <br /> <strong>WHEELS ON FIRE</strong> <br />  <br /> The propaganda directed against the so-called &lsquo;peace convoys&rsquo; by all sections of the media created an atmosphere which allowed draconian action. The Beanfield was not an isolated incident. The Nostell Priory busts of the previous year were a vicious foreboding of what was to come. Months before the Beanfield a convoy-peace camp site at Molesworth was evicted by police acting with 1500 troops and bulldozers headed by a flak-jacketed Michael Heseltine, then Defence Secretary. In 1986 Stoney Cross in the New Forest saw another mass eviction. At the time Thatcher said she was "<em>only too delighted to do what we can to  make things difficult for such things as hippy convoys</em>". This attempt to create a separate yet peaceful existence from mainstream society was to be ruthlessly suppressed.&nbsp; <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/678-tash-1-sm.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/678-tash-1-sm.jpg"  /></a></td></tr></table>Over the next ten years &ndash; notably with the Public Order Act 1986 and the Criminal Justice Act 1994 the whole lifestyle was virtually outlawed. As John Major said at the Tory Party conference in 1992 to thunderous applause: &ldquo;New age travellers &ndash; not in this age &ndash; not in any age&rdquo;. The CJA removed the duty of councils to provide stop-over sites for travellers and regular evictions began to punctuate traveller life. But it wasn&rsquo;t all one way, thousands stayed on the road and the free festival circuit was infused with fresh blood from the rave scene. Even after the massive crackdown that followed the Castlemorton free festival the convoys in many cases moved onto road protest sites. <br />  <br /> Ultimately however travellers were forced to adapt - abandoning the garish war paint of the hippy convoys for more anonymous vans, moving and taking sites in smaller groups. Many went abroad or were driven back into the cities. However, despite the worst excesses of the cultural clampdown, travellers remain all over the country. Many are now in smaller groups, inconspicuous and unregistered. It&rsquo;s become more common for vehicle dwellers to take dis-used industrial sites blurring then lines between travelling and squatting.&nbsp; <br />  <br /> The fact that Stonehenge is now open again on the solstice might - on the face of it - look like a victory. But the event is a top-down affair complete with massive police presence, burger vans and floodlights &ndash; a far cry from the anarchistic experiments of the 70s and 80s. A smaller gathering had been permitted just down the road at the Avebury stone circle over recent years with the National Trust taking a far more lenient stance on live-in vehicles than English Heritage. But even there, since 2007, there's now a ban on overnight stays on the solstice.&nbsp; <br />  <br /> Much of the festival circuit these days is in the hands of profit-motivated commercial promoters apart from the growing shoots of a range of smaller festivals, who continue in the spirit of people-led celebrations of community co-operation. But festivals today are also mostly buried under an avalanche of red tape and security, health and safety requirements - The Big Green gathering saw its security costs treble in one year (2007) as they were told to &lsquo;terrorist harden&rsquo; the event. <br />  <br /> When popular history recalls the pivotal moments in the mid-80s for Thatcher's Britain, the Battle Of The Beanfield rarely adequately takes its place alongside the Miners Strike and Wapping. For UK Plc, travellers became - and remain - another 'enemy within', to be dealt with by organised state violence, like all others who have found an escape route from a society subordinated to profit, where freedom had been reduced to a series of consumer choices. <br />  <br /> * See also <a href='../archive/news25.htm'>SchNEWS 25</a> <br />  <br /> * For the definitive account see Andy Worthington&rsquo;s book &lsquo;The Battle Of The Beanfield&rsquo; - <a href="http://www.andyworthington.co.uk" target="_blank">www.andyworthington.co.uk</a> <br />  <br /> </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>free party</span>, <span style='color:#777777; font-size:10px'>new age travellers</span>, <a href="../keywordSearch/?keyword=police&source=678">police</a>, <a href="../keywordSearch/?keyword=police+brutality&source=678">police brutality</a>, <span style='color:#777777; font-size:10px'>savernake forest</span>, <a href="../keywordSearch/?keyword=stonehenge&source=678">stonehenge</a></div>
<?php echo showComments(346, 1); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>THE RICH GET RICHTER</h3>

<p>
<p>On July 7th-10th the G8 Summit circus dares to return to Italy, eight years after the debacle of Genoa 2001 which saw the death of protester Carlo Giuliani (See <a href='../archive/news314.htm'>SchNEWS 314</a>, <a href='../archive/news315.htm'>315</a>). This year the venue has been changed at the last minute from the Sardinian island of La Maddalena to the rubble-strewn epicentre of this April&rsquo;s earthquake, the town of L&rsquo;Aquila , 120km north-east of Rome, in the region of Abruzzo. Rome will also host parts of the summit. The actual venue of the meeting in L&rsquo;Aquila is Coppito, the largest police academy in the country. But oh the irony of putting the summit and it&rsquo;s entourage of 5,000 in the midst of a disaster area where tens of thousands are still in refugee-style camps. And it&rsquo;s still getting the occasional tremor - so it could go off again (here's hoping). Apparently the change of location is to halve the security bill. <br />  <br /> Another reason to move the summit came after Berlusconi claimed that &ldquo;<em>I don&rsquo;t believe that anti-globalists will have the courage to organize violent demonstrations in this earthquake-stricken region</em>&rdquo;. Which is ridiculous because left and activist groups have been in this area since the earthquake in L&rsquo;Aquila killed 300 and made 30,000 homeless on April 6th 2009, providing food kitchens, infrastructure and aid. With Berlusconi&rsquo;s wobbling government looking more vulnerable than ever in the wake of his dalliances with teenage glamour girls, what better time to protest? <br />  <br /> * See also <a href="http://globalisenot.blogspot.com" target="_blank">http://globalisenot.blogspot.com</a> and <a href="http://www.gipfelsoli.org/Multilanguage" target="_blank">www.gipfelsoli.org/Multilanguage</a> <br />  <br />  <br /> </p>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>anti-globalisation</span>, <a href="../keywordSearch/?keyword=g8&source=678">g8</a>, <a href="../keywordSearch/?keyword=italy&source=678">italy</a>, <span style='color:#777777; font-size:10px'>l&#8217;aquila</span>, <a href="../keywordSearch/?keyword=summit+hopping&source=678">summit hopping</a></div>
<?php echo showComments(347, 2); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>TARA FOR NOW  </h3>

<p>
Fourteen protesters against the motorway development at the Hill Of Tara, Ireland (See <a href='../archive/news585.htm'>SchNEWS 585</a>), have had their cases dismissed in Trim District Court. Most cases arose from the &lsquo;Battle Of Soldiers Hill&rsquo;, on July 18th last year (See <a href='../archive/news597.htm'>SchNEWS 597</a>), where seven were arrested after being on the end of violence from security during some digger diving to prevent the road-building. <br />  <br /> The protesters&rsquo; defence in court was that they had a lawful excuse to stop the road building which was illegal under EU directives. The judge questioned the Gardai as to whether they&rsquo;d asked the protesters if they believed they had a lawful reason, and Gardai admitted they had failed to. After the Gardai and Siac/Ferrovial security gave contradictory evidence, it seems the judge decided to drop the cases, and avoid any unnecessary scrutiny into the illegality of the road. <br />  <br /> There is still a protest camp &ndash; the Vigil Camp - and direct action at the site, despite the fact that a lot of the destruction has happened, and the road could be finished some time next year. A decade-long campaign has been waged to stop this road and the desecration of land near the ancient monument, with protest camps and direct action since the summer of 2007. The Vigil Camp are still inviting all to come to the site and take part.&nbsp; <br />  <br /> * For Vigil Camp see <a href="http://www.tarapixie.net" target="_blank">www.tarapixie.net</a> <br />  <br /> * For more info about the protests see <a href="http://www.savetara.com" target="_blank">www.savetara.com</a> <br />  <br /> * On August 30th there will be a Heritage Week event at the Hill Of Tara <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>battle of soldiers hill</span>, <span style='color:#777777; font-size:10px'>digger diving</span>, <span style='color:#777777; font-size:10px'>hill of tara</span>, <a href="../keywordSearch/?keyword=protest&source=678">protest</a></div>
<?php echo showComments(348, 3); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>DEPORTATION ALERT</h3>

<p>
Cardiff resident Maryam - who is six months pregnant - and her son Mousa, from Iran, are to be deported 8.15pm TODAY (5th) by BMI airlines from Heathrow on flight BD931. <br />   <br /> Complain to BMI airlines on 0133285400 or 01332854854
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>british midland</span>, <a href="../keywordSearch/?keyword=deportation&source=678">deportation</a>, <a href="../keywordSearch/?keyword=iran&source=678">iran</a></div>
<?php echo showComments(353, 4); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>HOLY UNJUSTIFIED</h3>

<p>
We&rsquo;ve seen the movies &ndash; they just don&rsquo;t like folks being a bit &lsquo;different&rsquo; down in Texas. Last week a Dallas court gave the founders of a charity sending humanitarian aid to Palestine a legal lynching as they sent &lsquo;em down, with scant evidence, for seriously draconian sentences. Two of the defendants got 65 year sentences, one 20, and two others 15 &ndash; one for merely being a voluntary fundraiser for the group. <br />  <br /> This was obviously the &lsquo;right&rsquo; result as far as the government was concerned, with this being the culmination of the most costly US &lsquo;terror financing&rsquo; investigation ever. &nbsp; <br />  <br /> The Holy Land Foundation, once the biggest Muslim charity in America, was shut down shortly after 2001 in the wave of patriotic Homeland Security paranoia as (Texas boy) George Bush looked for scapegoatable terror targets close to home. Despite being nothing to do with Saudi nationals flying planes into buildings (like Saddam Hussein), the charity was accused of being a front for sending cash to support &lsquo;terrorist&rsquo; Hamas in its battle against the Israeli occupiers.&nbsp; <br />  <br /> A ten year investigation turned up nothing more than proof that the charity did exactly what it claimed to, i.e. sent money to officially recognised Palestinian charities which was provably distributed to provide humanitarian aid &ndash; charities which the US government itself also gave help to through USAID for some years after Holy Land was pounced on. Even using &lsquo;secret&rsquo; (ie undisclosed) Israeli intelligence and disputed documents from FBI tapping operations, the founders could only be charged with indirectly supporting terror by dealing with organisations which may have been in touch with or under the control of Hamas &ndash; something they, nor anyone else, could really know or avoid even if they wanted to. <br />  <br /> A previous trial of the defendants had ended in aquittal on some charges and a split jury on others, leading to a mistrial &ndash; so the government just decided to do it all again only this time a with more carefully selected jury. <br />  <br /> At his sentencing hearing, ex charity chairman Ghassan Elashi, himself Palestinian born, said, &ldquo;<em>Nothing was more rewarding than&hellip; turning the charitable contributions of American Muslims into life assistance for the Palestinians. We gave the essentials of life: oil, rice, flour. The occupation was providing them with death and destruction</em>.&rdquo; For his heinous crime, he was sentenced to 65 years in prison. What a great victory for the Land of the Free. <br />  <br /> * For more see: <a href="http://www.democracynow.org/2009/5/29/holy_land" target="_blank">www.democracynow.org/2009/5/29/holy_land</a> <br />  <br /> * Meanwhile, just how cosy that US / Israeli alliance has been revealed this week as the Israeli government looked for a way to derail Obama&rsquo;s demands for a freeze on all settlement activity in the occupied West Bank. Prime Minister Benjamin Netanyahu justified continued settlement construction by citing secret agreements between prior Israeli governments and the Bush administration. The Israeli government says it agreed to the 2003 US-backed Road Map that called for a settlement freeze only on condition that it be allowed to violate the plan&rsquo;s ban on expanding existing settlements. No problemo! said Geroge... Diplomacy in action.&nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=hamas&source=678">hamas</a>, <span style='color:#777777; font-size:10px'>holy land foundation</span>, <a href="../keywordSearch/?keyword=palestine&source=678">palestine</a></div>
<?php echo showComments(349, 5); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>PHOTO FINISH</h3>

<p>
Police may be forced to review the way they store images taken of people on protests following a recent legal victory by peace campaigner Andrew Wood. The appeal court ruled (on May 22nd) that pictures of Wood - media spokesperson for the Campaign Against the Arms Trade (CAAT) - taken on a surveillance operation by Met police must be destroyed because the images were &lsquo;a disproportionate infringement of his human rights&rsquo;. &nbsp; <br />  <br /> Lord Justice Dyson said: &ldquo;<em>The retention by the police of photographs taken of persons who have not committed an offence, and who are not even suspected of having committed an offence, is always a serious matter</em>.&rdquo;&nbsp; <br />  <br /> What this decision will mean for FIT teams busy filming anyone going anywhere near a protest is not yet known. The cops, never keen to see any roll-back of the police state, are now deciding whether to appeal the appeal ruling.&nbsp; <br />  <br /> * Watching them watching you watching them at <a href="http://www.fitwatch.blogspot.com&nbsp;" target="_blank">www.fitwatch.blogspot.com&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=678">arms trade</a>, <a href="../keywordSearch/?keyword=forward+intelligence+team&source=678">forward intelligence team</a>, <a href="../keywordSearch/?keyword=police&source=678">police</a></div>
<?php echo showComments(350, 6); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>REBELS WITHOUT A CAUSEWAY</h3>

<p>
The Rossport Solidarity Camp Summer Gathering took place last weekend at Glengad on the west coast of Ireland to protest against Shell building an offshore pipeline and gas refinery (See SchNEW 595). As expected this gathering was a busman&rsquo;s holiday for the 200-odd activists attending, with actions taking place from Friday night until Monday evening. On Sunday, following another day of direct action, over 200 people from the camp and the local community attempted to breach the compound fence in two places and were met by over 60 Gardai. Five people who managed to scale the fence were held by security guards and later arrested.&nbsp; <br />  <br /> On Monday evening protesters removed part of the Shell causeway that extends into the sea. Half an hour after around 40 protesters had waded out into the sea and removed a large amount of stone, a 70 strong Shell security crew arrived and the protesters dispersed. <br /> The Rossport Solidarity Camp is back for another up-fer-it summer of actions against Shell&rsquo;s gas development. <br />  <br /> * See <a href="http://www.rossportsolidaritycamp.110mb.com" target="_blank">www.rossportsolidaritycamp.110mb.com</a> <a href="http://www.corribsos.com" target="_blank">www.corribsos.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=678">direct action</a>, <a href="../keywordSearch/?keyword=protest+camp&source=678">protest camp</a>, <a href="../keywordSearch/?keyword=rossport&source=678">rossport</a>, <a href="../keywordSearch/?keyword=shell&source=678">shell</a></div>
<?php echo showComments(351, 7); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<div id="article">

<h3>BRIAN OF BRITAIN</h3>

<p>
This Wednesday (2nd) was the eighth anniversary of one-man-walking-protest Brian Haw's camp in Parliament Square, London. He set up in June 2001 - before 9-11 - in protest at the bombing and economic sanctions on Iraq before broadening to include the US and Britain&rsquo;s &lsquo;War on Terror&rsquo; in Afghanistan and Iraq. <br />  <br /> In his time Brian has been raided by police (see <a href='../archive/news545.htm'>SchNEWS 545</a>), taken Metropolitan Police Commissioner Ian Blair to court (see <a href='../archive/news609.htm'>SchNEWS 609</a>), had a classic New Labour authoritarian law drafted just for him (See <a href='../archive/news543.htm'>SchNEWS 543</a>) and won Channel 4&rsquo;s &lsquo;Most Inspiring Political Figure&rsquo; award (see <a href='../archive/news576.htm'>SchNEWS 576</a>). <br />  <br /> Fair play to Brian for keeping on keeping on, but with the newly branded &lsquo;Overseas Contingency Operations&rsquo; still in full swing in Afghanistan and Pakistan, it looks like he could be out there a while yet. <br />  <br /> * See <a href="http://www.parliament-square.org.uk" target="_blank">www.parliament-square.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>brian haw</span>, <a href="../keywordSearch/?keyword=civil+liberties&source=678">civil liberties</a>, <a href="../keywordSearch/?keyword=parliament+square&source=678">parliament square</a>, <a href="../keywordSearch/?keyword=protest+camp&source=678">protest camp</a>, <a href="../keywordSearch/?keyword=socpa&source=678">socpa</a></div>
<?php echo showComments(352, 8); ?>

<div style='border-bottom: 1px solid black'>&nbsp;</div>



<p><b>Disclaimer</b></p><p>SchNEWS reminds all readers about our joint benefit gig with Radio 4A on June 11th at The Volks, Brighton, 10pm-3am, fiver, with live music and DJs. Honest!</p>

<div style='border-bottom: 1px solid black'>&nbsp;</div>


</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>	<p><FONT FACE="Arial, Helvetica, sans-serif"><B>(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</B></FONT></p>
			<p> These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P>
			
			<p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>

