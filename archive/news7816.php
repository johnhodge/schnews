<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 781 - 29th July 2011 - Is-raeli Expensive Here!</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 781 Articles: </b>
<p><a href="../archive/news7811.php">In League With The Devil?</a></p>

<p><a href="../archive/news7812.php">Prison Hunger Strikes</a></p>

<p><a href="../archive/news7813.php">Not So Neat</a></p>

<p><a href="../archive/news7814.php">Stirchley Speaking</a></p>

<p><a href="../archive/news7815.php">Targeting Refugees</a></p>

<b>
<p><a href="../archive/news7816.php">Is-raeli Expensive Here!</a></p>
</b>

<p><a href="../archive/news7817.php">Shelling Out</a></p>

<p><a href="../archive/news7818.php">Sea Shepherd: Not Fareoes</a></p>

<p><a href="../archive/news7819.php">Vedenta: Tin Foiled</a></p>

<p><a href="../archive/news78110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th July 2011 | Issue 781</b></p>

<p><a href="news781.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IS-RAELI EXPENSIVE HERE!</h3>

<p>
<p>  
  	Soaring house prices have prompted a nationwide protest in Israel. Tent cities have popped up around the country and last Saturday around 20,000 marchers took to the streets of Tel Aviv. Elsewhere, roads are being blocked and activists have almost besieged the Knesset.  </p>  
   <p>  
  	This new movement is calling for affordable housing after&nbsp; house prices doubled since 2002, which along with global rises in food and gas prices, have hit ordinary Israelis. Meanwhile in some areas locals are priced out of the housing market or face a shortage of rental homes. A poll by Ha&rsquo;aretz showed 87% of Israelis supported the protests.  </p>  
   <p>  
  	The permanent war-footing of Israeli politics has tended to sideline domestic and economic issues andthe cabinet are now in crisis mode trying to resolve the unfamiliar situation. Their answers so far, based on building a large number of new homes mainly in central Israel, haven&rsquo;t impressed the protesters who want direct action on rental prices.  </p>  
   <p>  
  	The protests&rsquo; significance goes further than just putting the pressure on for lower property costs. The most active people involved in organising the demonstrations are said to be young professionals and students in their 20s and 30s, a group not previously known for their political activism.  </p>  
   <p>  
  	Organising on social networking sites, demonstrators are aware of mirroring other citizen uprisings in the region&nbsp; &ndash; some are even calling the protests Israel&rsquo;s Arab Spring.  </p>  
   <p>  
  	While the Israeli spring is unlikely to bring down the government, or spark civil war, it may have notable repercussions. The housing protests are happening against a background of economic discontent concerning low wages and a widening gap between rich and poor &ndash; a context familiar to many Europeans.  </p>  
   <p>  
  	Second is the hope that the protesters&rsquo; political awakening could prompt a change in Israel&rsquo;s political culture. Politics is dominated by the omnipresent issue of &lsquo;security&rsquo; - if Israeli society is finding its voice on housing could the barriers to debate on these issues also be opened? <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1306), 150); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1306);

				if (SHOWMESSAGES)	{

						addMessage(1306);
						echo getMessages(1306);
						echo showAddMessage(1306);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


