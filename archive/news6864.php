<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 686 - 7th August 2009 - Rotten To The Caucasus</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, chechnya, natalia estemirova, russia, despot" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 686 Articles: </b>
<p><a href="../archive/news6861.php">Blowing In The Wind</a></p>

<p><a href="../archive/news6862.php">Plugging Into The Mainshill</a></p>

<p><a href="../archive/news6863.php">Rabbiting On</a></p>

<b>
<p><a href="../archive/news6864.php">Rotten To The Caucasus</a></p>
</b>

<p><a href="../archive/news6865.php">The Garda They Come</a></p>

<p><a href="../archive/news6866.php">Down Under Fire</a></p>

<p><a href="../archive/news6867.php">Jungle Missive</a></p>

<p><a href="../archive/news6868.php">Yeah Yeah Yeah</a></p>

<p><a href="../archive/news6869.php">Afghan Hounding</a></p>

<p><a href="../archive/news68610.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th August 2009 | Issue 686</b></p>

<p><a href="news686.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROTTEN TO THE CAUCASUS</h3>

<p>
<div align="left">The murder of Russian human rights activist Natalia Estemirova in Chechnya on July 15th was the latest blow in the campaign against those trying to hold the authorities to account for a decade of atrocities in North Caucasus. Natalia was one of the most active members of long-running Russian human rights organisation Memorial, based in the Caucasus. Since Russian forces invaded Chechnya for the second time in recent history in 1999 (Russian Cossacks first invaded over 400 years ago), Natalia had not only documented human rights abuses in the region, but tried to hold the Russian and Chechen authorities to account for their role in the bloodshed.&nbsp; <br /> </div> <br /> The recent press release prepared by Natalia, with damning revelations about the brutal Kadyrov regime's new abductions, extrajudicial executions and public shooting of people in a Chechen village, must have been too much even for the Chechen dictator. Head of the Memorial board, Oleg Orlov, pointed the finger squarely at Kadyrov, acting with Russian complicity. &ldquo;<em>I know for sure who is responsible for the killing of Natalia Estemirova</em>,&rdquo; he said. &ldquo;<em>We all know it is Ramzan Kadyrov, president of the Chechen Republic. Ramzan threatened Natalia, insulted her, believed her to be his personal enemy. We don't know whether it was Ramzan himself who ordered Natalia's murder or if his close associates did it to please the ruling authority. President Medvedev seems satisfied to have a murderer as a head of one of Russia's republics</em>.&rdquo;&nbsp; <br />  <br /> Natalia's murder is the latest in a line of assassinations of people investigating atrocities covered up by the Russian establishment in the region. Three years ago journalist Anna Politkovskaya was murdered following a campaign of intimidation by the Russian and Chechen authorities (see <a href='../archive/news569.htm'>SchNEWS 569</a>). Following international outrage over the murder of Russia's bravest investigative journalist and a perfunctory police investigation, a trial was held earlier this year, at which three men were acquitted. A retrial was recently ordered by the Russian Supreme Court following international pressure. <br />  <br /> In January this year, human rights lawyer Stanislav Markelov and journalist Anastasia Baburova were shot dead in the centre of Moscow. Stanislav had represented the family of a Chechen girl  raped and strangled to death in 2000 by a Russian colonel who had just been released early from prison, as well as other Chechens tortured by Russian forces. <br />  <br /> The escalation of violence in the last year against those investigating atrocities in Chechnya seems to match an upsurge in disappearances in the region. NGOs operating in the region estimate these disappearances at between 3,500-5,000 since the outbreak of the second 'war', with the years 2006-2008 seeing a decline. However, according to Memorial, the last year has seen an increase in disappearances, reportedly at the hands of the police. With no official database of those missing and unidentified bodies ('we don't do body counts') it is left to the volunteers of Memorial to carry out the task. Memorial was in the process of compiling a list of the missing when Natalia was murdered, after which it suspended its work in Chechnya. <br />  <br /> This, no doubt, has long been the intention of the Russian and Chechen authorities, who have been obstructing human rights work for years. The Russian state has brought in new laws making it harder for foreign NGOs such as Amnesty International and Human Rights Watch to operate, whilst closing down domestic ones such as the 'Russian-Chechen Friendship Society' (see <a href='../archive/news531.htm'>SchNEWS 531</a>). <br />  <br /> The relatives of the disappeared who braved official intimidation and reprisals in approaching these NGOs, now have no-one in the region to turn to with the departure of the last independent monitors of the actions of Kadyrov's thugs. Relatives of alleged insurgents recently had their homes burnt down as his regime has become bolder in its hunt for the remaining opposition fighters. <br />  <br /> Russia officially declared an end to its counter-terrorism operations in Chechnya in April, after ten years. The havoc caused by Russian forces and the henchmen of the Kadyrovs in this time has led the toothless European Court of Human Rights to pass judgement over 100 times on Chechnya. However, the realpolitik of the West's need for Russia's vast natural resources, and support over Iran has prevented any meaningful action on these abuses. <br />  <br /> The situation is also far from rosy in neighbouring Caucasian republics. In Ingushetia, independent journalist and outspoken critic of the local regime, Magomed Yevloev, was murdered in police custody last year, while extrajudicial executions and torture by state forces are now commonplace as the insurgency has spilled over from Chechnya. Similar cases have also been reported in Dagestan by federal security forces and local police. <br />  <br /> Originally set up in 1991 to remember the victims of Stalin's repression, Memorial has embarrassed the Russian authorities over its documenting of abuses in the North Caucasus. Last November, its St. Petersburg office was raided and hard disks with the entire digital archive of the atrocities committed under Stalin were confiscated. Such official intimidation is hardly surprising given the current regime's fondness for the good old days of Uncle Joe &ndash; according to Putin,  &ldquo;<em>Stalin and the Soviet regime were successful in creating a great country</em>&rdquo;. <br />  <br /> As the region becomes a no-go area for human rights workers, the North Caucasus appears to be going the way of former Soviet Republics Turkmenistan and Uzbekistan (See <a href='../archive/news498.htm'>SchNEWS 498</a>), where all opposition has been quashed by brutal despots who murder dissenters with impunity. <br />  <br /> * See also <a href="http://www.memo.ru/eng/index.htm" target="_blank">www.memo.ru/eng/index.htm</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>chechnya</span>, <span style='color:#777777; font-size:10px'>despot</span>, <span style='color:#777777; font-size:10px'>natalia estemirova</span>, <a href="../keywordSearch/?keyword=russia&source=686">russia</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(425);
			
				if (SHOWMESSAGES)	{
				
						addMessage(425);
						echo getMessages(425);
						echo showAddMessage(425);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>