<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 774 - 3rd June 2011 - UKBA-RMY</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, asylum seekers, refugees, deportation, iraq" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 774 Articles: </b>
<p><a href="../archive/news7741.php">Going To Hell-as</a></p>

<p><a href="../archive/news7742.php">Hinkley: Gone Frission</a></p>

<p><a href="../archive/news7743.php">Not Playing Workfare</a></p>

<p><a href="../archive/news7744.php">Basildon Brush Off</a></p>

<p><a href="../archive/news7745.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7746.php">Hasta La Democracia</a></p>

<p><a href="../archive/news7747.php">Smooth Operators</a></p>

<p><a href="../archive/news7748.php">Gaza Freedom Flotilla Ii</a></p>

<b>
<p><a href="../archive/news7749.php">Ukba-rmy</a></p>
</b>

<p><a href="../archive/news77410.php">Schnews In Graphic Detail Out Now</a></p>

<p><a href="../archive/news77411.php">Gm: Sooner Or Tater</a></p>

<p><a href="../archive/news77412.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd June 2011 | Issue 774</b></p>

<p><a href="news774.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>UKBA-RMY</h3>

<p>
<p>  
  	Twenty four Iraqis have gone on hunger strike this week in an Oxford detention centre in protest against the government&rsquo;s plans to deport them.  </p>  
   <p>  
  	More than 70 refugees are facing deportation to Baghdad and in the last two years Britain has deported 300 Iraqis. There are currently around 3,200 people in immigration detention centres and the UK locks up 20,000 so-called &lsquo;illegals&rsquo; a year &ndash; more than any other European country.  </p>  
   <p>  
  	The Iraqis are by no means alone in their plight. Betty Tibikawa, a 22 year old Ugandan lesbian, has been denied asylum and is due to be deported back to Uganda, where homosexuality is illegal. Betty was &lsquo;outed&rsquo; in a national newspaper and attacked and beaten before coming to the UK. High profile gay rights activist David Kato was murdered in Uganda in January, and a similar fate could be waiting for Betty.  </p>  
   <p>  
  	Edson &ldquo;Eddy&rdquo; Cosmas, an openly gay man originally from Tanzania, is being held at Harmondsworth Removal Centre. He is threatened with deportation back to Tanzania where he&rsquo;d been beaten, stoned, abused and tortured for his sexuality, and where the &lsquo;crime&rsquo; of homosexuality carries up to 30 years imprisonment.  </p>  
   <p>  
  	There will be a No One Is Illegal public meeting in Oxford Town Hall on 16th June. In the meantime check <a href="http://barbedwirebritain.wordpress.com," target="_blank">http://barbedwirebritain.wordpress.com,</a> <a href="http://www.noii.org.uk" target="_blank">www.noii.org.uk</a> and <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> for more info about repression against asylum seekers in Britain.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1243), 143); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1243);

				if (SHOWMESSAGES)	{

						addMessage(1243);
						echo getMessages(1243);
						echo showAddMessage(1243);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


