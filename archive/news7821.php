<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 782 - 4th August 2011 - From Cradle to Graves</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, africa, environment" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 782 Articles: </b>
<b>
<p><a href="../archive/news7821.php">From Cradle To Graves</a></p>
</b>

<p><a href="../archive/news7822.php">Is It The End Of The Line For Shell?</a></p>

<p><a href="../archive/news7823.php">Safe To Go Back In The Water?</a></p>

<p><a href="../archive/news7824.php">Brutality In A China Shop</a></p>

<p><a href="../archive/news7825.php">Cia: We Do What We Haftar</a></p>

<p><a href="../archive/news7826.php">What Not To Malware</a></p>

<p><a href="../archive/news7827.php">Schnews In Brief</a></p>

<p><a href="../archive/news7828.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th August 2011 | Issue 782</b></p>

<p><a href="news782.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FROM CRADLE TO GRAVES</h3>

<p>
<p>  
  	<strong>AS THE WEST CAUSE MORE PROBLEMS IN EAST AFRICA THAN THE DROUGHTS</strong>  </p>  
   <p>  
  	East Africa is currently experiencing one of the worst famines in recent history, with most of Somalia and large parts of Ethiopia experiencing severe food shortages. The west has been quick to offer the solution &ndash; more aid, more trade and flying in David Cameron (although he left early to try and save his job back home). This approach of aid, trade and sending rich white men to point at the locals has been the standard response to African disasters for the past 25 years and yet little progress seems to have been made.  </p>  
   <p>  
  	It is true that the region is in the midst of an extreme drought and as a result pasture is parched and water sources for livestock have dried up. Unfortunately this means that if you bought an Ethiopian farmer a goat for Christmas, it&rsquo;s probably already dead. However, this famine was far from unavoidable. Whether or not the drought itself can be blamed on human activity is debatable, yet the impact of western &ldquo;foreign policy objectives&rdquo; is far clearer and, conversely, much less reported.  </p>  
   <p>  
  	Africa is relatively sparsely populated and contains 50% of the world&rsquo;s uncultivated arable land &ndash; more than enough to feed its populace. On top of this many of the currently in-vogue resources (including gold, diamond, platinum, oil and uranium) can be found in the region, so even in the worst drought the East Africans ought to be able to buy themselves out of trouble - yet this clearly isn&rsquo;t the case.  </p>  
   <p>  
  	Recently the US has compounded the situation in Somalia with the use of drones to bomb the country (see <a href='../archive/news780.htm'>SchNEWS 780</a>). Of course this kind of western intervention in Africa is nothing new&nbsp; &ndash; events like the French bombing of the Central African Republic to British support for Morocco&rsquo;s occupation of Western Sahara barely even register in the mainstream press. Recent ideologically- framed coverage of Libya has been a notable and predictable exception to this (see <a href='../archive/news777.htm'>SchNEWS 777</a>).  </p>  
   <p>  
  	Western media tends to ignore most events in Africa&rsquo;s poorer nations, only turning their attention to them to moralise when the situation reaches breaking point. They usually point the finger at local corruption, socialist governments, civil war, and &ldquo;anarchy&rdquo; for turning droughts into famines, but in reality it is outside intervention, not internal mismanagement, that is largely responsible for fuelling the catastrophes in the region.  </p>  
   <p>  
  	In the 1980s, when Bob Geldof set out to single-handedly save Africa, Europe stockpiled food from bumper harvests; meanwhile aid organisations were forced to purchase grain on the open market, further subsidising European agriculture. The system is much the same today.&nbsp;  </p>  
   <p>  
  	Developing nations have been forced to adopt neoliberal policies, opening up their markets to western multinationals while banning subsidies for their own producers.  </p>  
   <p>  
  	A trite solution to the &lsquo;African problem&rsquo; would be to spend the money currently devoted to bombing the region on aid (UK has offered &pound;52 million to relieve famine while the bombing of Libya has cost at least &pound;200 million). Yet this view reinforces the idea that Africa is dependent on the west and ignores the root cause of the majority of the region&rsquo;s troubles. Western countries have spent decades propping up dictators, imposing free market &lsquo;reforms&rsquo;, bombing rebels, bribing officials and flogging military equipment. The aim of these policies wasn&rsquo;t to support rebuilding a continent ravaged by colonial powers and proxy wars, but merely to protect and further western &lsquo;interests&rsquo; whether they be preferential access to resources, securing lucrative defence and infrastructure contracts, promoting stability (predictability) or holding back development. If these rich countries simply stopped enforcing foreign policy goals on Africa, the continent may stand a chance of healing itself.  </p>  
   <p>  
  	The west is just as instrumental in the oppression of ordinary Africans as it was in the days of colonies and empires, just in a less overtly racist manner. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1311), 151); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1311);

				if (SHOWMESSAGES)	{

						addMessage(1311);
						echo getMessages(1311);
						echo showAddMessage(1311);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


