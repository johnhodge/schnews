<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 723 - 21st May 2010 - Genoa A Good Lawyer?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, genoa, g8, anti-capitalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 723 Articles: </b>
<p><a href="../archive/news7231.php">All That Romains...</a></p>

<p><a href="../archive/news7232.php">Thailand: Red With Blood</a></p>

<p><a href="../archive/news7233.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7234.php">Village Greens</a></p>

<p><a href="../archive/news7235.php">Lost In Transnational</a></p>

<p><a href="../archive/news7236.php">Nama-ed And Shamed</a></p>

<b>
<p><a href="../archive/news7237.php">Genoa A Good Lawyer?</a></p>
</b>

<p><a href="../archive/news7238.php">Brim And Proper</a></p>

<p><a href="../archive/news7239.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st May 2010 | Issue 723</b></p>

<p><a href="news723.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GENOA A GOOD LAWYER?</h3>

<p>
More senior Italian police have been sentenced for their part in the infamous violent Carabinieri police raids during the G8 in Genoa on the night of July 21st 2001 (see <a href="../archive/news314-5.htm" target="_blank">www.schnews.org.uk/archive/news314-5.htm</a>). Since then it's been a nine-year legal effort to bring justice for the many who were mercilessly beaten while sleeping, arrested, denied medical attention, laid with false charges, and more, during the raid of the two schools being used as protest bases and accommodation. 93 were arrested that night, with 28 of those being hospitalised, three of them critically injured &ndash; one of them, Mark Covell, from UK Indymedia, who was in a coma for two days. It was an attack which was pre-planned and then covered-up by the police and state. Tensions were already high before the raid, as the day before, the Carabinieri had murdered protester Carlo Giuliani. <br />  <br /> Two trials against the police held in 2008 were only conditional victories with mainly lower-rank officers getting convictions, but those in commanding roles walking free. Fifteen police were convicted &ndash; and 30 cleared - after the July 2008 'Bolzaneto' trial, which focused on police brutality of the arrestees in the cells (see <a href='../archive/news640.htm'>SchNEWS 640</a>). Camp commander Antonio Gugliotta got five years, while the others got up to 28 months. Now, after the appeal, 25 of the 27 defendants in the November 2008 'Diaz' trial &ndash; which focused on the raid itself - have been convicted, getting sentences up to five years. <br />  <br /> Some of the names who lost the appeal include Giovanni Luperi &ndash; who was since promoted to chief of the Italian equivalent of MI5, and two of the country's most senior detectives - Gilberto Calderozzi and Francesco Gratteri, who went onto a senior intelligence job after Genoa. They all got four years. The head of the riot squad unit that lead the raid got five years, and the two who planted the Molotov cocktails in the building were each sentenced to three years and nine months. The evidence against the commanding officers of the raid included video footage of them outside the building as it happened. &nbsp; <br />  <br /> Unfortunately these convicted police will not be imprisoned because their offences will expire under a statute of limitation, but they have also got five-year disqualifications from public office (as if a police officer who's been sentenced to prison but hasn't been made to go inside should remain in the job). <br />  <br /> * For indepth coverage of Genoa see <a href="../sotw/genoa-eyewitness.htm" target="_blank">www.schnews.org.uk/sotw/genoa-eyewitness.htm</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-capitalism&source=723">anti-capitalism</a>, <a href="../keywordSearch/?keyword=g8&source=723">g8</a>, <span style='color:#777777; font-size:10px'>genoa</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(789);

				if (SHOWMESSAGES)	{

						addMessage(789);
						echo getMessages(789);
						echo showAddMessage(789);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


