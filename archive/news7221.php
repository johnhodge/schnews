<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 722 - 14th May 2010 - Squaring Up To 'em</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, david cameron, direct action, tory, parliament square" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 722 Articles: </b>
<b>
<p><a href="../archive/news7221.php">Squaring Up To 'em</a></p>
</b>

<p><a href="../archive/news7222.php">Not A Clegg To Stand On</a></p>

<p><a href="../archive/news7223.php">Decommissioners Trial Delayed</a></p>

<p><a href="../archive/news7224.php">Glade Rags</a></p>

<p><a href="../archive/news7225.php">Sweaty Palms At Unilever</a></p>

<p><a href="../archive/news7226.php">Raising Arizona</a></p>

<p><a href="../archive/news7227.php">Bounty Bar-stards</a></p>

<p><a href="../archive/news7228.php">Lab- Lib Pact</a></p>

<p><a href="../archive/news7229.php">Crude Awakening</a></p>

<p><a href="../archive/news72210.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/722-jake-the-clegg-lg.jpg" target="_blank">
													<img src="../images/722-jake-the-clegg-sm.jpg" alt="Jake the Clegg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 14th May 2010 | Issue 722</b></p>

<p><a href="news722.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SQUARING UP TO 'EM</h3>

<p>
<p class="MsoNormal"><b>AS PROTESTERS PREPARE TO GIVE THE TORYS A WARM WELCOME...</b> <br />  <br /> Only four days into the brave new Libservative era and the population is getting into the national coalition-building mood - a rag-tag rainbow alliance of the disaffected is set to pitch up in Parliament Square this Saturday (15th). But will it be a strong, stable protest in the national interests? <br />  <br /> <b>Rave Against The Machine 2</b> - Parliament Square street party and sound clash, 4pm-11pm. &ldquo;A smashing good day out!&rdquo; Enjoy the carnival and spectacle of a country pissed off with its &lsquo;leaders&rsquo;! &ldquo;Just like the last party, invite your friends, and bring booze!&rdquo; <b>Fuck The Torys! Fuck The Lib Dems! And Fuck Labour! </b> <br />  <br /> <b>The Democracy Village</b> was established in Parliament Square on May 1st (see <a href='../archive/news721.htm'>SchNEWS 721</a>) demanding on behalf of the majority of people that the war in Afghanistan is ended and the troops are brought home with immediate effect. The village is still going on strong - all are welcome to get involved. (see <a href="http://democracyvillage.blogspot.com" target="_blank">http://democracyvillage.blogspot.com</a> for more details) <br /> <b> <br /> Guerilla Gardening - Brighten up Parliament Square</b> - which is in desperate need of colourful flora. Starting at 12pm - bring plants, flowers and picnic food. From 4pm there will be live music, an open mic, and a discussion forum. <a href="http://democracyvillage.blogspot.com" target="_blank">http://democracyvillage.blogspot.com</a> <br />  <br /> <b>Purple Patch</b> - At 2pm a purple &lsquo;coalition&rsquo; (mostly pissed-off Lib-Dem supporters) will assemble at Parliament Square for the Take Back Parliament protest. This campaign has marshalled its forces surprisingly quickly and now there&rsquo;s purple protests all around the country. <a href="http://www.takebackparliament.com" target="_blank">www.takebackparliament.com</a> <br />  <br /> <b>Nakba Protest</b> - Outside 10 Downing St will be a Nakba* commemoration Free Palestine Demonstration 12pm-5pm, called by Palestine Solidarity Campaign, British Muslim Initiative, Stop the War Coalition, CND, Palestinian Forum in Britain. <a href="http://www.palestinecampaign.org" target="_blank">www.palestinecampaign.org</a>  <br />  <br /> * Arab name for the forced mass exodus of the Palestinian people in 1948 <br />  <br /> <b>Party At The Pumps Part 2</b> &ndash; The Canadian wilderness is the threatened by Shell&rsquo;s massive tar-sands mining .. Three days before the corporation&rsquo;s AGM, come to a party on the forecourt and help shut down Shell Hell. Meet 1pm at Oxford Circus for Party At The Pumps Part 2 &ndash; with sound system and samba band. Bring a zone 1-2 tube ticket, make some noise. There will also be a special Party at the Pumps Critical Mass which will begin at Marble Arch at 1pm, moving to the location of the party. <a href="http://no-tar-sands.org" target="_blank">http://no-tar-sands.org</a> <br />  <br /> <b>Climate Emergency Overnight Vigil</b> - outside Parliament. The event will &lsquo;kick off&rsquo; with a candlelit procession down Whitehall, with people assembling at 11pm outside St Martin-in-the-Fields, Trafalgar Square. <a href="http://www.campaigncc.org" target="_blank">www.campaigncc.org</a>  <br /> One permanent fixture at Parliament Square is the Peace Camp, featuring long-term resident Brian Haw. June 2nd is the 9th anniversary of Brian&rsquo;s initial camp &ndash; he&rsquo;s still there, just as are troops in Afghanistan. <a href="http://www.parliament-square.org.uk" target="_blank">www.parliament-square.org.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=david+cameron&source=722">david cameron</a>, <a href="../keywordSearch/?keyword=direct+action&source=722">direct action</a>, <a href="../keywordSearch/?keyword=parliament+square&source=722">parliament square</a>, <a href="../keywordSearch/?keyword=tory&source=722">tory</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(773);

				if (SHOWMESSAGES)	{

						addMessage(773);
						echo getMessages(773);
						echo showAddMessage(773);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


