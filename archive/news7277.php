<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - Union Jack The Ripper</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edl, london, anti-fascists, islamophobia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<p><a href="../archive/news7275.php">Turkey Shoot</a></p>

<p><a href="../archive/news7276.php">The Itt Crowd</a></p>

<b>
<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>
</b>

<p><a href="../archive/news7278.php">Hoto Finish</a></p>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>UNION JACK THE RIPPER</h3>

<p>
<p>
	There were clashes between the EDL and locals in Whitechapel on Tuesday (15th) despite the cancellation of a controversial Islamic conference and EDL counter-demo. </p>
<p>
	The UK Islamic Conference (UK-IC) scheduled for this Sunday (20th) was to have been a bigoted affair, with many speakers thrown out of Mosques for their extremist views condoning rape, paedophilia, killing homosexuals, military camps and anti-Semitism. The EDL reacted with plans to stage a provocative counter-demo taking them through parts of London with a large Muslim population. </p>
<p>
	The East End youth were readying themselves for some heated action. The Whitechapel Anarchist group alone had plans to get large numbers from all over the country out on the day and form a black bloc around the East London mosque area of Whitechapel to remind racist thugs that they are historically not welcome there. However, The EDL pulled out when the venue, the Troxy, decided to cancel the event. </p>
<p>
	But on Tuesday (15th), around fifteen EDL on their way back from a counter-demo in Barking against a &lsquo;Muslim&rsquo;s Against the Crusades&rsquo; demo protesting against soldiers and the war were spotted in Whitechapel market. </p>
<p>
	Word got out and between 40 to 70 (and increasing as the day went on) Bangladeshi youth rushed their way between Whitechapel tube and Aldgate East to confront them, only to get mixed up in the confusion and find out the police had escorted the bunch of angry drunk EDL yobs back on to the tube safe and sound out of harms way. It was reported beforehand they were on the loose allegedly spitting at both a Muslim man and woman. </p>
<p>
	As news spread like wildfire, hundreds came out to protect the mosque. The atmosphere was tense, with some even staying out over night just to be on the safe side. White skin heads were all suspect, according to the local youth and the police (who, some undercover, were also stop and searching many young Asian lads). </p>
<p>
	Unfortunately, four white local punk anarchists (who are actively against the EDL) were caught up in the atmosphere of paranoia and were attacked by 30 angry young Asian men. </p>
<p>
	By no means were all the teenage locals out for a fight though. SchNEWS spoke to a few gathering groups and the sense of solidarity was encouraging. &ldquo;<em>This is about the whole community coming together. Not just Muslims</em>&rdquo; said one youth standing outside the mosque. They came to defend and protect and to send a clear message that Whitechapel is strong. </p>
<p>
	* See <a href="http://whitechapelanarchistgroup.wordpress.com" target="_blank">http://whitechapelanarchistgroup.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(823), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(823);

				if (SHOWMESSAGES)	{

						addMessage(823);
						echo getMessages(823);
						echo showAddMessage(823);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


