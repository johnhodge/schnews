<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 714 - 19th March 2010 - Tit Top</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, titnore, direct action, protest camp, environmentalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 714 Articles: </b>
<b>
<p><a href="../archive/news7141.php">Tit Top</a></p>
</b>

<p><a href="../archive/news7142.php">Bristols At Dawn</a></p>

<p><a href="../archive/news7143.php">Netan-yahoo Sucks</a></p>

<p><a href="../archive/news7144.php">Shrop Till They Drop</a></p>

<p><a href="../archive/news7145.php">Thai Dyed</a></p>

<p><a href="../archive/news7146.php">Sus-sexin' It Up</a></p>

<p><a href="../archive/news7147.php">Acquit While You're A-head</a></p>

<p><a href="../archive/news7148.php">Bloody Bankers</a></p>

<p><a href="../archive/news7149.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/714-titnore-lg.png" target="_blank">
													<img src="../images/714-titnore-sm.png" alt="Titnore Victory" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 19th March 2010 | Issue 714</b></p>

<p><a href="news714.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>TIT TOP</h3>

<p>
<p class="MsoNormal"><strong>VICTORY FOR THE TITNORE CAMPAIGN AS COUNCIL THROWS OUT <br />   DEVELOPMENT <br />  <br /> Well blimey. Against all the odds it looks like Titnore Woods has been saved from the developers&rsquo; evil clutches.</strong> <br />    <br /> The decision made by Worthing Council&rsquo;s planning committee to reject the development plans for the new housing estate on the ancient woodland site on Monday (15th) had everyone shouting with joy and dancing in the public gallery. The crowd were so beside themselves they even gave the bloody councillors a standing ovation. <br />  <br /> The committee, who conducted the open proceedings in Worthing&rsquo;s Assembly Hall, voted unanimously to reject the plans to build 875 houses. Around 150 members of the public turned up for the crucial meeting with banners, placards and drums. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 200" width="200px"><a href="../images/714-titnore4-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/714-titnore4-sm.jpg"  alt='Titnore Protesters'  /></a></td></tr></table>&ldquo;<em>It&rsquo;s been a long fight</em>,&rdquo; said one protester, &ldquo;<em>but it just shows you it&rsquo;s always worth taking the bastards on</em>.&rdquo; <br />  <br /> &ldquo;<em>We thought we didn&rsquo;t have a chance</em>,&rdquo; said a Worthing resident, &ldquo;<em>but I think the councillors finally realised how angry local people were and voted with their consciences at last.</em>&rdquo; <br />  <br /> A significant police presence was deployed at the event and the public gallery was flanked on all sides by coppers and security staff keen to keep order. Members of the public were searched upon entry and had &lsquo;dangerous&rsquo; items such as banners and placards confiscated. But no security force could stop the fury and irreverence that spewed forth from the public gallery. <br />   <br /> From start to finish the committee was taunted, booed, heckled, laughed at and continuously interrupted. The impassioned speeches and questions that came from the floor rammed home the absurdity of the proposal even to the jobsworth committee. It was enough to raise a smile from even the most cynical anarchist. <br />  <br /> Councillor John Livermore, who opened the proceedings, explained that there would be 15 minutes (you&rsquo;re too kind) of questions allowed from the public followed by a number of pre-registered speakers presenting their arguments for and against the proposal. This was met with a shout of &ldquo;<em>no one here&rsquo;s in bloody favour of it!</em> &rdquo;. His reply, &ldquo;<em>may we remind you that these proceedings are part of a democratic process</em> &rdquo;, provoked mass laughter from the room. <br />  <br /> The first to speak from the floor was local resident, Joy Hurcombe, who advocated abandoning the meeting entirely. She said; &ldquo;<em>In Worthing the housing quota has already been exceeded. There is a large supply of brownfield sites &ndash; at least 900 - that could be used instead of exploiting and destroying ancient woodland. Why are we then proceeding with this meeting at all?</em>&rdquo; <br />   <br /> Robin Rogers, the Lib Dem Councillor called Titnore Worthing&rsquo;s &ldquo;jewels in the crown&rdquo; in a town &ldquo;woefully short on green spaces&rdquo;. He urged the committee to &ldquo;<em>stop dancing to the developer&rsquo;s tune</em>,&rdquo; saying that, &ldquo;<em>if this development goes ahead we are going to lose this site for ever</em>&rdquo;. <br />  <br /> Inevitable issues were brought up, such as the hideous idea of adding more traffic to the bloody nightmare that is the A27, and the utter stupidity of building on a flood plain in area already at flood risk. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/714-titnore1-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/714-titnore1-sm.jpg"  alt='Titnore Protests'  /></a></td></tr></table>The threat to protected species such as dormice, bats, nesting birds and great crested newts who inhabit the woods was raised again and again. <br />  <br /> One speaker said, &ldquo;<em>Do you understand that in this area there are many protected species? Do you imagine they will still be there after six years of development? What&rsquo;s the point of having protected species if they are not protected?</em>&rdquo; <br />  <br /> To this James Appleton, (Worthing council&rsquo;s head of &lsquo;planning, regeneration and well-being&rsquo; &ndash; oh, the irony) responded: &ldquo;<em>There are a range of mitigation and compensation measures that have been put in place</em>.&rdquo; Go figure that one. How exactly they plan to &lsquo;compensate&rsquo; for lost species evades us... <br />  <br /> Another speaker said, &ldquo;<em>There is nobody in this room who wishes to speak up in favour of this. Is it not plain that local residents do not want this development? Why then, at every turn, do you not allow us to speak? Why, for instance, did you change the time of this meeting to 2pm when it was originally set for 7pm when more people could attend. Why are you so afraid of local opinion?</em>&rdquo; <br />   <br /> Another speaker said: &ldquo;<em>I used to play at Titnore Woods when I was a child. When you build on green spaces like these you create a lot of angry children with nothing but concrete to bounce off</em>.&rdquo; <br />  <br /> Appleton replied that one of the reasons to support the development was to &ldquo;<em>improve on the existing deficit of services in the area</em>&rdquo;. <br />  <br /> He outlined the developer&rsquo;s disgusting bribe to provide a new school, library, medical centre and sports complex including (ooh ahh) floodlit sports pitches, changing rooms and a skateboard park. With plastic trees as decoration presumably. <br />  <br /> Councillor &lsquo;Killjoy&rsquo; Livermore warned that the developers would almost certainly submit an appeal or seek a judicial review into the decision. He said, &ldquo;<em>The decision is only one step down the road. It will be back, that&rsquo;s for sure.</em>&rdquo; <br />  <br /> Meanwhile the protest camp remains in full operation, buoyed no doubt, by this happy turn of events. <br />  <br /> * See <a href="http://www.protectourwoodland.co.uk" target="_blank">www.protectourwoodland.co.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=714">direct action</a>, <a href="../keywordSearch/?keyword=environmentalism&source=714">environmentalism</a>, <a href="../keywordSearch/?keyword=protest+camp&source=714">protest camp</a>, <a href="../keywordSearch/?keyword=titnore&source=714">titnore</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(697);
			
				if (SHOWMESSAGES)	{
				
						addMessage(697);
						echo getMessages(697);
						echo showAddMessage(697);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


