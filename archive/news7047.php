<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 704 - 20th December 2009 - Ahava Go Heroes</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<div class="bannerGraphic">
			<a href="http://www.smashedo.org.uk/remember-gaza.htm"><img 
						src="../images_main/remember-gaza-banner.png"
						alt="Remember Gaza"
						width="465" 
						height="90" 
						border="0" 
			/></a>
			</div>


</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<a href='../archive/index.htm'>
			<div class='navBar_item'>
				Back Issues
			</div>
		</a>
		<a href='http://www.schnews.org.uk/schmovies/index.php'>
			<div class='navBar_item'>
				SchMOVIES
			</div>
		</a>
		<a href='http://www.schnews.org.uk/features'>
			<div class='navBar_item'>
				Feature Articles
			</div>
		</a>
		<a href='http://www.schnews.org.uk/links/index.php'>
			<div class='navBar_item'>
				Contacts and Links
			</div>
		</a>
		<a href='http://www.schnews.org.uk/diyguide/index.htm'>
			<div class='navBar_item'>
				DIY Guide
			</div>
		</a>
		<a href='http://www.schnews.org.uk/monopresist/index.htm'>
			<div class='navBar_item'>
				Archive
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/about.php'>
			<div class='navBar_item'>
				About Us
			</div>
		</a>
		<a href='http://www.schnews.org.uk/pages_menu/subscribe.php'>
			<div class='navBar_item'>
				Subscribe
			</div>
		</a>

			
		<!--	SCROOGLE SEARCh BAR 	-->
		<div class='search_bar'>		<form action="https://duckduckgo.com/" method="get"  target="_blank"> 
		
		 						
				<input type="search" placeholder="Search" name="q" maxlength="300" > 
				&nbsp;&nbsp;
				<input type="hidden" name="sites" value="schnews.org"> 
				    	<input type="hidden" value="1" name="kh">
    					<input type="hidden" value="1" name="kn">
    					<input type="hidden" value="1" name="kac">
						<input type="hidden" value="w" name="kw">
						
			<!--<input name="submit2" type="submit" value="Go">-->
			<!--<button type="submit">Search</button>-->
				
		</form>
		</div>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 704 Articles: </b>
<p><a href="../archive/news7041.php">Not A Hopenhagen</a></p>

<p><a href="../archive/news7042.php">Tar Very Much</a></p>

<p><a href="../archive/news7043.php">No Oil For War</a></p>

<p><a href="../archive/news7044.php">Nazi Pieces Of Work</a></p>

<p><a href="../archive/news7045.php">Back In Feline-ment</a></p>

<p><a href="../archive/news7046.php">The Livni Daylights</a></p>

<b>
<p><a href="../archive/news7047.php">Ahava Go Heroes</a></p>
</b>

<p><a href="../archive/news7048.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">
	<img src="../images_main/description-new.png" width="643" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" />
</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Sunday 20th December 2009 | Issue 704</b></p>

<p><a href="news704.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AHAVA GO HEROES</h3>

<p>
Two activists shut down the Covent Garden branch of Israeli cosmetics company Ahava on Saturday by locking on to a concrete block inside the shop. The activists were finally removed by a police cutting team at 4.30pm, by which time the shop crew had given up on the day and gone home. <br />  <br /> Ahava sells products made from Dead Sea mud and minerals. It has a production facility on the illegal settlement of Mitzpe Shalem and extracts the mud for its products from a site close to the settlement of Kibbutz Kalia in the occupied West Bank. <br />  <br /> * See <a href="http://www.bigcampaign.org" target="_blank">www.bigcampaign.org</a>  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=704">direct action</a>, <a href="../keywordSearch/?keyword=israel&source=704">israel</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(614);
			
				if (SHOWMESSAGES)	{
				
						addMessage(614);
						echo getMessages(614);
						echo showAddMessage(614);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<font face="Arial, Helvetica, sans-serif"> 
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p></font>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>
		
</div>








</div>




</body>
</html>


