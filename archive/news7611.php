<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 761 - 4th March 2011 - Penny For The Guy Ropes</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, camp for climate action, drax, kingsnorth, heathrow" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 761 Articles: </b>
<b>
<p><a href="../archive/news7611.php">Penny For The Guy Ropes</a></p>
</b>

<p><a href="../archive/news7612.php">Crap Eviction Of The Week</a></p>

<p><a href="../archive/news7613.php">That'll Learn 'em</a></p>

<p><a href="../archive/news7614.php">Saying No To Nato</a></p>

<p><a href="../archive/news7615.php">Bonus Culture Uncut</a></p>

<p><a href="../archive/news7616.php">Blatant Liberty</a></p>

<p><a href="../archive/news7617.php">Lopp-sided In Calais</a></p>

<p><a href="../archive/news7618.php">Panama Moments</a></p>

<p><a href="../archive/news7619.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/761-climatecamp-lg.jpg" target="_blank">
													<img src="../images/761-climatecamp-sm.jpg" alt="Climate Camp" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 4th March 2011 | Issue 761</b></p>

<p><a href="news761.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>PENNY FOR THE GUY ROPES</h3>

<p>
<p>  
  	<strong>AS &lsquo;CLIMATE CAMP&rsquo; ACTIVISTS DECIDE TO PACK UP AND GO HOME...</strong>  </p>  
   <p>  
  	They came, they camped, they conquered (well, not quite). During a soul-searching Dorset retreat, Climate Camp have decided to suspend tent-centred activism - citing the &ldquo;radically different political landscape&rdquo; of 2011.  </p>  
   <p>  
  	Having been through Drax, Kingsnorth, Heathrow, RBS, Copenhagen and one helluva lot of hummus, the group are now turning their attention to coordination with the wider anti-cuts and anti-austerity movement.  </p>  
   <p>  
  	In what seems to be a direct attack on the autonomous, grass-roots nature of the collective, a small group of the &lsquo;old guard&rsquo; came to the national planning meeting on 21st - 26th February with their own agenda and passed the decision to kill Climate Camp, despite a block and four stand-asides in the &lsquo;consensus&rsquo; process.  </p>  
   <p>  
  	The majority of attendees at the &lsquo;Space for Change&rsquo; meet went expecting to discuss plans for this year&rsquo;s Climate Camp and the future for the movement. When they arrived, they were faced with an agenda that was geared towards the dissolution of the collective and a lack of willingness from the facilitators to engage in alternatives to total shut-down.  </p>  
   <p>  
  	Some key people within the group, involved since the first camp at Drax in 2006, went as far as to criticise the &lsquo;horizontal&rsquo; nature of the organisation, and the way people were able to enter into the planning processes and &lsquo;disrupt&rsquo; things. Strong words for a movement supposedly based on anarchist principles of open engagement and participation.  </p>  
   <p>  
  	The &lsquo;retreat&rsquo; was limited to 80 people - half the normal number of individuals at national gatherings - with places allocated on a first-come, first-served basis. It soon became clear that the five days would be framed around the dissolution question, with a &lsquo;them and us&rsquo; atmosphere quickly developing. On one side were the few remaining original members who did most of facilitating, arguing for either a more NGO-type structure to Climate Camp or no Climate Camp at all; on the other, the more grass-roots minded individuals from strong regional groups who saw the way forward as an increased push for organisational skill-sharing and local autonomy.  </p>  
   <p>  
  	On the first day the proposal to dissolve the shared national identity of Climate Camp reached an impasse with 6 blocks to the decision. Members in favour of dissolution fought back with what has now been termed as the &lsquo;anti-block&rsquo; &ndash; the threat to leave the group if the decision didn&rsquo;t pass. Blockers and anti-blockers met to thrash out the differences, resulting in one remaining block to the proposal. Despite the normal conventions of consensus decision-making, and the statement published on the website following the meeting, the proposal was carried forward without consensus, ignoring the block.  </p>  
   <p>  
  	The &lsquo;Metamorphosis Statement&rsquo; published after this blatant disregard for agreed process reads like a bizarre mix of self-congratulation and random key words. Citing events like &ldquo;droughts in the Amazon, floods in Pakistan; food prices rising [and] revolutions across the Middle East&rdquo; which have created a world &ldquo;very different from 2005 when the Camp for Climate Action first met&rdquo; it emphasises a need to change. Err &ndash; Boxing Day tsunami, Hurricane Katrina, oil price rises due to trouble in the Middle East, occupation of Iraq? All around 2005. And aren&rsquo;t extreme weather conditions that cause floods and droughts essentially climate change issues? A weird justification for the dissolution of a climate action group. The statement was not agreed collectively, but written afterwards by one of the group who had proposed the disbanding of the network.  </p>  
   <p>  
  	<strong>NEGOTIATIONS IN TENTS</strong>  </p>  
   <p>  
  	SchNEWS spoke to one attendee of the hijacked gathering who explained why the core group of founder members wanted out. &ldquo;<em>They went into it with best intentions 5 years ago, but they failed to skill share and get enough people involved in the core organising &ndash; they don&rsquo;t trust anyone else to do it. It got to the stage where they were completely burned out. A recurring complaint they had was that they weren&rsquo;t appreciated for their skills - but their skills seem to be mostly that they&rsquo;ve got a degree from Oxford or Cambridge. Everything about this has been the wrong way round - we went to discuss ideas for the future but the whole thing seemed to be geared towards not talking about the future because they wanted to kill it. The national process hasn&rsquo;t been encouraging localism. I think this has been planned since the last national Camp for Climate Action in Edinburgh</em>.&rdquo;  </p>  
   <p>  
  	Climate Camp has been dogged with criticism for the last few years. After reasonably successful mobilisations at Drax in 2006 and Heathrow in 2007 (<a href='../archive/news558.htm'>SchNEWS 558</a>, <a href='../archive/news600.htm'>600</a>), the intensification of police pressure on the group at Kingsnorth in 2008 (<a href='../archive/news642.htm'>SchNEWS 642</a>) led to 2009&rsquo;s activities being split into two parts, workshops and networking at Blackheath, London in August (<a href='../archive/news689.htm'>SchNEWS 689</a>), and direct action targeting Ratcliffe-on-Soar power station in October (<a href='../archive/news696.htm'>SchNEWS 696</a>). This separation of the &lsquo;fluffy&rsquo; side to Climate Camp and the &lsquo;spikey&rsquo; radical action just fuelled the disharmony. Individuals started to question whether the movement was losing its focus. Media attention was escalating and the group was in danger of becoming a comfy bandwagon for middle (or upper)-class self-professed environmentalists in high profile journalistic or political positions to add a &lsquo;radical&rsquo; element to their public image, without actually having to risk getting nicked.  </p>  
   <p>  
  	Concerns over misrepresentation by mass media, fears of police violence and infiltration, discontent from participating members and a lack of clear direction culminated in a palpable feeling of paranoia and exhaustion in 2010&rsquo;s Edinburgh national camp (see <a href='../archive/news736.htm'>SchNEWS 736</a>). The camp&rsquo;s direct action methods seemed less well equipped to tackle the smoke and mirrors of the financial system The change of focus and resulting confusion of tactics could in hindsight be seen as an indicator of the struggles within the core of the organisation, regarding the priorities of the campaign.  </p>  
   <p>  
  	The decision to totally erase the national identity of Climate Camp will surely come as a kick in the teeth to those in regional groups doing good work in their areas. Waking up to find that the name that you organise under has been pulled out from under your feet by centralised decision-making is unprecedented in activist circles. This issue transcends national boundaries, with groups meeting under the Climate Camp banner in Australia, Belgium, Canada, France, New Zealand, the USA and Ghana. Inspired by events in the UK, each country took on the Climate Camp name, and none have been consulted in this decision.  </p>  
   <p>  
  	Ultimately, the act of a minority to shut-down an organisation which is meant to be run autonomously is entirely contradictory. It shows a lack of flexibility, a willingness to allow active participation of new individuals, and an inability to allow dynamic change within a group that has an established public presence. Where Climate Camp goes after this, and whether local groups can effectively use the collective identity which has done so much for environmental activism over the last 5 years remains to be seen.  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	<span style="font-family:courier new,courier,monospace;"><span style="font-size:16px;"><u><strong>Update</strong></u></span> <br />  
  	<em>7th March 2011 6pm</em></span>  </p>  
   <p>  
  	<span style="font-family:courier new,courier,monospace;">This article has evidently caused a healthy amount of debate around the issues involved in decision making in autonomous organisations. The people that SchNEWS spoke to (and it was more than one!) had very strong opinions on how the weekend had been orchestrated, and were very much of the opinion that the move to shut down climate camp&#39;s national action was premeditated and not allowed to be discussed in an objective manner throughout the five days. It&#39;s also worth noting that all articles written by SchNEWS are group edited and collaborated on, so this piece is the product of several people&#39;s reactions to the announcement, and to Climate Camp&#39;s &#39;Metamorphosis Statement&#39;. <br />  
  	 <br />  
  	There are a few other points which underpin SchNEWS&rsquo; take on this event aside from the experiences of people who attended and then spoke to us. Claiming that the world is a very different place from 2005 in the Metamorphosis Statement is highly debatable. Giving this as the statement-of-fact reason for the disbanding of Climate Camp&#39;s national actions leaves the entire decision open to question. The majority of issues we faced then faced us today. The only clear difference is that in 2005, climate chaos was the hot topic of the moment, filling newspaper columns and keeping activists busy all over the country. Now, for obvious reasons, the economic agenda is one that is hitting the headlines and is high on the activist hit list. <br />  
  	 <br />  
  	If capitalism and economic factors are at the root of climate change issues (as those within Climate Camp are very keen to point out) then it is difficult to see why Climate Camp decided to not harness the building resistance to what is being done with investment both in the public and private sector to fuel climate action at a national gathering this year. This was attempted at Edinburgh last year, and the opinion has been mooted that it failed because key members within the camp didn&rsquo;t allow other individuals to become as involved as they may have wanted to. Of course opinion is divided on this. SchNEWS has in the past reported from national gatherings and given these other opinions. <br />  
  	 <br />  
  	In addition, and directly contrary to what is said in the Metamorphaosis Statement, the decision was not passed with consensus. Of course the consensus process should not be so weak as to generally allow decisions to be repeatedly halted by one renegade participant, but at a gathering which is making decisions on behalf of a large number of people, and those people are not even aware of the decision being made, this should not be allowed. If there was this discord surrounding the decision, why weren&rsquo;t more individuals within the network notified of what was going on? Why wasn&rsquo;t there there a wider call out for people to step in and take up the organisational mantle that others wanted to put down? <br />  
  	 <br />  
  	SchNEWS has attended nearly all of the past Climate Camps and found them to be effective, empowering and one of the great achievements of UK activism. There is respect for Climate Camp as a movement and it goes without saying that no-one who was involved in this article wanted to &lsquo;stick the boot in&rsquo; to an organisation that is going through growing pains. Despite this, and however you interpret the rhetoric within the public statement made by Climate Camp after the event, there are a lot of things wrong about the way this was done. <br />  
  	 <br />  
  	We hold our hands up to the inaccuracies within the piece regarding things like what days the discussion were had on, we were relying on sources who had attended but who may have made mistakes in the specifics of timings. However, SchNEWS stands by its interpretation. The article was written in good faith based on our conversations with people at Space for Change, and our angle was taken from the strong evidence that the consensus decision-making procedures were manipulated at the gathering to suit a premeditated agenda. It is agreed that this is one side of the argument. To redress any imbalance, and in the interest of open and honest journalism (which is what SchNEWS and Indymedia have always stood for) SchNEWS would like to now open it up to those who have commented on this article with a different version of events &ndash; if you would like to write us an account of the five days as you saw them, we will publish it on our website as a feature. Email: <a href="mailto:mail@schnews.org.uk</span>">mail@schnews.org.uk</span></a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1125), 130); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1125);

				if (SHOWMESSAGES)	{

						addMessage(1125);
						echo getMessages(1125);
						echo showAddMessage(1125);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


