<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 751 - 10th December 2010 - Anti-Tax Dodgers High Street Bonanza</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, austerity, direct action, financial crisis, cuts, vodafone" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 751 Articles: </b>
<p><a href="../archive/news7511.php">For Whom The Fee Tolls</a></p>

<b>
<p><a href="../archive/news7512.php">Anti-tax Dodgers High Street Bonanza</a></p>
</b>

<p><a href="../archive/news7513.php">A Hellas Of A Time</a></p>

<p><a href="../archive/news7514.php">Wikileaks: Hacktion Stations</a></p>

<p><a href="../archive/news7515.php">Schews In Brief</a></p>

<p><a href="../archive/news7516.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th December 2010 | Issue 751</b></p>

<p><a href="news751.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ANTI-TAX DODGERS HIGH STREET BONANZA</h3>

<p>
<p>
	The protest movement against the loophole-loving corporate sector that began in October with a small group of protesters venting spleen at Vodafone for massive tax dodging has snowballed into a nationwide campaign (see <a href='../archive/news746.htm'>SchNEWS 746</a>). </p>
<p>
	The outrage has been so widespread, members of the normally sheepish general public have been indisnguisable from die-hard black bloc with lairy actions taking place all over the country. <br /> 
	&nbsp;This expos&eacute; of the unjust underbelly of Britain&rsquo;s corporatocracy has captured the public&rsquo;s discontent &ndash; and why not, as crippling cuts targeting the poorest are made while the capitalist enterprises raking in billions are left unscathed. </p>
<p>
	Targets have grown from Vodafone to include Boots, Miss Selfridge, Wallis, Dorothy Perkins, Evans, Burtons, Lloyds TSB, Barclays, HSBC and Topshop, with Grolsch and Cadbury being the latest added to the list. Unsurprisingly, even politicians have being cashing in on legal loopholes with Channel 4 claiming that Chancellor George Osborne, Transport Secretary Philip Hammond and International Development Secretary Andrew Mitchell have also got out of paying millions. </p>
<p>
	Last Saturday, Topshop&rsquo;s flagship London shop was closed by hundreds of protesters emerging out of the consumer throng to beat saucepans, wave placards and shout slogans, before blockading the entrance for the whole day. Security guards and police manhandled demonstrators and journalists out of the way, but support from the public was unfaltering with one kindly gent even turning up with loads of pizza for the shivering blockaders. </p>
<p>
	Similar actions targeting Topshop were seen up and down the country, including Liverpool, Leicester, Leeds, Glasgow, Edinburgh, Brighton, York, Loughborough, Southampton, Sheffield, Reading, Portsmouth, Oxford, Nottingham, Newcastle, Manchester and Birmingham. In Brighton, 18 protesters were arrested after eight superglued themselves to the inside of the shop window, and others were kettled for hours in the freezing temperatures. They were threatened with arrest if they refused to give their names and addresses when finally allowed to leave. </p>
<p>
	More anti-cuts action on Saturday 4th saw Vodafone targeted in many cities with blockades and pickets, shutting down around 20 stores for the day. Boots were another focus for protests in at least nine locations from York to Brighton while fashion-floggers Miss Selfridge, Wallis, Dorothy Perkins, Evans and Burtons were also hit by sit-in protests. HSBC, Lloyds TSB and Barclays got their fair share of rage in the capital as well as at local branches nationwide. </p>
<p>
	SAB Miller, the owners of Grolsch, are now gaining attention after it was revealed they have paid no corporation tax at all in the last two years, while Kraft, new owners of Cadbury, have recently announced plans to move much of their business dealings to Switzerland, getting out of paying millions in UK taxes. At this rate the public might soon realise there&rsquo;s no end of corporate targets deserving of good kicking. </p>
<p>
	* More protests are planned for the coming days and weeks, check out <a href="http://www.ukuncut.org.uk/actions" target="_blank">http://www.ukuncut.org.uk/actions</a> for a list of demos near you or, better still, organise one of yer own and post it up there... <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1035), 120); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1035);

				if (SHOWMESSAGES)	{

						addMessage(1035);
						echo getMessages(1035);
						echo showAddMessage(1035);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


