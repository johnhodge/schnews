<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 696 - 23rd October 2009 - And Finally</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, financial crisis, anti-capitalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 696 Articles: </b>
<p><a href="../archive/news6961.php">Sooty & Swoop</a></p>

<p><a href="../archive/news6962.php">Edo Aquittals  </a></p>

<p><a href="../archive/news6963.php">It&#8217;s Brim Up North</a></p>

<p><a href="../archive/news6964.php">Cymru Have A Go If You Think Yer Hard Enuff</a></p>

<p><a href="../archive/news6965.php">Bright Sparks</a></p>

<p><a href="../archive/news6966.php">Schnews In Brief</a></p>

<b>
<p><a href="../archive/news6967.php">And Finally</a></p>
</b>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd October 2009 | Issue 696</b></p>

<p><a href="news696.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AND FINALLY</h3>

<p>
Great SchNEWS fodder surely, finding an article called, &ldquo;<strong>Death of &lsquo;Soul of Capitalism&rsquo;: 20 reasons America has lost its soul and collapse is inevitable</strong>.&rdquo; Should be a good rant with a title like that. And it is. <br />   <br /> The author notes that radical economist Marc Faber has brought out a damning analysis of the current capitalist state of play called &lsquo;Doom, Boom &amp; Gloom&rsquo; and proceeds to enthusiastically summarise all his main points and resoundingly agree with them. <br />    <br /> No pussyfooting here: &ldquo;<em>No, not just another meltdown, another bear market recession like the one recently triggered by Wall Street&rsquo;s &ldquo;too-greedy-to-fail&rdquo; banks. Faber is warning that the entire system of capitalism will collapse. Get it? The engine driving the great &lsquo;American Economic Empire&rsquo; for 233 years will collapse, a total disaster, a destiny we created ... OK, deny it. But I&rsquo;ll bet you have a nagging feeling maybe he&rsquo;s right, the end may be near... I have for a long time...</em>&rdquo; <br />  <br /> Headings include: &lsquo;<strong>Collapse is now inevitble</strong>&rsquo; and &lsquo;<strong>When greed was legalized</strong>&rsquo;, and &lsquo;<strong>Wall St sacked Washinton</strong>&rsquo;, cutting through the crap in fine style, for example:   <br /> &ldquo;...&rsquo;Wall Street America&rsquo; went over to the dark side, got mega-greedy and took control of &lsquo;Washington America&rsquo;. Their spoils of war included bailouts, bankruptcies, stimulus, nationalizations and $23.7 trillion new debt off-loaded to the Treasury, Fed and American people. <br />   <br /> &ldquo;<em>Who&rsquo;s in power? Irrelevant. The &lsquo;happy conspiracy&rsquo; controls both parties, writes the laws to suit its needs, with absolute control of America&rsquo;s fiscal and monetary policies. Sorry Jack, but the &ldquo;Battle for the Soul of Capitalism&rdquo; really was lost</em>.&rdquo; <br />  <br /> The other 15 &lsquo;reasons&rsquo; pull no punches either, a list of lucid facts and figures pointing out things like America&rsquo;s top 1% own more than 90% of the wealth, oil and energy costs will skyrocket,  foreign nations have started dumping the dollar as reserve currency and how the  new financial reforms will do nothing to prevent the next meltdown. <br />   <br /> The big finale is not only to endorse Faber&rsquo;s view but push it further: &ldquo;<em>Faber is uncertain about timing, we are not. There is a high probability of a crisis and collapse by 2012. The &lsquo;Great Depression 2&rsquo; is dead ahead. Unfortunately, there&rsquo;s absolutely nothing you can do to hide from this unfolding reality</em>...&rdquo; <br />  <br /> Wow, 2012! Even us conspiracy nuts and Mayan sympathisers in the office hesitate to stick out our necks so far as to definitely state that the London Olympics will actually be the start of armageddon, as attractive a proposition as that is. <br />   <br /> So which loony lot would be publishing such incendiary scaremongering doomism? Er, how about <a href="http://www.MarketWatch.com," target="_blank">www.MarketWatch.com,</a> a &lsquo;respectable&rsquo; news wire reporting the ups and downs of stocks and shares and packed full of investor related anaysis and trading tips. Owned in fact by Dow Jones, the American stock market index people,   <br /> Stranger and stranger. And who owns Dow Jones? Er, Rupert Murdoch&rsquo;s News Corporation &ndash; Fox news, the Sun, half the world&rsquo;s crappest right wing media etc...Damn, now we don&rsquo;t know now whether to write off any apocalypse-angled anaylsis as part of a double bluff elite conspiracy or head for hills and brush up on extreme survival skills...   <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-capitalism&source=696">anti-capitalism</a>, <a href="../keywordSearch/?keyword=financial+crisis&source=696">financial crisis</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(541);
			
				if (SHOWMESSAGES)	{
				
						addMessage(541);
						echo getMessages(541);
						echo showAddMessage(541);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>