<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - Gateway Gate: Straight From the Pig's Mouth</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, police, netcu, indymedia, internet" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>

<b>
<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>
</b>

<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>

<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<p><a href="../archive/news75511.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GATEWAY GATE: STRAIGHT FROM THE PIG'S MOUTH</h3>

<p>
<p>
	SchNEWS hopes to be in a position to publish the full backlog of comments (SchLeaks) soon. Meanwhile here&rsquo;s a small sample... </p>
<p>
	<strong>INCITEMENT:</strong> </p>
<p>
	Obviously fed up with the lack of militant law-breaking in the U.K the Gateway users try and whip up a storm of their own. </p>
<p>
	On the Milli-band protests - organised by Greenpeace against the Kingsnorth Power Station in July 4th 2009: </p>
<p>
	<strong>4th July - Shut Kingsnorth:</strong> <span style="font-family:courier new,courier,monospace;">With the police not opposing the Milliband what better time to shut Kingsnorth? Lets use this opportunity to shut it down once and for all!</span> posted by Milli-Band on 26.06.2009 07:29 </p>
<p>
	<strong>No - stuff that - SHUT the place:</strong> <span style="font-family:courier new,courier,monospace;">Let&rsquo;s not all stand around like lemmings - lets shut the place!Bring ladders and wire cutters. If there are enough of us we can shut it!</span> posted by Millitant Band on 26.06.2009 08:48 </p>
<p>
	<strong>ANIMAL RIGHTS TARGETS:</strong> </p>
<p>
	NETCU came out of AR monitoring group ARNI (Animal Rights National Index). Here the gateway boys try and get AR back on the streets and even have a positive take on arson! These three comments were posted on demo reports: </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Yeah - back to Sequani Let&rsquo;s get out this time - only 2 turned up at Highgate! We need loads at Sequani or they will think we have given up!</span> posted by Anon on 29.10.2010 </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Try HLS or Oxford: think they are still open! Why waste effort &lsquo;inspecting&rsquo; places that you know have closed - unless it is to make yourself sound important - it&rsquo;s not working.</span> posted by What&rsquo;s the point of these? on 26.02.2010 08:07 </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Yeah - Harrods and Josephes too..: Both of those still selling fur - both will be demo ed over Xmas. It&rsquo;s not over yet!</span> posted by CAFT on 21.12.2009 14:05 </p>
<p>
	On the burning down of Novartis CEO&rsquo;s Hunting Lodge and theft of his mother&rsquo;s ashes: </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Horrible own goal: Yes there will be a witch hunt - lest we forget the SNGP are mostly all still in jail and that incident lead to Opertion Achillies and the 50 year jail terms. Had they stopped at the arson they would have made a good point but the repeated exhumation just makes the whole AR movent look like unreasonable fanatics. Stupid Stupid Stupid.</span> posted by Green man 2009-08-05 11.00 </p>
<p>
	<strong>EARLY WARNING:</strong> </p>
<p>
	On two occasions users of Gateway 303 posted warnings/early reports of police raids on addresses. Why is a bit of mystery - maybe to see who contacts who in the afermath. </p>
<p>
	<strong>Antifa solidarity:</strong><span style="font-family:courier new,courier,monospace;"> Reports have been recieved of raids across the country. Details are unclear at present.</span> posted by Antifa on 29.07.2009 10:54 </p>
<p>
	<strong>AR raids - duck and cover:</strong> <span style="font-family:courier new,courier,monospace;">Early reports of raids on two addresses - AR related. Early morning raids on AR activists - some arrests - possible house searches. More details of nicks to follow - we must show solidarity.</span> posted by @tivist on 10.11.2009 08:20 </p>
<p>
	<strong>DEMORALISATION:</strong> </p>
<p>
	It has been speculated that one of the roles of the undercovers was to break groups up through demoralisation and cynicism. Ceratinly the 303 crew seem to be singing from the same sheet. </p>
<p>
	In relation to a protest at Didcot Power Station. </p>
<p>
	<strong>13 arrested - production not halted:</strong> <span style="font-family:courier new,courier,monospace;">13 arrested - production not halted, the rest up a stack waiting to come down and get nicked -what did this achive?</span> posted by Hang on on 27.10.2009 08:28 </p>
<p>
	In relation to graffiti at EDO MBM, a Brighton weapons factory: </p>
<p>
	<strong>That should stop production....</strong>.:<span style="font-family:courier new,courier,monospace;">Yep - no doubt paint on the walls stops production dead. Oh hang on......</span> posted by Paint on 05.02.2010 12:52 </p>
<p>
	In relation to a protest outside an animal lab: </p>
<p>
	<strong>The numbers are down from last year:</strong> <span style="font-family:courier new,courier,monospace;">There is nothing wrong with 70 - I think the initial alarm on the day and on here is that numbers were so reduced from last year and what the Police had been expecting</span>. posted by It&rsquo;s Fine on 02.11.2009 11:24 </p>
<p>
	Talking about fox-hunts on Boxing Day: </p>
<p>
	<strong>So many More now</strong>: <span style="font-family:courier new,courier,monospace;">Than there were before the ban makes you think eh?</span> posted by Pink on 23.12.2009 15:43 </p>
<p>
	<strong>SOWING DIVISION:</strong> </p>
<p>
	Another tactic is to cause divisions through the spreading of rumours against individuals even suggesting that they might be police informants. </p>
<p>
	Referring to long-term AR activist Lynn Sawyer: </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Another raid on Lyns house: And she hasn&rsquo;t been nicked?</span> posted by Again? on 10.11.2009 11:45 </p>
<p>
	Referring to AR activist Luke Steele: </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Just goes to show: Why is Luke Steele taken seriously I will never know. Steele attracts the law like a magnet and from what a &ldquo;screw&rdquo; said in the local pub he like to run and tell when he got slapped around. ...... Nothing new, he will always be a spoilt little mummy&rsquo;s boy. Be aware of Steele, he can&rsquo;t be trusted.</span> posted by Sab on 24.12.2009 10:28 </p>
<p>
	One comment aimed to discourage activism against the EDL - Undercover cop Mark Kennedy suggested that activists avoid confrontation with the EDL in a meeting at the EF! Gathering in 2010 </p>
<p>
	<strong>They were formed to fight: </strong><span style="font-family:courier new,courier,monospace;">Guys, the EDL are predominatly a group of football hooligans who were bored in the summer. This seems to be giving them exactly what they want - sombody to have a fight with! Why not ignore them?</span> posted by Observer on 08.12.2009 13:29 </p>
<p>
	<strong>BIZARRE:</strong> </p>
<p>
	These and many of the others seem like nothing more than bored trolling: </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Thanks for the heads up... :we will look into it.</span> posted by NETCU 22.02.2010 08:17 </p>
<p>
	<span style="font-family:courier new,courier,monospace;">Let&rsquo;s ask David Ike...: I&rsquo;m sure that Dave can save us!</span> posted by Purple Track Suit on 23.12.2009 08:54 <br /> 
	 <br /> 
	And this is a just a small selection of the over 100 comments intercepted on Indymedia. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1067), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1067);

				if (SHOWMESSAGES)	{

						addMessage(1067);
						echo getMessages(1067);
						echo showAddMessage(1067);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


