<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 723 - 21st May 2010 - All That Romains...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, romania, riots, financial crisis, eu" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 723 Articles: </b>
<b>
<p><a href="../archive/news7231.php">All That Romains...</a></p>
</b>

<p><a href="../archive/news7232.php">Thailand: Red With Blood</a></p>

<p><a href="../archive/news7233.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7234.php">Village Greens</a></p>

<p><a href="../archive/news7235.php">Lost In Transnational</a></p>

<p><a href="../archive/news7236.php">Nama-ed And Shamed</a></p>

<p><a href="../archive/news7237.php">Genoa A Good Lawyer?</a></p>

<p><a href="../archive/news7238.php">Brim And Proper</a></p>

<p><a href="../archive/news7239.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st May 2010 | Issue 723</b></p>

<p><a href="news723.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ALL THAT ROMAINS...</h3>

<p>
<strong>AS THE EURO ECONOMIC MELTDOWN HITS ROMANIA.....</strong> <br />  <br /> Welcome to the age of austerity. Romania has erupted in protests as harsh measures continue to grow and waves of discontent flood Europe. <br />  <br /> Romania&rsquo;s capital, Bucharest, stood still for a day on Wednesday (19th) as ferocious cuts cripple the Romanian populace. The city was  brought to a halt as an estimated 50,000 protesters marched through the streets in the largest of the demonstrations seen since the cuts were announced. Protesters poured in from all over the country, despite police attempts to block the streets to uphold order. <br />   <br /> The government&rsquo;s latest bright idea is proposed wage cuts of 25% and pension cuts of 15% to reduce the country&rsquo;s budget deficit along with the stated intention of slashing 70,000 state employees out of a public sector workforce of 1.36 million (a third of the workforce) before the end of the year. Romania&rsquo;s economy shrunk more than 7% last year and is going for an IMF bail-out to meet its wage bill. The government says it needs to implement new austerity measures to qualify for the next instalment of the 20m euro (&pound;17bn) IMF loan. No monetary aid packs are likely until the chiefs can demonstrate an ability to suppress dissent. <br />  <br /> Last Tuesday (11th) thousands of farmers, furious over overdue agricultural subsidies, blockaded the area, surrounding the main government building in Bucharest as they arrived in tractors and parked up outside the cabinet command post. As Wednesday rolled on, 500 irate pensioners had a crack at the presidential palace, with other demonstrations taking place around the country. The protest followed four days of protests by unions. <br />  <br /> The economy ministry official Marcel Hoara had stones hurled at him and was doused with water as he was jeered during a live televised debate in the middle of the protest. Police had to escort him from the area to safety after he was ambushed by demonstrators on his way out. &ldquo;<em>We will not leave until the government quits</em>,&rdquo; said Bogdan Hossu, leader of the Cartel Alfa trade union. Marian Gruia, head of the policemen&rsquo;s union, called on Romanians to unite, &ldquo;<em>as we did in 1989, when we overthrew the dictatorship</em>&rdquo;. Unions have issued a warning of a general strike on the 31st May if the Government continues to ignore their cries of insurgency. <br />  <br /> The demonstration was one of the largest to be seen in the country since the days of the Romanian Revolution of 1989, when the communist government was overthrown after a week long series of escalating violent riots and street fighting. The conflict ended with the execution of President Nicolae Ceausescu and his wife. Romania was the only eastern block country to overthrow its government forcefully or to execute its leaders. Since then the leadership has been more in the style of an austere free market capitalist product. <br />   <br /> Heavy foreign investment in the course of the last decade led to embellished economic growth which surprise, surprise has totally imploded as a result of the international economic crisis. <br />   <br /> The Romanians&rsquo; competence in resisting the measures will judge whether they will be the ones having to pay for the economic collapse. So it started with Greece, now we have Romania, so the question is &ldquo;Who&rsquo;s next in the Euro zone?&rdquo; Any takers? <br />   <br /> * See also <a href="http://www.libcom.org/news" target="_blank">www.libcom.org/news</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=eu&source=723">eu</a>, <a href="../keywordSearch/?keyword=financial+crisis&source=723">financial crisis</a>, <a href="../keywordSearch/?keyword=riots&source=723">riots</a>, <span style='color:#777777; font-size:10px'>romania</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(783);

				if (SHOWMESSAGES)	{

						addMessage(783);
						echo getMessages(783);
						echo showAddMessage(783);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


