<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 659 - 12th December 2008 - Having No Truck</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, afghanistan, barack obama, pakistan, karzai, humvee, taliban, iraq, war on terror, hellfire, terminator" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 659 Articles: </b>
<p><a href="../archive/news6591.php">Go Greece Fightin'</a></p>

<p><a href="../archive/news6592.php">Crap Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news6593.php">Having No Truck</a></p>
</b>

<p><a href="../archive/news6594.php">Constant Threat</a></p>

<p><a href="../archive/news6595.php">Anti-military Tattoo</a></p>

<p><a href="../archive/news6596.php">Stanstill</a></p>

<p><a href="../archive/news6597.php">Xmas Jeers</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 12th December 2008 | Issue 659</b></p>

<p><a href="news659.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>HAVING NO TRUCK</H3>

<p>
As far as anti-arms actions goes, it&#8217;s hard to beat the news from Pakistan from the other day. Three actions over four consecutive days have destroyed some 200 US Humvees and armoured personnel carriers, as well as other weapons heading for US troops engaged in the occupation of Afghanistan (sorry, we mean, at the invitation of &#8216;Mayor of Kabul&#8217; President Karzai). In one of the major blowbacks from the war in Afghanistan, groups fighting under the banner of &#8216;The Taliban&#8217; have virtually taken over the northwestern border of Pakistan. On the Afghan side of the same border, the original Taliban that were so famously routed from Mazar Al Sharif in 2001 by the US and the Northern Alliance, are back in force, controlling most of the South and East of the country.  <br />
 <br />
How things have changed in the last eight years. Back then most Afghans were glad to see the back of the Taliban, asking themselves &#8220;what could possibly be worse than that humourless shower of bastards?&#8221; Well, eight years later they&#8217;ve got their answer: The Americans. <br />
 <br />
Meanwhile some elements of the Taliban have softened their image a little- allowing some music and wedding parties here and there. And they&#8217;ve also been paying close attention to what their &#8216;Islamic brothers&#8217; have been getting up to in Iraq over these last years, and now the hills of Afghanistan are alive with the sound of car-bombs. The Taliban have taken over from the Iraqi insurgency as the biggest killer of US soldiers (after obesity that is). <br />
 <br />
What&#8217;s going on in Afghanistan hinges on its neighbour Pakistan much more than anything that US or British squaddies get up to in the country. Pakistan, having nurtured both the Taliban and Al Qaeda for years, got bullied into supporting the US war since 2001. They&#8217;ve been playing a double game ever since. Ironically both the Americans and the Taliban rely on Pakistan for their supplies. Whilst the US base in Tarbella is a launching pad for aerial assaults, and US weapons reach Afghan bases through Pakistan, Pashtun men regularly cross the porous North-Western Frontier into Afghanistan to fight the Americans. <br />
 <br />
The &#8216;War on Terror,&#8217; as viewed from the Northwestern Frontier Province, is a very weird place indeed. For a region that is probably the least developed in the whole of Asia, a war is being fought that could be the template for a slew of future wars which will look like scenes straight out of The Terminator. Remote control warriors sit in pre-fab buildings in Arizona controlling unmanned Predator Drones armed with Hellfire missiles that use smart targeting to hunt down terrorists from high in the skies. Needless to say the vast majority of the casualties of the Predators are civilian, as almost every week there are stories of yet another wedding party being hit by US remote missiles. <br />
 <br />
Somehow even though it&#8217;s politically unacceptable to allow US troops to fight in Pakistan, Playstation warriors who drop the kids off at school, commute to work and spend the day targeting towel-heads before coming home to dinner with the family is somehow alright. As villages that don&#8217;t have electricity or running water are ripped apart by the most sophisticated killing machines on the planet, the rage that these atrocities cause serve as the best possible recruitment campaign for the Taliban. Meanwhile, on Planet Washington president elect Obama promises to withdraw troops from Iraq to push them on to the new front line in Afghanistan, leaving the US sucked into an unwinnable war just like the one that the Soviets fought in the '80s.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=barack+obama&source=659">barack obama</a>, <a href="../keywordSearch/?keyword=iraq&source=659">iraq</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(165);
			
				if (SHOWMESSAGES)	{
				
						addMessage(165);
						echo getMessages(165);
						echo showAddMessage(165);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>