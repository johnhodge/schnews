<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 656 - 21st November 2008 - Minga The Merciless</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, colombia, farc, bogota, alvaro uribe, peasants movements, latin america, paramilitaries" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://edinantimil.livejournal.com/"><img 
						src="../images_main/anti-war-gathering-banner.png" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 656 Articles: </b>
<b>
<p><a href="../archive/news6561.php">Minga The Merciless</a></p>
</b>

<p><a href="../archive/news6562.php">Witness Appeal Of The Week</a></p>

<p><a href="../archive/news6563.php">So Solidarity Crew</a></p>

<p><a href="../archive/news6564.php">Fruity Number</a></p>

<p><a href="../archive/news6565.php">Snap Crackle Cop</a></p>

<p><a href="../archive/news6566.php">Rail Against The Machine</a></p>

<p><a href="../archive/news6567.php">Rnc U L8r</a></p>

<p><a href="../archive/news6568.php">3 Reichs And Yer Out</a></p>

<p><a href="../archive/news6569.php">T'rubble At Mill (rd)</a></p>

<p><a href="../archive/news65610.php">Off With Their Arms</a></p>

<p><a href="../archive/news65611.php">Identity Politics</a></p>

<p><a href="../archive/news65612.php">And Finally 656</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/http://www.schnews.org.uk/images/656-uribe-lg.jpg" target="_blank">
													<img src="../images/http://www.schnews.org.uk/images/656-uribe-sm.jpg" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 21st November 2008 | Issue 656</b></p>

<p><a href="news656.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>MINGA THE MERCILESS</H3>

<p>
<b>AS MASS PROTEST MOVEMENT SWEEPS ACROSS COLOMBIA</b> <br />
 <br />
The sight of 15,000 angry indigenous matching down your main motorway is enough to put the wind up any President, even one habitually referred to in the Western media as a &#8216;Latin American strongman&#8217;. And yet a couple of weeks ago President Alvaro Uribe of Colombia somehow found himself standing on a bridge in Cali with a megaphone shouting to make himself heard over thousands of these angry protesters calling him a &#8216;paramilitary son of a whore&#8217;. Addressing only the fuming remnants of the protesters after the leaders and most of the marchers had departed, he lamented their lack of desire for genuine dialogue and left in a huff. But this was after the president had failed to show at the appointed time and he also missed several subsequent deadlines. <br />
 <br />
A few days later another meeting was arranged, this time in the relative calm of the indigenous &#8216;reserve&#8217; of La Maria in the department of Cauca, where the protest began. For six hours leaders of the mobilisation discussed their agenda with the president. Following the meeting Aida Quilcu�, the Chief Counsel of the Indigenous Regional Council of Cauca (CRIC), announced to the waiting crowd that Uribe had failed to provide a &#8216;clear, concrete, political response&#8217; to their concerns and asked them if the protest should continue. The response was a resounding &#8216;S�&#8217; and it was decided that they should march on to the capital, Bogot�.  <br />
 <br />
The protest started as an indigenous mobilisation to demand that the government uphold previous land-rights accords and put an end to the militarisation of their territories and accompanying abuses. The movement has become known as Minga de los Pueblos (Minga of the People). Of special concern was the recent spate of unpunished assassinations in indigenous communities, 29 last month alone and 1240 so far in Uribe&#8217;s six-year presidency. <br />
 <br />
However it quickly broadened its base to include other social movements; sugar cane cutters, unions, displaced peoples, victims of state and paramilitary violence, students and anyone else who&#8217;s not a fan of El Presidente. They then devised an inclusive agenda in an attempt to articulate a formal and organised opposition to government policies. Aside from its original demands, the main points of the agenda demanded that the government address concerns regarding its respect, or lack of, for human rights, its policy of &#8216;Democratic Security&#8217; and the movement&#8217;s opposition to Free Trade agreements.  <br />
 <br />
<b>FARC'D OFF</b> <br />
 <br />
&#8216;Democratic Security&#8217; is the electoral name of the policies that furnished Uribe with the Colombian label of &#8216;war president&#8217;. On coming to power in 2002, Uribe reclassified Colombia&#8217;s forty-year conflict against a number of Marxist guerilla groups, the most prominent being the FARC (Revolutionary Armed Forces of Colombia), as a struggle against &#8216;narco-terrorism&#8217;, fitting in nicely with Washington rhetoric and securing huge stacks of gringo cash in the form of military aid under Plan Colombia (<a href="../archive/news327.htm">SchNEWS 327</a>).   <br />
 <br />
The FARC have been around in their current form since the sixties, with origins in the century old conflict between Colombia&#8217;s dominant political parties, the Conservatives and the Liberals. In recent years their policies of kidnapping, extortion and their lack of concern for civilian causalities have seen support for their insurgency steadily erode, even amongst their rural peasant base. <br />
 <br />
Uribe&#8217;s first act as president was to abandon the peace initiative of previous president Andr�s Pastrana, which had seen the FARC seize control of a third of the country by taking advantage of a demilitarised zone. An increase in military operations drove the FARC back into the deep jungle in many areas of the country. The FARC also suffered a series of high profile blows, the assassination of number two commander Raul Reyes in Ecuadorian territory, followed by the death of their leader Manuel Marulanda and the rescue of Ingrid Betancourt, former presidential candidate and their most high profile hostage. With kidnappings down, travel much safer and a drop in homicide rates in the major cities, &#8216;Democratic Security&#8217; has served Uribe well and earned him high approval ratings amongst the general population.  <br />
 <br />
On the other side of the political spectrum Uribe has made much of the supposed demobilisation of the AUC (United Self-Defence Forces of Colombia), the umbrella group for various right-wing paramilitary organisations. These groups were mostly started by drug traffickers and ranchers to protect themselves and their interests from the guerrillas and are heavily involved in the drug trade. Although it has provided him with plaudits both at home and abroad the &#8216;Peace and Justice&#8217; process has remained highly controversial. The two main issues being the lack of peace and the absence of justice. The high profile leaders of the AUC have faced feeble punishments for their part in massacres, torture and forced displacement along with impunity against extradition to the US due to double jeopardy laws.  <br />
 <br />
At the same time large numbers of AUC foot soldiers merely dipped out of the view only to reappear a year or two later with a new name, the &#8216;Black Eagles&#8217;, but very similar aims. They still fight the guerrillas by attacking the easier targets of &#8216;collaborators&#8217; and &#8216;subversives&#8217;.  All encompassing terms that are applied to everyone from human rights workers to butchers forced to sell FARC rustled cattle on pain of death.   <br />
 <br />
Uribe&#8217;s links to the paramilitaries goes back to his time as governor of Antioquia when he pioneered the CONVIVIR programme. The programme provided arms to &#8216;community groups&#8217; to combat the guerrillas. Predictably they went on the rampage, slaughtering not only guerrillas but also anyone they decided was a &#8216;collaborator&#8217;.  The programme was pulled by Pastrana but has been revived in Uribe&#8217;s reign. Meanwhile Uribe has found himself embroiled in the &#8216;parapolitica&#8217; scandal, an investigation into links between the paramilitaries and serving politicians. So far more than 60 congressman and 30 lawmakers, the vast majority of them Uribe supporters, are either under investigation or already in detention, as are some of his closest political allies including his cousin Mario Uribe.   <br />
 <br />
The indigenous movement, which has often been caught in the cross fire between the guerrillas and the paramilitaries and has suffered at the hands of both, has vociferously stressed it&#8217;s independence, despite government accusations of manipulation by the &#8216;bandits&#8217; and &#8216;delinquents&#8217; of FARC.  <br />
 <br />
<b>TRADING PLACES</b> <br />
 <br />
The Free Trade issue is one that has been making waves recently in the US. One of the campaign promises of Barrack &#8216;Change&#8217; Obama, and a key vote winner amongst working class Americans, was his opposition to the proposed Colombian FTA.  The agreement is part of the US revised policy of pursuing bilateral agreements following the collapse of the FTAA (Free Trade Area of the Americas) (<a href="../archive/news432.htm">SchNEWS 432</a>) as country after country in Latin America turned to the left and rejected the Free Trade consensus.   <br />
 <br />
As one of the only remaining rightist and American friendly countries, Colombia was seen as central to retaining an American finger in the South American pie.  Obama citied human rights concerns as his reason for opposition, particularly over the persistent assassinations of union leaders, one of the most dangerous jobs in Colombia. More than 600 have already been offed in Uribe&#8217;s presidency, 31 so far this year. Only last week he was supposedly offered a trade-off by still president Bush; a bail out for the automotive industry in return for the passing of the Colombian FTA. During the US election campaign Uribe made a series of what now appear to be horrendous gaffs, saying that he &#8216;deplored&#8217; the fact that Obama &#8216;ignores&#8217; Colombia&#8217;s progress in human rights, meeting with McCain and all but endorsing his campaign. Although he now claims he was &#8216;completely neutral&#8217; in the campaign he may find himself with some explaining to do to the new Mr President.   <br />
 <br />
In Colombia opposition focuses not only on human rights issues but also on economic matters. As has happened in other Latin American countries following similar agreements, the accord would expose small and small-medium sized agro-businesses - who grow quick yield crops like wheat, barley and corn - to potentially devastating competition from the US market. Meanwhile large landowners with the cash to develop such longer to yield crops like African palm oil, lumber and rubber will benefit. And who are these large landowners with nearly 50% of the most arable lands? Well, according to the government land institute INCORA, they&#8217;re drug traffickers who have bought up vast tracts of land in the last fifteen years.   <br />
 <br />
UR UNDER ATTACK <br />
 <br />
With these issues fuelling their rage the protest has travelled throughout Colombia, holding meetings, workshops and protests with various social movements and whoever else is willing to listen. Around 10,000 protesters arrived in Bogot� yesterday (20th) and now plan to hold a &#8216;Peoples Congress&#8217; in front of the government buildings in Plaza Bolivar. <br />
 <br />
And now it appears that with the unprecedented scale and organisation of the protests, the usually icy cool Uribe might actually be rattled. He doesn&#8217;t take criticism lightly.  <br />
 <br />
He has denounced certain (unspecified) human rights groups as &#8216;writers and demagogues at the service of terrorism who traffic in human rights&#8217; while a number of journalists questioning his links to the drug trade and to paramilitary groups have had to hot-foot it out of the country following death threats from, errm, paramilitary groups. With high popularity in the general population and a compliant mainstream media this is the first time he has had to face a co-ordinated opposition from the various sectors of the population who&#8217;ve suffered from his actions. <br />
   <br />
It remains to be seen whether this will represent a genuine turning point in Colombia&#8217;s political development. Already the issue is fading from view in the mainstream media. While images of the protesters blocking the highway and allegations and images of violent police responses were dramatic media fodder, peaceful protest just doesn&#8217;t cut it in the Colombian media. At least not when the Latin Grammys are on. But the protesters are still there, still marching and they seem to be finding a voice. And that is one of the most dangerous things to have in Colombia, both for Uribe and for them.  <br />
 <br />
 <br />
<b>For More See</b> <br />
 <br />
Colombia FTA:  <a href="http://www.narconews.com/Issue47/article2858.html" target="_blank">www.narconews.com/Issue47/article2858.html</a> <br />
 <br />
Indigenous Proposal: <a href="http://mamaradio.blogspot.com/2008/10/official-proposal-of-indigenous-and.html" target="_blank">http://mamaradio.blogspot.com/2008/10/official-proposal-of-indigenous-and.html</a> <br />
 <br />
Parpolitica: <a href="http://www.hrw.org/en/news/2008/08/03/colombia-proposal-threatens-parapolitics-investigations" target="_blank">www.hrw.org/en/news/2008/08/03/colombia-proposal-threatens-parapolitics-investigations</a>
</p>

</div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(133);
			
				if (SHOWMESSAGES)	{
				
						addMessage(133);
						echo getMessages(133);
						echo showAddMessage(133);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>