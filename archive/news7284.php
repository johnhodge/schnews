<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 728 - 25th June 2010 - Green Village Preservation Society</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, squatting, parliament square, direct action, london" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 728 Articles: </b>
<p><a href="../archive/news7281.php">Cutting 'n' Running</a></p>

<p><a href="../archive/news7282.php">Up Shit Greek</a></p>

<p><a href="../archive/news7283.php">Edo Trial Update</a></p>

<b>
<p><a href="../archive/news7284.php">Green Village Preservation Society</a></p>
</b>

<p><a href="../archive/news7285.php">Tome Raiders</a></p>

<p><a href="../archive/news7286.php">Razed Beds</a></p>

<p><a href="../archive/news7287.php">Sticky Wiki</a></p>

<p><a href="../archive/news7288.php">Israel In The Docks</a></p>

<p><a href="../archive/news7289.php">...and Finally...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 25th June 2010 | Issue 728</b></p>

<p><a href="news728.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREEN VILLAGE PRESERVATION SOCIETY</h3>

<p>
<p>
	Democracy Village and the camp which has occupied Parliament Square since Mayday is into the second week of the High Court battle &ndash; Democracy Village vs Boris Johnson (who wrongly assumed he had default power over the square &ndash; see <a href='../archive/news727.htm'>SchNEWS 727</a>). The judge will decide next Tuesday whether to issue a possession order to the GLA London council and, if that goes ahead, bailiffs could arrive for an eviction within days. In the 6-8 weeks there&rsquo;s been over 20 arrests including those for direct action rooftop banner drops and three arrests for climbing parliament. </p>
<p>
	The village are arguing on various fronts to win the case, including the need to have a public space and forum to discuss unlawful govt actions, such as illegal military adventures. Then there&rsquo;s four European human rights convention laws about freedom of expression, free assembly, religion and the right to legally question govt activites; Also the fact that traditionally, the land around Westminster Abbey was a mound which was the site of the Goreshedd of Tothill, a place of free speech, bardery, and festivities, enshrined by Richard I &ldquo;Lionheart&rdquo; in 1189, ratified by parliament in 1858 and never repealed. </p>
<p>
	There is still a call out to come to the square, and there will continue to be the regular events, including the talking circle on Wednesdays at 7pm, poetry and music from 5pm on Fridays, and the Peoples&rsquo; Assembly on Saturdays at 5pm. The whole camp is in fact a peoples&rsquo; assembly - a discussion forum - which became known as Democracy Village. It has side-stepped SOCPA laws (see <a href='../archive/news612.htm'>SchNEWS 612</a>) by saying it is a discussion not a protest. Even if there is an eviction, nothing can stop the continuation of peoples&rsquo; assemblies if it&rsquo;s new people or those not named on injunctions. </p>
<p>
	The court case which began on June 14th has been a lopsided battle between an unsympathetic judge &ndash; backed of course by extreme political weight &ndash; versus 19 uppity and up-for-it defendants aided by some top human rights lawyers and supporters. The judge got so pissed off with the defendants after being vigorously questioned about common law which he obviously didn&rsquo;t know, and being reminded of state war crimes, he declared that the public would no longer be allowed in, only the defendants and the press. </p>
<p>
	He also disallowed an expert on war legalities and the rights to assemble &ndash; because he&rsquo;s not a lawyer, even though he&rsquo;s spoken on government committees. But Monday (21st) saw star witness Tony Benn, who came out for the village and their right to hold the government to account, saying it was &lsquo;an integral part of the democratic process&rsquo;. </p>
<p>
	As an interim measure the GLA have allowed tents at the Great George Street corner on the paving &ndash; on a license for an undefined period - so this could go on for some time yet. <br /> 
	&nbsp;So it&rsquo;s not over yet: it needs new people - bring food, tea, tents, climbing gear, cake, skills to share and good vibes. If a possession order is issued next Tuesday, look out for call-outs to help defend the village, and make plans for the future. </p>
<p>
	* See <a href="http://www.democracyvillage.blogspot.com" target="_blank">www.democracyvillage.blogspot.com</a> and <a href="http://www.democracyvillage.org" target="_blank">www.democracyvillage.org</a> or see Facebook site. Site mobs 07981877462, 07722444146 </p>
<p>
	* This Tuesday (22nd) saw a day of actions at the square against the austerity measures which will effect the poor not the war effort, and Ed Milliband was confronted about whether he voted for the Iraq War &ndash; which he refused to respond to, as he was whisked off by a minder to avoid some activists who wanted to citizens arrest him for war crimes. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(834), 97); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(834);

				if (SHOWMESSAGES)	{

						addMessage(834);
						echo getMessages(834);
						echo showAddMessage(834);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


