<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 737 - 3rd September 2010 - Brighton Defence League</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, english nationalist alliance, english defence league, racism, brighton, anti-fascists, far right" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 737 Articles: </b>
<p><a href="../archive/news7371.php">Out Of Their League</a></p>

<b>
<p><a href="../archive/news7372.php">Brighton Defence League</a></p>
</b>

<p><a href="../archive/news7373.php">Grounded At Heathrow</a></p>

<p><a href="../archive/news7374.php">Marx And Sparks</a></p>

<p><a href="../archive/news7375.php">Shell Corrib Pipeline Under Attack</a></p>

<p><a href="../archive/news7376.php">On The Calais Beat</a></p>

<p><a href="../archive/news7377.php">Get Charter</a></p>

<p><a href="../archive/news7378.php">  Truth Or Dairy?</a></p>

<p><a href="../archive/news7379.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd September 2010 | Issue 737</b></p>

<p><a href="news737.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BRIGHTON DEFENCE LEAGUE</h3>

<p>
<p>
	The English Nationalist Alliance, a much smaller group have taken things a step further, moving on from targeting &lsquo;radical Islam&rsquo; to taking a stand against &lsquo;extremist students&rsquo; and their &lsquo;anti-English&rsquo; activities (see <a href='../archive/news736.htm'>SchNEWS 736</a>). Very much a poor cousin to the EDL, the ENA is essentially a response to the mobilisation against the March for England earlier this year (see <a href='../archive/news720.htm'>SchNEWS 720</a>). </p>
<p>
	There were fears that a success for the EDL in Bradford would have a knock-on effect in Brighton. However, only thirty idiots assembled at the train station with a few allies scattered around in local pubs. Those there soon dropped the ENA banner and openly paraded EDL flags and shirts. </p>
<p>
	The UAF for once did the right thing by arriving at the station en masse and together with other anti-fascists they succeeded in preventing the fash from marching for over an hour, before caving in to police requests to turn tail and march towards a pre-prepared cordon in Victoria gardens. </p>
<p>
	This left it up to the few non-aligned Brighton anti-fascists to try and stop the march. Sussex Police had clearly made a political decision to allow the march to go ahead, and with batons, dogs, and horses they proceeded to do exactly that. Even so they were forced to change the route of the march. Instead of parading through the town&rsquo;s main roads, the ENA/EDL were forced on a detour of Brighton&rsquo;s boutique-ridden North Laines, where they entertained locals with a healthy mixture of moronic racism and homophobia. </p>
<p>
	The small group of anarchos battled on for the next hour dragging wheelie bins into the road and resisting police charges by hand. They were joined by groups splintering away from the UAF demo and passers-by. Eventually however the fash were escorted back to the station, followed closely by a growing crowd of nosiy anti-fascists, who were clapped through the streets by passers-by. Small bands of racists continued to roam loose and on a couple of occasions breakaway groups were cornered in local pubs and had to be escorted away by police. The day ended with fourteen arrests, nine of ours and five of theirs. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(920), 106); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(920);

				if (SHOWMESSAGES)	{

						addMessage(920);
						echo getMessages(920);
						echo showAddMessage(920);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


