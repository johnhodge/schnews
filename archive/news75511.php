<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 755 - 21st January 2011 - And Finally</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 755 Articles: </b>
<p><a href="../archive/news7551.php">Inter-netcu</a></p>

<p><a href="../archive/news7552.php">Gateway Gate: Straight From The Pig's Mouth</a></p>

<p><a href="../archive/news7553.php">You Never Can Tel Aviv</a></p>

<p><a href="../archive/news7554.php">Bad Medicine... No Remedy For Uk Herbalists</a></p>

<p><a href="../archive/news7555.php">Fox Right Off</a></p>

<p><a href="../archive/news7556.php">The Network X-files</a></p>

<p><a href="../archive/news7557.php">Greece: The Conspiracy Of Fire Nuclei - Flamin' Eck</a></p>

<p><a href="../archive/news7558.php">Smashedo: C'mon Feel The Noise</a></p>

<p><a href="../archive/news7559.php">Fee For Yer Life</a></p>

<p><a href="../archive/news75510.php">A Bunch Of Cuts</a></p>

<b>
<p><a href="../archive/news75511.php">And Finally</a></p>
</b>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 21st January 2011 | Issue 755</b></p>

<p><a href="news755.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AND FINALLY</h3>

<p>
<p>
	<strong>UNDERCOVER COP SHOCKER EXCLUSIVE</strong>: The secretive anti-social world of policing has been rocked to its foundations this week by revelations that undercover eco-activists have been posing as police officers, working in the shadowy &lsquo;tactical units&rsquo; supposedly fighting the menace that they themselves threaten. </p>
<p>
	Clean-cut William Jackboot seemed like a regular plod, popular with the lads - a rising star, swiftly moved up the ladder until, in 2009, he was promoted to a position co-ordinating the elite forces of a new inter-constabulary force to combat activists, the Countering Undesirable Nasties Terrorising Squad. </p>
<p>
	As the team bodged operations, or arrived somewhere to find activists were one already step ahead, it was assumed all to be a result of normal police incompetence, but now that Jackboot has been outed and identified as really being a ramshackle hippie anarchist named Ramblin Bill Specialbrew who, six years ago, changed his name, got a haircut, and signed up. He proceeded to work his way up, alongside and friendly with unsuspecting colleagues - presumably all the while passing personal information and operational details back to his anti-authoritarian handlers. </p>
<p>
	Former friends on the force have been stunned by the realisation that &lsquo;Slick&rsquo; Willy was not all he seemed. They recount happy evenings of hard drinking at Spearmint Rhino. &lsquo;Why, he even helped me rough up a few black bloc&rdquo; said one, &ldquo;He had the Top Gear box-set n and didn&rsquo;t seem to give a shit about the state of environment,&rdquo; tells another. </p>
<p>
	Since his outing, which followed leaving copies of SchNEWS and a bong accidentally in his open locker one day, Specialbrew / Jackboot has gone into hiding in Armenia, saying he fears for his life. </p>
<p>
	&ldquo;<em>I&rsquo;m only the one they know about</em>,&rdquo; he said, <em>&ldquo;there are loads of secret tree-huggers in all branches of the force. On one occasion we went on an climate camp raid and there was only one real copper there - except for those undercover organising the activists we were after...</em> </p>
<p>
	&ldquo;<em>Anyway I was just doing what my eco-superiors suggested I do, after they&rsquo;d plied me with mushroom tea one night. But after a while you get confused. I found myself having sympathies with the cops plight. It was like stockholm syndrome - before I knew it I was busting kids and shaking down villains - and I started to love the life. But the pressure of this double life was grinding me down and I was getting no emotional support from my handlers. I&rsquo;d been walking, talking, shouting, pushing, beating like a copper for so long, I lost track of who I really was</em>.&rdquo; </p>
<p>
	Trust and moral within the force has plummeted as other eco-plants start to be weeded out and cops are wondering how far this thing goes. Brian Paddick? Sir Ian Blair? </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1076), 124); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1076);

				if (SHOWMESSAGES)	{

						addMessage(1076);
						echo getMessages(1076);
						echo showAddMessage(1076);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


