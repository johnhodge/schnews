<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 724 - 28th May 2010 - Flight Brigade</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, deportation, migrant rights" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 724 Articles: </b>
<b>
<p><a href="../archive/news7241.php">Flight Brigade</a></p>
</b>

<p><a href="../archive/news7242.php">Schmovies In Brief</a></p>

<p><a href="../archive/news7243.php">Cops Circle The Square</a></p>

<p><a href="../archive/news7244.php">War-sore</a></p>

<p><a href="../archive/news7245.php">Pan Handled</a></p>

<p><a href="../archive/news7246.php">H&k: A Kirk In The Teeth</a></p>

<p><a href="../archive/news7247.php">Homes Alone</a></p>

<p><a href="../archive/news7248.php">Bp: Block In The Pipeline</a></p>

<p><a href="../archive/news7249.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th May 2010 | Issue 724</b></p>

<p><a href="news724.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FLIGHT BRIGADE</h3>

<p>
<strong>EUROPEAN WEEK OF ACTION AGAINST THE DEPORTATION MACHINE&nbsp;</strong> <br />  <br /> <strong>&ldquo;<em>We now remove an immigration offender every eight minutes - but my target is to remove more, and remove them faster</em>.&rdquo; - Liam Byrne, Immigration Minister, 19th May 2008</strong> <br />  <br /> Migrant rights activists across the UK are readying themselves for next week&rsquo;s  European Week of Action Against the Deportation Machine. Campaigners are calling out for a week of protest and direct action targeting the government agencies and private companies responsible for shipping migrants out of Blighty and back to the desperate situations they risked their lives to flee, often subjecting them to violent abuse on their way (see <a href='../archive/news669.htm'>SchNEWS 669</a>). <br />  <br /> With parties across the political spectrum baying for the removal of migrants &ndash; easy scapegoats for any number of society&rsquo;s ills - the last government made the deportation of &lsquo;failed asylum seekers&rsquo; and &lsquo;immigration offenders&rsquo; (undocumented migrants who decided not to not to go through the torturous and twisted immigration process) a priority. The UK Border Agency (UKBA) shows scant regard for why these people may have risked their lives to come to Britain and unblinkingly deports them back to warzones, including Iraq and Afghanistan, or into the hands of the rapists and abusers they&rsquo;ve escaped. <br />    <br /> The deportation process is often brutal and both mentally and physically abusive. A 2008 report by Medical Justice Network and the National Coalition of Anti-deportation Campaigns (NCADC), &lsquo;Outsourcing Abuse&rsquo;, recorded incidents of beatings, people who were punched, kicked, choked or gagged, &lsquo;overzealously&rsquo; restrained, racially abused, dragged about by handcuffs or hair, sat on and even sexually abused. <br />   <br /> While many migrants are deported on standard flights, the increasing success of deportees in preventing flights by appealing to their fellow passengers or making a scene has led to the increasing use of specially chartered flights, which deport up to 80 people at a time. In 2009 there were 64 charter flights, deporting a total 973 people. The majority of them are deported to countries ripped apart by war and armed conflict such as Iraq, Afghanistan, DR Congo, Nigeria, Jamaica, Sri Lanka and Cameroon. Many have been kidnapped, imprisoned, tortured and killed on their return. <br />  <br /> With no ordinary passengers to keep the immigration authorities even vaguely civil, there is mounting evidence that the people on these flights are frequently subjected to horrendous abuse. Deportees are habitually handcuffed and forced onto the plane with threats of violence. Any attempts to resist are met with violent &lsquo;restraining techniques&rsquo;. On numerous occasions theses have degenerated into outright thuggery with contracted &lsquo;escorts&rsquo; savagely beating whole planeloads of resisting deportees. <br />   <br /> The removal of people on charter flights is also used to deny migrants even the basic legal rights permitted to them by a legal system hell bent on their removal. The UKBA-stards state: &ldquo;charter flights may be subject to &lsquo;different arrangements&rsquo; where it is considered appropriate because of the complexities, practicalities and costs of arranging an operation.&rdquo; These &lsquo;different arrangements&rsquo; include the stipulation that removals do not have to be stopped in the event of judicial reviews - removing the possibility of last-minute appeals - and that in many cases migrants do not have to be told the time, date or departure airport of the flights. Instead migrants are often woken up at the crack of dawn and told to switch off their phones so they can&rsquo;t alert family, friends or lawyers before being escorted to the airport (see <a href='../archive/news720.htm'>SchNEWS 720</a>) <br />  <br /> <strong>FLYING PICKETS</strong> <br />  <br /> Of course the UKBA is still some way from having its own cell-to-hell transport system, so relies on private companies who, of course, make a tidy profit from the inhumane opportunity. In 2008-09, the UKBA splashed out &pound;8.2 million on charter flights. Airlines that are known to have been used include Hamburg International, Czech Airlines, Titan Airways, BMI and others. Bus companies who drive deportees from detention centres to airports have included WH Tours and Woodcock Coaches. Security companies used to &lsquo;escort&rsquo; the deportees include Group 4 Securicor (G4S) and Serco. <br />   <br /> The action will kick off on Saturday (29th) with a picnic against deportations in Finsbury Park, where activists will gather to discuss and co-ordinate actions for the week ahead with, of course, some delicious scran and maybe a spot of frisbee. The week will climax with a mass demo against deportation the following Saturday (5th June). While a number of actions and campaigns are planned in-between, organisers are calling for activists to stand up and be counted and plan their own autonomous actions and protests throughout the week. <br />  <br /> <u><i><b><font size="4">The Plan:</font></b></i></u> <br />  <br /> <b>Tuesday, 1st June</b> &ndash; Pickets at immigration reporting centres. <b>1-2pm</b> &ndash; Picket at Communications House (Old Street, London), organised by Fight Racism! Fight Imperialism!.<b> 3-5pm </b>&ndash; Picket at Beckets House (London Bridge, London), organised by No Borders London. For more details, see here. <br />  <br /> <b>Wednesday, 2nd June</b> - An open discussion: &lsquo;Challenging deportations: Past, Present and Future&rsquo;, organised by No Borders London. 7pm @ LARC. For more details, see here. <br />  <br /> <b>Thursday, 3rd June</b> &ndash; Actions against private contractors. Watch out for details. Get in touch with Stop Deportation Network (see below) if you&rsquo;re interested. <br />  <br /> <b>Thursday, 3rd June/Friday, 4th June</b> &ndash;   Online campaign targeting booking agents and a major British airline involved in deportation flights, organised by the National Coalition of Anti-Deportation Campaigns (NCADC). More details soon. <br />  <br /> <b>Saturday, 5th June</b> &ndash; Mass demo against deportations in Parliament Square, London, called by Stop Deportation. 2-4pm. <br />  <br /> * See <a href="mailto:stopdeportation@riseup.net">stopdeportation@riseup.net</a> or <a href="http://www.stopdeportation.net" target="_blank">www.stopdeportation.net</a>  or  <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a> for details, or contact 07847 923251  <br />  <br /> ** For an account of the abuse suffered by deportees see <a href="http://www.medicaljustice.org.uk/images/stories/reports/outsourcing%20abuse.pdf" target="_blank">www.medicaljustice.org.uk/images/stories/reports/outsourcing%20abuse.pdf</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=deportation&source=724">deportation</a>, <a href="../keywordSearch/?keyword=migrant+rights&source=724">migrant rights</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(792);

				if (SHOWMESSAGES)	{

						addMessage(792);
						echo getMessages(792);
						echo showAddMessage(792);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


