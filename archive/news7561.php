<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 756 - 28th January 2011 - Mubarak's Against the Wall</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, protest, riots, egypt" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 756 Articles: </b>
<b>
<p><a href="../archive/news7561.php">Mubarak's Against The Wall</a></p>
</b>

<p><a href="../archive/news7562.php">A Funny Thing Happened On The Way To The Forum</a></p>

<p><a href="../archive/news7563.php">Under The Covers Cops</a></p>

<p><a href="../archive/news7564.php">A Cut Above</a></p>

<p><a href="../archive/news7565.php">Bristol: Branching Out</a></p>

<p><a href="../archive/news7566.php">My Big Fat Gypsy Eviction</a></p>

<p><a href="../archive/news7567.php">Rossport At The Ready</a></p>

<p><a href="../archive/news7568.php">Inside Schnews</a></p>

<p><a href="../archive/news7569.php">A Roamer Therapy</a></p>

<p><a href="../archive/news75610.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/756-egypt-lg.jpg" target="_blank">
													<img src="../images/756-egypt-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 28th January 2011 | Issue 756</b></p>

<p><a href="news756.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MUBARAK'S AGAINST THE WALL</h3>

<p>
<p>
	<strong>FROM TUNISA TO YEMEN, ARAB STREETS IN REVOLT AS OLD ORDER TREMBLES</strong> </p>
<p>
	Since last December Tunisia has been hit by relentless and transformative riots triggered by unemployment, food inflation, lack of freedom of speech and poor living conditions. The violent unrest eventually led to the ousting of President Zine El Abidine Ben Ali, who fled the country on the 14th of January after a hefty 23 years in power. Daily protests have continued due to prominent figures in the Ben Ali regime clinging on to posts in the new interim government. <br /> 
	&nbsp;The protests began after Mohamed Bouazizi, a Tunisian street vendor, burned himself to death in protest after police confiscated his produce cart. That one act of defiance has since been mirrored across North Africa and the Arab world, as have the the scenes of mass rebellion that ousted Ben Ali. </p>
<p>
	The new year began with mass protests in Algeria which saw 800 people injured in several days of rioting, before President Abdelaziz Bouteflika was forced to cut food costs and lower import duties to calm things down - for the time being. </p>
<p>
	Next in line stands Egypt&rsquo;s dictatorship, which faces its most substantial resistance in decades. The crossroads country between Europe, Africa and the Middle East has been in turmoil since Tuesday (25th), when thousands marched in the streets of Cairo and other cities. The protests appear to be leaderless, instigated by youth who hold no affiliation to known political groups. On Wednesday (26th), once again thousands of demonstrators took to the streets, despite violent police attempts to thwart them. </p>
<p>
	<strong>CAIRO MANIACS</strong> </p>
<p>
	The police fired rubber bullets and used tear gas, batons and water cannons to disperse the activists gathered in the centre of Cairo, who in turn responded by throwing rocks and burning tyres. Protesters in Suez set a government building on fire later in the evening. Elsewhere attempts were made to firebomb the government&rsquo;s local HQ before cops held crowds back with tear gas. </p>
<p>
	As night fell, more than 2,000 people remained out in the streets in various parts of Cairo. On Thursday (27th), Egyptian stock markets fell to their lowest level for two years, forcing a pause on trading as the situation seemingly discombobulated investors. The streets were comparably quiet apart from a small cluster of demonstrators outside the Egyptian lawyers&rsquo; syndicate. The lull before the storm. As one Egyptian protester put it &ldquo;This is do or die.&rdquo; </p>
<p>
	So far there have been at least four deaths, including three protesters. According to human rights groups, 2,000 demonstrators have been arrested and the count is still running. There have been reports of over 85 police officers injured in the clashes so far. The Egyptian government has now blocked social media websites such as Twitter and Facebook, websites of independent newspapers and live streaming applications in an attempt to repress unrest. </p>
<p>
	However, social online networks are still accessible via proxies. The interior ministry has decreed a public ban on demonstrations, with a warning that any protesters will be prosecuted. </p>
<p>
	Despite the ban, a massive rally has been called in Cairo for Friday (28th), &ldquo;with all the national Egyptian forces, the Egyptian people, so that this coming Friday will be the general day of rage for the Egyptian nation&rdquo;, according to a posting on the web. The protesters are demanding President Mohamed Hosni Mubarak not run again and that his son, Gamal, not run in his place - the old nepotism clause. The sands of time are shifting for Mubarak, who has been entombed at the top of Egyptian power pyramid since 1981 - a whopping 29 years in office. One wonders how he managed to win the single-candidate elections for so long. Mubarak may not be a popular figure amongst Egyptians but has that all important US backing, being one of largest recipients of US aid. Where will it go from here? Will he crush discontent or get swallowed up by the plague of protest? </p>
<p>
	<strong>YEMEN-TAL</strong> </p>
<p>
	And now Yemen has become the latest Arab state to be shaken by mass protests as tens of thousands of protesters hit the streets in the capital and other cities demanding a change in government. Turmoil and rebellion may be a common sight there, but this time the people of Yemen are calling for an end to the government of President Ali Abdullah Saleh, who tops the autocrat charts with a lengthy 32 years in power and happens to be another US ally. The spark that began in Tunisia, scalded Algeria, caressed Jordan and ignited Egypt, is now setting fires in one of the middle East&rsquo;s most destitute countries. Anti-government demonstrators marched through Sanaa on Thursday (27th) shouting for Saleh to step down. Unlike events in Egypt this week and Tunisia over the last month, the Yemen dissent has been relatively peaceful. Saleh has responded by denying claims about his son inheriting his position and promising to raise army salaries in a move to clinch troopers&rsquo; loyalty. </p>
<p>
	The question now occupying the Arab world is whether this stream of protests will turn into a flood, and where will it lead to next? The US, the world&rsquo;s busybody, has already stuck its oar in. Yankee wisdom alleges that there may be some turbulence but all is well in the Arab world. </p>
<p>
	Secretary of State Hilary Clinton&rsquo;s best effort consisted of, &ldquo;We see that Yemen is going through a transition,&rdquo; and &ldquo;our assessment is that the Egyptian Government is stable and looking for ways to respond to the interests of the Egyptian people.&rdquo; The truth is that regions all across the Arab world are raging due to common elements such as rising food prices, corruption, poverty, youth unemployment and dynastic totalitarian governments. This situation will require more profound solutions than placing the blame on the standard handful of &ldquo;terrorists&rdquo; that are letting the side down. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1077), 125); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1077);

				if (SHOWMESSAGES)	{

						addMessage(1077);
						echo getMessages(1077);
						echo showAddMessage(1077);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


