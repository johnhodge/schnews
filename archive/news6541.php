<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 654 - 7th November 2008 - B'iraq Obama</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, iraq, barack obama, usa, joe biden, us presidential election, oil law, occupation" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="****BANNER_GRAPHIC_LINK****"><img 
						src="../images_main/****BANNER_GRAPHIC****" 
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 654 Articles: </b>
<b>
<p><a href="../archive/news6541.php">B'iraq Obama</a></p>
</b>

<p><a href="../archive/news6542.php">Ray Ban</a></p>

<p><a href="../archive/news6543.php">Congo Much Worse</a></p>

<p><a href="../archive/news6544.php">Damp Squibb</a></p>

<p><a href="../archive/news6545.php">Trick Or Trot</a></p>

<p><a href="../archive/news6546.php">Gunpowder,  Class War & Plod</a></p>

<p><a href="../archive/news6547.php">Home Farm And Away  </a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/654-bush-dustbin-lg.png" target="_blank">
													<img src="../images/654-bush-dustbin-sm.png" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 7th November 2008 | Issue 654</b></p>

<p><a href="news654.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>B'IRAQ OBAMA</H3>

<p>
<b>AS SchNEWS PUTS A MUCH NEEDED PERSPECTIVE ON OBAMA VICTORY</b><br />
<br />
So eight years of Bush madness are finally over, and mercifully the election result didn't turn into a battle over hanging chads or the matter of how many Hispanics and blacks were excluded from voting. But do you really think that Obama will send the troops home the day after he takes office? Out on the campaign trail he was suspiciously silent about the Iraq war. Could it be he was scared of looking unpatriotic compared to McCain, or have some well placed Washington power brokers been having a few quiet words in his ear?<br />
<br />
A reality check: What Obama said before the election is a moot point now. The sign of a brilliant orator is the ability to hold a crowd spell-bound, without actually saying anything. On the subject of Iraq, he did announce that US forces would remain there against Iraqi wishes - although maybe without 'permanent' military bases - to train Iraqi personnel (death squads). He also stated that he will 'retain the right to intervene militarily, with our international partners, to suppress potential genocidal violence within Iraq.' All this equates to rehashed liberal interventionist arguments justifying a continued war on Iraq. But what we're really looking at was never a Bush plan, and isn't an Obama plan, but is the US Imperial plan for Iraq. There has been cross-party support for the Occupation since the beginning.<br />
<br />
Further clues to the direction of the future Obama Administration's foreign policy comes in the shape of the advisers he's surrounding himself with such as hawkish favourite of conspiracy theorists and former Carter advisor Zbigniew Brzezinski, as well as such leading humanitarians as General Merrill McPeak (a backer of Indonesia's occupation of East Timor) and Dennis Ross (supporter of Israel's occupation of the West Bank).<br />
<br />
To go into more depth about Team Obama's policy on Iraq, we need to take a look at his right hand man - soon to be Veep Joe Biden. Obama may have voted against the Iraq war but Biden was all for it.<br />
<br />
Biden's take on the US plan is for a 'third way' between occupation and independence that reads like divide and rule colonialism of the old school. His plan for Iraq was hatched in 2006: partition Iraq into three separate sectarian statelets, Sunni, Kurd and Shia, all governed by US-friendly puppets instructed to provide an uninterrupted supply of oil to Western companies.<br />
<br />
This 'third way' (where have we heard that before?) of 'soft partition' is designed to benefit first and foremost multinational oil interests - but is not so soft for those who would face death threats for being on the wrong side of a partition line. <br />
<br />
Also central to this strategy is the Oil Law, written in July 2006 and re-drafted with the advice of nine multinational oil companies, the US and UK authorities and the IMF. Its primary aim is to sanction Production Sharing Agreements - privatisation deals - and create a Federal Oil and Gas Council made up of regional political elites. <br />
<br />
The Law would allow regions to pass their own oil laws, run their own industries and sign their own contracts with international oil companies without any democratic oversight. What former policy boss of BP Nick Butler calls a 'parting gift of oil for peace' is really designed to create a race to the bottom, aimed to set the Iraqi factions climbing over each other to sell oil to the multinationals. If they ever pull it off the net results of this plan will be division, conflict and decades of dependency on foreign oil companies for Iraqis.<br />
<br />
The Kurdish Regional Government is already ahead of the game,  interpreting the Iraqi constitution as giving them the right to start signing privatisation contracts with foreign oil companies, passing their own regional oil law and forming an autonomous ministry of oil. The Oil Law's devolution provisions would also economically empower the Supreme Iraqi Islamic Councils (the country's most powerful political party) intentions to form a 9-region super-state in the south of Iraq. Meanwhile the oil unions are vehemently opposed to any division of Iraq, the Oil Law, foreign oil control and the occupation itself.<br />
<br />
<b>ONCE BIDEN, TWICE SHY</b><br />
<br />
But whoever's in the White House pulling the levers of power, all this might just turn out to be irrelevant after all. The Americans' arrogant disregard for Iraqis' sentiments has already caused a nationalist backlash that mobilised opposition to draft the 'Baghdad Charter' opposing any territorial division of Iraq.<br />
<br />
One striking feature of Iraqi politics of late is just how little ability the Americans have to call the shots in the country, even with 150,000 troops there. Despite the passage of the Oil Law, Iraqi politicians (even former US allies) are showing worryingly independent tendencies. The Status of Forces Agreement (SOFA) the Americans need if their presence in Iraq has even a token agreement from their puppets is foundering as the deadline for the UN Mandate runs out in December. Despite a mix of threats and concessions to the Iraqi government, even their hand-picked Prime Minister Maliki is still refusing to sign. US politicians, whether Democrat or Republican, just can't seem to grasp that they don't really control events on the ground. As much as they push for Oil Laws and agreements legalising their continued military presence, the truth is that the Occupation is hated too much for them to ever fulfil their ambitions in the country.<br />
<br />
Spouting empty slogans for 'Change' and engaging in no-detail rhetoric in televised debates and speeches shouldn't make anyone optimistic that he'll deliver what most Iraqis (and Americans and Brits for that matter) want - an immediate end to the occupation.<br />
<br />
<br />
For more see:<br />
<br />
<a href="http://www.handsoffiraqioil.org" target="_blank">www.handsoffiraqioil.org</a><br />
<a href="http://www.redpepperobamablog.blogspot.com" target="blank">www.redpepperobamablog.blogspot.com</a><br />
<a href="http://www.zmag.org" target="_blank">www.zmag.org</a><br />
<a href="http://www.counterpunch.org" target="_blank">www.counterpunch.org</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=iraq&source=654">iraq</a>, <a href="../keywordSearch/?keyword=usa&source=654">usa</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(111);
			
				if (SHOWMESSAGES)	{
				
						addMessage(111);
						echo getMessages(111);
						echo showAddMessage(111);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>