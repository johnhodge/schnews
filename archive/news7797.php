<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 779 - 15th July 2011 - Rotten Fruit</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, carmel agrexco, israel" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 779 Articles: </b>
<p><a href="../archive/news7791.php">Rolling Out The Unwelcome Mat</a></p>

<p><a href="../archive/news7792.php">Gaza Flotilla: Seige You Later</a></p>

<p><a href="../archive/news7793.php">Don't Mansion It</a></p>

<p><a href="../archive/news7794.php">Not The Full English</a></p>

<p><a href="../archive/news7795.php">Inside Schnews</a></p>

<p><a href="../archive/news7796.php">Like It Or Lumpur It</a></p>

<b>
<p><a href="../archive/news7797.php">Rotten Fruit</a></p>
</b>

<p><a href="../archive/news7798.php">Lyons Led By Donkeys</a></p>

<p><a href="../archive/news7799.php">C'mon Bunny Light My Fire</a></p>

<p><a href="../archive/news77910.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th July 2011 | Issue 779</b></p>

<p><a href="news779.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ROTTEN FRUIT</h3>

<p>
<p>  
  	Israel&rsquo;s largest exporter of fruit and veg is in dire straits this week as the debt-riddled corporation saw its CEO resign.  </p>  
   <p>  
  	Carmel Agrexco has long been under fire by campaigners for its direct support of illegal Israeli settlements in Palestine. Agrexco employs Palestinian workers and a Thai migrant labour force to tend Israeli-owned crops in many areas that have been occupied illegally by Israel. The wages are well below the national average and workers are often subjected to humiliating treatment as they attempt to pass checkpoints to get to Agrexco&rsquo;s fields and packing houses.  </p>  
   <p>  
  	Agrexco lost just under &pound;30million in 2010, and a state bailout of nearly &pound;10million in December 2010 has done little to alleviate the corporation&rsquo;s financial problems. The company has been through restructuring upheaval in the last few years as, in 2008, Agrexco joined the ranks of Israeli state-owned companies sold off for privatisation. The combined forces of worldwide recession, loss of market share and a bad year of harvests have been blamed for the collapse, and the company&rsquo;s bondholders are demanding their &pound;28million debt to be paid. Tnuva, formerly a kibbutz co-operative owned organisation, sold off to London-based private equity firm Apax Partners in 2006, owns 11% of Agrexco and has so far been the only major shareholder voicing a willingness to inject capital to keep the company going. The Plant Production and Marketing Board, a quango representing farmers with a 55% share has refused, as has the Israeli state, who own 30%.  </p>  
   <p>  
  	The UK Agrexco depot in Hayes, Middlesex has been repeatedly targeted over the years by activists (See <a href='../archive/news709.htm'>SchNEWS 709</a>, <a href='../archive/news666.htm'>666</a>, <a href='../archive/news597.htm'>597</a>, <a href='../archive/news585.htm'>585</a>...). Seven protesters who blockaded and occupied the depot in 2006 were later acquitted in court after the case was kicked out, but not before Agrexco were forced to reveal where their products came from. Since then, the corporation hasn&rsquo;t prosecuted any demonstrators who have done actions at the depot. <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1282), 148); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1282);

				if (SHOWMESSAGES)	{

						addMessage(1282);
						echo getMessages(1282);
						echo showAddMessage(1282);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


