<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 716 - 2nd April 2010 - Out To Launch</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, free gaza, gaza, palestine, rachel corrie" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 716 Articles: </b>
<p><a href="../archive/news7161.php">Eire We Go Again</a></p>

<p><a href="../archive/news7162.php">Down To The Wire</a></p>

<p><a href="../archive/news7163.php">Sand Storms</a></p>

<b>
<p><a href="../archive/news7164.php">Out To Launch</a></p>
</b>

<p><a href="../archive/news7165.php">No Expense Bared</a></p>

<p><a href="../archive/news7166.php">More Than Fair</a></p>

<p><a href="../archive/news7167.php">Hard Copy From Honduras</a></p>

<p><a href="../archive/news7168.php">Ukba Go Channel Hopping</a></p>

<p><a href="../archive/news7169.php">All's Well That Nz Well</a></p>

<p><a href="../archive/news71610.php">Story To Tel</a></p>

<p><a href="../archive/news71611.php">Un-irving</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd April 2010 | Issue 716</b></p>

<p><a href="news716.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>OUT TO LAUNCH</h3>

<p>
The Free Gaza Movement (FGM) has bought 1200 tonne cargo ship to add to its siege-breaking fleet. The boat was bought at auction after it had been impounded when an International Transport Federation (ITF) inspection found the owners had been exploiting their Lithuanian crew &ndash; not paying their wages and humiliating them. <br />   <br /> ITF Inspector and union organiser Ken Fleming said, &lsquo;We are pleased to announce that this vessel which was used to subject workers to modern day slavery, will now be used to promote human rights for the people of Palestine.&rsquo; <br />  <br /> The FGM have renamed the boat &ldquo;The Rachel Corrie&rdquo; after the Palestinian solidarity activist killed by an Israeli bulldozer in 2003 as she tried to prevent it from demolishing Palestinian homes (see <a href='../archive/news397.htm'>SchNEWS 397</a>). A civil action case against the state of Israel over Corrie&rsquo;s death, brought about by her parents, opened last month (See <a href="http://reider.wordpress.com" target="_blank">http://reider.wordpress.com</a>) <br />  <br /> &ldquo;The Rachel Corrie&rdquo; will join three other ships in taking humanitarian aid to Gaza in a flotilla setting off in May. The new ship will carry 500 tons of cement, as well as medicines, medical equipment and educational materials. <br />  <br /> * See <a href="http://www.freegaza.org" target="_blank">www.freegaza.org</a> <br />  <br /> ** <strong>April 20th: A Night of Comedy Music and Magic for Palestine</strong>. Ivor Dembina, Jeremy Hardy, Ian Saville, Maureen Younger &amp; Music by The Amigans. At the Dogstar, 389 Coldharbour Lane, Brixton SW9 8LQ, Doors open 7pm, show at 8pm. &pound;6/&pound;8, with all proceeds to The International Solidarity Movement, The Russell Tribunal on Palestine and The Lajee Centre in Bethlehem. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=free+gaza&source=716">free gaza</a>, <a href="../keywordSearch/?keyword=gaza&source=716">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=716">palestine</a>, <span style='color:#777777; font-size:10px'>rachel corrie</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(719);
			
				if (SHOWMESSAGES)	{
				
						addMessage(719);
						echo getMessages(719);
						echo showAddMessage(719);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


