<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 722 - 14th May 2010 - Decommissioners Trial Delayed</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, edo decommissioners, direct action, arms trade" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 722 Articles: </b>
<p><a href="../archive/news7221.php">Squaring Up To 'em</a></p>

<p><a href="../archive/news7222.php">Not A Clegg To Stand On</a></p>

<b>
<p><a href="../archive/news7223.php">Decommissioners Trial Delayed</a></p>
</b>

<p><a href="../archive/news7224.php">Glade Rags</a></p>

<p><a href="../archive/news7225.php">Sweaty Palms At Unilever</a></p>

<p><a href="../archive/news7226.php">Raising Arizona</a></p>

<p><a href="../archive/news7227.php">Bounty Bar-stards</a></p>

<p><a href="../archive/news7228.php">Lab- Lib Pact</a></p>

<p><a href="../archive/news7229.php">Crude Awakening</a></p>

<p><a href="../archive/news72210.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th May 2010 | Issue 722</b></p>

<p><a href="news722.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DECOMMISSIONERS TRIAL DELAYED</h3>

<p>
The trial of the EDO Decommisioners (see <a href='../archive/news721.htm'>SchNEWS 721</a>) has been delayed for three weeks. The trial now starts on June 7th at Hove Crown Court (TBC check <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> closer to the time). But our mates at SMASH EDO aren&rsquo;t letting the pressure drop. Every Wednesday 4pm-6pm they&rsquo;re bringing the noise outside the Factory of Death <a href="http://www.smashedo.org.uk/noise.htm" target="_blank">www.smashedo.org.uk/noise.htm</a>  <br />  <br /> <b>* Picket of Barclays Ban</b>k, (the market maker for ITT on the NY Stock Exchange), Western Road, Brighton, Sat May 22nd, 11am <br />  <br /> <b>* &lsquo;Gaza: Beneath the Bombs&rsquo;</b>: Sharyn Lock and Sarah Irving will be talking about their new book, based on ISM volunteer Sharyn&rsquo;s experiences of Israeli attacks on Gaza. Sat May 22nd 7pm, Cowley Club, 12 London Rd, Brighton.  <br />  <br /> <b>* Remember Gaza vigil</b> outside the Hove Crown Court (check website for confirmation), 9am, Monday 7th June (moved from 17th May). <br />  <br /> <b>* If I had a Hammer</b> - Decommissioners support demo at EDO MBM, Home Farm Road, Brighton, 4pm-6pm. Wednesday 9th June (moved from 19th May) <br />  <br /> <b>* Day of the Verdict</b> in the EDO/ITT Decommissioners Trial - The trial will be going on throughout June and July 2010. Whether the verdict is guilty or not guilty our resistance against EDO/ITT will continue... We will be here until EDO are not! We are planning to hold a demonstration as soon as the verdict is known - keep an eye on this site for updates.  <br />  <br /> If you&rsquo;d like to receive an update about the verdict and about the demonstrationemail your email address or mobile number to <a href="mailto:smashedo@riseup.net">smashedo@riseup.net</a> or text 07526557436. <a href="http://decommissioners.co.uk" target="_blank">http://decommissioners.co.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=722">arms trade</a>, <a href="../keywordSearch/?keyword=direct+action&source=722">direct action</a>, <a href="../keywordSearch/?keyword=edo+decommissioners&source=722">edo decommissioners</a>, <a href="../keywordSearch/?keyword=smash+edo&source=722">smash edo</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(775);

				if (SHOWMESSAGES)	{

						addMessage(775);
						echo getMessages(775);
						echo showAddMessage(775);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


