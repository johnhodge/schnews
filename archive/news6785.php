<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 678 - 5th June 2009 - Holy Unjustified</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, palestine, holy land foundation, hamas" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.london.noborders.org.uk/calais2009"><img 
						src="../images_main/no-border-calais-banner.png"
						alt="No Borders Camp, Calais, June 23-29th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 678 Articles: </b>
<p><a href="../archive/news6781.php">Convoy Polloi</a></p>

<p><a href="../archive/news6782.php">The Rich Get Richter</a></p>

<p><a href="../archive/news6783.php">Tara For Now  </a></p>

<p><a href="../archive/news6784.php">Deportation Alert</a></p>

<b>
<p><a href="../archive/news6785.php">Holy Unjustified</a></p>
</b>

<p><a href="../archive/news6786.php">Photo Finish</a></p>

<p><a href="../archive/news6787.php">Rebels Without A Causeway</a></p>

<p><a href="../archive/news6788.php">Brian Of Britain</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 5th June 2009 | Issue 678</b></p>

<p><a href="news678.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HOLY UNJUSTIFIED</h3>

<p>
We&rsquo;ve seen the movies &ndash; they just don&rsquo;t like folks being a bit &lsquo;different&rsquo; down in Texas. Last week a Dallas court gave the founders of a charity sending humanitarian aid to Palestine a legal lynching as they sent &lsquo;em down, with scant evidence, for seriously draconian sentences. Two of the defendants got 65 year sentences, one 20, and two others 15 &ndash; one for merely being a voluntary fundraiser for the group. <br />  <br /> This was obviously the &lsquo;right&rsquo; result as far as the government was concerned, with this being the culmination of the most costly US &lsquo;terror financing&rsquo; investigation ever. &nbsp; <br />  <br /> The Holy Land Foundation, once the biggest Muslim charity in America, was shut down shortly after 2001 in the wave of patriotic Homeland Security paranoia as (Texas boy) George Bush looked for scapegoatable terror targets close to home. Despite being nothing to do with Saudi nationals flying planes into buildings (like Saddam Hussein), the charity was accused of being a front for sending cash to support &lsquo;terrorist&rsquo; Hamas in its battle against the Israeli occupiers.&nbsp; <br />  <br /> A ten year investigation turned up nothing more than proof that the charity did exactly what it claimed to, i.e. sent money to officially recognised Palestinian charities which was provably distributed to provide humanitarian aid &ndash; charities which the US government itself also gave help to through USAID for some years after Holy Land was pounced on. Even using &lsquo;secret&rsquo; (ie undisclosed) Israeli intelligence and disputed documents from FBI tapping operations, the founders could only be charged with indirectly supporting terror by dealing with organisations which may have been in touch with or under the control of Hamas &ndash; something they, nor anyone else, could really know or avoid even if they wanted to. <br />  <br /> A previous trial of the defendants had ended in aquittal on some charges and a split jury on others, leading to a mistrial &ndash; so the government just decided to do it all again only this time a with more carefully selected jury. <br />  <br /> At his sentencing hearing, ex charity chairman Ghassan Elashi, himself Palestinian born, said, &ldquo;<em>Nothing was more rewarding than&hellip; turning the charitable contributions of American Muslims into life assistance for the Palestinians. We gave the essentials of life: oil, rice, flour. The occupation was providing them with death and destruction</em>.&rdquo; For his heinous crime, he was sentenced to 65 years in prison. What a great victory for the Land of the Free. <br />  <br /> * For more see: <a href="http://www.democracynow.org/2009/5/29/holy_land" target="_blank">www.democracynow.org/2009/5/29/holy_land</a> <br />  <br /> * Meanwhile, just how cosy that US / Israeli alliance has been revealed this week as the Israeli government looked for a way to derail Obama&rsquo;s demands for a freeze on all settlement activity in the occupied West Bank. Prime Minister Benjamin Netanyahu justified continued settlement construction by citing secret agreements between prior Israeli governments and the Bush administration. The Israeli government says it agreed to the 2003 US-backed Road Map that called for a settlement freeze only on condition that it be allowed to violate the plan&rsquo;s ban on expanding existing settlements. No problemo! said Geroge... Diplomacy in action.&nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=hamas&source=678">hamas</a>, <span style='color:#777777; font-size:10px'>holy land foundation</span>, <a href="../keywordSearch/?keyword=palestine&source=678">palestine</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(349);
			
				if (SHOWMESSAGES)	{
				
						addMessage(349);
						echo getMessages(349);
						echo showAddMessage(349);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>