<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 672 - 17th April 2009 - Easy, Tiger...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, tamil tigers, tamil, sri lanka, civil war, chemical weapons, ceylon, sinhala" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.smashedo.org.uk/mayday-09.htm"><img 
						src="../images_main/mayday-2009-banner.jpg"
						alt="Mayday - Smash EDO-ITT, Brighton, 4th May 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 672 Articles: </b>
<b>
<p><a href="../archive/news6721.php">Easy, Tiger...</a></p>
</b>

<p><a href="../archive/news6722.php">Day Of Anger</a></p>

<p><a href="../archive/news6723.php">Pathological Liars</a></p>

<p><a href="../archive/news6724.php">Right To Roma</a></p>

<p><a href="../archive/news6725.php">Too Cruel For School</a></p>

<p><a href="../archive/news6726.php">W-hacked In Jail</a></p>

<p><a href="../archive/news6727.php">Mallets Mallet</a></p>

<p><a href="../archive/news6728.php">Ray Of Blight   </a></p>

<p><a href="../archive/news6729.php">Getting On Our Tits</a></p>

<p><a href="../archive/news67210.php">Fools Gold</a></p>

<p><a href="../archive/news67211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/672-now-police-state-lg.jpg" target="_blank">
													<img src="../images/672-now-police-state-sm.jpg" alt="NOW THAT'S WHAT I CALL A POLICE STATE 2009" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 17th April 2009 | Issue 672</b></p>

<p><a href="news672.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EASY, TIGER...</h3>

<p>
<span style="font-size:13px; font-weight:bold">AS THE CIVIL WAR IN SRI LANKA ESCALATES WITH GROWING CASUALTY LIST</span> <br />
 <br />
<b>Worldwide protests - which in London led to violent clashes with police - from the Tamil diaspora have erupted in response to the escalation in the Sri Lankan military onslaught against Tamils in the so-called &#8216;safe-zone&#8217; in the north-east of the country. </b> <br />
 <br />
As reported in <a href='../archive/news665.htm'>SchNEWS 665</a> the Sri Lankan military has driven the separatist rebels of the Liberation Tigers of Tamil Eelam, AKA the Tamil Tigers, out of most of their former territory into a narrow enclave on the coast. Since then, in the face of a deafening silence from the international community, the Sri Lankan army has squeezed the Tigers into an ever tinier strip whilst a humanitarian disaster has unfolded with 200,000 civilians trapped between the forces. The media, which isn&#8217;t allowed into the zone, has received horrific eye-witness reports from those fleeing the violence, who managed to avoid being detained by government forces or shot by the Tigers. <br />
 <br />
 The ordeal continues for many Tamil civilians outside the zone when they are put in heavily militarised over-crowded &#8216;model villages&#8217;, or concentration camps, where those suspected of Tiger connections are disappeared by paramilitaries, whilst dissenting voices are silenced and accusations of &#8216;genocide&#8217; are becoming commonplace. The International Federation of Journalists accuses the Sri Lanka government of using a combination of anti-terrorism laws, disappearances and assassinations to silence journalists. Sri Lankan journalist Lasantha Wickrematunge was murdered on January 8th this year. <br />
 <br />
<span style="font-size:13px; font-weight:bold">HUNTED TO EXTINCTION</span> <br />
 <br />
The recent round of protests began on April 6th after reports of the Sri Lankan army use of chemical weapons on Tamil civilians at Bhuthukudiyrup in Mulaithivu in the &#8216;safety zone&#8217; over the Easter weekend. NGO &#8216;War without Witness&#8217; commissioned investigations by doctors concluded that victims&#8217; wounds were consistent with Mustard Gas attacks delivered via high explosive rocket launchers and aerial bombing. Due to government exclusion of international agencies and NGOs from the combat zone and hospitals no comprehensive forensic or chemical analysis could be carried out. <br />
 <br />
Hundreds of civilians were dying every day last week from government artillery strikes, the lack of medical treatment for victims and starvation. An attack on two hospitals killed 30 immediately, including young children and medical workers, and injured 300. At one hospital hundreds had been waiting for food when the shells landed. Despite condemnations from human rights and humanitarian agencies the world&#8217;s leaders have failed to find time from their summit junkets to act to end the unfolding tragedy. <br />
 <br />
The Sri Lankan government declared a two-day ceasefire on April 13-14th but still refuses to negotiate with the Tigers, who are holding the 200,000 civilians in the &#8216;safety zone&#8217; hostage. Determined to wipe out the Tigers the army resumed its shelling this Wednesday, with further massive civilian casualties likely. <br />
 <br />
<span style="font-size:13px; font-weight:bold">LAW OF THE JUNGLE</span> <br />
 <br />
Britain has witnessed a number of large scale and dramatic protests. 300 Tamil youths who began a blockade of Westminster Bridge were joined by thousands more throughout the week, culminating with over 100,000 Tamils marching through central London on April 11th. Police responded to the initial protests by kettling the group before attacking many with batons. Several people were arrested under the Terrorism Act for carrying the Tamil national flag, which is also the flag of the Tamil Tigers &#8211; officially a terrorist organisation. Two Tamil men went on hunger strike, although one has since suspended his fast in response to efforts to organise international talks. <br />
 <br />
It&#8217;s no coincidence that such a large section of the Tamil diaspora reside in Britain. The British meddling on the island began way back in 1776 and continues to the current day in the form of continued arms sales against the background of toothless requests for a ceasefire.  <br />
 <br />
When the British first arrived, following on the heels of previous colonial rulers the Portuguese and the Dutch, they merged the previously distinct kingdoms of the Tamils and the Sinhalese &#8211; the majority ethnic group of Sri Lanka -  to create a unitary state that was later named Ceylon. Following this they brought around a million Tamils from India to work in the tea fields. <br />
 <br />
Following independence in 1948 attempts were made to prevent persecution of the minority Tamils  by introducing a system of proportional representation to avoid Sinhalese electoral dominance while the new constitution included clauses prohibiting discriminatory legislation. However, the PR system was quickly dropped while the anti-discrimination laws were widely ignored until they were finally dropped completely in 1972. Around a million Tamils of Indian origin were disenfranchised immediately after independence and many were deported. This was followed by the Sinhala Only Act &#8211; which discriminated economically, culturally and linguistically against the Tamils. <br />
 <br />
Throughout the 50&#8217;s, 60&#8217;s and 70&#8217;s the Tamils responded to the increasing repression and discrimination with non-violent resistance and political negotiations. The response was military repression, torture and disappearances while agreements to devolve power to Tamils under a quasi-federal system were unilaterally abrogated by successive governments. The situation culminated in 1983 with the events of &#8216;Black July&#8217; when devastating anti-Tamil pogroms resulted in thousands of Tamils being burnt alive. <br />
 <br />
1983 is generally accepted as the start of the civil war that has raged since. While Tamil Tiger abuses have been widely documented since then, they pale in comparison to the campaign of torture, rape and extra-judicial killings on a genocidal scale subsequently waged by the authorities. <br />
 <br />
Throughout this period the British government has continued to supply arms to the Sri Lankan military. Although Home Secretary David Miliband has recently called for a ceasefire, action to make that a reality has been notably absent. Britain failed to support a UN briefing on Sri Lanka with Miliband declaring that a vetoed resolution would be worse than no resolution, leading to claims that Britain is shirking it&#8217;s obligation to prevent genocide.   <br />
 <br />
* See <a href="http://www.warwithoutwitness.com" target="_blank">www.warwithoutwitness.com</a> and <a href="http://www.guerrillanews.com/headlines/20130/Killings_and_Concentration_Camps_in_Sri_Lanka_International_Community_Silent" target="_blank">www.guerrillanews.com/headlines/20130/Killings_and_Concentration_Camps_in_Sri_Lanka_International_Community_Silent</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>ceylon</span>, <span style='color:#777777; font-size:10px'>chemical weapons</span>, <span style='color:#777777; font-size:10px'>civil war</span>, <span style='color:#777777; font-size:10px'>sinhala</span>, <span style='color:#777777; font-size:10px'>sri lanka</span>, <span style='color:#777777; font-size:10px'>tamil</span>, <span style='color:#777777; font-size:10px'>tamil tigers</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(284);
			
				if (SHOWMESSAGES)	{
				
						addMessage(284);
						echo getMessages(284);
						echo showAddMessage(284);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>