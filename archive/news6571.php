<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 657 - 28th November 2008 - Frozen Assets</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, reykjavik, iceland, financial crash, credit crunch, imf, international monetary foundation, baugur group" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://veggieclimatemarch.50webs.com/"><img 
						src="../images_main/vegan-climate-march-banner.jpg"
						alt="Vegan Climate March, Dec 6th 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 657 Articles: </b>
<b>
<p><a href="../archive/news6571.php">Frozen Assets</a></p>
</b>

<p><a href="../archive/news6572.php">Radio Active</a></p>

<p><a href="../archive/news6573.php">Agenda Bender</a></p>

<p><a href="../archive/news6574.php">Free Wheelers</a></p>

<p><a href="../archive/news6575.php">Pointing The Minga</a></p>

<p><a href="../archive/news6576.php">Bruges Crew</a></p>

<p><a href="../archive/news6577.php">Dedicated Followers Of Fascism</a></p>

<p><a href="../archive/news6578.php">Schnews In Brief</a></p>

<p><a href="../archive/news6579.php">657 And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/657-iceland-lg.jpg" target="_blank">
													<img src="../images/657-iceland-sm.jpg" alt="ICELAND BURNS!" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 28th November 2008 | Issue 657</b></p>

<p><a href="news657.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>FROZEN ASSETS</H3>

<p>
<b>THE STREETS OF REYKJAVIK HOT UP AS FINANCIAL MELTDOWN HITS ICELAND</b> <br />
 <br />
&#8220;<i>Even on TV and radio phone-ins callers are saying that maybe anarchy isn&#8217;t such a bad idea after all,</i>&#8221; Siggi P�nk, lead vocalist of Icelandic punk band D�s explains, &#8220;<i>people are saying capitalism in Iceland is dead, kill capitalism.</i>&#8221; <br />
 <br />
The crisis facing the global economy is particularly acute in Iceland. Before the credit crunch Icelanders were, per capita, the world&#8217;s fifth richest population. According to the UN Iceland is the most developed country in the world (whatever that means). Unemployment and homelessness was virtually unknown; the island of just over 300,000 people was one of the greatest success stories of neo-liberalism. Now, precisely because of the Thatcher inspired &#8216;good years&#8217;, the odds are strong on it being the first &#8216;developed&#8217; country to collapse into full economic depression. <br />
 <br />
So it&#8217;s fair to say that Icelanders are mildly angry. There&#8217;s been an upsurge in grassroots mobilisation as the scale of the damage to the economy has become apparent. Approximately 9,000 people (3% of the population) hit the streets in the last weekly anti-government-bank-billionaire demonstration. Open forums discussing the current situation are packed out  - and politicians aren&#8217;t allowed through the door - unless they&#8217;re ordered to attend. New political groups are everywhere - 500 people even physically attacked Reykjavik&#8217;s main Police station and broke out a political prisoner. The collapse of capitalism in Iceland seems to have re-awakened the island&#8217;s traditional spirit of independence.  <br />
 <br />
Precisely who caused this crash is the main unanswered question. What is perfectly clear is that it was the governments free market economic policy in the 80&#8217;s and 90&#8217;s which caused the nosedive. The Independence Party has ruled in every government since Iceland won it&#8217;s sovereignty in 1944. Now, for the first time, it has had to hire private bodyguards for it&#8217;s members, not least for Dav�� Oddson (Mayor of Reykjavik 1982-1991; Prime Minister 1991-2005; Governor of Central Bank 2005-present), its most vigorous exponent of neo-liberal policies.  <br />
 <br />
Unlike other Scandinavian countries, Iceland closely followed a Thatcherite route of privatising all its major industries and banks; slashing or abolishing corporate, inheritance, net wealth, income and company turnover taxes; signing National Accords (suicide pacts) with trade unions. The right-wing thinktank Economic Freedom of the World's annual survey rated Iceland 53rd of the 72 'free-est' (read deregulated) economies in 1975, while it was one of the poorest countries in Western Europe; by 2006 it had risen to be the 12th out of 141 and was amongst the richest on the planet. (Britain went from 23rd to 5th in the same period). <br />
  <br />
How does being 'economically free' translate into being ridiculously rich? Any fishmonger will tell you that no matter how many tax cuts you get, selling fish, Iceland&#8217;s main export, hardly makes you a millionaire. Instead their wealth came from debt. Icelandic bankers stuck their interest rates high so that foreign investors would leave their money with them. They then went on spending sprees across the world, especially in the UK and Denmark. Amongst the UK companies bought by the Icelandic Baugur group were Topshop, Debenhams, Iceland (the supermarket, for novelty value) and the biggest toy shop in Europe - Hamleys (well if you&#8217;re a billionaire you can&#8217;t just buy your kid an action man). Iceland&#8217;s glorious wealth however, was based on borrowed money. <br />
 <br />
Then the credit crunch bit the world, investors wanted their money back and it became blindingly obvious that the Icelandic bankers couldn&#8217;t pay. When all three of its main banks collapsed and were nationalised in the beginning of October (keeping most of the same management, of course), it was revealed that Iceland owed foreign investors $60 billion, 12 times their GDP! But, as these banks were now owned by the nation, it fell on the population as a whole, not the bankers and billionaires who&#8217;d taken out these loans, to repay. That&#8217;s a debt of about $100,000 per person! And of course, with foreign debts there&#8217;s interest and exchange rates to add on. The children of todays Icelanders will still be facing the hangover of the Good Years. <br />
 <br />
<b>SLUMS GONE TO ICELAND </b> <br />
 <br />
Out of the $11 billion package to &#8216;rescue&#8217; its banks, the Icelandic government signed a $2.1 billion &#8220;International Monetary Foundation (IMF) conditionality agreement&#8221;. In Godfather like tones the IMF once described themselves as &#8220;the credit community&#8217;s enforcer.&#8221; Whereas the logic of investment is usually based on getting money for the risks you take, just like betting on Kazakhstan winning the World Cup would make you rich if they came through but would hit you in the face if they didn&#8217;t; the IMF guarantees that it will get its investment back regardless, like someone who bets on Kazakhstan and then points a gun at the bookie to demand their &#8216;winnings&#8217; when they lose. <br />
 <br />
The IMF has ruined countless poorer countries with its debt-enforcement, forcing countries to repay loans by raising interest rates; increasing tax whilst reducing public spending; selling off public industries and property; removing restrictions on the movement of money; taking out more loans...and so on. In short it&#8217;ll be business as usual for the current Icelandic government, which is probably why their only comments on the conditions for the loan (there are always conditions to IMF loans) were that they wouldn&#8217;t have to do anything they wouldn&#8217;t have needed to do anyway. Meanwhile they raised interest rates to 18% - step one of the programme. <br />
 <br />
With billions of pounds of British cash nestling in the now decimated Icelandic banks the government has frozen assets using anti-terrorism laws. The Icelandic government was distinctly unimpressed, but has bigger issues to deal with on the home front. <br />
 <br />
<b>ICE BREAKER</b> <br />
 <br />
The movement that&#8217;s erupted in response to all of this has some unifying demands. 1) Immediate elections - for individuals instead of parties. 2) The entire Icelandic elite be jailed and forced to pay their debts themselves. 3) The entire finance regulatory organisation be replaced, including Davi� Oddson, now the most hated man in over a millennium of history. 4) That nobody should lose their home - after Christmas unemployment is predicted to jump to 7% and you can bet that under the gaze of the IMF the already inadequate benefits will be cut. <br />
 <br />
One shred of good news is that the last 15 years of ecological devastation on the island is grinding to a halt. There&#8217;s simply no money to build more mega-dams, geothermal energy plants and aluminum smelters, especially as the aluminum industry itself has gone belly up from the credit crisis. <br />
 <br />
On a recent demonstration someone climbed on top of the Icelandic parliament and hoisted the symbolic flag of the billionaire Baugur Group, a grinning fat pink piggybank - the logo of their supermarket B�nus. The idea being that Iceland&#8217;s recent and hard won parliament was nothing more than an office for the arrogant super-rich and so it should fly its own flag. But while on a university trip to Parliament two weeks later, the same protester was spotted by an MP who got him nicked and locked up for an action against the aluminum industry that he participated in two years ago. <br />
 <br />
Word spread and 500 people turned up outside the police station, at first demanding his release and then forcing their way in, lobbing stones at the building&#8217;s windows and ramming its doors down with poles. Police responded with teargas. Someone paid for his release because a giant group of riot cops were forming behind the station to attack supporters. Once out of nick, the protester gave a speech to the cheering crowd, shouting &#8220;<i>Such unity and power that you were upholding in front of this station should not be focused on getting some punk out of a prison cell. I&#8217;d rather you used this energy to bring the government to their knees. Launch a complete and general and immediate revolution!</i>&#8221; <br />
 <br />
All this, and yet, the effects of the crisis have yet to even set in&#8230;
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=credit+crunch&source=657">credit crunch</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(145);
			
				if (SHOWMESSAGES)	{
				
						addMessage(145);
						echo getMessages(145);
						echo showAddMessage(145);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>