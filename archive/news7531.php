<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 753 - 7th January 2011 - CSI Palestine</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, palestine, arms trade, israel" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 753 Articles: </b>
<b>
<p><a href="../archive/news7531.php">Csi Palestine</a></p>
</b>

<p><a href="../archive/news7532.php">Reading And Rioting</a></p>

<p><a href="../archive/news7533.php">In Pod We Trust</a></p>

<p><a href="../archive/news7534.php">Lean Dean Fighting Machine</a></p>

<p><a href="../archive/news7535.php">Bolivia: Losing The Morales Highground </a></p>

<p><a href="../archive/news7536.php">Getting Off Soot Free</a></p>

<p><a href="../archive/news7537.php">Holloway Hootenanny</a></p>

<p><a href="../archive/news7538.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/753-fireworks-lg.jpg" target="_blank">
													<img src="../images/753-fireworks-sm.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 7th January 2011 | Issue 753</b></p>

<p><a href="news753.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CSI PALESTINE</h3>

<p>
<p>
	<strong>AS SCHEWS ANALYSES THE USE OF CS GAS BY ISRAELI FORCES</strong> </p>
<p>
	The tragic death of an unarmed woman in Palestine has lead to a global cry for action against the increasing use of &lsquo;non-violent&rsquo; weapons in the continued repression of the Palestinian people. </p>
<p>
	Jawaher Abu Rahma died after inhaling the supposed &lsquo;non-lethal&rsquo; fumes of a CS gas canister, a substance which has been banned in the UK since 1964 due its capacity to kill hours after inhalation. Last Friday (31st) she was part of the weekly protest in Bil&rsquo;in, a village which has become the leading symbol of resistance against the Israeli Occupation. According to Jawaher&rsquo;s mother Subhiyeh who was with her at the time, &ldquo;<em>We weren&rsquo;t even very close to them and the soldiers fired tear gas at us&hellip; Jawaher told me that her chest hurt and she couldn&rsquo;t breathe. Then she fell down and started vomiting</em>.&rdquo; </p>
<p>
	There are numerous inconsistencies between the reports of eyewitnesses and the &lsquo;official&rsquo; line on her death. Claims made by the authorities range from stating Abu Rahma had luekemia and died at home after leaving hospital, to saying she may not have even been at the demonstration. Army sources have also claimed that gas was fired only after the crowd started throwing stones and acting aggressively. All of these statements have been disputed by Abu Rahma&rsquo;s friends, relatives and the ambulance staff who took her to hospital. </p>
<p>
	<strong>AT THE CRIME SCENE</strong> </p>
<p>
	Around 1,000 Palestinians, Israelis and foreigners were demonstrating in Bil&rsquo;in that day against Israel&rsquo;s construction of a wall through village land, separating residents from their livelihoods in a violation of international law. The community has been fighting an ongoing campaign against the building of the barrier, dubbed the apartheid wall (see <a href='../archive/news544.htm'>SchNEWS 544</a>) on their land, separating them from the olive groves that villagers have tended for generations. They succeeded in making the Israeli army announce they would re-route the wall in February 2010, and their actions and public statements have been used as the foundation for communities fighting the Israeli state-led injustices around the world. </p>
<p>
	Following Abu Rahma&rsquo;s death on Saturday (1st), a hastily organised demo took place that evening in Tel Aviv, attended by over 200 mourners and activists. Outside the Ministry of Defence, the protesters blocked traffic on one of the city&rsquo;s main highways, chanting slogans and holding pictures of Abu Rahma. Police charged the crowd and began to violently arrest demonstrators, carrying off a total of eight people, including a member of the Knesset, the Israeli parliament. </p>
<p>
	The remaining protesters held up traffic for a further 90 minutes, sitting down in the road, before moving to the police station where the arrestees were being held. </p>
<p>
	On the same night, a group of around 25 activists journeyed to the American ambassador&rsquo;s home in Tel Aviv to return discarded tear gas canisters. It is an American arms manufacturer, Combined Systems Inc.(CSI), who produces and supplies the long-range canisters to the Israeli forces. Marching through the ambassador&rsquo;s neighbourhood chanting slogans and talking with passersby, they congregated outside the residence to return canisters collected from Bil&rsquo;in. <br /> 
	&nbsp;Five were arrested and detained, and have since been charged with illegal demonstration and resisting arrest. Shortly after the arrests, the authorities claimed that some of the canisters were &lsquo;live&rsquo;, and charged the protesters were with attacking the US ambassador&rsquo;s home. The action was one of the first by Israeli activists to be directed at a foreign government&rsquo;s involvement in the conflict. </p>
<p>
	<strong>WORKING HOMICIDE</strong> </p>
<p>
	The type of gas canister which proved fatal for Jawaher Abu Rahm has killed at least two others since 2009. An 18 month-old toddler choked to death after a canister was fired into his home while he slept, and in a tragic twist of fate, Jawaher Abu Rahma&rsquo;s younger brother, Basem, was killed in April 2009 after being shot in the chest by one of the projectiles (See <a href='../archive/news673.htm'>SchNEWS 673</a>). </p>
<p>
	Hundreds of others have been injured, including US activist Tristan Anderson who was left severely brain damaged after being hit with a canister in March 2009 (See <a href='../archive/news669.htm'>SchNEWS 669</a>). Another US activist, Emily Henochowicz, lost an eye after being hit in the face in a demonstration following the attack of the Gaza flotilla in November 2010, and in the same month a young </p>
<p>
	Palestinian had to have reconstructive surgery on his leg after the bone was shattered by a canister launched by Israeli troops. There are countless reports of the armed forces aiming directly at people in crowds, rather than using them to simply encourage crowd dispersal. </p>
<p>
	The extended range canisters are shot from a rifle and are designed to break through barriers and release gas on contact with the ground. They are equipped with a propeller to send them over 400 metres through the air and have no gas trail when launched, making them extremely difficult for protesters to spot before they hit. Following the injury of Tristan Anderson, the Israeli Defence Forces banned the use of the projectile, a decision which was revoked just a few months later after police on a training exercise complained that being forced to use the shorter-range canisters placed them at risk from demonstrators throwing stones. </p>
<p>
	<strong>LAB RESULTS NEGATIVE</strong> </p>
<p>
	The CSI canisters are part of an increasing arsenal of &lsquo;non-lethal&rsquo; or &lsquo;less-lethal&rsquo; weapons which are being used against peaceful, unarmed demonstrators in Israel, Gaza, and the West Bank. Other weapons include rubber bullets which have been lethal in a number of cases and &lsquo;skunk water&rsquo;, a foul smelling liquid comprised of an unknown cocktail of chemicals that can be sprayed over a large area and last for weeks, making entire villages uninhabitable. The use of the Palestinian people as guinea pigs for a whole host of new forms of violent attack has been globally condemned. In a repeated cycle of the war machine, new weapons are developed, sold to Israel, trialled in the &lsquo;testing grounds&rsquo; of Gaza and the West Bank, and then mercilessly deployed in war zones across the planet. </p>
<p>
	A call out for action has been made to target CSI and also BAE Systems, who manufacture tear gas candles through a division of their company, USA-based Defense Technologies. <a href="http://</p>
<p>
	*www.palsolidarity.org" target="_blank"></p>
<p>
	*www.palsolidarity.org</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1048), 122); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1048);

				if (SHOWMESSAGES)	{

						addMessage(1048);
						echo getMessages(1048);
						echo showAddMessage(1048);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


