<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 726 - 11th June 2010 - For Who's Benefit?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, benefits, welfare state" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 726 Articles: </b>
<p><a href="../archive/news7261.php">Slick Publicity</a></p>

<p><a href="../archive/news7262.php">And So Itt Begins</a></p>

<p><a href="../archive/news7263.php">Calais Eviction Alert</a></p>

<b>
<p><a href="../archive/news7264.php">For Who's Benefit?</a></p>
</b>

<p><a href="../archive/news7265.php">Chilli Reception</a></p>

<p><a href="../archive/news7266.php">Hell And High Water</a></p>

<p><a href="../archive/news7267.php">Striking Blow</a></p>

<p><a href="../archive/news7268.php">Blowing A Raspberry</a></p>

<p><a href="../archive/news7269.php">Gas-tly Business</a></p>

<p><a href="../archive/news72610.php">Edl Offside In Cardiff</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th June 2010 | Issue 726</b></p>

<p><a href="news726.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FOR WHO'S BENEFIT?</h3>

<p>
Next Wednesday (16th) sees the first national day of action against the previous government&rsquo;s flagship scheme to attack the unemployed, the Flexible New Deal (FND). Another word for this is &lsquo;workfare&rsquo; &ndash; working for your benefits. Everyone on the scheme after six months has to work one month for their benefits &ndash; forcing them to do over thirty hours a week at a maximum of &pound;1.60 an hour (less than a third of the minimum wage), without normal labour rights. Workfare schemes like the FND don&rsquo;t create new jobs (apart from in the companies that run it!), they bring down wages and attack working conditions. <br /> 
	 <br /> 
	This was part of the first phase of FND with the second phase due to be rolled out in October &ndash; but now the Tories are in that&rsquo;s all going to phased out and something very similar called &lsquo;work programme&rsquo; brought in. FND will still run until next summer, and be piloted for six months in some areas. The &lsquo;work programme&rsquo; is sure to involve workfare and will no doubt involve most of the same companies like A4E, Seetec, Serco and Working Links bidding for the contracts. <br /> 
	 <br /> 
	Groups all over the country involved in the No to Welfare Abolition network will be taking action on June 16th against the private companies who run this scheme to let them know that they will have a hard time making money from the FND and whatever replaces it. There will be demos in Brighton, Cambridge, Edinburgh, Manchester, Nottingham, Sheffield and Sunderland amongst others. <br /> 
	 <br /> 
	Recently Brighton Benefits Campaign have organised successful pickets outside the offices of the companies profiting from the FND in Brighton (see <a href='../archive/news715.htm'>SchNEWS 715</a>): Maximus, Skills Training UK and Careers Development Group. Unsurprisingly they don&rsquo;t like the negative publicity this attracts because they know how fragile their operating margins are and what a shit service they provide. With payments based on performance these companies are vulnerable to attack. In Brighton meet 11am on June 16th outside Boots, Clocktower. <br /> 
	 <br /> 
	* See <a href="http://www.defendwelfare.org" target="_blank">www.defendwelfare.org</a> and <a href="http://brightonbenefitscampaign.wordpress.com" target="_blank">http://brightonbenefitscampaign.wordpress.com</a> <br /> 
	 <br /> 
	&nbsp; <br /> 
	 <br /> 
	&nbsp; <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>benefits</span>, <a href="../keywordSearch/?keyword=welfare+state&source=726">welfare state</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(810);

				if (SHOWMESSAGES)	{

						addMessage(810);
						echo getMessages(810);
						echo showAddMessage(810);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


