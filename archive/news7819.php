<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 781 - 29th July 2011 - Vedenta: Tin Foiled</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, india, mining" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 781 Articles: </b>
<p><a href="../archive/news7811.php">In League With The Devil?</a></p>

<p><a href="../archive/news7812.php">Prison Hunger Strikes</a></p>

<p><a href="../archive/news7813.php">Not So Neat</a></p>

<p><a href="../archive/news7814.php">Stirchley Speaking</a></p>

<p><a href="../archive/news7815.php">Targeting Refugees</a></p>

<p><a href="../archive/news7816.php">Is-raeli Expensive Here!</a></p>

<p><a href="../archive/news7817.php">Shelling Out</a></p>

<p><a href="../archive/news7818.php">Sea Shepherd: Not Fareoes</a></p>

<b>
<p><a href="../archive/news7819.php">Vedenta: Tin Foiled</a></p>
</b>

<p><a href="../archive/news78110.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th July 2011 | Issue 781</b></p>

<p><a href="news781.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>VEDENTA: TIN FOILED</h3>

<p>
<p>  
  	There were protests outside the AGM of FTSE 100 listed mining company Vedanta last Wednesday. Survival International joined forces with the newly formed Foil Vedanta to demonstrate their anger at the swathe of destruction wrought in India by the company. Although they&rsquo;ve been responsible for pollution and health and safety disasters across the sub-continent, the focus of protests was the land-grab in the Niyamgiri Hills in the Eastern Indian state of Orissa.  </p>  
   <p>  
  	The Niyamgiri Hills are a rich source of bauxite, (aluminium ore), but they&rsquo;re also sacred to the Dongria Kondh people, 8000 of whom live scattered across the landscape. They recently won a judicial victory over Vedanta&rsquo;s attempts to strip-mine their sacred mountain, however the Indian Supreme Court, wheels no doubt nicely greased, is due to reconsider the decision. Not all the struggle has been in the courtroom though &ndash; there has been sporadic fighting and attacks on Vedanta vehicles.  </p>  
   <p>  
  	One Dongria said &ldquo;Mining only makes profit for the rich. We will become beggars if the company destroys our mountain and our forest so that they can make money. We don&rsquo;t want the mine or any help at all from the company.&rdquo;  </p>  
   <p>  
  	Vedanta was founded by and is 63% owned by Anil Agarwil, the 17th richest man in the U.K. He had the pleasure of an impromptu demo outside his house on the day of the AGM with people screaming &ldquo;Anil Agarwil &ndash; blood on your hands&rdquo;. According to one black-clad eyewitness - &ldquo;Virtually every question asked in the AGM concerned human rights &ndash; but no-one was thrown out, it&rsquo;s clear they&rsquo;re trying to cover up with a charm offensive&rdquo;.  </p>  
   <p>  
  	*For more <a href="http://www.survivalinternational.org" target="_blank">http://www.survivalinternational.org</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1309), 150); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1309);

				if (SHOWMESSAGES)	{

						addMessage(1309);
						echo getMessages(1309);
						echo showAddMessage(1309);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


