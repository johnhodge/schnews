<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 705 - 15th January 2010 - Wild At Heart</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, arms trade, direct action, gaza" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 705 Articles: </b>
<b>
<p><a href="../archive/news7051.php">Wild At Heart</a></p>
</b>

<p><a href="../archive/news7052.php">Ships In The Fight</a></p>

<p><a href="../archive/news7053.php">Pyramid Schemers</a></p>

<p><a href="../archive/news7054.php">Gaza Arrests Witness Call Out</a></p>

<p><a href="../archive/news7055.php">Funky Gibbons</a></p>

<p><a href="../archive/news7056.php">Frisky Business</a></p>

<p><a href="../archive/news7057.php">Xmas-size Attack</a></p>

<p><a href="../archive/news7058.php">Bough Down</a></p>

<p><a href="../archive/news7059.php">Cat On A Squat Tin Roof</a></p>

<p><a href="../archive/news70510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/705-wild-at-heart-lg.jpg" target="_blank">
													<img src="../images/705-wild-at-heart-sm.jpg" alt="Wild at Heart" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 15th January 2010 | Issue 705</b></p>

<p><a href="news705.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WILD AT HEART</h3>

<p>
<strong>AS SMASH EDO ANNOUNCES THE LOCATION FOR MONDAY&rsquo;S REMEMBER GAZA DEMO...</strong> <br />  <br /> <strong>Our comrades at SMASH EDO have announced the location of their Remember Gaza demo.</strong> <br />  <br /> It&rsquo;s going to be in Wild Park, just outside the city centre and very near to EDO/ITT factory itself. <br />  <br /> Wild Park isn&rsquo;t difficult to find, just check out <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> for full directions. <br /> The demo is occurring virtually on the anniversary of the Decommissioning (see <a href='../archive/news663.htm'>SchNEWS 663</a>) when six activists broke into the factory and wrought havoc with hammers to as one of them Elijah Smith put it  &ldquo;<em>smash it up to the best of my ability so that it cannot actually work or produce munitions... which have been provided to the Israeli army so that they can kill children</em>.&rdquo; <br />  <br /> Once again the demo hasn&rsquo;t been negotiated with police (and anyone in any doubts as to why that is should check out our very own - On the Verge, which documents Sussex Police&rsquo;s efforts to shut down the campaign). <br />   <br /> As Smash EDO put it themselves - "<em>For three weeks in January 2009, the bombs rained down on Gaza. At the end of Israel&rsquo;s brutal bombing campaign and ground offensive over 1400 Palestinians had been murdered, including 314 children</em>. <br /> &nbsp; <br /> <em>Here in Brighton EDO MBM/ITT manufacture some of the weapons components that devastated so many lives. All over the world thousands of people watched appalled at the carnage on the streets of Gaza. Thousands marched and raged at the destruction of peoples&rsquo; homes and lives. On January 18th 2010, the anniversary of the final day of Operation Cast Lead, we will come together to remember the people of Gaza. <br />   <br /> We will not allow those who supported their pain and profited from their suffering to go unchallenged. We will not let this genocide be forgotten. On the first anniversary after their deaths, we will rise up. We will take to the streets. We will remembe</em>r&rdquo; <br />  <br /> <strong>SMASH EDO REMEMBER GAZA 1P.M WILD PARK, BRIGHTON</strong> 		 <br /> For more see <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=arms+trade&source=705">arms trade</a>, <a href="../keywordSearch/?keyword=direct+action&source=705">direct action</a>, <a href="../keywordSearch/?keyword=gaza&source=705">gaza</a>, <a href="../keywordSearch/?keyword=smash+edo&source=705">smash edo</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(616);
			
				if (SHOWMESSAGES)	{
				
						addMessage(616);
						echo getMessages(616);
						echo showAddMessage(616);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


