<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 712 - 5th March 2010 - School Of Hard Knocks</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, direct action, occupation, students, sussex university" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<?php

				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";

			?>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 712 Articles: </b>
<b>
<p><a href="../archive/news7121.php">School Of Hard Knocks</a></p>
</b>

<p><a href="../archive/news7122.php">Honduras: Fnr-i-p</a></p>

<p><a href="../archive/news7123.php">Headless Morse-man</a></p>

<p><a href="../archive/news7124.php">Migrant Strikes</a></p>

<p><a href="../archive/news7125.php">Edo: Sitting On Defence</a></p>

<p><a href="../archive/news7126.php">Calais: A Safe Harbour?</a></p>

<p><a href="../archive/news7127.php">Ukba'stards</a></p>

<p><a href="../archive/news7128.php">A Different Bail Game</a></p>

<p><a href="../archive/news7129.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/712-sussexUni-lg.jpg" target="_blank">
													<img src="../images/712-sussexUni-sm.jpg" alt="Sussex Uni students demand a much higher education...." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th March 2010 | Issue 712</b></p>

<p><a href="news712.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SCHOOL OF HARD KNOCKS</h3>

<p>
<p class="MsoNormal"><strong>AS SUSSEX STUDENTS TAKE ON UNIVERSITY CUT BACKS...</strong> <br />  <br /> Students at Sussex University came into violent conflict with riot cops on Wednesday (3rd) as they occupied the executive nerve centre of their university. Several weeks in the planning by Stop the Cuts campaign, around 80 students rushed the &lsquo;fortified&rsquo; Sussex House building with a supporting demo outside of around 300 people, all in protest against the proposed cuts to university funding. The occupation was part of a national day of action, called against &pound;950m of government cuts to higher education announced on February 1st. There were also actions in Norwich, Leeds and London. <br />  <br /> The day started at 12pm with an anti-cuts carnival in the main square of the campus, with demonstrators togged up in animal masks &ndash; a nod to the last occupation of Sussex House after which senior management branded students as behaving like &lsquo;animals&rsquo; (an occupation which also lead to Sussex House being made &lsquo;occupation proof&rsquo; with a multitude of complex security systems). Students then rushed the building, managing to occupy the floor that had all of the Vice Chancellor Executive Group offices. <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/712-sussex-uni-fight-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/712-sussex-uni-fight-sm.jpg"  /></a></td></tr></table>Cops arrived on the scene within 15 minutes, complete with dogs, riot gear and an Evidence Gathering unit. Cue a mad rush of bodies, and police ended up being kept at bay with critical mass. In the words of one participant, &ldquo;<em>F*cking hell &ndash; it&rsquo;s all batons and dogs here</em>&rdquo;. <br />  <br /> Inside, most staff left of their own accord, although the Head of Security, Roger Morgan (named by our source as a &lsquo;complete c**t&rsquo;) and another uni bigwig locked themselves in an office and refused to leave without a police escort as they were &lsquo;too intimidated&rsquo; to leave (as opposed to administrators and secretaries who seemed fully able to muster the courage to walk down a flight of stairs). <br />  <br /> While this was going on protesters managed to find some very juicy documents including some dodgy expense records (like a &pound;75 dinner and holiday flights for execs&rsquo; wives and an &pound;1800 hotel bill), and a letter from Peter Mandelson to the Vice Chancellor about the rise of &lsquo;domestic extremism&rsquo; in the university. Copies have been taken and should be released into the public domain soon. <br />  <br /> Halfway through the occupation activists attempted to rush the building and break through the lines of police &ndash; an action that resulted in batons being pulled out and things turning nasty &ndash; two arrests were made on suspicion of assaulting a police officer and both are still custody. <br />  <br /> After the failed rush, a vote was held inside the building about whether the occupiers should continue or leave voluntarily, resulting in a majority ballot to go whilst still having the upper hand. The SWP opened the doors and the students left. <br />  <br /> The demo was held in solidarity with ATL (Teachers&rsquo; and Lecturers&rsquo; Union) that had voted 75% in favour of strike action the same day. One source called the action he had seen &ldquo;the most effective bit of direct action seen on campus.&rdquo; <br />   <br /> <strong></strong><table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/712-sussex-uni-flag-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/712-sussex-uni-flag-sm.jpg"  /></a></td></tr></table><strong>UNIVERSITY CHALLENGED</strong> <br />  <br /> Current plans by University senior management include massive job losses and entire areas are facing the axe, despite committing themselves to spending &pound;112 million on new buildings (not to mention pay hikes for the senior management). The university found itself with a &pound;3 million shortfall, which it is trying to rectify by wildly slashing any area of the university that doesn&rsquo;t fit with its &lsquo;New Strategic Vision.&rsquo; <br />  <br /> Respected academics are facing forced redundancies and successful degree courses are due to be arbitrarily cancelled if management gets its way. The history department is being &lsquo;made more popular&rsquo; - as the process has been euphemistically described by management blurb. This means cutting all European history pre-1900, and all British history pre-1700. <br />   <br /> Meanwhile degrees in Human Sciences are set to go, and modern languages, engineering, and linguistics are all under threat. Nearly half the informatics dept is set to go, with 14 redundancies in the pipeline. Also at risk is the children&rsquo;s cr&egrave;che, recently rated in an Ofsted inspection as &lsquo;outstanding&rsquo; and currently serves the needs of 62 kids. The university reckons it can save &pound;5million with this act of kiddie (service) snatching. The sexual health centre UNISEX is also set to go too. <br />   <br /> The university, which went on an unsustainable, reckless spending binge during the boom times when rich foreign students were flocking to Blighty to study and government subsidies to higher education seemed unending, has found itself shackled with PFI deals. Rather than respond rationally, it appears that the management has just taken a look at any courses that have been perceived to be un-profitable and taken the knife to them. It&rsquo;s worth noting that neighbouring Brighton University (hardly a hotbed of firebrand radicalism) has traditionally underspent and not been quite so keen on the latest corporate faddishness - and isn&rsquo;t planning any job losses or major cuts. <br />  <br /> <strong>* As well as action in Sussex</strong>, students from the University of Westminster stormed the boardroom of their uni on Monday (1st) and confronted their Vice Chancellor, Geoffrey Pett, about the proposed 10% cuts in jobs. After receiving unsatisfactory answers, they occupied the Vice Chancellor&rsquo;s office to protest against the increasing fees and hefty management salaries, while teaching time decreases and class size grows by the term. See: <a href="http://london.indymedia.org.uk/articles/439" target="_blank">http://london.indymedia.org.uk/articles/439</a> for more info on the Westminster occupation. <br />  <br /> <strong>* To show solidarity with the teachers and lecturers</strong> currently facing hefty job cuts, join the March for Jobs this Saturday (6th) in Brighton. Meet at The Level at 12pm. <br />  <br />
</p>

<div style='border: 2px solid black; padding: 10px'>

<b style='font-size: 14px'>CORRECTION CORNER:</b> As any stalwart class(room) warrior doubtless already knows, it's the UCU (University and College Union) that voted for strike action, not the ATL as reported. The strike/picket is still planned for the 18th of March.
<br /><br />
For more info visit <a href='http://ucu.org.uk' target='_BLANK'>ucu.org.uk</a>

</div>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=712">direct action</a>, <a href="../keywordSearch/?keyword=occupation&source=712">occupation</a>, <a href="../keywordSearch/?keyword=students&source=712">students</a>, <a href="../keywordSearch/?keyword=sussex+university&source=712">sussex university</a></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(679);

				if (SHOWMESSAGES)	{

						addMessage(679);
						echo getMessages(679);
						echo showAddMessage(679);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


