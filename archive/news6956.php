<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 695 - 16th October 2009 - Fash In The Pan</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, english defence league, racism, anti-fascists, manchester, edl, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 695 Articles: </b>
<p><a href="../archive/news6951.php">La La La Bomba</a></p>

<p><a href="../archive/news6952.php">Kicking Up A Stink</a></p>

<p><a href="../archive/news6953.php">Harverster Loons</a></p>

<p><a href="../archive/news6954.php">Quids Pro Quo</a></p>

<p><a href="../archive/news6955.php">Art Attack</a></p>

<b>
<p><a href="../archive/news6956.php">Fash In The Pan</a></p>
</b>

<p><a href="../archive/news6957.php">695 Schnews In Brief</a></p>

<p><a href="../archive/news6958.php">Just Deserts</a></p>

<p><a href="../archive/news6959.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th October 2009 | Issue 695</b></p>

<p><a href="news695.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FASH IN THE PAN</h3>

<p>
SchNEWS was on the scene for the latest attempt by the <strong>English Defence League</strong> (See <a href='../archive/news693.htm'>SchNEWS 693</a>) to bring anti-Muslim extremism onto the streets, this time in Manchester. <br /> Although the EDL&rsquo;s demo wasn&rsquo;t due to start until 5pm, by midday a small knot of the lardy larger louts - brandishing England flags - had gathered near Piccadilly Gardens in the city centre. Surrounded by lines of police they were confronted by a several hundred strong group from the Unite against Fascism counter-demo. <br />  <br /> A game of push and shove followed with the cops eventually using dogs to clear a path for the EDL. Both demos were then cordoned in the centre of Piccadilly Gardens. Factions from both sides avoided the kettle and were moving around the outskirts of the demo. So far, so good but as the afternoon wore on more and more arrived to join the EDL demo. Although they were still outnumbered by counter demonstrators by around three to one, this still made it the EDLs biggest mobilisation yet with around three to four hundred present. <br />  <br /> The stand-off inside Picadilly Gardens lasted &lsquo;til 5pm with crowd surges and occasional bottles being lobbed. Outside the cordon the two groups eyed each other up but there were only occasional scuffles and a lot of strange conversations. One anti-fash protester told SchNEWS, &ldquo;<em>A lot of the EDL types didn&rsquo;t know why they were there &ndash; I heard comments ranging from &lsquo;Islam is a paedophile religion&rsquo; to the old NF slogan &lsquo;There ain&rsquo;t no black in the Union Jack&rsquo; What was most surprising was just how young many of them were, there seemed to be a lot of fifteen year lads out</em>&rdquo;. <br />  <br /> Eventually police marched the EDL back to the train stations. Altogether 48 arrests were made for public order offences. <br />  <br /> * The EDL bandwagon moves to Wales this week with demos in two cities this month &ndash; both to be responded to by counter-demos: The EDL have set up a &lsquo;Welsh Defence League&rsquo; to spread the anti-Muslim ignorance west of the border. This Saturday (17th), they&rsquo;re in Swansea &ndash; with the local community holding a counter-demo, meeting 4pm outside the YMCA, St Helen&rsquo;s Road, Swansea. Then on the 24th it&rsquo;s off to Newport &ndash; meet 1pm at John Frost Square.  <br />  <br /> See <a href="http://www.twitter.com/NewportCAR" target="_blank">www.twitter.com/NewportCAR</a> <a href="http://www.facebook.com/event.php?eid=158100673713&amp;ref=mf" target="_blank">www.facebook.com/event.php?eid=158100673713&amp;ref=mf</a> <br />  <br /> * Further demos on the EDL calender include October 31st in the Leeds city centre at 1pm; followed by Glasgow city centre on November 14th (not yet confirmed), and Nottingham on December 5th - details of counter-demos to follow. <br />  <br /> * For more coverage with footage of last Saturday in Manchester see <a href="http://jasonnparkinson.blogspot.com" target="_blank">http://jasonnparkinson.blogspot.com</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-fascists&source=695">anti-fascists</a>, <a href="../keywordSearch/?keyword=direct+action&source=695">direct action</a>, <span style='color:#777777; font-size:10px'>edl</span>, <a href="../keywordSearch/?keyword=english+defence+league&source=695">english defence league</a>, <a href="../keywordSearch/?keyword=manchester&source=695">manchester</a>, <a href="../keywordSearch/?keyword=racism&source=695">racism</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(531);
			
				if (SHOWMESSAGES)	{
				
						addMessage(531);
						echo getMessages(531);
						echo showAddMessage(531);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>