<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 702 - 4th December 2009 - If You're Not Swiss-ed Off...</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, wto, summit protests, neo-liberalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.climate-justice-action.org/"><img 
						src="../images_main/copenhagen-09-banner.jpg"
						alt="Copenhagen Conference"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 702 Articles: </b>
<b>
<p><a href="../archive/news7021.php">If You're Not Swiss-ed Off...</a></p>
</b>

<p><a href="../archive/news7022.php">Dow And Out In Bhopal</a></p>

<p><a href="../archive/news7023.php">When Chips Were Down</a></p>

<p><a href="../archive/news7024.php">Uni-lateral Decision</a></p>

<p><a href="../archive/news7025.php">Inside Schnews</a></p>

<p><a href="../archive/news7026.php">Dubai Now, Pay Later</a></p>

<p><a href="../archive/news7027.php">De-tension Centre</a></p>

<p><a href="../archive/news7028.php">Schnews In Brief</a></p>

<p><a href="../archive/news7029.php">Raising The Barclays</a></p>

<p><a href="../archive/news70210.php">Dale Farm Alert</a></p>

<p><a href="../archive/news70211.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/702-dubai-pear-shaped-lg.jpg" target="_blank">
													<img src="../images/702-dubai-pear-shaped-sm.jpg" alt="Greetings from Dubai" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 4th December 2009 | Issue 702</b></p>

<p><a href="news702.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IF YOU'RE NOT SWISS-ED OFF...</h3>

<p>
<p class="MsoNormal"><strong>AS THOUSANDS CONGREGATE IN <st1:city w:st="on"><st1:place w:st="on">GENEVA</st1:place></st1:city> TO RESIST WORLD TRADE ORGANISATION MEETING</strong> <br />  <br /> Ten years on from the &lsquo;Seattle Battle&rsquo;, about 5,000 people marched through Geneva for the Anti-WTO protest on the 28th November - and were met with tear gas, grenades and water cannon as they tried to disrupt the latest WTO Ministerial meeting, aimed at &lsquo;reviving&rsquo; world trade. This was part of a week of events, workshops, and demos involving a convergence of farmers, fishermen, unionists, and anarchists group from across Europe. <br />  <br /> Since Seattle shook the pants of the corporate world (See <a href='../archive/news239.htm'>SchNEWS 239</a>, <a href='../archive/news240.htm'>240</a>), the WTO hasn&rsquo;t expanded - a victory of sorts - but it hasn&rsquo;t shown any sign of disbanding yet. The number of people suffering extreme poverty has surged ahead since the organisation was set up and two-thirds of developing countries now import more food than they export. <br />  <br /> But what has changed since Seattle is the fact that populations and governments in the &lsquo;developing world&rsquo; have woken up to the sham of neo-liberalism, and are no longer the push-over the West would like. The Doha Round talks (See <a href='../archive/news332.htm'>SchNEWS 332</a>) are still in deadlock eight years on with countries more prepared to hold-out against US-led proposals. <br />  <br />  And why should they agree to plans designed to open up &lsquo;immature&rsquo; developing economies to explotation by global corporations, cutting tariffs and subsidies to ensure that the resultant &lsquo;level playing field&rsquo; tilts heavily in western economies&rsquo; favour? <br />  <br /> Doha was not even on the official agenda for the Geneva meeting, but was being pushed as an economy boosting measure off the record. <br />    <br /> Protesters descended for their own Geneva Convention, dressed-up to mock the shirts attending the conference. A black bloc threw fire bombs at police and damaged a dozen shopfront businesses, including a bank at Place Bel-Air, a jewelry shop and a hotel on the Quai des Bergues. A number of cars were burned, and three buses and numerous other cars were damaged. Thirty-three were arrested. There were no injuries to police or protesters, although a little 80-year-old lady was pushed over in a melee. <br />   <br /> * See <a href="http://www.anti-omc2009.org/?lang=en" target="_blank">http://www.anti-omc2009.org/?lang=en</a> <br />  <br /> * The climate caravan is leaving Geneva after the WTO protests to arrive in Copenhagen On December 9th for the COP15 Climate Conference - see <a href="http://www.climatecaravan.org" target="_blank">www.climatecaravan.org</a> <br />   <br /> <strong>* This was all just a warm up for next week&rsquo;s Copenhagen biggie</strong> - for a rundown of events see <a href='../archive/news700.htm'>SchNEWS 700</a>, <a href="http://www.klimaforum09.org" target="_blank">www.klimaforum09.org</a> , <a href="http://www.globalclimatecampaign.org" target="_blank">www.globalclimatecampaign.org</a> , <a href="http://www.climate-justice-action.org" target="_blank">www.climate-justice-action.org</a> , <a href="http://www.climatecamp.org.uk&nbsp;" target="_blank">www.climatecamp.org.uk&nbsp;</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>neo-liberalism</span>, <span style='color:#777777; font-size:10px'>summit protests</span>, <span style='color:#777777; font-size:10px'>wto</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(594);
			
				if (SHOWMESSAGES)	{
				
						addMessage(594);
						echo getMessages(594);
						echo showAddMessage(594);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>