<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 690 - 11th September 2009 - Dissin' The Dsei</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, anti-militarism, dsei, direct action, barclays, hsbc, fit team, edo, smash edo, itt, brimar" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 690 Articles: </b>
<b>
<p><a href="../archive/news6901.php">Dissin' The Dsei</a></p>
</b>

<p><a href="../archive/news6902.php">Blades Out</a></p>

<p><a href="../archive/news6903.php">Sent To Coventry</a></p>

<p><a href="../archive/news6904.php">Nuke Kids On The Block</a></p>

<p><a href="../archive/news6905.php">Hacked Off</a></p>

<p><a href="../archive/news6906.php">Room Servers</a></p>

<p><a href="../archive/news6907.php">Draxtic Action</a></p>

<p><a href="../archive/news6908.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/690-dsei-09-lg.jpg" target="_blank">
													<img src="../images/690-dsei-09-sm.jpg" alt="Destroy the Arms Trade" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 11th September 2009 | Issue 690</b></p>

<p><a href="news690.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>DISSIN' THE DSEI</h3>

<p>
<p class="MsoNormal"><strong>DISARM DSEI VS THE CITY &ndash; AS ANTI-ARMS TRADE CAMPAIGNERS CONFRONT THE SUITS....</strong> <br />  <br /> This week, between the 8th and 11th of September the <strong>Defence Services Equipment International Exhibition</strong>, the world&rsquo;s largest arms fair, was held at East London&rsquo;s ExCel Centre (see <a href='../archive/news689.htm'>SchNEWS 689</a>). There have been mass mobilisations against <strong>DSEi</strong> at the ExCel Centre since 1999, the largest being in 2003, and infamously in 2001 on the same day as 9-11. Over the years the Met have rolled out ever more repressive tactics &ndash; kettling, dodgy searches under the Terrorism Act and the usual Met favourite of trumped up arrests. Many oft nicked activists had vowed never to return to a DSEi mobilisation again, however this time round energies were spent in The City - against  investors in the international arms trade rather than the usual pig fest at the ExCel Centre. <br />  <br /> Fears of a continuation of the repressive policing of previous years meant that numbers were down. About 200 people met on Tuesday outside the RBS building in Aldgate and marched into The City - RBS are the biggest financier of the global arms trade. Next stop was Barclays, largest UK arms trade investor, where a banner was hung above London Wall proclaiming that &ldquo;<strong>Barclay Invests in the Arms Trade</strong>&rdquo;, and shoes were hurled Iraqi style at the building in disgust at their complicity in murder. More banner drops followed with HSBC and Legal and General exposed as arms trade investors. <br />  <br /> As the march carried on folks started to feel something was missing - being constantly penned in and prevented from doing what they wanted. A handful of police were present but they were not intervening in the demo and were not filming overtly. This seemed, at first, to leave people feeling uncertain, but not for long. <br />  <br /> When the crowd reached Legal and General, another large investor, messages were pasted to the building, more shoes were thrown and windows rattled. The demo moved to Gresham StreetDisarm  where Lloyds staff, disturbed while eating sandwiches on the steps of the Head Office, were besieged by protesters calling for an end to arms trade investment. <br />   <br /> BT Head office was then invaded by scores of activists carrying megaphones, and a reinforced banner carrying the tell-it-like-it-is message &ldquo;<strong>Bomb Makers and Bankers, Murdering Wankers</strong>&rdquo; - along with a sound system - as they poured through the revolving doors. BT hold &pound;59 million worth of shares in the arms trade. Last stop was AXA, one of the largest institutional investors, which had its door kicked in, windows broken and was pelted with paint bombs. <br />   <br /> Throughout all this the FIT were present. One FIT officer said at the beginning that they planned to &lsquo;do nothing&rsquo; to interfere with the demo. They became increasingly twitchy as windows were smashed and buildings invaded but not a single arrest was made. Something about being at a demo en masse just a few hundred metres away from where their colleagues had publicly beaten to death Ian Tomlinson a few months back must&rsquo;ve spooked them. <br />  <br /> The police have since been on their PR-directed best behaviour of late, part of a media game aimed at keeping the reigns off the police. The result, however, was one of the most successful DSEi mobilisations ever. <br />   <br /> So what next? This year it was clearly the right decision to mobilise in the City but maybe in 2011 a revitalised movement can pose a serious challenge to the delegates at DSEi? <br />  <br /> <strong>BATTLE OF THE BANKS</strong> <br />   <br /> Tuesday&rsquo;s demo was the second mass demo this year to focus on companies investing in the arms trade. Smash EDO&rsquo;s Mayday! Mayday! action saw thousands of people squaring up to Barclays and McDonalds, both investors in EDO-ITT (See <a href='../archive/news675.htm'>SchNEWS 675</a>). Since then anonymous pixies have glued up cash machines and smashed windows of banks investing in the arms trade on dozens of occasions in Brighton,&nbsp;Somerset and London. After Tuesday&rsquo;s demo Sophie Williams, from Disarm DSEi said &ldquo;<em>Any damage caused pales into insignificance in comparison with the destruction UK weapons have caused worldwide. The only way to make these companies divest from the arms trade is to hit them economically, and today&rsquo;s action is part of a growing movement of direct action against the dealers in death and the money behind them</em>.&rdquo; <br />  <br /> <strong><font size="3">DSEi 2009 TIMELINE</font></strong> <br />  <br /> <strong>Monday 7th September</strong> <br />   <br /> Our friends from ITT Corporation organised a warm up conference to DSEi at the QE2 conference centre near Victoria. Before the conference started a campaigner from Catholic Worker daubed &ldquo;Build Peace, Not War Machines&rdquo; across the front door. A handful of protesters turned up and staged an angry and noisy demo outside the centre. Police made a half hearted attempt to move them into a protest pen and made empty threats of arrests under SOCPA. One lone activist shackled himself inside the pen and began a hunger strike for the duration of the fair.&nbsp; <br />  <br /> ** Meanwhile, on the fourth plinth at Trafalgar square an installation called &ldquo;Never mind art - disarm the arms trade!&rdquo; was set up and activists made speeches in support of the EDO decommissioners (See <a href='../archive/news663.htm'>SchNEWS 663</a>). <br />  <br /> <strong>Tuesday 8th September</strong>  <br />  <br /> Disarm DSEi held mass demo in The City (see previous report) ** Two were arrested when Catholic Worker poured red paint over the sign for ExCel and unfurled a banner saying &ldquo;Forgive them father for they know not what they do&rdquo;. ** Critical Mass from the ExCel Centre. ** Campaign Against the Arms Trade (CAAT) held demos at the ExCel Centre and UK Trade and Industry. <br />  <br /> <strong>Wednesday 9th September</strong> <br />  <br /> Activists held a noise demo at Custom House station just outside DSEi and disrupted the DLR.  <br />  <br /> ** CAAT held a walking tour around arms dealers in the City of London.  <br />  <br /> ** Noise demonstration held outside DSEi delegate&rsquo;s hotels. <br />  <br /> <strong>Thursday 10th September</strong> <br />  <br /> An angry noise demo was held outside the doors of the Hilton Hotel in Park Lane where inside the DSEi delegates&rsquo; dinner was being held. Police issued a Section 14 order; people were moved away from the hotel and 10 arrests made for breach of Section 14. Looks like the police had had enough of their softly softly tactics. <br />  <br /> <strong><font size="3">FORTHCOMING ANTI- MILITARIST EVENTS</font> <br />  <br /> </strong><strong>* Support the EDO Decommissioners Week of Events</strong> &ndash; Brighton - 16th-26th October &ndash; see <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <br />  <br /> <strong>* Launch of Target Brimar Campaign</strong> &ndash; October 17th - Brimar is a Manchester based arms company &ndash; see www.targetbrimar.org.uk <br />  <br /> <strong>* Stop the NATO Parliamentary Assembly</strong> &ndash; Edinburgh &ndash; November 13th-17th See <a href="http://natowc.noflag.org.uk" target="_blank">http://natowc.noflag.org.uk</a> <br />  <br /> * For more info on the Anti-Militarist movement see <a href="http://www.antimilitaristnetwork.org.uk" target="_blank">www.antimilitaristnetwork.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>anti-militarism</span>, <a href="../keywordSearch/?keyword=barclays&source=690">barclays</a>, <span style='color:#777777; font-size:10px'>brimar</span>, <a href="../keywordSearch/?keyword=direct+action&source=690">direct action</a>, <a href="../keywordSearch/?keyword=dsei&source=690">dsei</a>, <a href="../keywordSearch/?keyword=edo&source=690">edo</a>, <a href="../keywordSearch/?keyword=fit+team&source=690">fit team</a>, <span style='color:#777777; font-size:10px'>hsbc</span>, <a href="../keywordSearch/?keyword=itt&source=690">itt</a>, <a href="../keywordSearch/?keyword=smash+edo&source=690">smash edo</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(484);
			
				if (SHOWMESSAGES)	{
				
						addMessage(484);
						echo getMessages(484);
						echo showAddMessage(484);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>