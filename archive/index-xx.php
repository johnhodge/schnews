<?php
	require '../storyAdmin/functions.php';
	require '../functions/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS Back Issues - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<META name=description content="Feature article by SchNEWS the free weekly direct action newsheet from Brighton, UK. Going back to issue 100, some available as PDF" />
<META name=keywords content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../archiveIndex.css" type="text/css" />
<link rel="stylesheet" href="../issue_summary.css" type="text/css" />

<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>

</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>

			<?php

				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";

			?>


</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<?php

			//	Include Left Bar
			require '../inc/archiveIndexLeftBar.php';

		?>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->>

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<p><font face="Arial, Helvetica, sans-serif" size="2"><b>
			<a href="../index.htm">Home</a> &#124; BACK ISSUES<br /> </b></font><font face="Arial, Helvetica, sans-serif" size="1">PDF
			(<a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html" target="_blank">Acrobat</a>)
			are available for all these issues. Read the <a href="../pages_menu/pdf_notes.html">notes
			on PDF files</a> for help, especially with printing.</font></p><p><font face="Arial, Helvetica, sans-serif"><a href="../search.htm"><b>SchNEWS
			SEARCH FACILITY</b></a></font></p><h3 align="left"><font face="Arial, Helvetica, sans-serif"><b>Most
			Recent...</b></font></h3>


			<div id="backIssues">

				

<div class="backIssue">
<table width='100%'><tr><td>
<a href="news796.htm"><img src='../images/796-cops-in-tents1-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news796.htm">SchNEWS 796, 11th November 2011</a></b>  <font size="1">Click <a href="pdf/news796.pdf">HERE</a> for PDF version</font>
<br />
<b>Nov 9 Summed Up</b> - 
	Wednesday (9th) saw yet another demonstration against cuts to education and public services, this time organised by the National Campaign Against Fees &amp; Cuts (NCAFC). So SchNEWS donned its best black hoody and attempted to infiltrate the &lsquo;violent minority&rsquo;. Much to our disappointment, by the end of the day, it was clear that the monopoly of violence was still very much in the hands of the state.
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news795.htm"><img src='../images/795-Greece-investors-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news795.htm">SchNEWS 795, 4th November 2011</a></b>  <font size="1">Click <a href="pdf/news795.pdf">HERE</a> for PDF version</font>
<br />
<b>Dedicated Followers of Fascism</b> - 
	On Saturday 29th October 2011 the English Defence League (EDL) paid their third visit to Birmingham. Previous excursions ended in running battles through the streets. The march was the EDL&rsquo;s second big mobilisation this year, although not hyped as much as their September outing in Tower Hamlets....

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news794.htm"><img src='../images/794-stPauls-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news794.htm">SchNEWS 794, 28th October 2011</a></b>  <font size="1">Click <a href="pdf/news794.pdf">HERE</a> for PDF version</font>
<br />
<b>Squatting: Empty Premises</b> - 
	On Wednesday (26th) the government published its response to the squatting consultation. While none of us were expecting anything sensible to come from a Tory government foaming at the mouth at the prospect of kicking an unpopular minority, the speed of the knee-jerk reaction and its implementation is beyond even our cynical predictions.
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news793.htm"><img src='../images/793-lightsabre-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news793.htm">SchNEWS 793, 21st October 2011</a></b>  <font size="1">Click <a href="pdf/news793.pdf">HERE</a> for PDF version</font>
<br />
<b>Dale Farm Evicted</b> - 
	At 7am on Wednesday (19th) morning hundreds of riot police stormed the rear of Dale Farm. They met with stiff resistance on the barricades, resorting to Tasering at least one person as they ran away from them. Eventually breaching the outer wall of the site, police then had to fight their way through several more roving barricades (some aflame) as they made their way to the main gate under a hail of half-bricks.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news792.htm"><img src='../images/792-occupy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news792.htm">SchNEWS 792, 14th October 2011</a></b>  <font size="1">Click <a href="pdf/news792.pdf">HERE</a> for PDF version</font>
<br />
<b>False Profits</b> - 
	We have settled into a continual cycle of panics over the eurozone economic situation. Recent chapters have included Italy and Spain&rsquo;s sovereign debt rating downgrades and Greece going to the brink of defaulting on its loans again &ndash; as it inevitably will without major debt restructuring, c&rsquo;mon people it doesn&rsquo;t take a rocket scientist to figure that one out!.
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news791.htm"><img src='../images/791-iLegacy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news791.htm">SchNEWS 791, 7th October 2011</a></b>  <font size="1">Click <a href="pdf/news791.pdf">HERE</a> for PDF version</font>
<br />
<b>NHS Direct Action</b> - 
	On Sunday, thousands are expected to blockade Westminster Bridge in a &lsquo;sick-in&rsquo; blockade spearheaded by the relentlessly organised UK Uncut. The &ldquo;Block the Bridge, Block the Bill&rdquo; action is taking place before the health and social care (read NHS privatisation) bill goes to the House of Lords on October 11th.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<p align='left'>
<b><a href="news790.htm">SchNEWS 790, 30th September 2011</a></b> 
<br />
<b>Fighting Frack Addiction</b> - 
	Over the weekend of 17-18 September local protesters from Lancashire joined forces with activists from across the country to attend Camp Frack- a two day camp focussed on networking, planning and skill-sharing at a site only a couple of miles away from the cabbage field where Cuadrilla Resources (the company currently leading the field in UK fracking) are conducting test drilling operations in preparation for an onslaught that could see 800 wells drilled in Lancashire alone...

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news789.htm"><img src='../images/789-CLEGG-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news789.htm">SchNEWS 789, 23rd September 2011</a></b>  <font size="1">Click <a href="pdf/news789.pdf">HERE</a> for PDF version</font>
<br />
<b>Flying the Flag</b> - 
	For dropping a banner off a bridge outside the this week&rsquo;s Libdem conference, one protester is now serving 10 days on remand at HMP Birmingham. Ed Bauer and two fellow activists from the&nbsp; National Campaign Against Fees and Cuts (NCARC) unfurled a banner with the straight and to the point message: &ldquo;Traitors Not Welcome: Hate Nick Clegg Love NCAFC&rdquo;.
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news788.htm"><img src='../images/788-battleship-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news788.htm">SchNEWS 788, 16th September 2011</a></b>  <font size="1">Click <a href="pdf/news788.pdf">HERE</a> for PDF version</font>
<br />
<b>World's Largest Arms Fail</b> - 
	This week London&rsquo;s Excel Centre has been hosting the biennial death-fest DSEi &ndash; the world&rsquo;s largest arms fair. Part funded and organised by the British government, DSEi is the place where warmongers tool up on the latest guns &lsquo;n&rsquo; ammo. Both the weapons used by Colonel Gaddafi and those used against him are on sale at DSEi. The UK exported &pound;33.8m of military and related products to Libya between Oct 2009 and Oct 2010.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news787.htm"><img src='../images/787-edl-rabbit-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news787.htm">SchNEWS 787, 9th September 2011</a></b>  <font size="1">Click <a href="pdf/news787.pdf">HERE</a> for PDF version</font>
<br />
<b>Tower to the People</b> - 
	It was going to be the &lsquo;Big One&rsquo;, the EDL&rsquo;s grand day out in the capital city. They proudly proclaimed that on 3rd September they were going into the &ldquo;Lion&rsquo;s Den&rdquo; the &ldquo;Islamic hell-hole&rdquo; &ndash; and what they no doubt had in mind when this hare-brained scheme was first announced was something like an Orange march through London&rsquo;s biggest Muslim community, with drums beating and the chant of&nbsp; &lsquo;E.E.EDL&rsquo; echoing off the walls of the East London Mosque as terrified Muslims fled. What actually happened?

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news786.htm"><img src='../images/786-polly-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news786.htm">SchNEWS 786, 2nd September 2011</a></b>  <font size="1">Click <a href="pdf/news786.pdf">HERE</a> for PDF version</font>
<br />
<b>EDL: It Takes One to E1</b> - 
	Despite Home Secretary Theresa May announcing a ban on all marches in Tower Hamlets, Newham, Waltham Forest, Islington, Hackney and the City of London for 30 days &ndash; the far-right English Defence League (EDL) will still be attempting to stage their biggest demonstration yet, in the heart of London. As anti-fascists, anti-racists, socialists and anarchists we should ensure we are there to greet whoever turns out on 3rd September

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news785.htm"><img src='../images/785-Atos-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news785.htm">SchNEWS 785, 26th August 2011</a></b>  <font size="1">Click <a href="pdf/news785.pdf">HERE</a> for PDF version</font>
<br />
<b>Dale Farm: Fight the Power</b> - 
	As the eviction date for Dale Farm is only a matter of days away, Basildon Council has announced that it will take the draconian step of cutting off all water and electricity to the site once the eviction notice period expires on midnight 31st August. This will leave sick, elderly, young, and pregnant residents without access to water or electricity. Amnesty International have condemned the removal of vital water and electricity in these circumstances, and asked their supporters to put pressure on the council to cease this serious violation of human rights.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news784.htm"><img src='../images/784-blame-parents-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news784.htm">SchNEWS 784, 19th August 2011</a></b>  <font size="1">Click <a href="pdf/news784.pdf">HERE</a> for PDF version</font>
<br />
<b>The U.N. Holy Land</b> - 
	The machine-gunning of an Israeli bus and the subsequent air-raids on Gaza represent another dangerous turn of the screw for Palestine. Anxious to rescue themselves from their own domestic discontents another border war might be just what the Israeli authorities want.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news783.htm"><img src='../images/783-loot-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news783.htm">SchNEWS 783, 12th August 2011</a></b>  <font size="1">Click <a href="pdf/news783.pdf">HERE</a> for PDF version</font>
<br />
<b>The Big Smoke</b> - 
	Across the entire media spectrum the opinions are now flying thicker and faster than the half-bricks were just two nights ago. So here&rsquo;s SchNEWS&rsquo; half-assed crack at explaining the recent turmoil on the streets of the U.K.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news782.htm"><img src='../images/782-police-notice-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news782.htm">SchNEWS 782, 4th August 2011</a></b>  <font size="1">Click <a href="pdf/news782.pdf">HERE</a> for PDF version</font>
<br />
<b>From Cradle to Graves</b> - 
	East Africa is currently experiencing one of the worst famines in recent history, with most of Somalia and large parts of Ethiopia experiencing severe food shortages. The west has been quick to offer the solution &ndash; more aid, more trade and flying in David Cameron.This approach of aid, trade and sending rich white men to point at the locals has been the standard response to African disasters for the past 25 years and yet little progress seems to have been made.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news781.htm"><img src='../images/781-hitler-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news781.htm">SchNEWS 781, 29th July 2011</a></b>  <font size="1">Click <a href="pdf/news781.pdf">HERE</a> for PDF version</font>
<br />
<b>In League with the Devil?</b> - 
	The Norway attacks have led to frenzied speculation on the nature of the crazed ideology that could lead to such horrific acts. Handily Andre Brievik not only allowed himself to be captured but posted a 1500 page manifesto online - grandly entitled 2083 - A European Declaration of Independence , and apart from a load of weird stuff about body-armour, the Knights Templar and the appropriate use of steroids, at the heart is a vision eerily familiar to readers of such fringe esoteric publications&nbsp; as say the Daily Express, Daily Mail or Sun...
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news780.htm"><img src='../images/780-murdochpie-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news780.htm">SchNEWS 780, 22nd July 2011</a></b>  <font size="1">Click <a href="pdf/news780.pdf">HERE</a> for PDF version</font>
<br />
<b>Let Them Eat Lead</b> - 
	Somalia has officially been designated &lsquo;world&rsquo;s worst country to be in&rsquo; by the UN, who declared the Horn of Africa region (Somalia, Eritrea, Ethiopia, Djbouti) a famine zone. The UK&rsquo;s own Disasters Emergency Commission has already started a fund-raising campaign for famine relief.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news779.htm"><img src='../images/779-marry-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news779.htm">SchNEWS 779, 15th July 2011</a></b>  <font size="1">Click <a href="pdf/news779.pdf">HERE</a> for PDF version</font>
<br />
<b>Rolling Out the Unwelcome Mat</b> - 
	The closure of the Immigration Advisory Service on Monday (11th), following new legislation denying legal aid for all immigration cases, has left hundreds of migrants and asylum seekers without legal support. The service had been running for 35 years from London and regional offices, with 300 employees. At the time of the shock closure - which staff discovered via notices on doors when they turned up for work - there were 650 active cases. Claimants are now being told to ask courts for their hearings to be delayed and to find themselves alternative legal representation.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news778.htm"><img src='../images/778-murdoch-beach-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news778.htm">SchNEWS 778, 8th July 2011</a></b>  <font size="1">Click <a href="pdf/news778.pdf">HERE</a> for PDF version</font>
<br />
<b>It's All Over</b> - 
	Yes the world of alternative jounalism was rocked to it&rsquo;s core this week as SchNEWS International&rsquo;s mogul boss Rupert Makepeace announced the sudden closure of the paper. Britain&rsquo;s most well-loved anarcho-newsheet has been serving up it&rsquo;s unpopular brand of poorly researched facts, corporate and political sleaze, eco-terror propaganda and non-gender specific titillation for 168 months, and commentators are still in shock that it will be no more.
	
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news777.htm"><img src='../images/777-Libya-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news777.htm">SchNEWS 777, 1st July 2011</a></b>  <font size="1">Click <a href="pdf/news777.pdf">HERE</a> for PDF version</font>
<br />
<b>Libya: Anti-Nato Classes</b> - 
	The bombs are still falling on Tripoli in yet another British intervention on behalf of Arab human rights in the Middle East (the 46th since 1945). What started out as a U.N backed campaign&nbsp; supposedly to protect anti-Gaddafi elements from a massacre has turned into reckless use of air-power to oust Gadaffi from power. And yet the UK peace movement is virtually nowhere to be seen.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news776.htm"><img src='../images/776-haw-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news776.htm">SchNEWS 776, 24th June 2011</a></b>  <font size="1">Click <a href="pdf/news776.pdf">HERE</a> for PDF version</font>
<br />
<b>Squatting on Heaven's Door</b> - 
	As expected: Following the opportunistic yelps of a jumped-up tory media-whore, against the background of a right-wing ideological crusade, the criminalisation of squatting is now on parliament&rsquo;s agenda. This week the government announced a brief consultation period was under-way on the issue of occupation without authorisation. The sights of the Tory legislative blunderbuss are slowly being zeroed in, battle lines are being drawn and arguments rehearsed. As every day goes by, the corporate press prejudice people further against one of the few remaining laws that empowers the many against the few.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news775.htm"><img src='../images/775-dontwork-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news775.htm">SchNEWS 775, 17th June 2011</a></b>  <font size="1">Click <a href="pdf/news775.pdf">HERE</a> for PDF version</font>
<br />
<b>Flaming June?</b> - 
	Is the big fight on? Union responses to the Tory cuts have so far been fairly muted - the M26 outing, resembling a cross between a family picnic and a Labour Party rally. However on June 30th the largest public sector strikes since the 80s are planned. With the Daily Mail claiming that &lsquo;Union Barons&rsquo; plan to &lsquo;unleash hell&rsquo;, what&rsquo;s actually going to happen?

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news774.htm"><img src='../images/774-poundland-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news774.htm">SchNEWS 774, 10th June 2011</a></b>  <font size="1">Click <a href="pdf/news774.pdf">HERE</a> for PDF version</font>
<br />
<b>Going to Hell-as</b> - 
	Over a thousand migrants have been arrested and made homeless over the last two months after the Greek government launched a sweeping crackdown in the port city of Igoumenitsa. Last month the Greek government announced the scorched earth policy at Igoumenitsa alongside plans to begin the construction of 14 new detention and deportation centres.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news773.htm"><img src='../images/773-homage-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news773.htm">SchNEWS 773, 27th May 2011</a></b> 
<br />
<b>No Spain, No Gain</b> - 
	We&#39;ve got the Arab Spring &ndash; what about a European summer? It&#39;s a week since thousands of pro-democracy demonstrators pitched up in central Madrid - and revolution fever is spreading across Europe like nits in a playground. With the Spanish sit-in still going strong, street demonstrations have also hit Greece, Georgia, and, er, Bristol. Protests are spreading to Italy, France, Portugal, Austria even German - could it be that last year&#39;s initial protests against austerity measures are maturing, one year on, into a broader demand for political reform?

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news772.htm"><img src='../images/772-MrMen-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news772.htm">SchNEWS 772, 20th May 2011</a></b>  <font size="1">Click <a href="pdf/news772.pdf">HERE</a> for PDF version</font>
<br />
<b>A Bit of Hows Yer Intifada?</b> - 
	A mass non-violent (or at least unarmed) resistance movement is on the move in Palestine. Inspired by the events in neighbouring Arab countries but drawing on decades of resistance - the Third Intifada may be here. The waves of the Arab movement are beginning to lap at the Israeli shore.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news771.htm"><img src='../images/771-frak-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news771.htm">SchNEWS 771, 13th May 2011</a></b>  <font size="1">Click <a href="pdf/news771.pdf">HERE</a> for PDF version</font>
<br />
<b>Fracking Hell</b> - 
	A few months since Fukishima, nearly a year since Deepwater Horizon but the global elite aren&rsquo;t resting on their laurels and it looks like we won&rsquo;t have to wait too long for the fossil fuel industry to cause the next environmental apocalypse. New kid on the block is the appropriately named &lsquo;fracking&rsquo; &ndash; or, more explicitly: hydraulic fracturing, a method of extracting natural gas from shale rock layers thousands of feet deep &ndash; and its coming here soon.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news770.htm"><img src='../images/770-sheep-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news770.htm">SchNEWS 770, 6th May 2011</a></b>  <font size="1">Click <a href="pdf/news770.pdf">HERE</a> for PDF version</font>
<br />
<b>Baa Baa Black Block</b> - 
	Operation Brontide swung into full force last week, coincidentally just before the Royal wedding. Cops claim to be after 276 people for offences including violent disorder and criminal damage committed during the March for the Alternative on 26th March. In fact this is&nbsp; a legally dubious fishing exercise designed to seize equipment and display state power.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news769.htm"><img src='../images/769-tes-copshop-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news769.htm">SchNEWS 769, 29th April 2011</a></b>  <font size="1">Click <a href="pdf/news769.pdf">HERE</a> for PDF version</font>
<br />
<b>Stoking the Fires</b> - 
	Last Thursday (21st), the Stokes Croft area of Bristol was the scene of violent clashes between police and protesters of&nbsp; he kind not seen in the city since 1980. The hours of rioting that lasted from 9pm until five or six the following morning were prompted firstly by the eviction of the Telepathic Heights squat, and then tapped into deep anger in the community against the opening of another unwanted Tesco store

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news768.htm"><img src='../images/768-brighton-mayday-150.png' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news768.htm">SchNEWS 768, 22nd April 2011</a></b>  <font size="1">Click <a href="pdf/news768.pdf">HERE</a> for PDF version</font>
<br />
<b>Watch My Lips</b> - 
	A hunger strike by 6 Iranian asylum seekers, five of whom have sewn their mouths shut, is currently in progress in London. Three are camping outside Amnesty International&#39;s offices in Clerkenwell, and three are at the Home Office in Croydon.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news767.htm"><img src='../images/767-Gadaffi-Cameron-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news767.htm">SchNEWS 767, 15th April 2011</a></b>  <font size="1">Click <a href="pdf/news767.pdf">HERE</a> for PDF version</font>
<br />
<b>Taking Libya-Ties</b> - 
	Medyan Daireh, a Brighton-based Al Jazeera journalist who has previously covered conflicts in Iraq, Palestine, Afghanistan, Turkey and Kosovo has just returned from a stint in Libya. He covered the uprising against Colonel Gadaffi, spent weeks living with the rebels and visited all of the rebel-controlled areas. SchNEWS took the opportunity to get his views on the rebels&rsquo; struggle and the situation in Libya:

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news766.htm"><img src='../images/766-teatowel-150.png' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news766.htm">SchNEWS 766, 8th April 2011</a></b>  <font size="1">Click <a href="pdf/news766.pdf">HERE</a> for PDF version</font>
<br />
<b>House Demolition</b> - 
	A police-escorted bulldozer razed the inside of migrant squat Africa House in Calais last Thursday (31st). In the early afternoon, large numbers of police descended on the site, home to tens of African migrants since the first Africa House was demolished last June.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news765.htm"><img src='../images/765-m26-2.gif.100.gif' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news765.htm">SchNEWS 765, 1st April 2011</a></b>  <font size="1">Click <a href="pdf/news765.pdf">HERE</a> for PDF version</font>
<br />
<b>M26: Ritz Spirit</b> - 
	Well - somebody had to do it. The TUC&rsquo;s glad-hand fest was never going to change the ConDEMS course by even one degree. It was hijacked by anarchists and it deserved to be. The TUC mobilised its masses and did everything they could to make sure that genuine dissent didn&rsquo;t rear its ugly head. The emphasis was to be on a family-friendly day out without frightening the horses. Quite who they think is going to hand out prizes for niceness in the shark tank that is global capitalism remains a mystery.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news764.htm"><img src='../images/764-march-26th-150.png' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news764.htm">SchNEWS 764, 25th March 2011</a></b>  <font size="1">Click <a href="pdf/news764.pdf">HERE</a> for PDF version</font>
<br />
<b>No Flies on Us</b> - 
	It&rsquo;s March, so it&rsquo;s time for a new war. To no-one&rsquo;s great surprise we&rsquo;re bombing the Arabs again. A few facts for history buffs - not only did &ldquo;Operation Defend Libyans From Bombs By Bombing Libyans&rdquo; start almost exactly seven years after the start of the Iraq War, but 2011 also marks one hundred years of aerial bombardment. The target of those first ever bombs dropped from planes in 1911? Libya

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news763.htm"><img src='../images/763-nuclearPower-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news763.htm">SchNEWS 763, 18th March 2011</a></b>  <font size="1">Click <a href="pdf/news763.pdf">HERE</a> for PDF version</font>
<br />
<b>Nuclear Power: Going Critical</b> - 
	The terrifying situation in Japan has rekindled the debate over nuclear power around the world. While in the US, Obama has reaffirmed his support for nuclear power to protect the massive investment his administration has made in the industry, over in Germany and Switzerland the governments have jammed the brakes on plans to build and replace nuclear plants. In the UK, David Cameron has already declared his intention to push on with plans to expand the UKs nuclear capacity, with up to eleven new power stations.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news762.htm"><img src='../images/762-wall-st-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news762.htm">SchNEWS 762, 11th March 2011</a></b>  <font size="1">Click <a href="pdf/news762.pdf">HERE</a> for PDF version</font>
<br />
<b>Profit Hungry</b> - 
	If you&rsquo;re the type to buy food, rather than only get it from bins, you&rsquo;ve probably noticed that food prices have been rising again. In fact, the international trading prices of major grains are 70% higher than they were this time last year, and in most cases near or above the levels during the price hikes of 2008. Demonstrations have already taken place this year in Tunisia, Jordan, Algeria and India. Should the problems continue many more can be expected.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news761.htm"><img src='../images/761-climatecamp-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news761.htm">SchNEWS 761, 4th March 2011</a></b>  <font size="1">Click <a href="pdf/news761.pdf">HERE</a> for PDF version</font>
<br />
<b>Penny For The Guy Ropes</b> - 
	They came, they camped, they conquered (well, not quite). During a soul-searching Dorset retreat, Climate Camp have decided to suspend tent-centred activism - citing the &ldquo;radically different political landscape&rdquo; of 2011. Having been through Drax, Kingsnorth, Heathrow, RBS, Copenhagen and one helluva lot of hummus, the group are now turning their attention to coordination with the wider anti-cuts and anti-austerity movement.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news760.htm"><img src='../images/760-cameron-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news760.htm">SchNEWS 760, 25th February 2011</a></b>  <font size="1">Click <a href="pdf/news760.pdf">HERE</a> for PDF version</font>
<br />
<b>Cameron Waves Arms About</b> - 
	As Egypt and its martial law swiftly falls down the mainstream rolling news agenda it has been replaced by even more dramatic events in Libya. While the Libyan people continue their life and death struggle against brutal dictator/deflated blow-up doll Ghaddafi, all these tottering dictatorships are causing a few awkward moments for the government and UK plc.&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news759.htm"><img src='../images/759-egypt-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news759.htm">SchNEWS 759, 18th February 2011</a></b>  <font size="1">Click <a href="pdf/news759.pdf">HERE</a> for PDF version</font>
<br />
<b>Middle Eastern Promise</b> - 
	Egyptian dissidents (along with the masses) celebrated on Friday as Hosni Mubarak finally threw in the towel after the mass protests in Cairo&rsquo;s Tahrir Square and across the country refused to abate (and presumably the Americans finally ordered him to load up the plane with gold and go in an attempt to ensure the power structures &ndash; and thus their influence &ndash; didn&rsquo;t collapse completely). But the Egyptian revolution is not yet won as the military have stepped in, repressed protest and threatened to declare martial law.&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news758.htm"><img src='../images/758-library-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news758.htm">SchNEWS 758, 11th February 2011</a></b>  <font size="1">Click <a href="pdf/news758.pdf">HERE</a> for PDF version</font>
<br />
<b>The Withdrawal Method</b> - 
	As noisy protests continued against tax avoidance by big business and cuts in education and benefits another, altogether quieter, national campaign took off this week at our most unsung of public services: the libraries. You can&rsquo;t imagine that Waterstones, WHSmiths, Amazon and others mind too much that libraries were forged from great social ideals. Not only is encouraging universal education and literacy good econincally for society, it&rsquo;s availability to serve as community hub, public space, creche and more to those without access to alternatives make it an all round force for social good.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news757.htm"><img src='../images/757-green-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news757.htm">SchNEWS 757, 4th February 2011</a></b>  <font size="1">Click <a href="pdf/news757.pdf">HERE</a> for PDF version</font>
<br />
<b>Cut To The Quick...</b> - 
	Thousands of protesters ran the police ragged in a hyperactive day of protest at the latest national demonstration against fees and cuts in London on Saturday (29th). Up to 10,000 people marched through the city towards Parliament Square, taking the route agreed with police. However, the crowd showed little interest in hanging around for speeches at the designated end point and most pushed on towards Milbank Towers &ndash; scene of the birth of the student protest movement in November

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news756.htm"><img src='../images/756-egypt-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news756.htm">SchNEWS 756, 28th January 2011</a></b>  <font size="1">Click <a href="pdf/news756.pdf">HERE</a> for PDF version</font>
<br />
<b>Mubarak's Against the Wall</b> - 
	Since last December Tunisia has been hit by relentless and transformative riots triggered by unemployment, food inflation, lack of freedom of speech and poor living conditions. The violent unrest eventually led to the ousting of President Zine El Abidine Ben Ali, who fled the country on the 14th of January after a hefty 23 years in power. Daily protests have continued due to prominent figures in the Ben Ali regime clinging on to posts in the new interim government.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news755.htm"><img src='../images/755-mark-kennedy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news755.htm">SchNEWS 755, 21st January 2011</a></b>  <font size="1">Click <a href="pdf/news755.pdf">HERE</a> for PDF version</font>
<br />
<b>Inter-NETCU</b> - 
	For the benefit of anyone who&rsquo;s been hiding in a hole wearing a tinfoil hat for the last fortnight (i.e most of our readership), it turns out that the U.K direct action/anarchist/environmental movement was infiltrated for number of years by undercover police. At least four cops have already been outed and its safe to assume there may be more. But while the mainstream media has focussed on the sleazy antics and dodgy love lives of these professional liars, SchNEWS can reveal that police attempts to disrupt our movement goes much further than a few unshaven plants in grubby t-shirts, and includes attacks on activist media and communications.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news754.htm"><img src='../images/754-Tory-tree-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news754.htm">SchNEWS 754, 14th January 2011</a></b>  <font size="1">Click <a href="pdf/news754.pdf">HERE</a> for PDF version</font>
<br />
<b>Fight Them on the Beeches</b> - 
	Last week we covered the protests against the sell-off of the Forest of Dean. But the implications of the Public Bodies Bill for the Forestry Commission&#8239; go a lot further than that.&#8239; Essentially the Tory&rsquo;s are planning (in time-honoured fashion) to flog off the family silver and privatise forests up and down the country. The sale is intended to raise &pound;2bn - less than half of one years tax avoidance by Vodafone.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news753.htm"><img src='../images/753-fireworks-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news753.htm">SchNEWS 753, 7th January 2011</a></b>  <font size="1">Click <a href="pdf/news753.pdf">HERE</a> for PDF version</font>
<br />
<b>CSI Palestine</b> - 
	The tragic death of an unarmed woman in Palestine has lead to a global cry for action against the increasing use of &lsquo;non-violent&rsquo; weapons in the continued repression of the Palestinian people. Jawaher Abu Rahma died after inhaling the supposed &lsquo;non-lethal&rsquo; fumes of a CS gas canister, a substance which has been banned in the UK since 1964 due its capacity to kill hours after inhalation.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news752.htm"><img src='../images/752-nativity-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news752.htm">SchNEWS 752, 17th December 2010</a></b>  <font size="1">Click <a href="pdf/news752.pdf">HERE</a> for PDF version</font>
<br />
<b>The Well Unfair State</b> - 
	Tuition fees got you riled up? Wait &lsquo;til you get a handle on what our Tory chums have got planned next - flagged up since the first announcement of spending cuts, the recent consultation paper entitled Universal Credit - is a massive demolition (sorry, overhaul) of the existing benefits system. With all benefits and tax credits being rolled into one system, the screws are gonna get tightened.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news751.htm"><img src='../images/751-charles-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news751.htm">SchNEWS 751, 10th December 2010</a></b>  <font size="1">Click <a href="pdf/news751.pdf">HERE</a> for PDF version</font>
<br />
<b>For Whom the Fee Tolls</b> - 
	The wave of student protests that sprang forth so vehemently on 11/11/10 faced its day of reckoning this Thursday (9th) as MPs huddled in Parliament to decide on the future of education. Chaos was on the menu as the tuition fees bill passed with a majority of 21 votes. The crowds started congregating at 12 noon by the University of London Union in Malet Street. The march stalled to hear speeches and bold declarations such as &ldquo;We will not be detained, constrained and kettled again!&rdquo; just before heading towards Parliament Square.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news750.htm"><img src='../images/750 FIFA.150.JPG' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news750.htm">SchNEWS 750, 2nd December 2010</a></b>  <font size="1">Click <a href="pdf/news750.pdf">HERE</a> for PDF version</font>
<br />
<b>Fee Foes' Fine Fun</b> - 
	Day X2, the second national day of action against education reforms, saw a whirlwind of protest around the country. From teach-ins to storming the town hall, walk-outs to battling police; students, school children and anti-cuts activists ensured the momentum of the student rebellion continues to build.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news749.htm"><img src='../images/749-Student-protest-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news749.htm">SchNEWS 749, 25th November 2010</a></b>  <font size="1">Click <a href="pdf/news749.pdf">HERE</a> for PDF version</font>
<br />
<b>New Kids on the Black Block</b> - 
	The second wave of student protest this week saw thousands walkout of their classrooms and on to the streets all over the country. Whilst events in London were amply covered &ndash; albeit traditionally skewed - by the mainstream media, SchNEWS was braving it on the frontline at the Brighton demo where around 3,000 students marched through the city centre.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news748.htm"><img src='../images/748-wills-kate-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news748.htm">SchNEWS 748, 18th November 2010</a></b>  <font size="1">Click <a href="pdf/news748.pdf">HERE</a> for PDF version</font>
<br />
<b>Complete Hissy Fit</b> - 
	Still smarting from the failure to contain the student rioters in central London, the Met are peevishly taking out their anger against an old enemy. On Monday evening the hosts of the FIT WATCH website were asked to close it down on the grounds of it &ldquo;being used to undertake criminal activities&rdquo;. In a letter from the MET, the company Justhost.com were warned that the website was being used in an attempt &ldquo;to pervert the course of justice&rdquo;. The DI demanded that the site be taken down for &lsquo;twelve months&rsquo;. Deciding caution was the better part of valour, Justhost decided to take it down.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news747.htm"><img src='../images/747-student-riot-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news747.htm">SchNEWS 747, 11th November 2010</a></b>  <font size="1">Click <a href="pdf/news747.pdf">HERE</a> for PDF version</font>
<br />
<b>Losing Their Faculties</b> - 
	Finally, perhaps, the wave is cresting... The fightback against the vicious Tory cuts programme has so far been restricted to placard waving demos and petitions. Not any more &ndash; the placards are still there but now they&rsquo;re being used to build bonfires in front of a windowless Tory HQ. And they say young people don&rsquo;t take an interest in politics.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news746.htm"><img src='../images/746-vodafone-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news746.htm">SchNEWS 746, 4th November 2010</a></b>  <font size="1">Click <a href="pdf/news746.pdf">HERE</a> for PDF version</font>
<br />
<b>The Vodafone-y War</b> - 
	In the past week as many as 21 Vodafone stores have been hit with direct action. The phone giant have become a widespread target for the national anti-cuts campaign following their multi-billion government-sanctioned tax-dodging shenanigans.&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news745.htm"><img src='../images/745-mark-kennedy-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news745.htm">SchNEWS 745, 29th October 2010</a></b>  <font size="1">Click <a href="pdf/news745.pdf">HERE</a> for PDF version</font>
<br />
<b>Serious Organised Crime</b> - 
	The six defendants in the second SHAC trial were hammered with vicious sentences this week. The harshest was the six years handed down to 53-year-old Sarah Whitehead. Sentences for the other defendants Tom Harris, Nicole Vosper, Jason Mullan, Nicola Tapping and Alfie Fitzpatrick ranged from a two year suspended sentence for the youngest, Alfie, to three and a half years for Nicole Vosper. All the defendants also copped lengthy ASBOs, which will prevent them from any further participation in animal rights activism.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news744.htm"><img src='../images/744-A-team-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news744.htm">SchNEWS 744, 22nd October 2010</a></b>  <font size="1">Click <a href="pdf/news744.pdf">HERE</a> for PDF version</font>
<br />
<b>Crude but Refined</b> - 
	Utmost secrecy, activist goodie bags and sheer determination shut down the the UK&rsquo;s busiest oil refinery last week in one of the most well-planned actions the climate justice movement has seen so far. Stilt walkers, a samba band and around 500 activists blockaded the road to Croydon oil refinery in Essex on Saturday, stopping an estimated 400,000 gallons of oil getting to London&rsquo;s petrol stations.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news743.htm"><img src='../images/743-hammertime-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news743.htm">SchNEWS 743, 15th October 2010</a></b>  <font size="1">Click <a href="pdf/news743.pdf">HERE</a> for PDF version</font>
<br />
<b>Hammering home the Point</b> - 
	The &lsquo;ITT&rsquo;s Hammertime&rsquo; Smash EDO demo in Brighton on Wednesday (13th) ended with over fifty arrests, after police made it clear they were going to protect the arms makers at any cost. The demonstration planned to lay siege to Brighton&rsquo;s premier weapons factory for the day, but police repression and a newly developed policy for preventative arrests put paid to the ambitious plans to push the factory out of town. With both a section 60 and highly restrictive section 14 in place, police were given even greater powers, which they gleefully exercised...

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news742.htm"><img src='../images/742-europe-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news742.htm">SchNEWS 742, 8th October 2010</a></b>  <font size="1">Click <a href="pdf/news742.pdf">HERE</a> for PDF version</font>
<br />
<b>Frontier Law</b> - 
	The No Borders camp in Brussels last week persevered in getting their message across by means of various direct actions despite widespread arbitrary arrests and shocking police violence, including physical and sexual abuse in police custody. At least 500 mostly &lsquo;preventative&rsquo; arrests took place, and 14 people were seriously injured. Here&rsquo;s a day by day report...
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news741.htm"><img src='../images/741-spain-strikes-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news741.htm">SchNEWS 741, 1st October 2010</a></b>  <font size="1">Click <a href="pdf/news741.pdf">HERE</a> for PDF version</font>
<br />
<b>Unrest For The Wicked</b> - 
	Mass protests and strikes have broken out across the whole of Europe this week as the reality of already imposed and still pending austerity cuts becomes clear. Across the EU, rallies were held in thirteen capital cities and in Spain a general strike saw millions take action.&nbsp;On Wednesday (29th) around 100,000 representatives of the European trade union movement, including German miners and Polish shipbuilders, brought Brussels to a standstill to protest against the forthcoming savage spending cuts. The message &ldquo;We will not pay for their crisis&rdquo; is now resounding across Europe.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news740.htm"><img src='../images/740-van-damme-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news740.htm">SchNEWS 740, 24th September 2010</a></b>  <font size="1">Click <a href="pdf/news740.pdf">HERE</a> for PDF version</font>
<br />
<b>Brussels Sprouts Camp</b> - 
	No Borders Camp 2010 in Brussels kicks off this Saturday (25th) til the 3rd October, and plenty are converging on that part of the continent in an effort to create a world where no one is illegal. Among the objectives of the camp are the denouncing of European migration policy; showing the links between this policy and the structures of capitalism and repression; the blocking of Brussels&rsquo; deportation system and the organisation of an autonomous safe space for the voices of migrants and activists to be heard.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news739.htm"><img src='../images/739-pope-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news739.htm">SchNEWS 739, 17th September 2010</a></b>  <font size="1">Click <a href="pdf/news739.pdf">HERE</a> for PDF version</font>
<br />
<b>Gauling Behaviour</b> - 
	Weak to begin with, France&rsquo;s attempts to deny that recent mass expulsions of Roma people were racist have been dealt a blow after a Ministry of Interior circular ordering evacuation of camps of Roma, as &ldquo;a matter of priority&rdquo; was leaked. From mid-August to early September this year, approximately 1000 Roma were deported from France and 128 Roma camps dismantled.

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news738.htm"><img src='../images/738-hovefields-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news738.htm">SchNEWS 738, 10th September 2010</a></b>  <font size="1">Click <a href="pdf/news738.pdf">HERE</a> for PDF version</font>
<br />
<b>Caravandals</b> - 
	The Hovefields Gypsy/Traveller site in Essex with 50-60 inhabitants has been evicted this week. At the time of writing, a group of these families are still on the road without anywhere to stop, having been also evicted from two other sites they tried to move on to, all within 24 hours. In fact it is illegal for them to stop anywhere as a group, as they are more than six live-in vehicles.
	&nbsp;

</p>
</td></tr></table>
</div>


<div class="backIssue">
<table width='100%'><tr><td>
<a href="news737.htm"><img src='../images/737-blair-mein-kampf-150.jpg' class='backIssue_image' align='right' /></a><p align='left'>
<b><a href="news737.htm">SchNEWS 737, 3rd September 2010</a></b>  <font size="1">Click <a href="pdf/news737.pdf">HERE</a> for PDF version</font>
<br />
<b>Out of Their League</b> - 
	It was supposed to be &lsquo;The Big One&rsquo; - that&rsquo;s how the EDL were billing their Bradford rally - a climactic moment in their campaign against &lsquo;radical Islam&rsquo;. According to puff pieces released on Youtube before the event, there were supposed to be 5,000 leaguers descending on the Yorkshire town on Saturday 28th August. The EDL had warned women and children not to be present and one flyer bore the slogan &lsquo;Burn, baby, burn&rsquo;.&nbsp;In the end a mere 700-800 EDLers were on display...

</p>
</td></tr></table>
</div>


			</div>





			<!-- BEGIN ARCHIVED BACK ISSUES -->

			<p align="left">&nbsp;</p>
			<p align="justify"><font face="Arial, Helvetica, sans-serif">
				<a name="backissues"></a>
				<font size="2"><b><font size="4">Back Issues...</font></b></font>
			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-701-750.htm"><b><font size="2">Issues 701-750 <br />
  				November 2009 - December 2010 </font></b></a>
  			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-651-700.htm"><b><font size="2">Issues 651-700 <br />
  				October 2008 - November 2009 </font></b></a>
  			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-601-650.htm"><b><font size="2">Issues 601-650 <br />
  				August 2007 - October 2008 </font></b></a>
  			</font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-551-600.htm"><b><font size="2">Issues 551-600<br />
            	July 2006 - August 2007</font></b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-501-550.htm"><b><font size="2">Issues 501-550<br />
            	June 2005 - July 2006</font></b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif" size="2">
            	<a href="index-451-500.htm"><b>Issues 451 - 500<br />
            	April 2004 - June 2005</b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif" size="2">
            	<a href="index-401-450.htm"><b>Issues 401 - 450<br />
            	April 2003 - April 2004</b></a>
            </font></p>

            <hr />

            <!-- END ARCHIVE BACK ISSUES -->



            <font face="Arial, Helvetica, sans-serif" size="2"></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-351-401.htm"><img src="../images_main/peacederesistanceweb.jpg" width="100" height="145" align="right" border="1" alt="SchNEWS - Peace De Resistance" /><b>Issues
			351-401<br /> April 2002 - April 2003</b></a><br> <b>These issues are published
			in the book 'Peace de Resistance':<br> </b>'As the Forces of Darkness gather for
			a new attack on Iraq, they're shaken by an unexpected explosion of resistance.
			US airbases are invaded; military convoys are blockaded; schoolkids stage mass
			walkouts from Manchester to Melbourne; roads are occupied, embassies besieged,
			and cities all over the world are brought to a standstill by the biggest mass
			demonstrations ever seen. Read about the efforts to sabotage the war machine that
			the corporate media ignored: not just the mass demos, but Tornado-trashing in
			Scotland, riots across the Middle East, and train blockades in Europe. Featuring
			SchNEWS 351-401, this book also covers many of the past year's other stories from
			the rebel frontlines, including GM crop-trashing, international activists in Palestine,
			and the 10th anniversary of Castlemorton. All that plus loads of new articles,
			photos, cartoons, subverts, satire, a comprehensive contacts list, and more&#133;'
			<br /> </font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>PLUS
			INCLUDES FREE CD-ROM by BEYOND TV featuring footage of many of the actions reported
			in the book, satire plus other digital resources</b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>&quot;Peace
			de Resistance&quot;<br /> </b>ISBN 09529748 7 8 <br /> &pound;7.00 inc. p&amp;p
			direct from us.<br /> Contact SchNEWS for distribution details<b><br /> <a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-301-350.htm" target="index-301-350.htm"><img src="../images_issues/sotw.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Of The World" /></a><b><font size="3"><a href="index-301-350.htm"><font size="2">Issues
			301-350 <br /> April 2001 - April 2002</font></a><br /> </font><b>These issues
			are published in the book 'SchNEWS Of The World':<br> </b></b>The temperature
			goes up a few notches as the earth's climate has its hottest year on record -
			and the corporate carve-up lifts its tempo in the paranoia and madness of September
			11th. Behind the haze Argentina goes into meltdown. Israel reoccupies Palestine
			causing a second Intifada. Hundreds of thousands come out on the street in Genoa,
			Quebec, Gothenburg, Barcelona, Brussels against globalised institutions. Even
			larger numbers fight for their land and livelihood against neo-liberalism in South
			Africa, India, South America and the rest of the global south. <br /> These 50
			issues plus 200 other pages of articles, cartoons, photos, graphics, satire, and
			a comprehensive contact database are all together in<b>...<br /> SchNEWS Of The
			World</b> .<br /> 300 pages, ISBN 09529748 6 X, &pound;4 inc. p&amp;p direct from
			us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b></font></p><hr />
			<p><font face="Arial, Helvetica, sans-serif" size="2"><img src="../images_issues/yearbook2001.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Squall Yearbook 2004" /><b><b><font size="3"><a href="index-251-300.htm"><font size="2">Issues
			251-300 <br /> March 2000 - April 2001</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchNEWS/Squall Yearbook 2001':</b></b><br> The
			Zapatistas march into Mexico City, thousands disrupt the World Bank meeting in
			Prague, Churchill gets an anarchist make-over: from Bognor to Bogota, Dudley to
			Delhi, and Kilburn to Melbourne, resistance has become as global as the institutions
			of capitalism. This was a year full of stories of people at the frontline of struggles
			worldwide, and creating sustainable solutions to the corporate carve-up of the
			planet. These 50 issues of SchNEWS along with the best of Squall magazine, plus
			loadsa photos, cartoons, satirical graphics, subverts, and a comprehensive contacts
			database are available together in....<br /> <b>SchNEWS/Squall Yearbook 2001</b>
			<br /> 300 pages, ISBN 09529748 4 3, BARGAIN - &pound;3.00 inc. p&amp;p direct
			from us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b>
			</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><img src="../images_issues/schquall.gif" width="100" height="144" align="right" border="1" alt="SchQUALL" /><b><font size="3"><a href="index-201-250.htm"><font size="2">Issues
			201-250<br /> February 1999 - March 2000</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchQUALL': <br> </b></b>Genetically modified
			crops get trashed, animal rights, freemasons, June 18th '99 international day
			of action against capitalism, Exodus Collective, Cuba, the November 30th '99 Battle
			for Seattle, climate change, parties and festivals, indigenous peoples' resistance
			to multinational corporations, the privatisation of everything in sight, crap
			arrests of the week, prisoner support and much more&#133;the full lowdown on the
			direct action movement in Britain and abroad! <br /> These 50 issues of SchNEWS
			plus the best of Squall magazine, top photographs, cartoons, subverts and much
			more are available together in ...<b><br /> SchQUALL - SchNEWS and Squall Back
			To Back</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/survival.gif" width="100" height="143" align="right" border="1" alt="SchNEWS Surviva Guide" /></b><font size="3"><a href="index-151-200.htm"><font size="2">Issues
			151-200 <br /> January 1998 - January 1999 </font></a></font></b><br /> <b>These
			issues are published in the book 'Survival Handbook': </b></b><br> Read all about
			it! Genetic crop sites get a good kicking; streets reclaimed all over the world;
			docks occupied in protest of death at work; protestors rude about multinational
			corporations' plans for world domination... <br /> SchNEWS gives you the news
			the mainstream media ignores. Tells you where to party and protest. Tries not
			to get all po-faced about what's going down in the world. <br /> These 50 SchNEWS
			issues are together with a set of 'survival handbook' stylee articles to help
			you survive into the new millenium... plus photos, cartoons, a comprehensive database
			of nearly five hundred grassroots organisations and more in... <br /> <b>SchNEWS
			Survival Handbook</b><br /> <b>SOLD OUT</b> - You might be able to find a copy
			in your local radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/annual_small.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Orange Book" /></b><font size="3"><a href="index-101-150.htm"><font size="2">Issues
			101-150<br /> December 1996 - January 1998</font></a></font><br /> These issues
			are published in the book 'SchNEWS Annual':</b></b><br> &quot;The McLibel trial
			ends, Manchester Airport second runway protest camp goes mad, eviction at Fairmile,
			the GANDALF Trial, anti-nuclear demos in Germany, the massive Reclaim The Streets
			that took back Trafalgar Square... New Labour breaking each and every promise
			they ever made.<br /> These 50 issues are together with loads more pics, graphics
			and articles in...<br /> <b>SchNEWS Annual </b><br /> 230 pages, ISBN: 0-9529748-1-9,
			&pound;BARGAIN - &pound;2.00 inc. p&amp;p direct from us.<b><br /> <b><a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/round.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Black Book" /><font size="3"><a href="index-051-100.htm"><font size="2">Issues
			51-100 <br /> December 1995 - November 1996</font></a></font></b><br> <b>These
			issues are published in the book 'SchNEWS Round':</b> <br /> </b>Direct Action
			takes over England... M41 motorway gets occupied in huge Reclaim The Streets party...
			The Third Battle Of Newbury... plus the national SchLIVE Tour, the Liverpool Dockers
			go 'direct action', resistance to Jobseekers Allowance, Exodus Collective, Trident
			Ploughshares women go free after smashing Hawk Jet, Selar Opencast Mine protest
			camp, Fairmile - A30 protest camp, Wandsworth Eco-village, the Squatter's Estate
			Agency...SchNEWS publishes the inside story from the activists themselves! <br />
			The 50 issues of SchNEWS featuring all this stuff, plus 60 extra pages of photos,
			cartoons, more articles plus the 1996 activists database is available in...</font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>SchNEWS
			Round</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><p></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/reader.gif" width="101" height="142" align="right" border="1" alt="SchNEWS Reader" /><font size="3"><a href="index-001-50.htm"><font size="2">Issues
			1-50 <br /> November 1994 - November 1995</font></a></font><br /> These issues
			are published in the book 'SchNEWS Reader':</b></b><br> Where It All Began...<br />
			From the pilot edition edition right through the first year of SchNEWS featuring
			the first of many Crap Arrest Of The Weeks, the Criminal Justice Act 'Arrestometer',
			and a running commentary of resistance as the sections of the Criminal Justice
			Act come in attacking Travellers, Squatters, Hunt Sabs, Ravers, Protesters, Footie
			Fans and freedom in general. In the news was... Shoreham Live Animal Export protests,
			the end of the No-M11 Link Rd campaign, Pollok Free State anti-M77 protest in
			Glasgow, eviction of Stanworth Valley 'village in the trees' near Blackburn against
			the building of the M65, opencast mining protest camps in South Wales and much
			more...<br /> These historic 50 issues of SchNEWS, plus loadsa cartoons by Kate
			Evans and more are available in...<b><br /> </b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b><b>SchNEWSreader</b><br />
			<b>SOLD OUT</b> </b>Rare as hen's teeth. If you missed out you can read most of
			the SchNEWS issues here - and if you really want to see em as they were you can
			download scans of them below....</font></p><p><img src="../images_main/spacer_black.gif" width="100%" height="10" /></p></td></tr>
			<tr> <td valign="bottom" width="10" height="2">&nbsp;</td><td valign="top" width="468" height="2"><img src="../images_main/spacer.gif" width="1" height="10" /><br />
			<font face="Arial, Helvetica, sans-serif" size="2"><font size="1">SchNEWS, c/o
			Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> Press/Emergency Contacts: +44 (0) 7947 507866<br />Phone: +44 (0)1273
			685913<br /> email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a></font></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><font size="1">@nti copyright
			- information for action - copy and distribute!</font></font></p>



</div>




<div class='mainBar_right'>

		<?php

		//	Latest Issue Summary
		require "../inc/currentIssueSummary.php";

		?>

</div>






<div class="footer">

		<?php

			//	Include Footer
			require "../inc/footer.php";

		?>

</div>








</div>




</body>
</html>




