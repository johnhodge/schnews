<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 717 - 9th April 2010 - Colombia: Crude Boys</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, colombia, bp, unions, oil, latin america" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 717 Articles: </b>
<p><a href="../archive/news7171.php">Counter Strike</a></p>

<p><a href="../archive/news7172.php">Stan By Your Man</a></p>

<p><a href="../archive/news7173.php">Calloo, Calais</a></p>

<b>
<p><a href="../archive/news7174.php">Colombia: Crude Boys</a></p>
</b>

<p><a href="../archive/news7175.php">Cementing Friendship</a></p>

<p><a href="../archive/news7176.php">Schnews In Brief</a></p>

<p><a href="../archive/news7177.php">Going Ballistic</a></p>

<p><a href="../archive/news7178.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th April 2010 | Issue 717</b></p>

<p><a href="news717.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>COLOMBIA: CRUDE BOYS</h3>

<p>
Colombia solidarity activists are stepping up their campaign against the human rights disaster that is oil giant BP. They are calling out for activists to picket BP&rsquo;s annual shareholders general meeting next Thursday (15th). <br />  <br /> Following the meeting, BP are set to respond the demands of the Colombian oilworkers union USO and the community movement Movimiento de Dignidad por el Casanare, who are calling for a resolution to the crisis at Tauramena Central Processing Facility in Casanare. <br />  <br /> After Colombian &ldquo;anti-mutiny&rdquo; police violently broke up a month old strike by workers at the facility last month (see <a href='../archive/news711.htm'>SchNEWS 711</a>), hospitalising three workers, BP finally met with union and community representatives - but not before trying (and failing) to run a group of scab workers through the picket line. <br />  <br /> An agreement was struck to set up negotiating commissions to deal with labour problems, social investment, goods and services, environment and human rights. However, the process soon broke down and on March 7th the local community staged a mass protest against the company&rsquo;s failure to address their concerns. On March 23rd, negotiators presented their demands to BP and are now waiting for a response. <br />  <br /> The USO is the first union to operate in the paramilitary dominated area and their successes have come at a price. On March 27th, in the nearby city of Villavicencio, two gunmen on a motorbike attacked USO officials, killing their bodyguard. <br />   <br /> The BP AGM kicks off at 10.30am at the Excel Conference Centre in London on April 15th. <br />  <br /> * see <a href="http://www.info@colombiasolidarity.org.uk" target="_blank">www.info@colombiasolidarity.org.uk</a>  <br />  <br /> <a href="http://www.sussexcolombiasolidarity.wordpress.com&nbsp;" target="_blank">www.sussexcolombiasolidarity.wordpress.com&nbsp;</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bp&source=717">bp</a>, <a href="../keywordSearch/?keyword=colombia&source=717">colombia</a>, <a href="../keywordSearch/?keyword=latin+america&source=717">latin america</a>, <a href="../keywordSearch/?keyword=oil&source=717">oil</a>, <a href="../keywordSearch/?keyword=unions&source=717">unions</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(731);
			
				if (SHOWMESSAGES)	{
				
						addMessage(731);
						echo getMessages(731);
						echo showAddMessage(731);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


