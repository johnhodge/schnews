<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 709 - 12th February 2010 - Carmel Agrexco : Slay It With Flowers</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, palestine, boycott israeli goods, direct action, carmel agrexco" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 709 Articles: </b>
<p><a href="../archive/news7091.php">Slung Out On Yer Frontier</a></p>

<b>
<p><a href="../archive/news7092.php">Carmel Agrexco : Slay It With Flowers</a></p>
</b>

<p><a href="../archive/news7093.php">I-ran Away</a></p>

<p><a href="../archive/news7094.php">Serco-mplicit  </a></p>

<p><a href="../archive/news7095.php">Cold Turkey  </a></p>

<p><a href="../archive/news7096.php">West Bank Story </a></p>

<p><a href="../archive/news7097.php">Flying In The Face</a></p>

<p><a href="../archive/news7098.php">Sus-sex Appeal</a></p>

<p><a href="../archive/news7099.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/709-carmel-lg.jpg" target="_blank">
													<img src="../images/709-carmel-sm.jpg" alt="Boycott Settlement Goods" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 12th February 2010 | Issue 709</b></p>

<p><a href="news709.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CARMEL AGREXCO : SLAY IT WITH FLOWERS</h3>

<p>
Last Sunday (8th), two teams of activists blockaded the gates of Cargoflora and Carmel Agrexco in protest at the importation of flowers from occupied Palestine. Several of the blockaders were severely assaulted by staff at the Cargoflora depot. <br />  <br /> One activist said, &ldquo;<em>We got there at around 5.30, and by 6 we had ten people locked on with D-Locks, superglue and Harris fencing panels &ndash; the security guard was asleep in his caravan the whole time</em>&rdquo;. <br />   <br /> Over at Carmel workers didn&rsquo;t do much except wait for the police - maybe Carmel have just got used to these interventions. <br />  <br /> At Cargoflora it was a different story; workers yanked at D-Locks and tried to intimidate activists. Eventually the police did show up but they refused to intervene as the staff wrenched one gate open, even using a fork lift truck to force out one blockader, still locked to the gate leading out to the road. Supporters rushed over to the other gate to prevent more of the same, and after tussles with the boltcropper-wielding staff  they were dragged away by police. <br />   <br /> Eventually the activists, point made, unlocked themselves. Both of the protesters on the second gate required hospital treatment. Meanwhile the Met&rsquo;s finest stopped &lsquo;n&rsquo; searched all of the two&rsquo;s supporters. As has become traditional at Carmel protests, no-one was arrested &ndash; clearly Cargoflora are just as worried as Carmel about subjecting the origin of their products to the court process. <br />  <br /> With Valentine&rsquo;s day looming (not that you needed reminding, right?) Israeli companies are set to make a fortune out of the romantically guilt-ridden British public. Amazingly many of those red roses and blue violets on sale in the florists are actually coming out of the besieged Gaza Strip. The relentless Israeli blockade of Palestinian products has been specially eased up during this period. Around 450,000 flowers are  passing through the Gaza crossings each week, bound for the UK, Holland and other parts of Europe. Carmel Agrexco&rsquo;s flowers are also sourced from the occupied West Bank. <br />   <br /> In previous years, Carmel-Agrexco&rsquo;s UK headquarters in Hayes, Middlesex, was the site of angry protests on Valentine&rsquo;s Day, (see <a href='../archive/news699.htm'>SchNEWS 699</a>) with campaigners calling for a boycott of Israeli flowers and other goods. SchNEWS has learned (off of Corporate Watch) that Cargoflora, which has premises around the corner from Agrexco&rsquo;s depot, has in previous years provided freight services for Carmel-Agrexco flowers over the Valentine&rsquo;s period. <br />   <br /> *see <a href="http://www.corporatewatch.org" target="_blank">www.corporatewatch.org</a> and <a href="http://www.bigcampaign.org&nbsp;" target="_blank">www.bigcampaign.org&nbsp;</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=boycott+israeli+goods&source=709">boycott israeli goods</a>, <a href="../keywordSearch/?keyword=carmel+agrexco&source=709">carmel agrexco</a>, <a href="../keywordSearch/?keyword=direct+action&source=709">direct action</a>, <a href="../keywordSearch/?keyword=palestine&source=709">palestine</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(652);
			
				if (SHOWMESSAGES)	{
				
						addMessage(652);
						echo getMessages(652);
						echo showAddMessage(652);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


