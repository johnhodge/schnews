<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 792 - 14th October 2011 - False Profits</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 792 Articles: </b>
<b>
<p><a href="../archive/news7921.php">False Profits</a></p>
</b>

<p><a href="../archive/news7922.php">Greece Tightening</a></p>

<p><a href="../archive/news7923.php">Honduran-ce Test</a></p>

<p><a href="../archive/news7924.php">Far Right Angels</a></p>

<p><a href="../archive/news7925.php">A Healthy Turnout</a></p>

<p><a href="../archive/news7926.php">Dale Farm Update</a></p>

<p><a href="../archive/news7927.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th October 2011 | Issue 792</b></p>

<p><a href="news792.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FALSE PROFITS</h3>

<p>
<p>  
  	<strong>AS SCHNEWS ENTERS THE TWILIGHT EUROZONE</strong>  </p>  
   <p>  
  	We have settled into a continual cycle of panics over the eurozone economic situation. Recent chapters have included Italy and Spain&rsquo;s sovereign debt rating downgrades and Greece going to the brink of defaulting on its loans again &ndash; as it inevitably will without major debt restructuring, c&rsquo;mon people it doesn&rsquo;t take a rocket scientist to figure that one out!.  </p>  
   <p>  
  	We&rsquo;ve had the usual swiftly convened crisis meetings of Euro-finance ministers desperately cobbling together another &lsquo;confidence boosting&rsquo; round of bailouts and guarantees. Just last week we&rsquo;ve had a new Greek &lsquo;deal&rsquo;, even as Slovakia blocked the last round of EU bailout measures (widely already seen by markets as woefully inadequate).&nbsp; Belgian (debt levels up near 100% GDP by the way &ndash; next highest after Greece, Italy, Ireland and Portugal) and French governments are desperately trying to prop up their collapsing toxic bank Dexia, and the Bank of England are printing another &pound;75bn of cash &ndash; &lsquo;quantitative easing&rsquo; in the hope of &lsquo;stimulating&rsquo; the economy (and inflating away some of the debt to boot).Like the many others since the 2008 crash, all these measures are also probably doomed to fail. Why? Because the slow burning Euro crisis has developed into a predictable little game and nice little (big) earner for a tiny percentage of extremely rich people. &nbsp;  </p>  
   <p>  
  	Following the collapse of the dodgy credit-fuelled boom which had been driving growth and apparent wealth in Europe and the US for a decade, the biggest private players in the finance markets &ndash; those with billions in loose change to play with &ndash; have been betting that Europe is going to ultimately fail. They see the overwhelming amounts of debt, the political difficulty of making the really savage levels of savage public spending cuts that could rebalance the books, and of co-ordinating so many different &lsquo;national interests&rsquo; (or at least short-term domestic political interests) in putting together regulatory changes / tax / funds redistribution that could steady the ship.  </p>  
   <p>  
  	If one of the US states was to go near bankrupt, for example, the federal government could just redistribute cash from other richer states to prop it up &ndash; but there&rsquo;s a limit to how many times German or French taxpayers are gonna swallow giving all &lsquo;their&rsquo; cash away to some foreigners who can&rsquo;t run their finances as well as &lsquo;we&rsquo; do. Of course its way more complicated than that &ndash; a large slice of that German wealth, for instance, has come from exporting all its goods to other European countries it has then lent the money (money created erroneously virtually out of thin air as it now transpires) to pay for it all. But tell that the doorstepping politician on the street.  </p>  
   <p>  
  	<strong>MARKET FARCES <br />  
  	</strong>  </p>  
   <p>  
  	So the market-makers have been applying the pressure &ndash; selling short stocks, and pushing up bond prices. Other investors with the herd mentality lose their confidence and follow their lead.  </p>  
   <p>  
  	The Euro politicians are then forced into another round of bail out measures. The market stabilises &ndash; and the big boys buy back all the assets they have just sold at a cheaper price. Kerching! As they begin buying, others again follow suit, prices rise again (and government borrowing gets a bit cheaper again for a little while) giving the mirage of &lsquo;confidence&rsquo; returning to the markets. When they get high enough, the selling starts again &ndash; realising, in profit, the difference between the current high and the previous low. And so the whole cycle begins again forcing the next partial bailout. &nbsp;  </p>  
   <p>  
  	So it seems that large slices of all this bailout cash actually has been going to that richest 1% of folks who own everything; that is why despite all the cash &lsquo;injected&rsquo; into the system there&rsquo;s still none that banks want to lend out to the other 99%.&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;  </p>  
   <p>  
  	And it seems likely to continue this way until the Euro-project falls apart or changes the rules radically. Euro-governments are nowhere near having the ability or mandate to agree the kind of &lsquo;back it all to the absolute hilt&rsquo; deal &ndash; or radical structural changes &ndash; that might stabilise things.  </p>  
   <p>  
  	And even then it may not work. The past few years has seen the most volatile trading on global markets ever recorded. More stocks, shares and bonds are traded, and more rapidly, than ever &ndash; and prices swing around more dramatically than ever before. And at least part of this is down to the introduction of &lsquo;AI&rsquo; computer traders.  </p>  
   <p>  
  	More and more of the big players are turning to software to make their trading decision. They use complex algorithms and game theory-based programs which automatically trade huge volumes of stocks back and forth, exploiting every logical avenue for arbitrage, faster than any human could, and without any logical or emotional baggage. And, in general, the software has proved very adept and successful &ndash; for its owners. The software has absolutely zero understanding of its real world influence (i.e. even less than a human trader) but just seeks profit according to it&rsquo;s understanding how complex markets work.  </p>  
   <p>  
  	But computers never quite seem to work how you&rsquo;d expect, do they? It seems they are prone to amplifying feedback loops as one software swinging dick battles it out with other virtual masters of the universe out there in the market (surreal or what?) and it has lead to wild swings and sudden&nbsp; crashes as all the robot Gordon Gekko&rsquo;s all sell off or buy vast volumes of stock in mere minutes.&nbsp; One such &lsquo;flash crash&rsquo; in May this year saw the second biggest one day swing of the Dow Jones ever, as the market fell 9% only to rebound back in minutes. The U.S. Securities and Exchange Commission and the Commodity Futures Trading Commission&rsquo;s investigation has since&nbsp; implicated the auto-trading systems for causing it.  </p>  
   <p>  
  	Europe to survive in its present form? We wouldn&rsquo;t bet on it. Those who have made all their vast wealth and power playing the system and now dominate it, seem hellbent on bringing it all down around the heads for the sake of squeezing the last pips out and everyone&rsquo;s grandchildren beyond their unborn ability to pay for it. Presumably they hope it will kill off the state for good and usher in an &uuml;ber-marketised private company system to run everything &ndash; companies they will then own.  </p>  
   <p>  
  	The SchNEWS answer? Everyone default. On everything. Plan a new level playing field and make the richest foot the bill for screwing up the last system. Or rise up and kick em all out. Over to you guys occupying Wall St and the London Stock Exchange...  </p>  
   <p>  
  	&nbsp;  </p>  
   <p>  
  	<u><strong>OCCUPY THE STOCK EXCHANGE</strong></u>  </p>  
   <p>  
  	This Saturday the 15th, join some of the 99% calling for equality and justice for all: Occupy the London Stock Exchange  </p>  
   <p>  
  	Assemble ready for action in front of St Pauls Cathedral at Midday &ndash; not too early or late. Bring tents &amp; sleeping bags.  </p>  
   <p>  
  	&nbsp;Follow @OccupyLSX on twitter for updates on the day. <br />  
  <a href="http://	www.occupylondon.org.uk" target="_blank">	www.occupylondon.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1387), 161); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1387);

				if (SHOWMESSAGES)	{

						addMessage(1387);
						echo getMessages(1387);
						echo showAddMessage(1387);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


