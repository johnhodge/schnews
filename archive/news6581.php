<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 658 - 5th December 2008 - Koch-a-hoop</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, heckler & koch, nottingham, no borders, arms trade, mark thomas, zimbabwe, africa, congo, darfur, refugees, asylum seekers, sudan, brian clough" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 658 Articles: </b>
<b>
<p><a href="../archive/news6581.php">Koch-a-hoop</a></p>
</b>

<p><a href="../archive/news6582.php">Whack-a-mole</a></p>

<p><a href="../archive/news6583.php">Od'd On Edo</a></p>

<p><a href="../archive/news6584.php">Get It E.on</a></p>

<p><a href="../archive/news6585.php">Somali-argh</a></p>

<p><a href="../archive/news6586.php">  Tactical Sabbatical</a></p>

<p><a href="../archive/news6587.php">Spit And Polish</a></p>

<p><a href="../archive/news6588.php">Schnews In Brief</a></p>

<p><a href="../archive/news6589.php">And Finally 658</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/658-brian-clough-lg.jpg" target="_blank">
													<img src="../images/658-brian-clough-sm.jpg" alt="Nottingham Forest Goes Ballistic" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 5th December 2008 | Issue 658</b></p>

<p><a href="news658.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>KOCH-A-HOOP</H3>

<p>
<b>FROM NOTTINGHAM TO NORTH WAZIRISTAN AS COUNTS THE HUMAN COST OF THE ARMS TRADE</b> <br />
 <br />
While a quick facts and figures check will reveal it to be quite a serious (or silly) exaggeration, Nottingham has been labelled the &#8216;gun crime capital of Britain&#8217;. It&#8217;s been compared to Al-Capone era Chicago and is dubbed &#8216;Shottingham&#8217; by the certain sectors of the media. So, as we reported back in May (see <a href="../archive/news631.htm">SchNEWS 631</a>), it&#8217;s always seemed a bit of an ironic location for the UK headquarters of Heckler & Koch, the world&#8217;s second-largest manufacturer of handguns, assault rifles, submachine guns, machine guns and grenade launchers. An irony not lost on &#8216;Notts Anti-Militarism&#8217; who have been holding monthly protests outside H&K&#8217;s office (Unit 3, Easter Park, Lenton Lane, Nottingham &#8211; for arms trade fans).  <br />
 <br />
Last Saturday (29th) at Nottingham Market Square, NA-M came together with No Borders Nottingham, who are part of the international network struggling for immigrant rights and promoting freedom of movement. The demo was entitled &#8216;Full Circle &#8211; from weapons to wars to refugees&#8217;, and focused on the links between the arms trade and the forced movement of peoples. It was attended by refugees from Sudan and Zimbabwe, who have survived the direct consequences of this deadly trade.      <br />
 <br />
<b>IMPORT/EXODUS</b> <br />
 <br />
Small arms are not only responsible for the deaths of half a million people a year &#8211; three quarters of them civilian &#8211; they also contribute to the displacement of millions more fleeing armed conflict. Literally millions of these weapons are made by H&K and have been liberally distributed throughout the world. From its Nottingham-based UK headquarters the German company has shipped arms to war zones such as Bosnia and Nepal and licensed its weapons for production in such human rights friendly countries as Saudi Arabia, Pakistan and Turkey. And that&#8217;s just the legal sales. <br />
 <br />
While in 2002 the Labour government did finally bring in some restrictions to arms sales with the Export Control Act, the law is riddled with loopholes. It doesn&#8217;t cover deals brokered by British companies if the arms don&#8217;t pass through the UK, or arms produced overseas under licence from British companies. It also completely fails to adequately monitor and control &#8216;end-use&#8217; &#8211; where the weapons end up actually being used and by who. Essentially, with a little legal wizardry you can still sell to whomever you like.   <br />
 <br />
As comedian Mark Thomas showed, getting round those pesky legal restrictions really isn&#8217;t at all difficult, even if you&#8217;re a cheeky cockney conman and not a real arms dealer at all. Thomas decided to do a little fictional gunrunning to Zimbabwe and using a front company, he contacted H&K-licensed companies based in Turkey, Pakistan and errm Iran. After checking that the arms were for the Zimbabwean military &#8211; Mugabe&#8217;s boys &#8211; and not for rebels (you can&#8217;t be too careful now), they all quite happily agreed to do a deal for a shipment of H&K MP5 sub-machine guns. The Pakistanis even offered a free gift; a snazzy holding bag for each gun if he took over 1000. <br />
 <br />
And it&#8217;s not just repressive regimes. 75% of the world&#8217;s small arms somehow or other find their way into the hands of criminal gangs and civilians, and H&K products are no exception. H&K guns have been found amongst gangs from Britain to Brazil. Even more serious is the role they have played in internal conflicts, especially in Africa, where they have made appearances amongst a number of non-state armed groups.  H&K weapons have been found being used by child soldiers in the Congo and by the Janjaweed in Darfur, where the H&K G3 rifle is rivalling the AK47 as the weapon of choice of the marauding militias.        <br />
 <br />
So what happens when you, legally or otherwise, ship vast quantities of small arms to conflict zones? According to the UN Refugee Agency, &#8220;<i>armed conflict is now the driving force behind most refugee flows.</i>&#8221; There&#8217;s currently an estimated 16 million refugees worldwide, a number that has risen considerably in the last two years.  Topping the list of source countries is, of course, Iraq, although also on the increase is the number of refugees fleeing Afghanistan, Pakistan, Zimbabwe and the Democratic Republic of the Congo. Although the vast majority (over 80%) only make it to bordering countries, of those who do make it as far as applying for asylum in the top 44 industrialised nations, around 9% come to Britain. <br />
 <br />
This year the government has been involved in High Court actions to secure its right to deport Zimbabweans and Congolese asylum seekers and in July it lifted its ban on deporting Sudanese applicants. Meanwhile 88% of Iraqis that apply for asylum in Britain are rejected at the initial decision-making stage. And for those that get to stay?  Daily Mail demonisation and crappy weather.   <br />
 <br />
The correlation between destinations of arms sales, countries we&#8217;ve stuck our military oar into and refugee numbers seems obvious but, for some reason, is rarely mentioned.  Fortunately, with actions like Full Circle in Nottingham and others like it around the country, there are plenty of people not only willing to join the dots but also to bring home the real life costs of the arms trade.      <br />
 <br />
* For more see: <a href="http://nottsantimilitarism.wordpress.com/heckler-koch" target="_blank">http://nottsantimilitarism.wordpress.com/heckler-koch</a> and <a href="http://www.nobordersnottingham.org.uk" target="_blank">www.nobordersnottingham.org.uk</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=no+borders&source=658">no borders</a>, <a href="../keywordSearch/?keyword=arms+trade&source=658">arms trade</a>, <a href="../keywordSearch/?keyword=refugees&source=658">refugees</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(154);
			
				if (SHOWMESSAGES)	{
				
						addMessage(154);
						echo getMessages(154);
						echo showAddMessage(154);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>