<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 721 - 7th May 2010 - A Smashing Defence</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, edo, itt, edo decommissioners, arms trade, raytheon, anti-war" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins May 17th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 721 Articles: </b>
<b>
<p><a href="../archive/news7211.php">A Smashing Defence</a></p>
</b>

<p><a href="../archive/news7212.php">Kant Pay, Won't Pay</a></p>

<p><a href="../archive/news7213.php">Got It Sussed</a></p>

<p><a href="../archive/news7214.php">Wurzel Damage</a></p>

<p><a href="../archive/news7215.php">Deportation Iran</a></p>

<p><a href="../archive/news7216.php">Inside Schnews</a></p>

<p><a href="../archive/news7217.php">Bita Sweet</a></p>

<p><a href="../archive/news7218.php">Meltdown Town</a></p>

<p><a href="../archive/news7219.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/721-greece-lg.jpg" target="_blank">
													<img src="../images/721-greece-sm.jpg" alt="Greek Referendum on Austerity: Exit Pole Suggests Government Will Take a Beating" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 7th May 2010 | Issue 721</b></p>

<p><a href="news721.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>A SMASHING DEFENCE</h3>

<p>
<strong>AS EDO DECOMMISSIONERS PREPARE FOR COURT.... <br />  <br /> &ldquo;<em>I told many people in Gaza about the people&rsquo;s strike on EDO MBM... When I recounted this action to people, I saw an expression come over their faces that I hadn&rsquo;t encountered before when talking about international solidarity. It was a kind of respect, a sense of surprised pride at a tiny move towards a leveling between the blood sacrifices and living hell of so many here, and sacrifices made by people in comparative comfort zones on the other side of the world - for them</em>&rdquo; - Ewa Jasiewicz - human rights activist/journalist eyewitness to Operation Cast Lead.</strong> <br />  <br /> In the early hours of 17th January 2009, during Israel&rsquo;s &lsquo;Operation Cast Lead&rsquo; offensive against Gaza, six people broke in to the EDO/ITT weapons&rsquo; components factory in Moulsecoomb, Brighton, and caused hundreds of thousands of pounds worth of damage to the production line. (see <a href='../archive/news663.htm'>SchNEWS 663</a> and also check out <a href="http://www.youtube.com/watch?v=M-281XjHw50" target="_blank">www.youtube.com/watch?v=M-281XjHw50</a> for the local paper&rsquo;s video of some of the damage) <br />  <br /> The six &lsquo;decommissioners&rsquo; barricaded themselves inside, and proceeded to smash computers and other equipment, with the aim of preventing the factory from being able to produce weapons. The action succeeded in stopping production at the factory for days.  <br /> On May 17th nine people will stand trial accused of &lsquo;conspiracy to commit criminal damage&rsquo;. Three were arrested outside the factory. The trial is expected to last for ten weeks at Hove Crown Court (see smashedo.org.uk to confirm). The decommissioners will be running the defence that their actions were legally justified as they were &ldquo;acting to prevent a greater crime&rdquo;. <br />   <br /> There is a history of juries finding anti-war activists not guilty when they attack machinery used in war crimes. In 1996 four women from Trident Ploughshares decommissioned a Hawk jet that was about to be shipped to Indonesia &ndash; they were found not guilty (see <a href='../archive/news62.htm'>SchNEWS 62</a>). In 2008 the Raytheon 9, who damaged a factory in Derry supplying weapons to Israel during the 2006 Lebanon war (see <a href='../archive/news555.htm'>SchNEWS 555</a>), were also unanimously acquitted by a jury. <br />  <br /> Before entering the EDO factory the decommissioners recorded statements detailing the reasons why they planned to take part in the action (see <a href="http://www.youtube.com/watch?v=mfa8R2AxUFg" target="_blank">www.youtube.com/watch?v=mfa8R2AxUFg</a>). One of the decommissioners, Elijah Smith said &ldquo;<em>I don&rsquo;t feel I&rsquo;m going to do anything illegal tonight, but I&rsquo;m going to go into an arms factory and smash it up to the best of my ability so that it cannot produce munitions and these very dirty bombs that have been provided to the Israeli army so that they can kill children. The time for talking has gone too far. I&rsquo;m not a writer, I&rsquo;m just a person from the community and I&rsquo;m deeply disgusted</em>.&rdquo; <br />  <br /> <table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/721-edo-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/../images/721-edo-sm.jpg"  alt='Every bomb that is dropped, every bullet that is fired in the name of this war on terror has to be made somewhere. And wherever that is, it can be resisted.'  /></a></td></tr></table>The EDO factory on Home Farm Road in Brighton, has been part of ITT Corporation since December 2007. Before ITT took over, the factory was known as EDO MBM, a trading unit of the EDO Corporation. ITT have an interesting history, having done business with Franco, Hitler and Pinochet. The company was established in 1920 and diversified to weapons production in the 1930s when it acquired the German Focke-Wulf company, a manufacturer of aircraft for the Nazi Luftwaffe. They are now the 19th largest defence manufacturer in the world. ITT has well established links to the supply of the Israeli armed forces. <br />  <br /> <strong>THAT DARK AND STORMY NIGHT</strong> <br />  <br /> On January the 17th 2009 the bombs had already fallen relentlessly on Gaza for three weeks (see <a href='../archive/news661.htm'>SchNEWS 661</a>). Massive, passionate demonstrations and pickets had been held in many cities around the UK and the world in protest against Israel&rsquo;s war crimes, but to no avail (see <a href='../archive/news662.htm'>SchNEWS 662</a>). A growing sense of helplessness was grabbing hold of the peace movement as the Palestinian body count stood at 1400 and rising. 300 of the dead were children. <br />  <br /> Just after midnight the six activists broke into EDO&rsquo;s premises with the aim of stopping the factory&rsquo;s production. This was an entirely accountable action where each person had pre-recorded a video in which they stated the reasons for their participation &ndash; to help dismantle the war machine from the factory floor (to view their video statements go to <a href="http://www.youtube.com/watch?v=mfa8R2AxUFg&amp;feature=related" target="_blank">www.youtube.com/watch?v=mfa8R2AxUFg&amp;feature=related</a> ). <br />   <br /> Once inside the building, the six barricaded themselves in and set to work; equipment used to make weapon components - including some used in Israeli F16 fighter jets - were trashed whilst computers were thrown out of the windows. A dummy missile was also hurled out into the car park. Once they were done they calmly waited for the police to arrest them. Three bystanders have also been charged with aiding the decommissioners. <br /> DCI Graham Pratt of Sussex Police was quoted in the Guardian, saying &ldquo;<em>Windows had been smashed and offices turned over in what I would describe as wanton vandalism, but with machinery and equipment so targeted that it could have been done with a view of bringing business to a standstill</em>&rdquo;. <br />   <br /> According to Andrew Beckett, increasingly secretive press spokesman for SMASH EDO, &ldquo;<em>Far from being a &lsquo;wanton&rsquo; act it was a pre-planned act of resistance against the massacre going on in Gaza. However, the second part of his statement was entirely accurate: the decommissioning prevented the manufacturing side of EDO from working, slowing down the murderous war machine they are a part of</em>&rdquo;. <br />   <br /> The Decommissioners supporters will be holding a vigil outside Hove Crown Court at 9am on the first day of the trial, May 17th. Supporters are encouraged to attend court but politely reminded that the decommissioners want to win this one through force of argument. <br />  <br /> There will be an &lsquo;If I had a hammer&rsquo; themed demo in support of the decommissioners on Wednesday 19th May - Bring rubber and inflatable hammers. <br />  <br /> * See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> <a href="http://www.decommissioners.co.uk" target="_blank">www.decommissioners.co.uk</a> <br />
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(764), 90); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(764);

				if (SHOWMESSAGES)	{

						addMessage(764);
						echo getMessages(764);
						echo showAddMessage(764);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


