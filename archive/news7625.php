<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 762 - 11th March 2011 - Awful Rebellion</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, lawful rebellion, british constitution group, ukip, roger hayes, council tax, tax objector" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 762 Articles: </b>
<p><a href="../archive/news7621.php">Profit Hungry</a></p>

<p><a href="../archive/news7622.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7623.php">A Liberal Dose</a></p>

<p><a href="../archive/news7624.php">The Mound That Roared</a></p>

<b>
<p><a href="../archive/news7625.php">Awful Rebellion</a></p>
</b>

<p><a href="../archive/news7626.php">Don't Mansion The War</a></p>

<p><a href="../archive/news7627.php">Ship Off, The Old Block</a></p>

<p><a href="../archive/news7628.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 11th March 2011 | Issue 762</b></p>

<p><a href="news762.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>AWFUL REBELLION</h3>

<p>
<p>  
  	Now SchNEWS has been hearing a fair bit about the whole &lsquo;Lawful Rebellion&rsquo; thing now for a year or two, picking up where 9/11 truth left off. It&rsquo;s a heady mix of the Magna Carta, contract law and monarchical anarchism (Occassionally it makes some good points about the relationship between state and individual.) But to be honest we&rsquo;d not realised quite how big it had got. On Monday 7th March, around 600 protesters attempted to &lsquo;arrest&rsquo; a judge at Birkenhead County Court. They were there to support Roger Hayes, chairman of the British Constitution Group (BCG), who was in court for a bankruptcy hearing after not paying council tax.  </p>  
   <p>  
  	Hayes attempted to prove that the enforced collection of council tax by government is unlawful because no contract has been agreed between the individual and the state. His argument is that he can represent himself as a third party in court and that &ldquo;Roger Hayes&rdquo; is a corporation and must be treated as one in the eyes of the law. This was apparently unsuccessful as he then &ldquo;asked if [the judge] was serving under his oath of office&rdquo; when Judge Michael Peake didn&rsquo;t answer this, Hayes &ldquo;civilly arrested&rdquo; him.  </p>  
   <p>  
  	The judge adjourned the case as he was bundled out of the room by court officials while supporters of Mr Hayes attempted to block the exits. Though the judge has so far escaped justice, members of BCG have said that they &ldquo;know his name and where he works, we can get back to him later.&rdquo;  </p>  
   <p>  
  	Six people (not including the judge) were arrested for assault police, breach of the peace and obstruct police.  </p>  
   <p>  
  	Roger Hayes is an ex-member of UKIP and was a candidate for the Referendum Party, receiving 1,490 votes. Following on from these successful forays into politics he got involved with the BCG. The stated aim of the BCG is, amongst other things, to get at least 1,000,000 people to engage in &lsquo;lawful rebellion&rsquo;. Although they succeeded in getting a few hundred to defend their leader, they&rsquo;ve yet to achieve all out rebellion. Much of their rhetoric has a lot going for it- &ldquo;end the bailout&rdquo;, &ldquo;we the people need to take charge&rdquo;, attacks on the corporatisation of Britain,but it is tinged with at best confused, at worst sinister, mix of monarchism, nationalism and individualism.  </p>  
   <p>  
  	In the words of their own declaration &ldquo;We, the British People have a right to govern ourselves&rdquo; good start, but apparently the problem is that &ldquo;Her Majesty Queen Elizabeth II... no longer governs us in accordance with our laws and customs, as was the situation when she was elected by the people as our Sovereign and our Head of State&rdquo;- makes perfect sense to us.  </p>  
   <p>  
  	* For more on the weird and wonderful world of lawful rebellion, see <a href="http://www.lawfulrebellion.org.uk" target="_blank">www.lawfulrebellion.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1138), 131); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1138);

				if (SHOWMESSAGES)	{

						addMessage(1138);
						echo getMessages(1138);
						echo showAddMessage(1138);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


