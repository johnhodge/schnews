<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 745 - 29th October 2010 - Cuts Both Ways</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gypsy, travellers, dale farm, austerity" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 745 Articles: </b>
<p><a href="../archive/news7451.php">Serious Organised Crime</a></p>

<p><a href="../archive/news7452.php">First Cuts Not The Deepest</a></p>

<p><a href="../archive/news7453.php">Edl: Down The Rabbi Hole</a></p>

<p><a href="../archive/news7454.php">Somerset Sabs Attacked</a></p>

<b>
<p><a href="../archive/news7455.php">Cuts Both Ways</a></p>
</b>

<p><a href="../archive/news7456.php">Naples: Load Of Rubbish</a></p>

<p><a href="../archive/news7457.php">Snogfest Vs Nazi Nonce</a></p>

<p><a href="../archive/news7458.php">The Us Itt Parade</a></p>

<p><a href="../archive/news7459.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 29th October 2010 | Issue 745</b></p>

<p><a href="news745.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CUTS BOTH WAYS</h3>

<p>
<p>
	After the violent and illegal eviction of Gypsy travellers at Hovefields, Essex in September (see <a href='../archive/news738.htm'>SchNEWS 738</a>), attention is back on the much larger nearby traveller community at Dale Farm (see <a href='../archive/news669.htm'>SchNEWS 669</a>). But an unexpected factor has come into the equation to thwart a possible eviction: due to belt-tightening, the council haven&rsquo;t got enough money for the police bill. </p>
<p>
	The cheapest thing would be to simply give planning permission for Dale Farm - but up til now, it wasn&rsquo;t about money - no expense was spared - but rather about ethnic cleansing and intolerance by the Tory council. Already &pound;2 million has been spent on legal costs and bailiffs Constant &amp; Co are on a &pound;3 million contract to evict the site. But now the police are saying their bill could be &pound;10 million. </p>
<p>
	For half that money, the council could give the Gypsies a nearby site and let them be legal. Dale Farm are currently applying for planning permission to go onto Homes and Communities Agency (HCA)-owned land at nearby Pound Lane, but are up against institutional racism and intolerance. </p>
<p>
	However, in the current climate of massive spending cuts it seems that Tony Ball, Tory leader of Basildon Council, has picked a bad time to go cap-in-hand for money. Communities Secretary Eric Pickles and Home Secretary Theresa May are both being approached by the council and Essex police to stump up the bill - but it&rsquo;ll be a battle to see which reactionary right-wing ideology wins out: savage public services cuts vs harsh retribution for those who step outside the bounds of &lsquo;normal&rsquo; society. </p>
<p>
	* See <a href="http://www.dalefarm.wordpress.com" target="_blank">www.dalefarm.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(991), 114); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(991);

				if (SHOWMESSAGES)	{

						addMessage(991);
						echo getMessages(991);
						echo showAddMessage(991);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


