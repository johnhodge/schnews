<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 729 - 2nd July 2010 - Atenco Prisoners Free</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, latin america" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 729 Articles: </b>
<p><a href="../archive/news7291.php">They Think It's All Over... Itt Is Now!!!</a></p>

<p><a href="../archive/news7292.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7293.php">Village People</a></p>

<p><a href="../archive/news7294.php">Community Guarding</a></p>

<p><a href="../archive/news7295.php">Calais Update</a></p>

<b>
<p><a href="../archive/news7296.php">Atenco Prisoners Free</a></p>
</b>

<p><a href="../archive/news7297.php">Fin Tuned Tactics</a></p>

<p><a href="../archive/news7298.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd July 2010 | Issue 729</b></p>

<p><a href="news729.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ATENCO PRISONERS FREE</h3>

<p>
<p>
	Twelve Mexican political prisoners seized in the aftermath of the 2006 Atenco uprising (see <a href='../archive/news543.htm'>SchNEWS 543</a>) have been released following a sensational high court ruling. Judges ruled that not only was there no evidence to sustain the prosecution&#39;s case but also that the charges the twelve faced - organised kidnapping - didn&#39;t even exist in Mexican law. </p>
<p>
	The victory was celebrated by activists in Mexico and around the world who have relentlessly campaigned for justice over the last four years. Outside the courthouse, Trini del Valle, wife of one of the prisoners, Ignacio del Valle, told the gathered crowd from the Popular Front in Defence of the Land (FPDT) and the Justice and Peace Committee for Atenco, &ldquo;I see tears of happiness in the eyes of my compa&ntilde;eros and I would like to say thank you to everyone, thank you to the Mexican people and international solidarity. The federal and state governments are not invincible!&rdquo; </p>
<p>
	As the High Court judges assembled on Tuesday (29th), international campaigners staged solidarity demos in Spain, Germany, the US, New Zealand and Britain. In London, activists from across the UK assembled outside the Mexican embassy and leafleted passers-by while blasting out Mexican revolutionary tunes. Following the ruling, a spokesperson for the UK Zapatista Solidarity Network, told SchNEWS, &quot;It&#39;s a fantastic example of how links across the world can make an enormous difference. There were demonstrations in so many places and I think if the supreme court of justice hadn&#39;t thought that people were watching all over the world then they would have just left them in prison.&quot; </p>
<p>
	Before the ruling the twelve had been serving sentences ranging from 31 to 112 years for their role in detaining police officers during the 2006 rebellion. The bloody clashes were sparked by an assault on local flower vendors, (see <a href='../archive/news543.htm'>SchNEWS 543</a>)who police were targeting to pave the way for a new commercial centre. Atenco residents and the FPDT came to the flower-sellers&#39; aid, driving off the police in skirmishes that saw police beaten by crowds and 15 taken hostage before being released to the Red Cross. Thousands of revenge hungry police then marched on the town armed with batons, tear gas and sub-machine guns. The residents defended themselves by building barricades and arming themselves with machetes and Molotovs. </p>
<p>
	The next day the police launched a brutal attack on the besieged town. Hundreds were savagely beaten and two people killed, including a 14-year old boy hit by a tear gas canister. Twenty-six women were subjected to horrific sexual abuse, a number of them raped. Homes were invaded by marauding coppers and around 200 people were arrested. </p>
<p>
	The rebellious residents of Atenco have been challenging the Mexican state since fighting off plans for an airport on local farm land in 2002 (see <a href='../archive/news367.htm'>SchNEWS 367</a>). Following that victory they declared the town the first &#39;autonomous municipality&#39; outside of the Zapatista zones in Chiapas. </p>
<p>
	Nearly a decade on and that spirit of rebellion is still going strong despite the oppression and injustices of the last years. The announcement of the ruling led to an impromptu town festival, while hundreds of locals and activists established a vigil outside the prison holding (1st) Ignacio del Valle Medina, Felipe &Aacute;lvarez and H&eacute;ctor Galind. There, they waited for the release of the prisoners behind a giant banner proclaiming &quot;Welcome, compa&ntilde;eros, to your town&quot;. </p>
<p>
	* See <a href="http://ukzapatistas.wordpress.com" target="_blank">http://ukzapatistas.wordpress.com</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(846), 98); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(846);

				if (SHOWMESSAGES)	{

						addMessage(846);
						echo getMessages(846);
						echo showAddMessage(846);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


