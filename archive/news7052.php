<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 705 - 15th January 2010 - Ships In The Fight</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, sea shepherd, whaling, environmentalism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 705 Articles: </b>
<p><a href="../archive/news7051.php">Wild At Heart</a></p>

<b>
<p><a href="../archive/news7052.php">Ships In The Fight</a></p>
</b>

<p><a href="../archive/news7053.php">Pyramid Schemers</a></p>

<p><a href="../archive/news7054.php">Gaza Arrests Witness Call Out</a></p>

<p><a href="../archive/news7055.php">Funky Gibbons</a></p>

<p><a href="../archive/news7056.php">Frisky Business</a></p>

<p><a href="../archive/news7057.php">Xmas-size Attack</a></p>

<p><a href="../archive/news7058.php">Bough Down</a></p>

<p><a href="../archive/news7059.php">Cat On A Squat Tin Roof</a></p>

<p><a href="../archive/news70510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th January 2010 | Issue 705</b></p>

<p><a href="news705.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SHIPS IN THE FIGHT</h3>

<p>
Since Japanese whalers sliced Sea Shepherd vessel the Ady Gil in two last week, the marine conservationists have faced a propaganda onslaught accusing them of lying, polluting and preparing to attack the whalers with bows and arrows. <br />  <br /> The Ady Gil, with the Bob Barker, had been chasing the Japanese fleet&rsquo;s mother ship, the Nisshin Maru, away from the Southern Ocean Whale Sanctuary in the Antarctic and had succeeded in bringing whaling operations to a standstill. <br />   <br /> The confrontation began when activists hurled stink bombs of rancid butter onto the deck of the  Nisshin Maru. The whalers responded by trying to force the Ady Gil away with high powered water canons. According to Captain Chuck Swift on the Bob Barker, harpoon ship the Shonan Maru No. 2 then started up suddenly and rammed the stationary Ady Gil, shearing off a eight foot section of the hull. <br />   <br /> The six crew members were rescued by the Bob Barker, but the boat could not be saved. Despite the crew of the Bob Barker&rsquo;s round the clock efforts it had taken on too much water and three days later had to be left to sink. Before abandoning , the crew boarded the boat and pumped out the fuel before it leaked into the sea. <br />  <br /> Despite video footage clearly showing the whaling ship tear into the Ady Gil, the whalers promptly fired up the propaganda machine with a statement from their PR company claiming not only had Sea Shepherd lied about the Ady Gil sinking, but also that they were polluting the environment with a fuel spill and, most bizarrely of all, that the crew had bows and arrows on board ready to launch a full on medieval assault on the whalers. <br />  <br /> Sea Shepherd Captain Paul Watson dismissed the claims, made in a statement by Glen Inwood, as, &ldquo;<em>one of the most absurd media releases I have seen this clown issue in years</em>.&rdquo; In response to photos of bows and arrows floating next to the stricken boat Captain Watson said, <em>&ldquo;I suppose the only reason for this is that guns don&rsquo;t float and he needed a &lsquo;smoking gun&rsquo; but had to settle for a &lsquo;floating arrow&rsquo;</em>.&rdquo; <br />  <br /> Unfortunately, the New Zealand government seems more than ready to believe that marine conservationists were preparing a pirate attack, Foreign Minister Murray McCully saying, &ldquo;<em>If they are going to go down there looking for trouble and determined to find it then there&rsquo;s nothing we can do to stop them, except urge them to improve their conduct</em>.&rdquo; <br />   <br /> Captain Watson responded to the comments saying, &ldquo;<em>Perhaps Murray McCully should review the video evidence before he fires off asinine statements half-cocked concerning issues he has not obviously been briefed on</em>.&rdquo; <br />   <br /> Despite the loss, Sea Shepherd operations are up and running with the Bob Barker joining the Steve Irwin in pursuit of the Whaling fleet. Captain Paul Watson said, &ldquo;<em>We now have a real whale war on our hands now and we have no intention of retreating</em>.&rdquo;  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=environmentalism&source=705">environmentalism</a>, <a href="../keywordSearch/?keyword=sea+shepherd&source=705">sea shepherd</a>, <a href="../keywordSearch/?keyword=whaling&source=705">whaling</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(617);
			
				if (SHOWMESSAGES)	{
				
						addMessage(617);
						echo getMessages(617);
						echo showAddMessage(617);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


