<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 730 - 9th July 2010 - Besetting A President</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, latin america, colombia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 730 Articles: </b>
<p><a href="../archive/news7301.php">The Hole Truth</a></p>

<p><a href="../archive/news7302.php">Decommissioners All Out</a></p>

<p><a href="../archive/news7303.php">Bog Roll Of Honour</a></p>

<p><a href="../archive/news7304.php">Village Fate</a></p>

<p><a href="../archive/news7305.php">Uzbeks Against The Wall</a></p>

<b>
<p><a href="../archive/news7306.php">Besetting A President</a></p>
</b>

<p><a href="../archive/news7307.php">Raven's Law</a></p>

<p><a href="../archive/news7308.php">Gettin On Their Wiki</a></p>

<p><a href="../archive/news7309.php">The Paper Trail</a></p>

<p><a href="../archive/news73010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th July 2010 | Issue 730</b></p>

<p><a href="news730.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>BESETTING A PRESIDENT</h3>

<p>
<p>
	The new president-elect of Colombia, Juan Manuel Santos, was met by a Downing Street picket as he arrived for a chinwag with chinless wonder David Cameron on Monday (5th). </p>
<p>
	Activists from the Colombia Solidarity campaign and the Colombian leftist opposition &ndash; the Polo Democratico &ndash; were joined by Chileans, the Movement of Ecuadorians in the UK, and the Latin American Workers Association in demanding an end to aid for the chronic human rights abusers in power in Colombia. </p>
<p>
	Santos recently defeated Green party candidate Antanus Mockus &ndash; a political outsider who surged through the polls on an anti-corruption manifesto. Before running for president, Santos had been the Defence Minister of current hard right president Alvaro Uribe. In his time working for Uribe, Santos found himself up to his neck in scandal. He was in charge of the army throughout the &lsquo;false positive&rsquo; affair, where Colombian soldiers were found to have murdered civilians and dressed them up as guerillas to claim kill bonuses. </p>
<p>
	He was also allegedly the architect of the air strike on Ecuadorian territory that killed one of the leaders of leftist guerilla group the FARC, igniting a diplomatic crisis in the process. As a result he currently can&rsquo;t set foot in Ecuador because of an outstanding arrest warrant for his part in the raid. Uribe and Santos&rsquo; political supporters have also been ravaged by the &lsquo;para-politcial&rsquo; scandal which exposed collusion between politicians from their party and paramilitary groups &ndash; groups who have admitted securing votes through violence and coercion. </p>
<p>
	Santos has pledged to continue both the military and economic policies of Uribe &ndash; a combination of militarism and neo-liberalism that has led to widespread human rights abuses and economic misery for the estimated 64% of Colombians living below the poverty line. </p>
<p>
	* Colombia Solidarity are also keeping up the pressure on BP over their abusive treatment of Colombian oil workers (see <a href='../archive/news724.htm'>SchNEWS 724</a>) with a picket outside BP HQ yesterday (8th). </p>
<p>
	* See <a href="http://www.colombiasolidarity.org.uk" target="_blank">www.colombiasolidarity.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(854), 99); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(854);

				if (SHOWMESSAGES)	{

						addMessage(854);
						echo getMessages(854);
						echo showAddMessage(854);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


