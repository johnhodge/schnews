<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 687 - 14th August 2009 - Ciggy Stubbed Out?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cigarrones, spain, andalusia, travellers, dragon festival, autonomous spaces, squatting" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 687 Articles: </b>
<p><a href="../archive/news6871.php">Fash Get The Brum Rush</a></p>

<p><a href="../archive/news6872.php">From Russia With Hate</a></p>

<p><a href="../archive/news6873.php">Notes From A Small Island  </a></p>

<p><a href="../archive/news6874.php">Celt In Action</a></p>

<b>
<p><a href="../archive/news6875.php">Ciggy Stubbed Out?</a></p>
</b>

<p><a href="../archive/news6876.php">Bad Korea Choice</a></p>

<p><a href="../archive/news6877.php">Greek Tragedy  </a></p>

<p><a href="../archive/news6878.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th August 2009 | Issue 687</b></p>

<p><a href="news687.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CIGGY STUBBED OUT?</h3>

<p>
As the British festival scene continues to assess the ramifications of the Big Green Gathering being shut down by a police campaign (See <a href='../archive/news685.htm'>SchNEWS 685</a>), SchNEWS was contacted this week by people living at the Cigarrones travellers site in southern Spain, to tell us that this site is also being clamped down on by authorities and currently in a very precarious state.&nbsp; <br />  <br /> The site, near Orgiva, Andalusia has been a popular park-up for (mostly) British travellers for 19 years, and alongside other nearby sites/camps like El Morion and Beneficio, has become a big part of the story of what happened to the British traveller/free festival scene after it was repeatedly attacked by the UK Govt - from the Battle Of The Beanfield in 1985 (See <a href='../archive/news678.htm'>SchNEWS 678</a>) through to the criminalisation of ravers and travellers by the Criminal Justice Act in 1994 (See SchNEWS 1-50). <br />  <br /> This April, as Ciggy prepared to hold the 13th annual Dragon Festival on the site, the local authorities and local landowners conspired to make the site not viable for a festival or park-up by digging some 2,500 6ft x 6ft holes in the dry riverbed land, on the pretext of planting trees &ndash; many of which have since died in the parched land. The excavations have also damaged the roots and killed several large established trees. Untaxed and unroadworthy live-in vehicles on the land were towed off and scrapped by police. <br />  <br /> The free Dragon festival did go ahead this year, although it was a tamer affair than usual as the sound system rigs were set up on another site. Afterwards, several people who owned land used for the Dragon were fined 30,000 Euros for holding an unlicensed event. Apparently it&rsquo;s been other &lsquo;posh neighbour&rsquo; Brits who own nearby land that have supported the clampdown, objecting to one of the most popular and anarchic free festivals in Europe, albeit with its reputation for being a noisy 24/7 &lsquo;avin&rsquo; it banging muntfest. <br />  <br /> Cigarrones hasn&rsquo;t been a Temporary Autonomous Zone &ndash; it&rsquo;s been a permanent one, and quite blatantly so &ndash; but the authorities and police have been winding up the pressure in recent years on the large traveller and hippy contingent that the Orgiva area has become a centre for, and getting hotter on such things as unlicensed live-in vehicles and cannabis cultivation. During the summer months sites in the area often dwindle as people head north to avoid the baking heat, but it&rsquo;s now questionable whether Cigarrones will be a viable site for those returning in the autumn. Some parts of the site have been bought and people remain there, but the rest of the site which was squatted is now more like a bomb site than a travellers site.&nbsp; <br />  <br /> If there is an agenda beyond simply clamping down on travellers, it could be that there&rsquo;s a sandstone quarry nearby, and the plan is to quarry the dry riverbed that the site sits on. Standing in the way are the parts of the site which are privately owned. It could be the end of an era at Cigarrones, but no doubt other sites will be expanded or new ones created, and hopefully the Dragon will reappear next year in another form. <br />  <br /> * See <a href="http://www.partyvibe.com/conspiracy" target="_blank">www.partyvibe.com/conspiracy</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>andalusia</span>, <span style='color:#777777; font-size:10px'>autonomous spaces</span>, <span style='color:#777777; font-size:10px'>cigarrones</span>, <span style='color:#777777; font-size:10px'>dragon festival</span>, <span style='color:#777777; font-size:10px'>spain</span>, <a href="../keywordSearch/?keyword=squatting&source=687">squatting</a>, <span style='color:#777777; font-size:10px'>travellers</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(460);
			
				if (SHOWMESSAGES)	{
				
						addMessage(460);
						echo getMessages(460);
						echo showAddMessage(460);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>