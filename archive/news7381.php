<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 738 - 10th September 2010 - Caravandals</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, travellers, essex, dale farm, constant & co, gypsy" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 738 Articles: </b>
<b>
<p><a href="../archive/news7381.php">Caravandals</a></p>
</b>

<p><a href="../archive/news7382.php">Lewes Road: Forked Off</a></p>

<p><a href="../archive/news7383.php">Hit The Road, Shac</a></p>

<p><a href="../archive/news7384.php">Jock's Trapped</a></p>

<p><a href="../archive/news7385.php">Blair Rich Project</a></p>

<p><a href="../archive/news7386.php">Alder Not Wiser</a></p>

<p><a href="../archive/news7387.php">Sky Larks</a></p>

<p><a href="../archive/news7388.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 10th September 2010 | Issue 738</b></p>

<p><a href="news738.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CARAVANDALS</h3>

<p>
<p>
	<strong>AS GYPSY SITE IN ESSEX IS EVICTED, LEAVING TRAVELLERS NOWHERE TO LEGALLY PARK...</strong> </p>
<p>
	&ldquo;<em>The police have been merciless with us, with their use of Section 61 [power to move vehicles on] on families who have just lost their homes. We have a woman who is pregnant. A few of our small children were sitting on the pavement traumatised, and the police seemed to think it was funny</em>.&rdquo; <strong>- Grattan Puxon, traveller spokesperson.</strong> </p>
<p>
	The Hovefields Gypsy/Traveller site in Essex with 50-60 inhabitants has been evicted this week. At the time of writing, a group of these families are still on the road without anywhere to stop, having been also evicted from two other sites they tried to move on to, all within 24 hours. In fact it is illegal for them to stop anywhere as a group, as they are more than six live-in vehicles. </p>
<p>
	Hovering ominously over this eviction is the extra concern is that this may be a &lsquo;warm-up&rsquo; exercise for a much larger one at the nearby site of Dale Farm, which has 1000 people and is the biggest Gypsy site in the country (see <a href='../archive/news669.htm'>SchNEWS 669</a>, <a href='../archive/news694.htm'>694</a>). The Hovefields clearance was an operation that Basildon Council seem to have carefully stage-managed &ndash; and that includes the PR, media and police, as well as the bailiffs. </p>
<p>
	After receiving a 28-day notice to quit in early August, Hovefields &ndash; a Gypsy site for twenty years - was threatened with eviction after August 31st (see <a href='../archive/news736.htm'>SchNEWS 736</a>). Then at 8am on Tuesday morning (7th), a team of over 50 bailiffs from Constant &amp; Co (more about them later) - with tow trucks, cranes, diggers and trucks carrying portable fencing - descended on the site accompanied by numerous police vans. A portacabin was installed on one of the plots to use as a base for operations &ndash; however it was also heavily used as a media centre for ITN, BBC and local media: it was essential that all media coverage was controlled from &lsquo;their&rsquo; side. </p>
<p>
	Residents were on stand-by to resist the eviction and, alongside other supporters, managed to stave off the work for around an hour by blocking the access road with cars. They were eventually dragged off and attacked by bailiffs and two protesters were arrested for Breach of the Peace - later released without charge. Also attempts to switch off the water and power to the site were thwarted for a time, before bailiffs got the upper hand. A seventy-two year old man suffered a broken nose after being smashed into a caravan. </p>
<p>
	Legal observers and monitors witnessed events, documenting numerous breaches of human rights; including failure to provide alternative housing, health and safety malpractices with heavy machinery, and the police&rsquo;s refusal to allow observers access to the site. </p>
<p>
	Once work could start, bulldozers began tearing up the caravan standings. By the end of Tuesday three families drove off the site, leaving four families who stayed the night. </p>
<p>
	The next day (8th) at 9am, police gave the remaining families an ultimatum to leave by midday, as the bailiffs continued to bulldoze plots. The four families complied, going to a nearby car park at Gardiners Way &ndash; owned by the Homes and Communities Agency (HCA) and earmarked as a possible replacement site (HCA boss Bob Kerslake supports the plight of travellers). Once there, they were threatened by police with Section 61 of CJA 1994 &ldquo;Notice To Move&rdquo; which illegalises gatherings of six or more vehicles and is the bain of all travellers. After police harassment the convoy moved off at 9pm &ndash; without a viable destination - and ending up stopping for the night at South Mimms motorway services. </p>
<p>
	Yesterday (9th) the convoy of four families came back to another HCA-owned site at Gardiners Close, and were again met with police brandishing Section 61 &ndash; forcing them to leave again. It was particularly traumatic and difficult for the families to endure three evictions in 24 hours. It is hoped that the HCA will provide some land in Essex for them, and a planning application for a site has been submitted. </p>
<p>
	This all comes after six families were evicted from Hovefields on June 29th (see pic), when Constant &amp; Co and police arrived without forewarning in the early hours of the morning and gave them one hour to go. A bulldozer smashed a building being used as a toilet and dug up the surface. Legal action is being taken regarding health and safety breaches during this eviction. </p>
<p>
	<strong>DALE FARM ALERT</strong> </p>
<p>
	One phrase heard several times from bailiffs as they performed the eviction was &ldquo;...next time...&rdquo; This is likely a reference to Dale Farm. The Tory Basildon Council are resolutely moving towards an eviction, which could possibly be the biggest ever in British history. The Council have given Constant &amp; Co a &pound;2 million contract to do the dirty work, and have for years waged a tabloid-paper fuelled publicity campaign against the site. </p>
<p>
	Dale Farm was started in the 1970s on a former scrap yard, and at the time got planning permission for 40 families to stay. But during recent decades, due to other evictions and the difficulty of living on the road, more and more have come to Dale Farm, and it&rsquo;s grown to it&rsquo;s current size. All except those original families are now facing eviction because they don&rsquo;t have planning permission to be there, despite the land being owned by the Gypsies. </p>
<p>
	Gypsies and those living nomadic and itinerant lives have always suffered from racism and persecution &ndash; right across Europe (including currently in France as widely reported in the mainstream press). </p>
<p>
	After the 1994 Criminal Justice Act and the repeal of the Caravan Sites Act 1968, local authorities were no longer obliged to provide park-up space for travellers, and the ensuing harrassment forced many to buy land just so they had a place to park &ndash; the opposite of their desire for a life on the road. This in turn led to the creation of the semi-permanent sites which exist today as refuge for hounded travellers. The trouble is that councils, despite kicking them out of everywhere else, very rarely issue planning permission for people live on these sites, and this is often the leverage used to expel them &ndash; an unofficial policy of ethnic cleansing masquerading as the enforcement of council regulations. </p>
<p>
	It must be said that settlements like Dale Farm are the consequence of a people whose way of life has been smashed by legislation, and also to some extent by the loss of the traditional livelihoods which sustained them. Of course there will be friction arising from disenfranchised people forced to live in an unintended ghetto, suffering persecution from nearby communities who don&rsquo;t see why they should be there and prompted towards ignorance and xenophobia by tabloid-paper propaganda. </p>
<p>
	A brief scan of readers&rsquo; comments on the local rag The Recorder&rsquo;s website reveals intolerance to the fact that Gypsies are there without planning permission. As if those same locals have any say whatsoever when fatcat businessmen get &lsquo;permission&rsquo; to impose whatever they want on their community. </p>
<p>
	<strong>CONSTANT BATTLE</strong> </p>
<p>
	Constant &amp; Co &ndash; the self-proclaimed &ldquo;gypsy and squatter eviction specialists&rdquo; have a long history of violence, serious human rights abuses, and are currently under investigation for breaches of safety law. Their resume includes a litany of traveller evictions in Britain, which routinely involve violently manhandling people, destroying property, digging up and trashing peoples&rsquo; land, and moving earth or concrete blocks to prevent vehicles getting back on-site. </p>
<p>
	In 2004 during the Meadowlands eviction - also in Essex - Gypsies negotiated with Constant &amp; Co to allow them to tow ten caravans to another site; instead Constant took them to a nearby lay-by and they were all torched overnight &ndash; which saw the company investigated for arson (see <a href='../archive/news439.htm'>SchNEWS 439</a>). At Bulkington Fields in Warwickshire, also in 2004, Roma travellers were booted off violently again by Constant, dwellings and land were trashed by diggers, and a court injunction prevented them returning to land they owned (see <a href='../archive/news461.htm'>SchNEWS 461</a>). </p>
<p>
	These evictions are never purely financial decisions because it would be far cheaper for councils to simply buy land for travellers to live on than pay millions more for drawn-out legal and eviction processes. Or even cheaper; simply grant more planing permission - currently they are refusing 90% of applications by travellers. But it&rsquo;s not about saving money, we already know that. <br /> 
	&nbsp;In the past few years there have been High Court cases, appeals and counter-appeals (see <a href='../archive/news669.htm'>SchNEWS 669</a>) which have stalled proceedings for Dale Farm, but after Hovefields it would appear that Constant &amp; Co are gearing up for the big one. Just what scale of operation would be required to evict 1,000 people, hundreds of vehicles, caravans, static mobile homes and more is hard to imagine. If Dale Farm is issued with a 28-day eviction notice they are ready to contest it and apply for a judicial review. If it does get to this, all are urged to get to Dale Farm and help resist the bailiffs. </p>
<p>
	* The residents of Dale Farm are holding a demo at Southend County Court on September 16th at 10am to request that the offer of housing from the Basildon Council be changed to an offer for land, in keeping with their way of life. </p>
<p>
	** See <a href="http://dalefarm.wordpress.com" target="_blank">http://dalefarm.wordpress.com</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(928), 107); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(928);

				if (SHOWMESSAGES)	{

						addMessage(928);
						echo getMessages(928);
						echo showAddMessage(928);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


