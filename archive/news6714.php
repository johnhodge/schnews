<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 671 - 3rd April 2009 - Boiling Kettle</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, g20 summit, direct action, london, bank of england, met police, royal bank of scotland" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 671 Articles: </b>
<p><a href="../archive/news6711.php">Bank Statement</a></p>

<p><a href="../archive/news6712.php">Crapping Arrest Of The Week</a></p>

<b>
<p><a href="../archive/news6714.php">Boiling Kettle</a></p>
</b>

<p><a href="../archive/news6715.php">Tents Affair</a></p>

<p><a href="../archive/news6716.php">Storming The Ramparts</a></p>

<p><a href="../archive/news6717.php">Reaching The Summit</a></p>

<p><a href="../archive/news6718.php">Ravens Art</a></p>

<p><a href="../archive/news6719.php">Spanners In The Works</a></p>

<p><a href="../archive/news67110.php">Nato Summit  Strasbourg</a></p>

<p><a href="../archive/news67111.php">Creepy Crawley</a></p>

<p><a href="../archive/news67112.php">Mal Maison</a></p>

<p><a href="../archive/news67113.php">Lady Of The Lakenheath</a></p>

<p><a href="../archive/news67114.php">Edo Briefs</a></p>

<p><a href="../archive/news67115.php">Right Off</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd April 2009 | Issue 671</b></p>

<p><a href="news671.htm">Back to the Full Issue</a></p>

<div id="article">

<div style='font-weight:bold; font-size:13px'><div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 G20 Summit Eyewitness Report 
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div></div>

<h3>BOILING KETTLE</h3>

<p>
Despite the media's apocalyptic predictions, the four horsemen (See <a href='../archive/news667.htm'>SchNEWS 667</a>) did at least make it to the Bank of England. Whether this was a good idea or not is open to question. It certainly brought a measure of mayhem to the financial heart of London, which seemed largely closed down for the duration. Our numbers were impressive - given the short notice and the media hype of extreme violence. But tactics adopted gave the Met free rein to place a huge cordon around the entire demo - the so-called kettle. <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0; width: 300" width="300px"><a href="../images/671-g20-4-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/671-g20-4-sm.jpg"  /></a></td></tr></table>As soon as the final Black Horse (ironically the one symbolising land enclosure) arrived, police lines rapidly snapped into place across the streets surrounding the plaza that the bank sits on. Unfortunately - although many did successfully make a break for it - the majority of the crowd, with little idea of what to do (unless they'd read last week's SchNEWS public order guide obviously) stood around as this manoeuvre was executed. Whilst we know that the protests were organised on very short notice, there seemed to be little aim other than simply getting into the area - there were no bust-cards, and no attempts at crowd co-ordination. <br />
 <br />
At first most seemed happy to be inside the huge kettle - a few soundsystems were blasting out and there was even a bizarre outburst of contemporary dance in front of the The Royal Exchange. As the hours wore on and the few city types caught in the circle had shown ID and got themselves extracted, it became obvious that if the police had their way no-one was getting out 'til long after dark. No water, no food, not even a toilet. The reason given? - 'to prevent a breach of the peace'. <br />
 <br />
By around half-one the kettle had been truly brought the boil and fighting had broken out along Threadneedle St. A line of police were pushed back by a crowd shouting, "Let us out". A few bottles were lobbed but even without these the cops were forced to give way to the sheer physical pressure. Alerted by the noise, support streamed over from the other exits to reinforce Threadneedle and push the cops back to the junction with Bartholomew Lane. This left the windows of Royal Bank of Scotland exposed. They were duly smashed, although rioters were outnumbered by photographers by around fifteen to one. However police lines here were too strong to breach. <br />
 <br />
At around 2.30, the crowd facing a thinner police line across Victoria St suddenly surged forward and by sheer weight of numbers pushed their way through. One of the shovers told SchNEWS, "<i>It was amazing - we were resigned to being in the kettle until midnight but the lines broke right in front of me and confused police were shouting asking each other, 'What's the plan?'</i>". Despite the rapid deployment of riot cops, possibly up to a thousand people escaped at this point. Soon the windows of HSBC on Cheapside had gone in. <br />
 <br />
SchNEWS has heard reports that others managed to sneak or blag their way out over the next few hours but during the afternoon the noose was gradually tightened with baton charges. Eyewitnesses reported a sense of panic developing inside the pen. People were not allowed out until after 8pm and only then after being photographed. <br />
 <br />
One man, Ian Tomlinson is known to have died inside the cordon. SchNEWS has heard conflicting reports as to whether he was struck by police. Perhaps a coroners inquiry into his death will expose  police tactics to public glare (unless they invoke their handy new powers to keep it all secret of course).
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bank+of+england&source=671">bank of england</a>, <a href="../keywordSearch/?keyword=direct+action&source=671">direct action</a>, <a href="../keywordSearch/?keyword=g20+summit&source=671">g20 summit</a>, <a href="../keywordSearch/?keyword=london&source=671">london</a>, <span style='color:#777777; font-size:10px'>met police</span>, <a href="../keywordSearch/?keyword=royal+bank+of+scotland&source=671">royal bank of scotland</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(272);
			
				if (SHOWMESSAGES)	{
				
						addMessage(272);
						echo getMessages(272);
						echo showAddMessage(272);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>