<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 785 - 26th August 2011 - ZAD: Breaking Da Vinci Code</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, eviction, airport expansion" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 785 Articles: </b>
<p><a href="../archive/news7851.php">Dale Farm: Fight The Power</a></p>

<b>
<p><a href="../archive/news7852.php">Zad: Breaking Da Vinci Code</a></p>
</b>

<p><a href="../archive/news7853.php">Splinter Of Discontent</a></p>

<p><a href="../archive/news7854.php">Gaddafi Ducks Out</a></p>

<p><a href="../archive/news7855.php">Atos Shrugged</a></p>

<p><a href="../archive/news7856.php">Autonomist Under Attack</a></p>

<p><a href="../archive/news7857.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 26th August 2011 | Issue 785</b></p>

<p><a href="news785.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>ZAD: BREAKING DA VINCI CODE</h3>

<p>
<p>  
  	The occupiers of the ZAD were facing court last week as the authorities began the eviction process against the anti-airport community. The ZAD -Zone d&rsquo;Am&eacute;nagement Differ&eacute;&nbsp; (dubbed the Zone A Defendre by the activists) is an area of 6 square miles in the French countryside near Nantes due to be flattened in order&nbsp; to make way for a new international airport, high-speed train and motorway (see <a href='../archive/news750.htm'>SchNEWS 750</a>). &nbsp;  </p>  
   <p>  
  	Activists held a demonstration of 300 people outside the court last Monday 17th August. The court cases were postponed until September 6th and 15th which allows more time to plan and prepare for eviction attempts.  </p>  
   <p>  
  	Despite the fact that there is already an airport near Nantes, the new &euro;580 million project has been in the planning since the sixties. As well as being another massive step back in the struggle against climate change, the building of the airport would destroy miles of beautiful countryside, including woodlands and meadows, and evict whole communities of people including around 50 farmers. Some community members gave up and moved away whilst many others have been fighting against the airport and in the defence of the land and their homes. Since the announcement of the plans, local residents, farmers and other activists have expressed their resistance through petitions, demonstrations, tractor occupations, blockades and machinery sabotage.  </p>  
   <p>  
  	The campaign got a new boost of energy in 2009 when people gathered in the ZAD for the first French climate camp. People then began occupying spaces on the threatened land creating over 20 varied protests, projects and homes. Some people determined to do whatever they can to stop the airport, others just looking for somewhere free to live during this financial crisis. People from many different places and backgrounds have come together to autonomously start community gardens, set up forest occupations, squat caravans and farmhouses. Some people are even building their own timber-frame houses. There&rsquo;s a welcome centre with a free supermarket from skipped food, a help-yourself tat shed so you can start your own projects, bike workshops, welding workshops, a bakery and libraries. More recently there has been a skill share week in April, a reclaim the land demo in May and an anti-G8/G20 camp in July.  </p>  
   <p>  
  	This environmentally catastrophic project is inevitably sold to us with the full green wash package, claiming to be &lsquo;the first airport to have a positive energy balance ...with the aim of preserving the natural state of the site and protected natural spaces&rsquo;.&nbsp; The specifics of how they actually plan to do this seemingly impossible task are, unsurprisingly, no-where to be seen. The corporate face of this is Vinci, an international transport infrastructure company, amongst other things, that also claim to have built France&rsquo;s first &lsquo;eco-motorway&rsquo;. They own many of France&rsquo;s motorways so they are the people who profit every time anyone drives on one of their paege (toll) roads. Anti-airport activists have taken advantage of this, however, by creating a popular and effective action. They hold the barrier of a pay point open allowing people to travel for free. All drivers get a flyer about what Vinci are up to and many donate money thus raising funds for the campaign whilst at the same time denting the pocket of Vinci.  </p>  
   <p>  
  	Activists from the ZAD have made a call out for people to come and get involved and help create ideas for actions and strategies against the forthcoming eviction. Their eviction wish list includes scaffolding, steel cables, barbed wire, nets, fencing, metal cutters / pliers, rope, banner material, fire extinguishers, saws, hammers, nails, gas masks, goggles, helmets, pallets, welding materials, megaphones, paint, blankets, bicycles, monsters...  </p>  
   <p>  
  	How to get there: The ZAD is north of Nantes, near the towns of Notre-Dame des Landes and&nbsp; Vigneux Grandchamp des Fontaines. It is made up of many different squats but the best place to head first is les Planchettes, the welcome centre (someone will be there to welcome everyday between 12-2pm) which is on the D28. It&rsquo;s a big farmhouse with big banners hanging above it.  </p>  
   <p>  
  	If you can&rsquo;t make it to France, Vinci also have their fingers in many pies in the UK including motorway and airport construction as well as offices across Britain which can be visited.  </p>  
   <p>  
  	* For more info visit zad.nadir.org or keep an eye on Nantes Indymedia. Email <a href="mailto:zad@riseup.net">zad@riseup.net</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1333), 154); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1333);

				if (SHOWMESSAGES)	{

						addMessage(1333);
						echo getMessages(1333);
						echo showAddMessage(1333);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


