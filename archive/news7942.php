<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 794 - 28th October 2011 - Occupy London: Loose Canon Fired</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 794 Articles: </b>
<p><a href="../archive/news7941.php">Squatting: Empty Premises</a></p>

<b>
<p><a href="../archive/news7942.php">Occupy London: Loose Canon Fired</a></p>
</b>

<p><a href="../archive/news7943.php">Run Of The Millbank</a></p>

<p><a href="../archive/news7944.php">Cuadrilla Thriller</a></p>

<p><a href="../archive/news7945.php">Wood You Believe It</a></p>

<p><a href="../archive/news7946.php">Dale Farm: Nomads Land</a></p>

<p><a href="../archive/news7947.php">Edl: Brum Fash Clash</a></p>

<p><a href="../archive/news7948.php">Poor's Pay Reigned In</a></p>

<p><a href="../archive/news7949.php">I Am Sparktacus</a></p>

<p><a href="../archive/news79410.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 28th October 2011 | Issue 794</b></p>

<p><a href="news794.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>OCCUPY LONDON: LOOSE CANON FIRED</h3>

<p>
<p>  
  	Despite threats of violence from the authorities and anti-activist propaganda from the media, Occupy London has gone from strength to strength. The leaderless representatives of the 99% celebrated their first week in occupation by taking a second site at Finsbury Square (about 5 minutes walk from the first site). The second site is still growing, and they need equipment and people. They&rsquo;ve a special callout for drivers to haul the tat they&rsquo;ve collected on site.  </p>  
   <p>  
  	The resignation of the Canon of St Pauls, Dr Giles Fraser, has grabbed everyone&rsquo;s attention.&nbsp; In his resignation statement he explained that &ldquo;I cannot support using violence to ask people to clear off the land.&rdquo;&nbsp; No wonder this has caught the mainstream media by surprise - this must be the only time that City figure has resigned on principle.  </p>  
   <p>  
  	The fake scandals, that the church &lsquo;had&rsquo; to close due to health and safety (the deadly presence of tents and stoves), and that the tents were empty at night have both unravelled due to them being total bollocks.  </p>  
   <p>  
  	Occupy LSX now boasts its own newspaper, The Occupied Times of London. The first edition came out on Wednesday, with more to follow. Read all about it here: <a href="http://occupylsx.org/?p=509" target="_blank">http://occupylsx.org/?p=509</a> .  </p>  
   <p>  
  	<em><strong>Meanwhile Outside of London...</strong></em>  </p>  
   <p>  
  	Fashionably late, but guaranteed to be fantastic darling, Occupy Brighton is holding its first General Assembly this Saturday at 2pm, Victoria Gardens.  </p>  
   <p>  
  	Unfortunately up in Occupied Glasgow things have taken a turn for the tragic. After a turnout of over 500 on their demo and a decent presence at the occupation, they looked set to defy the authorities and protest bankers&rsquo; greed. According to media reports, on Wednesday night one of the protesters was raped by a group of drunks who had invaded the site. Now Glasgow Council has ordered an eviction of the site.  </p>  
   <p>  
  	Other occupations continue both in the UK and abroad. SchNEWS&rsquo; award for &lsquo;Most Interesting Foreign Occupation of The Week&rsquo; goes to Occupy Oakland&nbsp; where particularly oppressive policing has seen tents getting routinely destroyed and one protester get a tear gas cannister fired into his face.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1403), 163); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1403);

				if (SHOWMESSAGES)	{

						addMessage(1403);
						echo getMessages(1403);
						echo showAddMessage(1403);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


