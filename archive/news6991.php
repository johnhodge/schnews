<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 699 - 13th November 2009 - Mexican Wave</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, latin america, mexico, workers struggles, privatisation, strike" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://natowc.noflag.org.uk/"><img 
						src="../images_main/nato-pa-09-banner.png"
						alt="Smash NATO"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 699 Articles: </b>
<b>
<p><a href="../archive/news6991.php">Mexican Wave</a></p>
</b>

<p><a href="../archive/news6992.php">Veg Of  Darkness</a></p>

<p><a href="../archive/news6993.php">Corporal Punishment</a></p>

<p><a href="../archive/news6994.php">Extermi-nato</a></p>

<p><a href="../archive/news6995.php">Big Peace Up</a></p>

<p><a href="../archive/news6996.php">Bad Apples</a></p>

<p><a href="../archive/news6997.php">Cross Words</a></p>

<p><a href="../archive/news6998.php">Unfasten Yer Seatbelts</a></p>

<p><a href="../archive/news6999.php">Tomlinson Vigil</a></p>

<p><a href="../archive/news69910.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/699-mexican-wave-lg.jpg" target="_blank">
													<img src="../images/699-mexican-wave-sm.jpg" alt="Mexicans vote in formation to go on strike" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 13th November 2009 | Issue 699</b></p>

<p><a href="news699.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MEXICAN WAVE</h3>

<p>
<strong>Around 200,000 workers, teachers, students, unionists, farmers and social campaigners shut  Mexican cities down on Wednesday (12th) in a national strike against the military backed privatisation of a national power company.</strong> <br />  <br /> On October 10th, 6000 federal police and soldiers occupied various sites of power company Luz y Fuerza around the country and summarily sacked the 44,000 strong workforce. Mart&iacute;n Esparza, leader of the Mexican Electrical Workers Union (SME) said, &ldquo;<em>They came by night, like bandits, like cowards, and barked an order. They thought that they were going to wipe us out, but here there is the conscience of more than 100 years of the SME movement</em>.&rdquo; <br />  <br /> In Mexico City the day&rsquo;s 12 hours of action began with a rally outside Luz y Fuerza. With thousands of other protesters assembling around at points around the city, most of the main roads were shut down and blockaded. Students occupied buildings at several universities, provoking an aborted attempt to flush them out by police. <br />  <br /> As the protesters began their march on the convergence point of the Zocalo - Mexico City&rsquo;s main square - there were several confrontations with police, who tried to disperse the crowds with tear gas and by firing in the air over marcher&rsquo;s heads. Three police were injured in the clashes and 12 protesters were arrested. <br />    <br /> There were also marches and pickets around the country and a number of blockades of major transit routes. Maria Pi&ntilde;ada de Zelaya of the People&rsquo;s Front in Defence of the Earth in Atenco (FPDTA), who was at the road blockade in Texcoco, said, &ldquo;<em>Today we are exercising our rights, the right to be free, the right to our roads. The rich have deceived us, they&rsquo;ve deceived everyone, day after day, through congress, through the senators, they take away these rights</em>.&rdquo;   <br />  <br /> The strike followed weeks of actions against the closure. A march of over a hundred thousand in Mexico City was followed by pickets in Tula, Tulancingo, and Juandho and an attempt to seize a substation in Pachuca. On October 24th Union members and social movements formed the National Assembly of Popular Resistance (CEND) to co-ordinate opposition to the government. <br />   <br /> Thousands of federal police supported by at least 10,000 police reserves and 3,000 soldiers have maintained control over the 100 facilities. The plants have been kept running by 3,000 workers brought in from the other state-owned power company, the Federal Electrical Commission (CFE) and by 800 engineers and technicians provided by the army. <br />   <br /> Since the plants came under police control, localised blackouts have left parts of Mexico City and other cities and towns without power for hours at a time, with hundreds of factories unable to function. <br />  <br /> The government claims Luz y Fuerza was economically non-viable - a financial black hole - with bloated payrolls, inherited jobs and massive pension payouts responsible for $42billion a year subsidies. <br />  <br /> The claim is denied by union leaders, who say CFE, which produces 95% of Mexico&rsquo;s electricty - 45% for private companies, was selling electricity to multinational companies and to Luz y Fuerza at a profit. The government was then forcing Luz y Fuerza to sell that electricity on to other companies and domestic consumers at a lower price. <br />  <br />  SME leader Jose Hernandez said, &ldquo;<em>So the more it sells, the more it loses...its absurd, one government company selling to another...and then the government says it has to subsidise Luz y Fuerza with a lot of money, and it tells everyone it&rsquo;s to pay the so called high salaries of the workers and the retirement payments, which is not true. Our salary represents a bit less than one third of those subsidies, really the subsidies are for the big companies and for the domestic consumers</em>.&rdquo; <br />  <br /> Instead, the union suggest the move was provoked by SME blocking the governments attempts to take control of Luz y Fuerza&rsquo;s fiber optic infrastructure. Fibre optic technology provides television, internet and telephone service on the same line when cable is installed on any normal domestic or low-voltage electrical line. Luz y Fuerza served around 25% of Mexico&rsquo;s population with these lines - including Mexico City, which is responsible for around 35% of Mexico&rsquo;s GDP. With the potential to bring fibre optic technology to this section of Mexican society, Luz y Fuerza&rsquo;s network is worth a fortune. <br />  <br /> In 1999, the Mexican government gave Spanish company WL Comunicaciones a permit to install, operate, and commercialise Luz y Fuerza&rsquo;s network to provide fiber optic services. SME claim the various agreements signed with WL have been nulled due to government legislation and WL not upholding its end of the deal. <br />  <br /> In June, Luz y Fuerza executives and SME submitted an application to provide fibre optics through Luz y Fuerza&rsquo;s infrastructure. The proposal only covered the existing network and would have allowed WL to install and operate fibre optics in the power lines that don&rsquo;t currently have it - the majority of the network. Still, it would have allowed Luz y Fuerza to provide a service to its customers that would have competed with the telecommunications giants Telmex and Cablevision - both of which&nbsp;have monopolised their markets. The government ignored the proposal. <br />  <br /> The SME say the contradiction in shutting down Luz y Fuerza for being uneconomical while refusing to consider its potential in profitably and competitively providing a service highlights the underlying motivation for the government&rsquo;s actions. Especially when three of the ruling party&rsquo;s most prominent politicians have major stakes in WL. <br />    <br /> Sergio Espinal Garcia of CEND said, &ldquo;<em>For us, it is clear that the primary objective of this blow against us was to fix the privatisation of all areas of electrical production. With this plan, the Federal Electricity Commission wants this small group of businessmen to control the entire network so they can continue to develop their enormous fortunes in the middle of this tremendous national crisis, which they want to load onto the backs of the people</em>.&rdquo; <br />  <br /> * <a href="http://mywordismyweapon.blogspot.com" target="_blank">http://mywordismyweapon.blogspot.com</a> <br />  <br /> * <a href="http://upsidedownworld.org" target="_blank">http://upsidedownworld.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=latin+america&source=699">latin america</a>, <a href="../keywordSearch/?keyword=mexico&source=699">mexico</a>, <a href="../keywordSearch/?keyword=privatisation&source=699">privatisation</a>, <a href="../keywordSearch/?keyword=strike&source=699">strike</a>, <a href="../keywordSearch/?keyword=workers+struggles&source=699">workers struggles</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(559);
			
				if (SHOWMESSAGES)	{
				
						addMessage(559);
						echo getMessages(559);
						echo showAddMessage(559);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>