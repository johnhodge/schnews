<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 775 - 17th June 2011 - Greece: No Cash in the Attica</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, riots, strike, eu" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 775 Articles: </b>
<p><a href="../archive/news7751.php">Flaming June?</a></p>

<b>
<p><a href="../archive/news7752.php">Greece: No Cash In The Attica</a></p>
</b>

<p><a href="../archive/news7753.php">Mexico In A Narcho-chy: Eyewitness Report</a></p>

<p><a href="../archive/news7754.php">Into Valley Of Darkness</a></p>

<p><a href="../archive/news7755.php">Soas I Was Saying</a></p>

<p><a href="../archive/news7756.php">Keepin It Montreal</a></p>

<p><a href="../archive/news7757.php">Thin End Of The Wedges</a></p>

<p><a href="../archive/news7758.php">Schnews In Brief</a></p>

<p><a href="../archive/news7759.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th June 2011 | Issue 775</b></p>

<p><a href="news775.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GREECE: NO CASH IN THE ATTICA</h3>

<p>
<p>  
  	On Wednesday(15th)&nbsp; rioting broke out in Athens as the Greek government is in the process of passing yet another new batch of austerity measures as the country&rsquo;s economy goes down the pan. With youth unemployment at 40 per cent, there was no shortage of angry youth available to join thousands of other unionists, activists and er pissed off people in besieging the parliament building&nbsp; in violent clashes with security forces. The riots coincided with a 24-hour general strike of public sector workers.  </p>  
   <p>  
  	The recent protests have been against the latest &pound;25 billions-worth of planned cuts, meaning further significant job losses and a mass programme of privatisation. This is despite everyone knowing that Greek elite have has run up their national credit card so high to the Euro-loan sharks that it will never be in a position to pay off it&rsquo;s debt, and can only be ground into the dirt in vain attempts to do so. This delaying the inevitable of outright default or at least &lsquo;serious restructuring&rsquo; just punishes the Greek people further. &nbsp;  </p>  
   <p>  
  	Bowing to some of the pressure, and with several politicians from his own of his own Labour-esque&nbsp; Pasok party refusing to pass the cuts, Prime Minister George Papandreou has made the desperate play of offering to resign. He thinks forming a new &ldquo;unity&rdquo; coalition with the right-wing party may bring some parliamentary stability and renewed legitimacy for the austerity programme.  </p>  
   <p>  
  	He&rsquo;s somewhat missing the glaring point that what the masses now want is an end to austerity, not just the government who authorised it all, holding the rights of German banks and pension funds in higher regard than its own people&rsquo;s welfare. And how much of all that profligate debt was sucked up by the rich elite in the first place? &nbsp;  </p>  
   <p>  
  	Not a citizenry known for its passivity, its likely Greece is entering a summer of serious discontent that will exceed the unrest of 2010 which saw brutal clashes with police at demonstrations, walk-outs and rolling strikes by unions, all of whom have rejected the government&rsquo;s proposals.  </p>  
   <p>  
  	But the biggest problem facing the government is not the strength of feeling against the cuts or a lack of alternatives &ndash; but it&rsquo;s complete subservience to the whims of international institutions. Tied into the eurozone, and having had its autonomy kicked into the gutter by restrictive IMF loan structural conditionalities &ndash; signed into last February when the first massive public protests began &ndash;&nbsp; public opinion is now the last thing informing Greek politics.  </p>  
   <p>  
  	When Greece was bailed out last spring, the IMF and EU countries had to pump money into the &lsquo;junk&rsquo; debt rated country in order to stop it pulling the rest of the eurozone down in international markets. This meant several European country&rsquo;s banks in particular &ndash; such as those of France, Germany, the UK and Portugal &ndash; are now relying on Greece&rsquo;s recovery or those debts won&rsquo;t be serviced.  </p>  
   <p>  
  	The lack of any such recovery over the last year has led to a new crisis. Similar to the events leading up to the 2008/09 credit crunch, the prospect that the billions owed by Greece might never reappear has meant more aid and more loans in the form of a second bail-out to avoid a capital flight away from the European markets, and more tax-payer fronted pay-outs.  </p>  
   <p>  
  	The rescue package, conditional on the government passing the harsh austerity measures, would allow Greece to pay off its creditors until September. Greek insolvency would plunge its creditors into their own crisis, forcing them to tax their citizens to make up for the deficits.  </p>  
   <p>  
  	A more dramatic possibility is that Greece could pull out of the euro altogether. Sending shock waves through global markets, and having to rebuild its economy, the future of Greece might signal a change in attitude towards the power of global capital. However should the state turn around to tell the EU/IMF to f*** off and return to the drachma, its newly insular economic future would be equally bleak.  </p>  
   <p>  
  	Initially seen as a cure-all political and economic project, the European superstate now hangs in the balance &ndash; and there&rsquo;s no provision for states deciding to renounce membership. What was thought of as a way to unite the nations may now be the reason for a fall-out of massive proportions.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1248), 144); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1248);

				if (SHOWMESSAGES)	{

						addMessage(1248);
						echo getMessages(1248);
						echo showAddMessage(1248);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


