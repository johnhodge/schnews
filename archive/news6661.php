<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 666 - 13th February 2009 - The Watchtower</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, surveillance, id cards, fitwatch, forward intelligence team, big brother, civil liberties, indymedia, free speech" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 666 Articles: </b>
<b>
<p><a href="../archive/news6661.php">The Watchtower</a></p>
</b>

<p><a href="../archive/news6662.php">...from The Book Of Schrevalations</a></p>

<p><a href="../archive/news6663.php">The Apes Of Wrath</a></p>

<p><a href="../archive/news6664.php">My Bloody Valentine</a></p>

<p><a href="../archive/news6665.php">Sounding Off</a></p>

<p><a href="../archive/news6666.php">Keep It Spikey</a></p>

<p><a href="../archive/news6667.php">...and Finally...</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/666-obama-lg.jpg" target="_blank">
													<img src="../images/666-obama-sm.jpg" alt="I told you, we need more than a miracle..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 13th February 2009 | Issue 666</b></p>

<p><a href="news666.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>THE WATCHTOWER</H3>

<p>
<b>NO NEW REVELATIONS FOR REGULAR SCHNEWS READERS AS WE LOOK AT THE LATEST CAMPAIGN FOR CIVIL LIBERTIES</b> <br />
 <br />
It shows how bad things have got for freedom in this country if we&#8217;re pinning any hopes on the moderate left. They&#8217;re finally waking up to the wholesale destruction of civil liberties by the Zanu-Labour juggernaut. Freedom is suddenly back on the agenda, despite it being on SchNEWS&#8217;s for the last fourteen years. <br />
 <br />
This week saw the publication of the Constitution Committee of the House of Lords report &#8216;<b>Surveillance: Citizens and the State</b>&#8217;.  The report noted that surveillance had become &#8220;<i>an inescapable part of life in the UK,</i>&#8221; and called it &#8220;<i>one of the most significant changes in the life of the nation since the end of the Second World War</i>&#8221;. The report goes on to document the massive extension of databases such as: ContactPoint &#8211; records on every child born in England and Wales. (<a href='../archive/news452.htm'>SchNEWS 452</a>); The computerization of NHS records (<a href='../archive/news481.htm'>SchNEWS 481</a>); ID Cards (<a href='../archive/news24.htm'>SchNEWS 24</a>, <a href='../archive/news454.htm'>454</a>, <a href='../archive/news466.htm'>466</a>, <a href='../archive/news580.htm'>580</a>);  RFID (<a href='../archive/news415.htm'>SchNEWS 415</a>, <a href='../archive/news616.htm'>616</a>) etc, etc. &#8211; enough to make you wonder how many of their Lordships are loyal SchNEWS readers. <br />
  <br />
Even SchNEWS&#8217; local MP David Lepper has hopped on the bandwagon after a police Forward Intelligence Team installed themselves opposite the Cowley Club during the Earth First! Winter Moot last weekend, taking photos of everyone entering and leaving. In a broadside attack the local rag the Argus lambasted their actions as &#8220;<i>ham-fisted scare tactics</i>&#8221;. <br />
 <br />
Girding their loins for the fight is the Convention for Modern Liberty &#8211; an odd coalition spanning Liberty and Amnesty International to the Countryside Alliance (wonder if our fox-hunting friends will be campaigning for the repeal of the Criminal Justice Act?).  They&#8217;re hosting meetings across the country on 28th February &#8211; with a massive backslapping fiesta in London for yer Tony Benns, Shami Chakrabartis and Yasmin Alibhai Browns. But as ever the question remains - liberty for who? The devil is as ever in the details. Henry Porter, Guardian columnist and convention co-director says for example &#8220;<i>no one is questioning the need for the state</i>&#8221;.  But the protest movements that bear the brunt of new repressive legislation: the climate camps, the animal liberationists, No Borders activists, the anti-militarist movement etc are in fact largely anti-state. The group most heavily affected &#8211; the muslim youth radicalised by the West&#8217;s assault on the Arab world have no say in the British state whatsoever. <br />
 <br />
And it&#8217;s these movements that are continually targeted. There were &#8216;revelations&#8217; in last weeks Mail on Sunday of a new police unit (the Confidential Intelligence Unit) to tackle &#8216;domestic extremism&#8217;. This won&#8217;t come as any surprise to anyone involved in &#8216;domestic extremism&#8217; (once known as &#8216;protest&#8217;).  <br />
 <br />
The job advert for the head of the CIU that served as the Mail&#8217;s source for the story said the post involved the management of &#8220;the covert intelligence function for domestic extremism&#8221;. The fact that the CIU post is advertised by ACPO (Association of Chief Police Officers) a private company free from scrutiny is incidental. But Special Branch have openly admitted to a long history of infiltration of the union movement, the anti-apartheid campaigns and more recently the anti-roads and animal rights movements. <br />
 <br />
The trouble with liberal outrage is that it&#8217;s so predictable. It&#8217;s OK to defend the free speech and movement of people who represent no threat to the status quo. If anyone&#8217;s in a position to effect actual radical change however then it&#8217;s OK for the gloves to come off.  Will the Convention for Modern Liberty or the House of Lords be troubling themselves with the vicious sentences handed down to animal rights activists? Or will they be too busy cosying up to their fox-hunting pals? <br />
 <br />
The recent seizure of an Indymedia Server (<a href='../archive/news663.htm'>SchNEWS 663</a>) and the subsequent arrest of the system administrator says more about the state of liberty than any convention. On Monday Kent Police arrested a man in Sheffield under the Serious Crime Act 2007 in relation to the recent Indymedia server seizure. His home was searched and computers  despite having neither technical, administrative nor editorial access to the Indymedia UK website. All he did was host its server. Kent police claim that they are after the IP address (despite knowing Indymedia do not log IP addresses) of the poster of two anonymous comments to a report about a recent animal liberation court case, which included personal details of the Judge. <br />
 <br />
Meanwhile some are fighting back &#8211; One F.I.T watcher (see <a href='../archive/news639.htm'>SchNEWS 639</a>) has just been convicted by magistrates of obstructing police for getting in the way of cameras last June. This judgement runs completely contrary to an earlier judicial review saying police had no power to remove obstructions to their view. However during the trial police were forced to admit that they take photos for databases and cross reference photos of different political views, of course this has been known for years but it&#8217;s good to have it from the horse&#8217;s mouth. <br />
 <br />
Our movements are under attack. It&#8217;s useful to have allies but ultimately...they have their agendas and we have ours...
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=big+brother&source=666">big brother</a>, <span style='color:#777777; font-size:10px'>civil liberties</span>, <a href="../keywordSearch/?keyword=fitwatch&source=666">fitwatch</a>, <a href="../keywordSearch/?keyword=forward+intelligence+team&source=666">forward intelligence team</a>, <span style='color:#777777; font-size:10px'>free speech</span>, <a href="../keywordSearch/?keyword=id+cards&source=666">id cards</a>, <span style='color:#777777; font-size:10px'>indymedia</span>, <a href="../keywordSearch/?keyword=surveillance&source=666">surveillance</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(222);
			
				if (SHOWMESSAGES)	{
				
						addMessage(222);
						echo getMessages(222);
						echo showAddMessage(222);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>