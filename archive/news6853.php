<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 685 - 27th July 2009 - Still Kemping It Up</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, starbucks, kemptown, tescos, titnore, worthing, keep brighton unique" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 685 Articles: </b>
<p><a href="../archive/news6851.php">The Hippy, Hippy Shakedown</a></p>

<p><a href="../archive/news6852.php">Still Camping It Up</a></p>

<b>
<p><a href="../archive/news6853.php">Still Kemping It Up</a></p>
</b>

<p><a href="../archive/news6854.php">Breaking Wind</a></p>

<p><a href="../archive/news6855.php">Testing Times</a></p>

<p><a href="../archive/news6856.php">Calais Abouts</a></p>

<p><a href="../archive/news6857.php">La Lutte Continue</a></p>

<p><a href="../archive/news6858.php">Twisty Tierney   </a></p>

<p><a href="../archive/news6859.php">Republic Gone Bananas</a></p>

<p><a href="../archive/news68510.php">Saving It For An Iran-y Day</a></p>

<p><a href="../archive/news68511.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Monday 27th July 2009 | Issue 685</b></p>

<p><a href="news685.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>STILL KEMPING IT UP</h3>

<p>
This week has seen the opening of the third corporate-run supermarket on St James&rsquo; Street in Kemptown, Brighton. In response Campaign group Keep Brighton Unique organised a demonstration calling for a boycott of the new Tesco store last Wednesday (22nd). <br />  <br /> Tesco recently cancelled plans to open a 100,000 sq ft store on London Road, Brighton, due to a locally organised action group&rsquo;s campaign of opposition during the consultation. The group, Another London Road, have been acting continuously to preserve Brighton from multinational development. Anti-corporate campaigners were hopeful when the St James&rsquo; St Tesco was initially refused an alcohol licence  on the grounds that it was in an booze control area within the city centre. However, magistrates bowed to the big business bullies on appeal and granted the licence. <br />  <br /> Kemptown, renowned for its quirky, individual and unique atmosphere as well as its independent traders and privately run shops, now has the dubious honour of having a Morrison&rsquo;s, Co-op, Tesco and Starbucks all along one stretch of high street.&nbsp; <br />  <br /> Despite a recent set back in their campaign against Starbucks, as the council accepted the questionable evidence of 51% of products being sold for take-out and allowed Starbucks to continue trading on a retail licence (<a href='../archive/news679.htm'>SchNEWS 679</a>), Keep Brighton Unique will be continuing to hold monthly protests outside various Starbucks in the city. To date, this rowdy bunch have collected over 3,000 anti-Starbucks signatures and staged weekly protests for more than a year.&nbsp; <br />  <br /> The next Keep Brighton Unique demo is midday 1st August outside the St James&rsquo; Street Starbucks, then on the first Saturday of every month. <br />  <br /> The protesters were joined last Wednesday by another group campaigning against a Tesco in Titnore Woods (near Worthing), where groundwork has started without full planning permission on an area with ancient woodland and that is at severe risk of flooding. Protesters have been camped in the nearby woods since May 2006 (See <a href='../archive/news648.htm'>SchNEWS 648</a>). Help is still needed with the camp. <br />  <br /> * Info on the London Road campaign group: <a href="http://anotherlondonroad.googlepages.com" target="_blank">http://anotherlondonroad.googlepages.com</a> <br />  <br /> * How to get involved in the anti-Titnore Tesco campaign:  <a href="http://www.protectourwoodland.co.uk" target="_blank">www.protectourwoodland.co.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>keep brighton unique</span>, <span style='color:#777777; font-size:10px'>kemptown</span>, <a href="../keywordSearch/?keyword=starbucks&source=685">starbucks</a>, <a href="../keywordSearch/?keyword=tescos&source=685">tescos</a>, <a href="../keywordSearch/?keyword=titnore&source=685">titnore</a>, <a href="../keywordSearch/?keyword=worthing&source=685">worthing</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(417);
			
				if (SHOWMESSAGES)	{
				
						addMessage(417);
						echo getMessages(417);
						echo showAddMessage(417);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>