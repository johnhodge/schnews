<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 791 - 7th October 2011 - NHS Direct Action</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 791 Articles: </b>
<b>
<p><a href="../archive/news7911.php">Nhs Direct Action</a></p>
</b>

<p><a href="../archive/news7912.php">Welling Up</a></p>

<p><a href="../archive/news7913.php">Angel Delight</a></p>

<p><a href="../archive/news7914.php">Squat A Waste Of Time</a></p>

<p><a href="../archive/news7915.php">Core Values</a></p>

<p><a href="../archive/news7916.php">Factory Finish?</a></p>

<p><a href="../archive/news7917.php">First We Take Manhattan</a></p>

<p><a href="../archive/news7918.php">'cos I'm Worthing It</a></p>

<p><a href="../archive/news7919.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th October 2011 | Issue 791</b></p>

<p><a href="news791.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NHS DIRECT ACTION</h3>

<p>
<p>  
  	<strong>AS SchNEWS TAKES THE TEMPERATURE OF RESISTANCE TO HEALTH PRIVATISATION&nbsp;&nbsp; </strong>  </p>  
   <p>  
  	On Sunday, thousands are expected to blockade Westminster Bridge in a &lsquo;sick-in&rsquo; blockade spearheaded by the relentlessly organised UK Uncut. The &ldquo;Block the Bridge, Block the Bill&rdquo; action is taking place before the health and social care (read NHS privatisation) bill goes to the House of Lords on October 11th.  </p>  
   <p>  
  	This is set to be the first mass protest against the selling-off of the NHS, despite the bill having gone through the Commons at the beginning of last month. Why has it taken us so long? Were we too busy watching the eurozone collapse, travellers not getting evicted and hoodlums nicking shit from JD Sports?  </p>  
   <p>  
  	Although this is the first large-scale act of civil disobedience, that&rsquo;s not to say the bill&rsquo;s had a smooth running so far. In fact, it&rsquo;s been slammed from all directions &ndash; and its not hard to see why. There&rsquo;s yet to be one compelling argument made for the whole-scale organisational changes; they are totally unpopular with the public; they&rsquo;re vehemently and vocally opposed from within the medical establishment; and they&rsquo;ve got no redeeming features in terms of quality of care or efficiency.  </p>  
   <p>  
  	Just a few days ago, the BMA (British Medical Association) again called for the bill to be scrapped, stating their fears that many of the UK&rsquo;s hospitals would close under the new laws.  </p>  
   <p>  
  	The government would effectively lose responsibility for providing a universal health service. <br />  
  	A recent study in the Journal of the Royal Society of Medicine showed that the NHS is the most efficient health service in the world in terms of lives saved per pound. But performance doesn&rsquo;t matter when a heady mix of money-making potential and neo-con ideology rules. &ldquo;The NHS is a sixty year mistake&rdquo; said tory MEP Daniel Hannan, speaking on Fox News in 2009. In the same year MP Oliver Letwin warned: &ldquo;The NHS will not exist within five years of a Conservative victory&rdquo;.  </p>  
   <p>  
  	That&rsquo;s not to say that the NHS could continue on its merry way as it currently stands given our semi-bust financial position: something had to give. We have a growing, ageing population, absorbing environmental health-harming chemicals over the course of several decades, and selfishly staying alive longer with the help of newly developed expensive drugs. Therefore the demand for - and the cost of - the NHS has risen exponentially.&nbsp;  </p>  
   <p>  
  	It&rsquo;s with the NHS is under such pressure, these changes couldn&rsquo;t come at a worse time. The abolition of the current service commissioning bodies, Primary Care Trusts, which the government portrays as useless bureaucracy &ndash;&nbsp; will actually rid the organisation of experienced managers at the time when the whole system is being pushed at breakneck speed into total organisational chaos. Chaos might be desirable at a punk gig, but not in a hospital. If the reforms are bulldozed through, there&rsquo;s a real risk of big care failures.  </p>  
   <p>  
  	And even if, by magic, the transition to G.P-led commissioning was as smooth as a pre-surgery shave, then the savings probably wouldn&rsquo;t match the hype. Because most G.Ps have no interest in being part of commissioning &lsquo;consortia&rsquo; (even though the word makes them sound all important, like), so it&rsquo;s private companies waiting in the wings to lap up the taxpayer dosh to provide the admin/manegerial support stuff they don&rsquo;t want to do.  </p>  
   <p>  
  	Tory health minister Andrew Lansley&rsquo;s answer to the cost conundrum, a continuation of Blair and Major before him, is that competition &ndash; in the form of outsourcing to the private sector - will make all service providers push down costs as much as possible, so the whole &lsquo;universal health care&rsquo; ideal will be cheaper.  </p>  
   <p>  
  	What is missing from the Big Idea is a simple understanding of the capitalist dynamic. To cut costs, several techniques are generally employed. One is cutting wages, and the number of people on the pay roll, as much as you can. No wonder docs and nurses are pissed off &ndash; and should it happen, they&rsquo;ll have one hell of a lot less clout to organise as a workforce for their rights when they&rsquo;ve been divided into endless private company employees, rather than organising en masse when they&rsquo;re all directly employed by the government. Another is to decrease the quality of services. And don&rsquo;t forget the loss of a central body to negotiate demand lower supply prices in return for&nbsp; higher volume deals. Outsourcing to the private sector just means they&rsquo;ll do a worse job, using lower resources, but costing the same or more as they improve the health of their shareholders&rsquo; wallets.  </p>  
   <p>  
  	The government has been glossing over the evidence that privatisation tends to result in falling standards and extra costs - like in the US, which has the most inefficient system in the world &ndash; while emphasising &ldquo;choice&rdquo; and &ldquo;patient empowerment&rdquo;. But when you need a broken leg plastered or a dose of chemotherapy, not many people complain that they want more &lsquo;choice&rsquo; or &lsquo;empowerment&rsquo; in where they get it from. They just want the first specialist they meet to help them. Yet this flimsy rhetoric constitutes the entire propaganda with which the Tories are trying to smash a cornerstone of the welfare state. The other motive &ndash; private profit - was let slip by Lord Howe when he said the Bill represented &lsquo;huge opportunities&rsquo; for private companies.  </p>  
   <p>  
  	Yep, the private health lobby has been busying bribing its way into position to attack our national treasure for years. Back in March, Corporate Watch published a report on the kind of shady characters and mutually-beneficial arrangements that form the backdrop of the NHS reform. To give just a snapshot, Andrew Lansley received funding from the wife of John Nash, former chairman of Care UK, a company that already operates various NHS clinics and treatment centres around the country. Lord Carter, head of the NHS&rsquo;s Competition and Cooperation Panel, is an advisor to Warburg Pincus International Ltd, a private equity firm with &lsquo;significant investments&rsquo; in the healthcare industry.  </p>  
   <p>  
  	And are these companies going to at least provide half-decent frontline services, while they skim tax money into shareholder bank accounts? As an example, somewhere between ten and twenty hospitals have been earmarked for takeover by German company Helios. The no2tories blog has dug up some interesting info about Helios&rsquo; operations in Germany: In June, the company&rsquo;s clinics were raided by 150 cops. Why? They were under suspicion of allowing under-qualified assistant doctors to conduct medical investigations, then passing them off as being done by chief medics, for the last three years. Then there&rsquo;s the overall patient satisfaction at Helios hospitals - recorded as averaging 3 out of 6 on public feedback websites, with at least four major clinics scoring a measly 2 out of 6. Then there&rsquo;s the way they&rsquo;ve set up private clinics within their public clinics, to side-step statutory charging guidelines and rip off the ill and vulnerable that little bit more.  </p>  
   <p>  
  	Need any more reasons to hit the streets of London on Sunday? See <a href="http://www.ukuncut.org.uk" target="_blank">www.ukuncut.org.uk</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1378), 160); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1378);

				if (SHOWMESSAGES)	{

						addMessage(1378);
						echo getMessages(1378);
						echo showAddMessage(1378);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


