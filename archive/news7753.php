<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 775 - 17th June 2011 - Mexico in A Narcho-chy: Eyewitness Report</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, mexico, drugs" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 775 Articles: </b>
<p><a href="../archive/news7751.php">Flaming June?</a></p>

<p><a href="../archive/news7752.php">Greece: No Cash In The Attica</a></p>

<b>
<p><a href="../archive/news7753.php">Mexico In A Narcho-chy: Eyewitness Report</a></p>
</b>

<p><a href="../archive/news7754.php">Into Valley Of Darkness</a></p>

<p><a href="../archive/news7755.php">Soas I Was Saying</a></p>

<p><a href="../archive/news7756.php">Keepin It Montreal</a></p>

<p><a href="../archive/news7757.php">Thin End Of The Wedges</a></p>

<p><a href="../archive/news7758.php">Schnews In Brief</a></p>

<p><a href="../archive/news7759.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th June 2011 | Issue 775</b></p>

<p><a href="news775.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>MEXICO IN A NARCHO-CHY: EYEWITNESS REPORT</h3>

<p>
<p>  
  	Life in Ciudad Juarez, Mexico seems pretty cheap. A city the size of Gaza (1.3million), physically separated from its neighbours in the rich world by an apartheid fence, it had a murder toll of 3,000 in 2010 - and 2011 has so far proved to be just as violent. To put it in some perspective that&rsquo;s like two Operation Cast Leads every year.  </p>  
   <p>  
  	Both the police and the state prosecutor&rsquo;s office (something like the CPS here) have a policy of not investigating murders. More than official lethargy or the result of intimidation by the narcos, this is a case of protecting their own. The soldiers, city police and federal police that patrol the streets are in a literal sense the real criminals. The organised murders, robberies, kidnappings and major drug deals, are generally committed by members of the armed and uniformed services. In Juarez they are known as &lsquo;La Linea&rsquo; - an semi-formal conspiracy of active and ex police who do the dirty work for the cartels.  </p>  
   <p>  
  	To quote on anonymous community worker in a barrio in western Juarez: &ldquo;<em>So to deal with the insecurity they billeted the army here. It was straight after that that the banks round here started getting knocked off, each time the robbers had military gear and military style clothes</em>.&rdquo;  </p>  
   <p>  
  	As a response, many residential neigbourhoods are literally closing themselves off from the problem; gated communities have become the norm for all but the poorest areas, chopping up streets and transforming the city into a series of suburban ghettos. Although many people hate the idea, when you hear the stories its hard to argue with. The same goes for the number of armed citizens here; after the 2nd, 3rd or 4th robbery, many people - old, young, men, women - keep a loaded gun under their bed. All these measures may help against the backdrop of desperate, disorganised crime that&rsquo;s the symptom of a completely broken society. If the organised criminals want you, then there&rsquo;s pretty much bugger all you can do about it. And those gates can equally serve to lock you in with the gangsters, preventing your escape.  </p>  
   <p>  
  	Sometimes during robberies and attempted kidnappings and extortions civilians have succeeded in fighting off the narcos - sometimes killing and wounding them. The narcos have a response ready for this: kill the home defender for sure, but make sure you kill his family first.  </p>  
   <p>  
  	Juarez was once described as the &lsquo;laboratory of the future&rsquo;. Perfectly situated to provide the USA with cheap goods, with a population totally lacking even the most basic rights, they constitute a perfect labour force from the point of view of employers. 17 years after the signing of NAFTA, that future has arrived. Whereas before the yearly murder rate was in the hundreds, now it is in the thousands. Yet, magically, in the last year or so the economy has begun to &lsquo;recover&rsquo; &ndash; but of the entire city only the maquilas (border sweatshops) are profiting; and they are also the only places free from narco violence.  </p>  
   <p>  
  	The same conditions that make the city perfect for big business are the same that make the city perfect for the narcos. With an average wage of $50 per week, it&rsquo;s all too easy for young kids to get sucked up into the world of the Cartels; canon-fodder for perhaps a few hundred dollars a week (a fortune in Juarez). And of course, free trade in goods means unending lines of trucks and goods flowing across the border. Whilst the US Immigration and Customs agents bust individuals with a kilo or two of weed on them, the goods trucks ferry tons of cartel grass, coke, heroin and meth to the US consumer, their journey facilitated by those same free trade arrangements. &nbsp;  </p>  
   <p>  
  	The official story here is that the violence in J&uacute;arez is due to the battle for control of the city being fought between El Chapo Guzman&rsquo;s &lsquo;Sinaloa&rsquo; Cartel and the local J&uacute;arez Cartel - and the government&rsquo;s battle against both. This may be part of the story, but more than a war between cartels and government, the war is a war against the civilian population.  </p>  
   <p>  
  	Where locals take on the power of big business, they suffer the same fate as those who interfere with narco-trafficking. One emblematic case is that of Lomas de Peleo, a neighbourhood on the outskirts of Juarez. It was founded a few decades back when citizens got together to buy the patch of unwanted barren land to build a community. All was well (if by well you mean poor and lacking in basic services) until the Zaragosa Group (a huge set of land business interests owned by the Zaragosa family) decided that it wanted the area to build a new industrial city on the outskirts of Juarez. Rather than offer to buy the land, recompense the locals or offer alternative housing, they instead launched a campaign of intimidation and violence to drive the community out.  </p>  
   <p>  
  	Some forty houses were burned down, a &lsquo;security fence&rsquo; was erected around the community to prevent them from leaving, and bulldozers were sent in to demolish houses. When the residents of Las Lomas de Poleo organised, activists began to receive death threats. One activist was shot and injured. Their lawyer was shot and killed. Now only a handful of residents remain. Ironically, due to the financial crisis, it&rsquo;s looking increasingly unlikely that the new industrial city will ever be built.  </p>  
   <p>  
  	Activists for peace and justice are in the front line of this war. Marisela Escobedo, a mother seeking justice for her 16 year old murdered daughter, was killed in plain sight by outside the town hall where she had been conducting a peace vigil. Although her murder was caught on CCTV (<a href="http://www.youtube.com/watch?v=QNvgrEKedsw" target="_blank">http://www.youtube.com/watch?v=QNvgrEKedsw</a>) it is unlikely that her murderers will ever be caught. That didn&rsquo;t stop the rest of the family protesting even louder, so they burned down the family lumber business during the funeral before kidnapping, torturing and killing Marisela&rsquo;s brother in law.  </p>  
   <p>  
  	Another activist, Josefina Reyes, had been campaigning against human rights abuses by the military and federales, ever since she suspected their involvement in the killing of her son. She herself was kidnapped and killed by &lsquo;unknown gunmen&rsquo; in January 2010. When her family took up the baton and carried on campaigning in her name, two of their houses were burned down. This still did not stop them, so these same gunmen kidnapped and killed her brother, her sister and her brother in law.  </p>  
   <p>  
  	Yet despite the almost unending bloodbath, people still know know how to have a good time. Clubs are still full at night, even though occasionally gangsters like to go on the occasional killing spree. Sometimes killings in popular places aren&rsquo;t reported, for fear of attracting bad publicity. Everyone knows though - there&rsquo;s something about stories of kill-crazy rampages that spreads quickly by world of mouth.  </p>  
   <p>  
  	More importantly, there is an active, activist civil society here - people aren&rsquo;t taking the violence and impunity lying down. The most visible manifestation is the vibrant is the graffiti and urban art scene. There&rsquo;s hardly a wall without graffiti, and a lot of it is high quality and loaded with social commentary.  </p>  
   <p>  
  	There are many street art collectives here - local students and artists working with kids from the barrios (many of the are barrio kids themselves of course). Their philosophy (whether spoken or not) is that the streets belong to the people that live there, and walls are their medium to express themselves. The Barrios&rsquo; street art has grown exponentially, and a lot of the graffiti is independent of the collectives, just local talent armed with paint brushes and spray cans.  </p>  
   <p>  
  	Despite the risks, community activists, radical liberation-theology Christians, human rights activists, and even a few Zapatistas, have been diligently going against the flow for years, documenting and protesting human rights abuses, counselling victims, organising self-help projects for the left out, and plugging away ceaselessly as the crimes have mounted up to incomprehensible levels.  </p>  
   <p>  
  	Civil society&rsquo;s biggest mobilisation for many years was visible on the streets on the 9th and 10th of June, as local activists prepared a welcome for the peace caravan of poet Javier Sicilia, travelling from Cuernavaca through Mexico&rsquo;s hotspots for the last few weeks. As the epicentre of Mexico&rsquo;s 21st century violence it made sense that Juarez would be the caravan&rsquo;s final destination. It was here that Sicilia and his &ldquo;Peace with Justice and Dignity&rdquo; movement signed their &ldquo;Pacto Ciudadano&rdquo; (Citizen&rsquo;s Pact) - demanding that the military return to their barracks and an end to narco and governmental impunity.  </p>  
   <p>  
  	These are necessary goals if Mexicans stand a chance of taking their country back, although there has been some complaints that Sicilia and his movement are a little too willing to sit down and discuss with the government. Just like the UK&rsquo;s Stop the War movement, mainstream peace movements that allow themselves to be distracted away from confrontation with the state have a poor record of success. Luckily local Juarenses are aware of this, and in keeping with the feisty nature of the city that Mexican revolutionary Pancho Villa called home, they are not ready to compromise with anyone.  </p>  
   <p>  
  	The demonstrations themselves pulled in around 3,000 on both days, with the march through the centre of Juarez a vibrant affair - complete with a solidarity march from El Paso, Texas, crossing the broder (for perhaps the first time ever), marionetas, anti-militarist pinatas of soldiers (for kids to hit with sticks) and representations from virtually all of Juarez&rsquo;s peace/activist organisations.  </p>  
   <p>  
  	The hope is that the demonstration serves to gather together the campaign organisations active in the city, and that armed with the confidence that they can rally thousands for peace and justice and against militarisation, Juarez&rsquo;s civilians can begin take the power back from the ruthless, illegitimate power of Mexico&rsquo;s legal and illegal elite. <br />  
  	 <br />  
  	<u><strong>More info:</strong></u>  </p>  
   <p>  
  <a href="	http://narconews.com" target="_blank">	http://narconews.com</a> (quite good coverage of Juarez) <br />  
  	 <br />  
  <a href="	http://english.aljazeera.net/programmes/faultlines/2011/06/201161493451742709.html" target="_blank">	http://english.aljazeera.net/programmes/faultlines/2011/06/201161493451742709.html</a> good recent AJE doco <br />  
  	 <br />  
  <a href="	http://variousenthusiasms.wordpress.com/2009/04/28/the-sicario-a-juarez-hit-man-speaks-by-charles-bowden-harpers/" target="_blank">	http://variousenthusiasms.wordpress.com/2009/04/28/the-sicario-a-juarez-hit-man-speaks-by-charles-bowden-harpers/</a> reprint of an interview by Charles Bowden for Harpers Magazine with a Juarez hitman- utterly chilling <br />  
  	 <br />  
  <a href="	http://juarezurbano.com" target="_blank">	http://juarezurbano.com</a> site documenting Juarez&rsquo;s graffiti. <br />  
  	 <br />  
  <a href="	http://redporlapazyjusticia.org" target="_blank">	http://redporlapazyjusticia.org</a> Javier Sicilia&rsquo;s movement for Peace with Justice and Dignity <br />  
  	&nbsp;  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1249), 144); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1249);

				if (SHOWMESSAGES)	{

						addMessage(1249);
						echo getMessages(1249);
						echo showAddMessage(1249);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


