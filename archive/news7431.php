<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 743 - 15th October 2010 - Hammering Home the Point</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, smash edo, brighton, direct action, arms trade" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 743 Articles: </b>
<b>
<p><a href="../archive/news7431.php">Hammering Home The Point</a></p>
</b>

<p><a href="../archive/news7432.php">Edl: Leicester Is More</a></p>

<p><a href="../archive/news7433.php">No Surrender Monkeys</a></p>

<p><a href="../archive/news7434.php">Hunt Intensifies</a></p>

<p><a href="../archive/news7435.php">Calais: Burning Issues</a></p>

<p><a href="../archive/news7436.php">Crude But Effective</a></p>

<p><a href="../archive/news7437.php">Schnews In Brief 743</a></p>

<p><a href="../archive/news7438.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th October 2010 | Issue 743</b></p>

<p><a href="news743.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>HAMMERING HOME THE POINT</h3>

<p>
<p>
	<strong><span style="font-size:11.0pt;line-height:115%;
font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:Calibri;mso-bidi-font-family:
&quot;Times New Roman&quot;;mso-ansi-language:EN-GB;mso-fareast-language:EN-US;
mso-bidi-language:AR-SA">EYEWITNESS REPORT FROM THIS WEEK&rsquo;S SMASH EDO PROTEST</span></strong> </p>
<p>
	<strong>The &lsquo;ITT&rsquo;s Hammertime&rsquo; Smash EDO demo in Brighton on Wednesday (13th) ended with over fifty arrests, after police made it clear they were going to protect the arms makers at any cost.</strong> </p>
<p>
	The demonstration planned to lay siege to Brighton&rsquo;s premier weapons factory for the day, but police repression and a newly developed policy for preventative arrests put paid to the ambitious plans to push the factory out of town. With both a section 60 and highly restrictive section 14 in place, police were given even greater powers, which they gleefully exercised. </p>
<p>
	The announced meeting point was Wild Park, near the site of the EDO-ITT factory in Brighton, while the &lsquo;Wildkatz Social Centre&rsquo; (a squatted building in nearby Stanmer village), had acted as a convergence space overnight, housing 70 or so activists from around the country. On the morning of the demo the squat was surrounded by around 200 police and the lanes of the tiny village lined with police vans. Eventually, after unmasking, around 50 protesters were able to leave the building in a mobile kettle which escorted them towards Wild Park, unfortunately having to leave a wealth of defensive props behind. Coppers also broke into someone&rsquo;s car at the convergence centre nicking two reinforced banners and... the car stereo. </p>
<p>
	The cops had built a heavy metal barricade at the top of Home Farm Road, where EDO-ITT is located, despite there being enough police to sink a ship at the bottom. On the grass verge by the turning the police had made a &lsquo;designated protest area&rsquo; in which protesters were permitted to be civilly disobedient. This consisted of metal fencing and, thoughtfully, some portaloos. </p>
<p>
	Meanwhile approximately 20 people were left surrounded in the convergence space and told by police that they were not allowed to leave to &ldquo;prevent a breach of the peace&rdquo; or, as it&rsquo;s more commonly known, a protest. About an hour later this small bloc, complete with wheely bin soundsystem, was permitted to walk out of Stanmer surrounded by a blue bloc at least three times their size. Halfway to the factory the guard detail received orders to scarper, jumping into vans and speeding ahead, not even stopping to offer a lift. Seizing on this brief respite from police accompaniment the protesters cut up through the woods to the factory. </p>
<p>
	<strong>GOING WILD</strong> </p>
<p>
	By 10am about 200 protesters had converged on Wild Park, mostly masked up black bloc style. There was a surprisingly professional papier-mache F16, the remaining reinforced banner and plenty of inflatable hammers. Early on in the morning an announcement was made that the factory was closed for the day, rousing a cheer from the barricades. Protesters were massively outnumbered by about 500 police who had come from as far afield as Wales. Their fun day out consisted of making neat lines and formations, like synchronised swimmers in yellow, trying to kettle the protesters from the word &lsquo;go&rsquo;. One protester told SchNEWS, &ldquo;<em>In one word, I&rsquo;d describe the policing of the day as &lsquo;overkill&rsquo;... If the financing of the policing of the demo had come out of the military budget rather than the police, it would have been a huge success</em>.&rdquo; </p>
<p>
	Undaunted the Smash EDO crew didn&rsquo;t make it so easy for them. As police lines advanced into the Wild Park meeting place, the protesters made a run for it into the woods, emerging some time later in the field behind the factory. Activists holding a reinforced banner clashed with the police line heading up the field, with one man arrested, suffering a cut on the head in the process. Approximately 20 were kettled at the top of the hill while the rest scattered back into the woods. </p>
<p>
	Determined not to abide by the police&rsquo;s plan for the day, groups of protesters avoided repeated attempts by the police to contain them. This resulted in an extended cat-and-mouse game around the local area over the next couple of hours that mainly centred around avoiding getting kettled rather than actual attempts to breach the factory fence. At midday around 100 activists were back at the &lsquo;designated protest space&rsquo;, and just as groups started to reorganise and gear up for another co-ordinated attack, police lines blocked the traffic and advanced, driving people onto the road. </p>
<p>
	As the protest was pushed away from the factory and towards town, police formed lines at strategic points along the way, splitting the group into smaller blocs, then kettling any group of 10 or more protesters. Some of those outside of these kettles were then told they had to either leave the area or be arrested, while other police informed them they couldn&rsquo;t leave, resulting in the farcical scene of a 12 foot plane being hurriedly manoeuvred through the back alleys of moulsecoomb. </p>
<p>
	By 3pm, the majority of the protest was kettled, leaving the stragglers to return to base to muse over a day that didn&rsquo;t go entirely to plan. Around 50 arrests were made throughout the day, mostly for Section 14 and &lsquo;breach of the peace&rsquo;, and mostly the unlucky ones caught in the kettles. This is one of the first times this police tactic of mass preventative arrests has been used in the UK, following widespread use in Brussels at the No Borders camp and in Copenhagen. As police brutality hasn&rsquo;t seemed to temper the commitment of Smash EDO protesters so far, could this be a new way of trying to sap the resolve of this determined bunch? </p>
<p>
	<strong>STICK IT TO &rsquo;EM</strong> </p>
<p>
	In town, a group from the main demo held a picket outside Barclays - ITT&rsquo;s middle man on the stock market - and attempted to glue on to the doors before realising that gluing on to sliding doors doesn&rsquo;t actually have the desired effect. Despite this apparent lack of foresight, the main entrance was closed as a result and they started to use the side door. Another glue-on happened at a branch of RBS (one of the main investors in the arms trade), this time on a more suitable type of door, though a group of people were arrested later on in the day, accused of the action. All are now bailed, and those arrested earlier on for somehow breaching the peace whilst in the middle of a kettle have also been released without charge. </p>
<p>
	As well as anarchist actions, Brighton saw some fascist fun and games. The English National Alliance and English Defence League announced that they were going to come to Brighton to &lsquo;ave a go at the Smash EDO lot, and a few did turn up at the train station to wait for homeward bound demonstrators. Despite the sad non-appearance of their great leader, Bill Baker, who had broken down at Pease Pottage, a few rather heated political debates ensued when the fash came up against the peace loving activists. </p>
<p>
	Smash EDO press spokesperson Andrew Beckett said, &ldquo;Despite repeated assertions that they were &lsquo;facilitating peaceful protest&rsquo;, the police employed highly repressive tactics in an attempt to quash the demonstration from the start. The large numbers of arrests made today, mostly from within police cordons, will only be used to attempt to justify this massive and disproportionate police operation to protect the workings of EDO/MBM ITT&rdquo;. </p>
<p>
	* See <a href="http://www.smashedo.org.uk" target="_blank">www.smashedo.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(972), 112); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(972);

				if (SHOWMESSAGES)	{

						addMessage(972);
						echo getMessages(972);
						echo showAddMessage(972);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


