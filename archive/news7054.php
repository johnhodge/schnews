<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 705 - 15th January 2010 - Gaza Arrests Witness Call Out</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, israel, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">
	
			<div class="schnewsLogo">
			<a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a>
			</div>
			<?php
			
				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";			
			
			?>

</div>	











<!--	NAVIGATION BAR 	-->

<div class="navBar">

		<?php
		
			//	Include Nav Bar
			require "../inc/navBar.php";
		
		?>

</div>







<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 705 Articles: </b>
<p><a href="../archive/news7051.php">Wild At Heart</a></p>

<p><a href="../archive/news7052.php">Ships In The Fight</a></p>

<p><a href="../archive/news7053.php">Pyramid Schemers</a></p>

<b>
<p><a href="../archive/news7054.php">Gaza Arrests Witness Call Out</a></p>
</b>

<p><a href="../archive/news7055.php">Funky Gibbons</a></p>

<p><a href="../archive/news7056.php">Frisky Business</a></p>

<p><a href="../archive/news7057.php">Xmas-size Attack</a></p>

<p><a href="../archive/news7058.php">Bough Down</a></p>

<p><a href="../archive/news7059.php">Cat On A Squat Tin Roof</a></p>

<p><a href="../archive/news70510.php">And Finally</a></p>
 
		
		</div>


</div>



	
<div class="copyleftBar">

	<?php
	
		//	Include Copy Left Bar
		require "../inc/copyLeftBar.php";
	
	?>

</div>






<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 15th January 2010 | Issue 705</b></p>

<p><a href="news705.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GAZA ARRESTS WITNESS CALL OUT</h3>

<p>
From 27th December 2008 to 18th January 2009 Israel launched a brutal assault on Gaza, murdering 1417 people. In London a huge outpouring of rage manifested itself with nightly demonstrations at the Israeli embassy. <br />   <br /> On the tenth of January 100,000 people marched from Trafalgar Square to the Israeli embassy. These demos were characterised by a militancy not seen before in the UK&rsquo;s mainstream Palestine solidarity movement. They also saw a growing number of young Muslims taking to the streets side by side with anarcho black-blockers. The Forward Intelligence Team (FIT) were out in force, however, and many people didn&rsquo;t mask up.  <br /> The state were clearly concerned by the prospect of a new mobilisation of angry young muslims, especially if they were to build bridges with other dissenters. In April and May, the gutter press and the crimewatch website published pictures of participants in the demos who the police were after. In total 73 people were arrested, both at the demos and in raids. <br />   <br /> Although people had been arrested for a variety of different offences (eg criminal damage and assault) all the charges have now been bumped up to violent disorder. <br />   <br /> The trial of the 73 Gaza defendants is one of the largest public order trials of recent years and is comparable in scale to the attempt to fit up hundreds of people after police attacked the 1990 poll tax demonstrations. The poll tax defendants were aided greatly by the Poll Tax Defendants Campaign who collected footage and witness statements. <br />   <br /> Initial hearings in the Gaza cases have been held at West London Magistrates Court and the trials are set to be held in March at Isleworth Crown Court. <br />  <br /> Several people have plead guilty and at least three have already been sentenced, to between 15 and 20 months in prison. More people are due to be sentenced on 12th and 19th February at Isleworth Crown Court. Judge Dennis, eager to bang as many people up as possible, has reserved the trials for himself. <br />   <br /> The defendants need support. Please try and make it to some of the hearings. Both witnesses and film footage are needed to help their defence - both to actual arrests and the general attitude and actions of the police. Witnesses who were present for the following times and places are especially needed: 28th December, 29th December, 3rd January, 7th January, 8th January, 10th January, 17th January, 24th January; Kensington High St, outside the Israeli Embassy (majority of charges relate to this location esp. between 16.45 and 17.45 on 10th January); Starbucks, Shaftsbury Avenue (on the 10th and 24th of January), Notting Hill Gate (10th Januray - 15.45); Hyde Park Underpass (including just before and just after Underpass, on the 3rd January - 17.15-18.15 and outside the Israeli Embassy from 18.00).&nbsp; <br />  <br /> Any footage showing any assault on protesters by the police at any stage during the demos is also vital. <br />   <br /> Please contact <a href="mailto:ellie.schling@googlemail.com">ellie.schling@googlemail.com</a> if you can help, think you were at one of the places above or have footage of the demonstrations.  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=705">gaza</a>, <a href="../keywordSearch/?keyword=israel&source=705">israel</a>, <a href="../keywordSearch/?keyword=palestine&source=705">palestine</a>, <a href="../keywordSearch/?keyword=protest&source=705">protest</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(619);
			
				if (SHOWMESSAGES)	{
				
						addMessage(619);
						echo getMessages(619);
						echo showAddMessage(619);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
		
</div>






<div class="footer">

		<?php
		
			//	Include Page Footer
			require "../inc/footer.php";
		
		?>
		
</div>








</div>




</body>
</html>


