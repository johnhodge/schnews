<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 732 - 23rd July 2010 - Getting Away With Murder</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, g20 summit, london, ian thomlinson, met police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../archive/news729.php" target="_blank"><img
						src="../images_main/decommissioners-victory.jpg"
						alt="EDO Decommissioners walk free after unanimous acquittals in trial"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 732 Articles: </b>
<b>
<p><a href="../archive/news7321.php">Getting Away With Murder</a></p>
</b>

<p><a href="../archive/news7322.php">The Democratic Process</a></p>

<p><a href="../archive/news7323.php">Banking On The Camp</a></p>

<p><a href="../archive/news7324.php">Aisle Be Back</a></p>

<p><a href="../archive/news7325.php">Hurndall Killer Released</a></p>

<p><a href="../archive/news7326.php">Lewes Road Digs In</a></p>

<p><a href="../archive/news7327.php">Action Aid - By George</a></p>

<p><a href="../archive/news7328.php">Sabs: Rover And Out</a></p>

<p><a href="../archive/news7329.php">Inside Schnews</a></p>

<p><a href="../archive/news73210.php">Calais: Out To Lunch</a></p>

<p><a href="../archive/news73211.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 23rd July 2010 | Issue 732</b></p>

<p><a href="news732.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>GETTING AWAY WITH MURDER</h3>

<p>
<p>
	<strong>&ldquo;IT&rsquo;S A JOKE&rdquo; - NO CHARGES OVER TOMLINSON DEATH...</strong> </p>
<p>
	<em><strong>&ldquo;After 16 months of waiting, to hear nothing is being done is a complete joke. Today they gave us no hope. This experience has broken our family apart. The DPP has told us there was an unlawful act, yet no charges are to be brought. This is no justice - everyone has failed us.&rdquo;</strong></em> - Ian Tomlinson&rsquo;s son Paul King </p>
<p>
	Ian Tomlinson, a 47-year-old news-vendor, was killed at the G20 protests in April 2009 (<a href='../archive/news672.htm'>SchNEWS 672</a>). His family have waited until now to find out what action the justice system was going to take. The Director of Public Prosecution Keir Starmer stated yesterday (22nd) that there would be. er. no charges brought against any police officers involved in the death of Ian Tomlinson. This astonishingly barefaced whitewash prompted the director of INQUEST Deborah Coles to say, &ldquo;<em>The eyes of the world will be looking on with incredulity as yet again a police officer is not facing any criminal charges after what is one of the most clear-cut and graphic examples of police violence that has led to death. This decision is a shameful indictment of the way police criminality is investigated and demonstrates a culture of impunity when police officers break the law. It follows a pattern of cases that reveal an unwillingness to treat deaths arising from the use of force by police as potential homicides</em>.&rdquo; </p>
<p>
	<strong>FREDDY, UNSTEADY, GO</strong> </p>
<p>
	The inquiry into Tomlinson&rsquo;s death was deliberately botched - Dr Freddy Patel, the pathologist who first examined him, claimed that he had died of a heart attack. He has now been suspended following irregularities in four other post-mortem examinations. Two other pathologists who examined the body found that he had died as a result of &lsquo;blunt force trauma&rsquo;. </p>
<p>
	Incredibly the discrepancy between the first report and the second two was cited by Keir Starmer as one reason for not proceeding with criminal charges. The idea that the Met simply got a bent pathologist in as part of a cover up doesn&rsquo;t seem to have occurred to him. </p>
<p>
	Of course there may never have been a second pathologists report if further evidence of Ian&rsquo;s treatment at the hands of police hadn&rsquo;t emerged. Footage of him shuffling away from a line of police with his hands in pockets was given to the Guardian newspaper (<a href="http://www.youtube.com/watch?v=HECMVdl-9SQ&amp;feature=player_embedded#at=30" target="_blank">www.youtube.com/watch?v=HECMVdl-9SQ&amp;feature=player_embedded#at=30</a>). It clearly shows a riot cop stepping up behind him, hitting him round the legs then pushing him over. More pointedly, the film shows the absolute indifference of the other officers. They don&rsquo;t even glance around: this is business as usual. As SchNEWS has witnessed, this kind of casual dishing out of violence is completely normal for police in this situation - but not always seen by millions on film. </p>
<p>
	The G20 protests were deliberately corralled in a &lsquo;kettle &lsquo; around the Bank of England. As the day wore on the police became increasingly violent. What happened to Ian was, apart from its tragic consequences, an experience shared by many. </p>
<p>
	Immediately after the news of Tomlinson&rsquo;s death surfaced the Met&rsquo;s PR machine whirred into motion. We were told by a dutiful mainstream press that a man who had had no violent contact with cops had died of a heart attack while officers attempted to resuscitate him. Lies were spread that these life-saving public servants were under a hail of bottles hurled by a filthy anarchist mob. Despite the fact that the media should have learned their lesson during the Menezes affair (see <a href='../archive/news508.htm'>SchNEWS 508</a>, <a href='../archive/news552.htm'>552</a>, <a href='../archive/news647.htm'>647</a>), once again they reported the lies verbatim. </p>
<p>
	<strong>THE SERIOUS GOONS</strong> </p>
<p>
	The cop who struck Tomlinson and pushed him over has now been named as PC Simon Harwood, part of the Met&rsquo;s Territorial Support Group (TSG). This is a specialised forced entry and public order unit of the Metropolitan Police &ndash; created the day after the notorious Special Patrol Group was disbanded in the wake of the death of Blair Peach, another man clubbed to death for being on the wrong demo at the wrong time, this time in 1979. The inquiry into his death has never been made public. </p>
<p>
	The TSG has gained its own share of infamy as the most macho and aggressive section of the most macho and aggressive police force in the UK. This is the same outfit that Sgt Delroy Smellie belongs to, caught on camera striking out at Nicola Fisher who was brandishing a carton of orange juice at the time. (<a href="http://news.bbc.co.uk/1/hi/uk/8597812.stm" target="_blank">http://news.bbc.co.uk/1/hi/uk/8597812.stm</a> ). It is clear that the casual use of violence is endemic in the unit and others like it. A total of 283 TSG officers had been investigated over 547 allegations of misconduct during the year 2008-09. Of these, 159 allegations were of assault. It is of course extremely useful to those in power to have at their disposal these shock troops of repression, and for them to be effective they must be guaranteed immunity from prosecution. </p>
<p>
	Legal manoeuvres have been used to shut down the family and the public&rsquo;s access to information - the policy has transparently been to kick the investigation into the long grass. The delay in the decision to prosecute for manslaughter has led to a situation where the time limit for a lesser charge has expired. The &lsquo;Independent&rsquo; Police Complaints Commission have repeatedly refused to release footage on the grounds that it was being used in an ongoing prosecution. The same excuse has until now been used to rule out any public inquiry into his death. The IPCC also refused to take over the investigation until seven days after Ian&rsquo;s death, leaving the forensic evidence to remain entirely in the Met&rsquo;s hands. </p>
<p>
	Ian Tomlinson was murdered by the police &ndash; that is a fact. It doesn&rsquo;t matter whether PC Simon Harwood actually wanted him to die. From the top down they embraced tactics that were indifferent as to whether he lived or not. When Ian&rsquo;s death was firmly laid at the Met&rsquo;s door, the only consideration was how the force could get away with it. But the surfacing of the footage, clearly evidencing an unprovoked assault, has at least has forced the inevitable cover-up to be be utterly implausible. </p>
<p>
	Ian&rsquo;s death is probably the most high profile death at the hands of the police in recent years. However, deaths in custody and after other contacts with the police happen regularly; 30 last year and 65 in 2008. No officer has ever been prosecuted for the death of a civilian caused in the line of duty. </p>
<p>
	<em><strong>&ldquo;We feel like it wasn&rsquo;t a full investigation from the beginning. It&rsquo;s been a big cover-up and they&rsquo;re incompetent. Why isn&rsquo;t there an assault charge? We feel very let down, very disappointed. We expected a charge. It clearly shows our dad being assaulted by a police officer&rdquo; </strong></em> <br /> 
	- Paul King, Ian Tomlinson&rsquo;s son. </p>
<p>
	*See <a href="http://www.iantomlinsonfamilycampaign.org.uk" target="_blank">www.iantomlinsonfamilycampaign.org.uk</a> and <a href="http://www.inquest.org.uk" target="_blank">www.inquest.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(870), 101); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(870);

				if (SHOWMESSAGES)	{

						addMessage(870);
						echo getMessages(870);
						echo showAddMessage(870);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


