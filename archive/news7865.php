<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 786 - 2nd September 2011 - Seeds of More Ruction</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, israel, palestine, occupation, protest" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 786 Articles: </b>
<p><a href="../archive/news7861.php">Edl: It Takes One To E1</a></p>

<p><a href="../archive/news7862.php">Kick Ass Moves</a></p>

<p><a href="../archive/news7863.php">Dale Of Reckoning</a></p>

<p><a href="../archive/news7864.php">Leaky Arguments</a></p>

<b>
<p><a href="../archive/news7865.php">Seeds Of More Ruction</a></p>
</b>

<p><a href="../archive/news7866.php">Hideous Khimki</a></p>

<p><a href="../archive/news7867.php">Tuckers Luck</a></p>

<p><a href="../archive/news7868.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd September 2011 | Issue 786</b></p>

<p><a href="news786.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SEEDS OF MORE RUCTION</h3>

<p>
<p>  
  	The Israeli military has begun supplying settlers with tear gas and stun grenades in anticipation of Palestinian protests. The arming of settlers is part of &lsquo;Operation Summer Seeds&rsquo; &ndash; the Israeli Defence Force&rsquo;s (IDF) response to Palestine&rsquo;s application for UN recognition (see <a href='../archive/news784.htm'>SchNEWS 784</a>), which is expected to go through in September.  </p>  
   <p>  
  	Although the US will veto full membership of the UN, Palestine is expected to gain the title of non-member state. This is unlikely to have much effect on the day-to-day life of a Palestinian, but Israeli authorities are expecting it to act as a trigger for further demonstrations like those seen in May (see <a href='../archive/news772.htm'>SchNEWS 772</a>).  </p>  
   <p>  
  	Worried by the prospect of mass, non-violent protests the IDF obviously feels vulnerable (despite having one of the most advanced armies on the planet) and has been stepping-up the training of &lsquo;civil defence teams&rsquo; &ndash; effectively settler paramilitaries. Although they&rsquo;ve been told to &ldquo;avoid killing civilians&rdquo; the Israeli security forces have drawn up red lines around settlements, once a Palestinian crosses this line, it&rsquo;s fair game to shoot at their legs.  </p>  
   <p>  
  	Human rights group B&rsquo;Tselem has reported 42 cases of settler violence so far this year, including the murder of two Palestinian teenagers. Meanwhile Avigdor Lieberman, Israel&rsquo;s foreign minister, has been firing up the already trigger happy settlers saying &ldquo;The PA (Palestinian Authority) is planning bloodshed the likes of which we&rsquo;ve never seen before&rdquo;.  </p>  
   <p>  
  	The Israeli government has ordered petrol stations and supermarkets to stockpile supplies in preparation for &ldquo;mass disorder&rdquo; from 19th September. This siege mentality and arming of fervent Israelis is unlikely to calm an already volatile situation.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1343), 155); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1343);

				if (SHOWMESSAGES)	{

						addMessage(1343);
						echo getMessages(1343);
						echo showAddMessage(1343);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


