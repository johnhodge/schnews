<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 764 - 25th March 2011 - No Flies On Us</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, middle east, libya, bahrain, yemen, arab spring, saudi arabia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://london.indymedia.org/events/7176" target="_blank"><img
						src="../images_main/763-banner-march-26th.png"
						alt="Mass Demonstration Against The Cuts - London March 26th 2011"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 764 Articles: </b>
<b>
<p><a href="../archive/news7641.php">No Flies On Us</a></p>
</b>

<p><a href="../archive/news7642.php">Edl: Reading Lessons</a></p>

<p><a href="../archive/news7643.php">Tuc Right Off</a></p>

<p><a href="../archive/news7644.php">Lucky Hetherington</a></p>

<p><a href="../archive/news7645.php">Squat Thrusts</a></p>

<p><a href="../archive/news7646.php">0742 Club Evicted</a></p>

<p><a href="../archive/news7647.php">Glengad Abouts</a></p>

<p><a href="../archive/news7648.php">Wood Cuts</a></p>

<p><a href="../archive/news7649.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 25th March 2011 | Issue 764</b></p>

<p><a href="news764.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>NO FLIES ON US</h3>

<p>
<p>  
  	<strong>AS SCHNEWS LOOKS INTO THE WEST&rsquo;S SELECTIVE SUPPORT OF THE ARAB REBELLIONS</strong>  </p>  
   <p>  
  	It&rsquo;s March, so it&rsquo;s time for a new war. To no-one&rsquo;s great surprise we&rsquo;re bombing the Arabs again. A few facts for history buffs - not only did &ldquo;Operation Defend Libyans From Bombs By Bombing Libyans&rdquo; start almost exactly seven years after the start of the Iraq War, but 2011 also marks one hundred years of aerial bombardment. The target of those first ever bombs dropped from planes in 1911? Libya.  </p>  
   <p>  
  	The surprise was that this war has been OK&rsquo;d by some sort of broad international consensus. There was even a vote by UN Security Council. The justification was the horrendous series of massacres conducted under Gaddafi&rsquo;s orders - artillery, tanks and aircraft have been used indiscriminately against demonstrators and civilians in rebel areas. Demonising Colonel Gaddafi was never going to be much of a hard sell. Gaddafi was the man the West loved to hate during the &lsquo;80s - serial financier of armed anti-imperialist groups around the world, and fingered for the Lockerbie bombing.  </p>  
   <p>  
  	All this changed back in 2003 though, when Gaddafi, despite his anti-imperialist bluster, blinked first during the USA&rsquo;s &lsquo;War on Terror&rsquo; and gave up his terrorist links and WMD programmes. In their place he got to hug Tony Blair (OK, so he didn&rsquo;t sever all his terror links), got BP oil investment and access to Europe&rsquo;s petroleum markets.  </p>  
   <p>  
  	Since then Gaddafi had been an ally of sorts of the west - useful for cheap oil, the torture of terror suspects (eg Omar Deghayes see <a href='../archive/news664.htm'>SchNEWS 664</a>) and, in Silvio Berlusconi&rsquo;s case, showing how to throw Bunga Bunga parties with underage prostitutes. But, as the old saying goes, there are no permanent allies, only permanent interests. And it seems that Gaddafi is no longer useful to the West.  </p>  
   <p>  
  	<strong>CRUISE CONTROL</strong>  </p>  
   <p>  
  	Libya&rsquo;s revolt/revolution is just one part of the great wave that is sweeping throughout the Middle East and North Africa. Now that the cruise missiles are flying you could be mistaken for thinking that massacres of protesters, and other grave breaches of human rights and international humanitarian law are only happening in Libya. But, ignored by the tame press, in Bahrain and Yemen things are as bad if not worse.  </p>  
   <p>  
  	In Bahrain the ongoing mass peace protests have stretched the monarchy&rsquo;s security forces to their limit. At their peak more than one quarter of the country was on the march, some 400,000 out of a population of 1.2 million. Fearful of what was going on on their doorstep the Saudi&rsquo;s decided to invade and occupy Bahrain, sending in 1,500 Saudi and UAE troops into the tiny island under the diplomatic cover of the Gulf Cooperation Council- the Saudis&rsquo; figleaf organisation for meddling in the Arabian peninsula.  </p>  
   <p>  
  	Bizarrely, one of the first actions of the Saudi forces was to destroy the statue in the centre of the roundabout that had become the focal point of the demonstrations, as if the statue was the cause of the unrest. As us Brits know all too well, monarchies tend to foster stupid inbred leaders. Ironically, the statue commemorated the Gulf Cooperation Council - the same GCC that invaded and destroyed it.  </p>  
   <p>  
  	But Bahrain and Saudi are US allies - Bahrain is home to the US 5th fleet, so don&rsquo;t expect any strong words, let alone calls to action, from the UN as Saudi and Bahraini troops arrest opposition leaders and shoot protesters at point blank range. Saudi Arabia&rsquo;s oilfields are of course the glittering strategic prize of the entire region. Saudi is also a major buyer of UK weapons systems. Armoured vehicles sent to help crush pro-democracy protests in Bahrain were made in the UK. Bahrain is also a big market for UK arms. In the first nine months of 2010, the UK approved export licenses for over &pound;5 million worth of arms including tear gas and crowd control ammunition, equipment for the use of aircraft cannons, assault rifles, shotguns, sniper rifles and sub-machine guns.  </p>  
   <p>  
  	If the oppression has been heaviest in Bahrain, it&rsquo;s uncertain whether Libya or Yemen has been the bloodiest in recent days. Yemen out-Gaddafi&rsquo;d Gaddafi when security forces attacked a demo on the 18th - killing between 40 and 50. They were under orders to shoot to kill - many protesters died of gunshots to the head and neck. The Yemeni government has likewise been shielded from international criticism thanks to President Ali Abdulla Saleh&rsquo;s commitment to the &lsquo;War on Terror&rsquo;, giving US and Saudi forces free rein to bomb Yemeni territory.  </p>  
   <p>  
  	These desperate, brutal measures look like they may have backfired though; since the massacres senior military and tribal leaders have defected to the rebels. In defiance of the state of emergency Yemenis are planning to march on the presidential palace this Friday, demanding that Saleh leaves power. Direct Saudi occupation a la Bahrain is unlikely though, its heavily armed population and recent history of rebellion and civil war means that a foreign occupation would be a suicide mission for any invader.  </p>  
   <p>  
  	<strong>REVOLUTION AND ON</strong>  </p>  
   <p>  
  	The wave of revolt through North Africa and the Middle East has shown no respect for the traditional politics of the region. Mass protests are taking place in countries with pro-western governments (Bahrain, Egypt, Yemen) and anti-western governments (Syria, Iran, Libya) alike. Regardless of their governments&rsquo; alliances, living conditions in these countries are very similar. Powerful security services backed up by networks of civilian informants have kept either monarchies or &lsquo;hereditary republics&rsquo; in power while the country&rsquo;s wealth is concentrated in the hands of a few.  </p>  
   <p>  
  	Democracy, redistribution of wealth, and an end to corruption and state violence have become suddenly become realistic aspirations thanks to the regional &lsquo;Arab intifada&rsquo;. Britain, France and the USA now act as if they can pick and chose amongst these revolutions - supporting state repression against popular movements in Bahrain and Yemen while condemning oppression in Libya and Syria. Meanwhile the people of North Africa and the Middle East have sensibly decided that they will carry on fighting for their rights regardless of whether the our governments&rsquo; weapons are launched against despotic regimes or sold directly to them.  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1153), 133); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1153);

				if (SHOWMESSAGES)	{

						addMessage(1153);
						echo getMessages(1153);
						echo showAddMessage(1153);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


