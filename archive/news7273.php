<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 727 - 18th June 2010 - Set The Controls For The Heart Of The Sun</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, festivals, autonomous spaces, stonehenge, festivals, battle of the beanfield" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk" target="_blank"><img
						src="../images_main/decommissioner-trial-banner.png"
						alt="Decommissioners Trial begins June 7th 2010 at Hove Crown Court"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 727 Articles: </b>
<p><a href="../archive/news7271.php">Raving Madness</a></p>

<p><a href="../archive/news7272.php">Wood You Believe It</a></p>

<b>
<p><a href="../archive/news7273.php">Set The Controls For The Heart Of The Sun</a></p>
</b>

<p><a href="../archive/news7274.php">Achy Breaky Hearts</a></p>

<p><a href="../archive/news7275.php">Turkey Shoot</a></p>

<p><a href="../archive/news7276.php">The Itt Crowd</a></p>

<p><a href="../archive/news7277.php">Union Jack The Ripper</a></p>

<p><a href="../archive/news7278.php">Hoto Finish</a></p>

<p><a href="../archive/news7279.php">No Crs-pite</a></p>

<p><a href="../archive/news72710.php">Under Who's Advice</a></p>

<p><a href="../archive/news72711.php">Detained Somali Freed</a></p>

<p><a href="../archive/news72712.php">Al-burn Out</a></p>

<p><a href="../archive/news72713.php">Night Mayor</a></p>

<p><a href="../archive/news72714.php">...and Finally Some High Art...</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 18th June 2010 | Issue 727</b></p>

<p><a href="news727.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SET THE CONTROLS FOR THE HEART OF THE SUN</h3>

<p>
<p>
	Just before you strap yer back pack on and head out the door for a festival you might want to check the web before you set off for yer weekend bash, because unless the festival is sponsored by a phone company, protected by bulldog snarling security or hosting the likes of U2, it might just have been pulled by the police. </p>
<p>
	Three home grown stalwart festivals have been scrutinised, pulled up and shut down for utterly spurious reasons in the past year. The Big Green Gathering in 2009 was cancelled literally at the last minute by Mendip District Council and Avon and Somerset Police - citing the potential for &lsquo;crime and disorder&rsquo; and safety concerns (see <a href='../archive/news685.htm'>SchNEWS 685</a>). </p>
<p>
	The same fate awaited Strawberry Fair in Cambridge this year (see <a href='../archive/news715.htm'>SchNEWS 715</a>). The Glade festival was to fall as well, again due to police intervention (see <a href='../archive/news722.htm'>SchNEWS 722</a>). </p>
<p>
	The organiser of the Thimbleberry festival in County Durham last September was arrested for allowing his premises to be used for cannabis smoking, and all the gate money - &pound;3500 &ndash; confiscated as &lsquo;proceeds of crime&rsquo; (see <a href='../archive/news715.htm'>SchNEWS 715</a>). The charge was thrown out of court and people are still hopeful the festival will be back on this September. </p>
<p>
	Free parties, raves and other illegal gatherings have more or less continued on over the past decade, but after the UK Teknival was busted in May (see main story), there has been other instances of an upsurge in police attacks on these events. </p>
<p>
	Festivals like Big Green Gathering - apart from those already mentioned which were actually stopped or made impossible by police - were already under severe financial pressure due to obligatory licensing and policing fees &ndash; bills which ran well into six figures for festivals like Big Green. Licenses for under-500-person events are still relatively obtainable, but any more than that and costs build up. One festival organiser told SchNEWS that these logistics point to having more, but smaller, festivals &ndash; and this is how it seems to be going (see the list below). Events on the scale of Big Green &ndash; which are not drenched in corporate sponsorship - are now nearly impossible. Having said that, rumour has it that Big Green may return next year &ndash; but we doubt it will be held anywhere near the county of Somerset. </p>
<p>
	But, perhaps we shouldn&rsquo;t be wide mouthed with shock... Let&rsquo;s Tardis travel 25 years back to another summer and materialise in a beanfield near Stonehenge. With travellers in a convoy of live-in vehicles on their way to the 11th Stonehenge Free Festival over the solstice, the massive Tory led Police clampdown surrounded, impounded and violently attacked the convoy before they got there. The infamous &lsquo;Battle of the Beanfield&rsquo; has since become a landmark in counter culture history (see <a href='../archive/news25.htm'>SchNEWS 25</a>, <a href='../archive/news678.htm'>678</a>). </p>
<p>
	Since 1999 summer solstice events are now allowed again at Stonehenge, but hardly in the heady vein of the Stonehenge Free Festivals. Heavy policing, stop-and-searches, flood lights, burger vans, and highly coordinated, limited access to the stones is the extent of it. Summer solstice 2009 saw a massive police presence including tooled-up private security, and an unmanned drone flying overhead filming it &ndash; with 37 arrests and 300 searches with sniffer dogs (see <a href='../archive/news681.htm'>SchNEWS 681</a>). </p>
<p>
	Despite all that, and Glastonbury having long since become a theme park for people on drugs in wacky hats, the real spirit of DIY lives on. Here&rsquo;s a bit of a SchNEWS summer guide for all those who want to keep festivals real and fun. They are still out there and they&rsquo;re usually fronted by small crews who deserve support: </p>
<p>
	<strong>* Sunrise Celebration</strong> - June 3rd-6th, Bruton, Somerset, <a href="http://www.sunrisecelebration.com" target="_blank">www.sunrisecelebration.com</a> </p>
<p>
	<strong>* Glastonwick 2010</strong> - June 4th-6th, Coombes farm, Coombes, West Sussex, <a href="http://www.cask-ale.co.uk/beerfestival.html" target="_blank">www.cask-ale.co.uk/beerfestival.html</a> </p>
<p>
	<strong>* Workhouse</strong> - July 9th-11th, Llanfyllin Wales, workhousefestival.co.uk </p>
<p>
	<strong>* Nozstock</strong> - July 9th-11th, Rowden Paddocks, Bromyard, Herefordshire, <a href="http://www.nozstockfestival.co.uk" target="_blank">www.nozstockfestival.co.uk</a> </p>
<p>
	<strong>* Larmar Tree</strong> - July 14th-18th, nr Salisbury, Wiltshire/Dorset border, <a href="http://www.larmertreefestival.co.uk" target="_blank">www.larmertreefestival.co.uk</a> </p>
<p>
	<strong>* Secret Garden Party</strong> - July 22nd-25th, near Huntington, Cambridgshire, <a href="http://uk.secretgardenparty.com/2010" target="_blank">http://uk.secretgardenparty.com/2010</a> </p>
<p>
	<strong>* Truck Festival</strong> - July 23rd-25th, Hill Farm, in Steventon, near Abingdon, South Oxfordshire, <a href="http://www.thisistruck.com" target="_blank">www.thisistruck.com</a> </p>
<p>
	<strong>* Y-Not Festival</strong> &ndash; July 30th - August 1st, Matlock, Derbyshire, <a href="http://www.ynotfestivals.co.uk" target="_blank">www.ynotfestivals.co.uk</a> </p>
<p>
	<strong>* Endorce IT</strong> - August 6th-8th, between Salisbury and Blandford, near the village of Sixpenny Handley, <a href="http://www.endorseit.co.uk" target="_blank">www.endorseit.co.uk</a> </p>
<p>
	<strong>* Belladrum Tartan Heart Festival</strong> - August 6th-7th, Phoineas, By Beauly, Inverness-shire, <a href="http://www.tartanheartfestival.co.uk" target="_blank">www.tartanheartfestival.co.uk</a> </p>
<p>
	<strong>* Boomtown Fair</strong> - August 13th-15th, Buckinghamshire, <a href="http://www.boomtownfair.co.uk" target="_blank">www.boomtownfair.co.uk</a> </p>
<p>
	<strong>* Green Man</strong> - August 20th-22nd, Brecon Beacons, Wales, <a href="http://www.greenman.net" target="_blank">www.greenman.net</a> </p>
<p>
	<strong>* Beautiful Days</strong> - August 21st-23rd, Escort Park, Devon, <a href="http://www.beautifuldays.org" target="_blank">www.beautifuldays.org</a> </p>
<p>
	<strong>* Small World</strong> - August 26th-30th, Headcorn, Kent, <a href="http://www.smallworldsolarstage.org" target="_blank">www.smallworldsolarstage.org</a> </p>
<p>
	<strong>* Solfest</strong> - August 27th-29th, Tarnside Farm, West Cumbria, <a href="http://www.solwayfestival.co.uk" target="_blank">www.solwayfestival.co.uk</a> </p>
<p>
	<strong>* Shambala</strong> - August 27th-30th, Hop Farm Northamptonshire, <a href="http://www.shambalafestival.org" target="_blank">www.shambalafestival.org</a> </p>
<p>
	<strong>* Thimbleberry</strong> - September - TBC - <a href="http://www.thimbleberry.co.uk" target="_blank">www.thimbleberry.co.uk</a> </p>
<p>
	<strong>* Out Of The Ordinary</strong> - September 17th-19th, Knockhatch Farm, Hailsham Bypass, Hailsham, <a href="http://www.outoftheordinaryfestival.com" target="_blank">www.outoftheordinaryfestival.com</a> </p>
<p>
	<strong>* As that&rsquo;s just a few of the hundreds of festivals and gatherings going on this summer</strong>. See <a href="http://www.efestivals.co.uk," target="_blank">www.efestivals.co.uk,</a> <a href="http://www.realfestivalmusic.co.uk," target="_blank">www.realfestivalmusic.co.uk,</a> <a href="http://www.festivaleye.com" target="_blank">www.festivaleye.com</a> and look on <a href="../pap" target="_blank">www.schnews.org.uk/pap</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(819), 96); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(819);

				if (SHOWMESSAGES)	{

						addMessage(819);
						echo getMessages(819);
						echo showAddMessage(819);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


