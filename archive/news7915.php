<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 791 - 7th October 2011 - Core Values</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 791 Articles: </b>
<p><a href="../archive/news7911.php">Nhs Direct Action</a></p>

<p><a href="../archive/news7912.php">Welling Up</a></p>

<p><a href="../archive/news7913.php">Angel Delight</a></p>

<p><a href="../archive/news7914.php">Squat A Waste Of Time</a></p>

<b>
<p><a href="../archive/news7915.php">Core Values</a></p>
</b>

<p><a href="../archive/news7916.php">Factory Finish?</a></p>

<p><a href="../archive/news7917.php">First We Take Manhattan</a></p>

<p><a href="../archive/news7918.php">'cos I'm Worthing It</a></p>

<p><a href="../archive/news7919.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 7th October 2011 | Issue 791</b></p>

<p><a href="news791.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CORE VALUES</h3>

<p>
<p>  
  	A quiet country road in Somerset was the setting for a 250 strong blockade this week as activists gathered to oppose the construction of Hinkley C - the first new nuclear power station to be built in the UK for over twenty years, part of government plans for up to ten in total. The project is backed by French energy corporation EDF.  </p>  
   <p>  
  	Protesters from as far afield as France and Belgium made the journey on Monday (3rd) to join local resistance to EDF&rsquo;s planned facility. A minimal police presence with a hands off approach and no sign of works traffic made for a relaxed atmosphere. Seize the Day and other musicians kept people entertained on the sound system and speeches were heard from local residents and activist groups.  </p>  
   <p>  
  	At noon, the demonstrators held a minute&rsquo;s silence for the victims of the Fukishima fallout, and then released 206 helium balloons, one for each day since the disaster.  </p>  
   <p>  
  	Earlier in the week a camp four miles away from the blockade site had been set up with workshops and speakers, and on Saturday (1st) the camp&nbsp; marched from EDF&rsquo;s offices in Bridgwater around the town.  </p>  
   <p>  
  	The reactor planned for HInkley C is based on an untested design. It is the third of its type under construction, and the other two in Finland and France are way over budget and behind schedule so are not yet operational. The future of both of these facilities is uncertain as building contractors Arriva and EDF wrangle in court over the extra costs in Finland, and France&rsquo;s nuclear build programme hangs in the balance until after their upcoming general election. <br />  
  	For this reason, and due to the relative lack of a strong anti-nuclear movement in the UK, EDF is willing to pour money into HInkley, and even risk losing money, as long as the end result is a functioning reactor which will sell the design to other countries.  </p>  
   <p>  
  	As one protester commented on the day, &ldquo;Today is symbolic. This is the beginning of a new movement: we need to be stronger and more committed if we are going to fight this. Last year there were seven people on a&#8239; blockade here, today there are 250. We need to build up a momentum of thousands, and then we have a real chance of convincing the government there is no future in nuclear.&rdquo;  </p>  
   <p>  
  	* <a href="http://www.stophinkley.org" target="_blank">www.stophinkley.org</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1382), 160); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1382);

				if (SHOWMESSAGES)	{

						addMessage(1382);
						echo getMessages(1382);
						echo showAddMessage(1382);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


