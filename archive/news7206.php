<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 720 - 30th April 2010 - Obtuse Angles</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, anti-fascists, edl, march for england" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://meltdown.uk.net/election/The_Plan_Mayday.html" target="_blank"><img
						src="../images_main/mayday-meltdown-2010-banner.png"
						alt="Mayday Meltdown - Election protest, May 1st, Parliament Square, London"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 720 Articles: </b>
<p><a href="../archive/news7201.php">Crash Of The Titans</a></p>

<p><a href="../archive/news7202.php">Mayday! Mayday!</a></p>

<p><a href="../archive/news7203.php">Meltdown Britain: Debts All Folks...</a></p>

<p><a href="../archive/news7204.php">Off The Rails</a></p>

<p><a href="../archive/news7205.php">Deportation Update</a></p>

<b>
<p><a href="../archive/news7206.php">Obtuse Angles</a></p>
</b>

<p><a href="../archive/news7207.php">Chinese Burn</a></p>

<p><a href="../archive/news7208.php">Radioactive Wasters</a></p>

<p><a href="../archive/news7209.php">Schnews In Brief</a></p>

<p><a href="../archive/news72010.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th April 2010 | Issue 720</b></p>

<p><a href="news720.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>OBTUSE ANGLES</h3>

<p>
March for England brought a variety of anti-social factions together in Brighton city centre last Sunday (25th). Over 100 officers from 3 counties including mounted police got called in to deal with the 150 nationalistic flag wavers and the 150 counter protesters that assembled. <br />  <br /> March for England group had sent ripples across the internet as they announced plans for a get together on St. George&rsquo;s day to celebrate their national identity. The UAF (United Against Fascism) caught wind of this and screamed that people must &ldquo;<strong>STOP THE RACIST EDL IN BRIGHTON</strong>&rdquo;. Controversy arose as to how factual this claim was, with some wondering whether the English Defence League, which have certain connections with MfE, would have shown face at all if the UAF hadn&rsquo;t announced their arrival in the first place. Hmm. <br />  <br /> The cluster of flags and football shirts was encircled by the police to keep rivals apart as they marched through town from the station. Outside the King&amp;Queen pub the UAF held a noisy counter-demo, contained by a crowd pen. The parade&rsquo;s finishing line was clearly separated for each to avoid clashes, as the two sides chanted, sang sweet melodies and yelled insults at each other. Local anarchists and anti fascists were also in attendance, keeping an eye on unfolding events and scouting the area after the crowds dispersed on the look out for the fascist element of the march. It seems the fanatical patriots for the most drank, grumbled amongst themselves and dispersed. Overall there was no blatant fascist presence achieved throughout the day. <br />  <br /> Nine arrests were made in an attempt to uphold the law, one in a minority report style where a Brighton resident got carted off to the cells as he merely approached the starting point of the march. <br />  <br /> Meanwhile the actual EDL are mobilising for a rally in Aylesbury this Saturday May 1st. <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=anti-fascists&source=720">anti-fascists</a>, <a href="../keywordSearch/?keyword=edl&source=720">edl</a>, <span style='color:#777777; font-size:10px'>march for england</span></div>


<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(759);

				if (SHOWMESSAGES)	{

						addMessage(759);
						echo getMessages(759);
						echo showAddMessage(759);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


