<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 786 - 2nd September 2011 - EDL: It Takes One to E1</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, anti-fascists, edl" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 786 Articles: </b>
<b>
<p><a href="../archive/news7861.php">Edl: It Takes One To E1</a></p>
</b>

<p><a href="../archive/news7862.php">Kick Ass Moves</a></p>

<p><a href="../archive/news7863.php">Dale Of Reckoning</a></p>

<p><a href="../archive/news7864.php">Leaky Arguments</a></p>

<p><a href="../archive/news7865.php">Seeds Of More Ruction</a></p>

<p><a href="../archive/news7866.php">Hideous Khimki</a></p>

<p><a href="../archive/news7867.php">Tuckers Luck</a></p>

<p><a href="../archive/news7868.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 2nd September 2011 | Issue 786</b></p>

<p><a href="news786.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>EDL: IT TAKES ONE TO E1</h3>

<p>
<p>  
  	<strong>AS SCHNEWS SIZES UP ANTI-FASH OPTIONS FOR TOWER HAMLETS BASH</strong>  </p>  
   <p>  
  	<strong>&ldquo;<em>Any chance we can do away with all the paki&rsquo;s and indians, they ain&rsquo;t no good to man or beast</em>&rdquo; - Brian Brison Rotchell, London supporter of &lsquo;human rights organisation&rsquo; EDL.</strong>  </p>  
   <p>  
  	Despite Home Secretary Theresa May announcing a ban on all marches in Tower Hamlets, Newham, Waltham Forest, Islington, Hackney and the City of London for 30 days &ndash; the far-right English Defence League (EDL) will still be attempting to stage their biggest demonstration yet, in the heart of London. As anti-fascists, anti-racists, socialists and anarchists we should ensure we are there to greet whoever turns out on 3rd September (see <a href='../archive/news785.htm'>SchNEWS 785</a>).  </p>  
   <p>  
  	Despite the draconian nature of the ban &ndash; it stops any protest marches in much of London for 30 days &ndash; it is not possible (yet) to ban static demos. The EDL have now announced &lsquo;muster pubs&rsquo; of O&rsquo;Neils and The Euston Flyer both on Euston Rd, NW1. From 2pm they are going to be marched in small groups to a heavily policed static demonstration, however most of their rank&rsquo;n&rsquo;file are unlikely to be satisfied with this &ndash; statements such as &ldquo;we&rsquo;ll march wherever we fucking want&rdquo; and (unfortunately) &ldquo;whose streets?&rdquo; are now commonplace on EDL forums.  </p>  
   <p>  
  	<strong>TROUBLE BREWING</strong>  </p>  
   <p>  
  	EDLers tend to meet up in pubs before hand then stumble to the demo start points to be marched around their city du jour in a mobile kettle before being deposited back in the drinking houses to toast their massive success. Given the media attention on the EDL, the small number of pubs still willing to host them (they made do with 2 that are more than 3 miles from their demo this time) and their well documented inability to drink a pint of Stella without shouting &ldquo;Allah is a paedo!&rdquo; it is somewhat surprising the hierarchy have stuck by this strategy &ndash; albeit with the march (officially) replaced by a static shouting contest at an unannounced location, believed to be near Aldgate East tube station.  </p>  
   <p>  
  	Police will most likely build a massive pen and devise a strategy to funnel the fash into it. What happens on the day though will depend on police tactics as well as EDL and opposition numbers.  </p>  
   <p>  
  	If they get a large turnout they will attempt to get near their &lsquo;targets&rsquo; (mosques, lefties, libraries), in which case it is up to us to stop them. Alternatively the EDL may be (physically) fragmented &ndash; either by choice or by baton. In this scenario smaller groups of boneheads may try to make their way into Tower Hamlets where locals and antifascists should be ready to run them out of town.  </p>  
   <p>  
  	 <br />  
  	<strong>UNITE MARE</strong>  </p>  
   <p>  
  	The only thing that is certain is that the greater the numbers on the streets to oppose them, the higher the chances of inflicting one last humiliating defeat on Tommy&rsquo;s Boys. Much of the soft Left has been calling for people to &ldquo;not [rise] to the provocation&rdquo;, &ldquo;turn our backs&rdquo; and &ldquo;stay at home and let the police deal with any visit by the EDL&rdquo;. Well we say f*ck that! You can&rsquo;t talk about how great Cable Street was and, in the same breath, discourage the community from opposing fascism on their doorstep.  </p>  
   <p>  
  	Having brought the ban about, Unite Against Fascism (UAF) have spent the last few days bitching that it shouldn&rsquo;t apply to them. They&rsquo;ve now (sensibly) moved their assembly point from Weavers Field to the corner of Vallance Road and Whitechapel Road, at 11am. This is the same area autonomous groups had been calling for people to gather and, while the increased numbers are welcome, they will likely come burdened with a police pen of their own as well as UAF stewards attempting to control the crowds.  </p>  
   <p>  
  	Although it&rsquo;s always preferable to avoid being kettled, if we are contained in the very area the EDL want to be it will still be a victory &ndash; of sorts. However our ability to stay mobile without being picked off is our greatest strength.  </p>  
   <p>  
  	* More at <a href="http://malatesta32.wordpress.com" target="_blank">http://malatesta32.wordpress.com</a>  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1339), 155); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1339);

				if (SHOWMESSAGES)	{

						addMessage(1339);
						echo getMessages(1339);
						echo showAddMessage(1339);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


