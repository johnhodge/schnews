<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 661 - 9th January 2009 - Media War</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, gaza, palestine, israel, media, bbc" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 661 Articles: </b>
<p><a href="../archive/news6611.php">Eyewitness : Rafah</a></p>

<p><a href="../archive/news6612.php">Eyewitness : Gaza</a></p>

<p><a href="../archive/news6613.php">Global Outrage</a></p>

<p><a href="../archive/news6614.php">Gaza Relief Boat Rammed</a></p>

<p><a href="../archive/news6615.php">Ceasefire? What Ceasefire?</a></p>

<p><a href="../archive/news6616.php">Gaza Gameplan</a></p>

<b>
<p><a href="../archive/news6617.php">Media War</a></p>
</b>

<p><a href="../archive/news6618.php">Elementary My Dear Watson</a></p>

<p><a href="../archive/news6619.php">Inside Schnews</a></p>

<p><a href="../archive/news66110.php">Outside Schnews</a></p>

<p><a href="../archive/news66111.php">Shac Verdict</a></p>

<p><a href="../archive/news66112.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 9th January 2009 | Issue 661</b></p>

<p><a href="news661.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>MEDIA WAR</H3>

<p>
<b>It's worth looking at how the mainstream media has been used as a tool in the Israel Army's high tech arsenal</b>. Israel attacked in the days between Christmas and New Year in the West. This is media 'down time' when both journalists and audiences are away from the offices, and most people are more concerned with sleeping off hangovers than watching the news. Major events can pass by with little initial outrage. This isn't the first time this tactic has been tried in recent years. On Christmas Eve 2006 Ethiopia and the USA attacked Somalia whilst the world's press was on holiday. It seems to be taken straight out of a US State Department handbook. <br />
 <br />
Israel has an army of highly polished, attractive spokespeople on standby to justify war crimes at a moment's notice. These talking heads are trotted out to explain why Israel never targets civilians, why the Qassam rockets threaten the existence of Israel, how Israel had been preparing this attack for six months and how that they were caught unawares. <br />
 <br />
But then Israel doesn't normally have to try so hard to justify themselves when they have the BBC to back them up. Especially in those first few days, the BBC was up to its old Iraq War tricks. <br />
 <br />
The first day of the bombing (230 dead), the lead article on the BBC website was "Israel Prepares for Prolonged Op" - a misleadingly biased headline for a massacre. They then state that Israel is bombing Gaza to stop rocket fire. No Israelis are quoted as saying this, it's taken as objective truth. When the toll of Palestinian dead is read out, the Palestinians are merely quoted as claiming so many died. Israel is then given over 100 words to describe why it had to attack Gaza. The Palestinian government of Gaza has to wait until the 3rd from last line to explain that there was "an ugly massacre" (three words only). <br />
 <br />
This style of reporting only applies to Israel and other chosen allies. When the BBC report on Darfur, they don't feel the same need to interview the Janjaweed militia commander about why he felt it necessary to burn the village to the ground. When the story of the Nazi death camps broke, the BBC didn't cut from the piles of dead bodies to the Jeremy Paxman of the day saying "And here with me in the studio is Joseph Goebbels. Mr Goebbels, can you explain to us why you think the final solution is necessary to defend the Reich?" <br />
 <br />
But this kind of media spin only works for a while. As the far right Jerusalem Post helpfully explains: "there are few dramatic pictures from Israel, and gaping holes in apartment buildings hit by Grad rockets can't compete with footage from Gaza of crying children splattered in blood." In other words - our propaganda campaign can't compete with the images of murdered children. <br />
 <br />
That's why Israel has imposed a news blackout on Gaza. Journalists are not allowed in. If Israel can't stop the terrible images coming out from the Gaza Strip, then at least they can stem the flow. And the Beeb is always helpfully on hand to remind people back home that "numbers of dead cannot be independently verified," as if Palestinian Medics can't be trusted to count the bodies in the morgue.
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=gaza&source=661">gaza</a>, <a href="../keywordSearch/?keyword=palestine&source=661">palestine</a>, <a href="../keywordSearch/?keyword=israel&source=661">israel</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(186);
			
				if (SHOWMESSAGES)	{
				
						addMessage(186);
						echo getMessages(186);
						echo showAddMessage(186);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>