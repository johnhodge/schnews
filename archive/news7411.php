<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 741 - 1st October 2010 - Unrest For The Wicked</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, eu, direct action, brussels, anti-capitalism, mass demonstrations, strike, financial crisis, poland, greece" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 741 Articles: </b>
<b>
<p><a href="../archive/news7411.php">Unrest For The Wicked</a></p>
</b>

<p><a href="../archive/news7412.php">No Borders Update: Bruss Versus Them</a></p>

<p><a href="../archive/news7413.php">Edls On Wheels</a></p>

<p><a href="../archive/news7414.php">Schnewsflash: Ecuador</a></p>

<p><a href="../archive/news7415.php">Pump Action</a></p>

<p><a href="../archive/news7416.php">Cheeky Minks</a></p>

<p><a href="../archive/news7417.php">Inca-hoots</a></p>

<p><a href="../archive/news7418.php">Siege Of Gaza: Update</a></p>

<p><a href="../archive/news7419.php">Tek The Biscuit</a></p>

<p><a href="../archive/news74110.php">Happen-stance</a></p>

<p><a href="../archive/news74111.php">Amsterdam-busters</a></p>

<p><a href="../archive/news74112.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 1st October 2010 | Issue 741</b></p>

<p><a href="news741.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>UNREST FOR THE WICKED</h3>

<p>
<p>
	<strong>A EUROPEAN UNION OF PROTESTS KICKS OFF AGAINST AUSTERITY MEASURES</strong> </p>
<p>
	Mass protests and strikes have broken out across the whole of Europe this week as the reality of already imposed and still pending austerity cuts becomes clear. Across the EU, rallies were held in thirteen capital cities and in Spain a general strike saw millions take action. </p>
<p>
	On Wednesday (29th) around 100,000 representatives of the European trade union movement, including German miners and Polish shipbuilders, brought Brussels to a standstill to protest against the forthcoming savage spending cuts. The message &ldquo;We will not pay for their crisis&rdquo; is now resounding across Europe. </p>
<p>
	Despite trade union claims that the event passed off entirely peacefully, the anti-capitalist bloc on the demonstration clashed with police. Their numbers swelled by visiting No Borders activists (see <a href='../archive/news740.htm'>SchNEWS 740</a>), local anti-capitalists joined the main demo. However they were isolated and surrounded by plain-clothes police. Over a hundred were arrested &lsquo;preventatively&rsquo; and many others injured. There was a heavy police presence outside the No Borders campsite all day. </p>
<p>
	As the day of action - called by trade union umbrella organisation ETUC - took place outside, the EU Commission announced a package of proposals to crack down on hard-pressed member states, threatening them with huge fines if they failed to run their economies &ldquo;efficiently.&rdquo; <br /> 
	There can be no doubt that the Commissions idea of &lsquo;efficiency&rsquo; will closely mirror the IMF prescriptions for failing economies - privatise everything in sight, cut benefits and drive down public sector wages. Banks of course, will still profit healthily having receiving life support from the public sector. </p>
<p>
	Meanwhile in Greece, where the financial crisis hit first and hardest in a country that was already in open revolt (see <a href='../archive/news711.htm'>SchNEWS 711</a>, <a href='../archive/news720.htm'>720</a>, <a href='../archive/news733.htm'>733</a>), new laws were introduced criminalising trade union activity. The government has completely removed the protective mechanisms within previous anti-terror legislation which previously prevented it from applying to trade unions or political activity &lsquo;in defence of freedom&rsquo;. </p>
<p>
	Simultaneously the law now punishes more severely (with 10 years imprisonment) anyone who gives &ldquo;substantial&rdquo; information to terrorist organizations to facilitate their work, or gives them material or &ldquo;immaterial&rdquo; support even if the &ldquo;terrorist acts&rdquo; were not finally carried out. </p>
<p>
	With these changes, the prosecuting authorities are basically given a free hand. Even minor offences (such as property damage or disruption of transport, etc) carried out by an organized team of demonstrators can now be arbitrarily characterised as terrorist activity. Coupled with this is new legislation allowing state&rsquo;s witnesses to be anonymous. Can the rest of Europe expect similar draconian legislation as unrest spreads? </p>
<p>
	<em><strong>Poland</strong></em> </p>
<p>
	Trade unionists threw petards and blew whistles as they marched through Warsaw demanding an end to &lsquo;abuse by economic elites&rsquo;. </p>
<p>
	<em><strong>Spain</strong></em> </p>
<p>
	Unions claimed ten million workers stayed at home for the nationwide strike. There were 11 reported injuries and 65 arrests during clashes with police. </p>
<p>
	<em><strong>Greece</strong></em> </p>
<p>
	Rail workers and doctors walked out yesterday in protest at deep cuts into workers&rsquo; allowances, which are designed to reduce the national deficit. </p>
<p>
	<em><strong>Slovenia</strong></em> </p>
<p>
	About half of public-sector workers remained on strike for the third day against a planned wage freeze. </p>
<p>
	<em><strong>Ireland</strong></em> </p>
<p>
	One man blocked the entrance to the Dail, the Irish parliament, with a cement truck in a protest against the country&rsquo;s enormous bank bailouts. Written across the back of the lorry was: &ldquo;All politicians should be sacked.&rdquo; - finally a concrete answer we can all agree with. </p>
<p>
	* For more from Brussels see <a href="http://bxl.indymedia.org" target="_blank">http://bxl.indymedia.org</a> (mostly in French), <a href="http://www.noborderbxl.eu.org" target="_blank">www.noborderbxl.eu.org</a> (in 12 languages including English) </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(952), 110); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(952);

				if (SHOWMESSAGES)	{

						addMessage(952);
						echo getMessages(952);
						echo showAddMessage(952);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


