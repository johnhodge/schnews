<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 788 - 16th September 2011 - World's Largest Arms Fail</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, arms trade, dsei, direct action" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../pages_merchandise/merchandise_books.php" target="_blank"><img
						src="../images_main/in-graphic-detail-banner2.jpg"
						alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 788 Articles: </b>
<b>
<p><a href="../archive/news7881.php">World's Largest Arms Fail</a></p>
</b>

<p><a href="../archive/news7882.php">Frack Shit Up</a></p>

<p><a href="../archive/news7883.php">Brighton 9: Sticking It To The Manikin</a></p>

<p><a href="../archive/news7884.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news7885.php">Nice One Giza</a></p>

<p><a href="../archive/news7886.php">De-fault Is All Greece's</a></p>

<p><a href="../archive/news7887.php">Edl: Tommy Chickens Out</a></p>

<p><a href="../archive/news7888.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 16th September 2011 | Issue 788</b></p>

<p><a href="news788.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WORLD'S LARGEST ARMS FAIL</h3>

<p>
<p>  
  	<strong>AS SchNEWS PADDLES IN THE MURKY WATERS OF THE ARMS TRADE</strong>  </p>  
   <p>  
  	This week London&rsquo;s Excel Centre has been hosting the biennial death-fest DSEi &ndash; the world&rsquo;s largest arms fair. Part funded and organised by the British government, DSEi is the place where warmongers tool up on the latest guns &lsquo;n&rsquo; ammo. Both the weapons used by Colonel Gaddafi and those used against him are on sale at DSEi. The UK exported &pound;33.8m of military and related products to Libya between Oct 2009 and Oct 2010.  </p>  
   <p>  
  	This year the highlight of the weapons wankathon got stopped in its tracks before it even reached the fair. Six protesters in inflatable dingies launched themselves into the path of the HMS Dauntless last Saturday as it was on its way to Victoria Docks ahead of DSEI, causing it to come to an unscheduled stop in the waters near Barking, Dagenham. It finally docked some 4 hours later. Of the 6 protesters, one got away and the other 5 were briefly detained by the river police.  </p>  
   <p>  
  	The Dauntless is a Type 45 Destroyer, one of the Royal Navy&rsquo;s latest, most high-tech battleships, as well as one of the newest, commissioned just last year. Supposedly able to track and destroy jet fighters and other warships, it must have been embarrassing for the captain to be caught by surprise by the tiny blowup flotilla. No doubt it will now be retrofitted with anti-inflatable technology.  </p>  
   <p>  
  	DSEi wouldn&rsquo;t be DSEi without several days of creative non-violent protest and a massive pig-fest, and Indymedia have a timeline of the entire thing. (See <a href="http://www.indymedia.org.uk/en/2011/09/484571.html" target="_blank">http://www.indymedia.org.uk/en/2011/09/484571.html</a>) A spontaneous die-in was held by activists from Campaign Against the Arms Trade, forcing arms dealers to walk over the restless dead to get to DSEi. One protester has been charged with aggravated trespass, and another nicked for throwing paint. Other protesters got in the way of arms dealers and corporate lobbyists on the trains to the (un)fair.  </p>  
   <p>  
  	The group Stop The Arms Fair pulled off a coup after getting word that the delegates would be attending an evening reception at the National Gallery. Whilst delegates struggled to get into the gallery via the back door, around 150 protesters came face to face with the arms dealers, forcing them to run the gauntlet of the baying mob.  </p>  
   <p>  
  	Although there was no single large mobilisation this year, there was a slew of actions by small, mobile and creative affinity groups that caused a headache for cops and war criminals alike. Stop the Arms Fair, a coalition of several groups including CAAT and Disarm DSEi, are already planning for 2013.  </p>  
   <p>  
  	*For more dsei.org &amp; stopthearmsfair.org.uk  </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1356), 157); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1356);

				if (SHOWMESSAGES)	{

						addMessage(1356);
						echo getMessages(1356);
						echo showAddMessage(1356);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


