<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 739 - 17th September 2010 - Cut to the Chase: Anti-austerity Coalition Launched</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, austerity, cuts" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.smashedo.org.uk/hammertime.htm" target="_blank"><img
						src="../images_main/hammertime-banner.jpg"
						alt="ITT's Hammertime at EDO-MBM/ITT, the Brighton bomb parts factory, October 13th."
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 739 Articles: </b>
<p><a href="../archive/news7391.php">Gauling Behaviour</a></p>

<p><a href="../archive/news7392.php">Tesco Check-out</a></p>

<b>
<p><a href="../archive/news7393.php">Cut To The Chase: Anti-austerity Coalition Launched</a></p>
</b>

<p><a href="../archive/news7394.php">Fire To The Prisons</a></p>

<p><a href="../archive/news7395.php">Calais: Channel Hoping</a></p>

<p><a href="../archive/news7396.php">Scottish Open Cast Mining: What's Happendon?</a></p>

<p><a href="../archive/news7397.php">Fash Mobs</a></p>

<p><a href="../archive/news7398.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 17th September 2010 | Issue 739</b></p>

<p><a href="news739.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CUT TO THE CHASE: ANTI-AUSTERITY COALITION LAUNCHED</h3>

<p>
<p>
	The bankers and their millionaire mates have never had it better. During the last two years the government has rescued big financial institutions and has spent lots of public money in an attempt to &lsquo;stimulate&rsquo; the economy. Now the masses are being asked to pay the bill: on 20 October the Con-Dem government will publish a budget which is going to make huge cuts in public services and benefits. Among other savage cuts, a proposed cut in the levels of all housing allowances is going to affect millions of tenants, whether they are &lsquo;hard workers&rsquo; or not. It has already been estimated that 700,000 families will lose their homes. </p>
<p>
	Too busy playing with their giant financial scissors perhaps, the government has conveniently ignored calculations that show that it is perfectly possible to cover the deficit by raising taxes for the rich and the bankers, who caused the crisis in the first instance, and tackling tax avoidance and tax evasion. Only this month it was revealed that the business-friendly government have done a cosy deal that effectively lets Vodafone off entirely for offshore tax dodging that&rsquo;s cost around &pound;6bn in revenue &ndash; just about the same amount as the Con-Dem&rsquo;s said needed cutting from public budgets immediately. </p>
<p>
	On Thursday (9th) more than 200 people attended the launch of the Stop the Cuts Coalition (STCC), formed by unionised workers, anti-privatisation campaigners such as Keep our NHS Public and Keep the Post Public, representatives of the voluntary sector, and Brighton Benefits Campaign. Coalitions like these are mushrooming all over the country, while the police warns the government that their own budget cannot be cut, or they won&rsquo;t be able to restrain a coming waves of strikes and social unrest. </p>
<p>
	STCC is now working hard to turn those fears into reality. </p>
<p>
	* Wednesday 29th of September is the<strong> European Day of Action</strong> against cuts &ndash; a day when Greece or Spain will be paralysed by mass actions. Meanwhile in Brighton a demo will start at King&rsquo;s House, Grand Avenue, at 4.00 and will march to Brighton Town Hall for a rally at 5.00pm. </p>
<p>
	* On October 20th, the Spending Review will be announced to Parliament. There&rsquo;ll be a <strong>mass street action</strong> in the evening. Wait for announcement of the assembly, place and time. </p>
<p>
	<strong>* Public meeting</strong> on benefit cuts: next Thursday, 23 September, 7.00pm, Friend Meeting House, Ship Street, Brighton. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(938), 108); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(938);

				if (SHOWMESSAGES)	{

						addMessage(938);
						echo getMessages(938);
						echo showAddMessage(938);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


