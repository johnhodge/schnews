<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 746 - 4th November 2010 - G4S Deportation Death</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, deportation, migrant rights, detention, immigration" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../index.htm" target="_blank"><img
						src="../images_main/sch-ben-banner.jpg"
						alt="SchNEWS Benefit Gig at Hectors House 31st October 2010"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 746 Articles: </b>
<p><a href="../archive/news7461.php">The Vodafone-y War</a></p>

<p><a href="../archive/news7462.php">Students Do It Dub-style</a></p>

<p><a href="../archive/news7463.php">Need Scumbody To Love</a></p>

<p><a href="../archive/news7464.php">Coal-ition Politics</a></p>

<p><a href="../archive/news7465.php">Above Their Station</a></p>

<b>
<p><a href="../archive/news7466.php">G4s Deportation Death</a></p>
</b>

<p><a href="../archive/news7467.php">3-pronged Attack</a></p>

<p><a href="../archive/news7468.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Thursday 4th November 2010 | Issue 746</b></p>

<p><a href="news746.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>G4S DEPORTATION DEATH</h3>

<p>
<p>
	Migrant rights activists will be marching on the Home Office next Friday (12th) in protest against the killing of refugee Jimmy Mubenga at the hands of deportation heavies G4S. <br /> 
	&nbsp;On October 12th, Jimmy was to be deported to his native Angola, a country he had fled fourteen years earlier after facing persecution for his political activism. Three G4S security guards escorted Jimmy onto a British Airways flight where, after talking to his wife for the last time, he became agitated and according to eyewitness accounts began screaming and shouting &ldquo;<em>They are going to kill me</em>&rdquo;. </p>
<p>
	The G4S thugs handcuffed him and pinned him down in his chair. Stunned passengers listening to his cries of pain were reassured by his &lsquo;escorts&rsquo; that &ldquo;He&rsquo;ll be alright once we get him in the air &ndash; he just doesn&rsquo;t want to go&rdquo;. One passenger estimated they restrained him in that position for 45 minutes. For the last ten minutes he was repeatedly heard to say he couldn&rsquo;t breathe. </p>
<p>
	When Jimmy lost consciousness, paramedics were called and he was taken to hospital where he was pronounced dead. He left behind a wife and five children. </p>
<p>
	In 1996 Jimmy fled the oil-fuelled cycle of conflict, repression and abuse that has blighted Angola for decades. After making a life for himself in Ilford, his world began to fall apart in 2006 after he became involved in a bar brawl. For this one mistake, a first offence, he was sentenced to two years for ABH. </p>
<p>
	After serving his sentence he was sucked into the blackhole of indefinite incarceration in an immigration detention centre. His desperate attempts to be reunited with his family &ndash; who are legal UK residents &ndash; were frustrated, with at least seven applications to be released on bail rejected. After spending 14 months in detention, he was finally released on bail, but was to spend two further stints in detention centres over the next two years &ndash; the last of his life. </p>
<p>
	The three G4S security guards involved were arrested but released on bail til December and have not yet been charged with any offence. G4S has a long history of abuse and violence while carrying out the UK Border Agency&rsquo;s dirty work, having rebranded from Group 4 in an attempt to escape their negative public image (see also <a href='../archive/news419.htm'>SchNEWS 419</a>). </p>
<p>
	In addition to dragging migrants back to countries torn apart by war, conflict or repression, they have also been running four of the UK&rsquo;s eleven immigration detention centres and most of the UK&rsquo;s short-term holding facilities at ports, airports and immigration reporting centres. While raking in the millions through these lucrative contracts, G4S have time and again been involved in scandals over abuse of migrants (see medicaljustice.org.uk and corporatewatch.org.uk for reports). </p>
<p>
	G4S have since lost the contract to remove deportees and are to be replaced by Reliance Security Task Management Limited, a firm currently to be found lurking in police stations and prison systems around the country. </p>
<p>
	With Jimmy&rsquo;s death bringing the brutal treatment of migrants in the UK immigration centre to the country&rsquo;s attention, migrant rights groups alongside many migrants themselves have been calling for not just for accountability over his death, but also an examination of the dehumanising system responsible. </p>
<p>
	The march will assemble at the Angolan Embassy, 22 Dorset Street London, on Friday 12th at 10.30am before heading to the home office. </p>
<p>
	* See <a href="http://www.corporatewatch.org.uk" target="_blank">www.corporatewatch.org.uk</a> <a href="http://www.ncadc.org.uk" target="_blank">www.ncadc.org.uk</a> </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1001), 115); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1001);

				if (SHOWMESSAGES)	{

						addMessage(1001);
						echo getMessages(1001);
						echo showAddMessage(1001);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


