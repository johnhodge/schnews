<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 744 - 22nd October 2010 - France: Will You Still Need Me, Feed Me, When I'm 62?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, france, protest, fuel shortages, cuts" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://www.anarchistbookfair.org.uk" target="_blank"><img
						src="../images_main/an-bkfr-2010-banner.jpg"
						alt="Anarchist Bookfair 2010, London, October 23rd"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 744 Articles: </b>
<p><a href="../archive/news7441.php">Crude But Refined</a></p>

<p><a href="../archive/news7442.php">Huntington Eviction Alert</a></p>

<p><a href="../archive/news7443.php">The Big Libcon Cuts: To The Manor Osbourne</a></p>

<b>
<p><a href="../archive/news7444.php">France: Will You Still Need Me, Feed Me, When I'm 62?</a></p>
</b>

<p><a href="../archive/news7445.php">Chev It Up Ya Prs</a></p>

<p><a href="../archive/news7446.php">Who Ya Gonna Coal?</a></p>

<p><a href="../archive/news7447.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 22nd October 2010 | Issue 744</b></p>

<p><a href="news744.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>FRANCE: WILL YOU STILL NEED ME, FEED ME, WHEN I'M 62?</h3>

<p>
<p>
	With violence in the cities, rubbish piling up in the streets, fuel supplies running low, transport chaos and school blockades, France is paralysed and reaching boiling point. </p>
<p>
	The rolling strikes continued on Wednesday (20th), causing widespread disruption to schools and transport and adding to a growing fuel crisis. One third of petrol stations across France still had no fuel. All 12 refineries and some fuel depots were blockaded but police broke up the barricades at three strategic fuel depots in Donge, Le Mans and La Rochelle. </p>
<p>
	Pickets and stoppages continued at airports with Toulouse airport blockaded and cancellations at Orly and Charles de Gaulle in Paris. Train, bus and tram staff were still striking across the country. Bus drivers in Rennes went on strike after tear gas was fired at the city&rsquo;s bus depot. Police fired tear gas to break up demonstrations in the eastern cities of Mulhouse and Montbelliard. In Marseille piles of uncollected rubbish are creating a stench across the city. </p>
<p>
	In Lyon and the outskirts of Paris an outbreak of &lsquo;mini-riots&rsquo; saw disenchanted youth fight running battles with police, burning cars and smashing shop windows. The interior minister, Brice Hortefeux, threatened to send in paramilitary police to tackle the violence, saying, &ldquo;<em>We will mobilise all means necessary to put these thugs out of harm&rsquo;s way</em>&rdquo;, adding that he would not hesitate to send in armed units. Officially, 1,423 people involved were arrested in the past week, including the arrest of 428 rioters on Tuesday (19th). Some 1,000 people remain in custody. </p>
<p>
	On Saturday night (16th), there was a miniature storming of the Bastille by 200 anarchists chanting the slogan: &ldquo;<strong>Down with the state, the cops and the management!</strong>&rdquo; </p>
<p>
	In addition to violence in Lyon and near Paris, Tuesday (19th) saw 4,000 petrol stations waiting on supplies. Sarkozy was, er, driven to plead for calm and responsibility but insisted he had no intentions of backing away from his pension reforms. Day 6 also gave rise to increasing action by children with 379 secondary schools blockaded. </p>
<p>
	With women, employees in hazardous jobs and the young set to be hit hardest by the unjust reforms, the protests have seen up to 3.5 million take to the streets. Sarkozy however, is holding firm and on Thursday (21st) warned violent protesters that they would not &ldquo;have the last word&rdquo; and would be pursued and punished &ldquo;with no weakness&rdquo; on the part of the authorities. </p>
<p>
	Over to you, Britain.. </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(983), 113); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(983);

				if (SHOWMESSAGES)	{

						addMessage(983);
						echo getMessages(983);
						echo showAddMessage(983);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


