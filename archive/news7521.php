<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS 752 - 17th December 2010 - The Well Unfair State</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, cuts, benefits, welfare state, tory" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" />


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>


</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="../schmovies/index.php" target="_blank"><img
						src="../images_main/raiders-banner.jpg"
						alt="Raiders Of The Lost Archive Vol 1 - the new SchMOVIES DVD - OUT NOW!"
						border="0"
				/></a>
			</div>

</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 752 Articles: </b>
<b>
<p><a href="../archive/news7521.php">The Well Unfair State</a></p>
</b>

<p><a href="../archive/news7522.php">Soar Point</a></p>

<p><a href="../archive/news7523.php">No White Xmas After All</a></p>

<p><a href="../archive/news7524.php">Another Smashing Week</a></p>

<p><a href="../archive/news7525.php">Wiki-keeper Bailed</a></p>

<p><a href="../archive/news7526.php">Gaza Vigil</a></p>

<p><a href="../archive/news7527.php">A Cocktail Of Two Cities</a></p>

<p><a href="../archive/news7528.php">And Finally</a></p>


		</div>


</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000">
									<tr>
										<td>
											<div align="center">
												<a href="../images/752-nativity-lg.jpg" target="_blank">
													<img src="../images/752-nativity-300.jpg" alt="" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 17th December 2010 | Issue 752</b></p>

<p><a href="news752.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>THE WELL UNFAIR STATE</h3>

<p>
<p>
	<strong>SCHNEWS SURVEYS THE GOVT&rsquo;S PLANS FOR WICKED CUTS TO BENEFIT SYSTEM</strong> </p>
<p>
	Tuition fees got you riled up? Wait &lsquo;til you get a handle on what our Tory chums have got planned next - flagged up since the first announcement of spending cuts, the recent consultation paper entitled Universal Credit - is a massive demolition (sorry, overhaul) of the existing benefits system. With all benefits and tax credits being rolled into one system, the screws are gonna get tightened. </p>
<p>
	As part of their &lsquo;divide and rule&rsquo; tactics, the present government of millionaires claim that the coming cuts in benefits are all being made against those out of work - in the interest of those in work. This is a lie. In fact 80% of those in receipt of housing benefit are working, and there will be cuts on Working Tax Credits! </p>
<p>
	The truth is that the proposed benefits cuts are not made against &lsquo;dole scroungers&rsquo;, they are made against all of us, whether in work or out of work, on benefits or not. Their aim is to lower wages and make us fight each other, rather than fighting against them. Only the rich and the bankers who caused the economic crisis will benefit. </p>
<p>
	According to Brighton Benefits Camapign, who alongside many around the country are staging protests -<em>&rdquo;It is not time anymore to have &lsquo;disabled&rsquo;, &lsquo;single mothers&rsquo;, or &lsquo;dole claimants&rsquo; campaigns - this is an attack on everybody, especially those in work, and we need to fight together. This is NOT a struggle to protect the state and its benefits system, but to defend ourselves from a vile attack from an entrenched gang of millionaires and big businesses, who want to use the benefit system to squeeze us and multiply their profits</em>.&rdquo; </p>
<p>
	Housing benefits will be one of the biggest casualties, decimated by politicians who have never in their silver-spoon lives had to worry how they&rsquo;re gonna pay the rent. </p>
<p>
	<strong>SQUEEZE THE SPONGERS!</strong> </p>
<p>
	While Local Housing Allowances - the amount of housing benefit you can claim - will be reduced across the board to match the lowest 30% of rents in the area (rather than the previous 50%), there are to be caps on LHA. This&rsquo;ll cause many of the highest price property areas, like inner-city London, to become stomping grounds of the rich only, &lsquo;cleansing&rsquo; the areas from ordinary tenants on lower wages. </p>
<p>
	If your under 35 and single, you&rsquo;ll only be able to claim for rooms in shared accommodation. And if you&rsquo;re on JSA and Housing Benefits for a year, your housing benefit will be slashed by 10%, along with the lower JSA you can receive and the hassle from the Job Centre. In the government&rsquo;s warped logic, the mass unemployment caused by the recession and the fucked-up sky-high rents seen as the norm are you&rsquo;re fault. And you have to pay. </p>
<p>
	In the biggest assault on the poorest sections of society for a generation, around 750,000 people are set to become homeless as a result of these changes according to the Chartered Institute of Housing. With an average decrease in benefit of &pound;12 a week, many households will be forced into spiralling debt. That&rsquo;s mainly working households, remember. </p>
<p>
	There&rsquo;s another benefit that&rsquo;s designed to ease the poverty and inequality that&rsquo;s a direct result of the capitalist system: Tax Credits. </p>
<p>
	These are set to be abolished in 2014, and are being cut now. Although Child Tax Credit looks safe for the time being, draconian cuts are being made to Working Tax Credits. Parents will have to work 24 hours a week to qualify at all, and from April 2012, if your income decreases by less than &pound;2500 you&rsquo;ll still not be entitled to any increase in your Working Tax Credit. This&rsquo;ll cause extreme hardship. According to estimations made by the Tax Credits Unit at least half a million working families will lose more than &pound;1,000 per year due to these changes. </p>
<p>
	<strong>DOLING OUT PUNISHMENT</strong> </p>
<p>
	If this makes you want to escape the rat-race and earn your crust on your own terms, you&rsquo;re out of luck. If you&rsquo;re self-employed you will have to prove your earnings work out at the minimum wage for your hours. If you work less than 16 hours a week (or even more) you&rsquo;ll be categorised as an unproductive member of society - and forced into the job centre to sign on and look for a &lsquo;real&rsquo; job. </p>
<p>
	Finally, many of those currently receiving Disability Living Allowance may lose their entitlement after it&rsquo;s replaced by the stricter Personal Independence Payment. Bizarrely, if you can use your wheelchair, for example, you won&rsquo;t be entitled to the mobility component of the benefit. </p>
<p>
	All of this will be obsolete in 2014, when most benefits including tax credits and housing are simply deleted in favour of a &lsquo;Universal Credit&rsquo;. In line with the government&rsquo;s plan to get us all slaving away at insecure and badly paid jobs, this plays into the hands of the monster corporations who&rsquo;d prefer not to have to give us secure employment and decent wages. No wonder so many big business bullies like M&amp;S and Boots gave the government their whole-hearted support. </p>
<p>
	This is the crux of the government&rsquo;s ideologically-driven assault on welfare state, and that old-fashioned idea that just maybe, people have a right to decent work and a decent wage, to be able to support and house themselves and their families. </p>
<p>
	Resistance is brewing. Across the land, people are realising that the selfish, money-grabbing twats in power and their business cronies can&rsquo;t get away with it. With the biggest splashes so far coming from the student demos and the tax avoidance actions prompted by UKUncut (see <a href='../archive/news746.htm'>SchNEWS 746</a>,<a href='../archive/news751.htm'>751</a>), the coming months will see action on a wider scale. </p>
<p>
	To read the full proposals <a href="http://www.dwp.gov.uk/policy/welfare-reform/legislation-and-key-documents/universal-credit" target="_blank">www.dwp.gov.uk/policy/welfare-reform/legislation-and-key-documents/universal-credit</a> <br /> 
	&nbsp; </p>
</p>

</div>
 <?php echo createKeywordList(get_keywords_for_article(1040), 121); ?>

<br /><br />

		</div>


		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">

			<?php

				recordHit(1040);

				if (SHOWMESSAGES)	{

						addMessage(1040);
						echo getMessages(1040);
						echo showAddMessage(1040);

				}

			?>

		</div>





			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?).
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS
			is post-free to prisoners. </font></p><p>
			</p><p></p>



</div>



<div class='mainBar_right'>

<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

	
</div>






<div class="footer">

<!--#include virtual="../inc/footer.php"-->

</div>








</div>




</body>
</html>


