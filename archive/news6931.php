<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 693 - 2nd October 2009 - Who Are Ya?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, english defence league, far-right, nick griffin, bnp, racism" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.anarchistbookfair.org/"><img 
						src="../images_main/anc-bkfr-09-banner.jpg"
						alt="Anarchist Bookfair"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 693 Articles: </b>
<b>
<p><a href="../archive/news6931.php">Who Are Ya?</a></p>
</b>

<p><a href="../archive/news6932.php">Press Ganged</a></p>

<p><a href="../archive/news6933.php">Given A Barak-ing</a></p>

<p><a href="../archive/news6934.php">On Yer Denmarks....</a></p>

<p><a href="../archive/news6935.php">Vestas Interests</a></p>

<p><a href="../archive/news6936.php">All Sizewell That Ends Well</a></p>

<p><a href="../archive/news6937.php">693 Schnews In Brief</a></p>

<p><a href="../archive/news6938.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/693-capello-lg.jpg" target="_blank">
													<img src="../images/693-capello-sm.jpg" alt="What England needs now is much more strength down the left wing..." border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 2nd October 2009 | Issue 693</b></p>

<p><a href="news693.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>WHO ARE YA?</h3>

<p>
<strong>AS SchNEWS LOOKS THE WRONG WAY AT THE ENGLISH DEFENCE LEAGUE</strong> <br />  <br /> <strong>Seemingly coming from nowhere, the EDL and their allies have grabbed headlines and created a very public right-wing street presence for the first time in many years.</strong> Although they&rsquo;ve had a sound thrashing everywhere they&rsquo;ve raised their heads since their first announced demo in Birmingham in August, (see <a href='../archive/news687.htm'>SchNEWS 687</a>) they keep coming back for more. <br />   <br /> In fact their latest flyer takes advantage of the beatings they&rsquo;ve received &ndash; with the slogan &ldquo;<strong>It&rsquo;s not racist to oppose Radical Islam</strong>&rdquo; the image  (taken during the Birmingham fighting), is of a grey haired geezer having the Union Jack pulled off him by a snarling mob of Asian youth. In a high profile grab for attention they&rsquo;re planning demos in Manchester on Oct 10th, Swansea on the 17th and Newport on the 24th. They&rsquo;re coming to town near you, soon. <br />   <br /> So are the EDL racist, fascist or what? They&rsquo;ve made a big deal about how they have black members and in a recent photo opportunity (for the Daily Star), they publicly burned the swastika flag and brandished a sign defending &ldquo;Israel&rsquo;s right to exist&rdquo;. In that sense they&rsquo;re doing a lot to distance themselves from the lunatic far-right&rsquo;s obsession with the evil hand of the Jew being behind every disaster that befalls the white race. This has caused a certain amount of amusing kerfuffle on Stormfront, (our favourite race-hate website) with the white supremacist wi-fi warriors really not knowing whether to sieg heil or stay away. The EDL have even taken to brandishing the Israeli flag at their demos. <br />   <br /> But the EDL aren&rsquo;t the first organisation on the right to trade in the tired old horse of anti-semitism for the brash new nag of Islamophobia. The BNP took much the same route a few years ago. Back in 2004 Nick Griffin penned a column explicitly distancing himself from what had been the orthodoxy of his fellow-travellers on the far-right (only ten years ago Nick was as raving a holocaust-denying, anti-semite as one could hope to meet in a dark alley). The tabloid hysteria and public reaction following 9/11 and 7/7 created an environment in which campaigning against radical Islam was likely to be far more fruitful. Since then the BNP&rsquo;s focus has been on Muslims, with Nick Griffin referring to Islam as &lsquo;a cancer&rsquo;. The party has even had a Jewish candidate in Epping Forest. <br />   <br /> Some criticism of &ldquo;radical Islam&rdquo; is obviously entirely valid &ndash; and indeed there&rsquo;s probably a few hundred nutters in the country who would like to see the victory of a global caliphate and the introduction of sharia law, but they&rsquo;re hardly representative of the Muslim community as a whole. In fact they probably see the EDL as a brilliant recruiting tool. But strangely, the EDL haven&rsquo;t chosen to stand outside Finsbury Park mosque; instead they&rsquo;ve taken their cause to the centre of major cities with large Muslim populations and waited for it to kick off. <br />  <br /> Could this all be part of an overall strategy? Who benefits from images of radicalised Asian youth fighting with outnumbered white men? The perception that some kind of race-war is taking place on the UK&rsquo;s streets can only benefit the BNP. As long as the resistance to the EDL is perceived as coming entirely from Muslim youth then this becomes a persuasive narrative for many. A &lsquo;strategy of tension&rsquo; could pay dividends for white nationalists. The 2001 Oldham riots were sparked by National Front activists in Asian areas (See <a href='../archive/news313.htm'>SchNEWS 313</a>). As coverage of the EDL&nbsp;in the mainstream press didn&rsquo;t start until their Birmingham demos, not many people are aware of the ruckus in Luton back in May (See <a href='../archive/news670.htm'>SchNEWS 670</a>), when pre-cursors to the EDL overturned cars and smashed windows in Asian areas. That&rsquo;s why the youth are out in numbers to confront them. <br />  <br /> As the far-right seemed to break up into one main party (the BNP) seeking electoral success, and a bewildering variety of splinter groups (compared to the loony far-right the left is just one big happy family), many were left wondering &ndash; where are the footsoldiers? A crucial part of any fascist party&rsquo;s route to power lies through control of the streets or, as Nick Griffin once put it,&ldquo;<strong>well-directed fists and boots</strong>&rdquo;. A BNP that devotes itself exclusively to ballot box politics isn&rsquo;t likely to end up in government. On the other hand being publicly associated with a gang of boneheads probably isn&rsquo;t going get you invited on to Question Time. The BNP also publicly disassociated themselves from the instigators of the Oldham trouble, while privately congratulating them. <br />  <br /> Searchlight have uncovered numerous behind-the scenes links between the BNP and the EDL, with BNP organisers taking a leading role. That at least would explain the EDL&rsquo;s public hatred of the National Front, long the BNP&rsquo;s political rivals. Certainly a glance at the EDL&rsquo;s forums suggests that there is no chance of them forming their own political party. With Casuals United they are trying to recruit from the football terraces as though it&rsquo;s like the 70s when anti-IRA sentiment was the single issue that bound the feuding football tribes together. <br />  <br /> How much of a threat are the EDL? Potentially, given the anti-Islamic propaganda spewed out by the tabloid press, they could act as a potent recruiting tool for the far-right, sparking riots which lead to greater voter turnout for the BNP. But it&rsquo;s questionable how many of their footsoldiers are really ideologically committed to the project and, if met with a robust response now, they may disappear as quickly as they came. <br />  <br /> * See <a href="http://www.antifa.org.uk" target="_blank">www.antifa.org.uk</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=bnp&source=693">bnp</a>, <a href="../keywordSearch/?keyword=english+defence+league&source=693">english defence league</a>, <a href="../keywordSearch/?keyword=far-right&source=693">far-right</a>, <a href="../keywordSearch/?keyword=nick+griffin&source=693">nick griffin</a>, <a href="../keywordSearch/?keyword=racism&source=693">racism</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(511);
			
				if (SHOWMESSAGES)	{
				
						addMessage(511);
						echo getMessages(511);
						echo showAddMessage(511);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>