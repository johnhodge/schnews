<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 697 - 30th October 2009 - In A Hot Climate</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, climate change, coal, camp for climate action, australia" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://natowc.noflag.org.uk/"><img 
						src="../images_main/nato-pa-09-banner.png"
						alt="Smash NATO"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 697 Articles: </b>
<p><a href="../archive/news6971.php">A Liberal Helping?</a></p>

<p><a href="../archive/news6972.php">Coal: It's The Pits</a></p>

<p><a href="../archive/news6973.php">Kick In The Ghoulies</a></p>

<p><a href="../archive/news6974.php">Schnews In Brief</a></p>

<p><a href="../archive/news6975.php">Iraq And Back</a></p>

<p><a href="../archive/news6976.php">Calling Thyme</a></p>

<b>
<p><a href="../archive/news6977.php">In A Hot Climate</a></p>
</b>

<p><a href="../archive/news6978.php">An Outbreak Of The Trots</a></p>

<p><a href="../archive/news6979.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 30th October 2009 | Issue 697</b></p>

<p><a href="news697.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>IN A HOT CLIMATE</h3>

<p>
In Australia they are part-way through a series of four <strong>Climate Camps</strong> across the country &ndash; because, as our SchNEWS correspondent says - it saves them &ldquo;chugging across the country in dodgy hippy vans&rdquo;, and causing more er carbon emissions. <br />  <br /> As we covered in <a href='../archive/news691.htm'>SchNEWS 691</a>, the first was held September 12th-13th in Victoria, when over 500 people went to the Hazelwood coal fired power station in the Latrobe Valley, calling for an end to coal power and a switch to renewables. Despite a heavy police presence, 22 people scaled the fence to serve a Community Decommission Order on the power station. <br />  <br /> Next up was South Australia, where from September 24th-27th a Climate Camp targeted a coal train running to Port Augusta. 50 people camped for four days and held a demo outside the Port Augusta coal fired power station, presenting a Community Decommission Order and 350 native flowers at the gate. Police had agreed that the coal train wouldn't run all weekend on safety grounds, but later &ndash; making a mockery of attempts to negotiate with police - it turns out that the train had run, at a different time, because apparently it had accidentally derailed. <br />  <br /> NSW climate campers experienced some extreme climatic conditions, including gale force winds and heavy rain during their camp October 9th-11th. 200 people camped near the Metropolitan Colliery in Helensburgh for three days of workshops, campaign planning and direct action. The camp included a sovereignty tent, where Traditional Owners from around the country gathered to share stories of resistance. The camp was also visited by members of an Aboriginal community in the Northern Territory who have recently walked off their community in protest against the continuing racist 'intervention' in indigenous communities (for more info on that, check out <a href="http://interventionwalkoff.wordpress.com" target="_blank">http://interventionwalkoff.wordpress.com</a>). Early in the morning of Sunday 11th October, four people entered the Dendrobium coal mine and locked on to a conveyor belt. Later that day 500 people attended a big rally through town, finishing at the Colliery where eight more were arrested after jumping the fence. Despite threatening drinking water supplies, the mine is set for expansion, so no doubt we'll be hearing more from local campaigners down there. <br />  <br /> And even though Queenslanders didn't have a camp, on October 22nd, 20 people went out in kayaks on the Brisbane River attempting to stop a ship loaded with 90,000 tonnes of coal from leaving the city's port. See <a href="http://www.sixdegrees.org.au" target="_blank">www.sixdegrees.org.au</a> <br />  <br /> As the Copenhagen talks come to an end in December, Western Australians will camp in the state's coal capital, Collie.  <br />  <br /> For more see <a href="http://www.climatecamp.org.au" target="_blank">www.climatecamp.org.au</a> <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=australia&source=697">australia</a>, <a href="../keywordSearch/?keyword=camp+for+climate+action&source=697">camp for climate action</a>, <a href="../keywordSearch/?keyword=climate+change&source=697">climate change</a>, <a href="../keywordSearch/?keyword=coal&source=697">coal</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(548);
			
				if (SHOWMESSAGES)	{
				
						addMessage(548);
						echo getMessages(548);
						echo showAddMessage(548);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>