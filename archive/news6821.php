<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 682 - 3rd July 2009 - Rumble In The Jungle</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, immigration, no borders, calais" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 682 Articles: </b>
<b>
<p><a href="../archive/news6821.php">Rumble In The Jungle</a></p>
</b>

<p><a href="../archive/news6822.php">Coup Blimey</a></p>

<p><a href="../archive/news6823.php">Don&#8217;t Coal Home  </a></p>

<p><a href="../archive/news6824.php">A Lot To Ansar For</a></p>

<p><a href="../archive/news6825.php">Island Mentality  </a></p>

<p><a href="../archive/news6826.php">Sez Who?</a></p>

<p><a href="../archive/news6827.php">Ich Bin Ein Burnin</a></p>

<p><a href="../archive/news6828.php">Lions And Tigers</a></p>

<p><a href="../archive/news6829.php">Squats Up: South West London Squat Round Up</a></p>

<p><a href="../archive/news68210.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/682-jackson-demerol-lg.jpg" target="_blank">
													<img src="../images/682-jackson-demerol-sm.jpg" alt="Don't blame it on the vicodin" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 3rd July 2009 | Issue 682</b></p>

<p><a href="news682.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>RUMBLE IN THE JUNGLE</h3>

<p>
<strong>REPORT BACK FROM THE CALAIS NO BORDERS CAMP</strong> <br />  <br /> No Borders campaigners, calling for freedom of movement for all people and an end to migration controls, culminated their week long camp in Calais (See <a href='../archive/news681.htm'>SchNEWS 681</a>) with a demonstration outside the city&rsquo;s main port last Saturday. Over 2,000 people took to the streets against the increasingly tight border controls at the channel crossing, the bottleneck of Fortress Europe where many undocumented migrants - or &lsquo;sans papier&rsquo;, people without papers &ndash; risk their life for months, sometimes years, just to move from France to the UK.&nbsp; <br />  <br /> The demo was subject to severe police controls before it even began. Hundreds of demonstrators were blockaded by police around every corner, with a drafted force of 2,500 - including CRS riot police - preventing people from reaching the starting point of the march. Despite police repression, and after hours of cat and mouse, protesters from the camp joined other groups including trade unions, humanitarian organisations and left leaning political parties, quadrupling the number of people on the march. But with the town centre barricaded out of bound by riot vans, the demo was forced into the outskirts and around the dock, out of the public eye.&nbsp; <br />  <br /> The No Borders camp, from 23rd - 28th of June, was set up alongside the main motorway leading to the harbour and near a junction where many migrants try to jump on passing trucks heading to Britain. During the week practical workshops ranging from first aid to direct action, and meetings with campaigning groups from across Europe, focused on organising transnational action against closed borders and draconian immigration policies. The camp was made up of 500-800 activists mainly from France, UK and Belgium, along with around 100-200 migrants stranded at the border.&nbsp; <br />  <br /> &nbsp;At any one time there are 1,000 &ndash; 2,000 refugees in Calais alone, mainly young men and children from the Middle East and Horn of Africa who squat in town buildings or make-shift settlements in &lsquo;the jungle&rsquo;, a wooded area surrounding the port. Many have travelled for years, forced to leave their homes to escape war, poverty and abuses and see the UK as the end point of their journey. People go to the UK for many reasons, not least because of colonial ties. A large proportion of migrants in Calais are from Iraq and Afghanistan; many worked with British forces during the occupation and now fear persecution as collaborators. <br />  <br /> <strong>FRONTIER SANS MEDICIN</strong> <br />  <br /> In 2002, the French and UK authorities forcibly closed a Red-Cross run refuge centre in Calais, the Sangette, claiming it creates &lsquo;incentives for immigrants&rsquo;. Currently, a couple of voluntary organisations, through tacit agreement with local authorities, provide food handouts five days a week. Ministers have shot down ideas of re-opening the Sangette or establishing a new refugee centre. However, the UNHCR &ndash; the UN refugee agency - announced last Tuesday (1st July) that they will be establishing a full-time presence in Calais. While recognizing the &lsquo;squalid&rsquo; conditions people are living in, the UNCHR is limited to only providing information and advice &lsquo;to help the migrants and asylum seekers to make an informed decision&rsquo;.&nbsp; <br />  <br /> Migrants face constant harassment by police who raid and destroy their camps, tear gas whole sections of the jungle and regularly arrest and detain people, taking their fingerprints before releasing them, only to repeat the whole process over again. Migrants have no access to health care or legal representation. It is illegal to assist migrants in any way; as a result of article L622-1 of the French penal code anyone &ldquo;aiding or facilitating either directly or indirectly the arrival, circulation or residence of illegal immigrants in France&rdquo; is a punishable with up to five years in prison and a &pound;25,000 fine. There is a high suicide rate amongst the migrants, who are forced to live in inhumane conditions, risk their lives every night trying to climb into, or under, trucks and face abuse from truck drivers and police.&nbsp; <br />  <br /> By the end of this year, in collaboration with the UK, France plans to evict all migrants from Calais by clearing out the jungle and building a new detention centre to supplement the existing Croquelles CRE, enabling mass deportations, mainly to Afghanistan and Iraq. French immigration minister, Eric Besson said, &ldquo;<em>We are going to make the zone around Calais watertight to illegal immigration</em>&rdquo;. <br />  <br /> Currently French courts are refusing to send illegal immigrants back to countries where they may be persecuted so, through technical manoeuvring, the new detention centre will be a hybrid of Franco-British powers, located on a carved out &lsquo;British control zone&rsquo; in Calais. This will allow them to pick-and-mix legal loopholes, manipulate ambiguous legal grey areas and cut through red tape, allowing migrants to be deported more easily under UK immigration law. <br />  <br /> The system will effectively create an &lsquo;off-shore, on-shore&rsquo; detention centre that exploits legal systems and evades European and international law on immigration and asylum in order to fast-track people out of Europe, no questions asked. <br />  <br /> Earlier this year, when discussing proposals to externalise the UK&rsquo;s powers and control migration from beyond its borders, Phil Woolas, British immigration minister, said he wants to &lsquo;<em>send a message&hellip; back to Afghanistan and Iraq that Britain is not the Promised Land</em>&rsquo;. <br />  <br /> * See <a href="http://www.noborders.org.uk" target="_blank">www.noborders.org.uk</a> <br />  <br /> <em><font face="courier new"><font size="4">"Free immigration prisoners, no one is illegal"</font></font></em> <br />  <br /> <strong>No Borders supports freedom of movement and as first steps, demands:&nbsp;</strong> <br />  <br /> 1) Unconditional entry into the UK for all <br />  <br /> 2) The places where migrants are living must not be raided or destroyed, and access to healthcare must be guaranteed <br />  <br /> 3) Freedom of movement for all around Calais: the ability to travel to all parts without restriction, harrassment or fear of arrest <br />  <br /> 4) No repeat arrests&nbsp; <br />  <br /> 5) Freedom of expression for all, including migrants: the right to protest and the right to make complaints to the authorities, individually or collectively <br />  <br /> 6) No deportations (whether by charter flight or not) <br />  <br /> 7) End repression of associations/individuals who support migrants, including by transporting them <br />  <br /> 8) Free and impartial legal advice on UK and other asylum and immigration systems <br />  <br /> 9) Britain&rsquo;s policy of arbitrary immigration detention without time limit must not be exported to Calais. There must be no new detention centre and especially no Guantanamo style facility <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=calais&source=682">calais</a>, <a href="../keywordSearch/?keyword=immigration&source=682">immigration</a>, <a href="../keywordSearch/?keyword=no+borders&source=682">no borders</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(383);
			
				if (SHOWMESSAGES)	{
				
						addMessage(383);
						echo getMessages(383);
						echo showAddMessage(383);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>