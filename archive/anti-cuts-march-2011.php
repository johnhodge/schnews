<?php
	include "../storyAdmin/functions.php";
	include "../functions/comments.php";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS 764 - 25th March 2011 - As SchNEWS Looks Into The West's Selective Support Of The Arab Rebellions</title>
<meta name="description" content="SchNEWS looks into the West's selective support of the Arab rebellions, Far-right EDL stagger around Reading, Get ready for a day of mass anti-cuts protests in London on Saturday, and more..." />
<meta name="keywords" content="SchNEWS, direct action, Brighton, middle east, libya, bahrain, yemen, arab spring, saudi arabia, edl, anti-fascists, ena, cuts, protest, uk uncut, london, students, universities, occupation, squatting, tory, squatting, sheffield, social centre, rossport, shell, climate change, environmentalism, shell to sea, ireland, direct action, cuts, forests, environment" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../issue.css" type="text/css" />



<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />

<script language="JavaScript" type="text/javascript" src='../javascript/jquery.js'></script>
<script language="JavaScript" type="text/javascript" src='../javascript/issue.js'></script>

<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>
</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>
			<div class="bannerGraphic">
				<a href="http://london.indymedia.org/events/7176" target="_blank"><img
						src="../images_main/763-banner-march-26th.png"
						alt="Mass Demonstration Against The Cuts - London March 26th 2011"
						border="0"
				/></a>
			</div>



</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">
<!--#include virtual="../inc/navBar.php"-->
		


</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

					<!-- RSS LINK -->
			<div id="rss">
				<p><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A></p>
			</div>

			<!-- BACK ISSUES -->
			<P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P>



<div id="backIssues">

<div id="backIssue">
<p><b><a href="../archive/news763.htm">SchNEWS 763, 18th March 2011</a></b><br />
<b>Nuclear Power: Going Critical</b> -
	The terrifying situation in Japan has rekindled the debate over nuclear power around the world. While in the US, Obama has reaffirmed his support for nuclear power to protect the massive investment his administration has made in the industry, over in Germany and Switzerland the governments have jammed the brakes on plans to build and replace nuclear plants. In the UK, David Cameron has already declared his intention to push on with plans to expand the UKs nuclear capacity, with up to eleven new power stations.
	&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news762.htm">SchNEWS 762, 11th March 2011</a></b><br />
<b>Profit Hungry</b> -
	If you&rsquo;re the type to buy food, rather than only get it from bins, you&rsquo;ve probably noticed that food prices have been rising again. In fact, the international trading prices of major grains are 70% higher than they were this time last year, and in most cases near or above the levels during the price hikes of 2008. Demonstrations have already taken place this year in Tunisia, Jordan, Algeria and India. Should the problems continue many more can be expected.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news761.htm">SchNEWS 761, 4th March 2011</a></b><br />
<b>Penny For The Guy Ropes</b> -
	They came, they camped, they conquered (well, not quite). During a soul-searching Dorset retreat, Climate Camp have decided to suspend tent-centred activism - citing the &ldquo;radically different political landscape&rdquo; of 2011. Having been through Drax, Kingsnorth, Heathrow, RBS, Copenhagen and one helluva lot of hummus, the group are now turning their attention to coordination with the wider anti-cuts and anti-austerity movement.
	&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news760.htm">SchNEWS 760, 25th February 2011</a></b><br />
<b>Cameron Waves Arms About</b> -
	As Egypt and its martial law swiftly falls down the mainstream rolling news agenda it has been replaced by even more dramatic events in Libya. While the Libyan people continue their life and death struggle against brutal dictator/deflated blow-up doll Ghaddafi, all these tottering dictatorships are causing a few awkward moments for the government and UK plc.&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news759.htm">SchNEWS 759, 18th February 2011</a></b><br />
<b>Middle Eastern Promise</b> -
	Egyptian dissidents (along with the masses) celebrated on Friday as Hosni Mubarak finally threw in the towel after the mass protests in Cairo&rsquo;s Tahrir Square and across the country refused to abate (and presumably the Americans finally ordered him to load up the plane with gold and go in an attempt to ensure the power structures &ndash; and thus their influence &ndash; didn&rsquo;t collapse completely). But the Egyptian revolution is not yet won as the military have stepped in, repressed protest and threatened to declare martial law.&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news758.htm">SchNEWS 758, 11th February 2011</a></b><br />
<b>The Withdrawal Method</b> -
	As noisy protests continued against tax avoidance by big business and cuts in education and benefits another, altogether quieter, national campaign took off this week at our most unsung of public services: the libraries. You can&rsquo;t imagine that Waterstones, WHSmiths, Amazon and others mind too much that libraries were forged from great social ideals. Not only is encouraging universal education and literacy good econincally for society, it&rsquo;s availability to serve as community hub, public space, creche and more to those without access to alternatives make it an all round force for social good.
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news757.htm">SchNEWS 757, 4th February 2011</a></b><br />
<b>Cut To The Quick...</b> -
	Thousands of protesters ran the police ragged in a hyperactive day of protest at the latest national demonstration against fees and cuts in London on Saturday (29th). Up to 10,000 people marched through the city towards Parliament Square, taking the route agreed with police. However, the crowd showed little interest in hanging around for speeches at the designated end point and most pushed on towards Milbank Towers &ndash; scene of the birth of the student protest movement in November
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news756.htm">SchNEWS 756, 28th January 2011</a></b><br />
<b>Mubarak's Against the Wall</b> -
	Since last December Tunisia has been hit by relentless and transformative riots triggered by unemployment, food inflation, lack of freedom of speech and poor living conditions. The violent unrest eventually led to the ousting of President Zine El Abidine Ben Ali, who fled the country on the 14th of January after a hefty 23 years in power. Daily protests have continued due to prominent figures in the Ben Ali regime clinging on to posts in the new interim government.
	&nbsp;
</p>
</div>

<div id="backIssue">
<p><b><a href="../archive/news755.htm">SchNEWS 755, 21st January 2011</a></b><br />
<b>Inter-NETCU</b> -
	For the benefit of anyone who&rsquo;s been hiding in a hole wearing a tinfoil hat for the last fortnight (i.e most of our readership), it turns out that the U.K direct action/anarchist/environmental movement was infiltrated for number of years by undercover police. At least four cops have already been outed and its safe to assume there may be more. But while the mainstream media has focussed on the sleazy antics and dodgy love lives of these professional liars, SchNEWS can reveal that police attempts to disrupt our movement goes much further than a few unshaven plants in grubby t-shirts, and includes attacks on activist media and communications.
</p>
</div>


</div>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<div id="issue">

	<p><b><div style='width: 40%; padding: 10px 0px 10px 0px; border-bottom: 1px solid #bbbbbb; border-top: 1px solid #bbbbbb; margin: 10px 0px 10px 0px'>Sunday 27th March 2011</div></b></p>

	<h1>PUTTING IN THE RITZ</h1>

	<h3><i>SchNEWS emergency bulletin on the M26 demos in London</i></h3>

	<div>
	<p><b><i>"HUNDREDS of anti-royal anarchists ran riot across central London yesterday, turning a peaceful demonstration about spending cuts into a class war"</i> - Sunday Express</b></p>
<p>
Well - somebody had to do it. - The TUC's glad-hand fest was never going to change the ConDEMS course by even one degree. It was hijacked by anarchists and it deserved to be. The TUC mobilised its masses and did everything they could to make sure that genuine dissent didn't rear its ugly head. The emphasis was to be on a family-friendly day out without frightening the horses. Quite who they think is going to hand out prizes for niceness in the shark tank that is global capitalism remains a mystery.
<p>
For those who went to the endpoint rally in Hyde Park, even moderate voices like Bob Crow of the RMT and Caroline Lucas MP of the Green Party were not allowed to speak. They were left out in favour of Ed Millaround the  gormless puppet who's now trying to pass off the Labour Party as a revitalised radical alternative to the Tories - less than eleven months after they were run out of office. Is this then the TUCs strategy? Wait four years,  and then vote in a business friendly Labour government - and they wonder why union membership is at an all time low.
<p>
While dressing himself up as the re-incarnation of Emily Pankhurst and Martin Luther King, and the bearer of Mandela's torch, Milliband junior was emphatic about yesterday's disorder, condemning it roundly while ignoring the fact that all the struggles he was paying homage to involved breaking more than a few windows.
<p>
Earlier that morning, the feeder marches, all condemned as unauthorised by the TUC made their way largely unhindered to greet the masses gathered at Embankment. A half-hearted attempt by cops to put a line across Westminster bridge in front of the Radical Workers block that had left from Kennington Park was shrugged aside.
<p>
The anarchist block split from the main march past Embankment Station, as a voice over a megaphone beckoned marchers to come with them for a sight-seeing tour around London. The break off bloc poured past Trafalgar Square, past the front of the TUC demo where hundreds of disenchanted activists decided to sack off the A to B marching exercise and join the black bloc. By the time the bloc had made its way through Covent Garden to Oxford Street it numbered several thousand.
<p>
Trudging along with the masses, other anarchists filtered through the crowd to UK Uncut's pre-arranged meet up on Oxford St at 2pm. UK Uncut had called for occupations of corporate tax dodgers. The police tactic for the day was clearly the protection of known UK Uncut targets, with lines of police in front of branches of Boots, Vodaphone and Topshop. The cops even seemed willing to protect Topshop at the expense of more traditional anti-capitalist targets, with unfortified McDonalds and Starbucks getting done over anyway, showing that this was far from a single issue demo.
<p>
And that was where the fun really started - Topshop, one of UK Uncuts main targets was the first to get it and then, for over hour and a half a rag-tag black block, numbering around a thousand, ran cops ragged around Soho and Piccadilly Circus. Selected corporate targets were attacked with smoke flares, paint and street furniture. Banks bore the brunt of the anger with branches of HSBC, Santander and Lloyds getting the paint and broken glass treatment. The  HSBC in Cambridge Circus got given some special attention. As 'Smash the banks' was daubed across the window, the crowd began to move on, until, bearing in mind the complete absence of Old Bill, people decided to take the advice. Windows were smashed and a green wheelie bin was used to batter open the doors. The inside of the bank was turned over before activists did a runner as the plod turned up. Watch the video <a href='http://www.thesun.co.uk/sol/homepage/news/3493079/Riot-cops-arrest-201-in-cuts-demo.html?OTC-RSS&ATTR=News' target='_BLANK'>here</a>. The F.I.T team were chased off in Cambridge Circus. Even London's Boris bikes scheme wasn't ignored, with activists peeling off the Barclay's stickers while leaving the machines intact. It might have been a touch violent but it wasn't mindless.
<p>
At the Ann Summers shop, militant queer action emerged from the crowd, spray-painting Ann Summers shiny shop front of apparently taking a stand against "mainstream heteronormatively constructed sexuality", and finally putting a bin right through the window.
<p>
Police coming into the area were harassed, with street signs and wheelie bins hurled under vehicles. Riot vans were liberally re-decorated. Some incredibly loud bangers or thunderflashes caused cops to flinch. Plumes of coloured smoke could be seen. Often groups of cops were rushing to protect targets that had been left by the fluid and swift-moving crowd. Uncontained, the block, waving red and black flags, moved along Piccadilly towards the Ritz. The chant went up - "The Ritz, the Ritz - we're gonna get rid of the Ritz" as tables smashed through the windows.
<p>
The block then re-united with the main march, giving many a chance to de-mask and slip away. Nearing Hyde Park, no doubt fearing an outburst of sentimental centre left rhetoric, they split off back into Mayfair. Symbols of wealth, including a Porsche dealership and several overpriced sports car were done, along with a couple of shops selling overwhelmingly priced status antiques. Every corner turned in Mayfair showed just how much grotesque wealth is still out there waiting to be redistributed.
<p>
Meanwhile at 3.30pm on Oxford Circus , UK Uncuts pre-arranged meet up was underway. After converging  demonstrators were slipped a card which told them which  colour flag to follow, with the aim of separating into branches then joining forces for the occupation of the target.
<p>
The target - Fortnum&Mason, was well chosen, grocers to the Queen and corporate tax dodgers. Unfortunately the sheer size of the protests meant the different crowds didn't arrive simultaneously and there was a stand-off with protestors inside the building and police lines forming outside. A push and shove ensued but due to the pavement barriers not enough of the crowd could get behind the push and the cops remained unbudged, blockading the entrances.  The two to three hundred protesters who were already inside, were left to soak up the atmosphere, and eyeing up bargains like the twenty quid jam and �65 napkin rings. Others took a more direct route and climbed up a street lamp onto the first floor.
<p>
Unfortunately here, as during the rest of the day, there seemed to be more people filming than actually getting involved in any action. Anyone who wanted to get stuck in had to push their way through a line of happy snappers first. Where does all this footage end up? - Think before you get yer zoom lens out kids, 'cos careless shots cost lives.
<p>
The rest of the day was a mix of stand-offs and running skirmishes. The Peace horse was torched outside Oxford St tube.  By this time the block was being pushed back onto Piccadilly circus as, freed of the obligation to police the TUC demo, that was safely in Hyde Park, more cops began arriving. Sealing off Fortnum and Masons they made numerous arrests.
<p>
A number of the crowd gathered in Trafalgar Sq, where some had suggested a 24 hr occupation. Described to SchNEWS by one eyewitness as "just a party, really", the cops seized their opportunity to wade in. A kettle was put in place and despite a fight being put up the crowd was dispersed or arrested by 1 a.m.
<p>
According to Green&Black Cross legal support there have been over 200 arrests - and that's just those who've contacted the help-line. Most are being bailed now (Sunday evening) many having had their clothes and phones seized. Interestingly, they are receiving bail conditions to stay away from central London on the day of the next TUC mobilisation 1st of May and on the 29th April, the date of Kate&Wills happy nuptials.
<p>
If you were arrested then there's a Defendants' meeting - Saturday 2nd April - 2pm - University of London Student Union, Malet Street, WC1E 7HY
<p>
Otherwise - See you on the streets!

<div style='border-bottom: 1px solid black; margin: 8px; padding: 8px; padding-top: 0px; margin-top: 0px; padding-bottom:25px; margin-bottom: 25px'>
	&nbsp;
</div>

<p>
Anti-cuts protestors take over London- <a href='http://www.indymedia.org.uk/en/2011/03/476754.html' target='_BLANK'>http://www.indymedia.org.uk/en/2011/03/476754.html</a>
<p>
200 arrested as hardcore anarchists fight police long into night in Battle of Trafalgar Square after 500,000 march against the cut- <a href='http://www.dailymail.co.uk/news/article-1370053/TUC-anti-spending-cuts-protest-200-arrested-500k-march-cut.html#comments' target='_BLANK'>http://www.dailymail.co.uk/news/article-1370053/TUC-anti-spending-cuts-protest-200-arrested-500k-march-cut.html#comments</a>
<p>
Anti-cuts march: more than 200 in custody after violence- <a href='http://www.guardian.co.uk/society/2011/mar/27/cuts-march-200-custody-violence' target='_BLANK'>http://www.guardian.co.uk/society/2011/mar/27/cuts-march-200-custody-violence</a>
<p>
Stop the cuts: Marchers turn on troublemakers- <a href='http://www.mirror.co.uk/news/top-stories/2011/03/27/anarchists-try-to-hijack-london-march-115875-23018699/' target='_BLANK'>http://www.mirror.co.uk/news/top-stories/2011/03/27/anarchists-try-to-hijack-london-march-115875-23018699/</a>
<p>
Cuts protest violence sparks new fears for Royal wedding - <a href='http://www.dailyexpress.co.uk/posts/view/237001' target='_BLANK'>http://www.dailyexpress.co.uk/posts/view/237001</a>
<p>
Riot cops arrest 201 in cuts demo- <a href='http://www.thesun.co.uk/sol/homepage/news/3493079/Riot-cops-arrest-201-in-cuts-demo.html?OTC-RSS&ATTR=News' target='_BLANK'>http://www.thesun.co.uk/sol/homepage/news/3493079/Riot-cops-arrest-201-in-cuts-demo.html?OTC-RSS&ATTR=NewsM</a>
<p>
Up to 500,000 protestors attend anti-cuts demo- <a href='http://www.independent.co.uk/news/uk/home-news/up-to-500000-protestors-attend-anticuts-demo-2253791.html' target='_BLANK'>http://www.independent.co.uk/news/uk/home-news/up-to-500000-protestors-attend-anticuts-demo-2253791.html</a>
<p>
A peaceful protest. The inevitable aftermath- <a href='http://www.independent.co.uk/news/uk/politics/a-peaceful-protest-the-inevitable-aftermath-2254167.html' target='_BLANK'>http://www.independent.co.uk/news/uk/politics/a-peaceful-protest-the-inevitable-aftermath-2254167.html</a>

<div style='border-bottom: 1px solid black; margin: 8px; padding: 8px; padding-top: 0px; margin-top: 0px; padding-bottom:25px; margin-bottom: 25px'>
	&nbsp;
</div>

<p><p>
<h3>Riot Porn</h3>

<p>
<a href='http://www.flickr.com/photos/33387220@N03/sets/72157626360253700/with/5563034860/' target='_BLANK'>http://www.flickr.com/photos/33387220@N03/sets/72157626360253700/with/5563034860/</a>
<p>
<a href='http://www.flickr.com/photos/kenchie/sets/72157626233272581/with/5561520511/' target='_BLANK'>http://www.flickr.com/photos/kenchie/sets/72157626233272581/with/5561520511/</a>
<p>
<a href='http://www.flickr.com/photos/glaidlaw/sets/72157626235042775/with/5562331109/' target='_BLANK'>http://www.flickr.com/photos/glaidlaw/sets/72157626235042775/with/5562331109/</a>
<p>
<a href='http://www.flickr.com/photos/semisara/sets/72157626358108452/with/5561468671/' target='_BLANK'>http://www.flickr.com/photos/semisara/sets/72157626358108452/with/5561468671/</a>
<p>
<a href='http://www.flickr.com/photos/alaanphotos/sets/72157626238301265/with/5564074436/' target='_BLANK'>http://www.flickr.com/photos/alaanphotos/sets/72157626238301265/with/5564074436/</a>
<p>
<a href='http://www.flickr.com/photos/33387220@N03/sets/72157626360253700/with/5563034860/' target='_BLANK'>http://www.flickr.com/photos/33387220@N03/sets/72157626360253700/with/5563034860/</a>
<p>
<a href='http://www.bbc.co.uk/news/uk-12871187' target='_BLANK'>http://www.bbc.co.uk/news/uk-12871187</a>
<p>
<a href='http://news.sky.com/skynews/Home/UK-News/Spending-Cuts-Workers-To-Stage-A-March-for-the-Alternative-Protest-Against-Government---UK-Uncut/Article/201103415960442?f=rss' target='_BLANK'>http://news.sky.com/skynews/Home/UK-News/Spending-Cuts-Workers-To-Stage-A-March-for-the-Alternative-Protest-Against-Government---UK-Uncut/Article/201103415960442?f=rss</a>

	</div>

	<div style='border-bottom: 1px solid black'>&nbsp;</div>

</div>



			<H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../schmovies/index.php">SchMOVIES</A></B>
			</FONT></H3>

			<P><B><A href="../schmovies/index.php" TARGET="_blank">RAIDERS OF THE LOST ARCHIVE</A></B>
			-<B> Part one of the SchMOVIES collection 2009-2010 </B>
			- This DVD features a number of films which were held by Sussex police for over a year following the raid and confiscation of all SchMOVIES equipment during an intelligence gathering operation in June 2009 related to the <a href='http://www.smashedo.org.uk/' target='_BLANK'>Smash EDO</a> campaign.</p>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#rftv" TARGET="_blank">REPORTS FROM THE VERGE</A></B>
			-<B> Smash EDO/ITT Anthology 2005-2009 </B> - A new collection of twelve SchMOVIES covering the Smash EDO/ITT's campaign efforts to shut down the Brighton based bomb factory since the company sought its draconian injunction against protesters in 2005.</P>

			<P><B><A href="../pages_merchandise/merchandise_video.php#uncertified" TARGET="_blank">UNCERTIFIED </A></B> -<B> OUT NOW on DVD- SchMOVIES DVD Collection 2008</B> - Films on this DVD include... The saga of On The verge &ndash; the film they tried to ban, the Newhaven anti-incinerator campaign, Forgive us our trespasses - as squatters take over an abandoned Brighton church, Titnore Woods update, protests against BNP festival and more... To view some of these films <A HREF="../schmovies/index.php">click here</a></P>

			<P><B><A HREF="../pages_merchandise/merchandise_video.php#verge" TARGET="_blank">ON
			THE VERGE</A></B> -<B> The Smash EDO Campaign Film</B> - is out on DVD. The film
			police tried to ban - the account of the four year campaign to close down a weapons
			parts manufacturer in Brighton, EDO-MBM. 90 minutes, &pound;6 including p&amp;p
			(profits to Smash EDO)</P>

			<P><STRONG><A HREF="../pages_merchandise/merchandise_video.php#take3" TARGET="_blank">TAKE
			THREE</A> - SchMOVIES Collection DVD 2007 </STRONG>featuring thirteen short direct
			action films produced by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash
			EDO, Naked Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.
			&pound;6 including p&amp;p.</P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_video.php#v" TARGET="_blank">V
			For Video Activist </A>- the</B> <B>SchMOVIES 2006 DVD Collection </B>- twelve
			short films produced by SchMOVIES in 2006. only &pound;6 including p&amp;p.</FONT></P><P><B><A HREF="../pages_merchandise/merchandise_video.php#2005">SchMOVIES
			DVD Collection 2005</A></B> - all the best films produced by SchMOVIES in 2005.
			Running out of copies but still available for &pound;6 including p&amp;p.</P><H3><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php" TARGET="_blank">SchNEWS
			Books</A></B> </FONT> </H3><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#schnewsat10" TARGET="_blank">SchNEWS
			At Ten</A> - A Decade of Party &amp; Protest </B>- 300 pages, <B>&pound;5 inc
			p&amp;p</B> (within UK) </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#PDR" TARGET="_blank">Peace
			de Resistance</A> </B>- issues 351-401, 300 pages, &pound;5 inc p&amp;p</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#sotw" TARGET="_blank">SchNEWS
			of the World</A> </B>- issues 300 - 250, 300 pages,&pound;4 inc p&amp;p. </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_one" TARGET="_blank">SchNEWS
			and SQUALL&#146;s YEARBOOK 2001</A> </B>- SchNEWS and Squall back to back again
			- issues 251-300, 300 pages, &pound;4 inc p&amp;p.</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_two" TARGET="_blank">SchQUALL</A></B>
			- SchNEWS and Squall back to back - issues 201-250 -<B> Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_three" TARGET="_blank">SchNEWS
			Survival Guide</A></B> - issues 151-200 <B>- Sold out - Sorry</B></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><B><A HREF="../pages_merchandise/merchandise_books.php#book_four" TARGET="_blank">SchNEWS
			Annual</A> </B>- issues 101-150<B> - Sold out - Sorry</B></FONT></P>
			
			<p><strong><FONT FACE="Arial, Helvetica, sans-serif">(US Postage &pound;6.00 for individual books, &pound;13 for above offer).</FONT></strong></p>
			
			
			<P>These books are mostly collections of 50 issues of SchNEWS from each year,
			containing an extra 200-odd pages of extra articles, photos, cartoons, subverts,
			a &#147;yellow pages&#148; list of contacts, comedy etc. SchNEWS At Ten is a ten-year
			round-up, containing a lot of new articles. </P>
			
			<P><FONT FACE="Arial, Helvetica, sans-serif"><B>Subscribe
			to SchNEWS:</B> Send 1st Class stamps (e.g. 10 for next 9 issues) or donations
			(payable to Justice?). Or &pound;15 for a year's subscription, or the SchNEWS
			supporter's rate, &pound;1 a week. Ask for &quot;originals&quot; if you plan to
			copy and distribute. SchNEWS is post-free to prisoners. </FONT></P><p></p><img align="center" src="../images_main/spacer_black.gif" width="100%" height="10" />


</div>




<div class='mainBar_right'>
<!--
	<div style='padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','../images_main/features-button-mo-225.png',1)">
		<a href='../features/' style='border: none'>
			<img name='featuresTitle' src='../images_main/features-button-225.png' width="225px" width="55px" style='border:none; padding-left: 15px' />
		</a>
	</div>

	<?php
			echo get_features_for_homepage(20, false, '../features/');
	?>
-->
</div>






<div class="footer">

		<!--  Include for Footer -->

		<p class="small">
				SchNEWS, c/o Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> 
				
				Press/Emergency Contacts: +44 (0) 7947 507866<br />
				
				Phone: +44 (0)1273 685913<br />
				
				Email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>
		</p>
		<p class="small"><font size="-2" face="Arial, Helvetica, sans-serif">
				@nti copyright - information for action - copy and distribute!
		</font></p>

</div>








</div>




</body>
</html>

