<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 682 - 3rd July 2009 - Sez Who?</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, bengal, adivasis, special economic zones, anti-corporate, indigenous struggles, india" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="../pages_merchandise/merchandise_video.php#uncertified"><img 
						src="../images_main/uncertified-banner.jpg"
						alt="Uncertified - SchMOVIES DVD Collection 2008"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 682 Articles: </b>
<p><a href="../archive/news6821.php">Rumble In The Jungle</a></p>

<p><a href="../archive/news6822.php">Coup Blimey</a></p>

<p><a href="../archive/news6823.php">Don&#8217;t Coal Home  </a></p>

<p><a href="../archive/news6824.php">A Lot To Ansar For</a></p>

<p><a href="../archive/news6825.php">Island Mentality  </a></p>

<b>
<p><a href="../archive/news6826.php">Sez Who?</a></p>
</b>

<p><a href="../archive/news6827.php">Ich Bin Ein Burnin</a></p>

<p><a href="../archive/news6828.php">Lions And Tigers</a></p>

<p><a href="../archive/news6829.php">Squats Up: South West London Squat Round Up</a></p>

<p><a href="../archive/news68210.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 3rd July 2009 | Issue 682</b></p>

<p><a href="news682.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>SEZ WHO?</h3>

<p>
In mid-June the Indian government launched a massive operation in an adivasi (indigenous) region of West Bengal, where locals had been protesting against the state facilitated corporate land-grab for one of the notorious Special Economic Zones (SEZ).&nbsp; <br />  <br /> Since the mid-nineties the Indian government has seized thousands of acres of land, uprooting adivasis, dalits, small farmers and landless farm workers - affecting around 250 million. The land was handed over to multinationals &ndash; Indian and foreign &ndash; in the name of economic liberalisation. SEZs were created where industries are exempted from labour and environmental laws, granted complete tax exemption and are constitutionally to be treated as foreign territories on Indian soil, fully equipped with special courts to serve the purposes of the corporations.&nbsp; <br />  <br /> There was already local anger at torture and arbitrary arrests at the hands of the Lalgarh police when the state government of West Bengal seized 5000 acres of land for an SEZ to be handed over to Indian multinational steel company Jindal Steel. Last November, a convoy carrying the chief minister of West Bengal was targeted by a land mine on its way back from laying the foundation stone of the steel plant, injuring six policemen. The attack was claimed by India&rsquo;s Maoists (CPIM) who had been active in cooperating with the adivasis until being recently driven out by federal forces. The police responded by beating and arresting locals, leading them to effectively seize control of the rural region. Road blockades were formed from felled trees and trenches, and locals stopped selling the police food, forcing them to withdraw. <br />  <br /> The adivasis have distanced themselves from the Maoists, asserting their peaceful credentials whilst the Maoists have never claimed the adivasi movement to be under their control. The Maoists were recently declared a terrorist organisation. <br />  <br /> As the press and independent monitors have been banned from the area, the extent of the abuses suffered by the adivasis at the hands of the military is unclear, although there have been reports of mass detentions and the displacement of entire villages. <br />  <br /> The repression of resistance to land grabs for SEZs is commonplace in West Bengal. In protests against forced displacements in early 2007, 25 locals were killed by state police and over 20 women raped. An Amnesty report in January 2008 condemned widespread human rights abuses including killings and rapes carried out by West Bengal state forces. <br />  <br /> A picket of the Indian Consulate in Birmingham has been called for 4pm on Friday 10th July to protest the Indian state aggression against the adivasis of West Bengal. <br />  <br /> * See also <a href="http://antilandgrab.wordpress.com" target="_blank">http://antilandgrab.wordpress.com</a> <a href="http://sanhati.com" target="_blank">http://sanhati.com</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <span style='color:#777777; font-size:10px'>adivasis</span>, <a href="../keywordSearch/?keyword=anti-corporate&source=682">anti-corporate</a>, <span style='color:#777777; font-size:10px'>bengal</span>, <a href="../keywordSearch/?keyword=india&source=682">india</a>, <span style='color:#777777; font-size:10px'>indigenous struggles</span>, <span style='color:#777777; font-size:10px'>special economic zones</span></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(388);
			
				if (SHOWMESSAGES)	{
				
						addMessage(388);
						echo getMessages(388);
						echo showAddMessage(388);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>