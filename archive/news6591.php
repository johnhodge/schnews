<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 659 - 12th December 2008 - Go Greece Fightin'</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, greece, athens, riots, direct action, alexis grigoropoulos, riot porn, police" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://www.guantanamo.org.uk/content/view/157/37/"><img 
						src="../images_main/guantanamo-7th-banner.jpg"
						alt="Guantanamo Bay 7th Anniversary Day Of Action, January 11th 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 659 Articles: </b>
<b>
<p><a href="../archive/news6591.php">Go Greece Fightin'</a></p>
</b>

<p><a href="../archive/news6592.php">Crap Arrest Of The Week</a></p>

<p><a href="../archive/news6593.php">Having No Truck</a></p>

<p><a href="../archive/news6594.php">Constant Threat</a></p>

<p><a href="../archive/news6595.php">Anti-military Tattoo</a></p>

<p><a href="../archive/news6596.php">Stanstill</a></p>

<p><a href="../archive/news6597.php">Xmas Jeers</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">	<table  border="0" align="right" bordercolor="#999999" bgcolor="#666666" style="margin-left: 15px; border: 2px solid #000000"> 
									<tr> 
										<td> 
											<div align="center">
												<a href="../images/659-greece-1-lg.jpg" target="_blank">
													<img src="../images/659-greece-1-sm.jpg" alt="As Athens host the Paramilitary Olympics" border="0" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>
<p><b><a href="../index.htm">Home</a> | Friday 12th December 2008 | Issue 659</b></p>

<p><a href="news659.htm">Back to the Full Issue</a></p>

<div id="article">

<H3>GO GREECE FIGHTIN'</H3>

<p>
<b>ATHENS EXPLODES INTO RIOTS AFTER MURDER OF 15 YEAR OLD PUNK</b> <br />
 <br />
Six days of solid rioting, banks and government buildings burned to the ground, police stations besieged by thousands of angry demonstrators chucking Molotov&#8217;s. Universities have been occupied, a general strike has been called, the opposition is calling for new elections and the government seems to have gone to ground. The few cabinet members who&#8217;ve been spotted talking to the media have been giving it a &#8216;nothing to do with me mate&#8217; shrug as chaos reigns in Athens and beyond. Students at the Medical School of Athens kept the deputy Minister of Health hostage in the Ministry building for an hour. There&#8217;s been over 200 million euros worth of damage in total, and something like 600 buildings have been damaged or destroyed. It&#8217;s official - Greece is in a state of Anarchy. <br />
 <br />
It all started last Saturday when police shot dead a fifteen year old punk, Alexis Grigoropoulos. This act of cold-blooded murder by overtly fascist police was caught on mobile phone cameras, and was the spark for a wave of protests against the police. The Greek police&#8217;s immediate reaction - tear gas, batons and bullets - turned the situation into a city-wide riot as gangs of young anarchist punks took the opportunity to tell the police what they thought of them - in the only language that they understand. <br />
 <br />
Greek streets are littered with burning cop cars and riot vans, and running battles are continuing between police and demonstrators. Parliament and the HQ of the riot police were besieged on Tuesday. Despite rumours earlier in the day that the protests were running out of steam, around twenty police stations were attacked yesterday (11th). <br />
 <br />
The Greek police are notorious for their brutality and racism, as well as their links to far right groups. They haven&#8217;t been reformed since the days of the fascist colonels came to an end in the '70s, and pretty much every Greek has got a story of brutality at the hands of the police.  <br />
After having killed a young boy, the police, in typical arrogant fashion, spun the story that he was killed by a ricochet. Erm, excuse me- Don&#8217;t �*&%ing shoot at kids! It&#8217;s no defence to say that &#8216;oh I only meant to shoot at his feet&#8217; - if you shoot at kids you&#8217;re already beyond the pale. When you hit one it&#8217;s murder.  <br />
 <br />
But anyway the Greek youth (and most of the older population as well come to think of it) seem pretty savvy to police lies. The riots have already gone way beyond protesting against the police, becoming a mass wave of direct action against an unpopular government. Greeks have put up with a corrupt and incompetent government for years now, they&#8217;ve watched as the state has squandered their wealth and dashed their hopes for material improvement. <br />
 <br />
<b>GREEK EARN</b> <br />
 <br />
Things have reached a real tipping point when they start to affect the middle classes in a big way. The working (and workless) classes are usually screwed over anyways, so the downturn isn&#8217;t such a big shock to them, just another in a long line of shocks. But so long as the middle classes are there to help keep the proles down the rich can sleep in peace at night. But when the middle classes start getting it in the neck, the elites start to notice that they&#8217;re out on a limb. <br />
 <br />
<table align="left"><tr><td style="padding: 0 8 8 0"><a href="../images/659-greece-2-lg.jpg" target="_blank"><img style="border:2px solid black" src="http://www.schnews.org.uk/images/659-greece-2-sm.jpg"  /></a></td></tr></table>What&#8217;s known in Greece as the 800 Euro generation (highly skilled people in supposedly &#8216;decent&#8217; middle class graduate jobs who work for around 700 quid a month) had been promised that joining the Eurozone would be a short-cut to North European style-prosperity and opportunities. Instead what they&#8217;ve got is English prices and East European wages, and a shrinking say in how their country is run in a privatise-or-die age. <br />
 <br />
What&#8217;s really interesting is that the people on the streets are rallying behind Anarchist banners when they challenge the state. Indymedia Greece is reckoned to be the most active local Indymedia site in the world, and the old guard of moribund Stalinist trades unions haven&#8217;t had a bright idea since 1989.  <br />
 <br />
As one Athenian lefty who was willing to talk to us put it: &#8220;<i>Greece is like a coiled spring. We&#8217;re in an impossible situation here. All of our expectations have suddenly been cut short. This is a poor country, and normally people are very conformist, but when you take away peoples&#8217; hopes everything&#8217;s up for grabs. <br />
 <br />
Just a few years ago the Anarchists were seen by most people as just a bunch of troublemakers. Now suddenly everyone is radicalised. Let me give you an example: Some of my friends are in an anarchist group that steals stuff from supermarkets and distributes at in the local markets. A few years ago people didn&#8217;t want to know, but now when they do it people applaud them.</i>&#8221; <br />
 <br />
And possibly, just possibly, what&#8217;s happening in Greece is just the beginning. Peel away the spark (Alexis&#8217; death) and specifically local issues (a government that makes even our Brown-streak-of-a-government look like a paragon of competence and virtue - nearly), and what we begin to see is the effects of the credit-crunch (better known as a recession) on first world countries. The same pressures that have pulled the rug from under the Greek economic non-miracle are there in the UK, it&#8217;s just that here peoples&#8217; relative wealth has shielded them from rising food and oil prices. In Greece a few extra pence to a loaf of bread or a bus fare might be just enough to turn grumblings against the Man into inklings that maybe someone ought to do something about it, and maybe, just maybe, they&#8217;re the ones to do it. <br />
 <br />
What with factory occupations in Obama&#8217;s home town of Chicago, and a general strike in Italy (and serious protests in Iceland - see <a href="../archive/news657.htm">SchNEWS 657</a>), these local actions look like they might be beginning to coalesce into a movement to challenge the smug and self-satisfied bastards that have been getting away with far to much for far to long. <br />
 <br />
Already, Greek-inspired anarchy (and Anarchism) seem to be spreading around Europe. When The Times talks about &#8220;mobs causing violent scenes in Italy, Spain, Russia, Denmark and Turkey&#8221; you know something&#8217;s afoot. There have even been solidarity actions across the world including Germany, Melbourne and New York, and cities across Blighty have joined in the fun. Hundreds marched in Edinburgh in support of Greek comrades, the Greek Embassy in London was shut down, and in Bristol an amazing 30 police vehicle were attacked, as well as other actions in Newcastle, Leeds and elsewhere. Olympic Airways (Greece&#8217;s national carrier) was blockaded on Wednesday. <br />
 <br />
<div style="padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:50%; align:left" align="left">&nbsp;</div> <br />
 <br />
<b>Get yer riot porn now...</b> <br />
* See <a href="http://www.indymedia.org.uk" target="_blank">www.indymedia.org.uk</a> <br />
* <a href="http://www.libcom.org" target="_blank">www.libcom.org</a> - for comment and analysis <br />
* <a href="http://www.occupiedlondon.org/blog" target="_blank">http://www.occupiedlondon.org/blog</a> - up to date, blow by blow account of the riots <br />
* For some proper riot porn see <a href="http://english.aljazeera.net/Services/Gallery/Default.aspx?GalleryID=200812721427182379" target="_blank">http://english.aljazeera.net/Services/Gallery/Default.aspx?GalleryID=200812721427182379</a>
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=direct+action&source=659">direct action</a>, <a href="../keywordSearch/?keyword=police&source=659">police</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(163);
			
				if (SHOWMESSAGES)	{
				
						addMessage(163);
						echo getMessages(163);
						echo showAddMessage(163);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>