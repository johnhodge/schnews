<?php
require_once '../storyAdmin/functions.php';
require_once '../storyAdmin/config.php';
?>

<html>
<head>
<title>SchNEWS 687 - 14th August 2009 - Celt In Action</title>
<meta name="description" content="SchNEWS the free weekly direct action newsheet from Brighton, UK" />
<meta name="keywords" content="SchNEWS, direct action, Brighton, camp for climate action, scotland, wales, mainshill, open cast mining, coal" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" href="../schnews.css" type="text/css" />
<link rel="stylesheet" href="../old_css/article.css" type="text/css" /> 
<!--[if IE]> <style type="text/css">@import "../article_ie.css";</style> <![endif]-->


<link rel="search" href="../search.htm"/>
<link rel="SHORTCUT ICON" href="/favicon.ico"/>
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit" />
<script language="JavaScript" type="text/javascript">

<!--

function MM_swapImgRestore() { //v3.0

  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

}

function MM_findObj(n, d) { //v4.0

  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

  if(!x && document.getElementById) x=document.getElementById(n); return x;

}
//-->

</script>






</head>



<body 
		leftmargin="0px" 
		topmargin="0px" 
		link="#FF6600" 
		vlink="#FF6600" 
		alink="#FF6600" 
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>



<!--	PAGE TITLE	-->
<div class="pageHeader">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="../index.htm"><img 
					src="../images_main/schnews.gif" 
					width="292px" 
					height="90px" 
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental 
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." 
					border="0" 
			/></a></td>
			<td><a href="http://climatecamp.org.uk/"><img 
						src="../images_main/climate-camp-09-banner.jpg"
						alt="Camps For Climate Action 2009"
						width="465" 
						height="90" 
						border="0" 
			/></a></td>
		</tr>
	</table>
</div>	





<!--	NAVIGATION BAR 	-->


<div class="navBar">
		<table cellpadding="0" cellspacing="0">
			<tr>
				<td><a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('subscribe','','../images_menu/subscribe-over.gif',1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="Subscribe to receive SchNEWS erery week by email" /></a></td>
					
				<td><a href="../archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('archive','','../images_menu/archive-over.gif',1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View all of our back issues" /></a></td>
				
				<td><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('about','','../images_menu/about-over.gif',1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about the history of SchNEWS and how it's put together every week" /></a></td>
				
				<td><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contacts','','../images_menu/contacts-over.gif',1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links of protest and community groups in UK and abroad" /></a></td>
				
				<td><a href="../diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('diyguide','','../images_menu/guide-over.gif',1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="A selection of How To guides for direct action andother fun stuff" /></a></td>
				
				<td><a href="../monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('monopolise','','../images_menu/monopolise-over.gif',1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Previous articles and hot topics" /></a></td>
				
				<td><a href="../schmovies/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('SchMOVIES','','../images_menu/schmovies-over.gif',1)"><img src="../images_menu/schmovies.gif" alt="SchMOVIES" name="SchMOVIES" width="90" height="32" border="0" id="SchMOVIES" /></a></td>
				
			</tr>
		</table></div>
<div class="copyleftBar">
<img src="../images_menu/description.gif" width="640" height="20" alt="Copyleft - Information for direct action - Published weekly in Brighton since 1994" /></div>





<!--	=========================================== -->	
<!--					LEFT BAR 	-->	
<!--	=========================================== -->	
<div class="leftBar"> 

		<!-- Links to other stories in this edition -->
		<div id="leftLinks">
<b>Issue 687 Articles: </b>
<p><a href="../archive/news6871.php">Fash Get The Brum Rush</a></p>

<p><a href="../archive/news6872.php">From Russia With Hate</a></p>

<p><a href="../archive/news6873.php">Notes From A Small Island  </a></p>

<b>
<p><a href="../archive/news6874.php">Celt In Action</a></p>
</b>

<p><a href="../archive/news6875.php">Ciggy Stubbed Out?</a></p>

<p><a href="../archive/news6876.php">Bad Korea Choice</a></p>

<p><a href="../archive/news6877.php">Greek Tragedy  </a></p>

<p><a href="../archive/news6878.php">And Finally</a></p>
 
		
		</div>


</div>





<!-- ============================================================================================
       						
                            MAIN TABLE CELL
                                
============================================================================================ --> 
<div class="mainBar">

		<!-- Main body of article -->
		<div id="article">
<p><b><a href="../index.htm">Home</a> | Friday 14th August 2009 | Issue 687</b></p>

<p><a href="news687.htm">Back to the Full Issue</a></p>

<div id="article">

<h3>CELT IN ACTION</h3>

<p>
As the <strong>Scottish Camp for Climate Action</strong> at Mainshill draws to a close after a successful week of workshops and action, the camp has moved south to the site of yet another open-cast coal mine, this time in Wales. <br />  <br /> Following last week&rsquo;s sabotage of a conveyor at a nearby mining site (see <a href='../archive/news686.htm'>SchNEWS 686</a>), a visit was paid to Lord Home, the Mainshill site landowner (currently under investigation by the FBI for fraud as well as by Scotland Yard for false expense claims) who has an agreement with Scottish Coal to turn over part of his estate for a new  mine. Banners were draped, frisbee games and a picnic were held on Home&rsquo;s lawn. <br />  <br /> The Camp culminated with a demo against the serious health hazards for the local community and coal&rsquo;s contribution to climate change, outside the offices of South Lanarkshire Council whose members have approved new coal extraction despite massive local opposition. <br />  <br /> &nbsp;Local Councillor Daniel Meikle of the family business Meikle Construction and chair of the planning committee for many years, was a particular target for campaigners&rsquo; ire. <br />  <br /> The Camp also released a dossier entitled &lsquo;Adverse Effects of Opencast Mining&rsquo; which paints an alarming picture of a whole host of health issues from cancer to asthma and depression, caused not just by the coal dust, but also by noise and the increase in heavy goods traffic.&nbsp; <br />  <br /> * See <a href="http://climatecampscotland.org.uk" target="_blank">http://climatecampscotland.org.uk</a> and <a href="http://coalactionedinburgh.noflag.org.uk&nbsp;" target="_blank">http://coalactionedinburgh.noflag.org.uk&nbsp;</a> <br />  <br /> Following hot on its heels, <strong>Climate Camp Wales</strong> premi&egrave;red on Wednesday (12th) with around 60 protesters descending on Ffos-y-Fran, a controversial open-cast coal mine in the Merthyr Tydfil borough, about four miles from Newport. <br />  <br /> The Camp will occupy the site until 16th August, with campaigners from the six areas of Wales aiming to address the lack of honesty in government policy - with its grand promises about limiting climate damaging activities, yet continued investment in and expansion of the coal mining industry.&nbsp; <br />  <br /> Local campaign groups in the region, such as Residents Against Ffos-y-Fran (RAFF), have been consistently ignored by government and planners, despite some campaigners living only 36 metres from the 1,000 acre mine. Although a report funded by the Welsh Assembly expressed concern over the detrimental health effects of the air pollution and dust particles, objections have so far been discounted. <br />  <br /> The camp is offering a variety of workshops from herbalism and low impact development to direct action and lobbying, as well as providing information about current climate campaigns such as those at Vestas and Rossport. This Friday sees a &lsquo;green awareness day&rsquo;, organised by residents in the Merthyr Tydfil area. The event, taking place in the town market, will include representatives from sustainable energy firms, the local council&rsquo;s recycling department, Friends of the Earth and the Climate Camp. <br />  <br /> For directions and how to get involved see: <a href="http://www.climatecampcymru.org" target="_blank">www.climatecampcymru.org</a> <br />  <br />
</p>

</div>

<div id="keywords" style="margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px" align="right"><b>Keywords:</b> <a href="../keywordSearch/?keyword=camp+for+climate+action&source=687">camp for climate action</a>, <a href="../keywordSearch/?keyword=coal&source=687">coal</a>, <a href="../keywordSearch/?keyword=mainshill&source=687">mainshill</a>, <span style='color:#777777; font-size:10px'>open cast mining</span>, <a href="../keywordSearch/?keyword=scotland&source=687">scotland</a>, <a href="../keywordSearch/?keyword=wales&source=687">wales</a></div>


<br /><br />

		</div>
		
		
		<!-- Messages/Discussion -->
		<a name="comments">&nbsp;</a>
		<div id="messagesBoard">
			
			<?php
				
				recordHit(459);
			
				if (SHOWMESSAGES)	{
				
						addMessage(459);
						echo getMessages(459);
						echo showAddMessage(459);
				
				}
			
			?>
		
		</div>
		
		
		
		
			
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			<p><font face="Arial, Helvetica, sans-serif"><b>Subscribe to SchNEWS:</b> Send 
			1st Class stamps (e.g. 10 for next 9 issues) or donations (payable to Justice?). 
			Or &pound;15 for a year's subscription, or the SchNEWS supporter's rate, &pound;1 
			a week. Ask for &quot;originals&quot; if you plan to copy and distribute. SchNEWS 
			is post-free to prisoners. </font></p><p>
			<img src="../images_main/divider.gif" width="48%" height="40" /><a href="#top"><img src="../images_main/divider_arrow.gif" width="24" height="40" border="0" /></a><img src="../images_main/divider.gif" width="48%" height="40" />
			</p><p></p>



</div>








</body>
</html>