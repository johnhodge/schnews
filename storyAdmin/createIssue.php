<?php
/* CREATE THE EDITION ON THE SERVER

AUTHOR: ANDREW WINTERBOTTOM support@tendrousbeastie.com www.tendrousbeastie.com
LICENSING: GPL V2.
COMPATIBILITY: PHP4+ MYSQL 4+ XHTML

*/


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';

/* PAGE SPECIFIC CONSTANTS - THESE CAN BE USED TO OVERRIDE THE GLOBAL CONFIG ARS IF REQUIRED */
// define('DEBUG', 1);



/* DATA PROCESSING SECTION */

// GET ISSUE ID
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{

	$issueId	=	$_GET['issue'];

}

if (!isset($issueId))	{
	if (DEBUG)	echo "<br>createIssue.php :: data processing :: issueId not set ;; exiting silently <br>";
	exit;
}

if (!isset($stage) || !is_numeric($stage))	{
	if (DEBUG)	echo "<br>createIssue.php :: data processing :: stage not set ;; exiting silently <br>";
	exit;

}

switch ($stage)		{

	case '1':
		createIssueStage1($issueId);
		break;

	case '2':
		createIssueStage2($issueId);
		break;

	case '3':
		createIssueStage3($issueId);
		break;

	case '4':
		createIssueStage4($issueId);
		break;
}













/*
 * =======================================================================================
 *
 * 							STAGE 1 - NOTIFY TO SCREEN
 *
 * 	======================================================================================
 */


function createIssueStage1($issueId)	{

	if (!is_numeric($issueId))	return false;

	echo "<h2>Stage 1</h2>";
	echo "<blockquote>";
	echo "<p>This will create the new issue on the server and generate the RSS feed.</p>";
	echo "<p>If the files for this issue already exist on the server they will be overwritten with these new ones...</p>";
	echo "<p>&nbsp;</p>";
	echo "<p><b>Warning:</b> Please make sure you have previewed all pages before you run this process.</p>";
	echo "</blockquote>";
	echo "<br /><br /><br />";
	echo "<p>Do you want to continue?</p>";
	echo "<form method=\"POST\" action=\"index.php?createFullEdition=2&issue=$issueId\">
				<input type='submit' value='Yes, I want to continue' />
			</form>
			<br />
			<form method=\"POST\" action=\"index.php?showIssue=$issueId\">
				<input type='submit' value='No, back to the edition list' />
			</form>
			<br /><br /><br />
			<form method=\"POST\" action=\"index.php?createFullEdition=3&issue=$issueId\">
				<input type='submit' value='I do not want to make any files, just let me make the Plain Text Email' />
			</form>";
	return true;


}














/*
 * =======================================================================================
 *
 * 							STAGE 2 - CREATE THE PRIMARY HTM AND PHP FILES
 *
 * 	======================================================================================
 */


function createIssueStage2($issueId)	{

	if (!is_numeric($issueId))	return false;

	require '../storyAdmin/storyCreationFunctions.php';

	$conn		=	mysqlConnect();
	$sql		=	" SELECT issue FROM issues WHERE id = $issueId ";
	$rs			=	mysqlQuery($sql, $conn);
	$res		=	mysqlGetRow($rs);
	$issueNum	=	$res['issue'];

	$issue			=	makeFullIssue($issueId);
	$issueFilename 	= "../archive/news$issueNum.php";
	$issueRedirect 	= "../archive/news$issueNum.htm";

	echo "<h1>Creating Issue...</h1>";
	echo "<p>..................................................</p>";
	echo "<h3>Please scroll down to view the full report and further options...</h3>";
	echo "<p>If there are any errors listed then it probably means some files were not generated. This is most likely to do with a problem on the server. Please contact <a href='mailto:support@tendrousbeastie.com'>support@tendrousbeastie.com</a> mentioning the issue number and the file/s that had the error</p>";
	echo "<p>..................................................</p>";

	echo "<blockquote>";
	echo "<p><b>Creating $issueFilename .....</b></p>";





	if (file_exists($issueRedirect)) unlink($issueRedirect);
	if (!file_put_contents($issueRedirect, makeIssueRedirect($issueNum))) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Main issue redirect</b></font></blockquote></blockquote>";
		echo "<br />";

	} else	{

		echo "<blockquote><blockquote>Main issue redirect created</blockquote></blockquote>";
		echo "<br />";

	}

	@chmod($issueRedirect, 0755);






	if (file_exists($issueFilename)) unlink($issueFilename);
	if (!file_put_contents($issueFilename, $issue)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Main issue file</b></font></blockquote></blockquote>";
		echo "<br /><br /><br />";

	} else	{

		echo "<blockquote><blockquote>Main issue file created</blockquote></blockquote>";
		echo "<br /><br /><br />";

	}
	chmod($issueFilename, 0755);

	$sql		=	" SELECT id, story_number FROM articles WHERE issue = $issueId ";
	$rs			=	mysqlQuery($sql, $conn);
	while ($res		=	mysqlGetRow($rs))	{

			$articleId			=	$res['id'];
			$articleNum			=	$res['story_number'];
			$articleFilename	=	"../archive/news$issueNum$articleNum.php";

			$article			=	makeFullArticle($articleId);

			echo "<p><b>Creating $articleFilename .....</b></p>";

			if (file_exists($articleFilename))	unlink($articleFilename);
			file_put_contents($articleFilename, $article);
			if (!file_put_contents($articleFilename, $article)) {

				echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Article number $articleNum file</b></font></blockquote></blockquote>";
				echo "<br /><br /><br />";

			} else	{

				echo "<blockquote><blockquote>Article number $articleNum file created</blockquote></blockquote>";
				echo "<br /><br /><br />";

			}
			chmod($articleFilename, 0755);


	}


	//	RSS FEED
	$rssFilename		=	"../feed.xml";

	echo "<p><b>Creating RSS Feed .....</b></p>";
	$rssFeed			=	makeFullRSSFeed($issueId);

	if (file_exists($rssFilename))	unlink($rssFilename);
	if (!file_put_contents($rssFilename, $rssFeed)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create RSS Feed file</b></font></blockquote></blockquote>";
		echo "<br /><br /><br />";

	} else	{

		echo "<blockquote><blockquote>RSS Feed file created</blockquote></blockquote>";
		echo "<br /><br /><br />";

	}
	chmod($rssFilename, 0755);


	//	HOME PAGE
	$homepageFilename		=	"../index.php";

	echo "<p><b>Creating Homepage.....</b></p>";
	$homepage			=	makeHomepage($issueId);

	if (file_exists($homepageFilename))	unlink($homepageFilename);
	if (!file_put_contents($homepageFilename, $homepage)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Homepage file</b></font></blockquote></blockquote>";
		echo "<br /><br /><br />";

	} else	{

		echo "<blockquote><blockquote>Homepage file created</blockquote></blockquote>";
		echo "<br /><br /><br />";

	}
	chmod($homepageFilename, 0755);


	//	ARCHIVE INDEX
	$archiveIndexFilename		=	"../archive/index.php";

	echo "<p><b>Creating Back Issues Homepage.....</b></p>";
	$archiveIndex			=	makeArchiveIndex($issueId);

	if (file_exists($archiveIndexFilename))	unlink($archiveIndexFilename);
	if (!file_put_contents($archiveIndexFilename, $archiveIndex)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Archive Index file</b></font></blockquote></blockquote>";
		echo "<br /><br /><br />";

	} else	{

		echo "<blockquote><blockquote>Archive Index file created</blockquote></blockquote>";
		echo "<br /><br /><br />";

	}
	chmod($archiveIndexFilename, 0755);



	//	LATEST NEWS (/NEW)
	echo "<p><b>Creating New Shortcut Redirect.....</b></p>";
	$new			=	"<?php \r\n header(\"Location: http://www.schnews.org.uk/archive/news$issueNum.php\"); \r\n ?> ";
	$newLocation	=	"../new/index.php";
	if (file_exists($newLocation)) unlink($newLocation);
	if (!file_put_contents($newLocation, $new)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create /new redirect</b></font></blockquote></blockquote>";
		echo "<br />";

	} else	{

		echo "<blockquote><blockquote>/new redirect created</blockquote></blockquote>";
		echo "<br />";

	}
	@chmod($newLocation, 0755);




	//	TWITTER REDIRECT (/NEW)
	echo "<p><b>Creating Twitter Redirect.....</b></p>";
	$new			=	"<?php \r\n header(\"Location: http://www.schnews.org.uk/archive/news$issueNum.php\"); \r\n ?> ";
	if (!mkdir("../n/$issueNum"))	{

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Twitter redirect directory</b></font></blockquote></blockquote>";
		echo "<br />";

	}


	$newLocation	=	"../n/$issueNum/index.php";
	if (file_exists($newLocation)) unlink($newLocation);
	if (!file_put_contents($newLocation, $new)) {

		echo "<blockquote><blockquote><font color=\"red\"><b>Error: Couldn't create Twitter redirect</b></font></blockquote></blockquote>";
		echo "<br />";

	} else	{

		echo "<blockquote><blockquote>Twitter redirect created</blockquote></blockquote>";
		echo "<br />";

	}
	@chmod($newLocation, 0755);




	echo "</blockquote>";

	echo "<h2>Your issue has been created</h3>";

	$sql	=	" 	UPDATE issues
					SET posted = 1
					WHERE id = $issueId ";
	$rs		=	mysqlQuery($sql, $conn);

	echo "<h2>It has been posted in the database</h3>";

	echo "<br /><br />";
	echo "<p>.............................</p>";
	echo "<br /><br />";

	echo "<h1><span style='color@#666666'>Stage 2 :</span> Plain Text Email Options</h1>";
	echo "<br />";

	echo "<p><b style='font-size:14px'>Option 1 :</b> Click here to send the email automatically to the mailing list. You will then need to go to the mailing list administration page and authorise the email to have it sent...</p>";
	echo "<form method=\"POST\" action=\"index.php?createFullEdition=4&issue=$issueId\">
				<input type='submit' value='Send Email to Mailing List' />
			</form>";

	echo "<br /><br />";

	echo "<p><b style='font-size:14px'>Option 2 :</b> Click below to generate the Plain Text Email and simply display it on screen</p>";
	echo "<form method=\"POST\" action=\"index.php?createFullEdition=3&issue=$issueId\">
				<input type='submit' value='Create Plain Text Email' />
			</form>";

	echo "<br /><br />";

	echo "<p><b style='font-size:14px'>Option 3 :</b>  ...or go back to the main page without making the plain text email...</p>";
	echo "<form method=\"POST\" action=\"index.php?showIssue=$issueId\">
				<input type='submit' value='Back to main page' />
			</form>";

	return true;

}














/*
 * =======================================================================================
 *
 * 							STAGE 3 - CREATE THE PLAIN TEXT EMAIL
 *
 * 	======================================================================================
 */


function createIssueStage3($issueId)	{

	if (!is_numeric($issueId))	return false;

	require '../storyAdmin/storyCreationFunctions.php';

	$txtEmail		=	makeFullTXTEmail($issueId);

	echo $txtEmail;

	echo "<br /><br /><br />";

	echo "<p>Click here to send the email automatically to the mailing list...</p>";
	echo "<form method=\"POST\" action=\"index.php?createFullEdition=4&issue=$issueId\">
				<input type='submit' value='Send Email to Mailing List' />
			</form>";

	echo  "<p>Or click here to return to the edition list...</p>";
	echo "<form method=\"POST\" action=\"index.php?showIssue=$issueId\">
				<input type='submit' value='No, back to the edition list'
			</form>";

	return true;

}






















/*
 * =======================================================================================
 *
 * 							STAGE 4 - SEND PLAIN TEXT EMAIL
 *
 * 	======================================================================================
 */


function createIssueStage4($issueId)	{

	if (!is_numeric($issueId))	return false;

	require '../storyAdmin/storyCreationFunctions.php';

	$txtEmailSource		=	makeFullTXTEmail($issueId);
	$txtEmail		=	strip_tags(str_replace('<br /><br />', chr(13), $txtEmailSource));

	$conn			=	mysqlConnect();
	$sql			=	" SELECT issue, date FROM issues WHERE id = $issueId ";
	$rs			=	mysqlQuery($sql, $conn);
	$issue			=	mysqlGetRow($rs);


	$subject		=	"SchNEWS ".$issue['issue'].", ".makeHumanDate($issue['date'], 'words+day')." - PLAIN TEXT";

	if (file_exists('text_email.txt')) unlink('text_email.txt');
		file_put_contents('text_email.txt', $txtEmail);

	 $mailResult = send_smtp_from_custom_sender(PLAIN_TEXT_MAILING_LIST_ADDRESS, 'Jo Makepeace <webmaster@schnews.org.uk>',$subject, html_entity_decode($txtEmail));
	//send_smtp_from_custom_sender('andy@tendrousbeastie.com', 'Jo Makepeace <webmaster@schnews.org.uk>',$subject, $txtEmail);

	if ($mailResult)	{

			echo "<h1>Email Sent to the Mailing List to ".PLAIN_TEXT_MAILING_LIST_ADDRESS."</h1>";

	}	else {

			echo "<h1>Problem sending email ".PLAIN_TEXT_MAILING_LIST_ADDRESS.". Contact <a href='mailto:support@tendrousbeastie.com'>support@tendrousbeastie.com</a></h1>";

	}
	echo "<br /><br /><br />";
	echo $txtEmailSource;

	echo "<br /><br /><br />";
	echo "<br /><br /><br />";
	echo "<br /><br /><br />";

	echo 	"<form method=\"POST\" action=\"index.php?showIssue=$issueId\">
				<input type='submit' value='Back to the edition list'
			</form>";

	return true;

}







function makeIssueRedirect($issueNum)	{

		if (!is_numeric($issueNum))	{

				return "<?php header('Location: http://www.schnews.org.uk'); ?>";

		}
		$location   =   "http://www.schnews.org.uk/archive/news$issueNum.php";
		return		"<?php header('Location: $location'); ?>";

}





