<?php

// 	CHECK FOR LOGIN
if (!$_SESSION[SES.'loggedin']) exit;

//	Test that article_id is set...
if (!isset($article_id))	{

	if (DEBUG)	echo "article id not set :: exiting";
	exit;

}


//	=====================================================================================
//									DATA PREPARATION

//	Get the data for the article (will return empty data if id == 0)
$article		=	diy_get_article($article_id);


//	Prepare a few vars dependining on whether we're adding or editing
if ($article_id)	{

	$mode	=	'edit';
	$submit_button = 'Save Changes to Article';

}	else	{

	$mode	=	'add';
	$submit_button	= 'Save New Article';

}


//	Cache the DIY categories
$categories		=	diy_get_categories();





//	=====================================================================================
//									DISPLAY OUTPUT

// Draw basic form, using an output buffer...
$output		=	array();

//	Begin Form
$output[]	=	"<form method='POST' action='?dit=1&edit_article=$article_id&do=1'>";
$output[]	=	"<table width='700px'>";

//	Title
$output[]	=	"<tr>";
$output[]	=	"<td><b>Title : </b></td>";
$output[]	=	"<td><input type='text' name='title' value='".stripslashes($article['title'])."' style='width: 525px' /></td>";
$output[]	=	"</tr>";

//	Content
$output[]	=	"<tr>";
$output[]	=	"<td><b>Content : </b></td>";
$output[]	=	"<td><textarea name='content' style='width: 525px; height: 700px'>".stripslashes($article['content'])."</textarea></td>";
$output[]	=	"</tr>";

//	Categories
$output[]	=	"<tr>";
$output[]	=	"<td><b>Category : </b></td>";
$output[]	=	"<td><select name='category'>";
foreach ($categories as $id => $cat)	{

	$option 	=	"<option value='$id' ";
	if ($article['category'] == $id) $option .= ' selected ';
	$option 	.=	" >$cat</option>";
	$output[]	=	$option;

}
$output[]	=	"</select></td>";
$output[]	=	"</tr>";

//	Submit Button
$output[]	=	"<tr>";
$output[]	=	"<td colspan='2' align='right'>";
$output[]	=	"<input type='submit' name='article_submit' value='$submit_button' />";
$output[]	=	'</td></tr>';



//	End Form
$output[]	=	"</table>";
$output[]	=	"</form>";

//	Flush buffer to screen
echo implode("\r\n", $output);

?>