<?php
	require '../storyAdmin/functions.php';
	require '../functions/functions.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>SchNEWS Back Issues - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<META name=description content="Feature article by SchNEWS the free weekly direct action newsheet from Brighton, UK. Going back to issue 100, some available as PDF" />
<META name=keywords content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">

<link rel="stylesheet" href="../old_css/schnews_new.css" type="text/css" />
<link rel="stylesheet" href="../archiveIndex.css" type="text/css" />
<link rel="stylesheet" href="../issue_summary.css" type="text/css" />

<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0

  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>

</head>



<body
		leftmargin="0px"
		topmargin="0px"
		link="#FF6600"
		vlink="#FF6600"
		alink="#FF6600"
		onLoad=		"MM_preloadImages('../images_menu/archive-over.gif',
					'../images_menu/about-over.gif',
					'../images_menu/contacts-over.gif',
					'../images_menu/guide-over.gif',
					'../images_menu/monopolise-over.gif',
					'../images_menu/prisoners-over.gif',
					'../images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'../images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="../javascript/indexMain.js"></script>




<div class='main_container'>





<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="../index.htm"><img
					src="../images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>

			<?php

				//	Include Banner Graphic
				require "../inc/bannerGraphic.php";

			?>


</div>











<!--	NAVIGATION BAR 	-->

<div class="navBar">

<!--#include virtual="../inc/navBar.php"-->

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

		<?php

			//	Include Left Bar
			require '../inc/archiveIndexLeftBar.php';

		?>

</div>




<div class="copyleftBar">

<!--#include virtual="../inc/copyLeftBar.php"-->>

</div>






<!-- ============================================================================================

                            MAIN TABLE CELL

============================================================================================ -->
<div class="mainBar">


<p><font face="Arial, Helvetica, sans-serif" size="2"><b>
			<a href="../index.htm">Home</a> &#124; BACK ISSUES<br /> </b></font><font face="Arial, Helvetica, sans-serif" size="1">PDF
			(<a href="http://www.adobe.com/products/acrobat/readstep2_allversions.html" target="_blank">Acrobat</a>)
			are available for all these issues. Read the <a href="../pages_menu/pdf_notes.html">notes
			on PDF files</a> for help, especially with printing.</font></p><p><font face="Arial, Helvetica, sans-serif"><a href="../search.htm"><b>SchNEWS
			SEARCH FACILITY</b></a></font></p><h3 align="left"><font face="Arial, Helvetica, sans-serif"><b>Most
			Recent...</b></font></h3>


			<div id="backIssues">

				****BACK_ISSUES****

			</div>





			<!-- BEGIN ARCHIVED BACK ISSUES -->

			<p align="left">&nbsp;</p>
			<p align="justify"><font face="Arial, Helvetica, sans-serif">
				<a name="backissues"></a>
				<font size="2"><b><font size="4">Back Issues...</font></b></font>
			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-701-750.htm"><b><font size="2">Issues 701-750 <br />
  				November 2009 - December 2010 </font></b></a>
  			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-651-700.htm"><b><font size="2">Issues 651-700 <br />
  				October 2008 - November 2009 </font></b></a>
  			</font></p>

			<hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-601-650.htm"><b><font size="2">Issues 601-650 <br />
  				August 2007 - October 2008 </font></b></a>
  			</font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-551-600.htm"><b><font size="2">Issues 551-600<br />
            	July 2006 - August 2007</font></b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif">
            	<a href="index-501-550.htm"><b><font size="2">Issues 501-550<br />
            	June 2005 - July 2006</font></b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif" size="2">
            	<a href="index-451-500.htm"><b>Issues 451 - 500<br />
            	April 2004 - June 2005</b></a>
            </font></p>

            <hr />

            <p><font face="Arial, Helvetica, sans-serif" size="2">
            	<a href="index-401-450.htm"><b>Issues 401 - 450<br />
            	April 2003 - April 2004</b></a>
            </font></p>

            <hr />

            <!-- END ARCHIVE BACK ISSUES -->



            <font face="Arial, Helvetica, sans-serif" size="2"></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-351-401.htm"><img src="../images_main/peacederesistanceweb.jpg" width="100" height="145" align="right" border="1" alt="SchNEWS - Peace De Resistance" /><b>Issues
			351-401<br /> April 2002 - April 2003</b></a><br> <b>These issues are published
			in the book 'Peace de Resistance':<br> </b>'As the Forces of Darkness gather for
			a new attack on Iraq, they're shaken by an unexpected explosion of resistance.
			US airbases are invaded; military convoys are blockaded; schoolkids stage mass
			walkouts from Manchester to Melbourne; roads are occupied, embassies besieged,
			and cities all over the world are brought to a standstill by the biggest mass
			demonstrations ever seen. Read about the efforts to sabotage the war machine that
			the corporate media ignored: not just the mass demos, but Tornado-trashing in
			Scotland, riots across the Middle East, and train blockades in Europe. Featuring
			SchNEWS 351-401, this book also covers many of the past year's other stories from
			the rebel frontlines, including GM crop-trashing, international activists in Palestine,
			and the 10th anniversary of Castlemorton. All that plus loads of new articles,
			photos, cartoons, subverts, satire, a comprehensive contacts list, and more&#133;'
			<br /> </font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>PLUS
			INCLUDES FREE CD-ROM by BEYOND TV featuring footage of many of the actions reported
			in the book, satire plus other digital resources</b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>&quot;Peace
			de Resistance&quot;<br /> </b>ISBN 09529748 7 8 <br /> &pound;7.00 inc. p&amp;p
			direct from us.<br /> Contact SchNEWS for distribution details<b><br /> <a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="index-301-350.htm" target="index-301-350.htm"><img src="../images_issues/sotw.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Of The World" /></a><b><font size="3"><a href="index-301-350.htm"><font size="2">Issues
			301-350 <br /> April 2001 - April 2002</font></a><br /> </font><b>These issues
			are published in the book 'SchNEWS Of The World':<br> </b></b>The temperature
			goes up a few notches as the earth's climate has its hottest year on record -
			and the corporate carve-up lifts its tempo in the paranoia and madness of September
			11th. Behind the haze Argentina goes into meltdown. Israel reoccupies Palestine
			causing a second Intifada. Hundreds of thousands come out on the street in Genoa,
			Quebec, Gothenburg, Barcelona, Brussels against globalised institutions. Even
			larger numbers fight for their land and livelihood against neo-liberalism in South
			Africa, India, South America and the rest of the global south. <br /> These 50
			issues plus 200 other pages of articles, cartoons, photos, graphics, satire, and
			a comprehensive contact database are all together in<b>...<br /> SchNEWS Of The
			World</b> .<br /> 300 pages, ISBN 09529748 6 X, &pound;4 inc. p&amp;p direct from
			us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b></font></p><hr />
			<p><font face="Arial, Helvetica, sans-serif" size="2"><img src="../images_issues/yearbook2001.gif" width="100" height="146" align="right" border="1" alt="SchNEWS Squall Yearbook 2004" /><b><b><font size="3"><a href="index-251-300.htm"><font size="2">Issues
			251-300 <br /> March 2000 - April 2001</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchNEWS/Squall Yearbook 2001':</b></b><br> The
			Zapatistas march into Mexico City, thousands disrupt the World Bank meeting in
			Prague, Churchill gets an anarchist make-over: from Bognor to Bogota, Dudley to
			Delhi, and Kilburn to Melbourne, resistance has become as global as the institutions
			of capitalism. This was a year full of stories of people at the frontline of struggles
			worldwide, and creating sustainable solutions to the corporate carve-up of the
			planet. These 50 issues of SchNEWS along with the best of Squall magazine, plus
			loadsa photos, cartoons, satirical graphics, subverts, and a comprehensive contacts
			database are available together in....<br /> <b>SchNEWS/Squall Yearbook 2001</b>
			<br /> 300 pages, ISBN 09529748 4 3, BARGAIN - &pound;3.00 inc. p&amp;p direct
			from us.<br /> <b><a href="../pages_merchandise/merchandise.html">Order This Book</a></b>
			</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><img src="../images_issues/schquall.gif" width="100" height="144" align="right" border="1" alt="SchQUALL" /><b><font size="3"><a href="index-201-250.htm"><font size="2">Issues
			201-250<br /> February 1999 - March 2000</font></a></font></b></b><br /> <b><b>These
			issues are published in the book 'SchQUALL': <br> </b></b>Genetically modified
			crops get trashed, animal rights, freemasons, June 18th '99 international day
			of action against capitalism, Exodus Collective, Cuba, the November 30th '99 Battle
			for Seattle, climate change, parties and festivals, indigenous peoples' resistance
			to multinational corporations, the privatisation of everything in sight, crap
			arrests of the week, prisoner support and much more&#133;the full lowdown on the
			direct action movement in Britain and abroad! <br /> These 50 issues of SchNEWS
			plus the best of Squall magazine, top photographs, cartoons, subverts and much
			more are available together in ...<b><br /> SchQUALL - SchNEWS and Squall Back
			To Back</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/survival.gif" width="100" height="143" align="right" border="1" alt="SchNEWS Surviva Guide" /></b><font size="3"><a href="index-151-200.htm"><font size="2">Issues
			151-200 <br /> January 1998 - January 1999 </font></a></font></b><br /> <b>These
			issues are published in the book 'Survival Handbook': </b></b><br> Read all about
			it! Genetic crop sites get a good kicking; streets reclaimed all over the world;
			docks occupied in protest of death at work; protestors rude about multinational
			corporations' plans for world domination... <br /> SchNEWS gives you the news
			the mainstream media ignores. Tells you where to party and protest. Tries not
			to get all po-faced about what's going down in the world. <br /> These 50 SchNEWS
			issues are together with a set of 'survival handbook' stylee articles to help
			you survive into the new millenium... plus photos, cartoons, a comprehensive database
			of nearly five hundred grassroots organisations and more in... <br /> <b>SchNEWS
			Survival Handbook</b><br /> <b>SOLD OUT</b> - You might be able to find a copy
			in your local radical bookshop -if you're lucky.</font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><b><img src="../images_issues/annual_small.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Orange Book" /></b><font size="3"><a href="index-101-150.htm"><font size="2">Issues
			101-150<br /> December 1996 - January 1998</font></a></font><br /> These issues
			are published in the book 'SchNEWS Annual':</b></b><br> &quot;The McLibel trial
			ends, Manchester Airport second runway protest camp goes mad, eviction at Fairmile,
			the GANDALF Trial, anti-nuclear demos in Germany, the massive Reclaim The Streets
			that took back Trafalgar Square... New Labour breaking each and every promise
			they ever made.<br /> These 50 issues are together with loads more pics, graphics
			and articles in...<br /> <b>SchNEWS Annual </b><br /> 230 pages, ISBN: 0-9529748-1-9,
			&pound;BARGAIN - &pound;2.00 inc. p&amp;p direct from us.<b><br /> <b><a href="../pages_merchandise/merchandise.html">Order
			This Book</a></b></b></font></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/round.gif" width="101" height="145" align="right" border="1" alt="SchNEWS Black Book" /><font size="3"><a href="index-051-100.htm"><font size="2">Issues
			51-100 <br /> December 1995 - November 1996</font></a></font></b><br> <b>These
			issues are published in the book 'SchNEWS Round':</b> <br /> </b>Direct Action
			takes over England... M41 motorway gets occupied in huge Reclaim The Streets party...
			The Third Battle Of Newbury... plus the national SchLIVE Tour, the Liverpool Dockers
			go 'direct action', resistance to Jobseekers Allowance, Exodus Collective, Trident
			Ploughshares women go free after smashing Hawk Jet, Selar Opencast Mine protest
			camp, Fairmile - A30 protest camp, Wandsworth Eco-village, the Squatter's Estate
			Agency...SchNEWS publishes the inside story from the activists themselves! <br />
			The 50 issues of SchNEWS featuring all this stuff, plus 60 extra pages of photos,
			cartoons, more articles plus the 1996 activists database is available in...</font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b>SchNEWS
			Round</b><br /> <b>SOLD OUT</b> - You might be able to find a copy in your local
			radical bookshop -if you're lucky.</font></p><p></p><hr /> <p><font face="Arial, Helvetica, sans-serif" size="2"><b><b><img src="../images_issues/reader.gif" width="101" height="142" align="right" border="1" alt="SchNEWS Reader" /><font size="3"><a href="index-001-50.htm"><font size="2">Issues
			1-50 <br /> November 1994 - November 1995</font></a></font><br /> These issues
			are published in the book 'SchNEWS Reader':</b></b><br> Where It All Began...<br />
			From the pilot edition edition right through the first year of SchNEWS featuring
			the first of many Crap Arrest Of The Weeks, the Criminal Justice Act 'Arrestometer',
			and a running commentary of resistance as the sections of the Criminal Justice
			Act come in attacking Travellers, Squatters, Hunt Sabs, Ravers, Protesters, Footie
			Fans and freedom in general. In the news was... Shoreham Live Animal Export protests,
			the end of the No-M11 Link Rd campaign, Pollok Free State anti-M77 protest in
			Glasgow, eviction of Stanworth Valley 'village in the trees' near Blackburn against
			the building of the M65, opencast mining protest camps in South Wales and much
			more...<br /> These historic 50 issues of SchNEWS, plus loadsa cartoons by Kate
			Evans and more are available in...<b><br /> </b></font></p><p><font face="Arial, Helvetica, sans-serif" size="2"><b><b>SchNEWSreader</b><br />
			<b>SOLD OUT</b> </b>Rare as hen's teeth. If you missed out you can read most of
			the SchNEWS issues here - and if you really want to see em as they were you can
			download scans of them below....</font></p><p><img src="../images_main/spacer_black.gif" width="100%" height="10" /></p></td></tr>
			<tr> <td valign="bottom" width="10" height="2">&nbsp;</td><td valign="top" width="468" height="2"><img src="../images_main/spacer.gif" width="1" height="10" /><br />
			<font face="Arial, Helvetica, sans-serif" size="2"><font size="1">SchNEWS, c/o
			Community Base, 113 Queens Rd, Brighton, BN1 3XG, England <br /> Press/Emergency Contacts: +44 (0) 7947 507866<br />Phone: +44 (0)1273
			685913<br /> email: <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a></font></font>
			<p><font face="Arial, Helvetica, sans-serif" size="2"><font size="1">@nti copyright
			- information for action - copy and distribute!</font></font></p>



</div>




<div class='mainBar_right'>

		<?php

		//	Latest Issue Summary
		require "../inc/currentIssueSummary.php";

		?>

</div>






<div class="footer">

		<?php

			//	Include Footer
			require "../inc/footer.php";

		?>

</div>








</div>




</body>
</html>




