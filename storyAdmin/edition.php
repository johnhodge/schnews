<?php
/* MAIN EDITION VISUALISER

AUTHOR: ANDREW WINTERBOTTOM support@tendrousbeastie.com www.tendrousbeastie.com
LICENSING: GPL V2.
COMPATIBILITY: PHP4+ MYSQL 4+ XHTML

*/


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';
require '../storyAdmin/storyCreationFunctions.php';

/* PAGE SPECIFIC CONSTANTS - THESE CAN BE USED TO OVERRIDE THE GLOBAL CONFIG ARS IF REQUIRED */
// define('DEBUG', 1);



/* DATA PROCESSING SECTION */

// GET ISSUE ID
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{

	$issueId	=	$_GET['issue'];

}
if (!isset($issueId))	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: issueId not set ;; exiting silently <br>";
	exit;
}




/*
 * 	If $make is set then we should actually make the file in /archive/newsXXX.php
 */
$make		=	false;
if (isset($_GET['make']) && $_GET['make'] == 1)	{

	$make 	=	true;

	//	We will need the issue number in order to make the file...
	$conn	=	mysqlConnect();
	$sql	=	"	SELECT issue
					FROM issues
					WHERE id = $issueId ";
	$rs		=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) != 1)	{

		if (DEBUG)	echo "<p>Not one result when getting issue number to make issue";
		exit;

	}
	$res		=	mysqlGetRow($rs);
	$issueNum	=	$res['issue'];


	//	Get each article from the database and create a full HTML article for it...
	$sql		=	"	SELECT id, story_number
						FROM articles
						WHERE issue = $issueId
						ORDER BY story_number ASC ";
	$rs			=	mysqlQuery($sql, $conn);

	$articles	=	array();
	while ($res		=	mysqlGetRow($rs))	{

		$story_number 	=	$res['story_number'];
		$articles[$story_number]	=	makeFullArticle($res['id']);

	}



}

$homepage		=	makeHomepage($issueId);
$target			=	"../index.php";
if (file_exists($target))	unlink ($target);
file_put_contents($target, $homepage);




/*
 * Are we making the file or just previewing it?
 */
if ($make)	{

	$target		=	"../archive/news$issueNum.php";

}	else	{

	$target 	=	"temp.php";

}






//	Make the issue and store in a local var...
$issue = makeFullIssue($issueId);

//	Write the issue to file...
if (file_exists($target))	unlink ($target);
file_put_contents($target, $issue);


//	Redirect to the issue...
$link = dirname($_SERVER['REQUEST_URI']).'/'.$target;




if ($make)	{

	foreach ($articles as $story_number => $article)	{

		$target		=	"../archive/news".$issueNum.$story_number.".php";

		//	Write the article to file...
		if (file_exists($target))	unlink ($target);
		file_put_contents($target, $article);

	}

}
header("Location: $link");

exit;

?>