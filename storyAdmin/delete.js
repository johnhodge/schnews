
function confirmSubmit()
        {
        var agree=confirm("Are you sure you want to delete this comment? It cannot be recovered once it is deleted.");
        if (agree)
            return true ;
        else
            return false ;
        }

function confirmSpam()
{
var agree=confirm("Are you sure you want to mark this as spam? Doing so will remove it from public display.");
if (agree)
    return true ;
else
    return false ;
}


function confirmDeleteFeature()
        {
        var agree=confirm("Are you sure you want to delete this feature article? It cannot be recovered once it is deleted.");
        if (agree)
            return true ;
        else
            return false ;
        }


function confirmDeleteSchmovieCategory()
{
var agree=confirm("Are you sure you want to delete this SchMOVIES category? It cannot be recovered once it is deleted.\r\n\r\nIf deleted then all SchMOVIES listed under this category will be listed under Category: None");
if (agree)
    return true ;
else
    return false ;
}



function confirmDeleteSchmovie()
{
var agree=confirm("Are you sure you want to delete this SchMOVIE? It cannot be recovered once it is deleted.");
if (agree)
    return true ;
else
    return false ;
}