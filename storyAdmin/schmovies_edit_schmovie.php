<?php
/*
 * 		DISPLAY FORM TO ADD OR EDIT A SCHMOVIE
 */



/*
 * 	=========================================================
 * 						Prepare Data
 * 	=========================================================
 */
$schmovie_id		=	$_GET['edit_schmovie'];
$schmovie_updated	=	false;









//	Edit / Add Form Submission
if (isset($_POST['submit_schmovie']))	{

	unset($_POST['submit_schmovie']);
	require 'schmovies_edit_schmovie_add.php';

}



//	If it doesn't exist, then we will need to initialise to an empty array...
if (!isset($errors))	{

	$errors		=	array();

}











/*
 * 	If we are adding a new SchMOVIE (in which case the ID will be
 * 	0) then we need to initialist the data set with empty values...
 */
if 	($schmovie_id == 0)	{

	$name 			=	'';
	$headline		=	'';
	$body_text		=	'';
	$blurb			=	'';
	$category		=	0;
	$date			=	date("Y-m-d");
	$image_small	=	'';
	$image_large	=	'';
	$image_vsmall	=	'';
	$mode			=	'add';
	$live			=	0;

}

/*
 * 	...otherwise we need to grab all the data from the database for
 *  this SchMOVIE...
 */
else	{

	//	Do the database lookup...
	$conn		=	mysqlConnect();
	$sql		=	" 	SELECT *
						FROM schmovies
						WHERE id = $schmovie_id ";
	$rs			=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) != 1) {

		if (DEBUG) 	echo "<div style='color:red'>Not one result returned from SQL lookup - schmovies_edit_schmovie.php</div>";
		exit;

	}
	$schmovie	=	mysqlGetRow($rs);
	mysql_close($conn);

	//	Populate the data set...
	$name 			=	stripslashes($schmovie['name']);
	$headline		=	stripslashes($schmovie['headline']);
	$body_text		=	stripslashes($schmovie['body_text']);
	$blurb			=	stripslashes($schmovie['blurb']);
	$category		=	$schmovie['category'];
	$date			=	$schmovie['date'];
	$image_small	=	stripslashes($schmovie['image_small']);
	$image_large	=	stripslashes($schmovie['image_large']);
	$image_vsmall	=	stripslashes($schmovie['image_vsmall']);
	$mode			=	'edit';
	$live			=	$schmovie['live'];
	if ($live != 1) $live = 0;

	//	Are the images correct and locatable?
	$image_small_found		=	false;
	$image_large_found		=	false;
	$image_vsmall_found		=	false;
	if (process_schmovie_image_link($image_small) != false)		$image_small_found		=	true;
	if (process_schmovie_image_link($image_vsmall) != false)	$image_vsmall_found		=	true;
	if (process_schmovie_image_link($image_large) != false)		$image_large_found		=	true;

}












/*
 * 	If a submission has already been made then we want to override
 * 	the defaults with the POST data...
 */
$possible_post_entries		=	array(

	'name',
	'headline',
	'body_text',
	'blurb',
	'category',
	'image_small',
	'image_vsmall',
	'image_large',
	'live'

);
foreach ($possible_post_entries 	as $pos_entry)	{

	if (isset($_POST[$pos_entry]))			$$pos_entry			=	$_POST[$pos_entry];

}
if (	isset($_POST['date_day'])		&&
		isset($_POST['date_month'])		&&
		isset($_POST['date_year'])		)	{

				$date		=	$_POST['date_year'].'-'.$_POST['date_month'].'-'.$_POST['date_day'];

	}











//	Cache the cateogries
$categories 	=	get_schmovies_categories();










/*
 * 	=========================================================
 * 						Display Output
 * 	=========================================================
 */
if (!$schmovie_updated)	{



	$output		=	array();		//	Output buffer

	//	Cancel Edit and back to main SchMOVIES menu...
	$output[]	=	"<h3><a href='index.php?schmovies=1'>Back to main SchMOVIES list</a></h3>";
	$output[]	=	"<p>............</p>";

	//	Title
	if ($mode == 'add')	{

		$output[]		=	"<h3>Adding New SchMOVIE Item</h3>";

	}	else	{

		$output[]		=	"<h3>Editing SchMOVIE Item</h3>";

	}







	//	Draw Form
	$output[]		=	"<form method='POST' action='index.php?schmovies=1&edit_schmovie=$schmovie_id'>";
	$output[]		=	"<table>";

	//	SchMOVIE Name
	$output[]		=	show_schmovies_error_message('name', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>SchMOVIE Name:</td>";
	$output[]		=	"<td><input type='text' name='name' value='$name' class='schmovies_form_element' /></td>";
	$output[]		=	"</tr>";

	//	SchMOVIE Headline
	$output[]		=	show_schmovies_error_message('headline', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>SchMOVIE Headline:</td>";
	$output[]		=	"<td><input type='text' name='headline' value='$headline' class='schmovies_form_element' /></td>";
	$output[]		=	"</tr>";

	//	SchMOVIE Body Text
	$output[]		=	show_schmovies_error_message('body_text', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Main Text Description:</td>";
	$output[]		=	"<td><textarea name='body_text' class='schmovies_form_element' style='height: 300px'>$body_text</textarea></td>";
	$output[]		=	"</tr>";

	//	SchMOVIE Blurb
	$output[]		=	show_schmovies_error_message('blurb', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Blurb:</td>";
	$output[]		=	"<td><input type='text' name='blurb' value='$blurb' class='schmovies_form_element' /></td>";
	$output[]		=	"</tr>";

	//	SchMOVIE Category
	$output[]		=	show_schmovies_error_message('category', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Category:</td>";
	$output[]		=	"<td><select name='category'>";
	foreach ($categories as $cat_id => $cat)	{

		$selected		=	'';
		if ($category == $cat_id || ( $category == 0 && $cat == 'None'))		$selected	=	' selected ';
		$output[]		=	"<option value='$cat_id' $selected >".stripslashes($cat).'</option>';

	}
	$output[]		=	"</select></td>";
	$output[]		=	"</tr>";

	//	SchMOVIE Date
	$output[]		=	show_schmovies_error_message('date', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Date:</td>";
	$output[]		=	"<td>".makeDateSelector('date', $date)."</td>";
	$output[]		=	"</tr>";

	//	Very Small Image
	$output[]		=	show_schmovies_error_message('image_vsmall', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Image (Very Small):</td>";
	$output[]		=	"<td><input type='text' name='image_vsmall' value='$image_vsmall' class='schmovies_form_element' /></td>";
	if ($schmovie_id	!= 	0)	{

			if ($image_vsmall_found)	{

				$output[]	=	"<td><img src='../images/tick.gif' /></td>";

			}	else	{

				$output[]	=	"<td><img src='../images/cross.gif' /></td>";

			}

	}
	$output[]		=	"</tr>";

	//	Small Image
	$output[]		=	show_schmovies_error_message('image_small', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Image (Small):</td>";
	$output[]		=	"<td><input type='text' name='image_small' value='$image_small' class='schmovies_form_element' /></td>";
	if ($schmovie_id	!= 	0)	{

			if ($image_small_found)	{

				$output[]	=	"<td><img src='../images/tick.gif' /></td>";

			}	else	{

				$output[]	=	"<td><img src='../images/cross.gif' /></td>";

			}

	}
	$output[]		=	"</tr>";

	//	Large Image
	$output[]		=	show_schmovies_error_message('image_large', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Image (Large):</td>";
	$output[]		=	"<td><input type='text' name='image_large' value='$image_large' class='schmovies_form_element' /></td>";
	if ($schmovie_id	!= 	0)	{

			if ($image_large_found)	{

				$output[]	=	"<td><img src='../images/tick.gif' /></td>";

			}	else	{

				$output[]	=	"<td><img src='../images/cross.gif' /></td>";

			}

	}
	$output[]		=	"</tr>";

	//	Live
	$output[]		=	show_schmovies_error_message('live', $errors);
	$output[]		=	"<tr>";
	$output[]		=	"<td>Live (Show on Website):</td>";
	$output[]		=	"<td>";
	$output[]		=	"<select name='live'>";
	$no_selected	=	'';
	$yes_selected	=	'';
	if ($live	==	1)	$yes_selected = ' selected ';
	else	$no_selected = ' selected ';
	$output[]		=	"<option value='0' $no_selected > No </option>";
	$output[]		=	"<option value='1' $yes_selected > Yes </option>";
	$output[]		=	"</select>";
	$output[]		=	"</td></tr>";

	//	Submit Form
	$output[]		=	"<tr>";
	$output[]		=	"<td></td>";
	if ($schmovie_id == 0)	$submit_value		=	'Save New SchMOVIE';
	else					$submit_value		=	'Save Changes to SchMOVIE';
	$output[]		=	"<td align='right'><input type='submit' name='submit_schmovie' value='$submit_value' /></td>";

	//	Cancel Editing
	$output[]		=	"<td>&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp; <b><a href='index.php?schmovies=1'>Cancel edit and go back to main SchMOVIES list</a></b></td>";
	$output[]		=	"</tr>";


	$output[]		=	"</table>";

	$output[]		=	"</form>";



	//	Flush output buffer to screen
	echo implode("\r\n", $output);

}


?>