<?php

// 	CHECK FOR LOGIN
if (!$_SESSION[SES.'loggedin']) exit;


// 	NEW DB CONNECTION
$conn	=	mysqlConnect();






/*	===============================================================
			TEST POST VARS */


/*	ISSUE #
NOTE: THIS SHOULD BE A NUMBER */
if (isset($_POST['issue']) && $_POST['issue'] != '')	{

		$issue		=	$_POST['issue'];
		if (!is_numeric($issue))	{

				$errors['issue']		=	"The issue number was not a number";

		}

}	else {

	$errors['issue']		=	"No issue number was set";

}







/* DATE */
if (!$date	=	makeDateFromPostData('date', $_POST))	{

	$errors['date'] 		=	"The date wasn't set correctly";

}



/*	HEADLINE */
if (isset($_POST['headline']) && $_POST['headline'] != '')	{

		$headline		=	cleanInput($_POST['headline']);

}	else {

	$errors['headline']		=	"No headline was set";

}


/* 	IS SPECIAL */
if (isset($_POST['is_special']) && $_POST['is_special'] == 'on')	{

		$is_special 	=	'1';

}	else	{

		$is_special 	=	'0';

}



//	No PDF
if (isset($_POST['no_pdf']) && $_POST['no_pdf'] == 'on')	{

		$no_pdf			=	'1';

}	else 	{

		$no_pdf			=	'0';

}



//	MAIN STORY
if (isset($_POST['main_story']) && is_numeric($_POST['main_story']))	{

		$main_story		=	$_POST['main_story'];

}	else	{

		$main_story		=	0;

}


/*	SUMMARY */
if (isset($_POST['summary']) && $_POST['summary'] != '')	{

		$summary		=	cleanInput($_POST['summary']);

}	else {

	$errors['summary']		=	"No summary was set";

}




/*	RECENT STORY TEXT */
if (isset($_POST['recentStoryText']) && $_POST['recentStoryText'] != '')	{

		$recentStoryText		=	cleanInput($_POST['recentStoryText']);

}	else {

	$errors['recentStoryText']		=	"No Recent Story Text was set";

}




/*	WAKE UP TEXT */
if (isset($_POST['wakeUpText']) && $_POST['wakeUpText'] != '')	{

		$wakeUpText		=	cleanInput($_POST['wakeUpText']);

}	else {

	$errors['wakeUpText']		=	"No Wake Up!!! Text was set";

}



/*	DISCLAIMER TEXT */
if (isset($_POST['disclaimer']) && $_POST['disclaimer'] != '')	{

		$disclaimer		=	cleanInput($_POST['disclaimer']);

}	else {

	$errors['disclaimer']		=	"No disclaimer was set";

}



/*	PAGE TITLE TEXT */
if (isset($_POST['pageTitleText']) && $_POST['pageTitleText'] != '')	{

		$pageTitleText		=	cleanInput($_POST['pageTitleText']);

}	else {

	$errors['pageTitleText']		=	"No page title was set";

}



/*	MAIN GRAPHIC V-SMALL */
if (isset($_POST['mainGraphicVSmall']) && $_POST['mainGraphicVSmall'] != '')	{

		$mainGraphicVSmall		=	cleanInput($_POST['mainGraphicVSmall']);

}	else {

		$mainGraphicVSmall		=	'';

}

/*	MAIN GRAPHIC SMALL */
if (isset($_POST['mainGraphicSmall']) && $_POST['mainGraphicSmall'] != '')	{

		$mainGraphicSmall		=	cleanInput($_POST['mainGraphicSmall']);

}	else {

		$mainGraphicSmall		=	'';

}

/*	MAIN GRAPHIC LARGE */
if (isset($_POST['mainGraphicLarge']) && $_POST['mainGraphicLarge'] != '')	{

		$mainGraphicLarge		=	cleanInput($_POST['mainGraphicLarge']);

}	else {

		$mainGraphicLarge		=	'';

}

/*	MAIN GRAPHIC STORY */
if (isset($_POST['mainGraphicStory']) && is_numeric($_POST['mainGraphicStory']))	{

		$mainGraphicStory		=	$_POST['mainGraphicStory'];

}	else {

		$mainGraphicStory		=	0;

}


/*	MAIN GRAPHIC ALT TEXT */
if (isset($_POST['mainGraphicAlt']) && $_POST['mainGraphicAlt'] != '')	{

		$mainGraphicAlt		=	cleanInput($_POST['mainGraphicAlt']);

}	else {

		$mainGraphicAlt		=	'';

}


/*	BANNER GRAPHIC */
if (isset($_POST['bannerGraphic']) && $_POST['bannerGraphic'] != '')	{

		$bannerGraphic		=	cleanInput($_POST['bannerGraphic']);

}	else {

	$errors['bannerGraphic']		=	"No banner graphic was set";

}

/*	BANNER GRAPHIC LINK */
if (isset($_POST['bannerGraphicLink']) && $_POST['bannerGraphicLink'] != '')	{

		$bannerGraphicLink		=	cleanInput($_POST['bannerGraphicLink']);

}	else {

	$errors['bannerGraphicLink']		=	"No large main graphic was set";

}


/*	BANNER GRAPHIC ALT TEXT */
if (isset($_POST['bannerGraphicAlt']) && $_POST['bannerGraphicAlt'] != '')	{

		$bannerGraphicAlt		=	cleanInput($_POST['bannerGraphicAlt']);

}	else {

		$bannerGraphicAlt		=	'';

}


/* =======================================================
			ERROR HANDLING */

/* IF WE HAVE ANY ERRORS DETECTED THEN WE NEED TO CALL BACK TO THE
EDIT SCRIPT AND THEN EXIT THE PROGRAM */
if (isset($errors)) {

	require "editIssue.php";
	exit;

}






/*	========================================================
			DATABASE UPDATE */

/* IF ISSUEID IS > 0 THEN WE ARE UPDATING AN EXISTING STORY */
if ($issueId > 0)	{

	$sql		=	"UPDATE issues SET
					issue		= '$issue',
					date		= '$date',
					headline 	= '$headline',
					is_special 	= '$is_special',
					summary		= '$summary',
					recent_story_text = '$recentStoryText',
					wake_up_text = '$wakeUpText',
					disclaimer	= '$disclaimer',
					page_title_text = '$pageTitleText',
					main_graphic_small = '$mainGraphicSmall',
					main_graphic_vsmall = '$mainGraphicVSmall',
					main_graphic_large = '$mainGraphicLarge',
					main_graphic_story = '$mainGraphicStory',
					main_graphic_alt = '$mainGraphicAlt',
					banner_graphic	=	'$bannerGraphic',
					banner_graphic_link = '$bannerGraphicLink',
					banner_graphic_alt	= '$bannerGraphicAlt',
					no_pdf = '$no_pdf',
					main_story = '$main_story'

					WHERE
					id = $issueId";
	$rs			=	mysqlQuery($sql, $conn);




} else {


	$sql		=	"INSERT INTO issues (issue, date, headline, is_special,  summary, recent_story_text, wake_up_text, disclaimer, page_title_text, main_graphic_vsmall,  main_graphic_small, main_graphic_large, main_graphic_story, main_graphic_alt, banner_graphic, banner_graphic_link, banner_graphic_alt, no_pdf, main_story)
					VALUES
					('$issue', '$date', '$headline', '$is_special', '$summary', '$recentStoryText', '$wakeUpText', '$disclaimer', '$pageTitleText', '$mainGraphicVSmall', '$mainGraphicSmall', '$mainGraphicLarge', '$mainGraphicStory', '$mainGraphicAlt', '$bannerGraphic', '$bannerGraphicLink', '$bannerGraphicAlt', '$no_pdf', '$main_story' )";
	$rs			=	mysqlQuery($sql, $conn);
	$issueId	=	mysqlGetInsertId($conn);

}




require "editIssue.php";


?>


