<?php


// 	CHECK FOR LOGIN
if (!$_SESSION[SES.'loggedin']) exit;


// 	NEW DB CONNECTION
$conn	=	mysqlConnect();









/* =====================================================
		GET ARTICLE DETAILS */

/* 	ARTICLEID WILL = 0 IF IT IS A NEW ARTICLE BEING ADDED, ELSE
IT WILL BE A POSITIVE INTEGER CORRESPONDING TO THE DB ARTICLE ID */
if ($articleId != 0)	{

	/* GRAB ALL THE ARTICLE DETAILS */
	$sql		=	"SELECT * FROM articles WHERE id = $articleId";
	$rs			=	mysqlQuery($sql, $conn);

	/* IF THERE AREN'T ONLY 1 RESULTS THEN WE HAVE A PROBLEM AND
	HAVE TO EXIT FOR DB INTEGRITY REASONS */
	if (mysqlNumRows($rs, $conn) != 1) {

		if (DEBUG) echo "<br>EditArticle.php :: not one result from ArticleId ;; exiting script for DB integrity purposes<br>";
		exit;
	}

	/* STORE ARTICLE DETAILS IN TEMP VARS */
	$res		=	mysqlGetRow($rs);
	$storyNum	=	$res['story_number'];
	$issueId	=	$res['issue'];
	$pretitle	=	$res['pretitle'];
	$headline	=	stripslashes($res['headline']);
	$story		=	stripslashes($res['story']);
	$rssText	=	stripslashes($res['rss_text']);

	unset($keywords);
	$sql_k		=	"SELECT keyword FROM keywords WHERE article_id = $articleId ";
	$rs_k		=	mysqlQuery($sql_k, $conn);
	while ($res_k = mysqlGetRow($rs_k)) {

			$keywords[]		=	stripslashes($res_k['keyword']);

	}


	if (isset($keywords))	$keywords	=	implode(', ', $keywords);
		else $keywords = '';

/* THIS CLAUSE IS ONLY RUN IF WE ARE ADDING A NEW STORY */
} else {

	$pretitle	=	'';
	$headline	=	'';
	$story		=	'';
	$keywords	=	'';
	$rssText	=	'';
	$issueId	=	$issue;

	$sql		=	"SELECT max(story_number) as num FROM articles WHERE issue = $issueId";
	$rs			=	mysqlQuery($sql,$conn);
	$res		=	mysqlGetRow($rs);
	$storyNum	=	$res['num'] + 1;
}




/* GET THE CURRENT KEYWORDS AS AN ARRAY (IF THERE ARE ANY) */
if ($articleId != 0)	{
		$rs_kw		=	mysqlQuery(" SELECT keyword FROM keywords WHERE article_id = $articleId", $conn);
		$currentKeywords = array();
		while ($res_kw = mysqlGetRow($rs_kw))	{

				$currentKeywords[]		=	$res_kw['keyword'];

		}

}



/* IF THIS IS BEING RUN AFTER A FAILED ATTEMPT TO UPDATE
A STORY THEN WE NEED TO UPDATE THE TEMP VARS TO HOLD THE
ERRONEOUS DATA */
if (isset($errors))	{

	$pretitle	=	stripslashes($_POST['pretitle']);
	$headline	=	stripslashes($_POST['headline']);
	$story		=	stripslashes($_POST['story']);
	$keywords	=	stripslashes($_POST['keywords']);
	$rssText	=	stripslashes($_POST['rss_text']);

}







/* =================================================
		GET ISSUE DETAILS */

/* GRAB ALL ISSUE DETAILS FROM DB */
$sql		=	"SELECT * FROM issues WHERE id = $issueId";
$rs			=	mysqlQuery($sql, $conn);

/* IF THERE AREN'T ONLY 1 RESULTS THEN WE HAVE A PROBLEM AND
HAVE TO EXIT FOR DB INTEGRITY REASONS */
if (mysqlNumRows($rs, $conn) != 1) {

	if (DEBUG) echo "<br>EditArticle.php :: not one result from Issue ;; exiting script for DB integrity purposes<br>";
	exit;
}

/* STICK ALL THE ISSUE DETAILS IN TEMP VARS */
$res			=	mysqlGetRow($rs);
$issue			=	$res['issue'];
$issueHeadline	=	$res['headline'];
$issueSummary	=	$res['summary'];
$issueDate		=	$res['date'];





if (isset($_GET['instructions']) && $_GET['instructions'] == '1')		$_SESSION['instructions'] = 1;
if (isset($_GET['instructions']) && $_GET['instructions'] == '0')		$_SESSION['instructions'] = 0;
if (!isset($_SESSION['instructions']))									$_SESSION['instructions'] = 0;







if ($articleId == 0)	{
	$submit			=	'Add';
	$titlePhrase	=	'Adding';
}	else	{
	$titlePhrase	=	"Editing";
	$submit			=	'Update';
}


/* 	=================================================
				OUTPUT */

/* DRAW TITLE */
echo 	"<br /><div align=\"right\"><a href=\"index.php?showIssue=$issueId\">Back to main list of stories</a></div>";

echo 	"<h2>$titlePhrase story# $storyNum of Issue $issue of ".makeHumanDate($issueDate).'</h2>';
echo 	"<p>............</p>";

echo 	"<h3><b>$issue - $issueHeadline</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>".makeHumanDate($issueDate, 'words')."</i></h3>";
echo 	"<p>............</p>";



		echo "<p><a id='toggleInstructions1' class='javaLink'>Toggle Instructions</a></p>";

		echo "<div id='instructions' class='instructions'>";
			echo "<p><h3>Instructions</h3></p>";
			echo "<p>This page allows you to add or edit a story for issue $issue.</p>";
			echo "<p>You can use the editor to complete the main story content. The formatting applied in this editor will be reflected on the actual article onsite.</p>";
			echo "<p>&nbsp;</p>";
			echo "<p>There is an optional pretitle field. Click the '<b>Toggle Pretitle</b>' link to enable it. The pretitle shows above the main title, and is useful for stories with a special theme. See an example <a href='gfx/example_PreTitle.gif' target='_BLANK'>here</a>.</p>";
			echo "<p>To add some borders on the headlines simply add three hyphens either side of the headline. In the example <a href='gfx/example_PreTitle.gif' target='_BLANK'>here</a> the pretitle was entered as \"<b>--- G20 Summit Eyewitness Report ---</b>\"</p>";
		echo "</div>";
		echo "<p>............</p>";








?>

<!--DRAW FORM -->
<form method="post" action="index.php?updateArticle=<?php echo $articleId; ?>&issue=<?php echo $issueId; ?>&showIssue=<?php echo $issueId; ?>" >


<table>

<tr><td><a class='javaLink' id='togglePreTitle'>Toggle Pretitle</a></td></tr>




<!--	PRETITLE -->
<tr id='pretitle'><td width="10%" align="right"><b>Pre-Title ::</b> </td><td>
<?php
	if (isset($errors['pretitle'])) 	{

			?> <div class="errorMessage"><?php echo $errors['pretitle']; ?></div>

<?php	}	?>
<input style="width: 560px" id='preTitleInput' type="text" name="pretitle" value="<?php echo $pretitle; ?>" /></td>

<td><a href='gfx/example_PreTitle.gif' target='_blank'>What is This?</a></td>

</tr><tr><td><br /></td></tr><tr>




<!--	HEADLINE -->
<td width=\"10%\" align="right" ><b>Headline ::</b> </td><td>
<?php
	if (isset($errors['headline'])) {

			?>	<div class="errorMessage"><?php $errors['headline']; ?></div>

	<?php }	?>

<input style="width: 560px" type="text" name="headline" value="<?php echo $headline; ?>" /></td>

</tr><tr><td><br /></td></tr><tr>





<!-- STORY -->
<td align="right" valign="top"><b>Story ::</b> </td><td>
<?php
	if (isset($errors['story']))	{

		?>	<div class="errorMessage"><?php echo $errors['story']; ?></div>

	<?php } ?>


<textarea name='story'><?php echo $story; ?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'story',

			{

				<?php echo $editArticle_ckeditor_settings; ?>

			}

			 );
</script>
<td valign="top" style="padding: 20px; border-right: 1px solid #dddddd; color:#777777" ><?php echo nl2br($editArticle_storyHelp); ?></td>

</tr><tr><td><br /></td></tr><tr>






<!-- KEYWORDS -->
<td width="10%" align="right"><b>Keywords ::</b> </td><td>

<?php
	if (isset($errors['keywords']))	{

			?>	<div class="errorMessage"><?php $errors['keywords']; ?></div>

	<?php } ?>

<input style="width: 560px" type="text" name="keywords" value="<?php echo $keywords; ?>" />
</td>
<td valign="top" style="padding: 20px; border-right: 1px solid #dddddd; color:#777777" >
			Comma separated list (e.g. "gaza, palestine, israel")
			</td>

</tr>
<tr>
<td></td>
<td><a href='tutorials/keywords.html' target="_blank">What is This?</a></td>
</tr>

<tr><td><br /></td></tr><tr>
</tr><tr><td><br /></td></tr><tr>




<!-- RSS TEXT -->
<td width="10%" align="right"><b>RSS Text ::</b> </td><td>

<?php
	if (isset($errors['rss_text']))		{

		?>	<div class="errorMessage"><?php echo $errors['rss_text']; ?></div>

	<?php } ?>
<textarea style='width:560px; height:100px' name='rss_text'><?php echo $rssText; ?></textarea>

</td>
<td valign="top" style="padding: 20px; border-right: 1px solid #dddddd; color:#777777" >
			Any return breaks and HTML tags will be stripped out.
			</td>
</tr><tr><td><br /></td></tr><tr>

<!--	SUBMIT AND CANCEL BUTTONS -->
<td>

<?php
	if ($articleId != 0)	{

		?>	<input type="submit" onClick="return confirmSubmit()" name="delete" value="Delete Story" />
	<?php	}	?>

</td><td align="right">
<input type="submit" value="Back to Main List" name="cancel" />
&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;
<input type="submit" value="<?php echo $submit; ?>" name="submit" /></td>


</tr></table>
</form>

<?php


echo "<p>______________________________________________</p>";

if ($articleId != 0)	{


		echo "<p><b>Keyword Results...</b>";
		echo "<a name='keywords'>&nbsp;</a>";


				$settings 	=	getSettings();

				echo "<p><a id='toggleInstructions2' class='javaLink'>Toggle Instructions</a></p>";

				echo "<div id='instructions' class='instructions'>";
					echo "<p><h3>Instructions</h3></p>";
					echo "<p>This list shows the current keywords for this article.</p>";
					echo "<p>It show the number of times the keyword occurs in other stories. If the keyword occurs more than {$settings['numKeywords']} times then this story will link to common stories that share that keyword. </p>";
				echo "</div>";
				echo "<p>............</p>";







		foreach ($currentKeywords as $keyword)	{

				echo "<p><b>$keyword</b> :: ";
				$hits = getNumKeywordOccurances($keyword);
				$hits--;
				switch ($hits)	{

					default:
						echo "$hits hits</p>";
						break;

					case '1':
						echo "1 hit</p>";
						break;

					case '0':
						echo "no hits</p>";
						break;

				}


		}


}







function freeRTE_Preload($content) {
	// Strip newline characters.
	$content = str_replace(chr(10), " ", $content);
	$content = str_replace(chr(13), " ", $content);
	// Replace single quotes.
	$content = str_replace(chr(145), chr(39), $content);
	$content = str_replace(chr(146), chr(39), $content);

	$content = str_replace('<img', '<image', $content);
	// Return the result.
	return $content;
}
