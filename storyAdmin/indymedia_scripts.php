<?php
session_start();

require "functions.php";

//	Ensure login...
if (!$_SESSION[SES.'loggedin']) exit;




//	Process the GET var to determine which operation we are 
//	being asked to do...
$mode		=	false;
if (isset($_GET['make_html']))	{
	
	$mode		=	'make_html';
	$id			=	$_GET['make_html'];
	if (!is_numeric($id))	{
		
		if (DEBUG) echo "<b style='color:red'>ID for make_html is not a number</a>";
		exit;
		
	}
	
}


//	Execute main flow control....
switch ($mode)	{
	
	case 'make_html':
		echo im_make_html($id);
		break;
	
	
}








//	================================================================================
//			PAGE SPECIFIC FUNCTIONS


function im_make_html($id)	{
	
	//	Test input...
	if (!is_numeric($id))	return false;
	
	$conn	= 	mysqlConnect();
		$sql	=	"	SELECT pretitle, headline, story
					FROM articles
					WHERE id = $id ";
	$rs		=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) != 1)	return false;
	$res	=	mysqlGetRow($rs);
	
	
	$headline	=	trim(stripslashes($res['headline']));
	$story		=	trim(stripslashes($res['story']));
	$pretitle	=	false;
	if (trim($res['pretitle']) != '')	{
		
		$pretitle		=	trim(stripslashes($res['pretitle']));
		
	}
	$story		=	scanForLinks($story);
	
	$output		=	array();
	if ($pretitle)	{
		
		$output[]	=	"<p><i>$pretitle</i></p>";
		
	}
	$output[]	=	"<p><b>$headline</b></p>";
	$output[]	=	$story;
	
	$output		=	implode("\r\n", $output);
	
	return im_process_paragraphs(htmlentities($output));
	
}


function im_process_paragraphs($text)	{
	
	//	Test inputs...
	if (!is_string($text) || trim($text) == '')	return $text;

	$text		=	str_replace(array(" \r", " \n", "\r ", "\n ", "\r", "\n", "\t"), '', $text);

	$text		=	str_replace(htmlentities(' </p>'),htmlentities('</p>'), $text);
	$text		=	str_replace("&amp;#8239;", ' ', $text);
	
	$find		=	array(htmlentities('<strong>'), htmlentities('<em>'), htmlentities('</strong>'), htmlentities('</em>'), '&amp;' );
	$replace	=	array(htmlentities('<b>'), htmlentities('<i>'), htmlentities('</b>'), htmlentities('</i>'), '&');
	$text		=	str_replace($find, $replace, $text);
	
	$find		=	array(htmlentities("<p>"), htmlentities("</p>"));
	$replace	=	array("<br />".htmlentities("<p>"), htmlentities("</p>")."<br />");
	$text		=	str_replace($find, $replace, $text);
	
	return $text;
	
}

