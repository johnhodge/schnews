<?php

// 	CHECK FOR LOGIN
if (!$_SESSION[SES.'loggedin']) exit;


// 	NEW DB CONNECTION
$conn	=	mysqlConnect();





/*	===============================================================
			TEST POST VARS */


/*	HEADLINE */
if (isset($_POST['headline']) && $_POST['headline'] != '')	{

		$headline		=	cleanInput($_POST['headline']);

}	else {

	$errors['headline']		=	"No headline was set";

}



/*	PRETITLE */
if (isset($_POST['pretitle']) && $_POST['pretitle'] != '')	{

		$pretitle		=	cleanInput($_POST['pretitle']);

}	else {

		$pretitle		=	'';

}



/*	STORY */
if (isset($_POST['story']) && $_POST['story'] != '')	{

	/*
		$story		=	cleaninput($_POST['story']);
		$story		=	str_replace('<p>', '', $story);
		$story		=	str_replace('</p><br />', "<br />", $story);
		$story		=	str_replace('</p>', "<br /><br />", $story);
		$story		=	str_replace("\\r", '', $story);
		$story		=	str_replace("\\n", '', $story);
		$story		=	str_replace("<br /><br /><br />", '<br /><br />', $story);
		$story		=	cleaninput($_POST['story']);
	*/
	$story		=	cleaninput($_POST['story']);
	$story		=	str_replace("</p>", " </p>", $story);
	$story		=	str_replace("</P>", " </p>", $story);


}	else {

	$errors['story']		=	"No story was set";

}



/*	KEYWORDS */
if (isset($_POST['keywords']) && $_POST['keywords'] != '')	{

		$keywords		=	explode(',', strtolower(cleanInput($_POST['keywords'])));

}	else {

		$keywords		=	'';

}


/*	RSS TEXT */
if (isset($_POST['rss_text']) && $_POST['rss_text'] != '')	{

		$rssText		=	strip_tags(cleanInput($_POST['rss_text']));
		$rssText		=	str_replace("\\r", ' ', $rssText);
		$rssText		=	str_replace("\\n", ' ', $rssText);
		$rssText		=	str_replace("  ", ' ', $rssText);

}	else {

		$rssText		=	'';

}







/* =======================================================
			ERROR HANDLING */

/* IF WE HAVE ANY ERRORS DETECTED THEN WE NEED TO CALL BACK TO THE
EDIT SCRIPT AND THEN EXIT THE PROGRAM */
if (isset($errors)) {

	require "editArticle.php";
	exit;

}








/*	========================================================
			DATABASE UPDATE */

/* IF ARTICLEID IS > 0 THEN WE ARE UPDATING AN EXISTING STORY */
if ($articleId > 0)	{

	$sql		=	"UPDATE articles SET
					pretitle	= '$pretitle',
					headline 	= '$headline',
					story		= '$story',
					rss_text	= '$rssText'
					WHERE
					id = $articleId";
	$rs			=	mysqlQuery($sql, $conn);




} else {

	$sql		=	"SELECT max(story_number) as num FROM articles WHERE issue = $issue";
	$rs			=	mysqlQuery($sql,$conn);
	$res		=	mysqlGetRow($rs);
	$storyNum	=	$res['num'] + 1;

	$sql		=	"INSERT INTO articles (pretitle, headline, story, issue, story_number, rss_text)
					VALUES
					('$pretitle', '$headline', '$story', $issue, $storyNum, '$rssText' )";
	$rs			=	mysqlQuery($sql, $conn);
	$articleId	=	mysqlGetInsertId($conn);

}

mysqlQuery("DELETE FROM keywords WHERE article_id = $articleId", $conn);

if (is_array($keywords))	{

	foreach ($keywords as $keyword)		{

			$keyword	=	trim(mysql_escape_string($keyword));
			$sql		=	" 	INSERT INTO keywords ( article_id, keyword)
									VALUES
								( $articleId, '$keyword' ) ";
			$rs			=	mysqlQuery($sql, $conn);

	}

}




require "editArticle.php";


?>
