<?php
/*
 * 		DISPLAY LIST OF SCHMOVIES AND OPTIONS TO EDIT OR ADD NEW ONES
 */



//	Add new SchMOVIE Link
echo "<h3><a href='index.php?schmovies=1&edit_schmovie=0'>Add New SchMOVIE</a></h3>";
echo "<p>............</p>";







/*
 * 	=============================================================
 *
 *		 				GRAB AND PREPARE DATA
 *
 * 	=============================================================
 */


//	Are we deleting an item...
if (isset($_GET['delete_schmovie']) && is_numeric($_GET['delete_schmovie']))	{

	$delete_id		=	$_GET['delete_schmovie'];
	$conn			=	mysqlConnect();

	/*
	 * 	Get SchMOVIE name
	 */
	$sql			=	" SELECT name FROM schmovies WHERE id = $delete_id ";
	$rs				=	mysqlQuery($sql, $conn);
	$res			=	mysqlGetRow($rs);
	$delete_name	=	stripslashes($res['name']);


	/*
	 * 	Delete SchMOVIE
	 */
	$sql			=	" DELETE FROM schmovies WHERE id = $delete_id";
	$rs				=	mysqlQuery($sql, $conn);


	/*
	 * 	Close DB and display a notice on screen
	 */
	mysql_close($conn);
	$text			=	"SchMOVIE <b>'$delete_name'</b> has been deleted";
	echo	"<h2 style='color:red'>$text</h2>";

}



//	Grab Data for SchMOVIES items...
$conn		=	mysqlConnect();
$sql		=	"	SELECT *
					FROM schmovies
					ORDER BY date DESC, id DESC
				";
$rs			=	mysqlQuery($sql, $conn);



//	Cache Categories...
$categories		=	get_schmovies_categories();




if (!isset($_SESSION['schmovies_show']))		$_SESSION['schmovies_show']		=	0;
if (isset($_GET['show_item']) && is_numeric($_GET['show_item']))	{

	$_SESSION['schmovies_show']		=	$_GET['show_item'];

}














/*
 * 	=============================================================
 *
 *		 					DISPLAY DATA
 *
 * 	=============================================================
 */

$output			=	array();			//	Output buffer

//	Title
$output[]		=	"<div class='schmovies_title'>This page lists the SchMOVIES items in the order they'll appear on the SchNEWS website.<br /><br />Any items with a <span style='border: 2px dotted red'>dotted red border</span> are not yet live and so will not appear on the website. This can be changed by editing the item.</div>";


while	($item		=	mysqlGetRow($rs))	{

	$name		=	stripslashes($item['name']);
	$headline	=	stripslashes($item['headline']);
	$date		=	$item['date'];
	$id			=	$item['id'];
	$cat_id		=	$item['category'];
	$live		=	$item['live'];

	if ($_SESSION['schmovies_show'] == $item['id'])	{

		$open_close_src		=	'../pap/close.gif';
		$open_close_target	=	'index.php?schmovies=1&show_item=0';
		$show_full			=	true;

	}	else	{

		$open_close_src		=	'../pap/open.gif';
		$open_close_target	=	'index.php?schmovies=1&show_item='.$item['id'];
		$show_full			=	false;

	}

	/*
	 * 	Open / Close Button
	 */
	$output[]	=	"<div class='schmovies_open_close'>";
	$output[]	=	"<a href='$open_close_target'><img src='$open_close_src' /></a>";
	$output[]	=	'</div>';

	$class		=	'schmovies_item';
	if (!$live)	$class	=	'schmovies_item_red';
	$output[]	=	"<div class='$class'>";


	/*
	 * 	Edit / Delete Button
	 */
	if ($show_full)	{

		$output[]	=	"<div class='schmovies_edit_delete'>";
		$output[]	=	"<a href='index.php?schmovies=1&edit_schmovie=$id'>Edit SchMOVIE</a>";
		$output[]	=	"&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp; ";
		$output[]	=	"<a href='index.php?schmovies=1&delete_schmovie=$id' onClick=\"return confirmDeleteSchmovie()\">Delete SchMOVIE</a>";
		$output[]	=	'</div>';

	}


	/*
	 * 	Display Titles and Dates
	 */
	$output[]	=	"<div class='schmovies_name'>$name</div>";
	$output[]	=	"<div class='schmovies_headline'>$headline</div>";
	$output[]	=	"<div class='schmovies_date'>".makeHumanDate($date, 'words')."</div>";



	/*
	 * 	Display the full SchMOVIE Items...
	 */
	if ($show_full)	{

		$output[]		=	"<div class='schmovies_full_item_container'>";
		$output[]		=	make_schmovie_item($id);
		$output[]		=	'</div>';

	}



	/*
	 * 	Display Category
	 */
	if ($show_full)	{

		$output[]	=	"<div class='schmovies_category'>";
		$output[]	=	"<b>Category:</b> ".get_schmovies_category($cat_id);
		$output[]	=	'</div>';

	}



	$output[]	=	"</div>";

}

echo implode("\r\n", $output);

//	Edit Categories Link
echo "<p>............</p>";
echo "<h3><a href='index.php?schmovies=1&edit_schmovies_categories=1'>Edit SchMOVIES Categories</a></h3>";



?>