<?php

// 	CHECK FOR LOGIN
if (!$_SESSION[SES.'loggedin']) exit;


// 	NEW DB CONNECTION
$conn	=	mysqlConnect();







/*	=========================================================
						DATA PREP */


/* IF ISSIEID = 0 THEN WE ARE ADDING A NEW ISSUE, ELSE
WE ARE UPDATING AN EXISTING ONE. */
if ($issueId != '0')	{

		/* GRAB THE ENTIRE ISSUE FROM THE DB */
		$sql		=	"SELECT * FROM issues WHERE id = $issueId";
		$rs			=	mysqlQuery($sql, $conn);

		/* IF WE DON'T HAVE ONE RESULT THE WE SHOULD BAIL */
		if (mysqlNumRows($rs, $conn) != 1) {

				if (DEBUG) echo "<br>EditIssue.php :: not one result from ArticleId ;; exiting script for DB integrity purposes<br>";
				exit;
		}

		$res		=	mysqlGetRow($rs);

		$issue				=	$res['issue'];
		$headline			=	stripslashes($res['headline']);
		$isSpecial			=	$res['is_special'];
		$summary			=	stripslashes($res['summary']);
		$date				=	$res['date'];
		$recentStoryText	=	stripslashes($res['recent_story_text']);
		$wakeUpText			=	stripslashes($res['wake_up_text']);
		$disclaimer			=	stripslashes($res['disclaimer']);
		$pageTitleText		=	stripslashes($res['page_title_text']);
		$mainGraphicLarge	=	stripslashes($res['main_graphic_large']);
		$mainGraphicSmall	=	stripslashes($res['main_graphic_small']);
		$mainGraphicVSmall	=	stripslashes($res['main_graphic_vsmall']);
		$mainGraphicStory	=	$res['main_graphic_story'];
		$mainGraphicAlt		=	$res['main_graphic_alt'];
		$bannerGraphic		=	$res['banner_graphic'];
		$bannerGraphicLink	=	$res['banner_graphic_link'];
		$bannerGraphicAlt	=	$res['banner_graphic_alt'];
		$noPDF				=	$res['no_pdf'];
		$mainStory			=	$res['main_story'];

		$sql_a				=	" SELECT id, story_number, headline FROM articles WHERE issue = {$res['id']} ";
		$rs_a				=	mysqlQuery($sql_a, $conn);
		if (mysqlNumRows($rs_a, $conn) == 0)	$mainGraphicOptions	=	array(0 => '');
		else {

				while ($res_a	= mysqlGetRow($rs_a)) {

					$mainGraphicOptions[$res_a['id']]	=	$res_a['story_number'].' - '.$res_a['headline'];

				}

		}


		if ($mainGraphicLarge == '') 							$mainGraphicLargeExists	=	'2';
		elseif (fileLinkValid('../images/'.$mainGraphicLarge) || fileLinkValid($mainGraphicLarge))
					$mainGraphicLargeExists	=	true;
		else													$mainGraphicLargeExists = 	false;


		if ($mainGraphicSmall == '') 							$mainGraphicSmallExists	=	'2';
		elseif (fileLinkValid('../images/'.$mainGraphicSmall) || fileLinkValid($mainGraphicSmall))
					$mainGraphicSmallExists	=	true;
		else													$mainGraphicSmallExists = 	false;

		if ($mainGraphicVSmall == '') 							$mainGraphicVSmallExists	=	'2';
		elseif (fileLinkValid('../images/'.$mainGraphicVSmall) || fileLinkValid($mainGraphicVSmall))
					$mainGraphicVSmallExists	=	true;
		else													$mainGraphicVSmallExists = 	false;

		if ($bannerGraphic == '') 								$bannerGraphicExists	=	'2';
		elseif (fileLinkValid('../images_main/'.$bannerGraphic) || fileLinkValid($bannerGraphic))
					$bannerGraphicExists	=	true;
		else													$bannerGraphicExists 	= 	false;

}	else {

		$sql		=	"SELECT max(issue) as num FROM issues";
		$rs			=	mysqlQuery($sql,$conn);
		$res		=	mysqlGetRow($rs);
		$issue		=	$res['num'] + 1;
		$headline			=	'';
		$isSpecial			=	'0';
		$summary			=	'[ Enter the issue\'s summary here ]';
		$recentStoryText	=	'';
		$wakeUpText			=	'';
		$disclaimer			=	'';
		$pageTitleText		=	'';
		$noPDF				=	0;
		$main_story			=	'';
		$mainGraphicLarge	=	'';
		$mainGraphicSmall	=	'';
		$mainGraphicVSmall	=	'';
		$mainGraphicStory	=	'';
		$mainGraphicAlt		=	'';
		$mainGraphicOptions	=	array(0 => '');

		$mainGraphicLargeExists		=	2;
		$mainGraphicSmallExists		=	2;
		$mainGraphicVSmallExists		=	2;
		$bannerGraphicExists		=	2;

		$sql		=	"SELECT banner_graphic, banner_graphic_link, banner_graphic_alt, date FROM issues ORDER BY issue DESC LIMIT 0,1";
		$rs_b		=	mysqlQuery($sql, $conn);
		$res_b		=	mysqlGetRow($rs_b);
		$bannerGraphic		=	$res_b['banner_graphic'];
		$bannerGraphicLink	=	$res_b['banner_graphic_link'];
		$bannerGraphicAlt 	=	$res_b['banner_graphic_alt'];
		$date		=	strtotime($res_b['date']) + ((3600 * 24) * 7 );
		$date 		=	date('Y', $date)."-".date('m', $date)."-".date('d', $date);
}



/* IF ERRORS IS SET, THEN THIS PAGE MUST HAVE BEEN CALLED AS A RESULT
OF A FAILED ATTEMPTED UPDATE.
IN THIS CASE WE SHOULD USE THE POST DATA FROM THE UPDATE ATTEMPT. */
if (isset($errors))	{

		$issue				=	$_POST['issue'];
		$headline			=	$_POST['headline'];
		$summary			=	$_POST['summary'];
		$date				=	makeDateFromPostData('date', $_POST);
		$recentStoryText	=	$_POST['recentStoryText'];
		$wakeUpText			=	$_POST['wakeUpText'];
		$disclaimer			=	$_POST['disclaimer'];
		$pageTitleText		=	$_POST['pageTitleText'];
		$mainGraphicLarge	=	$_POST['mainGraphicLarge'];
		$mainGraphicSmall	=	$_POST['mainGraphicSmall'];
		$mainGraphicVSmall	=	$_POST['mainGraphicSmall'];
		$mainGraphicStory	=	$_POST['mainGraphicStory'];
		$bannerGraphic		=	$_POST['bannerGraphic'];
		$bannerGraphicLink	=	$_POST['bannerGraphicLink'];
		$isSpecial			=	!isset($_POST['is_special']) ? NULL : $_POST['is_special'];
		if ($isSpecial == 'on') $isSpecial = '1';
		if ($isSpecial == 'off') $isSpecial = '0';
		$noPDF			=	!isset($_POST['no_pdf']) ? NULL : $_POST['no_pdf'];
		$mainStory		=	!isset($_POST['main_story']) ? NULL : $_POST['main_story'];
		if ($noPDF	== 	'on')	$noPDF	=	'1';
		if ($noPDF	==	'off')	$noPDF	=	'0';

}



/*	PREPARE A COUPLE OF TEXT STRINGS FOR USE LATER ON DEPENDING
ON WHETHER WE ARE ADDING OR UPDATING */
if ($issueId == 0)	{
	$submit			=	'Add';
	$titlePhrase	=	'Adding';
}	else	{
	$titlePhrase	=	"Editing";
	$submit			=	'Update';
}









/*	============================================================================
					DISPLAY */



/* TITLE */
/* DRAW TITLE */
echo 	"<br /><div align=\"right\"><a href=\"index.php?showIssue=$issueId\">Back to main list of stories</a></div>";

echo 	"<h2>$titlePhrase Issue $issue </h2>";
echo 	"<p>............</p>";

if (isset($errors))	{

		echo "<font color=\"red\">There were some errors... Please see below...";

}

/* MAIN FORM */
echo	"<form method=\"post\" action=\"index.php?updateIssue=$issueId&showIssue=$issueId\" >";

?>
<!-- Include the Free Rich Text Editor Runtime -->
<script src="RTE/js/richtext.js" type="text/javascript" language="javascript"></script>
<!-- Include the Free Rich Text Editor Variables Page -->
<script src="RTE/js/config_editIssue.js" type="text/javascript" language="javascript"></script>
<?php

echo	"<table ><tr>";


/* ISSUE */
echo	"<td width=\"10%\" align=\"right\"><b>Issue # ::</b> </td><td>";
if (isset($errors['issue'])) echo	"<div class=\"errorMessage\">".$errors['issue']."</div>";
echo	"<input style=\"width: 100px\" type=\"text\" name=\"issue\" value=\"$issue\" /></td>";

echo	"</tr><tr><td><br /></td></tr><tr>";

/* DATE */
echo	"<td width=\"10%\" align=\"right\"><b>Date ::</b> </td><td>";
echo	makeDateSelector('date', $date)."</td>";

echo	"</tr><tr><td><br /></td></tr><tr>";

/*	HEADLINE */
echo	"<td  align=\"right\"><b>Headline ::</b> </td><td>";
if (isset($errors['headline'])) echo	"<div class=\"errorMessage\">".$errors['headline']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"headline\" value=\"$headline\" />";
echo	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
echo 	"<a href='gfx/example_headline.gif' target='_blank'>What is this?</a></td>";
echo	"</tr><tr><td><br /></td></tr><tr>";

echo	"<td colspan='2' style='padding-left:200px; font-size: 7pt; color:#777777'>Any html tags entered in the summary will be stripped out. The first sentence will be automatically enboldened.</td></tr><tr>";

/* SUMMARY */
echo	"<td align=\"right\" valign=\"top\"><b>Summary ::</b> </td><td>";
if (isset($errors['summary'])) echo	"<div class=\"errorMessage\">".$errors['summary']."</div>";
?>
<textarea name='summary'><?php echo addslashes($summary); ?></textarea>
<script type="text/javascript">
	CKEDITOR.replace( 'summary',

			{

				<?php echo $editIssue_ckeditor_settings; ?>

			}

			 );
</script>
<?php
echo 	"<a href='gfx/example_summary.gif' target='_blank'>What is this?</a></td>";
echo	"</td></tr><tr><td><br /></td></tr><tr>";

/* RECENT STORIES TEXT */
echo	"<td width=\"10%\" align=\"right\"><b>Recent Stories Link ::</b> </td><td>";
if (isset($errors['recentStoryText'])) echo	"<div class=\"errorMessage\">".$errors['recentStoryText']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"recentStoryText\" value=\"$recentStoryText\" />";
echo	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
echo 	"<a href='gfx/example_recentStories.gif' target='_blank'>What is this?</a></td>";

echo	"</tr><tr><td><br /></td></tr><tr>";

/* WAKE UP TEXT */
echo	"<td width=\"10%\" align=\"right\"><b>Wake Up!!! Text ::</b> </td><td>";
if (isset($errors['wakeUpText'])) echo	"<div class=\"errorMessage\">".$errors['wakeUpText']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"wakeUpText\" value=\"$wakeUpText\" />";
echo	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
echo 	"<a href='gfx/example_wakeUpText.gif' target='_blank'>What is this?</a></td>";

echo	"</tr><tr><td><br /></td></tr><tr>";

/* DISCLAIMER */
echo	"<td width=\"10%\" align=\"right\"><b>Disclaimer Text ::</b> </td><td>";
if (isset($errors['disclaimer'])) echo	"<div class=\"errorMessage\">".$errors['disclaimer']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"disclaimer\" value=\"$disclaimer\" />";
echo	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
echo 	"<a href='gfx/example_disclaimer.gif' target='_blank'>What is this?</a></td>";

echo	"</tr><tr><td><br /></td></tr><tr>";

/* PAGE TITLE TEXT */
echo	"<td width=\"10%\" align=\"right\"><b>Page Title Text ::</b> </td><td>";
if (isset($errors['pageTitleText'])) echo	"<div class=\"errorMessage\">".$errors['pageTitleText']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"pageTitleText\" value=\"$pageTitleText\" />";
echo	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
echo 	"<a href='gfx/example_pageTitle.gif' target='_blank'>What is this?</a></td>";
echo	"</tr><tr><td><br /></td></tr>";




/*	NO PDF	*/
echo 	"<tr><td style='border-bottom:1px solid #777777'>&nbsp;</td>";
echo 	"</tr><tr>";

echo 	"<td><h3>No PDF?</h3></td>";

echo 	"<td><input type='checkbox' name='no_pdf' value='on' ";
if ($noPDF) echo ' checked ';
echo 	" />&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;Tick if there is no PDF for this issue - if ticked it will remove the PDF links on the Homepage and Back Issues page</td>";

echo 	"</tr>";







/*	MAIN STORY	*/
echo 	"<tr><td style='border-bottom:1px solid #777777'>&nbsp;</td>";
echo 	"</tr><tr>";

echo 	"<td><h3>Main Story</h3></td>";

/*
 * Cache story titles/numbers and IDs
 */
$sql		=	"	SELECT id, story_number, headline
					FROM articles
					WHERE issue = $issueId ";
$rs			=	mysqlQuery($sql, $conn);
$options	=	array();
$values		=	array();
while ($option	=	mysqlGetRow($rs))	{

		$options[]		=	$option['story_number'].' - '.stripslashes($option['headline']);
		$values[]		=	$option['id'];

}


/*
 * 	Draw the SELECT box
 */
echo 	"<td><select name='main_story'>";
foreach ($options as $key => $option)	{

	$selected	=	'';
	if ($values[$key]	==	$mainStory)		$selected	=	' selected ';
	echo "<option value='{$values[$key]}' $selected >$option</option>";

}
echo	"</select></td></tr>";









echo 	"<tr><td style='border-bottom:1px solid #777777'>&nbsp;</td>";
echo 	"</tr><tr>";

echo 	"<td><h3>Main Graphic</h3></td>";

echo 	"</tr><tr>";

echo 	"<td><a href='tutorials/mainGraphic.html' target='_blank'>What is This</a></td>";

echo 	"</tr><tr>";

echo	"<td colspan='2' style='padding-left:200px; font-size: 7pt; color:#777777'>Just enter the image filename. The system will find it regardless of which folder its in on the server...</td></tr><tr>";

/* MAIN GRAPHIC (LARGE AND SMALL) */
echo	"<td width=\"10%\" align=\"right\"><b>Main Graphic (V. Small) ::</b> </td><td>";
if (isset($errors['mainGraphicVSmall'])) echo	"<div class=\"errorMessage\">".$errors['mainGraphicVSmall']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"mainGraphicVSmall\" value=\"$mainGraphicVSmall\" />";

if ($mainGraphicVSmallExists == '1')					echo	"<img style='margin-left:20px' src='../images/tick.gif' />";
elseif ($mainGraphicVSmallExists == false) 		echo	"<img style='margin-left:20px' src='../images/cross.gif' />";

echo	"</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Main Graphic (Small) ::</b> </td><td>";
if (isset($errors['mainGraphicSmall'])) echo	"<div class=\"errorMessage\">".$errors['mainGraphicSmall']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"mainGraphicSmall\" value=\"$mainGraphicSmall\" />";

if ($mainGraphicSmallExists == '1')					echo	"<img style='margin-left:20px' src='../images/tick.gif' />";
elseif ($mainGraphicSmallExists == false) 		echo	"<img style='margin-left:20px' src='../images/cross.gif' />";

echo	"</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Main Graphic (Large) ::</b> </td><td>";
if (isset($errors['mainGraphicLarge'])) echo	"<div class=\"errorMessage\">".$errors['mainGraphicLarge']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"mainGraphicLarge\" value=\"$mainGraphicLarge\" />";

if ($mainGraphicLargeExists == '1')					echo	"<img style='margin-left:20px' src='../images/tick.gif' />";
elseif ($mainGraphicLargeExists == false) 		echo	"<img style='margin-left:20px' src='../images/cross.gif' />";

echo	"</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Main Graphic (Alt-Text) ::</b> </td><td>";
if (isset($errors['mainGraphicAlt'])) echo	"<div class=\"errorMessage\">".$errors['mainGraphicAlt']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"mainGraphicAlt\" value=\"$mainGraphicAlt\" />";

echo	"</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Main Graphic Story ::</b> </td><td>";
echo	"<select name=\"mainGraphicStory\">";
	foreach ($mainGraphicOptions	as	$id	=> $option)	{

			echo	"<option value=\"$id\" ";
			if ($mainGraphicStory	== $id)	echo	" selected ";
			echo	" >$option</option>";

	}
echo	"</select>";

echo	"</tr><tr><td><br /></td></tr><tr>";

echo 	"<td style='border-bottom:1px solid #777777'>&nbsp;</td>";
echo 	"</tr><tr>";

echo 	"<td><h3>Banner Graphic</h3></td>";

echo 	"</tr><tr>";

echo 	"<td><a href='tutorials/bannerGraphic.html' target='_blank'>What is This</a></td>";

echo 	"</tr><tr>";

echo	"<td colspan='2' style='padding-left:200px; font-size: 7pt; color:#777777'>Just enter the image filename. The system will find it regardless of which folder its in on the server...</td></tr><tr>";

/* BANNER GRAPHIC */
echo	"<td width=\"10%\" align=\"right\"><b>Banner Graphic ::</b> </td><td>";
if (isset($errors['bannerGraphic'])) echo	"<div class=\"errorMessage\">".$errors['bannerGraphic']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"bannerGraphic\" value=\"$bannerGraphic\" />";

if ($bannerGraphicExists == '1')						echo	"<img style='margin-left:20px' src='../images/tick.gif' />";
elseif ($bannerGraphicExists == false) 			echo	"<img style='margin-left:20px' src='../images/cross.gif' />";

echo	"</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Banner Graphic (Link) ::</b> </td><td>";
if (isset($errors['bannerGraphicLink'])) echo	"<div class=\"errorMessage\">".$errors['bannerGraphicLink']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"bannerGraphicLink\" value=\"$bannerGraphicLink\" /></td>";

echo	"</td></tr><tr>";

echo	"<td colspan='2' style='padding-left:200px; font-size: 7pt; color:#777777'>[ Optional ]</td></tr><tr>";

echo	"<td width=\"10%\" align=\"right\"><b>Banner Graphic (Alt) ::</b> </td><td>";
if (isset($errors['bannerGraphicAlt'])) echo	"<div class=\"errorMessage\">".$errors['bannerGraphicAlt']."</div>";
echo	"<input style=\"width: 560px\" type=\"text\" name=\"bannerGraphicAlt\" value=\"$bannerGraphicAlt\" /></td>";


echo	"</tr><tr><td><br /></td></tr><tr>";

/*	SUBMIT AND CANCEL BUTTONS */
echo	"<td>";
echo	"<input type=\"submit\" value=\"$submit\" name=\"submit\" />";
echo	"</td><td align=\"right\">";
echo	"<input type=\"submit\" value=\"Back to Main List\" name=\"cancel\" />";
if ($issueId != 0) 	{ echo	"&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;&nbsp;";
echo	"<input type=\"submit\" name=\"delete\" onClick=\"return confirmSubmit()\" value=\"Delete Issue\"/></td>";

}


echo	"</tr></table>";
echo	"</form>";









function freeRTE_Preload($content) {
	// Strip newline characters.
	$content = str_replace(chr(10), " ", $content);
	$content = str_replace(chr(13), " ", $content);
	// Replace single quotes.
	$content = str_replace(chr(145), chr(39), $content);
	$content = str_replace(chr(146), chr(39), $content);

	$content = str_replace('<img', '<image', $content);
	// Return the result.
	return $content;
}







