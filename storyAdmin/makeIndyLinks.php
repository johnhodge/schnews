<?php
//	Test inputs
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{

	$issue_id		=	$_GET['issue'];

}	else	{

	if (DEBUG)	echo "<div class='errorMessage'>Input $issue not an int</div>";
	exit;

}



//	Grab our includes
require "functions.php";



//	Test for ommision
//		(which story do we want not to show?)
$omit		=	false;
if (isset($_GET['omit']) && is_numeric($_GET['omit']))	{

	$omit		=	$_GET['omit'];

}



//	Grab what we need from the database...

//	First, work out the issue number...
$conn		=	mysqlConnect();
$sql		=	"	SELECT issue
					FROM issues
					WHERE id = $issue_id ";
$rs			=	mysqlQuery($sql, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{

	if (DEBUG)	echo "<div class='errorMessage'>Couldn't get issue num from DB. Not 1 SQL result</div>";
	exit;

}
$res		=	mysqlGetRow($rs);
$issue_num	=	$res['issue'];


// Now we'll make some indexed array for the article data...
$sql		=	"	SELECT id, headline, rss_text, story_number
					FROM articles
					WHERE issue = $issue_id
					ORDER BY story_number ASC ";
$rs			=	mysqlQuery($sql, $conn);

$headlines		=	array();
$rss_texts		=	array();
$story_numbers	=	array();

while ($res	=	mysqlGetRow($rs))	{

	$id 	=	$res['id'];

	//	Are we not showing this story...
	if ($id == $omit)	{

		continue;

	}

	$headlines[$id]		=	$res['headline'];
	$rss_texts[$id]		=	$res['rss_text'];
	$story_numbers[$id]	=	$res['story_number'];

}















//	===================================================
//					Output the HTML...

//	New output buffer
$output		=	array();

$output[]	=	"<p><b>";
$output[]	=	"Also in <a href='../archive/news".$issue_num.".php'>SchNEWS $issue_num</a>:</b> ";

$article_buffer		=	array();
foreach ($headlines as $id => $headline)	{

	$rss_text		=	$rss_texts[$id];
	$story_number	=	$story_numbers[$id];

	$temp_buffer	=	array();
	$temp_buffer[]	=	'';
	$temp_buffer[]	=	"<a href='../archive/news".$issue_num.$story_number.".php'><b>".stripslashes($headline)."</b></a> - ";
	$temp_buffer[]	=	stripslashes($rss_text);
	$temp_buffer[]	=	'';

	$temp_buffer		=	implode('<br />', $temp_buffer);
	$article_buffer[]	=	$temp_buffer;

}

$article_buffer		=	implode(' | ', $article_buffer);

$output[]	=	$article_buffer;
$output[]	=	"</p>";


//	Flush the output buffer...
$with_line_breaks		=	implode('<br />', $output);
$without_line_breaks	=	str_replace(array('<br />', '<br>'), '', implode('', $output));

echo "<div style='padding:25px; margin: 25px; background-color: #eeeeee; border: 3px solid #cccccc'>";
echo "<h1>Without Line Breaks and showing HTML</h1>";
echo htmlentities($without_line_breaks);
echo "</div>";

echo "<div style='padding:25px; margin: 25px; background-color: #eeeeee; border: 3px solid #cccccc'>";
echo "<h1>With Line Breaks and showing HTML</h1>";
echo str_replace("&lt;br /&gt;", "<br />", htmlentities($with_line_breaks));
echo "</div>";

echo "<div style='padding:25px; margin: 25px; background-color: #eeeeee; border: 3px solid #cccccc'>";
echo "<h1>Without Line Breaks</h1>";
echo $without_line_breaks;
echo "</div>";

echo "<div style='padding:25px; margin: 25px; background-color: #eeeeee; border: 3px solid #cccccc'>";
echo "<h1>With Line Breaks</h1>";
echo $with_line_breaks;
echo "</div>";













//	===========================================================
//				Draw the Omit Options

//	New output buffer
$output			=	array();

$output[]			=	"<div style='padding: 25px; margin: 25px'>";
$output[]			=	"<h2>Click on a headline to omit it from the HTML above...</h2>";

foreach ($headlines as $id => $headline)	{

	$output[]		=	"<a href='makeIndyLinks.php?issue=$issue_id&omit=$id'>".stripslashes($headline)."</a>";

}

$output[]		=	'';
$output[]		=	"<a href='makeIndyLinks.php?issue=$issue_id'>Show All</a>";
$output[]		=	"</div>";

//	Flush the buffer
echo implode("<br />", $output);


