<?php

if (!$_SESSION[SES.'loggedin']) exit;

// TITLE
echo "<h2>Story Comments</h2>";
echo "<p>............</p>";


echo "<p><a id='toggleInstructions1' class='javaLink'>Toggle Instructions</a></p>";

		echo "<div id='instructions' class='instructions'>";	
			echo "<p><h3>Instructions</h3></p>";
			echo "<p>This page lists all the comments made on stories by users.</p>";
			echo "<p>Its main function is to identify and deal with spam or malicious comments. As our comment system does not require users to be logged in in order to post a comment we tend to get a lot of spam comments posted.</p>";
			echo "<p>This software has a built in spam filter system, which does its best to recognise and quarentine spam message, based on an assessment of the number of links in the message, the number of profanities, and the number of post attempts from the same IP in a given time period.";
			echo "<p>&nbsp;</p>";
			echo "<p>This list will need scanning regularly in order to:</p>";
			echo "<blockquote>- Make sure that no real, legitimate messages have been marked as spam.</blockquote>";
			echo "<blockquote>- Make sure that no spam message have been missed by the software, as these will be visible to the public on the SchNEWS site.</blockquote>";
			echo "<p>&nbsp;</p>";
			echo "<p>This page is broken into three views - '<b>View Spam</b>' shows just the messages marked as spam, '<b>View Not Spam</b>' show just the message that the software thinks are real (i.e. not spam) messages, and '<b>View All</b>' shows every message in the system.";
			echo "<p>&nbsp;</p>";
			echo "<p>Within any view you can show '<b>per article</b>' or in '<b>date order</b>'.";
			echo "<blockquote><b>Per Article</b> groups the comments together per article/story, so you can more easily see an entire discussion thread on a given story.</blockquote>";
			echo "<blockquote><b>Date Order</b> show the articles in the order they we posted, so you can easily see the new entries that need dealing with..</blockquote>";
			echo "<p>&nbsp;</p>";
			echo "<p><b>Note: </b>IP Addresses are not logged, and the datestamp on all comments is slightly randomised by up to 15 minutes so as to make it harder to identify a user from the server logs.";
		echo "</div>";
		
		
echo "<p>............</p>";

//	PROCESS GET VARS
if (isset($_GET['commentDelete']) && is_numeric($_GET['commentDelete']))	{
	
	if (deleteCommentAdmin($_GET['commentDelete']))	echo "<p><font color=\"red\">Comment number ".$_GET['commentDelete']." has been deleted</font></p>";

}

if (isset($_GET['delete']) && $_GET['delete'] == 'all')	{
	
	if (deleteAllSpamComments())	echo "<p><font color=\"red\">All Spam Comments have been deleted</font></p>";
	
}

if (isset($_GET['commentNoSpam']) && is_numeric($_GET['commentNoSpam']))	{
	
	if (noSpamCommentAdmin($_GET['commentNoSpam']))	echo "<p><font color=\"red\">Comment number ".$_GET['commentNoSpam']." has been marked as Not Spam</font></p>";
	
}

if (isset($_GET['commentIsSpam']) && is_numeric($_GET['commentIsSpam']))	{
	
	if (isSpamCommentAdmin($_GET['commentIsSpam']))	echo "<p><font color=\"red\">Comment number ".$_GET['commentIsSpam']." has been marked as Spam</font></p>";
	
}



if (!isset($_SESSION['commentViewMode']))	$_SESSION['commentViewMode']		=	'date';
if (isset($_GET['mode']))		$_SESSION['commentViewMode'] 	=	$_GET['mode'];


if (!isset($_SESSION['commentView']))		$_SESSION['commentView']	=	0;
if (isset($_GET['commentView']) && is_numeric($_GET['commentView']))	$_SESSION['commentView'] = $_GET['commentView'];

	$style		=	"style=\"border-top:1px solid blue; border-left:1px solid blue; border-right:1px solid blue; padding:5px\" ";
	$styleThick	=	"style=\"border-top:3px solid blue; border-left:3px solid blue; border-right:3px solid blue; padding:5px\" ";
	$style1		=	$style;
	$style2		=	$style;
	$style3		=	$style;

switch ($_SESSION['commentView'])	{
	
	case '0':
		$style1	=	$styleThick;
		break;
		
	case '1':
		$style2	=	$styleThick;
		break;
		
	case '2':
		$style3	=	$styleThick;
		break;
		
	
}
	
	
	


	$styleBase		=	"style=\"border-top:3px solid blue\" ";


echo "<br /><br />";

echo "<table>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td $style1 >
				<a href=\"?comments=1&commentView=0\" onMouseOver=\"Tip('$tip_comments_viewSpam')\">View Spam</a>
			</td>
			<td $style2 >
				<a href=\"?comments=1&commentView=1\" onMouseOver=\"Tip('$tip_comments_viewNotSpam')\">View Not Spam</a>
			</td>
			<td $style3 >
				<a href=\"?comments=1&commentView=2\" onMouseOver=\"Tip('$tip_keywords_keywordSort')\">View All</a>
			</td>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>";

			switch($_SESSION['commentViewMode'])		{
				
				default:
					echo "<a href='?comments=1&mode=article' onMouseOver=\"Tip('$tip_comments_perArticle')\" >Show per Article</a>";
					if ($_SESSION['commentView'] == 0)	{
							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							echo "<a href='?comments=1&delete=all' onClick=\"return confirmSubmit()\">Delete All on Page</a>";
					}
					break;
					
				case 'article':
					echo "<a href='?comments=1&mode=date'onMouseOver=\"Tip('$tip_comment_dateOrder')\">Show in Date Order</a>";
					break;
				
			}
			
echo 		"</td>
		</tr>
		<tr>
			<td $styleBase colspan='5'>
				&nbsp;
			</td>
		</tr>
	</table>";





if (!isset($_SESSION['commentsDrill']))	$_SESSION['commentsDrill'] = 0;
if (isset($_GET['commentsDrill']))	$_SESSION['commentsDrill'] = $_GET['commentsDrill'];

switch ($_SESSION['commentView'])	{
	
	case '0':
			$sqlAdd		=	' WHERE spam = 1 ';
			break;
			
	case '1':
			$sqlAdd		=	' WHERE spam = 0 ';
			break;
			
	case '2':
			$sqlAdd		=	'';
			break;
	
}


if ($_SESSION['commentViewMode'] == 'article')	{

		$conn	=	mysqlConnect();
		$sql	=	" SELECT DISTINCT articleid FROM story_comments ";
		$sql	.=	$sqlAdd.' ORDER BY articleid DESC ';
		$rs		=	mysqlQuery($sql,$conn);
		
		while ($res		=	mysqlGetRow($rs))	{
				
				$articleId	=	$res['articleid'];
			
				$sqlA		=	" SELECT * FROM articles WHERE id = $articleId ";
				$rsA		=	mysqlQuery($sqlA, $conn);
				$resA		=	mysqlGetRow($rsA);
				
				$issue			=	$resA['issue'];
				$storyNumber	=	$resA['story_number'];
				$headline		=	$resA['headline'];
				
				$sqlI		=	" SELECT issue FROM issues WHERE id = $issue ";
				$rsI		=	mysqlQuery($sqlI, $conn);
				$resI		=	mysqlGetRow($rsI);
				
				$issueNum	=	$resI['issue'];
				
				if ($_SESSION['commentsDrill'] == $articleId)	{
					
					$button	=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=0\"><img src=\"../pap/close.gif\" /></a>";
					
					$comments 	=	getCommentsForAdmin($articleId);			
					
							
				}	else {
					
					$button	=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=$articleId\"><img src=\"../pap/open.gif\" /></a>";
					$comments = '';
				}
				
				$output		=	'<p>'.$button.'&nbsp;&nbsp;&nbsp;&nbsp;';
				$output		.=	"Issue $issueNum - Story $storyNumber :: <b>$headline</b></p>";
				$output		.=	$comments;
				
				echo $output;
		
}



}	else	{
	
	
		$conn	=	mysqlConnect();
		$sql	=	" SELECT * FROM story_comments ";
		$sql	.=	$sqlAdd.' ORDER BY datetime DESC ';
		$rs		=	mysqlQuery($sql,$conn);
		
		while ($res		=	mysqlGetRow($rs))	{
			
			if ($res['articleid'] != 0 && $res['articleid'] != '')	{
			
				$sqlA		=	" SELECT issue, story_number, headline FROM articles WHERE id = ".$res['articleid'];
				$rsA		=	mysqlQuery($sqlA, $conn);
				$resA		=	mysqlGetRow($rsA);
						
				$issue			=	$resA['issue'];
				$storyNumber	=	$resA['story_number'];
				$headline		=	$resA['headline'];
				
				if (!is_numeric($issue))	continue;
				
				$sqlI		=	" SELECT issue FROM issues WHERE id = $issue ";
				$rsI		=	mysqlQuery($sqlI, $conn);
				$resI		=	mysqlGetRow($rsI);
				
				$issueNum	=	$resI['issue'];

			}	elseif ($res['featureid'] != '' && $res['featureid'] != 0)	{
			
				$sqlA		=	" SELECT id, title FROM features WHERE id = ".$res['featureid'];
				$rsA		=	mysqlQuery($sqlA, $conn);
				$resA		=	mysqlGetRow($rsA);
				
				$issueNum		=	"<b style='color: red'>Feature</b>";
				$storyNumber	=	$resA['id'];
				$headline		=	$resA['title'];
			
			}
			
			
			$comment 	=	nl2br(htmlentities(stripslashes($res['comment'])));
			
			$author		=	htmlentities(stripslashes($res['author']));
			$ipAddress	=	$res['ip_address'];
			$date		=	makeHumanDate($res['datetime']);
			$time		=	makeTimeFromSql($res['datetime']);
			
			$links		=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=".$res['articleid']."&commentDelete=".$res['id']."\" onClick=\"return confirmSubmit()\" onMouseOver=\"Tip('$tip_comments_deleteComment')\">Delete Comment</a>";
			
			if ($res['spam'] == 1) $spam = true;
				else $spam = false;
				
			if ($spam)	{
			
				$links	.=	'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;';
				$links	.=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=".$res['articleid']."&commentNoSpam=".$res['id']."\" onMouseOver=\"Tip('$tip_comments_notSpam')\">Not Spam</a>";

			}	else {
				
				$links	.=	'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;';
				$links	.=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=".$res['articleid']."&commentIsSpam=".$res['id']."\" onClick=\"return confirmSpam()\" onMouseOver=\"Tip('$tip_comments_IsSpam')\">Is Spam</a>";

				
			}
			
			
			
			echo "<p>Issue $issueNum - Story $storyNumber :: <b>$headline</b></p>";
			echo "<blockquote><p><b>Added on $date at $time by '$author' from IP '$ipAddress'</b></p></blockquote>";
			
			if ($spam)	echo "<blockquote style='color:red'>$comment</blockquote>";
			else echo "<blockquote>$comment</blockquote>";
			
			echo $links;
			
			echo '<br /><br />';
			
			echo "<div style='border-bottom: 1px dotted #666666'>&nbsp;</div>";
			
			echo '<br /><br />';
			
		}


}










function	getCommentsForAdmin($articleId)	{
	
		if (!is_numeric($articleId))	return false;
		
		require 'js_tooltip/popupText.php';
		$conn		=	mysqlConnect();
		$sql		= 	" SELECT * FROM story_comments WHERE articleId = $articleId ";	

		switch ($_SESSION['commentView'])	{
	
			case '0':
					$sqlAdd		=	' AND spam = 1 ';
					break;
					
			case '1':
					$sqlAdd		=	' AND spam = 0 ';
					break;
					
			case '2':
					$sqlAdd		=	'';
					break;
	
		}
		$sql	.=	$sqlAdd.' ORDER BY datetime DESC ';
		$rs		=	mysqlQuery($sql,$conn);
		
		$output	=	'';
		while ($res	=	mysqlGetRow($rs))		{
				
			$commentId	=	$res['id'];
			$comment 	=	nl2br(htmlentities(stripslashes($res['comment'])));
			$author		=	htmlentities(stripslashes($res['author']));
			$ipAddress	=	$res['ip_address'];
			$date		=	makeHumanDate($res['datetime']);
			$time		=	makeTimeFromSql($res['datetime']);
			if ($res['spam'] == 1) $spam = true;
				else $spam = false;
			if ($res['deleted'] == 1) $deleted = true;
				else $deleted = false;
			if ($res['authorised'] == 1) $authorised = true;
				else $authorised = false;	
			
			//	MAKE COMMENT
			$output	 	.=	"<p><b>Added on $date at $time by '$author' from IP '$ipAddress'</b></p>";
			$output		.=	"<p>";
			if ($spam)	$output	.=	"<font color=\"red\">";
			$output		.=	$comment;
			if ($spam)	$output	.=	"</font>";
			$output		.= "</p>";
			
			//	DELETE LINK
			$links		=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=$articleId&commentDelete=$commentId\" onClick=\"return confirmSubmit()\" onMouseOver=\"Tip('$tip_comments_deleteComment')\">Delete Comment</a>";
			
			if ($spam)	{
			
				$links	.=	'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;';
				$links	.=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=$articleId&commentNoSpam=$commentId\" onMouseOver=\"Tip('$tip_comments_notSpam')\">Not Spam</a>";

			}	else {
				
				$links	.=	'&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;';
				$links	.=	"<a href=\"?comments=1&commentView=".$_SESSION['commentView']."&commentsDrill=$articleId&commentIsSpam=$commentId\" onClick=\"return confirmSpam()\" onMouseOver=\"Tip('$tip_comments_IsSpam')\">Is Spam</a>";

				
			}
			
			$output		.=	"<div style=\"padding-left:25%\">$links</div>";
			$output		.=	"<div style=\"margin-bottom:10px; border-bottom:1px solid #bbbbbb\">&nbsp;</div>";
			$output		.=	"<br />";
							
		}

		
		$output		=	"<div style=\"margin-left:100px; padding-left:25px; border-left:1px solid #999999\">$output</div>";
		return $output;
		
		
}





function deleteCommentAdmin($commentId)	{
	
	if(!is_numeric($commentId))	return false;
	
	$conn		=	mysqlConnect();
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return false;
	
	
	$sql		=	" DELETE FROM story_comments WHERE id = $commentId ";
	$rs			=	mysqlQuery($sql, $conn);
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return true;
		return false;
	
}



function noSpamCommentAdmin($commentId)	{
	
	if(!is_numeric($commentId))	return false;
	
	$conn		=	mysqlConnect();
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId && spam = 1 ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return false;
	
	$sql		=	" UPDATE story_comments SET spam = 0 WHERE id = $commentId ";
	$rs			=	mysqlQuery($sql, $conn);
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId && spam = 0 ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return false;
	return true;
		
}

function isSpamCommentAdmin($commentId)	{
	
	if(!is_numeric($commentId))	return false;
	
	$conn		=	mysqlConnect();
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId && spam = 0 ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return false;
	
	$sql		=	" UPDATE story_comments SET spam = 1 WHERE id = $commentId ";
	$rs			=	mysqlQuery($sql, $conn);
	
	$sql		=	" SELECT id FROM story_comments WHERE id = $commentId && spam = 1 ";
	$rs			=	mysqlQuery($sql,$conn);
	if (mysqlNumRows($rs, $conn) == 0) 	return false;
	return true;
	
	
}



function deleteAllSpamComments()	{
	
		$conn		=	mysqlConnect();
		
		$sql		=	" DELETE FROM story_comments WHERE spam = 1 ";
		$rs			=	mysqlQuery($sql, $conn);
		
		$sql		=	" SELECT id FROM story_comments WHERE spam = 1 ";
		$rs			=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs, $conn) === 0)		return true;
		return false;
	
	
}

