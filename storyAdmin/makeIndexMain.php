<?php


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';


/* DATA PROCESSING SECTION */

// GET ISSUE ID
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{
	
	$issueId	=	$_GET['issue'];
		
}

if (!isset($issueId))	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: issueId not set ;; exiting silently <br>";
	exit;
}


require '../storyAdmin/storyCreationFunctions.php';


//	Homepage will be returned as a string, which is basically a full HTML file...
$homepage 	= 	makeHomepage($issueId);


if (file_exists('../index_temp.php'))	unlink ('../index_temp.php');

$file		=	fopen('../index_temp.php', 'x+');
fwrite($file, $homepage);
fclose($file);

/*
if (!file_put_contents('../index_temp.php', $homepage))	{
	
		echo "<h1>Couldn't write file</h1>";
	
}
*/

$link = dirname($_SERVER['REQUEST_URI']).'/../index_temp.php';

//echo $_SERVER['REQUEST_URI']."<br />";
//echo $link;

header("Location: $link");

exit;

?>
