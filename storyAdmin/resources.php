<?php


$edition_pageHeader		=	'<a href="/index.htm"><img src="../images_main/schnews.gif" width="292" height="90" border="0" alt="back to the SchNEWS Home Page" /></a><A HREF="../schmovies/index-on-the-verge.htm" TARGET="_blank"><IMG SRC="../images_main/on-the-verge-banner-tor.jpg" ALT="ON THE VERGE - The Smash EDO Campaign Film" BORDER="0" WIDTH="465" HEIGHT="90"></a>';


$edition_navBar			=	'<a href="../pages_menu/subscribe.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("subscribe","","../images_menu/subscribe-over.gif",1)"><img name="subscribe" border="0" src="../images_menu/subscribe.gif" width="90" height="32" alt="subscribe to receive SchNEWS every week by email" /></a><a href="/archive/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("archive","","../images_menu/archive-over.gif",1)"><img name="archive" border="0" src="../images_menu/archive.gif" width="90" height="32" alt="View any SchNEWS back issues" /></a><a href="../pages_menu/about.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("about","","../images_menu/about-over.gif",1)"><img name="about" border="0" src="../images_menu/about.gif" width="90" height="32" alt="Find out about SchNEWS" /></a><a href="../links/index.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("contacts","","../images_menu/contacts-over.gif",1)"><img name="contacts" border="0" src="../images_menu/contacts.gif" width="100" height="32" alt="Search for contacts and links" /></a><a href="/diyguide/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("diyguide","","../images_menu/guide-over.gif",1)"><img name="diyguide" border="0" src="../images_menu/guide.gif" width="90" height="32" alt="Various guides on how to do direct action etc" /></a><a href="/monopresist/index.htm" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage("monopolise","","../images_menu/monopolise-over.gif",1)"><img name="monopolise" border="0" src="../images_menu/monopolise.gif" width="90" height="32" alt="Details of previous hot topics" /></a><A href="../schmovies" ONMOUSEOUT="MM_swapImgRestore()" ONMOUSEOVER="MM_swapImage("SchMOVIES","","http://www.schnews.org.uk/images_menu/schmovies-over.gif",1)"><IMG NAME="SchMOVIES" BORDER="0" SRC="http://www.schnews.org.uk/images_menu/schmovies.gif" WIDTH="90" HEIGHT="32" ALT="SchMOVIEWS - Short films produced by SchNEWS" /></A><br /> 
<img src="../images_menu/description.gif" width="640" height="20" alt="The weekly newsletter from Justice? - Brighton"s direct action collective" />';


$edition_leftBar		=	'<p><B><A HREF="news176.htm">SchNEWS - this week 10 years ago</A></B></p><P><B><SPAN CLASS="style1"><A HREF="news597.htm">SchNEWS 
- this time last year</A></SPAN></B></P><P><A href="../pages_menu/defunct.php"><IMG SRC="../images/rss_feed.gif" WIDTH="32" HEIGHT="15" BORDER="0" /></A> 
</P><P><B><FONT FACE="Arial, Helvetica, sans-serif">BACK ISSUES</FONT></B></P><P><font face="Arial, Helvetica, sans-serif"><a href="news639.htm"><b>SchNEWS 
639, 11th July 2008</b></a></font><font size="4"><strong><br> </strong></font><strong> 
FIT for Purpose </strong> - SchNEWS looks at the police Forward Intelligence Team 
(FIT), who have become a common sight at protests, and a campaign to resist them... 
plus - the British govt makes it harder for immigrants and asylum seekers to get 
the free health care, international day of action against private equity firms, 
the site of a proposed City Academy in Wembley has been re-occupied with a tent 
city as the locals, and more...</P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news638.htm"><B>SchNEWS 
638, 4th July 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><STRONG><BR> </STRONG></FONT><STRONG> Empre of the Vanities 
</STRONG> - An outrageous story of greed, lust and structural adjustment as we 
chart the steady decline of the G8 cartel... plus - Protests against open coal 
mine in Derbyshire get unexpected Court reprieve, photography is new focus of 
civil liberty crackdown, Anti-Israel protests at "Salute to Israel" march in central 
London to celebrate 60 years of oppression, London resident Binyam Mohamed still 
in Guantanamo after years of torture, and more...</P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news637.htm"><B>SchNEWS 
637, 27th June 2008</B></A></FONT><FONT SIZE="4"><STRONG><BR> </STRONG></FONT><STRONG> 
Hounded </STRONG> - SchNEWS asks who&rsquo;s harassing who as The Crawley and 
Horsham Foxhunt seeks giant exclusion zone against local hunt monitors and Wildlife 
Protection Group, plus - Carmel Agrexco&rsquo;s UK depot was shut down, anniversary 
of McLibel trial victory, ten animal rights activists held without charge under 
anti-terror laws in Austria, and more...<FONT FACE="Arial, Helvetica, sans-serif"></FONT><font face="Arial, Helvetica, sans-serif"></font><FONT FACE="Arial, Helvetica, sans-serif"></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news636.htm"><B>SchNEWS 
636, 20th June 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><STRONG><BR> </STRONG></FONT><STRONG> Along For Fluoride</STRONG> 
- The UK government is intent on expanding the fluoridisation of drinking water, 
but yet all medical evidence is clearly against this "forced mass medication". 
So why are they doing it? Plus - Austrian animal rights groups targeted by violent 
police campaign, open-cut coal mining protests in Derbyshire, Hicham Bezza is 
released, protests as George Bush visits London, breakout at Campsfield Detention 
Centre and more...<FONT FACE="Arial, Helvetica, sans-serif"></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news635.htm"><B>SchNEWS 
635, 13th June 2008</B></A></FONT><FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"><B>.</B></FONT><FONT SIZE="4"><STRONG><BR> 
</STRONG></FONT><STRONG> Cock "n" Kabul Story </STRONG> - How the US / UK plan 
to devastate and then "reconstruct" Afghanistan is paying dividends (for some)...<FONT FACE="Arial, Helvetica, sans-serif">Plus, 
</FONT> Brits prepare to give Dubya a decent send off, BNP festival under threat 
after anti-fascist actions, <FONT FACE="Arial, Helvetica, sans-serif">government 
plans to bribe communities to swallow nuclear waste, Guantanamo inmates get right 
to civilian trial - maybe, and more... </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news634.htm"><B>SchNEWS 
634, 6th June 2008</B></A></FONT><FONT SIZE="4"><STRONG><BR> </STRONG></FONT><STRONG> 
Academic Freedom</STRONG> - As Nottingham post-grad faces deportation after failed 
terror arrest.... <FONT FACE="Arial, Helvetica, sans-serif">Plus, </FONT> Full 
Report into the Smash-Edo Carnival Against the Arms Trade, Animal Rights website 
owner sentenced to four and a half years, <FONT FACE="Arial, Helvetica, sans-serif">third 
week of hunger strike for Czech protesters against US "Star Wars" military bases, 
Israel at 60, and more... </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news633.htm"><B>SchNEWS 
633, 23rd May, 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><BR> </FONT><STRONG>Self Defence </STRONG> - Peace activist 
students on "aggravated trespass" charges denied legal aid... <FONT FACE="Arial, Helvetica, sans-serif">Plus, 
</FONT> Wrexham woman on the wrong side of the law when she takes plastic chairs 
from a council tip, Bush visits U.K for last time as president, Nottingham university 
terror arrest signals wider clampdown<FONT FACE="Arial, Helvetica, sans-serif">, 
Mass tresspass called on site of proposed Derbyshire Opencast mine, and more... 
</FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news632.htm"><B>SchNEWS 
632, 16th May, 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><BR> </FONT><STRONG>Sects and the City </STRONG> - Cult 
friction as police harass anti-scientology protest in London... <FONT FACE="Arial, Helvetica, sans-serif">Plus, 
</FONT> Man jailed after massive animal rights conspiracy trial, Cardiff anarchist 
newsletter "Gagged" in libel shocker, Brighton squatters in occupied Methodist 
Church defy bailiffs,<FONT FACE="Arial, Helvetica, sans-serif"> Housing benefit 
snoopers to get latest lie-detecting gear, and more... </FONT><FONT FACE="Arial, Helvetica, sans-serif"></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news631.htm"><B>SchNEWS 
631, 9th May, 2008</B></A></FONT><FONT SIZE="4"><BR> </FONT><STRONG>Heckler at 
the Back </STRONG> - Protesters have targeted the Nottingham premises of Heckler 
&amp; Koch... <FONT FACE="Arial, Helvetica, sans-serif">P</FONT><FONT FACE="Arial, Helvetica, sans-serif">lus, 
</FONT> activists arrested last year after banner drops in protest against the 
widening of the M1 have their charges dropped, BASF trying another crop trial 
of a GM potato, Northern Petroleum trying to drill in an area of outstanding natural 
beauty in West Sussex,<FONT FACE="Arial, Helvetica, sans-serif"> and more... </FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news630.htm"><B>SchNEWS 
630, 2nd May, 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><BR> </FONT><STRONG>Snatch of the Day </STRONG> - No Borders 
actions around the country target Borders and Immigration Agency snatch squads... 
Plus, the SOCPA laws are to be replaced with a new armoury of anti-protest legislation, 
Southampton University becomes the latest victim of censorship when it is forced 
to pull the scheduled screening of the Smash EDO campaign film, bio-tech corporation 
Novartis is in SHAC"s sights for the World Day For Lab Animals, and more...<FONT FACE="Arial, Helvetica, sans-serif"></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news629.htm"><B>SchNEWS 
629, 18th April, 2008</B></A></FONT><FONT SIZE="4"><BR> </FONT><STRONG>Mayan the 
Force be with You</STRONG> - Thousands of indigenous Mayans are threatened with 
displacement by new hydroelectric dam project...<FONT FACE="Arial, Helvetica, sans-serif">Plus, 
</FONT> Adbusters have called for people to switch off all their electronic gadgets 
in Mental Detox Week, incoming UN Human Rights Council investigator into the Israeli 
oppression of Palestine, compares Isreal"s treatment of Palestine to Nazi Germany, 
US Raytheon arms manufacturers have their facility in Glenrothes, Scotland hit 
by d-lock action,<FONT FACE="Arial, Helvetica, sans-serif"> and more... </FONT><FONT FACE="Arial, Helvetica, sans-serif"></FONT></P><P><FONT FACE="Arial, Helvetica, sans-serif"><A HREF="news628.htm"><B>SchNEWS 
628, 11th April, 2008</B></A></FONT> <FONT SIZE="1" FACE="Arial, Helvetica, sans-serif"> 
</FONT><FONT SIZE="4"><STRONG><BR> </STRONG></FONT><STRONG><FONT FACE="Arial, Helvetica, sans-serif"> 
F.A.C.K. YOU - </FONT></STRONG> <FONT FACE="Arial, Helvetica, sans-serif">Ten 
years after the death of Simon Jones at Shoreham docks in an accident, SchNEWS 
asks whether workplace safety has </FONT>improved in the past decade... <FONT FACE="Arial, Helvetica, sans-serif">Plus, 
</FONT> hunger strike at Harmondsworth asylum seekers detention centre, MESHO, 
a 16 page, tabloid sized spoof paper has been released and will be distributed 
across Britain, <FONT FACE="Arial, Helvetica, sans-serif">Street Blitz is coming 
to London - where artists will install art around the capital in public place 
and more... </FONT><FONT FACE="Arial, Helvetica, sans-serif"></FONT></P></div>';