<?php



/*	==================================================================================================
{con00001}				DB FUNCTIONS */
function mysqlConnect()	{
	
	$conn	=	mysql_connect(DBHOST, DBUSER, DBPASS, true);
	$db		=	mysql_select_db(DBNAME, $conn);
	if (DEBUG) echo mysql_error();
	
	
	if (DEBUG) echo "<br>New mysql connection made<br>";
	return $conn;
		
}

function mysqlQuery($sql, $conn)	{
	
	if (!is_string($sql))	{
		if (DEBUG)	echo '<br>mysqlQuery :: sql not string<br>';
		return false;
	}
	
	$rs		=	mysql_query($sql, $conn);
	if (DEBUG) echo mysql_error();
	if (DEBUG) echo "<br>mysqlQuery :: SQL :: $sql<br>";
	
	return $rs;
}

function mysqlNumRows($rs, $conn)	{
	
	$num	=	mysql_num_rows($rs);
	return $num;
}

function mysqlGetRow($rs)	{
	
	return mysql_fetch_array($rs);
	
}

function mysqlGetInsertId($conn)	{
	
	if (!is_resource($conn)) {
		
		if (DEBUG)	echo "<br>mysqlGetInsertId :: conn not a resource <br>";
		return false;
	}
	$id		=	mysql_insert_id($conn);
	if (!is_numeric($id)) {
		if (DEBUG)	echo "<br>mysqlGetInsertId :: id not numeric <br>";
		return false;
	}
	return $id;
	
}



?>
