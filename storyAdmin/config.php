<?php

// DATABASE CONFIG
define('DBHOST', '127.0.0.1');
define('DBUSER', 'schnews');
define('DBPASS', "P@lm4crav");
define('DBNAME', 'schnews_db');


// EMAIL SETTINGS
define('SENDTYPE', 1);		//	1 = SENDMAIL / 0 = SMTP SERVER

// SMTP CONFIG
define('SMTPHOST',"mail.tendrousbeastie.com");
define('SMTPUSER',"andrewhome");
define('SMTPPASS',"peacefulsolution");
define('SMTPSENDER',"no-reply@schnews.org.uk");



// DEBUGGING
define('DEBUG', 0);



// SESSION COLLISION PREVENTION
define('SES', 'sdfs9d0e9fi903wi90wecjwe09cj');



// LOGIN DETAILS
define('USERNAME', 'schnews');
define('PASSWORD', 'CourtHse');

// 	SPAM NOTIFIER RECIPIENTS
define ('SPAMNOTICERECIPIENTS', 'schnews@tendrousbeastie.com');


// 	SPAM HAMMER DURATION
define ('SPAMHAMMERDURATION', 30);


// 	HOW MANY ISSUES TO SHOW ON THE LEFT BAR OF BACK ISSUES FOR EACH EDITION
define ('NUMBACKISSUES', 10);


// 	HOW MANY ISSUES TO SHOW AS PREVIOUS ISSUES ON THE HOME PAGE
define ('NUMPREVIOUSISSUES', 14);


//	HOW MANY ISSUES TO SHOW ON THE RECENT STORIES LINKS
define ('NUMRECENTSTORIESLINKS', 10);



define ('KEYWORDSHITSFORSTORYLINK', 5);
define ('DOSTORYKEYWORDS', 0);


//	How many editors must vote to authorise a feature before it goes live...
define ('NUM_FEATURE_VOTES', 2);

//	How many feature summaries to show on the right bar of the show_features.php file
define ('NUM_RIGHT_BAR_FEATURES', 15);

//	How many days since publication should a feature be considered new for...
define ('DAYS_FEATURE_IS_NEW', 14);


//	DEFINE SETTINGS
if (function_exists('getSettings'))	{
	$settings	=	getSettings();
	define ('SHOWKEYWORDS', $settings['keywords']);
	define ('SHOWMESSAGES', $settings['messages']);
	define ('SPAMCOUNT', $settings['spamCount']);
	define ('LINKCOUNT', $settings['linkCount']);
	define ('NUMKEYWORDS', $settings['numKeywords']);
	define ('SPAMHAMMER', $settings['hammerDuration']);
	define ('PLAIN_TEXT_MAILING_LIST_ADDRESS', $settings['txtMailingListAddress']);
}



// HOW MANY ISSUES TO SHOW ON THE STORY ADMIN EDITION LIST
define ('NUM_STORY_ADMIN_ISSUES', 100);



$smtp_host		=	SMTPHOST;
$smtp_user		=	SMTPUSER;
$smtp_pass		=	SMTPPASS;

























/* ===========================================================================

								TEXT STRINGS

/*	==========================================================================
			EDITARTICLE.PHP
			INFO ABOUT WHICH TAGS CAN BE USED */
$editArticle_storyHelp		=	"
<b><u>How links are handled...</u></b>

All URLs within the text you enter will be automatically turned into external links ( i.e. target=\"_BLANK\" ).

This is also true for email addresses.

Internal pointers to previous issues (e.g. see SchNEWS 631, 642) will be turned into internal links.

<div style=\"border-bottom: 1px solid #dddddd\">&nbsp;</div>




";























/* ===========================================================================

								CKEditor Settings

/*	========================================================================== */



//	Edit Article
$editArticle_ckeditor_settings		=	"

toolbar 	:
	[
		['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Image'],
		'/',
		['Font', 'FontSize', 'TextColor',  '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
	],

uiColor 	: 'ffffff',
height  	: '450px'

";




//	Edit Headline Summary
$editIssue_ckeditor_settings		=	"

toolbar 	:
	[
		['Bold', 'Italic', 'Underline']
	],

uiColor 	: 'ffffff',
height  	: '150px',
width		: '560px'

";




//	Edit Feature
$editFeature_ckeditor_settings		=	"

toolbar 	:
	[
		['Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', '-', 'Image'],
		'/',
		['Font', 'FontSize', 'TextColor',  '-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
	],

uiColor 	: 'ffffff',
height  	: '450px'

";


?>
