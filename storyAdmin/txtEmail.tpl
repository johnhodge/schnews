======================================================================


SchMOVIES - http://www.schnews.org.uk/schmovies/index.php


RAIDERS OF THE LOST ARCHIVE
Part one of the SchMOVIES collection 2009-2010

This DVD features a number of films which were held by Sussex police
for over a year following the raid and confiscation of all SchMOVIES
equipment during an intelligence gathering operation in June 2009
related to the Smash EDO campaign.



REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009

A new collection of twelve SchMOVIES covering the Smash EDO/ITT's
campaign efforts to shut down the Brighton based bomb factory since
the company sought its draconian injunction against protesters in 2005.



UNCERTIFIED - OUT NOW on DVD- SchMOVIES DVD Collection 2008
Films on this DVD include... The saga of On The verge � the film they
tried to ban, the Newhaven anti-incinerator campaign, Forgive us our
trespasses - as squatters take over an abandoned Brighton church,
Titnore Woods update, protests against BNP festival and more... To
view some of these films click here



'ON THE VERGE' - the Smash EDO campaign film

Screenings throughout the year, or download or order a copy.

For updates and details click here
http://www.schnews.org.uk/schmovies/index-on-the-verge.htm



TAKE THREE - SchMOVIES Collection DVD 2007
*** OUT NOW ***

This is the third SchMOVIES DVD collection featuring short direct
action films produced by SchMOVIES in 2007, covering Hill Of Tara
Protests, Smash EDO, Naked Bike Ride, The No Borders Camp at Gatwick,
Class War plus many others. To buy a copy click here:
http://www.schnews.org.uk/schmovies/index.php#take-three


To download (free) and watch a selection of SchMOVIES short films click
here http://www.schnews.org.uk/schmovies


======================================================================


PARTY & PROTEST
What's On? Check out our Party and Protest guide at
www.schnews.org.uk/pap/guide.php - it's updated every week, has
sections on
regular events, local events, protest camps and more...


======================================================================

https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=paypal%40schnews%2eorg%2euk&item_name=Donation%20to%20SchNEWS&item_number=Donation%20to%20SchNEWS&no_shipping=1&cn=Notes&tax=0&currency_code=GBP&bn=PP%2dDonationsBF&charset=UTF%2d8

------------------------------------------------------------------

HOW TO UN/SUBSCRIBE TO SCHNEWS-LETTER

To unsubscribe, go to the website and follow the instructions
there, or send a message to webmaster@schnews.org.uk with subject
UNSUBSCRIBE PLAIN TEXT - email@address.com

To subscribe, go to the website or email webmaster@schnews.org.uk
with subject SUBSCRIBE PLAIN TEXT - email@address.com

------------------------------------------------------------------

SchNEWS At Ten It costs �5 inc. p&p per book, further details as below.



PEACE DE RESISTANCE It costs �3.30 INC. p&p per book, further
details as below...this one comes with a free multimedia CD too!
SchNEWS Of The World - issues 301-350 for �4!! Past books are
goin' cheap... SchNEWSround issues 51-100 - SOLD OUT; SchNEWS
annual issues 101-150 - a snip at �2; Survival Handbook issues
151-200 - sold out; SchQUALL issues 201-250 - sold out; Yearbook
2001 issues 251-300 - bargain �3. All prices inc. p&p.
Cheques to 'Justice?' - Honest!

In addition to 50 issues of SchNEWS, each book contains articles,
photos, cartoons, subverts, a "yellow pages" list of contacts,
comedy etc. You can also order the books from a bookshop or
library.

Subscribe to SchNEWS: Send 1st Class stamps (e.g. 10 for next 9
issues) or donations (payable to Justice?). Or �15 for a year's
subscription, or the SchNEWS supporter's rate, �1 a week. Ask for
"originals" if you plan to copy and distribute. SchNEWS is
post-free to prisoners.

------------------------------------------------------------------

SchNEWS, c/o Community Base,
113 Queens Rd,
Brighton, BN1 3XG, UK
Phone: 01273 685913
Email: mail@schnews.org.uk
Web: www.schnews.org.uk

------------------------------------------------------------------

copyleft - information for action - copy and distribute!
