/**********************************************************************
	Version: FreeRichTextEditor.com Version 1.00.
	License: http://creativecommons.org/licenses/by/2.5/
	Description: Configuration File.
	Author: Copyright (C) 2006  Steven Ewing
**********************************************************************/

// Width of the rich text editor.
rteWidth = "560px";
// Name of the IFRAME (content editor).
rteName = "freeRTE";
// Name of the hidden form field.
rteFormName = "summary";
// Height of the rich text editor.
rteHeight = "200px";
// Path to the images folder.
rteImagePath = "RTE/images/";
// Path to insert form popup.
rteHTMLPathInsertForm = "RTE/html/insert_form.html";
// Path to insert checkbox popup.
rteHTMLPathInsertCheckbox = "RTE/html/insert_checkbox.html";
// Path to insert radio button popup.
rteHTMLPathInsertRadiobutton = "RTE/html/insert_radiobutton.html";
// Path to insert text area popup.
rteHTMLPathInsertTextArea = "RTE/html/insert_textarea.html";
// Path to insert submit button popup.
//rteHTMLPathInsertSubmit = "../freerte_v1html/insert_submit.html";
// Path to insert image submit button popup.
rteHTMLPathInsertImageSubmit = "RTE/html/insert_image_submit.html";
// Path to reset form button popup.
rteHTMLPathInsertReset = "RTE/html/insert_reset.html";
// Path to insert hidden form field popup.
rteHTMLPathInsertHidden = "RTE/html/insert_hidden.html";
// Path to insert password field popup.
rteHTMLPathInsertPassword = "RTE/html/insert_password.html";
// Path to insert text field popup.
rteHTMLPathInsertText = "RTE/html/insert_text.html";
// Path to insert table popup.
rteHTMLPathInsertTable = "RTE/html/insert_table.html";
// Path to edit table properties popup.
rteHTMLPathEditTable = "RTE/html/edit_table.html";
// Path to insert link popup.
rteHTMLPathInsertLink = "RTE/html/insert_link.html";
// Path to edit link popup.
rteHTMLPathEditLink = "RTE/html/edit_link.html";
// Path to insert image popup.
rteHTMLPathInsertImage = "RTE/html/insert_image.html";
// Format Menu (H1, H2, H3 etc etc).
rteFormat = false;
// Font Face Menu (Arial, Verdana etc etc).
rteFontFace = false;
// Font Size Menu (1, 2, etc etc).
rteFontSize = false;
// Font Color Menu.
rteFontColor = false;
// Bold Text Button.
rteBold = true;
// Italicize Text Button.
rteItalic = true;
// Underline Text Button.
rteUnderline = true;
// Strikethrough Text Button.
rteStrikeThrough = false;
// Left Justify Button.
rteLeftAlign = false;
// Center Justify Button.
rteCenterAlign = false;
// Right Justify Button.
rteRightAlign = false;
// Full Justify Button.
rteFullAlign = false;
// Insert Horizontal Rule Button.
rteHorizontalRule = false;
// Superscript Text Button.
rteSuperscript = false;
// Subscript Text Button.
rteSubscript = false;
// Insert Hyperlink Button.
rteLink = false;
// Remove Hyperlink Button.
rteUnlink = false;
// Insert Image Button.
rteImages = false;
// Remove Formatting Button.
rteRemoveFormat = false;
// Table Formatting Buttons.
rteTables = false;
// Insert an ordered list Button.
rteOrderedList = false;
// Insert an unordered list Button.
rteUnorderedList = false;
// Indent Button.
rteIndent = false;
// Outdent Button.
rteOutdent = false;
// Undo Button.
rteUndo = true;
// Redo Button.
rteRedo = true;
// Cut, Copy & Paste Buttons.
rteCutCopyPaste = false;
// Insert form button.
rteInsertForm = false;
// Insert checkbox button.
rteInsertCheckbox = false;
// Insert radio button.
rteInsertRadio = false;
// Insert textarea.
rteInsertTextArea = false;
// Insert submit button.
rteInsertSubmit = false;
// Insert image submit button.
rteInsertImageSubmit = false;
// Insert reset button.
rteInsertReset = false;
// Insert hidden field.
rteInsertHidden = false;
// Insert password field.
rteInsertPassword = false;
// Insert text field.
rteInsertTextField = false;
// Print Rich Text Area Content.
rtePrint = false;
// Select All of Rich Text Area Content.
rteSelectAll = false;
// Spell Checker.
rteSpellCheck = false;
// Show Preview Button.
rtePreviewMode = false;
// Show Code Edit Button.
rteCodeMode = false;
// Show Design Mode Button.
rteDesignMode = false;