<?php

require "functions.php";


/*
 * 	If $do then process the POST data, else draw the form
 */
$do 	=	false;
if (isset($_GET['mode']) && $_GET['mode'] == 1)	{
	
	$do 	=	true;
	
}



switch ($do)	{
	
	case false:
		
		$output		=	array();
		$output[]	=	"<html>";
		$output[]	=	"<head>";
		$output[]	=	"<title>HTML Link Inserter</title>";
		$output[]	=	"</head>";
		$output[]	=	"<body>";
		
		$intro		=	"Paste your text into this box and it will convert any links into HTML code...";
		$output[]	=	"<div><i>$intro</i></div>";
		
		$output[]	=	"<form method='POST' action='?mode=1'>";
		$output[]	=	"<div>";
		$output[]	=	"<textarea name='data' style='width: 600px; height: 600px; border: 2px solid #4444ff'></textarea>";
		$output[]	=	"</div>";
		$output[]	=	"<div>";
		$output[]	=	"<input type='submit' value='Convert Links' />";
		$output[]	=	"</div>";
		$output[]	=	"</form>";
		
		$output[]	=	"</body>";
		$output[]	=	"</html>";
		
		echo implode("\r\n", $output);
		
		break;
	
		
		
		
		
	case true:
		
		if (!isset($_POST['data']))	{
			
			echo "<div style='color:red'><b><i>Data not detected. Please use the browser's back button and enter some text.</i></b></div>";
			break;	
			
		}
		
		$data		=		$_POST['data'];
		$data		=		scanForLinks($data);
		$data		=		htmlentities($data);
		$data		=		nl2br($data);

		echo $data;
		
		break;
	
	
}


?>