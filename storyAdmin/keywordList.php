<?php

//	PROCESS GET VARS
if (isset($_GET['sort']) && is_numeric($_GET['sort']))	$sort	=	$_GET['sort'];
else $sort = 1;

if (isset($_GET['merge']))	{
	
		$mergeId	= $_GET['merge'];
		echo "<h3>You are merging keyword '<i style='color:red'>$mergeId</i>'<br /><br />Choose another keyword to merge into...</h3>";
		echo "<div style='border-bottom: 1px solid #777777; margin-bottom: 15px; width: 250px; position: relative; top:-7px'>&nbsp;</div>";
		echo "<a href='?keywordsList=1&sort=$sort'>Cancel Merge</a>";
		echo "<div style='border-bottom: 1px solid #777777; margin-bottom: 15px; width: 250px'>&nbsp;</div>";
		
}
else $mergeId	= 0;


if (isset($_GET['mergeFrom']) && isset($_GET['mergeTo']))		{
	
			$mergeFrom		=	cleaninput($_GET['mergeFrom']);
			$mergeTo		=	cleaninput($_GET['mergeTo']);
			
			if (mergeKeywords($mergeFrom, $mergeTo))	{
				
					echo "<h3>All keyword entries of '<i style='color:red'>$mergeFrom</i>' have been changed to '<i style='color:red'>$mergeTo</i>'</h3>";
					echo "<div style='border-bottom: 1px solid #777777; margin-bottom: 15px; width: 250px'>&nbsp;</div>";
					
			}	else {
				
					echo "<h3 style='color:red'>There was a problem merging the keywords</h3>";
					echo "<div style='border-bottom: 1px solid #777777; margin-bottom: 15px; width: 250px'>&nbsp;</div>";
			}
		
	
}


echo "<p><a id='toggleInstructions1' class='javaLink'>Toggle Instructions</a></p>";

		echo "<div id='instructions' class='instructions'>";	
			echo "<p><h3>Instructions</h3></p>";
			echo "<p>This page lists all the keywords for the stories</p>";
			echo "<p>It is useful for making sure that the keywords you add to your stories match up to the existing keywords for previous stories.</p>";
			echo "<p>&nbsp;</p>";
			echo "<p>You can also merge on keyword into another. This is useful when two very similar keywords exist - one of the keywords can be merged into the other. This means that more stories will share the same keywords..</p>";
			echo "<p>&nbsp;</p>";
			echo "<p>What are keywords? <a href='tutorials/keywords.html' target='_blank'>See here</a>";
		echo "</div>";
		
echo "<div style='border-bottom: 1px solid #777777; margin-bottom: 15px; width: 250px; position: relative; top:-7px'>&nbsp;</div>";		
		


$keywordList	= getAllRankedKeywords();

if ($sort == 1)	$keywordList	=	sortArray($keywordList);

$style		=	" style=\"padding:4px; border:1px solid #999999\" ";

echo 	"<table>
			<tr>
				<th $style><a href=\"?keywordsList=1&sort=0\" onMouseOver=\"Tip('$tip_keywords_keywordSort')\">Keyword</a></th><th $style><a href=\"?keywordsList=1&sort=1\" onMouseOver=\"Tip('$tip_keywords_numericSort')\">Num Hits</a></th>
			</tr>";



foreach ($keywordList as $keyword => $numHits	)	{
	
		echo "<tr><td $style>$keyword</td><td $style>$numHits</td>";
		if ($mergeId == '0')	echo "<td><a href='?keywordsList=1&sort=$sort&merge=".urlencode($keyword)."' onMouseOver=\"Tip('$tip_keywords_merge')\">Merge Keyword</a></td>";		
		else	echo "<td><a href='?keywordsList=1&sort=$sort&mergeFrom=".urlencode($mergeId)."&mergeTo=".urlencode($keyword)."'>Merge '<b style='font-size:12px'>$mergeId</b>' Into</a>				
		</td>";		
		echo "</tr>";
	
}

echo "<tr><td style=\"border: 0px solid white\"><br /></td></tr>";

echo "<tr><th $style>Total Unique Keywords</th><th $style>Total Hits</th></tr>";
echo "<tr><td $style>".count($keywordList)."</td><td $style>".array_sum($keywordList)."</td></tr>";
echo "<tr><th $style>Ratio</th><td $style>".round(count($keywordList)/array_sum($keywordList), 3)."</td></tr>";
echo "</table>";



?>