<?php



/*	===================================================================================================
{dandt00002}					DATE AND TIME FUNCTIONS */



function makeSqlDatetime()
{

		$date 		=	date('Y')."-".date('m')."-".date('d');
		$time		=	date('H').":".date('i').":".date('s');
		
		return	$date." ".$time;
		
}


function make_sql_datetime()	{
	
		return makeSqlDatetime();
	
}

function make_time_from_sql($datetime)	{
	
		return makeTimeFromSql($datetime);
	
}

function makeTimeFromSql($datetime)	{
	
	
		$minute		=		substr($datetime, 14, 2);
		$hour		=		substr($datetime, 11, 2);
		
		return	$hour.":".$minute;
		
}

if (!function_exists('make_date_from_sql'))	{
function make_date_from_sql($datetime)	{
	
	return makeHumanDate(substr($datetime, 0, 10));
	
}
}


/* Convert an SQL date into a huma readable date string, in either UK or US format */

/* Format options = uk (dd-mm-yyyy) / us (mm-dd-yyyy) / words (ddth month yyyy) */
function makeHumanDate($date, $format = "uk")	{
	
	if (strlen($date) == 19) $date = substr($date, 0 , 10);
	
	if (strlen($date) != 10)		return false;
	
	if ($format != "uk" && $format != "us" && $format != "words" && $format != "words+day") $format = "uk";
	
	
	$y		=	substr($date, 0, 4);
	$m		=	substr($date, 5, 2);
	$d		=	substr($date, 8, 2);
	
	if (!is_numeric($y) || !is_numeric($m) || !is_numeric($d)) return false;
	
	switch ($format)	{
		
		default:
			return "$d-$m-$y";
			break;
		
		case 'us':
			return "$m-$d-$y";
			break;
			
		case "words":
			
			if (strlen($m) == 2 && substr($m, 0, 1) == 0) $m = substr($m, 1,1);
			$months 	=	array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
									7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
			$m			=	$months[$m];
			
			$st			=	array(1,21,31);
			$nd			=	array(2, 22);
			$rd			=	array(3,23);
			$th			=	array(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,24,25,26,27,28,29,30);
			
			if (strlen($d)	==	2 && substr($d, 0, 1) == '0')	{
				
					$d2	=	substr($d, 1, 2);
				
			}	else $d2	=	$d;
			if (in_array($d, $st))	$d2	=	$d2.'st';
			if (in_array($d, $nd))	$d2	=	$d2.'nd';
			if (in_array($d, $rd))	$d2	=	$d2.'rd';
			if (in_array($d, $th))	$d2	=	$d2.'th';
			
			return "$d2 $m $y";
			break;
			
		case "words+day":
			
			if (strlen($m) == 2 && substr($m, 0, 1) == 0) $m = substr($m, 1,1);
			$months 	=	array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
									7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
			$m			=	$months[$m];
			
			$st			=	array(1,21,31);
			$nd			=	array(2, 22);
			$rd			=	array(3,23);
			$th			=	array(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,24,25,26,27,28,29,30);
			
			if (strlen($d)	==	2 && substr($d, 0, 1) == '0')	{
				
					$d2	=	substr($d, 1, 2);
				
			}	else $d2	=	$d;
			if (in_array($d, $st))	$d2	=	$d2.'st';
			if (in_array($d, $nd))	$d2	=	$d2.'nd';
			if (in_array($d, $rd))	$d2	=	$d2.'rd';
			if (in_array($d, $th))	$d2	=	$d2.'th';
			
			return date('l',strtotime($date))." $d2 $m $y";
			break;
			
	}	
	
}


/* Returns a formatted set of FORM SELECTs for day, month and year, for the user
to select a date with.

Each SELECT is named as $name.'_day', $name.'_month' and $name.'_year'. */
function makeDateSelector($name, $date){
		
			if ($name == "" || $name === null) return false;
		
			if (strlen($date) == 10)	{
					$y		=	substr($date, 0, 4);
					if (!is_numeric($y)) return false;
					
					$m		=	substr($date, 5, 2);
					if (!is_numeric($m)) return false;
					
					$d		=	substr($date, 8, 2);
					if (!is_numeric($d)) return false;
				
			}	else {
				
					$d		=	'';
					$m		=	'';
					$y		=	'';
				
			}
			
			
			
			// DAY SELECTOR
						
			// Make an array of the 31 days
			for ($n = 0; $n <= 31; $n++)	{
					
					// Add a preceding zero to single digit values
					if (strlen($n) == 1)	{	
							if ($n == 0)	$days[] = "";
							else $days[]		=	"0".$n; 
							$values[] = "0".$n;	
					}
					else { $days[]	=	$n; $values[] = $n;	}
			
			}	
			$options	=	'';
			foreach ($days as $key=>$day)	{
				
				$options 	.=	"<option value=\"{$values[$key]}\" ";
				if ($d == $values[$key]) $options .= " selected ";
				$options	.=	" >$day</option>";
				
			}
			
			$output	= "<select name=\"".$name."_day\" >".$options."</select> ";
				
			
			
			
			// MONTH SELECTOR
			$months		=	array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
			$values		=	array("00", "01","02","03","04","05","06","07","08","09","10","11","12");
			
			
			
			$options	=	'';
			foreach ($months as $key=>$month)	{
				
				$options 	.=	"<option value=\"{$values[$key]}\" ";
				if ($m == $values[$key]) $options .= " selected ";				
				$options 	.=	" >$month</option>";
				
			}
			
			$output	.= "<select name=\"".$name."_month\" >".$options."</select> ";
			
			
			
			unset ($values);
			// YEAR SELECTER
			$years[]		=	"";
			$values[]		=	"0000";
			
			for ($n = 2003; $n<=2015; $n++){
				
					$years[] = $n;
					$values[] = $n;				
					
			}
						
			$options	=	'';
			foreach ($years as $key=>$year)	{
				
				$options 	.=	"<option value=\"{$values[$key]}\" ";
				if ($y == $values[$key]) $options .= " selected ";
				$options	.=	" >$year</option>";
				
			}
			
			$output	.= "<select name=\"".$name."_year\" >".$options."</select>";
			
			return $output;
			
	}
	
	
	

/* Takes a set of POST data create by the makeDateSelector function and parses it back
into an SQL date */
function makeDateFromPostData($dateVarName, $post)	{
	
	/* The dateVarName must be a set string. It specifies the base name of the POST elements */
	if ($dateVarName == '' || is_null($dateVarName))	{
		if (DEBUG) echo "<br>makeDateFromPostData :: dateVarName is empty or null ;; returning FALSE<br>";
			return false;		
	}
	
	/* The 3 POST elements must be set, one for day, one for month and one for year */
	if (!isset($post[$dateVarName.'_day'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: day not set in post data ;; returning FALSE<br>";
			return false;		
	}
	
	if (!isset($post[$dateVarName.'_month'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: month not set in post data ;; returning FALSE<br>";
			return false;		
	}
	
	if (!isset($post[$dateVarName.'_year'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: year not set in post data ;; returning FALSE<br>";
			return false;		
	}
	
	
	/* The 3 POST elements must also all be numerics */
	if (!is_numeric($post[$dateVarName.'_day'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: day not numeric in post data ;; returning FALSE<br>";
			return false;		
	}
	
	if (!is_numeric($post[$dateVarName.'_month'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: month not numeric in post data ;; returning FALSE<br>";
			return false;		
	}
	
	if (!is_numeric($post[$dateVarName.'_year'])) {
			if (DEBUG) echo "<br>makeDateFromPostData :: year not numeric in post data ;; returning FALSE<br>";
			return false;		
	}
	
	
	/* STORE THE POST ELEMENTS IN TEMP VARS */
	$day	=	$post[$dateVarName.'_day'];
	$month	=	$post[$dateVarName.'_month'];
	$year	=	$post[$dateVarName.'_year'];
	
	
	/* IF DAY OR MONTH ARE ONLY ONE CHAR THEN PREPEND A 0 ZERO
	I.E. '5' BECOMES '05' */
	if (strlen($day) == 1) 		$day		=	'0'.$day;
	if (strlen($month) == 1)	$month		=	'0'.$month;
	
	
	/* DAY AND MONTH SHOULD BOTH BE TWO CHARS LONG.
	YEAR SHOULD BE FOUR.
	
	WE HAVE A PROBLEM AND NEED TO BAIL IF THIS ISN'T THE CASE */
	if (strlen($day) != 2) {
			if (DEBUG) echo "<br>makeDateFromPostData :: day is not 2 chars long ;; returning FALSE<br>";
			return false;		
	}
	if (strlen($month) != 2) {
			if (DEBUG) echo "<br>makeDateFromPostData :: month is not 2 chars long ;; returning FALSE<br>";
			return false;		
	}
	if (strlen($year) != 4) {
			if (DEBUG) echo "<br>makeDateFromPostData :: year is not 4 chars long ;; returning FALSE<br>";
			return false;		
	}
	
	return $year.'-'.$month.'-'.$day;
	
}


/* TAKES AN SQL DATE AS INPUT AND TESTS TO SEE WHETHER IT EXISTS (I.E. JULY 54TH WOULD FAIL)

OPTIONALLY, THE SECOND ARGUEMNET WILL CAUSE THE FUNCTION TO FIX THE DATE IF IT 
IS OUT OF RANGE.

0 = DON'T FIX
1 = BRING DATE DOWN TO THE NEXT ACCEPTABLE DATE (i.e. 2008-02-35 would become 2008-02-28)
2 = BRING IT UP TO THE NEXT ACCEPTABLE DATE (i.e. 2008-02-35 would become 2008-03-01) */
function testDateIsInRage($date, $fix = 0)	{
	
	/* DATE SHOULD BE 10 CHARS LONG IS IT IS A VALID SQL DATE */
	if (strlen($date) != 10) {
			if (DEBUG) echo "<br>testDataIsInRange :: date is not 10 chars chars long ;; returning FALSE<br>";
			return false;		
	}
	
	/* DATE SHOULD CONTAIN 2 HYPENS */
	if (strlen(str_replace('-', '', $date)) != 8) {
			if (DEBUG) echo "<br>testDataIsInRange :: date does not contain 2 hypens ;; returning FALSE<br>";
			return false;		
	}
	
	/* ALL OTHER CHARS SHOULD BE NUMBERS */
	if (!is_numeric((str_replace('-', '', $date)))) {
			if (DEBUG) echo "<br>testDataIsInRange :: date does not contain only numbers and hypens ;; returning FALSE<br>";
			return false;		
	}
	
	$day	=	substr($date, 8, 2);
	$month	=	substr($date, 5, 2);
	$year	=	substr($date, 0, 4);
	
	
	
}



function is_date_x_days_old($date, $num_days)	{

		if (strlen($date) < 10)	return false;
		if (substr($date, 4, 1) != '-' || substr($date, 7, 1) != '-')	return false;
		if (!is_numeric(str_replace('-', '', substr($date, 0, 10)))) return false;
		
		$date		=	substr($date, 0, 10);
		
		$day		=	3600 * 24;					//	Numer of seconds in one day
			
		$timestamp	=	strtotime($date);			//	Time stamp of our source date
		
		$now 		=	date("U");							//	Time stamp now
		$then		=	$now	-	($day * $num_days);		//	Time stamp of now minus our input number of days
		
		//	If our input timestamp is greater than the then timestamp then the injput date is with the input number of days...
		if ($then < $timestamp)	{
		
			return true;
			
		}	else	{
		
			return false;
		
		}
		
}


?>
