<?php

/*
 * 	Returns an indexed array of the SchMOVIES categories, where the key is
 * 	the MySQL ID and the value is the category.
 */
function get_schmovies_categories()	{

	//	Grab categories from DB...
	$conn		=	mysqlConnect();
	$sql		=	"	SELECT *
						FROM schmovies_categories
						ORDER BY category ASC
						";
	$rs			=	mysqlQuery($sql, $conn);


	//	Put together the data into an array...
	$categories		=	array();
	while ($cat	=	mysqlGetRow($rs))	{

		$categories[$cat['id']]		=	$cat['category'];

	}

	//	Close the SQl connection and return out of function...
	mysql_close($conn);
	return $categories;

}





/*
 *	Checks the $error array to see if an error message corresponds that to
 *	$name value, and returns a formatted HTML table row is so...
 */
function show_schmovies_error_message($name, $errors)	{

	//	Test inputs
	if (!is_string($name))	{

		if (DEBUG) echo "<div style='color:red'>Input name not a string</div>";
		return false;

	}
	if (!is_array($errors))	{

		if (DEBUG) echo "<div style='color:red'>Input errors not a array</div>";
		return false;

	}

	// If the error message exists then format it and return as HTML...
	if (isset($errors[$name]))	{

			$output		=	array();
			$output[]	=	"<tr>";
			$output[]	=	"<td colspan='2'>";
			$output[]	=	"<div class='errorMessage' align='center'>".$errors[$name].'</div>';
			$output[]	=	'</td>';
			$output[]	=	'</tr>';

			return implode("\r\n", $output);

	}

	return '';
}




/*
 * 	Returns the category name based on the given cateogry ID...
 */
function get_schmovies_category($cat_id)	{

	//	Test inputs...
	if (!is_numeric($cat_id) || $cat_id == 0)	return false;


	//	Do DB Lookup
	$conn		=	mysqlConnect();
	$sql		=	"	SELECT category
						FROM schmovies_categories
						WHERE id = $cat_id	";
	$rs			=	mysqlQuery($sql, $conn);


	//	If there isn't just one then return with an false
	if (mysqlNumRows($rs, $conn) != 1)	return false;


	//	Return data...
	$res		=	mysqlGetRow($rs);
	mysql_close($conn);
	return stripslashes($res['category']);

}





function make_schmovie_item($id)	{

	//	Test Inputs...
	if (!is_numeric($id) || $id == 0)	return false;


	//	Grab data from DB
	$conn		=	mysqlConnect();
	$sql		=	"	SELECT *
						FROM schmovies
						WHERE id = $id	";
	$rs			=	mysqlQuery($sql, $conn);


	//	We are expecting only 1 results. Fail if this is not the case...
	if (mysqlNumRows($rs, $conn) != 1)	return false;


	//	Store a local copy of the item and close the MySQL connection...
	$item		=	mysqlGetRow($rs);
	mysql_close($conn);


	//	=================================================================
	//				Create HTML Formatted Item...
	//	=================================================================
	$output		=	array();

	//	Open Table
	$output[]	=	"<table class='schmovie_item_main_table'><tr><td>";

	//	Image
	$output[]	=	"<div class='schmovie_item_image_small_container'>";
	$large_image_set	=	false;
	if ($large_image_link	=	process_schmovie_image_link(stripslashes($item['image_large'])))	{

			$output[]	=	"<a href='$large_image_link' target='_BLANK'>";
			$large_image_set	=	true;
	}
	$output[]	=	"<img src='".process_schmovie_image_link(stripslashes($item['image_small']))."' class='schmovie_item_image_small' />";

	if ($large_image_set)	{

		$output[]	=	"<div class='schmovie_item_image_small_link'>";
		$output[]	=	'Click Here for Larger Image';
		$output[]	=	'</div>';#
		$output[]	=	'</a>';

	}
	$output[]	=	'</div>';

	//	Title
	$output[]	=	"<div class='schmovie_item_title'>";
	$output[]	=	stripslashes($item['name']);
	$output[]	=	"</div>";

	//	Headline
	$output[]	=	"<div class='schmovie_item_headline'>";
	$output[]	=	stripslashes($item['headline']);
	$output[]	=	"</div>";

	//	Main body text
	$output[]	=	"<div class='schmovie_item_body'>";
	$output[]	=	stripslashes(nl2br($item['body_text']));
	$output[]	=	"</div>";

	//	Close Table
	$output[]	=	"</td></tr></table>";


	//	Return data
	return implode("\r\n", $output);

}



/*
 * 	Takes an image filename as an input, and returns the full path
 * 	to the image, automaticaly locating the relevant directory structure...
 *
 * 	If the image cannot be found it returns false.
 */
function process_schmovie_image_link($filename)	{

	//	Test inputs...
	if (!is_string($filename) || $filename == '')	return false;


	/*
	 *	We first assume that it should be in the SchMOVIES -> Images folder, so
	 *	we'll check here first...
	 */
	$path		=	'../schmovies/images/'.$filename;
	if (file_exists($path))	return $path;


	/*
	 * 	Otherwise, we'll test the various images dirs in the server root directory...
	 */
	$dirs		=	array('images', 'images_main', 'images_extra', 'images_menu', 'images_main-01');
	foreach ($dirs as $dir)	{

		$path	=	"../$dir/".$filename;
		if (file_exists($path))	return $path;

	}


	/*
	 * 	Otherwise we have to conclude that the file is not locatable on
	 * 	the server, and return false...
	 */
	return false;


}




/*
 * 	Finds the number of SchMOVIES that are listed under a given category
 */
function get_num_schmovies_for_category($cat_id)	{

	//	Test Inputs...
	if (!is_numeric($cat_id) || $cat_id == 0)	return false;


	//	Do Db Lookup
	$conn		=	mysqlConnect();
	$sql		=	" SELECT count(*) as num_hits FROM schmovies WHERE category = $cat_id";
	$rs			=	mysqlQuery($sql, $conn);
	$res		=	mysqlGetRow($rs);
	$num_hits	=	$res['num_hits'];
	mysql_close($conn);


	//	Test results and return...
	if (!is_numeric($num_hits))	return 0;
	return	$num_hits;


}


?>