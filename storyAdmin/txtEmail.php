<?php
/* TXT EMAIL VISUALISER

AUTHOR: ANDREW WINTERBOTTOM support@tendrousbeastie.com www.tendrousbeastie.com
LICENSING: GPL V2.
COMPATIBILITY: PHP4+ MYSQL 4+ XHTML

*/


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';




/* DATA PROCESSING SECTION */

// GET ISSUE ID
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{
	
	$issueId	=	$_GET['issue'];
		
}

if (!isset($issueId))	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: issueId not set ;; exiting silently <br>";
	exit;
}


require '../storyAdmin/storyCreationFunctions.php';
echo makeFullTXTEmail($issueId);

exit;

?>












