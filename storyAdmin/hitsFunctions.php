<?php 


/*
 * 	---	CONFIG --- 
 */

//	DATABASE ACCESS
if (!defined('DBHOST'))		define('DBHOST', 'localhost');
if (!defined('DBUSER'))		define('DBUSER', 'schnews');
if (!defined('DBPASS'))		define('DBPASS', "P@lm4crav");
if (!defined('DBNAME'))		define('DBNAME', 'schnews_db');

//	IF ENABLED WILL SHOW MYSQL DEBUGGING ON SCREEN
if (!defined('DEBUG'))			define('DEBUG', 0);

//	THE TITLE OF THE SITE THAT IS BEING ANALYSED
if (!defined('TITLE'))			define('TITLE', 'SchNEWS');

// 	IF ENABLED, WILL REQUIRE THE SESSION['LOGGEDIN'] VAR TO BE TRUE;
if (!defined('REQUIRELOGIN'))	define('REQUIRELOGIN', 0);

// 	VERSION NUMBER OF THIS SCRIPT, DISPLAYED ON SCREEN
if (!defined('VERSION_NUM'))	define('VERSION_NUM', '2.0');

// 	CONTROLS THE NUMBER OF RESULTS TO SHOW FOR BROWSER, REFERRED, AND IP ADRESS LISTS...
if (!defined('NUM_TO_SHOW'))	define('NUM_TO_SHOW', 30);

// 	WHICH PAGENAME IS THE DEFAULT HOMEPAGE. UNIQUE VISITORS WILL BE CALCULATED USING HITS TO THE HOMEPAGE.
if (!defined('HOMEPAGE'))		define('HOMEPAGE', 'home');




/*
 * 	LIST OF BROWSER ABBREVIATIONS
 * 
 * 	THIS IS USED TO IDENTIFY THE BROWSER CLASS BASED ON THE HTTP HEADER DATA
 */
$listOfBrowsers		=	array(		'Yahoo! Slurp'	=>	'Yahoo Spider',
									'Baiduspider' 	=> 	'Baiduspider',
									'Googlebot'		=>	'Google Spider (Googlebot)',
									'MSIE 6'		=> 	'Internet Explorer 6',
									'MSIE 7'		=> 	'Internet Explorer 7',
									'MSIE 8'		=> 	'Internet Explorer 8',
									'MSIE 5'		=> 	'Internet Explorer 5',
									'msnbot'		=>	'MSN Spider (msnbot)',
									'Opera'			=>	'Opera',
									'Firefox'		=> 	'Firefox',
									'Ask Jeeves'	=> 	'Ask Jeeves',
									'Safari'		=>	'Safari'
);


function ht_mysqlConnect()	{
	
	$conn	=	mysql_connect(DBHOST, DBUSER, DBPASS, true);
	$db		=	mysql_select_db(DBNAME, $conn);
	if (DEBUG) echo mysql_error();
	
	
	if (DEBUG) echo "<br>New mysql connection made<br>";
	return $conn;
		
}

function ht_mysqlQuery($sql, $conn)	{
	
	if (!is_string($sql))	{
		if (DEBUG)	echo '<br>mysqlQuery :: sql not string<br>';
		return false;
	}
	
	$rs		=	mysql_query($sql, $conn);
	if (DEBUG) echo mysql_error();
	if (DEBUG) echo "<br>mysqlQuery :: SQL :: $sql<br>";
	
	return $rs;
}

function ht_mysqlNumRows($rs, $conn)	{
	
	$num	=	mysql_num_rows($rs);
	return $num;
}

function ht_mysqlGetRow($rs)	{
	
	return mysql_fetch_array($rs);
	
}

function ht_mysqlGetInsertId($conn)	{
	
	if (!is_resource($conn)) {
		
		if (DEBUG)	echo "<br>mysqlGetInsertId :: conn not a resource <br>";
		return false;
	}
	$id		=	mysql_insert_id($conn);
	if (!is_numeric($id)) {
		if (DEBUG)	echo "<br>mysqlGetInsertId :: id not numeric <br>";
		return false;
	}
	return $id;
	
}







//	===============================================================================================
//	===============================================================================================


function incrementHitCount($page)	{
	
	$conn		=	ht_mysqlConnect();
	
	$ip			=	mysql_escape_string($_SERVER['REMOTE_ADDR']);
	$browser	=	mysql_escape_string($_SERVER['HTTP_USER_AGENT']);
	//$browser	=	explode(' ', $browser);
	//$browser 	=	$browser[0];
	$date		=	substr(makeSqlDatetime(), 0, 10);
	if (isset($_SERVER['HTTP_REFERER']))	{
	
			$source		=	trimSource(mysql_escape_string($_SERVER['HTTP_REFERER']));
			$search		=	getSearchTermFromURL(mysql_escape_string($_SERVER['HTTP_REFERER']));
	
	}	else	{
		
			$source 	=	'';
			$search		=	'';
		
	}
	
	if (DEBUG)	{
			
			echo "<p>HTTP_REFERER = '".$_SERVER['HTTP_REFERER']."'</p>";

	}
	
	global $listOfBrowsers;
	foreach	($listOfBrowsers as $ht_browser => $nameOfBrowser)	{
		
			if (strpos($browser, $ht_browser) !== false)	$browser 	=	$nameOfBrowser;		
		
	}
	
	$sql		=	" INSERT INTO hits 
						(date, ip_address, page, browser, source, search)
					VALUES
						('$date', '$ip', '$page', '$browser', '$source', '$search')";
	$rs			=	ht_mysqlQuery($sql, $conn);

	return true;
	
}


function subDaysFromDate($date, $offset, $mode = 'sub')	{
		
		if ($mode != 'sub' && $mode != 'add') $mode = 'sub';
	
		if (!$date		=	strtotime($date)) return $date;
		
		$offsetSeconds	=	$offset * 24 * 3600;
		
		if ($mode 	==	'sub')	{

				$date		-=		$offsetSeconds;
			
		}	else	{
			
				$date		+=		$offsetSeconds;
			
		}
		
		$date		=	date('Y-m-d', $date);
		
		return $date;		
	
}



function trimSource($url)	{
	
		if ($url == '' || !is_string($url)) return $url;
		
		if (substr($url, 0, 7) == 'http://')		$offset 	=	7;
		else										$offset 	=	0;
		
		$posSlash		=	strpos($url, '/', $offset);
		
		$trimmedURL		=	substr($url, 0, $posSlash);
		
		return $trimmedURL;
		
}



function getSearchTermFromURL($url)	{
	
		if ($url == '' || !is_string($url)) return $url;
		
		
		
		//	TEST FOR GOOGLE SEARCH...
		if (!$posSearchVar		=	strpos($url, '&q='))	{
			
			if (!$posSearchVar		=	strpos($url, '?q='))	{
					
				return '';
				
			}
				
			
		}
		if (DEBUG)	echo "Search URL = '$url'";
		$posSearchVar 			+= 	strlen('?q=');
		$searchTerm				=	substr($url, $posSearchVar, strpos($url, '&', $posSearchVar) - $posSearchVar);
		$searchTerm				=	str_replace('+', ' ', $searchTerm);
		return strtolower($searchTerm);
	
}



function makeSqlDatetime()
{

		$date 		=	date('Y')."-".date('m')."-".date('d');
		$time		=	date('H').":".date('i').":".date('s');
		
		return	$date." ".$time;
		
}


function makeHumanDate($date, $format = "uk")	{
	
	if (strlen($date) == 19) $date = substr($date, 0 , 10);
	
	if (strlen($date) != 10)		return false;
	
	if ($format != "uk" && $format != "us" && $format != "words" && $format != "words+day") $format = "uk";
	
	
	$y		=	substr($date, 0, 4);
	$m		=	substr($date, 5, 2);
	$d		=	substr($date, 8, 2);
	
	if (!is_numeric($y) || !is_numeric($m) || !is_numeric($d)) return false;
	
	switch ($format)	{
		
		default:
			return "$d-$m-$y";
			break;
		
		case 'us':
			return "$m-$d-$y";
			break;
			
		case "words":
			
			if (strlen($m) == 2 && substr($m, 0, 1) == 0) $m = substr($m, 1,1);
			$months 	=	array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
									7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
			$m			=	$months[$m];
			
			$st			=	array(1,21,31);
			$nd			=	array(2, 22);
			$rd			=	array(3,23);
			$th			=	array(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,24,25,26,27,28,29,30);
			
			if (strlen($d)	==	2 && substr($d, 0, 1) == '0')	{
				
					$d2	=	substr($d, 1, 2);
				
			}	else $d2	=	$d;
			if (in_array($d, $st))	$d2	=	$d2.'st';
			if (in_array($d, $nd))	$d2	=	$d2.'nd';
			if (in_array($d, $rd))	$d2	=	$d2.'rd';
			if (in_array($d, $th))	$d2	=	$d2.'th';
			
			return "$d2 $m $y";
			break;
			
		case "words+day":
			
			if (strlen($m) == 2 && substr($m, 0, 1) == 0) $m = substr($m, 1,1);
			$months 	=	array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June',
									7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
			$m			=	$months[$m];
			
			$st			=	array(1,21,31);
			$nd			=	array(2, 22);
			$rd			=	array(3,23);
			$th			=	array(4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,24,25,26,27,28,29,30);
			
			if (strlen($d)	==	2 && substr($d, 0, 1) == '0')	{
				
					$d2	=	substr($d, 1, 2);
				
			}	else $d2	=	$d;
			if (in_array($d, $st))	$d2	=	$d2.'st';
			if (in_array($d, $nd))	$d2	=	$d2.'nd';
			if (in_array($d, $rd))	$d2	=	$d2.'rd';
			if (in_array($d, $th))	$d2	=	$d2.'th';
			
			return date('l',strtotime($date))." $d2 $m $y";
			break;
			
	}	
	
}

?>