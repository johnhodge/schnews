<?php

if (!$_SESSION[SES.'loggedin']) exit;

// TITLE
echo "<h2>Current Editions</h2>";
echo "<p>............</p>";





//	=================================
// PREP THE GET VARIABLES

// SHOW ISSUE IS SET IF WE ARE DRILLING INTO AN ISSUE
if (isset($_GET['showIssue']) && is_numeric($_GET['showIssue']))	$showIssue = $_GET['showIssue'];
	else $showIssue = 0;

// SHOW ARTICLE IS SET IF WE ARE DRILLING INTO AN ARTICLE WITHIN A GIVEN ISSUE
if (isset($_GET['showArticle']) && is_numeric($_GET['showArticle']))	$showArticle = $_GET['showArticle'];
	else $showArticle = 0;

//	RAISE AND LOWER AND ARTICLE
if (isset($_GET['raiseArticle']) && is_numeric($_GET['raiseArticle']))	{

	$raiseArticle		=	$_GET['raiseArticle'];
	raiseArticle($raiseArticle, 'raise');

}
if (isset($_GET['lowerArticle']) && is_numeric($_GET['lowerArticle']))	{

	$lowerArticle		=	$_GET['lowerArticle'];
	raiseArticle($lowerArticle, 'lower');

}


//	LOCK OR UNLOCK A ARTICLE COMMENTS...
if (isset($_GET['lock']) && ($_GET['lock'] == 1 || $_GET['lock'] == 0))	{

		if (isset($_GET['lockStory']) && is_numeric($_GET['lockStory']))	{

				lockStoryForComments($_GET['lockStory'], $_GET['lock']);

		}

}

//	HIDE OR UNHIDE A ARTICLE COMMENTS...
if (isset($_GET['hide']) && ($_GET['hide'] == 1 || $_GET['hide'] == 0))	{

		if (isset($_GET['hideStory']) && is_numeric($_GET['hideStory']))	{

				hideStoryForComments($_GET['hideStory'], $_GET['hide']);

		}

}





if (isset($_GET['instructions']) && $_GET['instructions'] == '1')		$_SESSION['instructions'] = 1;
if (isset($_GET['instructions']) && $_GET['instructions'] == '0')		$_SESSION['instructions'] = 0;
if (!isset($_SESSION['instructions']))									$_SESSION['instructions'] = 0;




echo "<p><a id='toggleInstructions1' class='javaLink'>Toggle Instructions</a></p>";

		echo "<div id='instructions' class='instructions'>";
			echo "<p><h3>Instructions</h3></p>";
			echo "<p>This is the main page for the Story Editor</p>";
			echo "<p>This page lists each issue in order, and allows you to edit them and add more.</p>";
			echo "<p>You can use the <img src='../pap/open.gif' /> and <img src='../pap/close.gif' /> buttons to open and close a particular story and thus see more information about it.";
			echo "<p>&nbsp;</p>";
			echo "<p>Within a particular story you can add or edit stories, preview the issue, homepage, RSS, etc, and finally create the full issue on the website.</p>";
			echo "<p>&nbsp;</p>";
			echo "<p>Click Here for full step by step instructions on how to add a story and generate the file onsite.";
			echo "<p>&nbsp;</p>";
			echo "<p>At the bottom of the page are various links, which will display the settings, list of keywords, and the list of story comments.";
			echo "<p>&nbsp;</p>";
			echo "<p><b>Note:</b> No changes will be made the publicly visible SchNEWS site until the new issue is created with the 'Create New Edition' button. Until then no changes are visible to anyone.</p>";
			echo "<p>&nbsp;</p>";
			echo "<p><b>Tip:</b> Hover your mouse pointer over any link or control and you will get a description of what it does.</p>";
		echo "</div>";


echo "<p>............</p>";

echo "<div><a style='font-size: 16px; font-weight: bold' href='index.php?editIssue=0' onMouseOver=\"Tip('$tip_addNewEdition')\" >Add New Edition</a></div>";

echo "<p>............</p>";



// NEW MYSQL CONNECTION
$conn	=	mysqlConnect();

// GRAB THE HIGHEST ISSUE NUMBER FRM THE SYSTEM
$rs			=	mysqlQuery(' SELECT issue FROM issues ORDER BY issue DESC LIMIT 0,1 ', $conn);
$res		=	mysqlGetRow($rs);
$maxIssue   = 	$res['issue'];


// SELECT THE LAST X ISSUES
$number_of_issues = NUM_STORY_ADMIN_ISSUES;
$rs		=	mysqlQuery('SELECT * FROM issues ORDER BY issue DESC LIMIT 0, '.$number_of_issues, $conn);


// CYCLE THROUGH ALL THE ISSUES
while ($res		=	mysqlGetRow($rs))	{

	// GRAB THE DATA FOR THIS ISSUE FROM THE SQL ARRAY
	$id				=	$res['id'];
	$issue			=	trim($res['issue']);
	$headline		=	stripslashes(trim($res['headline']));
	$date			=	$res['date'];
	$isSpecial 		=	$res['is_special'];
	$numHits		=	$res['hits'];
	if (!is_numeric($numHits) || $numHits == '' || !$numHits) $numHits = 0;
	if ($numHits	==	0)	$numHits = '';
	else 	$numHits	=	"[ $numHits Hits ]";


	if ($isSpecial)		$special = ' <span style="color:#666666; font-size:8px">SPECIAL ISSUE :</span> ';
	else	$special = '';

	// PREPARE THE + - BUTTON DEPENDING ON I WE ARE DRILLING INTO AN ISSUIE
	if ($showIssue == $id)	{
			$button		=	"close.gif";
			$link		=	"index.php";
	}	else 	{
			$button		=	"open.gif";
			$link		=	"index.php?showIssue=$id";
	}

	// SHOW THE +- BUTTON AND TITLE
	if ($showIssue == $id) 	$text	=	"<div style='border:2px solid #bbbbbb; padding: 15px'>";
	else $text = '';
	$text  	.=	"<h3>";
	$text	.=	"<a href=\"$link\"><img src=\"../pap/$button\"></a>";
	$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;<b>$issue - $special $headline</b>&nbsp;&nbsp;&nbsp;&nbsp;<i>".makeHumanDate($date, 'words')."</i>&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:10px; color: #666666'>$numHits</span>";

	if ($showIssue == $id)	{

		$text	.=	"<div style='margin-left:15%; padding-top: 7px; margin-top:5px; border-top: 1px solid #999999; padding-bottom: 7px; margin-bottom:5px; border-bottom: 1px solid #999999;'>";

		$text	.=	"<div style='padding: 5px'>";

		// SHOW THE EDIT LINK
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"index.php?editIssue=$id\" onMouseOver=\"Tip('$tip_editIssue')\">Edit Issue</a></span>";

		// SHOW THE PREVIEW LINK
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"edition.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_previewIssue')\">Preview Issue</a></span>";

		// SHOW THE TXT EMAIL LINK
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"txtEmail.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_makeTXTEmail')\">Make TXT Email</a></span>";

		// SHOW THE RSS FEED LINK
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"rssFeed.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_previewRSSFeed')\">Make RSS Feed</a></span>";

		// SHOW THE HOMEPAGE LINK
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"makeIndexMain.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_previewHomePage')\">Preview Home Page</a></span>";

		// SHOW THE ARCHIVE INDEX LINK
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"makeArchiveIndex.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_previewArchiveIndex')\">Preview Archive Index</a></span>";

		$text	.=	"</div><div style='padding: 5px'>";

		// SHOW THE REMAKE ISSUE LINK
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"edition.php?issue=$id&make=1\" target=\"_blank\" onClick=\"return confirmMakeIssue()\" onMouseOver=\"Tip('$tip_remakeIssue')\">[ Make Issue only on Server ]</a></span>";


		//	LINK FOR THE INDYMEDIA ISSUE LINKS...
		$text	.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
		$text	.=	"<span style=\"font-size: 8px\"><a href=\"makeIndyLinks.php?issue=$id\" target=\"_blank\" onMouseOver=\"Tip('$tip_makeIndyLinks')\">[ Make Links for IndyMedia Submission ]</a></span>";



		$text	.=	"</div>";

		$text	.=	"</div>";

	}
	$text	.=	"</h3>";

	// IF WE ARE DRILLING INTO AN ISSUE THEN SHOW ITS STORIES...
	if ($showIssue == $id)	{

		$summary = nl2br($res['summary']);
		$text	.=	 "<blockquote><blockquote>";
		$text	.=	processSummaryText($summary);
		$text	.=	"</blockquote></blockquote>";



		//	SHOW ARTICLE TITLES HERE
		$sql_i		=	"SELECT * FROM articles WHERE issue = $id ORDER BY story_number ASC";
		$rs_i		=	mysqlQuery($sql_i, $conn);
		while ($res_i 	=	mysqlGetRow($rs_i))	{

			//	PREPARE DATA FOR THIS ARTICLE...
			$headline		=	stripslashes($res_i['headline']);
			$storyNum		=	$res_i['story_number'];
			$storyId		=	$res_i['id'];
			$hits			=	$res_i['hits'];
			if ($res_i['locked'] == 1) 	$locked = 1;
			else $locked = 0;
			if ($res_i['hide_comments'] == 1) $hide = 1;
			else	$hide = 0;
			if ($hits == '0') $hits = '';
			$numComments		=	getNumberOfCommentsForArticle($storyId);
			if ($numComments == '0') $numComments = '';
			$numSpams			=	getNumberOfSpamCommentsForArticle($storyId);
			if ($numSpams == '0')	$numSpams 	=	'';


			//	PREPARE SOME DATA DEPENDING ON WHETHER WE ARE DRILLING INTO THIS ARTICLE OR NOT...
			if ($showArticle == $storyId)	{
				$button		=	"close.gif";
				$link		=	"index.php?showIssue=$id";
				$link2		=	"index.php?showIssue=$id&showArticle=$storyId";

			}	else 	{
				$button		=	"open.gif";
				$link		=	"index.php?showIssue=$id&showArticle=$storyId";
				$link2		=	"index.php?showIssue=$id";
			}

			$text	.=		"<blockquote style='border-bottom: 1px solid #d8d8d8'><blockquote>";
			$text	.=		"<a href=\"$link\"><img src=\"../pap/$button\"></a>";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			if ($showArticle == $storyId)	$text	.=		"$storyNum :: <b>$headline</b>";
				else 	$text	.=		"$storyNum :: $headline";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			$text	.=		"<span style=\"font-size: 8px\"><a href=\"index.php?issue=$id&editArticle=$storyId\" onMouseOver=\"Tip('$tip_editStory')\">Edit Story</a></span>";

			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";

			$text	.=		"<span style=\"font-size: 8px\"><a href=\"article.php?article=$storyId\" target=\"_blank\" onMouseOver=\"Tip('$tip_previewStory')\">Preview Story</a></span>";

			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			if (is_numeric($hits))	{

					$text	.=		" [ $hits Hits ] ";

					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			}

			if (is_numeric($numComments))	{

					if ($locked)	$lockedText		=	" <b>- Comments Locked</b> ";
					else 	$lockedText 	=	'';

					$text	.=		" [ $numComments Comments $lockedText ] ";

					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			}

			if (is_numeric($numSpams))	{

					$text	.=		" [ $numSpams Spam Comments ] ";

					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
					$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			}

			//	DRAW UP AND DOWN ARROWS
			$text	.=		"<a href=\"$link2&raiseArticle=$storyId\" onMouseOver=\"Tip('$tip_arrow')\"><img src=\"../images/uparrow.gif\" /></a>";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";
			$text	.=		"<a href=\"$link2&lowerArticle=$storyId\" onMouseOver=\"Tip('$tip_arrow')\"><img src=\"../images/downarrow.gif\" /></a>";
			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;";

			//	DRAW LOCK/UNLOCK COMMENTS
			if ($locked)	{

					$text	.=		"<a href='?showIssue=$id&lockStory=$storyId&lock=0'><span style='font-size:14px; font-weight:bold'>[ Un-Lock Comments ]</span></a>";

			}	else 	{

					$text	.=		"<a href='?showIssue=$id&lockStory=$storyId&lock=1'>[ Lock Comments ]</a>";

			}

			$text	.=		"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";

			if ($hide)	{

					$text	.=		"<a href='?showIssue=$id&hideStory=$storyId&hide=0'><span style='font-size:14px; font-weight:bold'>[ Un-Hide Comments ]</span></a>";

			}	else 	{

					$text	.=		"<a href='?showIssue=$id&hideStory=$storyId&hide=1'>[ Hide Comments ]</a>";

			}

			//	SHOW FULL STORY PREVIEW...
			if ($showArticle == $storyId)	{

				// SHOW THE MAKE INDYMEDIA HTML LINK
				$text	.=	"<blockquote><blockquote><div style=\"padding:10px; border-left: 1px solid #bbbbbb; border-right: 1px solid #bbbbbb\" >";

				$text	.=	"<div style='text-align: right'>";
				$text	.=	"<span style=\"font-size: 8px\"><a href=\"indymedia_scripts.php?make_html=$storyId\" target=\"_blank\" onMouseOver=\"Tip('$tip_indymedia_html')\">[ Make HTML for IndyMedia Submission ]</a></span>";
				$text	.=	"</div>";

				$text	.=	makeArticle($storyId, $conn);

				//$text	.=	"<h3>".str_replace('SCHNEWS', 'SchNEWS', strtoupper($res_i['headline'])).'</h3>';
				//$text	.=	'<p align="justify">'.nl2br(stripslashes(scanForLinks(scanForCustomTags($res_i['story'])))).'</p>';
				$text	.=	"</div></blockquote></blockquote>";

			}

			$text	.=		"</blockquote></blockquote>";

		}

		//	ADD NEW STORY LINK
		$text	.=	 "<blockquote><blockquote>";
		$text	.=	"<div style=\"width: 100px; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
		$text	.=	"<span style=\"font-size: 14px\"><a style=\"font-size: 14px; font-weight: bold\" href=\"index.php?editArticle=0&issue=$id\" onMouseOver=\"Tip('$tip_addNewArticle')\">Add New Article</a></span>";
		$text	.=	"</blockquote></blockquote>";

		if ($issue == $maxIssue)	{
			$text	.=	 "<blockquote><blockquote>";
			$text	.=	"<div style=\"width: 100px; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
			$text	.=	"<span style=\"font-size: 14px\"><a style=\"font-size: 14px; font-weight: bold\" href=\"index.php?createFullEdition=1&issue=$id\" onMouseOver=\"Tip('$tip_createFullEdition')\">Create Files on Server</a></span>&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;&nbsp;<span style='color:#666666'>This will commit your issue to the schnews.org.uk server, generate the RSS feed and send the plain text issue to the mailing list.</span>";
			$text	.=	"</blockquote></blockquote>";
		}	else	{
			$text	.=	 "<blockquote><blockquote>";
			$text	.=	"<div style=\"width: 100px; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
			$text	.=	"<span style=\"font-size: 8px; color:#888888\">You can onlu Create the Full Edition for the most recent edition</span>";
			$text	.=	"</blockquote></blockquote>";
		}

		if ($showIssue == $id) $text	.=	"</div>";

		$text	.=	"<div style=\"width: 80%; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";


	}



	echo $text;

}

	$text	=	'<br /><br />';
	$text	.=	"<div style=\"width: 80%; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
	$text	.=	"<a href=\"?keywordsList=1\" target=\"_blank\" onMouseOver=\"Tip('$tip_keywords')\">View List of Keywords</a>";
	$text	.=	"<div style=\"width: 80%; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
	$text	.=	"<a href=\"?comments=1\" target=\"_blank\" onMouseOver=\"Tip('$tip_comments')\">Comments</a>";
	$text	.=	"<div style=\"width: 80%; border-bottom: 1px dotted #888888\">&nbsp;</div><br />";
	$text	.=	"<a href=\"?settings=1\" target=\"_blank\" onMouseOver=\"Tip('$tip_settings')\">Settings</a>";

	echo $text;

?>
