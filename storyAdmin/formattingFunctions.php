<?php





/*	==================================================================================

{outpu00004}				OUTPUT AND FORMATTING FUNCTIONS

	==================================================================================	*/

//	TAKE A GIVEN ISSUE NUMBER AND OUTPUT AN HTML FORMATTED SET OF BACK ISSUE SUMMARIES.
function makeBackIssues($issueId, $conn)		{

		if (!is_numeric($issueId))	return false;

		$sql	=	" SELECT issue FROM issues WHERE id = $issueId ";
		$rs		=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs,$conn) != 1)	return false;
		$res	=	mysqlGetRow($rs);
		$issueNum	=	$res['issue'];


		$backIssues		=	"\r\n";
		for ($n	=	($issueNum - 1) ; $n > ($issueNum - NUMBACKISSUES) ; $n--)	{

			$sql_bi		=	" SELECT issue, headline, summary, date FROM issues WHERE issue = $n ";
			$rs_bi		=	mysqlQuery($sql_bi, $conn);
			if (mysqlNumRows($rs_bi, $conn)	!= 1)	continue;
			$backIssue	=	mysqlGetRow($rs_bi);

			$bi_link		=	"SchNEWS $n, ".makeHumanDate($backIssue['date'], 'words');
			$bi_link		=	"<b><a href=\"../archive/news$n.htm\">$bi_link</a></b>";

			$bi_title		=	"<b>".$backIssue['headline']."</b>";

			$bi_summary		=	strip_tags($backIssue['summary']);

			$backIssues		.=	"\r\n<div id=\"backIssue\">\r\n<p>$bi_link<br />\r\n$bi_title - $bi_summary</p>\r\n</div>\r\n";

		}

		return $backIssues;

}



/**
 * Creates and returns formatted HTML code for a given article.
 *
 * @param INT $id			Id of article from DB
 * @param RESOURCE $conn	MySQL Connection resources
 * @param INT $mode			Where are we outputting to? 0 = website, 1 = plain text
 *
 * @return STRING			HTML code for article.
 */
function makeArticle($id, $conn, $mode	=	0)	{

		if (!is_numeric($id))	return false;			// ID MUST BE A NUMBER OR WE NEED TO QUIT OUT.
		if (!is_numeric($mode))	$mode = 0;				// MODE MUST BE A NUMBER ELSE WE NEED IT TO DEFAULT TO 0

		//	GRAB HEADLINE AND STORY FOR THE ARTICLE
		$sql	=	" SELECT pretitle, headline, story, issue FROM articles WHERE id = $id ";
		$rs		=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs,$conn) == 0)	return false;
		$res	=	mysqlGetRow($rs);

		switch ($mode)	{		// PUT TOGETHER THE ARTICLE DEPENDING ON WHICH MODE WE'RE IN.

			default:
				if (trim($res['pretitle']) != '')	{

						$ib_pretitle	=	"\r\n<br /><br /><br /><div style='font-weight:bold; font-size:13px; clear:all; float:none'>".processHeadline($res['pretitle'])."</div>\r\n";

				}	else	$ib_pretitle		=	'';
				$ib_title		=	"\r\n"."<h3>".processHeadline(stripslashes(strtoupper($res['headline'])))."</h3>\r\n";
				$ib_story		=	"\r\n<p>\r\n".stripslashes(scanForCustomTags(scanForLinks($res['story'])))."\r\n</p>\r\n";
				$articleBody	=	"\r\n<div id=\"article\">\r\n".$ib_pretitle.$ib_title.$ib_story."\r\n</div>\r\n";

				if (DOSTORYKEYWORDS)	{

					$keywordList		=	getListOfRankingKeywords();
					foreach ($keywordList as $keyword)	{

							$articleBody		=	str_replace(" $keyword ", ' <a href="../keywordSearch/?keyword='.urlencode($keyword).'&source=$id">'.$keyword.'</a> ', $articleBody);

					}

				}

				//$rs				=	mysqlQuery(" SELECT keyword FROM keywords WHERE article_id = $id ", $conn);
				//$keywords		=	array();
				//while ($res_kw	=	mysqlGetRow($rs))	{

				//	$keywords[]		=	$res_kw['keyword'];
				//}
				//if ($keywords		=	createKeywordList($keywords, $res['issue']))	{

				$keywords 	=	" <?php echo createKeywordList(get_keywords_for_article($id), ".$res['issue']."); ?>";
				if (SHOWKEYWORDS)		$articleBody	.=		$keywords;



				break;

			case '1':
				$ib_title		=	"\r\n\r\n".strtoupper(stripslashes($res['headline']))."\r\n";
				$ib_story		=	"\r\n".stripslashes($res['story'])."\r\n";
				$articleBody	=	$ib_title.$ib_story;
		}

		return $articleBody;
}


function getListOfRankingKeywords()	{

			$conn 	=	mysqlConnect();
			$sql	=	" SELECT COUNT(id), keyword FROM keywords GROUP BY keyword ";
			$rs		=	mysqlQuery($sql, $conn);

			$list	=	array();
			while ($res		=	mysqlGetRow($rs))	{

					if ($res['COUNT(id)'] > KEYWORDSHITSFORSTORYLINK)	{

							$list[]		=	$res['keyword'];

					}

			}

			return $list;
}






function cleanOutputTxt($text)	{

	//return $text;

	$search		=	array(chr(145), chr(146), '&#8220;', '&#8221;');
	//$text		=	str_replace($search, '"', $text);

	$search		=	array(chr(147), chr(148), '&#8216;', '&#8217;');
	$text		=	str_replace($search, "'", $text);

	$search		=	array(chr(150), chr(151), '&#8211;');
	$text		=	str_replace($search, '-', $text);

	$search		=	array('&#8230;');
	$text		=	str_replace($search, '...', $text);

	//$text		=	str_replace(chr(146), '"', $text);
	//$text		=	str_replace(chr(147), "'", $text);
	//$text		=	str_replace(chr(148), "'", $text);

	//$text		=	str_replace(chr(150), '-', $text);
	//$text		=	str_replace(chr(151), '-', $text);

	return $text;

}



/**
 * Replaces quote marks in the text so that is can be used in the form input...
 *
 * @param string $text			//	The text to be cleaned
 * @return string				//	The cleaned text
 */
function cleanTextForForms($text)
{
	//	Test inputs
	if (!is_string($text) || trim($text) == '')	return $text;

	//	Search and replace arrays...
	$search 	=	array(
							"&",
							"'",
							"\""
						);
	$replace	=	array(
							"&amp;",
							"&apos;",
							"&quote;"
						);

	//	Do the operation
	return str_replace($search, $replace, $text);

}


function createKeywordList($keywords, $issueId = 0)	{

	if (!is_array($keywords))	return  false;
	if (count($keywords) == 0)	return 	false;

	//if (!$keywords = filterKeywordList($keywords))	return false;

	$issueGet		=	'';
	if (is_numeric($issueId) && $issueId != 0)	{

			$conn		=	mysqlConnect();
			$sql		=	" SELECT issue FROM issues WHERE id = $issueId ";
			$rs			=	mysqlQuery($sql, $conn);
			if (mysqlNumRows($rs, $conn) == 1)	{

					$res		=	mysqlGetRow($rs);
					$issueGet	=	'&source='.$res['issue'];

			}

	}

	//echo "<p>Before Sort :: Count of Keywords = ".count($keywords)."</p>";
	sort($keywords);
	//echo "<p>After Sort :: Count of Keywords = ".count($keywords)."</p>";

	foreach ($keywords as $key => $keyword)	{

		if (getNumKeywordOccurances($keyword) < NUMKEYWORDS) {

				$keywords[$key]		=		"<span style='color:#777777; font-size:10px'>".stripslashes($keyword)."</span>";

		}	else	{

				$keywords[$key]		=		"<a href=\"../keywordSearch/?keyword=".urlencode($keyword)."$issueGet\">".stripslashes($keyword)."</a>";

		}
	}

	$output		=	"\r\n<div id=\"keywords\" style=\"margin-left:35%; margin-top:28px; border-top:1px solid #ddddbb; padding-top:8px\" align=\"right\"><b>Keywords:</b> ".implode(', ',$keywords)."</div>\r\n";

	return $output;


}







/*	===================================================================================================
{messb0005}					MESSAGE BOARD FUNCTIONS */


function showAddMessage($articleId, $isFeature = false)	{

	if (!is_numeric($articleId))	return false;
	if (!is_bool($isFeature))		$isFeature	=	false;

	$conn		=	mysqlConnect();
	if ($isFeature)		{

		$sql		=	" SELECT locked FROM features WHERE id = $articleId ";

	}	else {

		$sql		=	" SELECT issue, story_number, locked FROM articles WHERE id = $articleId ";
	}
	$rs			=	mysqlQuery($sql, $conn);

	if (mysqlNumRows($rs, $conn) != 1)	return false;

	$res		=	mysqlGetRow($rs);

	if ($isFeature)	{

		$locked			=	$res['locked'];

	}	else 	{

		$issueId		=	$res['issue'];
		$storyNumber	=	$res['story_number'];
		$locked 		=	$res['locked'];

	}
	
	$locked = 1;
	

	if ($locked ==1) {

			$text		=	"<div style='text-align: center; font-size: 14px; color:#888888; font-weight:bold; border-top:2px solid #888888; border-bottom: 2px solid #888888; padding: 10px; margin: 10px; margin-top: 25px'>
							THE POSTING OF COMMENTS ON THIS STORY HAS BEEN PROHIBITED BY A MODERATOR</div>";
			return $text;

	}

	if ($isFeature)	{

		$articleLink	=	"../features/show_feature.php?feature=$articleId#comments";

	}	else {

		$sql		=	" SELECT issue FROM issues WHERE id = $issueId ";
		$rs			=	mysqlQuery($sql, $conn);

		if (mysqlNumRows($rs, $conn) != 1)	return false;
		$res		=	mysqlGetRow($rs);
		mysql_close($conn);

		$issue		=	$res['issue'];

		$articleLink	=	'../archive/news'.$issue.$storyNumber.'.php#comments';

	}

	$text		 =	"<div style=\"width: 80%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
	$text		.=	"<form method=\"POST\" action=\"$articleLink\" >";
	$text		.=	"<b><i>Add a new comment on this story...</i></b><br /><br />";
	$text		.=	"<div align=\"right\" style=\"width:70%\">Who are you :: <input type=\"input\" name=\"author\" /></div>";
	$text		.=	"<div class='website'>Website : <input type='text' class='website' name='website' /></div>";
	$text		.=	"<textarea name=\"newComment\" style=\"width:70%; height:100px; margin-top:5px\"></textarea>";
	$text 		.=	"&nbsp;&nbsp;&nbsp;";
	$text		.=	"<input type=\"submit\" name=\"addCommentSubmit\" value=\"Add Comment\" />";
	$text		.= "</form>";
	$text		.=	"<div style=\"width: 100%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
	$text		.=	"<div style='margin-left:30px; font-size:9px; padding:3px; border-bottom:1px solid #cccccc; width: 70%'>SchNEWS reserves the right to remove any comments made by nutters or idiots...</div>";
	$text		.=	"<div style='margin-left:30px; font-size:9px; padding:3px'>No IP addresses or other identifiable information is logged with your post</div>";
	$text		.=	"<div style=\"width: 80%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";

	return $text;

}



function addMessage($articleId, $isFeature = false)	{

	return true;

	$_SESSION['spam'] = 0;
	if (!isset($_POST['addCommentSubmit']) || $_POST['addCommentSubmit'] != 'Add Comment')	return false;
	if (!is_bool($isFeature))	$isFeature	=	false;

	$comment		=	cleaninput($_POST['newComment']);
	$author			=	cleaninput($_POST['author']);
	if ($author == '')	$author		=	'Anonymous';

	if ($comment == '') return false;

	if (isSpamEntry($comment, $author))	{

			$spam = 1;
			$_SESSION['spam'] = 1;

	}		else $spam = 0;

	if (isset($_POST['website']) && trim($_POST['website']) != '')	{

			$spam = 1;
			$_SESSION['spam'] = 1;

	}


	if (!is_numeric($articleId))	return false;
	$conn		=	mysqlConnect();

	$datetime	=	randomiseDatetime(makeSqlDatetime());
	$ipAddress	=	md5($_SERVER['REMOTE_ADDR']);

	if ($isFeature)		{

			$type		=	'featureid';

	}	else {

			$type		=	'articleid';

	}

	$sql		=	" SELECT comment, $type FROM story_comments WHERE comment = '$comment' AND $type = $articleId ";
	$rs			=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) != 0)	return false;

	if ($isFeature)	{

		$featId		=	$articleId;
		$artId		=	0;

	}	else {

		$featId		=	0;
		$artId		=	$articleId;

	}

	$sql		=	" INSERT INTO story_comments
						(articleid, featureid, comment, author, datetime, ip_address, spam, deleted, authorised)
					VALUES
						($artId, $featId, '$comment', '$author', '$datetime', '$ipAddress', '$spam', 0, 0) ";

	$rs			=	mysqlQuery($sql, $conn);

	echo mysql_error();


	if ($spam == 1)	{

			$subject = "SchNEWS :: A potential spam message has been submitted to article id $articleId";
			$body =
"The following message has been submitted to article $articleId, which the system considers may be spam.

It was submitted on ".makeSqlDatetime()." from IP address '$ipAddress'.


===================================================

Author : $author

$comment

";
			$body	=	stripslashes(str_replace('\r\n', chr(10).chr(13), $body));
			send_smtp(SPAMNOTICERECIPIENTS, $subject, $body);


	}


	if ($id		=	mysqlGetInsertId($conn))	return true;
	return false;

}


function randomiseDatetime($datetime, $maxminutes = 15)	{

	//echo "<p>Input date = '$datetime'";
	if (strlen($datetime) != 19) return $datetime;
	if (!is_numeric($maxminutes)) $maxminutes = 15;

	$unixTime		=	strtotime($datetime);

	$offset			=	rand(10, $maxminutes * 60);

	$direction 		= 	rand(0,1);

	//echo "<p>Offest = $offset";
	if ($direction)	{

		$unixTime		+=		$offset;

	}	else {

		$unixTime		-=		$offset;

	}

	$newDatetime = date("Y-m-d H:i:s", $unixTime);

	//echo "<p>New datetime = '$newDatetime'";
	return $newDatetime;



}



function getMessages($articleId, $isFeature = false)	{

	if (!is_numeric($articleId))	return false;
	if (!is_bool($isFeature))	$isFeature	=	false;

	$conn		=	mysqlConnect();

	if ($isFeature)	{

		$type	=	'featureid';

	}	else 	{

		$type	=	'articleid';

	}


	if (is_comments_display_disabled($articleId))	{

		return "<div style='text-align: center; font-weight: bold; font-size: 18px; COLOR: #999999'>COMMENTS HAVE BEEN DISABLED</div>";

	}

	$sql		=	" SELECT comment, author, datetime FROM story_comments WHERE $type = $articleId AND spam = 0 ORDER BY datetime desc ";
	$rs			=	mysqlQuery($sql, $conn);

	if ($_SESSION['spam'] == 1)	{

		$spamWarning = "<p><font color=\"red\">Your post has been flagged a spam, and so wont be shown. Our moderators will have a look at it, and may unflag it at their discretion..</font></p>";

	}	else 		$spamWarning = '';

	if (mysqlNumRows($rs, $conn)	== 0)	{

				$text	=	$spamWarning;

	}	else {

			$text	=	"<div style=\"width: 80%; border-bottom: 1px solid #888888\">&nbsp;</div><br />";
			$text	.=	$spamWarning;
			$text	.=	"<p><b><i>The following comments have been left on this story by other SchNEWS readers...</i></b></p>";

			while ( $res = mysqlGetRow($rs) )	{

				$comment	=	nl2br(htmlentities(strip_tags((stripslashes($res['comment'])))));
				$author		=	htmlentities(stripslashes($res['author']));
				$date		=	makeHumanDate(substr($res['datetime'], 0, 10), 'words');
				$time		=	makeTimeFromSql($res['datetime']);

				$text		.=	"<p>";
				$text		.=	"<font color=\"#888888\">Added on $date at $time by <b>$author</b></font><br />";
				$text		.=	'<blockquote style="border-left:1px solid #999999; padding-left:6px">'.scanForLinks($comment).'</blockquote>';
				$text		.=	'</p><br />';

			}


	}

	return $text;



}


function is_comments_display_disabled($id)	{

	//	Test inputs
	if (!is_numeric($id) || $id == 0)	return false;

	$conn		=	mysqlConnect();
	$sql		=	" 	SELECT hide_comments
						FROM articles
						WHERE id = $id";
	$rs			=	mysqlQuery($sql, $conn);

	if (mysqlNumRows($rs, $conn) != 1) return false;

	$res		=	mysqlGetRow($rs);
	if ($res['hide_comments'] == 1) return true;
	return false;

}


function scanForLinks($input)	{

		$input				=	str_replace('<br />', ' <br /> ', $input);
		$input				=	str_replace('<br>', ' <br> ', $input);
		$input 				=	str_replace("\r", " \r ", $input);
		$input 				=	str_replace("\n", " ", $input);

		$input 				=	str_replace("<p>", " <p> ", $input);
		$input 				=	str_replace("</p>", " </p> ", $input);

		$input				=	str_replace('(', ' ( ', $input);
		$input				= 	str_replace(')', ' ) ', $input);
		$words				=	explode(" ", $input);
		$isInternalLink		=	0;
		$exceptions			=	array('<a', '</a>', 'href', 'target', '="www', '="http"');
		foreach		($words as $index => $word)		{

					//$word			=	' '.$word;
					if (firstPos($word, $exceptions) != false) continue;

					if (strpos(' '.$word, "@"))	{

								if (strpos(strrchr($word, "@"), ".")) $words[$index]	=	"<a href=\"mailto:$word\">".$word."</a>";
							}

					if ( (strpos(strtolower(" ".$word), "http") || strpos(strtolower(" ".$word), "www")) && substr(strtolower($word), 0, 4) != 'src=' ) 	{

								if (strpos(strtolower(" ".$word), "http")) 	$words[$index]	=	"<a href=\"$word\" target=\"_blank\">".$word."</a>";
								else						$words[$index]	=	"<a href=\"http://$word\" target=\"_blank\">".$word."</a>";

							}




			}

	$output		=	"";
	foreach ($words as $word)	{

				$output		.=	$word." ";
			}

	//$output		=	str_replace(' <br />', '', $output);
	//$output		=	str_replace('<br>', '', $output);
	$output		=	str_replace(' ( ', '(', $output);
	$output		=	str_replace(' ) ', ')', $output);

	$output		=	scanForSchNEWSLinks($output);

	//	Add border styles to any images...
	$output		=	str_ireplace('" style="', '" style="border:2px solid black;', $output);

	//	Left / Right Formatting
	$output		=	str_ireplace('<img', '<img style="margin-right: 10px; border: 2px solid black" align="left" ', $output);

	$floats		=	array(';float: right', '; float: right', ';float:right', '; float:right');
	$output		=	str_ireplace($floats, '; margin-left: 10px" align="right', $output);

	$floats		=	array(';float: left', '; float: left', ';float:left', '; float:left');
	$output		=	str_ireplace($floats, '; margin-right: 10px" align="left', $output);

	return trim($output);



}



function scanForSchNEWSLinks($input)	{

		if (!is_string($input))	return $input;
		if (DEBUG)	{

			echo "<div style='color:red'>input length is ".strlen($input)."</div>";

		}

		$isSchNEWSLink		=	false;
		for ($n = 0; $n < strlen($input) ; $n++ )	{

				if (strtolower(substr($input, $n, 7)) == 'schnews' && $input{$n+7} == ' ' &&
						( is_numeric(substr($input, $n+8, 2)) || is_numeric(substr($input, $n+8, 3)) || is_numeric(substr($input, $n+8, 4)) )   )	{

									if (DEBUG)	{

											echo "<div style='color:red'>Enabling isSchNEWSLInk :: n = $n :: input{n} is ".$input{$n}." :: input length is ".strlen($input)." :: string is ".substr($input, $n, 12)."</div>";

									}
									$howManyNumbers		=	0;
									$number				=	'';

									for ($i = 8; $i < 12 ; $i++)	{

										if (is_numeric($input{$n+$i}))	{

												$howManyNumbers++;
												$number		.=	$input{$n+$i};

										}	else {

												break;

										}

									}

									$inputLeft		=	substr($input, 0, $n);
									$inputMiddle	=	"<a href='../archive/news$number.htm'>SchNEWS $number</a>";
									$inputRight		=	substr($input, $n+8+$howManyNumbers);

									$n				=	$n+strlen($inputMiddle);
									$input			=	$inputLeft.$inputMiddle.$inputRight;
									$isSchNEWSLink	=	true;
				}


				if ($isSchNEWSLink)	{

						if (( is_numeric(substr($input, $n, 2)) || is_numeric(substr($input, $n, 3)) || is_numeric(substr($input, $n, 4)) ) && $input{$n} != ' ')	{

									if (DEBUG)	{

											echo "<div style='color:red'>On second link :: input{n} is ".$input{$n}." :: input length is ".strlen($input)." :: string is ".substr($input, $n, 12)."</div>";

									}

									$howManyNumbers		=	0;
									$number				=	'';

									for ($i = 0; $i < 4 ; $i++)	{

										if (is_numeric($input{$n+$i}))	{

												$howManyNumbers++;
												$number		.=	$input{$n+$i};

										}	else {
												if (DEBUG)	echo "<div style='color:red'>Breaking</div>";
												break;

										}

									}
									if (DEBUG)	echo "<div style='color:red'>howManyNumber = $howManyNumbers :: number = $number</div>";

									$inputLeft		=	substr($input, 0, $n);
									$inputMiddle	=	"<a href='../archive/news$number.htm'>$number</a>";
									$inputRight		=	substr($input, $n+$howManyNumbers);

									$n				=	$n+strlen($inputMiddle);
									$input			=	$inputLeft.$inputMiddle.$inputRight;
									$isSchNEWSLink	=	true;

						}	elseif ($input{$n} != ',' && $input{$n} != ' ')	{

									if (DEBUG)	{

											echo "<div style='color:red'>Disabling isSchNEWSLInk :: n = $n :: input{n} is ".$input{$n}."</div>";

									}
									$isSchNEWSLink		=	false;

						}

				}

		}

		return $input;

}


function firstPos($string, $needles)	{

	if (!is_string($string)) return false;
	if (!is_string($needles) && !is_array($needles))	return false;

	if (!is_array($needles)) $needle = array($needles);

	foreach ($needles as $needle)	{

		$posT 	=	strpos(' '.$string, $needle);
		if (is_numeric($posT)) $pos[]	=	$posT;

	}

	if (!isset($pos)) return false;
	$min		=	min($pos);
	if (is_numeric($min)) return $min;
	else return false;

}

function lastPos($string, $needles)	{

	if (!is_string($string)) return false;
	if (!is_string($needles) && !is_array($needles))	return false;

	if (!is_array($needles)) $needle = array($needles);

		foreach ($needles as $needle)	{

		$posT 	=	strpos(' '.$string, $needle);
		if (is_numeric($posT)) $pos[]	=	$posT;

	}

	if (!isset($pos)) return false;
	$max		=	max($pos);
	if (is_numeric($min)) return $max;
	else return false;

}


function isValidEntry($comment, $author)	{

		if (empty($comment)) return false;
		if ($comment == $author) return false;

		return true;

}


function isSpamEntry($comment, $author)	{

		// CREATE KEYWORD LIST
		$keywordList		=	 	str_replace('/r/n', '<br />',(file_get_contents('../storyAdmin/profanityList.txt')));


		// TEST COMMENT AGAINST KEYWORD LIST...
		$comment			=		str_replace('\r\n', ' ', $comment);
		$commentWords		=		explode(' ', $comment);
		$spamCount			=		0;		//	COUNT HOW MANY HITS THERE ARE FOR THIS COMMENT AGAINST THE SPAM KEYWORD LIST
		$linkCount			=		0;		//	COUNT HOW MANY LINKS (BOTH MAILTO AND HREF) THERE ARE IN THE COMMENT
		foreach($commentWords as $word)	{

				//	IF THE WORD OCCURS IN THE KEYWORD LIST (AND IS 4 LETTERS OR MORE) THEN INCREMENT THE COUNTER
				if (strlen($word) > 3 && strpos(strtolower($keywordList), strtolower($word)) != false)	$spamCount++;

				//	IF THE WORD IS A LINK THEN INCREMENT THE COUNTER
				if (isLink($word))	$linkCount++;

		}

		if ($spamCount >= SPAMCOUNT) return true;	//	IF THE KEYWORD HIT EXCEEDS THE THRESHOLD THEN FLAG THIS COMMENT AS SPAM
		if ($linkCount >= LINKCOUNT) return true;	//	IF THE NUMBER OF LNKS EXCEEDS THE THRESHOLD THEN TREAT THE COMMENT AS SPAM


		//	TEST IF THE IP ADDRESS HAS JUST POSTED ANYTHING IN THE PAST 30 SECONDS
		$ipAddress		=	$_SERVER['REMOTE_ADDR'];
		$numPosts		=	getNumPostsInTimeframe($ipAddress);
		if ($numPosts >= SPAMHAMMER) return true;

		return false;

}


function getNumPostsInTimeframe($ipAddress)	{

		// TEST INPUTS AND FAIL IF BAD
		if (!is_numeric(str_replace('.', '', $ipAddress)))	return false;


		// GET ALL DATES FOR THIS IP ADDRESS FROM DB IN REVERSE CHRONOLOGICAL ORDER
		$conn		=	mysqlConnect();
		$sql		=	" SELECT datetime FROM story_comments WHERE ip_address = '$ipAddress' ORDER BY datetime DESC ";
		$rs			=	mysqlQuery($sql, $conn);

		$now		=	strtotime(makeSqlDatetime());
		$threshold	=	$now - SPAMHAMMERDURATION;

		$numPosts	=	0;
		while($res	=	mysqlGetRow($rs))	{

				if (strtotime($res['datetime']) > $threshold) $numPosts++;

		}

		return $numPosts;

}


function isLink($word)	{

	//	IF THE WORD IS EMPTY THEN IT ISN'T A LINK...
	if (empty($word))	return false;

	// 	IF THE WORD HAS NO FULL STOPS THEN IT ISN'T A LINK...
	if (strpos($word, '.') == false)	return false;

	// TEST IF IT IS AN EMAIL ADDRESS...
	if ($at = strpos($word, '@')) {

			if (strrpos($word, '.') > $at) return true;

	}

	$word 		=	strtolower($word);

	//	TEST IF IT IS A HYPERLINK...
	if ($prot = strpos(' '.$word, 'http'))	{

			if (strrpos($word, '.') > $prot)	return true;

	}
	if ($prot = strpos(' '.$word, 'www'))	{

			if (strrpos($word, '.') > $prot)	return true;

	}

	return false;

}



function processSummaryText($summary)	{

	if (!is_string($summary))	return false;
	$summary = strip_tags($summary, '<b><i><strong>');

	return $summary;

}




function processHeadline($headline)	{

	if (!is_string($headline))	return $headline;

	if (substr($headline, 0, 3) == '---' && substr($headline, strlen($headline) -3) == '---')	{

			$headline = str_replace('---', '', $headline);
			$headline = "<div align='center' style='text-align:center; border-top: 3px double black; border-bottom: 3px double black;
										padding-top:5px;padding-bottom:5px'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										$headline
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>";

	}

	return $headline;

}





function make_features_tab($mode = 'issues')	{

	if (isset($_GET['edit_users']))		return false;
	if (isset($_GET['comments']))		return false;
	if (isset($_GET['keywordsList']))	return false;
	if (isset($_GET['settings']))		return false;


	//	Test inputs...
	$possible_modes		=	array('issues', 'features', 'schmovies', 'diy');
	if (!in_array($mode, $possible_modes))	{

		$mode 	=	'issues';

	}



	$class_issues		=	'light_tab';
	$class_features		=	'light_tab';
	$class_schmovies	=	'light_tab';
	$class_diy			=	'light_tab';

	switch ($mode)	{

		case 'issues':
			$class_issues		=	'heavy_tab';
			break;

		case 'features':
			$class_features		=	'heavy_tab';
			break;

		case 'schmovies':
			$class_schmovies	=	'heavy_tab';
			break;

		case 'diy':
			$class_diy			=	'heavy_tab';
			break;

	}


	$output		=	array();

	$output[]	=	"<div style='width:800px; border: 1px solid white; float: none'>";

	$output[]	=	"<table align='left' width='90%'>";
	$output[]	=	'<tr>';

	$output[]	=	"<td width='10%'>";
	$output[]	=	"&nbsp;";
	$output[]	=	"</td>";

	$output[]	=	"<td width='20%' class='$class_issues'>";
	$output[]	=	"<a href='index.php'>Issues</a>";
	$output[]	=	"</td>";

	$output[]	=	"<td width='20%' class='$class_features'>";
	$output[]	=	"<a href='index.php?features=1'>Features</a>";
	$output[]	=	"</td>";

	$output[]	=	"<td width='20%' class='$class_schmovies'>";
	$output[]	=	"<a href='index.php?schmovies=1'>SchMOVIES</a>";
	$output[]	=	"</td>";

	$output[]	=	"<td width='20%' class='$class_diy'>";
	$output[]	=	"<a href='index.php?diy=1'>DIY Guide</a>";
	$output[]	=	"</td>";


	$output[]	=	"<td width='10%'>";
	$output[]	=	"&nbsp;";
	$output[]	=	"</td>";

	$output[]	=	"</tr>";

	$output[]	=	"<tr>";
	$output[]	=	"<td colspan='6' class='features_bottom_tab'>";
	$output[]	=	"&nbsp;";
	$output[]	=	"</td>";
	$output[]	=	"</tr>";

	$output[]	=	"</table>";

	$output[]	=	"</div>";

	$output[]	=	"<br /><br /><br /><br />";



	return implode("\r\n", $output);


}




function get_schmovies_index_preview(){

	$file	=	file_get_contents("inc/schmovies-index-text.htm");
	//$file	=	strip_tags($file, "<table><tr><td><p><b><img><a><hr><br>");
	$file	=	str_replace('src="images', 'src="schmovies/images', $file);
	return $file;

}


?>
