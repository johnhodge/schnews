<?php

include 'storyAdmin/functions.php';
require "functions/functions.php";


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>

<title>SchNEWS - direct action protest demonstrations - free weekly newsletter - crap arrest, climate change, party, DIY, Brighton, animal rights, asylum, permaculture, privatisation, neoliberal, media, copyleft, globalisation</title>
<meta name="keywords" content="SchNEWS, direct action, Brighton, information for action, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, climate change, IMF, WTO, World Bank, WEF, GATS, no borders, Simon Jones, protest, privatisation, neo-liberal, Rising Tide, Kyoto protocol, climate change, global warming, G8, GMO, anti-war, permaculture, sustainable, Schengen, reclaim the streets, RTS, food miles, copyleft, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, stop the war, Iraq sanctions, squatting, subvertise, satire, alternative news, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, Palestine occupation, Indymedia, Women speak out, Titnore Woods, piqueteros, cacerolas, asylum seeker, critical mass, animal rights, tsunami " />
<meta name="description" content="SchNEWS - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and social issues, direct action protests and campaigns, both UK and abroad. For this week's issue, Party & Protest events listing, free SchMOVIES, archive and more" />

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

<link rel="stylesheet" href="schnews_new.css" type="text/css" />
<link rel="stylesheet" href="index.css" type="text/css" />


<link rel="search" href="../search.htm" /> <link rel="SHORTCUT ICON" href="/favicon.ico" />
<link rel="alternate" type="application/rss+xml" href="/feed.xml" title="SchNEWS - a drop of truth in an ocean of bullshit. Weekly independent direct action news." />
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<script language="JavaScript"  type="text/javascript" src='javascript/index.js'></script>

</head>








<body
		leftmargin="0px"
		topmargin="0px"
		onLoad=		"MM_preloadImages('images_menu/archive-over.gif',
					'images_menu/about-over.gif',
					'images_menu/contacts-over.gif',
					'images_menu/guide-over.gif',
					'images_menu/monopolise-over.gif',
					'images_menu/prisoners-over.gif',
					'images_menu/subscribe-over.gif',
					'images_main/donate-mo.gif',
					'images_menu/schmovies-over.gif',
					'images_main/schmovies-button-over.png',
					'images_main/features-button-mo.png',
					'images_main/pap-button-mo.png',
					'images_main/schmovies-button-mo.png',
					'images_main/contacts-lhc-button-mo.gif',
					'images_main/book-reviews-button-mo.gif',
					'images_main/merchandise-button-mo.gif')"
>
<script type="text/javascript" language="javascript" src="javascript/jquery.js"></script>
<script type="text/javascript" language="javascript" src="javascript/indexMain.js"></script>




<div class='main_container'>



<!--	PAGE TITLE	-->
<div class="pageHeader">

			<div class="schnewsLogo">
			<a href="index.php"><img
					src="images_main/schnews.gif"
					width="292px"
					height="90px"
					alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..."
					border="0"
			/></a>
			</div>

			<?php

				//	Include the banner graphic
				require "inc/bannerGraphic.php";

			?>


</div>







<!--	NAVIGATION BAR 	-->

<div class="navBar">

	<?php
		//	Include the nav bar
		require "inc/navBar.php";

	?>

</div>







<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">

	<?php

		//	Include the Left Links Bar
		$includeLeftBarVideos	=	true;
		require "inc/leftBarMain.php";

	?>

</div>









<!-- ==============================================================================================

                        			COPYLEFT BAR

     ============================================================================================== -->
<div class="copyleftBar">

	<?php

		//	Include the CopyLeft Bar
		require "inc/copyLeftBar.php";

	?>
</div>













<!-- ==============================================================================================

                        			MAIN TABLE - LEFT BAR

     ============================================================================================== -->

<div class="mainBar_left">

		<div align="center" style='font-size: 10px'>
		  <p><b><font face="Arial, Helvetica, sans-serif">Recent
		  Main Stories ****RECENT_MAIN_STORIES****</font></b></p>

  </div>




  ****CURRENT_ISSUE****


  <p><?php echo make_horiz_divider(95, "000000"); ?>




<!-- Previous Issues -->

<font face="Arial, Helvetica, sans-serif"><b>Previous Issues</b></font></p>


***PREVIOUS_ISSUES****



<?php echo make_horiz_divider(95, "000000"); ?>
<br />



</div>
















<!-- ==============================================================================================
			Party and Protest Column
	=============================================================================================== -->
<div class="papBar">


		<!-- BEGIN UGLY BENEFITS GIG HACK -->

		<a href='http://www.schnews.org.uk/images/sea_final_900.jpg' target='_BLANK'>
			<img src='http://www.schnews.org.uk/images/sea_final_225.jpg' style='border: 2px solid black' alt='SchNEWS Sea Shepherd Punk Benefit Gig, 25th November...' />
		</a>

		<br />
		
		<?php echo make_horiz_divider(95, "000000"); ?>

		<!-- END UGLY BENEFITS GIG HACK -->



		<!--	Party and Protest Link	-->
		<div onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('papTitle','','images_main/pap-button-mo-225.png',1)">
			<a href='pap/guide.php'>
				<img name='papTitle' src='images_main/pap-button-225.png' width="225px" height="62" style='border: none' />
			</a>
		</div>
		<div style="padding-top: 8px; text-align:center">
			<a href="pap/guide.php">
				Full Listings
			</a>
			<br />
			<a href="pap/yourarea.php">
				What's On In Your Area
			</a>
		</div>

		<?php echo make_horiz_divider(95, "000000"); ?>

		<div style="text-align: center">
			<a href="forms/partysubmit.php">
				Submit New Event
			</a>
		</div>

		<?php echo make_horiz_divider(95, "000000"); ?>





		<p style="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><a name="jan" id="jan"></a></font></p>

					<?php
					// 	===================================================================================================
					//
					//			This processes the PaP entries from the database and shows the next xxx entries
					//
					//	===================================================================================================
					require "functions/config.php";


					$conn		=		mysql_connect($db_host, $db_user, $db_pass);
					$db			=		mysql_select_db($db_name, $conn);

					$now		=	substr(make_sql_datetime(), 0, 10).' 00:00:00';

					$i			=		1;
					for ($y		=	2008 ; $y <= 2020 ; $y++	)	{

								$sql	=	"SELECT id FROM pap WHERE (date_to >= '$now' OR date_from >= '$now') AND DATE_FORMAT(date_from, '%Y') = $y AND auth = 1 ORDER BY date_from ASC";
								$rs			=	mysql_query($sql);
								echo			mysql_error();
								//echo			"<br>$sql><br>";
								if (mysql_num_rows($rs) != 0)	{

								//echo "<b><font size=\"4\">$y</font></b><br><br>";

									for ($m		=	1; $m  <= 12 ; $m++)			{

										$sql_m			=	"SELECT id FROM pap WHERE (date_to >= '$now' OR date_from >= '$now') AND DATE_FORMAT(date_from, '%Y') = $y AND DATE_FORMAT(date_from, '%m') = $m AND auth = 1 ORDER BY date_from ASC";
										$rs_m			=	mysql_query($sql_m);
										echo			mysql_error();
										//echo			"<br>$sql_m<br>";

										if (mysql_num_rows($rs_m) != 0)	{

											if ($i <= $max_pap_index) echo "<div style='padding-top:5px; margin-top:10px; border-top: 1px solid #bbbbbb'><b><font size=\"3\">".date("F", mktime(0, 0, 0, $m, 1, 2000))."</font></b></div>";

											while	($res		=	mysql_fetch_row($rs_m) )	{
													if ($i > $max_pap_index) break;
													echo "<p>".show_pap($res[0], 40, 0)."</p>";

													$i++;
												}

											}
										}
									}

								}

					?> <p style="margin-bottom: 0cm"><font face="Arial, Helvetica, sans-serif"><a href="pap/index.htm">Click
		for the whole Party &amp; Protest guide</a></font></p>

</div>










<!-- ==============================================================================================

                        			MAIN TABLE - RIGHT BAR

     ============================================================================================== -->


<div class='mainBar_right'>


		<!-- BEGIN UGLY RADIERS 2 HACK -->

		<a href='http://www.schnews.org.uk/pages_merchandise/merchandise_video.php#raiders2'>
			<img src='../images_main/raiders2-ad-225.jpg' style='border: 2px solid black' alt='SchNEWS in Graphic Detail - Out Now' />
		</a>

		<br />

		<?php echo make_horiz_divider(95, "000000"); ?>

		<!-- END UGLY RADIERS 2 HACK -->




	<!-- BEGIN SchMOVIES	-->
	<div style='text-align: center; font-weight: bold; font-size: 15px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('schmoviesTitle','','images_main/schmovies-button-mo-225.png',1)">
		<a href='schmovies/' style='border: none'>
			<img src='images_main/schmovies-button-225.png' width="225px" height="60px" name='schmoviesTitle' style='border:none'/>
		</a>
	</div>

	<div>
		<?php echo get_schmovies_index_preview(); ?>
	</div>

	<?php echo make_horiz_divider(95, "000000"); ?>
	<br /><br />

	<!-- END SchMOVIES -->


	<!--	BEGIN FEATURES	-->
	<div style='text-align: center; padding-bottom: 10px' onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('featuresTitle','','images_main/features-button-mo-225.png',1)">
		<a href='features/' style='border: none'>
			<img name='featuresTitle' src='images_main/features-button-225.png' width="225px" width="55px" style='border:none'/>
		</a>
	</div>

	<div>
	For those with an attention span of more than three minutes, here is a new collection of articles too long, analytical or high-brow to normally publish in the weekly newsheet.
	</div>

	<?php echo make_horiz_divider(95, "ffffff"); ?>

	<div>
		<a href='features'>Click Here</a> for full list of feature articles...
	</div>

	<div class='feature_summary_list'>

		<?php
			echo get_features_for_homepage(6, false, 'features/');
		?>

	</div>

	<!-- END FEATURES -->

	<?php echo make_horiz_divider(95, "000000"); ?>
	<br /><br />




	<!--  Contact DB Advert -->
	<?php

		//	Require the contacts Advert
		require "inc/contacts_right_bar_advert.php";

	?>

	<!-- IndyMedia Link -->
	<div class='indyMediaLink'>
		<a href='http://www.indymedia.org.uk/' target='_BLANK'>
			<img src='images_main/imc-button.png' width='225px' height='43px' align='center' id='indyMediaLink' />
		</a>
	</div>

</div>








<div class="footer">

		<?php

			//	Include footer
			require "inc/footer.php";

		?>

</div>





<!-- CLOSE THE CONTAINER -->
</div>





</body>
</html>

























