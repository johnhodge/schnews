<?php 
session_start();
require 'hitsFunctions.php';

if (REQUIRELOGIN)	{
	
		if (!isset($_SESSION['loggedin']) || !$_SESSION['loggedin'])	exit;
	
}

		$percStart			=	"&nbsp;&nbsp;&nbsp<span style='color: #666666; font-size: 10pt'>";
		$percEnd			=	"</span>";
		$dateNow		=	substr(makeSqlDatetime(), 0, 10);
		$dateThen		=	subDaysFromDate($dateNow, 30);
		$whereDate		=	" date >= '$dateThen' ";
		
		$topLine		=	'Showing Data for Past 30 days';
		
		if (isset($_GET['date']) && strlen($_GET['date']) == 10)	{
			
				$dateGet		=	$_GET['date'];
				$dateGet		=	substr($dateGet, 6, 4).'-'.substr($dateGet, 3,2).'-'.substr($dateGet, 0,2);
				$whereDate		=	" date = '".mysql_escape_string($dateGet)."'";
				$topLine		=	'Showing Data for '.makeHumanDate($dateGet, 'words');
		}

?>


<html>
<head>
	<title><?php echo TITLE; ?>Traffic Analysis</title>
	<style>
	
		.heavy_border	{ border: 2px solid #555555; padding: 4px; font-weight: bold }
		.light_border	{ border: 1px solid #888888; padding: 4px } 
	
	</style>
</head>
<body>

<table width="90%" height="100%" align="center"><tr><td width="100%" height="100%" class="main_table" valign="top"><br><br><br>

	<h1 class="normal_text_large" align="center"><?php echo TITLE; ?> Traffic Analysis</h1>
	
	<h3 align='center'><?php echo $topLine; ?></h3>
	<?php 
	
		if (isset($dateGet))	{
			
				echo "<div align='center'><a href='?hits=1'>Show 30 days worth of data</a></div>";
			
		}
	
	?>
	
	<br />
	<br />
	<br />
	
	<?php 
		
	
		/*
		 * ==================================================================================================
		 * 
		 * 											PAGES
		 * 
		 * ==================================================================================================
		 */
		echo 	"<table width='95%' align='center'><tr><td width='50%' valign='top'>";
			
		$sql			=	" SELECT DISTINCT page FROM hits WHERE $whereDate ";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>Page</td>";
				echo 	"<td class='heavy_border'>Num Hits in Past 30 Days</td>";
				echo 	"</tr>";
				
				$pages		=	array();
				$percs		=	array();
				$totalHits	=	0;
				
				while ($res		=	ht_mysqlGetRow($rs))	{
						
						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE page = '{$res['page']}' AND $whereDate ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];	
						$pages[$res['page']]	=	$numHits;	
						$totalHits	+=		$numHits;									
					
				}
				
				foreach ($pages as $key => $value)	{
					
						$percs[$key]		=	($value / $totalHits) * 100;
					
				}
				
				arsort($pages);
				
				foreach($pages	as $page => $numHits )	{
					
						echo "<tr>";
						echo "<td class='light_border'>$page</td>";
						echo "<td class='light_border'>$numHits $percStart ( ". round($percs[$page]) ."% ) $percEnd </td>";
						echo "</tr>";
				}
				
				echo "</table>";
		
				
				
				
		/*
		 * ==================================================================================================
		 * 
		 * 											DATES
		 * 
		 * ==================================================================================================
		 */	
		echo 	"</td><td width='50%' valign='top'>";
		
				
				$sql			=	" SELECT DISTINCT date FROM hits WHERE date >= '$dateThen' ORDER BY date DESC";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>Date</td>";
				echo 	"<td class='heavy_border'>Hits</td>";
				echo 	"</tr>";
				
				$count		=	0;
				$hits		=	0;
				$max		=	0;
				$min		=	9999999999999;
				while ($res		=	ht_mysqlGetRow($rs))	{
					
						echo "<tr>";
						$date		=	makeHumanDate($res['date']);
						$day 		=	date ('l', mktime(0,0,0, substr($res['date'], 5, 2), substr($res['date'], 8, 2), substr($res['date'], 0, 4) ));
						echo "<td class='light_border'><a href='?hits=1&date=$date'>$date ( $day )</a></td>";
						
						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE date = '{$res['date']}' AND page = '".HOMEPAGE."' ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];	
					
						echo "<td class='light_border'>$numHits</td>";
						echo "</tr>";
						
						$count++;
						$hits	+= $numHits;
						if ($numHits > $max)	$max = $numHits;
						if ($numHits < $min)	$min = $numHits;
						
				}
				
				if ($count > 2)	{
					
					$hits -= $max;
					$hits -= $min;
					$count 	 -= 2;
					
				}
				if ($count != 0)		$averageHits	=	round($hits / $count);
				else	$averageHits = 0;
				
				echo "<tr><td class='heavy_border'>Daily Average</td><td class='light_border'>$averageHits</td></tr>";
				
				
				echo "</table>";
				
				
		
		echo 	"</td></tr></table>";
				

		?>
		<table class="table_border_bottom" width="100%">
			<tr>
				<td width="100%" ></td>
			</tr>
		</table><br />
		<?php 
	
		
		
		
		/*
		 * ==================================================================================================
		 * 
		 * 											REFERERS
		 * 
		 * ==================================================================================================
		 */
		echo 	"<table width='95%' align='center'><tr><td width='50%' valign='top'>";
		
		
				$sql			=	" SELECT DISTINCT source FROM hits WHERE $whereDate ";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>Referrer</td>";
				echo 	"<td class='heavy_border'>Num Hits in Past 30 Days</td>";
				echo 	"</tr>";
				
				$refs		=	array();		
				$percs		=	array();
				$totalHits	=	0;
				
				while ($res		=	ht_mysqlGetRow($rs))	{

						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE source = '{$res['source']}' AND $whereDate ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];
						$refs[$res['source']]	=	$numHits;	
						$totalHits	+=		$numHits;
					
				}
				
				foreach ($refs as $key => $value)	{
					
						$percs[$key]		=	($value / $totalHits) * 100;
					
				}
				
				arsort($refs);
				
				$i = 0;
				foreach	($refs as $ref => $numHits)	{
					
						$i++;
						if ($i == NUM_TO_SHOW) break;
						echo "<tr>";
						if ($ref == '')	{
							
							echo "<td class='light_border'>None</td>";
							
						}	else 	{
							
							echo "<td class='light_border'>$ref</td>";
						
						}
											
						echo "<td class='light_border'>$numHits $percStart ( ". round($percs[$ref], 2) ."% ) $percEnd </td>";
						echo "</tr>";
				}
				
				echo "</table>";
				
								
				
		/*
		 * ==================================================================================================
		 * 
		 * 											BROWSERS
		 * 
		 * ==================================================================================================
		 */		
		echo 	"</td><td width='50%' valign='top'>";
		
		
				$sql			=	" SELECT DISTINCT browser FROM hits WHERE $whereDate ";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>Browser</td>";
				echo 	"<td class='heavy_border'>Num Hits in Past 30 Days</td>";
				echo 	"</tr>";
				
				$browsers		=	array();
				$percs		=	array();
				$totalHits	=	0;
				
				while ($res		=	ht_mysqlGetRow($rs))	{

						if (DEBUG) echo "<b>Browser</b> : ".$res['browser'].'<br />';

						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE browser = '".mysql_escape_string($res['browser'])."' AND $whereDate ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];	
						
						$browsers[$res['browser']]	=	$numHits;
						
						$totalHits	+=		$numHits;
					
				}
				
				foreach ($browsers as $key => $value)	{
					
						$percs[$key]		=	($value / $totalHits) * 100;
					
				}
				
				arsort($browsers);
				
				$i = 0;
				foreach ($browsers as $browser => $numHits)	{
					
						$i++;
						if ($i == NUM_TO_SHOW) break;
						echo "<tr>";
						if ($browser == '')	{
							
							echo "<td class='light_border'>Unknown</td>";
							
						}	else 	{
							
							echo "<td class='light_border'>$browser</td>";
						
						}					
					
						echo "<td class='light_border'>$numHits $percStart ( ". round($percs[$browser], 2) ."% ) $percEnd </td>";
						echo "</tr>";
				}
				
				echo "</table>";
		
		
		
		echo 	"</td></tr></table>";
		
		
		?>
		<table class="table_border_bottom" width="100%">
			<tr>
				<td width="100%" ></td>
			</tr>
		</table><br />
		<?php 
	
		
		
		
		/*
		 * ==================================================================================================
		 * 
		 * 											IP ADDRESSES
		 * 
		 * ==================================================================================================
		 */
		echo 	"<table width='95%' align='center'><tr><td width='50%' valign='top'>";
		
		
				$sql			=	" SELECT DISTINCT ip_address FROM hits WHERE $whereDate ";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>IP Address</td>";
				echo 	"<td class='heavy_border'>Num Hits in Past 30 Days</td>";
				echo 	"</tr>";
				
				$ips		=	array();
				$percs		=	array();
				$totalHits	=	0;			
				
				while ($res		=	ht_mysqlGetRow($rs))	{	
					
						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE ip_address = '{$res['ip_address']}' AND $whereDate ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];	
						$ips[$res['ip_address']]	=	$numHits;
						$totalHits	+=		$numHits;
					
				}
				
				foreach ($ips as $key => $value)	{
					
						$percs[$key]		=	($value / $totalHits) * 100;
					
				}
				
				arsort($ips);
				
				
				$i = 0;
				foreach ($ips as $ip => $numHits)	{
					
						$i++;
						if ($i == NUM_TO_SHOW) break;
						echo "<tr>";
						echo "<td class='light_border'>$ip</td>";
						echo "<td class='light_border'>$numHits $percStart ( ". round($percs[$ip], 2) ."% ) $percEnd </td>";
						echo "</tr>";
				}
				
				echo "</table>";
				
				

				
				
		/*
		 * ==================================================================================================
		 * 
		 * 										SEARCH TERMS
		 * 
		 * ==================================================================================================
		 */
		echo 	"</td><td width='50%' valign='top'>";
		
		
				$sql			=	" SELECT DISTINCT search FROM hits WHERE $whereDate AND search <> '' ";
				$conn			=	ht_mysqlConnect();
				$rs				=	ht_mysqlQuery($sql, $conn);
				
				echo 	"<table width='95%' align='center'><tr>";
				echo 	"<td class='heavy_border'>Search Keywords</td>";
				echo 	"<td class='heavy_border'>Num Hits in Past 30 Days</td>";
				echo 	"</tr>";
				
				$searches		=	array();
				$percs		=	array();
				$totalHits	=	0;			
				
				while ($res		=	ht_mysqlGetRow($rs))	{

						
					
						$sql_t		=		" SELECT count(id) as numHits FROM hits WHERE search = '{$res['search']}' AND $whereDate ";
						$rs_t		=		ht_mysqlQuery($sql_t,$conn);
						$res_t		=		ht_mysqlGetRow($rs_t);
						$numHits	=		$res_t['numHits'];
						$searchTerm	=		str_replace('+', ' ', $res['search']);	
						$searches[$searchTerm]		=	$numHits;
						$totalHits	+=		$numHits;
					
				}
				
				foreach ($searches as $key => $value)	{
					
						$percs[$key]		=	($value / $totalHits) * 100;
					
				}
				
				arsort($searches);

				$i = 0;
				foreach($searches as $search => $numHits)	{
						
						$i++;
						if ($i == NUM_TO_SHOW) break;
						echo "<tr>";
						echo "<td class='light_border'>$search</td>";
						echo "<td class='light_border'>$numHits $percStart ( ". round($percs[$search], 2) ."% ) $percEnd </td>";
						echo "</tr>";
				}
				
				echo "</table>";
		
		
		
		echo 	"</td></tr></table>";
				
		
	
	?>
	
	
</body>
</html>