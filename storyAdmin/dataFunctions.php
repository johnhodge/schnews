<?php

/*	==================================================================================

{datap00003}				MISC DATA PROCESING FUNCTIONS

	==================================================================================	*/
if (! function_exists ( 'file_put_contents' )) {
	function file_put_contents($filename, $data) {
		$f = @fopen ( $filename, 'w' );
		if (! $f) {
			return false;
		} else {
			$bytes = fwrite ( $f, $data );
			fclose ( $f );
			return $bytes;
		}
	}
}

function getNumberOfCommentsForArticle($storyId) {

	if (! is_numeric ( $storyId ))
		return false;
	$conn = mysqlConnect ();
	$sql = " SELECT id FROM story_comments WHERE articleid = $storyId AND spam = 0 ";
	$rs = mysqlQuery ( $sql, $conn );
	return mysqlNumRows ( $rs, $conn );
}

function getNumberOfSpamCommentsForArticle($storyId) {

	if (! is_numeric ( $storyId ))
		return false;
	$conn = mysqlConnect ();
	$sql = " SELECT id FROM story_comments WHERE articleid = $storyId AND spam = 1 ";
	$rs = mysqlQuery ( $sql, $conn );
	return mysqlNumRows ( $rs, $conn );
}

function cleanEmail($email) {

	if (! is_string ( $email ))
		return $email;

	$replacements = array ('&ldquo;' => "'", '&rsquo;' => "'", '&rdquo;' => "'", '&lsquo;' => "'", '&ndash;' => '-', '&nbsp;' => ' ', '$pound;' => '�', '&aacute;' => 'a' );
	foreach ( $replacements as $find => $replace ) {

		$email = str_replace ( $find, $replace, $email );

	}

	return $email;

}

if (! function_exists ( 'send_smtp' )) {
	function send_smtp($recipient, $subject, $body) {

		// Clean Inputs
		$subject = stripslashes ( $subject );
		$body = cleanEmail ( stripslashes ( $body ) );

		if (SENDTYPE) {

			return @mail ( $recipient, $subject, $body, "From: " . SMTPSENDER . "\r\nContent-Type: text/plain; charset=ISO-8859-1" );

		}

		// Instantiate the PEAR::Mail class and pass it the SMTP auth details
		require_once ('Mail.php');
		$mail = Mail::factory ( "smtp", array ('host' => SMTPHOST, 'auth' => TRUE, 'username' => SMTPUSER, 'password' => SMTPPASS ) );

		if (is_array ( $recipient )) {
			foreach ( $recipient as $to ) {
				// Create the mail headers and send the mail via our SMTP object
				$headers = array ("From" => SMTPSENDER, "Subject" => $subject );
				$output = $mail->send ( $to, $headers, $body );

				if (PEAR::isError ( $mail )) {
					$return = FALSE;
				} else {
					$return = TRUE;
				}
			}
		} else {
			// Create the mail headers and send the mail via our SMTP object
			$headers = array ("To" => $recipient, "From" => SMTPSENDER, "Subject" => $subject );
			$output = $mail->send ( $recipient, $headers, $body );

			if (PEAR::isError ( $mail )) {
				$return = FALSE;
			} else {
				$return = TRUE;
			}
		}

		return $return;

	}
}

function raiseArticle($id, $mode) {

	if (! is_numeric ( $id )) {
		if (DEBUG)
			echo "<br>raiseArticle :: id not numeric <br>";
		return false;
	}
	if ($mode != 'raise' && $mode != 'lower') {
		if (DEBUG)
			echo "<br>raiseArticle :: mode not +- 1 <br>";
		return false;
	}

	$conn = mysqlConnect ();
	$sql = "SELECT story_number, issue FROM articles WHERE id = $id";
	$rs = mysqlQuery ( $sql, $conn );

	if (mysqlNumRows ( $rs, $conn ) != 1) {
		if (DEBUG)
			echo "<br>raiseArticle :: not one result from inital story selection <br>";
		return false;
	}

	$res = mysqlGetRow ( $rs );
	$storyNumber = $res ['story_number'];
	$issue = $res ['issue'];

	if ($mode == 'raise')
		$tempStoryNumber = $storyNumber - 1;
	else
		$tempStoryNumber = $storyNumber + 1;

	$sql = "SELECT id FROM articles WHERE story_number = $tempStoryNumber AND issue = $issue";
	$rs = mysqlQuery ( $sql, $conn );
	$res = mysqlGetRow ( $rs );
	$tempId = $res ['id'];

	if (mysqlNumRows ( $rs, $conn ) != 1) {
		if (DEBUG)
			echo "<br>raiseArticle :: story id + $mode doesn't exist <br>";
		return false;
	}

	$sql = "UPDATE articles SET story_number = $tempStoryNumber WHERE id = $id";
	$rs = mysqlQuery ( $sql, $conn );

	$sql = "UPDATE articles SET story_number = $storyNumber WHERE id = $tempId";
	$rs = mysqlQuery ( $sql, $conn );

	return true;

}

function recordHit($id) {

	if (! is_numeric ( $id ))
		return false;

	$conn = mysqlConnect ();
	$sql = " SELECT hits FROM articles WHERE id = $id ";
	$rs = mysqlQuery ( $sql, $conn );
	if (mysqlNumRows ( $rs, $conn ) != 1)
		return false;
	$res = mysqlGetRow ( $rs );
	$hits = $res ['hits'];

	if ($hits == '')
		$hits = 0;
	$hits ++;

	$sql = " UPDATE articles SET hits = $hits WHERE id = $id ";
	$rs = mysqlQuery ( $sql, $conn );

	return true;

}

function getNumKeywordOccurances($keyword) {

	if (! isset ( $keyword ) || $keyword == null || $keyword == '') {

		return false;

	}

	$keyword = mysql_escape_string ( $keyword );

	$conn = mysqlConnect ();
	$rs = mysqlQuery ( "SELECT id FROM keywords WHERE keyword = '$keyword' ", $conn );
	return mysqlNumRows ( $rs, $conn );

}

function filterKeywordList($keywords) {

	return $keywords;

	if (! is_array ( $keywords ))
		return false;

	$filteredList = array ();
	foreach ( $keywords as $keyword ) {

		if (getNumKeywordOccurances ( $keyword ) >= NUMKEYWORDS)
			$filteredList [] = $keyword;
	}

	if (count ( $filteredList ) == 0)
		return false;
	return $filteredList;

}

function getAllRankedKeywords() {

	$conn = mysqlConnect ();
	$sql = " SELECT keyword FROM keywords ORDER BY keyword ";
	$rs = mysqlQuery ( $sql, $conn );

	$tempKeyword = '';
	$keywords = array ();
	while ( $res = mysqlGetRow ( $rs ) ) {

		if ($tempKeyword != $res ['keyword']) {

			$tempKeyword = $res ['keyword'];
			$keywords [$tempKeyword] = 0;

		}

		$keywords [$tempKeyword] ++;

	}

	return $keywords;

}

function getSettings() {

	$conn = mysqlConnect ();
	$sql = " SELECT * FROM config ";
	$rs = mysqlQuery ( $sql, $conn );

	$res = mysqlGetRow ( $rs );

	$settings ['keywords'] = $res ['show_keywords'];
	$settings ['messages'] = $res ['show_messages'];
	$settings ['spamCount'] = $res ['spam_count'];
	$settings ['linkCount'] = $res ['link_count'];
	$settings ['numKeywords'] = $res ['num_keywords'];
	$settings ['hammerDuration'] = $res ['hammer_duration'];
	$settings ['txtMailingListAddress'] = $res ['txt_mailing_list_address'];

	return $settings;

}

function updateSettings($settings) {

	if (! is_array ( $settings ))
		return false;

	$conn = mysqlConnect ();
	$sql = " UPDATE config SET
							show_keywords = {$settings['keywords']},
							show_messages = {$settings['messages']},
							spam_count	  = {$settings['spam_count']},
							link_count	  = {$settings['link_count']},
							num_keywords  = {$settings['num_keywords']},
							hammer_duration = {$settings['hammer_duration']},
							txt_mailing_list_address = '{$settings['txt_mailing_list_address']}'
						WHERE id = 1
							";
	$rs = mysqlQuery ( $sql, $conn );

	return true;
}

function sortArray($array, $order = 'desc', $what = 'value') {

	if (! is_array ( $array ))
		return false;
	if ($what != 'value' && $what != 'key')
		$what = 'value';
	if ($order != 'desc' && $order != 'asc')
		$order = 'desc';

	if ($order == 'asc')
		array_multisort ( $array );
	else
		array_multisort ( $array, SORT_DESC );
	return $array;

}

function getIdsForKeyword($keyword) {

	if (! isset ( $keyword ) || is_null ( $keyword ) || $keyword == '')
		return false;
	$keyword = mysql_escape_string ( $keyword );

	$conn = mysqlConnect ();
	$rs = mysqlQuery ( " SELECT article_id FROM keywords WHERE keyword = '$keyword' ORDER BY article_id DESC ", $conn );
	if (mysqlNumRows ( $rs, $conn ) == 0)
		return false;

	$ids = array ();
	while ( $res = mysqlGetRow ( $rs ) ) {

		if ($res ['article_id'] == 0 || $res ['article_id'] == null)
			continue;

		$ids [] = $res ['article_id'];

	}
	return $ids;

}

function getFeatureIdsForKeyword($keyword) {

	if (! isset ( $keyword ) || is_null ( $keyword ) || $keyword == '')
		return false;
	$keyword = mysql_escape_string ( $keyword );

	$conn = mysqlConnect ();
	$rs = mysqlQuery ( " SELECT feature_id FROM keywords WHERE keyword = '$keyword' ORDER BY article_id DESC ", $conn );
	if (mysqlNumRows ( $rs, $conn ) == 0)
		return false;

	$ids = array ();
	while ( $res = mysqlGetRow ( $rs ) ) {

		if ($res ['feature_id'] == 0 || $res ['feature_id'] == null)
			continue;

		$ids [] = $res ['feature_id'];

	}
	return $ids;

}

/**
 * Returns the number of articles that feature a given keyword.
 *
 * @param STRING $kewyord
 * @return ARRAY
 */
function getNumofKeyword($keyword) {

	if (! isset ( $keyword ) || is_null ( $keyword ) || $keyword == '')
		return false;
	$keyword = mysql_escape_string ( $keyword );

	$conn = mysqlConnect ();
	$rs = mysqlQuery ( " SELECT article_id FROM keywords WHERE keyword = '$keyword' ORDER BY article_id DESC ", $conn );
	return mysqlNumRows ( $rs, $conn );

}

/**
 * Returns an associative array containing all the data for a given article
 *
 * @param INT $id
 * @return ARRAY
 */
function getArticleSummary($id) {

	if (! is_numeric ( $id ))
		return false; //	INPUT MUST BE A NUMBER


	$conn = mysqlConnect ( $id );
	$rs = mysqlQuery ( " SELECT issue, story_number, headline, story FROM articles WHERE id = $id ", $conn );
	if (mysqlNumRows ( $rs, $conn ) != 1)
		return false; // THERE SHOULD BE ONLY ONE ARTICLE FOR THE GIVEN ID


	$article = mysqlGetRow ( $rs );

	$rs = mysqlquery ( " SELECT issue, date FROM issues WHERE id = {$article['issue']} ", $conn );
	if (mysqlNumRows ( $rs, $conn ) != 1)
		return false; // THERE SHOULD BE ONLY ONE ARTICLE FOR THE GIVEN ID
	$issue = mysqlGetRow ( $rs );

	$output = array ('headline' => $article ['headline'], 'story' => $article ['story'], 'story_number' => $article ['story_number'], 'issue' => $issue ['issue'], 'date' => $issue ['date'] );
	return $output;

}

function cleaninput($input) {

	if (! is_string ( $input ))
		return $input;
	if (get_magic_quotes_gpc ()) {

		return $input;

	} else {

		return mysql_escape_string ( $input );

	}

}

function scanForCustomTags($text) {

	//return $text;

	if (! is_string ( $text ))
		return $text;

	$pos = 0;
	$output = '';

	while ( $pos < strlen ( $text ) ) {

		if ($text {$pos} == '<') {

			$tagEnd = strpos ( $text, '>', $pos ) + 1;
			if (! is_numeric ( $tagEnd )) {

				$pos ++;
				continue;

			}

			$tag = substr ( $text, $pos, $tagEnd - $pos );

			if (! $parsedTag = parseTag ( $tag )) {

				if (substr($tag, 0, 4) == '<img') echo "<p>Image tag found :: $tag";
				//echo "<p>Failed parseTag on <b>$tag</b></p>";
				$output .= $tag;
				$pos = $tagEnd;
				continue;

			} else {

				if (DEBUG && is_string($tag)) {

					echo "<div style='color:red'>Tag = " . htmlspecialchars ( $tag ) . "</div><div style='color:red'>Parsed Tag = " . var_dump ( $parsedTag ) . "</div>";

				}

				if (! $output .= replaceCustomTags ( $parsedTag )) {
					//echo "<p>Failed replaceCustomTags</p>";
					$output .= $tag;
					$pos = $tagEnd;
					continue;
				}

			}

			$pos = $tagEnd;

		} else {

			$output .= $text {$pos};
			$pos ++;

		}

	}
	$output = str_replace ( '<heading>', '<span style="font-size:13px; font-weight:bold">', $output );
	$output = str_replace ( '</heading>', '</span>', $output );

	$output = str_replace ( '<q>', '<span style="font-style:italic">', $output );
	$output = str_replace ( '</q>', '</span>', $output );

	return $output;

}

function replaceCustomTags($tag) {

	//echo "<p><b>Replace Custom Tags Running</b></p>";


	if (! is_array ( $tag ) || ! isset ( $tag ['name'] )) {
		//echo "<p>replaceCustomTags :: not array or name not set</p>";
		return false;
	}

	switch ($tag ['name']) {

		case 'image' :
			if (! isset ( $tag ['big'] ) || ! isset ( $tag ['small'] ))
				break;
			else {

				$tag ['small'] = processImageURL ( $tag ['small'] );
				$imageSize = getimagesize ( $tag ['small'] );
				$width = $imageSize [0];

				$imageTemplate = "<table align=\"left\"><tr><td style=\"padding: 0 8 8 0; width: $width\" width=\"" . $width . "px\"><a href=\"***IMAGE_LARGE***\" target=\"_blank\"><img style=\"border:2px solid black\" src=\"***IMAGE_SMALL***\" ***ALT_TEXT*** /></a></td></tr>***CAPTION***</table>";
				$output = str_replace ( '***IMAGE_LARGE***', processImageURL ( $tag ['big'] ), $imageTemplate );
				$output = str_replace ( '***IMAGE_SMALL***', processImageURL ( $tag ['small'] ), $output );

				if (isset ( $tag ['alt'] ))
					$altTag = " alt='{$tag['alt']}' ";
				else
					$altTag = '';
				$output = str_replace ( '***ALT_TEXT***', $altTag, $output );

				if (isset ( $tag ['caption'] ))
					$captionTag = "<tr><td align='center'><div style=\"text-align: center; font-size:7px; color:#444444; border-bottom:solid 1px #666666; margin-bottom:6px; padding-bottom: 6px; width:" . ($width - 25) . "\">{$tag['caption']}</div></td></tr>";
				else
					$captionTag = '';
				$output = str_replace ( '***CAPTION***', $captionTag, $output );

				return $output;

			}
			break;

		case 'img' :

			if (isset($tag['big']) && isset($tag['small']))	{

				if (! isset ( $tag ['big'] ) || ! isset ( $tag ['small'] ))
					return '<img src="' . $tag ['src'] . '" />';
				else {

					$tag ['small'] = processImageURL ( $tag ['small'] );
					$imageSize = getimagesize ( $tag ['small'] );
					$width = $imageSize [0];

					$imageTemplate = "<table align=\"left\"><tr><td style=\"padding: 0 8 8 0; width: $width\" width=\"" . $width . "px\"><a href=\"***IMAGE_LARGE***\" target=\"_blank\"><img style=\"border:2px solid black\" src=\"***IMAGE_SMALL***\" ***ALT_TEXT*** /></a></td></tr>***CAPTION***</table>";
					$output = str_replace ( '***IMAGE_LARGE***', processImageURL ( $tag ['big'] ), $imageTemplate );
					$output = str_replace ( '***IMAGE_SMALL***', processImageURL ( $tag ['small'] ), $output );

					if (isset ( $tag ['alt'] ))
						$altTag = " alt='{$tag['alt']}' ";
					else
						$altTag = '';
					$output = str_replace ( '***ALT_TEXT***', $altTag, $output );

					if (isset ( $tag ['caption'] ))
						$captionTag = "<tr><td align='center'><div style=\"text-align: center; font-size:7px; color:#444444; border-bottom:solid 1px #666666; margin-bottom:6px; padding-bottom: 6px; width:" . ($width - 25) . "\">{$tag['caption']}</div></td></tr>";
					else
						$captionTag = '';
					$output = str_replace ( '***CAPTION***', $captionTag, $output );

					return $output;

				}

			}	else	{

				$output		=	'<'.$tag['name'];
				foreach ($tag as $element => $value)	{

					if ($element == 'name') continue;
					$output	.=	" $element=\"$value\" ";

				}
				$output		.=	" /> ";
			}

			return $output;
			break;


		case 'line' :
			$tagTemplate = "<div style=\"padding-top:1px; border-bottom:1px solid #999999; margin-bottom:10px; width:{$tag['width']}%; align:left\" align=\"left\">&nbsp;</div>";
			return $tagTemplate;
			break;

		case 'box' :
			if (! isset ( $tag ['text'] ))
				break;
			$output = "<table width='250px' align='right'><tr><td>";
			$output .= "<div style='width:250px; font-size: 13px; font-family:times new roman; font-style: italic ; padding:18px; color: #777777; text-align:center'>{$tag['text']}</div>";
			$output .= "</td></tr></table>";
			return $output;

	}

	//echo "<p>replaceCustomTags :: no output made</p>";
	return false;

}

function parseTag($tag) {

	//echo "<p><b>parseTag Running</b><br />Tag :: '".htmlspecialchars($tag)."'</p>";


	//$tagElements	=	explode(' ', $tagStripped);
	if (!$tagElements = splitTagElements ( $tag ))	{

		if (DEBUG) var_dump($tag);
		return false;

	}

	foreach ( $tagElements as $tagElement ) {

		if ($tagElement == 'image')
			$data ['name'] = 'image';
		if ($tagElement == 'img')
			$data ['name'] = 'img';
		elseif ($tagElement == 'line')
			$data ['name'] = 'line';
		elseif ($tagElement == 'box')
			$data ['name'] = 'box';

		else {

			$attributes = explode ( '=', $tagElement );
			if (count ( $attributes ) != 2)
				continue;
			$attributeName = $attributes [0];
			$attributeValue = $attributes [1];
			//$attributeValueStripped = 	str_replace(array('"', "'"), '', $attributeValue);
			//if (strlen($attributeValue) != strlen($attributeValueStripped) + 2) continue;


			$attributeValue = trim ( $attributeValue );
			$attributeValue = substr ( $attributeValue, 1 );
			$attributeValue = substr ( $attributeValue, 0, strlen ( $attributeValue ) - 1 );

			$data [$attributeName] = $attributeValue;

		}

	}

	if (isset ( $data ['name'] ))
		return $data;
	else {
		//echo "<p>parseTag :: name not set</p>";
		return false;
	}

}

function splitTagElements($tag) {

	//	TEST INPUTS
	if (! is_string ( $tag ))
		;

	//	REMOVE THE ARROW BRACKETS
	$tagStripped = trim ( str_replace ( array ('<', '>' ), '', $tag ) );
	if (strlen ( $tagStripped ) != (strlen ( $tag ) - 2)) {
		//echo "<p>parseTag :: stripped len != len - 2</p><p>TagStripped = ".htmlentities($tagStripped)."; Tag = ".htmlentities($tag)."</p>";
		return false;
	}

	//	SPLIT THE TAG APPART USING SPACES AS A DELIMITER
	$tagSplit = explode ( ' ', $tagStripped );

	// CYCLE THROUGH EACH ELEMENT AND GLUE TOGETHER ANY QUOTES PHRASES
	foreach ( $tagSplit as $key => $element ) {

		$lookupMode = false;
		if (strpos ( $element, '="' ) != false && (strlen ( $element ) - 2) != strlen ( str_replace ( '"', '', $element ) ))
			$lookupMode = '"';
		if (strpos ( $element, "='" ) != false && (strlen ( $element ) - 2) != strlen ( str_replace ( "'", '', $element ) ))
			$lookupMode = "'";

		if ($lookupMode != false) {

			$n = $key + 1;
			$stillInQuotedSection = true;
			while ( $stillInQuotedSection ) {

				if (! isset ( $tagSplit [$n] ))
					break;

				$tagSplit [$key] .= ' ' . $tagSplit [$n];

				if (strpos ( $tagSplit [$n], $lookupMode ) != false) {

					$stillInQuotedSection = false;

				}

				unset ( $tagSplit [$n] );

				$n ++;

			}

		}

	}
	//var_dump($tagSplit);
	return $tagSplit;

}

function testLinkTargetExists($link) {

	// *****************************************************
	//		KNIGHTSBRIDGE HAVE BORKED THE CURL LIBRARY SUPPORT ON THEIR SERVER.
	//
	//		ONCE THEY HAVE FIXED IT TAKE THIS LINE OUT.
	//			03-04-09


	if (! is_string ( $link ))
		return false;
	if (! isAbsoluteLink ( $link ))
		return false;

	//$cHandle		=	curl_init($link);


	//echo "<br>$link<br>";
	$ch = curl_init ();
	curl_setopt ( $ch, CURLOPT_URL, $link );
	curl_setopt ( $ch, CURLOPT_HEADER, true );
	curl_setopt ( $ch, CURLOPT_NOBODY, true );
	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $ch, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt ( $ch, CURLOPT_MAXREDIRS, 10 ); //follow up to 10 redirections - avoids loops
	$data = curl_exec ( $ch );
	curl_close ( $ch );
	preg_match_all ( "/HTTP\/1\.[1|0]\s(\d{3})/", $data, $matches );
	$code = end ( $matches [1] );
	if (! $data) {

		//echo "Domain could not be found";
		return false;

	} else {

		if ($code == 200) {

			//echo "Page Found";
			return true;

		} elseif ($code == 404) {

			//echo "Page Not Found";
			return false;

		}

	}

	//curl_close($cHandle);
	return true;

}

function isAbsoluteLink($link) {

	if (! is_string ( $link ))
		return false;

	$linkParts = array ('http://', 'ftp;//', 'www', '.com.', 'co.uk', 'org', 'https://' );
	if (! firstPos ( $link, $linkParts ))
		return false;
	return true;

}

function fileLinkValid($link) {

	if (! is_string ( $link ))
		return false;

	if (isAbsoluteLink ( $link )) {

		//echo "<br>it is an abs link<br>";
		if (testLinkTargetExists ( $link ))
			return true;
		else
			return false;

	} else {

		//echo "<br>it is not an abs link<br>";
		if (file_exists ( $link ))
			return true;
		else
			return false;

	}

	return false;

}

function processImageURL($image) {

	if (! is_string ( $image ))
		return false;

	//if (file_exists($image) || testLinkTargetExists($image)) return $image;


	if (! file_exists ( $image ) && ! isAbsoluteLink ( $image )) {

		if (file_exists ( 'images/' . $image ))
			return 'images/' . $image;
		if (file_exists ( '../images/' . $image ))
			return '../images/' . $image;
		if (file_exists ( 'images_main/' . $image ))
			return 'images_main/' . $image;
		if (file_exists ( '../images_main/' . $image ))
			return '../images_main/' . $image;
		if (file_exists ( 'images_issues/' . $image ))
			return 'images_issues/' . $image;
		if (file_exists ( '../images_issues/' . $image ))
			return '../images_issues/' . $image;

	} elseif (! testLinkTargetExists ( $image )) {

		if (testLinkTargetExists ( 'http://' . $image ))
			return 'http://' . $image;
		if (testLinkTargetExists ( 'https://' . $image ))
			return 'https://' . $image;
		if (testLinkTargetExists ( 'ftp://' . $image ))
			return 'ftp://' . $image;

	}

	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images/' . $image )) {

		return 'http://www.schnews.org.uk/images/' . $image;

	}
	if (testLinkTargetExists ( '../images_main/' . $image )) {

		return '../images_main/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images_issues/' . $image )) {

		return 'http://www.schnews.org.uk/images_issues/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images-book-reviews/' . $image )) {

		return 'http://www.schnews.org.uk/images-book-reviews/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images_extra/' . $image )) {

		return 'http://www.schnews.org.uk/images_extra/' . $image;

	}
	if (testLinkTargetExists ( '../images_main-01/' . $image )) {

		return '../images_main-01/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images_menu/' . $image )) {

		return 'http://www.schnews.org.uk/images_menu/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images_merchandise/' . $image )) {

		return 'http://www.schnews.org.uk/images_merchandise/' . $image;

	}
	if (testLinkTargetExists ( 'http://www.schnews.org.uk/images-reviews/' . $image )) {

		return 'http://www.schnews.org.uk/images-reviews/' . $image;

	}

	return $image;

}

function mergeKeywords($from, $to) {

	if (! is_string ( $from ) || ! is_string ( $to ))
		return false;

	$conn = mysqlConnect ();
	$sql = "UPDATE keywords SET keyword = '$to' WHERE keyword = '$from'";
	$rs = mysqlQuery ( $sql, $conn );

	$sql = " SELECT id FROM keywords WHERE keyword = '$from' ";
	$rs = mysqlQuery ( $sql, $conn );

	if (mysqlNumRows ( $rs, $conn ) != 0)
		return false;
	else
		return true;

}

function incrementIssueHitCounter($issueId) {

	//	ENSURE OUR INPUT IS GOOD...
	if (! is_numeric ( $issueId ) || ! $issueId)
		return false;

	$conn = mysqlConnect ();
	$sql = " SELECT hits FROM issues WHERE id = $issueId ";
	$rs = mysqlQuery ( $sql, $conn );

	if (mysqlNumRows ( $rs, $conn ) != 1)
		return false;
	$res = mysqlGetRow ( $rs );
	$num = $res ['hits'];

	if (! is_numeric ( $num ) || ! $num || $num == '')
		$num = 0;
	$num ++;

	$sql = " UPDATE issues SET hits = $num WHERE id = $issueId ";
	$rs = mysqlQuery ( $sql, $conn );
	$conn = null;

	return true;

}

/**
 * Updates the locked state of a story. Locked stories cannot have comments posted.
 *
 * @param int $storyId					The MySQL id of the row in the articles table
 * @param int $mode						1 = lock the story / 0 = unlock the story
 * @return bool							Success state of the operation.
 */
function lockStoryForComments($storyId, $mode) {

	//	TEST INPUTS
	if (! is_numeric ( $storyId ))
		return false;
	if (! is_numeric ( $mode ) || ($mode != 1 && $mode != 0))
		return false;

	//	DO THE SQL UPDATE
	$conn = mysqlConnect ();
	$sql = " UPDATE articles SET locked = $mode WHERE id = $storyId ";
	$rs = mysqlQuery ( $sql, $conn );
	$conn = null;

	// END FUNCTION
	return true;

}


/**
 * Updates the hidden state of a story. Hidden stories cannot have comments posted.
 *
 * @param int $storyId					The MySQL id of the row in the articles table
 * @param int $mode						1 = hide the story / 0 = unhide the story
 * @return bool							Success state of the operation.
 */
function hideStoryForComments($storyId, $mode) {

	//	TEST INPUTS
	if (! is_numeric ( $storyId ))
		return false;
	if (! is_numeric ( $mode ) || ($mode != 1 && $mode != 0))
		return false;

	//	DO THE SQL UPDATE
	$conn = mysqlConnect ();
	$sql = " UPDATE articles SET locked = $mode, hide_comments = $mode WHERE id = $storyId ";
	$rs = mysqlQuery ( $sql, $conn );
	$conn = null;

	// END FUNCTION
	return true;

}





function get_feature_categories() {

	$conn = mysqlConnect ();
	$sql = " 	SELECT *
					FROM features_categories
					ORDER BY category ASC ";
	$rs = mysqlQuery ( $sql, $conn );

	$output = array ();

	while ( $res = mysqlGetRow ( $rs ) ) {

		$output [$res ['id']] = $res ['category'];

	}

	return $output;

}

/**
 * Returns an array of usernames, or user pseudonyms, with their MySQL id as the key
 *
 * @param bool $use_pseudonyms			//	Use pseudonyms rather than real names if true
 * @return array						//	List of names, with MySQL ids as keys
 */
function get_users($use_pseudonyms = false) {

	//	Test inputs...
	if (! is_bool ( $use_pseudonyms ))
		$use_pseudonyms = false;

	//	Are we looking for names or pseudonyms?
	if ($use_pseudonyms) {

		$type = 'pseudonym';

	} else {

		$type = 'name';

	}

	//	Grab the data from the database
	$conn = mysqlConnect ();
	$sql = " 	SELECT id, $type
					FROM users
					ORDER BY name ASC ";
	$rs = mysqlQuery ( $sql, $conn );

	//	Add the data to the output array...
	$output = array ();
	while ( $res = mysqlGetRow ( $rs ) ) {

		$output [$res ['id']] = $res [$type];

	}

	//	Close the MySQL connection...
	mysql_close ( $conn );

	//	Return the array and finish...
	return $output;

}

/**
 * Gets all the user info for a given user (as an assoc-array)
 *
 * @param int $id			//	The MySQL id of the user
 * @return array			//	Array containing the user info, in the form of 'MySQL Field' => 'Value'
 */
function get_user($id) {

	//	Test inputs...
	if (! is_numeric ( $id ))
		return false;

	//	Get the user from the database...
	$conn = mysqlConnect ();
	$sql = "	SELECT *
					FROM users
					WHERE id = $id";
	$rs = mysqlQuery ( $sql, $conn );

	//	Store the user in a local var...
	$user = mysqlGetRow ( $rs );

	//	Close the MySQL connection
	mysql_close ( $conn );

	//	Return the user array...
	return $user;

}

function get_user_types() {

	return array (0 => 'Feature Writer', 1 => 'Editor', 2 => 'Webmaster' );

}

function test_login_attempt($username, $password) {

	if ($username == '' || $password == '')
		return false;

	$username = cleaninput ( $username );
	$password = cleaninput ( $password );

	$conn = mysqlConnect ();
	$sql = "	SELECT id
						FROM users
						WHERE		username = '$username'
								AND	password = '$password'";
	$rs = mysqlQuery ( $sql, $conn );

	if (mysqlNumRows ( $rs, $sql ) == 0)
		return false;
	$res = mysqlGetRow ( $rs );
	return $res ['id'];

}

function make_random_string($length = 10) {

	//	Test inputs
	if (! is_numeric ( $length ) || $length == 0) {

		$length = 10;

	}

	$chars = array ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' );

	$string = '';
	for($n = 0; $n < $length; $n ++) {

		$num = rand ( 0, count ( $chars ) - 1 );
		$string .= $chars [$num];

	}

	return $string;

}

function get_num_votes_for_features($feature_id) {

	//	Test inputs...
	if (! is_numeric ( $feature_id ) || $feature_id == 0)
		return false;

	$conn = mysqlConnect ();
	$sql = "	SELECT	count(id) AS num
					FROM features_voting
					WHERE feature_id = $feature_id";
	$rs = mysqlQuery ( $sql, $conn );
	$res = mysqlGetRow ( $rs );
	mysql_close ( $conn );

	if (is_numeric ( $res ['num'] )) {

		return $res ['num'];

	} else {

		return false;

	}

}

/*
 * 	Creates an array, showing the number of comments per feature, either spam comments or real
 *
 * 	Inputs
 * 		$spam		bool		//	True is we are looking for num spam comments, false if not
 *
 * 	Output
 * 		array					//	Key = feature id, value = number of comments
 */
function get_num_feature_comments($spam = false) {

	//	Test inputs...
	if (! is_bool ( $spam ))
		$spam = false; //	If spam is true then we're getting the number of spam comments per feature, if false then we're getting the number of non-spam comment per feature


	//	Are we looking for spam comments or not, construct an addition to the MySQL where clause depending...
	if ($spam) {

		$where = " spam = 1 ";

	} else {

		$where = " spam = 0 ";

	}

	//	Grab what we need from the DB...
	$conn = mysqlConnect ();
	$sql = " 	SELECT featureid	as	id
							FROM story_comments
							WHERE 	featureid IS NOT null
							AND 	featureid <> 0
							AND		$where ";
	$rs = mysqlQuery ( $sql, $conn );

	//	Prepare the comments array for output...
	$comments = array ();
	while ( $res = mysqlGetRow ( $rs ) ) {

		if (isset ( $comments [$res ['id']] ))
			$comments [$res ['id']] ++;
		else
			$comments [$res ['id']] = 1;

	}

	// Be tidy...
	mysql_close ( $conn );

	return $comments;

}






function get_feature_test_hash($feature_id) {

	//	Test inputs
	if (! is_numeric ( $feature_id )) {

		if (DEBUG) echo "get_feature_hash input id not numeric";
		return false;

	}

	//	Query the DB...
	$conn = mysqlConnect ();
	$sql = " 	SELECT test_hash
						FROM features
						WHERE id = $feature_id ";
	$rs = mysqlQuery ( $sql, $conn );

	//	Test that we got a row and only one row...
	if (mysqlNumRows ( $rs, $conn ) != 1) {

		return false;

	}

	//	Get hash...
	$res = mysqlGetRow ( $rs );
	$hash = $res ['test_hash'];
	$conn = mysql_close ( $conn );

	//	Test hash...
	if ($hash == '') {

		return 'not set';

	} else {

		return $hash;

	}

}



function set_feature_test_hash($feature_id)	{

	//	Test inputs
	if (! is_numeric ( $feature_id )) {

		if (DEBUG) echo "set_feature_hash input id not numeric";
		return false;

	}

	$hash 	= 	cleaninput(md5(makeSqlDatetime()));
	$conn	=	mysqlConnect();
	$sql	=	" 	UPDATE features
					SET test_hash = '$hash'
					WHERE id = $feature_id ";
	$rs		=	mysqlQuery($sql, $conn);

	mysql_close($conn);

	return $hash;

}



function increment_other_hits($type)	{

	if (!is_string($type) || $type == '')	return false;

	$conn		=	mysqlConnect();
	$sql		=	" SELECT hits FROM other_hits WHERE type = '$type' ";
	$rs			=	mysqlQuery($sql, $conn);

	if (mysqlNumRows($rs,$conn) == 0)	{

		$sql		=	" INSERT INTO other_hits (type, hits) VALUES ('$type', 1)";
		$rs			=	mysqlQuery($sql, $conn);
		mysql_close($conn);
		return true;

	}

	if (mysqlNumRows($rs, $conn) == 1)	{

		$res		=	mysqlGetRow($rs);
		$hits		=	$res['hits'];

		$sql		=	" UPDATE other_hits SET hits = ".($hits + 1)." WHERE type = '$type' ";
		$rs			=	mysqlQuery($sql, $conn);
		mysql_close($conn);
		return true;

	}

	return false;

}




//	Selects all the DIY categories from the database and returns them as an indexed array (key = sql ID, value = cat)
function diy_get_categories()	{

	//	Do DB Lookup
	$conn	=	mysqlConnect();
	$sql	=	"	SELECT *
					FROM diy_categories
					ORDER BY category ASC ";
	$rs		=	mysqlQuery($sql, $conn);

	//	Put results into array...
	$output	=	array();
	while	($res	=	mysqlGetRow($rs))	{

		$output[$res['id']]		=	$res['category'];

	}

	mysql_close($conn);
	return $output;

}


//	Gets a given article's data from DB. Returns empty data is ID == 0
function diy_get_article($article_id)	{

	//	Test input...
	if (!is_numeric($article_id))	{

		if (DEBUG) echo "article_id not numeric :: returning false";
		return false;

	}

	//	Initialise our array;
	$article				=	array();
	$article['title']		=	'';
	$article['category']	=	'';
	$article['content']		=	'';
	$article['date_add']	=	'';
	$article['date_mod']	=	'';
	$article['hits']		=	'';

	//	If we are adding the article then return the empty data...
	if ($article_id == 0)	{

		return false;

	}

	//	Else we muct be editing an existing article....
	$conn		=	mysqlConnect();
	$sql		=	"	SELECT title, category, content
						FROM diy_articles
						WHERE id = article_id	";
	$rs			=	mysqlQuery($sql, $conn);

	//	There should only be one result...
	if (mysqlNumRows($rs, $conn))	{

		if (DEBUG) echo "Not one result when getting article..";
		exit;

	}

	$article	=	mysqlGetRow($rs);
	return $article;

}



//	Edits a given category in the database (will add the category if id == 0)
function diy_edit_category($id, $category)	{

	//	Test inputs
	if (!is_numeric($id))	{

		if (DEBUG) echo "Input ID not numberic";
		return false;

	}
	if (!is_string($category) || trim($category) == '')	{

		if (DEBUG) echo "Input Category not valid string";
		return false;

	}


	$categories		=	diy_get_categories();	//	Cache existing categories

	$category		=	cleaninput($category);

	if (in_array($category, $categories))	{

		echo "<div style='color:red'>Category '<b>$category</b>' is already in the database</div>";
		return false;

	}


	$conn			=	mysqlConnect();
	/*
	 * 	If we are editing an existing category then ID will be a positive integer corresponding
	 * 	to the SQL id of the category.
	 */
	if ($id)	{

		$sql		=	"	UPDATE diy_categories
							SET category = '$category'
							WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);

	/*
	 * 	If we are adding a new category then the ID will be zero (i.e. false)
	 */
	}	else	{

		$sql		=	" 	INSERT INTO diy_categories
							( category )
							VALUES
							( '$category' ) ";
		$rs			=	mysqlQuery($sql, $conn);
		$id			=	mysqlGetInsertId($conn);

	}

	mysql_close($conn);
	return $id;

}


//	Deletes an category entry from the DB, based on a given SQL row ID.
function diy_delete_category($id)	{

	//	Test inputs...
	if (!is_numeric($id) || !$id)	{

		if (DEBUG) echo "Input ID not a postive integer";
		return false;

	}

	//	Delete from DB
	$conn	=	mysqlConnect();
	$sql	=	"	DELETE FROM diy_categories
					WHERE id = $id ";
	$rs		=	mysqlQuery($sql, $conn);

	return true;

}



function is_email_address($email) {
  // First, we check that there's one @ symbol, and that the lengths are right
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
     if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
      return false;
    }
  }
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}




/*
 * 	Gets the keywords for a given article
 *
 * 	Inputs
 * 		$article_id			INT			//	The mysql id of the article
 *
 * 	Output
 * 		$keywords			ARRAY		//	The keywords in an indexed array
 */
function get_keywords_for_article($article_id)	{

	// Test inputs
	if (!is_numeric($article_id) || !$article_id)	return false;

	//	Grab data from DB
	$conn		=	mysqlConnect();
	$sql		=	"	SELECT keyword
						FROM keywords
						WHERE article_id = $article_id
						ORDER BY keyword ASC";
	$rs			=	mysqlQuery($sql, $conn);

	//	If there are no keywords we may as well just return an empty array...
	if (mysqlNumRows($rs, $conn) == 0) return array();

	// Put the results into an indexed array...
	$keywords		=	array();
	while ($res		=	mysqlGetRow($rs))	{

		$keywords[]		=	stripslashes($res['keyword']);

	}

	return $keywords;

}





?>