<?php


/*
 * 	============================================================
 * 						Prepare Data
 * 	============================================================
 */

//	Are we editing a cateogry or adding a new one?
$edit_category	=	0;
if (isset($_GET['category']) && is_numeric($_GET['category']))	{

	$edit_category		=	$_GET['category'];

}



if (isset($_GET['edit_category']) && is_numeric($_GET['edit_category']) && isset($_POST['category_submit']))	{

		$update_id			=	$_GET['edit_category'];
		$update_category	=	cleaninput($_POST['category']);

		$conn				=	mysqlConnect();
		if ($update_id	==	0)	{

			//	Test for duplicates
			$sql	=	"	SELECT id FROM schmovies_categories WHERE category = '$update_category' ";
			$rs		=	mysqlQuery($sql, $conn);

			if (mysqlNumRows($rs, $conn) == 0)	{

				$sql	=	" INSERT INTO schmovies_categories (category) VALUES ( '$update_category' ) ";
				$rs		=	mysqlQuery($sql, $conn);
				$edit_category	=	mysqlGetInsertId($conn);
				$text	=	"New category <b>'$update_category'</b> added";

			}	else	{

				$text	=	"Cannot add category <b>'$update_category'</b> as it already exists";

			}

		}	else	{

			//	Test for duplicates
			$sql	=	"	SELECT id FROM schmovies_categories WHERE category = '$update_category' && id <> $update_id ";
			$rs		=	mysqlQuery($sql, $conn);
			$edit_category	=	$update_id;

			if (mysqlNumRows($rs, $conn) == 0)	{

				$sql	=	" UPDATE schmovies_categories SET category = '$update_category' WHERE id = $update_id ";
				$rs		=	mysqlQuery($sql, $conn);
				$text 	=	"Category <b>'$update_category'</b> has been updated";

			}	else	{

				$text	=	"Cannot update category <b>'$update_category'</b> as it already exists";


			}

		}
		mysql_close($conn);

		echo	"<h2 style='color:red'>$text</h2>";

}




if (isset($_GET['delete_category']) && is_numeric($_GET['delete_category']))	{

	$delete_id		=	$_GET['delete_category'];
	$conn			=	mysqlConnect();

	/*
	 * 	Identify MySQL ID that corresponds to the 'None' category
	 */
	$sql	=	"	SELECT id FROM schmovies_categories WHERE category = 'None' ";
	$rs		=	mysqlQuery($sql, $conn);
	if (mysqlNumRows($rs, $conn) == 1)	{

		$res		=	mysqlGetRow($rs);
		$none_id	=	$res['id'];

	}	else	{

		$none_id	=	false;

	}


	/*
	 * 	Get the Category name, so we use it in the notice
	 */
	$sql	=	" 	SELECT category FROM schmovies_categories WHERE id = $delete_id ";
	$rs		=	mysqlQuery($sql, $conn);
	$res	=	mysqlgetRow($rs);
	$delete_category	=	$res['category'];


	/*
	 * 	Delete the category
	 */
	$sql	=	"	DELETE FROM schmovies_categories WHERE id = $delete_id ";
	$rs		=	mysqlQuery($sql, $conn);


	/*
	 * 	Update all SchMOVIES using this category to use the 'None' category
	 */
	$sql	=	" UPDATE schmovies SET category = $none_id WHERE category = $delete_id ";
	$rs		=	mysqlQuery($sql, $conn);


	/*
	 * 	Close the DB and display a message on screen...
	 */
	mysql_close($conn);
	$text	=	"Category <b>'$delete_category'</b> has been deleted. All SchMOVIES that were listed under this category are now listed under <b>'None'</b>";
	echo	"<h2 style='color:red'>$text</h2>";

}



//	Cache Categories...
$categories		=	get_schmovies_categories();
















/*
 * 	============================================================
 * 						Display Data
 * 	============================================================
 */



//	Cancel Edit and back to main SchMOVIES menu...
$output		=	array();
$output[]	=	"<h3><a href='index.php?schmovies=1'>Back to main SchMOVIES list</a></h3>";
$output[]	=	"<p>............</p>";
echo implode("\r\n", $output);		//	Flush Output Buffer



/*
 * 	Category Table
 */
$output		=	array();			//	New Output buffer

$output[]	=	"<table class='schmovies_category_edit_table'>";
$output[]	=	"<th>Category</th><th># SchMOVIES</th>";
foreach ($categories as $id => $category)	{

	if ($category == 'None') continue;

	$output[]	=	"<tr>";
	$output[]	=	"<td>$category</td>";
	$output[]	=	"<td>".get_num_schmovies_for_category($id)."</td>";
	$output[]	=	"<td><a href='index.php?schmovies=1&edit_schmovies_categories=1&category=$id'>Edit</a></td>";
	$output[]	=	"<td><a href='index.php?schmovies=1&edit_schmovies_categories=1&delete_category=$id' onClick=\"return confirmDeleteSchmovieCategory()\">Delete</a></td>";
	$output[]	=	"</tr>";

}

$output[]	=	'</table>';

echo implode("\r\n", $output);		//	Flush Output Buffer







/*
 *		Edit Form
 */
$output		=	array();			//	New Output Buffer
$category	=	'';
$button		=	"Save New Category";
$title		=	"Enter a new category...";
$cancel_edit	=	"";
if ($edit_category != 0)	{

	$category 	=	stripslashes($categories[$edit_category]);
	$button		=	"Save Updated Category";
	$title		=	"Modify the existing category below...";
	$cancel_edit	=	"<a href='index.php?schmovies=1&edit_schmovies_categories=1'><b>Stop Editing This Category</b></a>";

}

//	Create the form.
$output[]	=	"<div class='schmovies_category_edit_form'>";
$output[]	=	"<b>$title</b>";
$output[]	=	"<form method='POST' action='index.php?schmovies=1&edit_schmovies_categories=1&edit_category=$edit_category'>";
$output[]	=	"<input type='text' name='category' value='$category' class='schmovies_category_text_edit' />";
$output[]	=	"<input type='submit' name='category_submit' value='$button' />";
$output[]	=	"</form>";
$output[]	=	'</div>';

$output[]	=	$cancel_edit;

echo implode("\r\n", $output);		//	Flush Output buffer







