<h2>DIY Guide</h2>



<?php


/*
 * 	***********************************************************************
 *
 * 	GET Var and Flow Control....
 */
$mode		=	'list';

//	Edit_Article is set when editing or adding an article (will == 0 if adding)...
if (isset($_GET['edit_article']) && is_numeric($_GET['edit_article']))	{

	$article_id		=	$_GET['edit_article'];		//	The SQL id of the article
	$mode	=	'edit_article';

}

if (isset($_GET['edit_diy_categories']) && $_GET['edit_diy_categories'] == 1)	{

	$mode	=	'edit_categories';

}





//	Execute Flow Control
switch ($mode)	{

	default:
		require "diy_guide_show_list.php";
		break;

	case "edit_article":
		require "diy_guide_edit_article.php";
		break;

	case 'edit_categories':
		require "diy_edit_categories.php";
		break;

}








