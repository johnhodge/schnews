<?php

if (!$_SESSION[SES.'loggedin']) exit;

// TITLE
echo "<h2>SchMOVIES</h2>";
echo "<p>............</p>";


/*
 * 	==============================================
 * 					FLOW CONTROL
 * 	==============================================
 */

//	Test $_GET vars
$mode		=	'schmovie_list';
if (isset($_GET['edit_schmovie']) && is_numeric($_GET['edit_schmovie']))	{

	$edit_schmovie		=	$_GET['edit_schmovie'];
	$mode				=	'edit_schmovie';

}

if (isset($_GET['edit_schmovies_categories']) && $_GET['edit_schmovies_categories'] == 1)	{

	$mode				=	'edit_categories';

}


//	Test $MODE and load in correct script
switch ($mode)	{

	default:
		require "schmovies_list.php";
		break;

	case 'edit_schmovie':
		require "schmovies_edit_schmovie.php";
		break;

	case 'edit_categories':
		require "schmovies_edit_categories.php";
		break;

}

?>