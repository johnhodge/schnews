<?php

/*
 * =====================================================================================
 *
 * 					MAKE EDITION
 *
 *  ====================================================================================
 */
function makeFullIssue($issueId)	{


/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs			=	mysqlQuery('SELECT * FROM issues WHERE id = '.$issueId, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$issue	=	mysqlGetRow($rs);



/* GET ALL ARTICLES FOR THIS ISSUE AND PUT THEM IN A SET OF NUMERED ARRAYS */
$rs			=	mysqlQuery("SELECT * FROM articles WHERE issue = $issueId ORDER BY story_number ASC ", $conn);
if (mysqlNumRows($rs, $conn) < 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing ::less than one article found for issue $issueId ;; exiting silently <br>";
	exit;
}

while ($res = mysqlGetRow($rs))	{
		$storyNumbers[]		=	$res['story_number'];
		$articleIds[$res['story_number']]			=	$res['id'];
		$articleHeadlines[$res['story_number']]		=	stripslashes($res['headline']);
		$articleStories[$res['story_number']]		=	$res['story'];
		$articleKeywords[$res['story_number']]		=	stripslashes($res['keywords']);

}




$description 	=	array();

$rs_d	=	mysqlQuery("SELECT rss_text FROM articles WHERE issue = $issueId ORDER BY story_number ASC LIMIT 0, 3", $conn);
if (mysqlNumRows($rs, $conn) < 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: on line ".__LINE__." <br>";
	exit;
}

while ($res_d = mysqlGetRow($rs_d))
{
	$description[]	=	trim(strip_tags(stripslashes(str_replace('.','',$res_d['rss_text']))));
}
$description 	=	implode(", ", $description).", and more...";





/* 	====================================================================================

					PREP DATA

========================================================================================				*/
$templateFile	=	'issueTemplate.tpl';				//	LOCATION OF THE ISSUE TEMPLATE

//	HEADER TITLE
$title		=	"SchNEWS {$issue['issue']} - ".makeHumanDate($issue['date'], 'words')." - {$issue['page_title_text']}";		//	PAGE TITLE
$titleTag	=	"<title>$title</title>";			// PAGE TITLE AS HTML TAG

//	HEADER KEYWORDS
$articleIdList		=	implode(',', $articleIds);
$rs_k		=	mysqlQuery(" SELECT keyword FROM keywords WHERE article_id IN ($articleIdList) ", $conn);
$keywords	=	'SchNEWS, direct action, Brighton';		// INITIATE KEYWORDS VAR
while ($res	=	mysqlGetRow($rs_k))	{
		$keywords	.=	", ".stripslashes($res['keyword']);		// CREATE CSV VAR OF ALL KEYWORDS.
}
$keywordsTag	=	"<meta name=\"keywords\" content=\"$keywords\" />";	//	CREATE AN HTML TAG OF THE KEYWORDS.

//	HOME LINK
$homeLink		=	makeHumanDate($issue['date'], 'words+day')." | Issue {$issue['issue']}";
$homeLink		=	"<div style='width: 40%; padding: 10px 0px 10px 0px; border-bottom: 1px solid #bbbbbb; border-top: 1px solid #bbbbbb; margin: 10px 0px 10px 0px'>".$homeLink."</div>";


//	WAKE UP TEXT
$wakeUpText		=	strtoupper(stripslashes($issue['wake_up_text']));

//	DISCLAIMER
$disclaimer		=	stripslashes($issue['disclaimer']);



// CONSTRUCT THE BACK ISSUES TEXT ON THE LEFT BAR.
$backIssues		=	makeBackIssues($issue['id'], $conn);


$issueHitCounter	=	"incrementIssueHitCounter($issueId);";


//	CONSTRUCT THE STORY LINKS
$storyLinkText		=	"\r\n<b>Story Links : </b>";
foreach ($articleHeadlines	as $key => $sl_headline)	{

		$storyLinks[]	=	" <a href=\"../archive/news{$issue['issue']}{$key}.php\">".$sl_headline."</a> ";

}

$storyLinkText		.=	implode(' | ', $storyLinks)."\r\n";

$graphicSmall	=	processImageURL($issue['main_graphic_small']);
$graphicLarge	=	processImageURL($issue['main_graphic_large']);
$graphicAlt		=	$issue['main_graphic_alt'];


//	CONSTRUCT THE MAIN ISSUE
if ($issue['main_graphic_small'] != '')	{

	$issueBody	=	"
									<div align=\"center\" class='issue_main_image'>
										<a href=\"".$graphicLarge."\" target=\"_blank\">
											<img src=\"".$graphicSmall."\" alt=\"$graphicAlt\" border=\"0\" >
											<br />click for bigger image
										</a>
									</div>
								";

}	else {

	$issueBody	=	'';

}

$issueBody		.=	"\r\n<p><b>".$homeLink."</b></p>\r\n<p><b><i>".$wakeUpText."</i></b></p>\r\n";
$issueBody		.=	"\r\n<h3><i>SchNEWS</i></h3>\r\n";

if (!$issue['no_pdf'])	{

	$issueBody		.=	"\r\n<p><font size=\"1\"><a href=\"../archive/pdf/news".$issue['issue'].".pdf\" target=\"_blank\">PDF Version - Download, Print, Copy and Distribute!</a></font></p>\r\n";

}

$issueBody		.=	"<p><font size=\"1\"><a href='../archive/news".$issue['issue'].".php?print=1' target='_BLANK'>Print Friendly Version</a></font></p>";

$issueBody		.=	"\r\n<p>$storyLinkText</p>\r\n\r\n";
//$ib_divider		=	"\r\n<img src=\"../images_main/divider.gif\" width=\"48%\" height=\"40\" /><a href=\"#top\"><img src=\"../images_main/divider_arrow.gif\" width=\"24\" height=\"40\" border=\"0\" /></a><img src=\"../images_main/divider.gif\" width=\"48%\" height=\"40\" />\r\n";
$ib_divider		=	"\r\n<div style='border-bottom: 1px solid black'>&nbsp;</div>\r\n";
//$ib_divider			=	"\r\n";

$sql_ar		=	" SELECT id FROM articles WHERE issue = {$issue['id']} ORDER BY story_number ";
$rs_ar		=	mysqlQuery($sql_ar, $conn);

$count = 1;
while ($res_ar		=	mysqlGetRow($rs_ar))	{

		$issueBody	.=	makeArticle($res_ar['id'], $conn);
		$issueBody  .=  "<?php echo showComments(".$res_ar['id'].", $count); ?>";
		$issueBody	.=	"\r\n$ib_divider\r\n\r\n";
		$count++;

}

$issueBody		.=		"\r\n<p><b>Disclaimer</b></p><p>$disclaimer</p>\r\n$ib_divider";








/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$template		=	file_get_contents('../storyAdmin/'.$templateFile);

$template		=	str_replace('****BANNER_GRAPHIC_LINK****', $issue['banner_graphic_link'], $template);
$template		=	str_replace('****BANNER_GRAPHIC****', processImageURL($issue['banner_graphic']), $template);
$template		=	str_replace('****ISSUE_HIT_COUNTER****', $issueHitCounter, $template);
$template		=	str_replace('****BANNER_GRAPHIC_ALT****', $issue['banner_graphic_alt'], $template);
$template		=	str_replace('<title></title>', $titleTag, $template);
$template		=	str_replace('<meta name="keywords" content="" />', $keywordsTag, $template);
$template		=	str_replace('<div id="backIssues">', '<div id="backIssues">'.$backIssues, $template);
$template		=	str_replace('<div id="issue">', '<div id="issue">'.$issueBody, $template);
$template		=	str_replace("****PRINT_ISSUE_ID****", $issueId, $template);
$template		=	str_replace("****DESCRIPTION****", $description, $template);








/* 	====================================================================================

					OUTPUT CONTENTS AN EXIT

========================================================================================				*/
return 	$template;



}



































/*
 * =====================================================================================
 *
 * 										MAKE ARTICLE
 *
 *  ====================================================================================
 */

function makeFullArticle($articleId)	{



/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs			=	mysqlQuery('SELECT * FROM articles WHERE id = '.$articleId, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>article.php :: article data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$article	=	mysqlGetRow($rs);


$sql		=	" SELECT * FROM issues WHERE id = ".$article['issue'];
$rs			=	mysqlQuery($sql, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>article.php :: issue data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$issue	=	mysqlGetRow($rs);


/* GET ALL ARTICLES FOR THIS ISSUE AND PUT THEM IN A SET OF NUMERED ARRAYS */
$rs			=	mysqlQuery("SELECT id, headline, story_number FROM articles WHERE issue = {$article['issue']} ORDER BY story_number ASC ", $conn);
if (mysqlNumRows($rs, $conn) < 1)	{
	if (DEBUG)	echo "<br>article.php :: data processing ::less than one article found for issue $issueId ;; exiting silently <br>";
	exit;
}

while ($res = mysqlGetRow($rs))	{
		$storyNumbers[]		=	$res['story_number'];
		$articleIds[$res['story_number']]			=	$res['id'];
		$articleHeadlines[$res['story_number']]			=	stripslashes($res['headline']);

}


$rs_k		=	mysqlQuery(" SELECT keyword FROM keywords WHERE article_id = $articleId ", $conn);










/* 	====================================================================================

					PREP DATA

========================================================================================				*/
$templateFile	=	'articleTemplate.tpl';				//	LOCATION OF THE ARTICLE TEMPLATE




//	HEADER TITLE
$title		=	"SchNEWS {$issue['issue']} - ".makeHumanDate($issue['date'], 'words')." - {$article['headline']}";		//	PAGE TITLE
$titleTag	=	"<title>$title</title>";			// PAGE TITLE AS HTML TAG

//	HEADER KEYWORDS
$keywords	=	'SchNEWS, direct action, Brighton';		// INITIATE KEYWORDS VAR
while ($res	=	mysqlGetRow($rs_k))	{
		$keywords	.=	", ".stripslashes($res['keyword']);		// CREATE CSV VAR OF ALL KEYWORDS.
}
$keywordsTag	=	"<meta name=\"keywords\" content=\"$keywords\" />";	//	CREATE AN HTML TAG OF THE KEYWORDS.

//	HOME LINK
$homeLink		=	makeHumanDate($issue['date'], 'words+day')." | Issue {$issue['issue']}";
$homeLink		=	"<a href=\"../index.php\">Home</a> | ".$homeLink;


$fullIssueLink	=	"<a href=\"news{$issue['issue']}.htm\">Back to the Full Issue</a>";



//	CONSTRUCT THE STORY LINKS
$storyLinkText		=	"\r\n<b>Issue {$issue['issue']} Articles: </b>";
foreach ($articleHeadlines	as $key => $sl_headline)	{

		$temp	=	"\r\n<p><a href=\"../archive/news{$issue['issue']}{$key}.php\">".ucwords(strtolower($sl_headline))."</a></p>\r\n";
		if ($articleIds[$key] == $articleId)	$temp	=	"\r\n<b>$temp</b>\r\n";
		$storyLinkText	.=	$temp;

}




//	CONSTRUCT THE MAIN ISSUE
$ib_divider		=	"\r\n<img src=\"../images_main/divider.gif\" width=\"48%\" height=\"40\" /><a href=\"#top\"><img src=\"../images_main/divider_arrow.gif\" width=\"24\" height=\"40\" border=\"0\" /></a><img src=\"../images_main/divider.gif\" width=\"48%\" height=\"40\" />\r\n";

if ($issue['main_graphic_story'] == $articleId && $issue['main_graphic_small'] != '')	{

		$issueBody		=	"	<table  border=\"0\" align=\"right\" bordercolor=\"#999999\" bgcolor=\"#666666\" style=\"margin-left: 15px; border: 2px solid #000000\">
									<tr>
										<td>
											<div align=\"center\">
												<a href=\"".processImageURL($issue['main_graphic_large'])."\" target=\"_blank\">
													<img src=\"".processImageURL($issue['main_graphic_small'])."\" alt=\"".$issue['main_graphic_alt']."\" border=\"0\" >
													<br />click for bigger image
												</a>
											</div>
										</td>
									</tr>
								</table>";

} else {

		$issueBody		=	'';

}

$issueBody		.=	"\r\n<p><b>".$homeLink."</b></p>\r\n";
$issueBody		.=	"\r\n<p>$fullIssueLink</p>\r\n";

$headline		=	"<h3>".strtoupper(stripslashes($article['headline']))."</h3>";

$story			=	makeArticle($articleId, $conn);

$issueBody		.=	$story;

$issueBody		.=		"\r\n\r\n<br /><br />";








/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$template		=	file_get_contents($templateFile);

$template		=	str_replace('****BANNER_GRAPHIC_LINK****', $issue['banner_graphic_link'], $template);
$template		=	str_replace('****BANNER_GRAPHIC****', processImageURL($issue['banner_graphic']), $template);
$template		=	str_replace('****BANNER_GRAPHIC_ALT****', $issue['banner_graphic_alt'], $template);
$template		=	str_replace('<title></title>', $titleTag, $template);
$template		=	str_replace('<meta name="keywords" content="" />', $keywordsTag, $template);
$template		=	str_replace('<div id="leftLinks">', '<div id="leftLinks">'.$storyLinkText, $template);
$template		=	str_replace('<div id="article">', '<div id="article">'.$issueBody, $template);
$template		=	str_replace('****ARTICLEID****', $articleId, $template);
$template		=	str_replace('****ISSUEID****', $issue['id'], $template);







/* 	====================================================================================

					OUTPUT CONTENTS AN EXIT

========================================================================================				*/
return	$template;


}


























/*
 * =====================================================================================
 *
 * 				MAKE RSS FEED
 *
 *  ====================================================================================
 */
function makeFullRSSFeed($issueId)	{




/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs			=	mysqlQuery('SELECT * FROM issues WHERE id = '.$issueId, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$issue	=	mysqlGetRow($rs);















/* 	====================================================================================

					PREP DATA

========================================================================================				*/
$templateFile	=	'rssFeed.tpl';				//	LOCATION OF THE TXT Email TEMPLATE



//	Title Issue Link
$titleIssueLink		=	"SchNEWS, Issue {$issue['issue']}, ".makeHumanDate($issue['date'], 'words+day');


//	WAKE UP TEXT
$wakeUpText		=	strtoupper($issue['wake_up_text']);

//	DISCLAIMER
$disclaimer		=	$issue['disclaimer'];




//	CONSTRUCT THE MAIN ISSUE
$sql_ar		=	" SELECT id, headline, rss_text, story_number FROM articles WHERE issue = {$issue['id']} ORDER BY story_number ";
$rs_ar		=	mysqlQuery($sql_ar, $conn);


/*
 * 	============================================================
 * 							ISSUE STORIES
 * 	============================================================
 */
$issueBody	=	'';
while ($res_ar		=	mysqlGetRow($rs_ar))	{

		$issueBody	.=	"\r\n<!-- Item Details -->\r\n";
		$issueBody	.=	"<item>\r\n";
		$issueBody	.=	"<title>".rssChars(cleanOutputTxt(htmlspecialchars(strip_tags(stripslashes($res_ar['headline'])))))."</title>\r\n";
		$issueBody	.=	"<link>http://www.schnews.org.uk/archive/news{$issue['issue']}".$res_ar['story_number'].".php</link>\r\n";
		$issueBody	.=	"<description>".rssChars(cleanOutputTxt(htmlspecialchars(strip_tags(stripslashes($res_ar['rss_text'])))))."</description>\r\n";
		$issueBody	.=	"</item>\r\n";
		$issueBody	.=	"<!-- End Item Details -->\r\n\r\n\r\n";

}




/*
 * 	============================================================
 * 							FEATURE ARTICLES
 * 	============================================================
 */
$old_time_stamp		=		date("U") - (10 * 3600 * 24);
$old_sql_date		=		date("Y-m-d", $old_time_stamp);

$sql		=	"	SELECT id, title, blurb FROM features WHERE date > '$old_sql_date' ";
$rs			=	mysqlQuery($sql, $conn);

if (mysqlNumRows($rs, $conn) > 0)	{


		while ($feature		=	mysqlGetRow($rs))	{

					$issueBody	.=	"\r\n<!-- Item Details -->\r\n";
					$issueBody	.=	"<item>\r\n";
					$issueBody	.=	"<title>New Feature Article: ".rssChars(cleanOutputTxt(htmlspecialchars(strip_tags(stripslashes($feature['title'])))))."</title>\r\n";
					$issueBody	.=	"<link>http://www.schnews.org.uk/features/show_feature.php?feature=".$feature['id']."</link>\r\n";
					$issueBody	.=	"<description>".rssChars(cleanOutputTxt(htmlspecialchars(strip_tags(stripslashes($feature['blurb'])))))."</description>\r\n";
					$issueBody	.=	"</item>\r\n";
					$issueBody	.=	"<!-- End Item Details -->\r\n\r\n\r\n";

		}


}





/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$template		=	file_get_contents($templateFile);

$output			=	str_replace('<!-- Item Details -->', $issueBody, $template);






/* 	====================================================================================

					OUTPUT CONTENTS AND EXIT

========================================================================================				*/
return 	$output;



}


























/*
 * =====================================================================================
 *
 * 										MAKE TXT EMAIL
 *
 *  ====================================================================================
 */
function makeFullTXTEmail($issueId)	{


/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs			=	mysqlQuery('SELECT * FROM issues WHERE id = '.$issueId, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$issue	=	mysqlGetRow($rs);


/* GET ALL ARTICLES FOR THIS ISSUE AND PUT THEM IN A SET OF NUMERED ARRAYS */
$rs			=	mysqlQuery("SELECT * FROM articles WHERE issue = $issueId ORDER BY story_number ASC ", $conn);
if (mysqlNumRows($rs, $conn) < 1)	{
	if (DEBUG)	echo "<br>edition.php :: data processing ::less than one article found for issue $issueId ;; exiting silently <br>";
	exit;
}

while ($res = mysqlGetRow($rs))	{
		$storyNumbers[]								=	$res['story_number'];
		$articleIds[$res['story_number']]			=	$res['id'];
		$articleHeadlines[$res['story_number']]		=	stripslashes($res['headline']);
		$articleStories[$res['story_number']]		=	$res['story'];
		$articleKeywords[$res['story_number']]		=	stripslashes($res['keywords']);

}







/* 	====================================================================================

					PREP DATA

========================================================================================				*/
$templateFile	=	'txtEmail.tpl';				//	LOCATION OF THE TXT Email TEMPLATE

$divider		=	"======================================================================";


$titleNoReply	=	"PLEASE DON'T REPLY TO THIS EMAIL

FOR CORRESPONDENCE USE mail@schnews.org.uk THANK YOU.

For this week's SchNEWS in full technicolour:
www.schnews.org.uk/archive/news{$issue['issue']}.htm

";


//	Title Issue Link
$titleIssueLink		=	"SchNEWS, Issue {$issue['issue']}, ".makeHumanDate($issue['date'], 'words+day');


//	WAKE UP TEXT
$wakeUpText		=	strtoupper(stripslashes($issue['wake_up_text']));

//	DISCLAIMER
$disclaimer		=	stripslashes($issue['disclaimer']);




//	CONSTRUCT THE MAIN ISSUE
$sql_ar		=	" SELECT id, headline, story FROM articles WHERE issue = {$issue['id']} ORDER BY story_number ";
$rs_ar		=	mysqlQuery($sql_ar, $conn);

$issueBody	=	'';
while ($res_ar		=	mysqlGetRow($rs_ar))	{

		// Step 1 : Remove all line breaks, carriage breaks and tabs
		$body	=	str_replace(array("\r", "\n", "\t&nbsp;", "\t &nbsp;", "\t"), '', $res_ar['story']);
		// Step 2 : Replace all line break type HTML tags with ASCI line breaks
		//$body	=	strip_tags(stripslashes(str_replace(array('<p>', '<br />', '<br>'), "\r\n", $body)));
		$body 	=	str_replace(array('<p>', '</p>'), '<br />', $body);
		//body 	=	str_replace()
		
		// Step 3 : Put it together...
		$issueBody		.=	"<br /><br />".strtoupper(stripslashes($res_ar['headline']))."<br /><br />".$body."<br /><br />".$divider;

}

$issueBody		.=		"<br /><br />Disclaimer<br /><br />$disclaimer";








/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$template		=	file_get_contents($templateFile);


$output			=	$titleNoReply;
$output			.=	$divider."<br /><br />";
$output			.=	$wakeUpText."<br /><br />".$titleIssueLink."<br /><br />$divider";
$output			.=	$issueBody."<br /><br />".$template;






/* 	====================================================================================

					OUTPUT CONTENTS AN EXIT

========================================================================================				*/
return 	"<font face=\"Courier New, Courier, mono\">".nl2br(wordwrap(strip_tags(str_replace(array('<br />', '<p>'), "\r\n", cleanOutputTxt($output)), '<br>'), 70))."</font>";


}




























function send_smtp_from_custom_sender($recipient, $sender, $subject, $body)
{


	//	==================================================================
	//
	//			CONFIG
	//
	//	==================================================================
	global $smtp_host;
	global $smtp_user;
	global $smtp_pass;
	$smtp_sender = $sender;




	// Clean Inputs
	$subject			=	stripslashes($subject);
	$body				=	cleanEmail(htmlspecialchars_decode(html_entity_decode(stripslashes($body))));

	if (SENDTYPE)	{

		return mail($recipient, $subject, $body, "From: ".SMTPSENDER);

	}

	// Instantiate the PEAR::Mail class and pass it the SMTP auth details
	require_once ('Mail.php');
    	$mail = Mail::factory("smtp", array('host' => $smtp_host, 'auth' => TRUE, 'username' => $smtp_user, 'password' => $smtp_pass));



			// Create the mail headers and send the mail via our SMTP object
			$headers = array("To"=>$recipient, "From" => $smtp_sender, "Subject"=>$subject, "Content-type"=>'text/plain; charset=iso-8859-1');
			$output	=	$mail->send($recipient, $headers, $body);

			if (PEAR::isError($mail))
				{
					$return 		=	FALSE;
				}
			else
				{
					$logFile	=	fopen ('../functions/emailLog.txt', 'a+');
					$datetime	=	make_sql_datetime();
					$date		=	make_date_from_sql($datetime);
					$time		=	make_time_from_sql($datetime);
					$logOutput	=	"email sent, $date, $time, ".$_SERVER['REMOTE_ADDR'].', '.$_SERVER['REQUEST_URI'].", $subject";
					fwrite ($logFile, $logOutput);
					fclose($logFile);
					$return 		=	TRUE;
				}



	return $return;

}





















/*
 * =====================================================================================
 *
 * 					MAKE HOMEPAGE
 *
 *  ====================================================================================
 */
function makeHomepage($issueId)	{


/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs		=	mysqlQuery('SELECT * FROM issues WHERE id = '.$issueId, $conn);
if (mysqlNumRows($rs, $conn) != 1)	{
	if (DEBUG)	echo "<br>MakeIndexMain.php :: data processing :: not one row returned ;; exiting silently <br>";
	exit;
}
$issue			=	mysqlGetRow($rs);





/* =====================================================
		PREPARE DATA
======================================================== */


$recentStoriesLinks		=	array("<a href=\"http://www.schnews.org.uk/archive/news{$issue['issue']}.htm\">{$issue['recent_story_text']}</a>");

//	Headlines...
$headlines		=	get_headlines_for_homepage($issueId, $issue['issue'], $issue['main_story']);

//	CURRENT ISSUE
$dateline		=	"\r\n<div class='latest_issue_dateline'>\r\n<span class='latest_issue_dateline_title'>Latest Issue</span><br />".makeDateLink($issue['issue'], $issue['date'])."\r\n</div>\r\n";




/*
 * 	===============================================================
 *						MAKING CURRENT ISSUE
 *	===============================================================
 */
$currentIssue	=	"\r\n\r\n<!-- LATEST ISSUE -->\r\n<?php echo make_horiz_divider(90, 'ff6600', 2); ?>";



$currentIssue	.=	"\r\n<div class='currentIssue' id='currentIssue'>";


$currentIssue	.=	$dateline;


$currentIssue	.=	"\r\n<div class='latest_issue'>";

$currentIssue	.=	"\r\n<a href='archive/news".$issue['issue'].".php'>";
$currentIssue	.=	"\r\n<img src='images/".$issue['main_graphic_vsmall']."' align='right' class='latest_issue_image_preview' />";
$currentIssue	.=	"\r\n</a>";

$currentIssue	.=	"\r\n<a href='archive/news".$issue['issue'].".php'>";
$currentIssue	.=	"\r\n<div class='latest_headline'>".str_replace('<br />', '', stripslashes($issue['headline']))."</div>";
$currentIssue	.=	"\r\n</a>";

$currentIssue	.=	"\r\n<div class='latest_issue_summary'>";
$currentIssue	.=	stripslashes(str_replace('<br />', '', $issue['summary']))." <span class='latest_issue_read_more'><a href='archive/news".$issue['issue'].".php'>[ Read More ]</a></span>";
$currentIssue	.=	'</div>';

$currentIssue   .=	"\r\n</div>";

$currentIssue	.=	"\r\n<div class='headlines_title'>";
$currentIssue 	.=	"\r\nOther stories from this issue...";
$currentIssue	.=	"\r\n</div>";

$currentIssue	.=	$headlines;


$currentIssue	.=	"\r\n</div>";



$currentIssue	.=	"\r\r\r\n<div class='headlines_bottom_of_list'>";
$currentIssue	.=	"\r\n&nbsp;</div>";



$currentIssue	.=	"\r\n<?php echo make_horiz_divider(90, 'ff6600', 2); ?>\r\n\r\n";

$currentIssue	.=	"\r\r\r\n<div class='latest_issue_pdf_download'>";
$currentIssue	.=	"\r\n<a href='archive/pdf/news".$issue['issue'].".pdf'>";
$currentIssue	.=	"\r\nDownload this Issue as PDF";
$currentIssue	.=	"\r\n</a>";
$currentIssue	.=	"\r\n</div>";



//	PREVIOUS ISSUES
$sql		=	" SELECT issue, date, headline, summary, main_graphic_vsmall, recent_story_text, is_special, id FROM issues WHERE issue < {$issue['issue']} ORDER BY issue DESC LIMIT 0,".NUMPREVIOUSISSUES;
$rs_pr		=	mysqlQuery($sql, $conn);

$previousIssues = 	'';
while($prevIssue	=	mysqlGetRow($rs_pr))	{

	if (count($recentStoriesLinks) < NUMRECENTSTORIESLINKS)
		$recentStoriesLinks[]		=	"<a href=\"http://www.schnews.org.uk/archive/news{$prevIssue['issue']}.htm\">{$prevIssue['recent_story_text']}</a>";

		if ($prevIssue['main_graphic_vsmall'] != '')	{

				$image_link		=	"<a href=\"http://www.schnews.org.uk/archive/news{$prevIssue['issue']}.htm\"><img src=\"images/{$prevIssue['main_graphic_vsmall']}\" align='left' class='issue_image_preview' alt=\"Click here to see this weeks issue\"></a>";

		}	else $image_link	=	'';
		$previousIssues 	.=	"\r\n\r\n<p><b>".makeDateLink($prevIssue['issue'], $prevIssue['date'])."</b><br />\r\n$image_link\r\n".makeIssueSummary($prevIssue['headline'], $prevIssue['summary'], $prevIssue['issue'], $prevIssue['is_special'])."</p>\r\n\r\n";

}

$recentStoriesLinks	=	implode(' | ', $recentStoriesLinks);





/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$templateFile		=	'indexMain.tpl';
$template		=	file_get_contents($templateFile);

$template		=	str_replace('****RECENT_MAIN_STORIES****', $recentStoriesLinks, $template);
$template		=	str_replace('****CURRENT_ISSUE****', $currentIssue, $template);
$template		=	str_replace('***PREVIOUS_ISSUES****', $previousIssues, $template);
$template		=	str_replace('****BANNER_GRAPHIC_LINK****', $issue['banner_graphic_link'], $template);
$template		=	str_replace('****BANNER_GRAPHIC****', 'images_main/'.$issue['banner_graphic'], $template);
$template		=	str_replace('****BANNER_GRAPHIC_ALT****', $issue['banner_graphic_alt'], $template);


return $template;

}




function makeDateLink($issue, $date)	{

	if (!is_numeric($issue))	return false;
	$date	=	makeHumanDate($date, 'words+day');
	return "Issue $issue, $date";

}

function makeIssueSummary($headline, $summary, $issue, $isSpecial = 0, $isCurrentIssue = 0, $show_headlines = false, $headlines = '')	{

	if (!is_numeric($isSpecial))		$isSpecial = false;
	if (!is_numeric($isCurrentIssue))	$isCurrentIssue = false;
	if (!is_string($headline) || !is_string($summary) || !is_numeric($issue)) return false;
	if (!is_bool($show_headlines))	$show_headlines 	=	false;
	if (!is_string($headlines))		$headlines	=	'';
	if ($show_headlines && $headlines	==	'')	$show_headlines		=	false;

	$output		=	'';
	if ($isCurrentIssue)	{

		$output		.=	"\r\n";
		$output		.=	"<div align='left' style='text-align: left'>";
		$output		.=	"<b><a href=\"http://www.schnews.org.uk/archive/news$issue.htm\"><font size=\"4\">$headline<br /></font></a></b></div>\r\n";

	}	else	{

		$output		.=	"\r\n<div style='text-align: left'><b><a href=\"http://www.schnews.org.uk/archive/news$issue.htm\"><font size=\"4\">$headline<br /></font></a></b></div>\r\n";

	}
	$output		.=	"\r\n".processSummaryText($summary)."<br />\r\n";

	if ($show_headlines)	{

		$output		.=	"<div id='headline_container' style='height: 25px'>\r\n<div class='show_hide_headlines_div'>\r\n<a class='show_hide_headlines' id='show_hide_headlines'>Show headlines...</a>\r\n$headlines\r\n</div>\r\n</div>\r\n";

	}

	return $output;

}















/*
 * =====================================================================================
 *
 * 					MAKE ARCHIVE INDEX
 *
 *  ====================================================================================
 */
function makeArchiveIndex($issueId)	{

if (!is_numeric($issueId)) return false;


/* GET THE DATA FOR THIS ISSUE FROM THE DB */
$conn		=	mysqlConnect();
$rs			=	mysqlQuery(" SELECT issue, date FROM issues WHERE id = $issueId ", $conn);
$res		=	mysqlGetRow($rs);
$issue		=	$res['issue'];
$date		=	$res['date'];
$issue_line	=	"SchNEWS $issue, ".makeHumanDate($date, 'words');


$current_issue_summary	=	make_current_issue_summary($issueId, $issue);


$rs		=	mysqlQuery('SELECT issue, headline, summary, date, main_graphic_vsmall, no_pdf  FROM issues WHERE issue <= '.$issue.' ORDER BY issue DESC LIMIT 0, 60 ', $conn);


$backIssues	=	'';
while ($issue = mysqlGetRow($rs))	{

	$link		=	"SchNEWS {$issue['issue']}, ".makeHumanDate($issue['date'], 'words');
	$link		=	"<b><a href=\"news{$issue['issue']}.htm\">$link</a></b> ";
	if (!$issue['no_pdf'])	{

		$link		.=	" <font size=\"1\">Click <a href=\"pdf/news{$issue['issue']}.pdf\">HERE</a> for PDF version</font>";

	}

	$mainBlock	=	"<b>{$issue['headline']}</b> - ".stripTagsForArchiveIndex($issue['summary']);

	if ($issue['main_graphic_vsmall'] != '')	{

			$image		=	$issue['main_graphic_vsmall'];
			$image		=	"<a href=\"news{$issue['issue']}.htm\"><img src='".processImageURL($image)."' class='backIssue_image' align='right' /></a>";

	}	else $image	=	'';

	$thisBackIssue	=	"\r\n\r\n<div class=\"backIssue\">\r\n<table width='100%'><tr><td>\r\n$image<p align='left'>\r\n$link\r\n<br />\r\n$mainBlock\r\n</p>\r\n</td></tr></table>\r\n</div>\r\n";

	$backIssues	.=	$thisBackIssue;

}

$rs			=	mysqlQuery(" SELECT * FROM issues WHERE id = $issueId ", $conn);
$issue		=	mysqlGetRow($rs);

/* 	====================================================================================

					READ FILE AND DO REPLACEMENTS

========================================================================================				*/
$templateFile		=	'archiveIndex.tpl';
$template		=	file_get_contents($templateFile);

$template		=	str_replace('****BACK_ISSUES****', $backIssues, $template);
$template		=	str_replace('****LATEST_ISSUE_SUMMARY****', $current_issue_summary, $template);
$template		=	str_replace('****LATEST_ISSUE_NUM_AND_DATE****', $issue_line, $template);
$template		=	str_replace('****BANNER_GRAPHIC_LINK****', $issue['banner_graphic_link'], $template);
$template		=	str_replace('****BANNER_GRAPHIC****', processImageURL($issue['banner_graphic']), $template);
$template		=	str_replace('****BANNER_GRAPHIC_ALT****', $issue['banner_graphic_alt'], $template);

return $template;

}



function stripTagsForArchiveIndex($input)	{

		if (!is_string($input))		return $input;
		$input		=	strip_tags($input, '<sh>');

		$input		=	str_replace('<sh>', '<b>', $input);
		$input		=	str_replace('</sh>', '</b>', $input);

		return $input;
}






function rssChars($text)	{

	if (!is_string($text)) return $text;

	$text		=	str_replace('�', '&pound;', $text);

	return $text;

}







function make_current_issue_summary($issueId, $issueNum)	{

		//	Test Inputs
		if (!is_numeric($issueId))	return;

		//	Get stories for this issue...
		$conn		=	mysqlConnect();
		$sql		=	"	SELECT headline, rss_text, story_number
							FROM articles
							WHERE issue = $issueId ";
		$rs			=	mysqlQuery($sql, $conn);


		$output		=	array();
		while ($res 	=	mysqlGetRow($rs)) {

				$this_output		=	array();
				$this_output[]		=	"<div class='story_preview_element'>";
				$this_output[]		=	"<div style='font-weight: bold; font-size: 13px'><a href='news$issueNum".$res['story_number'].".php'>".stripslashes($res['headline'])."</a></div>";
				$this_output[]		=	"<div><i>".stripslashes($res['rss_text'])."</i></div>";
				$this_output[]		=	"</div>";

				$output[]			=	implode("\r\n", $this_output);
		}

		//	Close MySQL connection
		mysql_close($conn);

		return implode("\r\n", $output);

}





function get_headlines_for_homepage($issue_id, $issue_num, $main_story)	{

	//	Test inputs...
	if (!is_numeric($issue_id))	return;
	if (!is_numeric($main_story) || $main_story == '' || $main_story == 0)	{

		$main_story		=	get_first_story_id($issue_id);

	}

	$conn		=	mysqlConnect();
	$sql		=	"	SELECT id, story_number, headline, rss_text
						FROM articles
						WHERE issue = $issue_id AND id <> $main_story
						ORDER BY story_number ASC	";
	$rs			=	mysqlQuery($sql, $conn);

	//	Begin output buffer
	$output		=	array();
	$output[]	=	"<div class='homepage_headlines' id='homepage_headlines'>";
	// Cycle through each article...
	while ($article		=	mysqlGetRow($rs))	{

			$output[]		=	"<div class='homepage_headline'>";
			$output[]		=	"<a href='archive/news".$issue_num.$article['story_number'].".php' class='homepage_headline_link'><b>".$article['headline']."</b></a>";
			$output[]		=	' - '.$article['rss_text'];
			$output[]		=	"</div>";

	}
	$output[]	=	"</div>";

	//	Close the MySQL connection
	mysql_close($conn);

	return implode("\r\n", $output);

}




function get_first_story_id($issue_id)	{

	//	Test inputs
	if (!is_numeric($issue_id))	return false;

	$conn	=	mysqlConnect();
	$sql	=	"	SELECT	id
					FROM articles
					WHERE 		issue = $issue_id
							AND	story_number	=	1		";
	$rs		=	mysqlQuery($sql, $conn);

	if (mysqlNumRows($rs, $conn)	!= 1)	return false;

	$article 	=	mysqlGetRow($rs);

	return $article['id'];

}





function is_main_story($article_id)	{

	//	Test inputs
	if (!is_numeric($article_id) || $article_id == 0)	return false;

	$conn	=	mysqlConnect();

	//	Get the issue id...
	$sql	=	"	SELECT issue
					FROM articles
					WHERE id = $article_id";
	$rs		=	mysqlQuery($sql, $conn);

	//	There should be only one result...
	if (mysqlNumRows($rs, $conn) != 1)	return false;

	$res		=	mysqlGetRow($rs);
	$issue_id	=	$res['issue'];


	//	Get the main stroy for the issue...
	$sql	=	"	SELECT main_story
					FROM issues
					WHERE id = $issue_id";
	$rs		=	mysqlQuery($sql, $conn);

	//	There should be only one result...
	if (mysqlNumRows($rs, $conn) != 1)	return false;

	$res	=	mysqlGetRow($rs);

	//	DEBUGGING
	$output	=	"Article ID = $article_id\r\n\r\nIssue ID = $issue_id\r\n\r\nMain Story = {$res['main_story']}";
	file_put_contents('debugging.txt', $output);

	//	Do they match?
	if ($res['main_story'] == $article_id)	return true;

	if ($res['main_story'] == 0 && get_first_story_id($issue_id) == $article_id)	return true;

	return false;

}


