<?php


	$tip_addNewEdition		=	"Adds a new blank issue.<br /><br />It will assume the next issue number, and a date 7 days after the previous issue (these can be ovewritten) ";



	$tip_editIssue			=	"Allows you to edit the basic details about the issue, such as the headline title, issue date, banner graphic, etc.";

	$tip_previewIssue		=	"Generates a preview of the issue, showing exactly how it would look when placed on the site.<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_makeTXTEmail		=	"Generates the plain text email of the issue.<br /><br />This is primarily useful for preview purposes, and the software will send the plain text to the mailing list when the issue is generated on the site<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_previewRSSFeed		=	"Generates a preview of the RSS Feed, showing exactly how it would look when place on the site.<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_previewHomePage	=	"Generates a preview of the Home Page, showing exactly how it would look when place on the site.<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_previewArchiveIndex =	"Generates a preview of the Archive sections home page, showing exactly how it would look when place on the site.<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_remakeIssue		=	"Causes the issue to be made live on the server. Only the issue will be changed/made, the homepage, archive index, etc. will not be affected. If the issue file already exists it will be overwritten.";

	$tip_makeIndyLinks		=	"Generate the HTML tags for the IndyMedia links, so they can be pasted to IndyMedia";




	$tip_previewStory		 =	"Generates a preview of the individual article, showing exactly how it would look when place on the site.<br /><br /><b>Note:</b> This does not permanantly affect the site at all, and will not make anything visible ot the public. It is quite safe to use this at any time.";

	$tip_editStory			 =	"Edit the basic details of the article, such as the headline, main body, RSS text and keywords";


	$tip_arrow				=	"Re-arrange the order of the stories within this issue by moving them up or down the list.";




	$tip_addNewArticle		=	"Adds a new empty article to the end of this story";

	$tip_createFullEdition	=	"Begins the process of generating the files for this issue on the server and sending the plain text email to the mailing list.<br /><br />This will affect the public apperance on the site, so please make sure that you have previewed all the pages using the links at the top of the page.<br /><br /><b>Note:</b> For the sake of safety you can only create files for the most recent issue.";





	$tip_keywords			=	"This will show a list of all the keywords in use for all articles. You can list them either in order of frequency of usage or in alphabetical order.<br /><br />This is usal to ensure that the keywords you enter for a story match up to existing keywords.";

	$tip_comments			=	"Lists the comments users have entered on various stories.<br /><br />It is important to check this regularly to ensure that any comments that are spam are removed from the live listings, and that any comments that the softwar ehas flagged incorrectly as spam are addressed";

	$tip_settings			=	"Various settings regarding the software in general. <br /><br />Each item within will be listed in detail. Don't change anything unless you are sure you know what you are doing";




	$tip_keywords_keywordSort	=	"Sort the list in alphabetical order";

	$tip_keywords_numericSort	=	"Sort the list in order of most popular keyword";

	$tip_keywords_merge			=	"Merge this keyword into another.<br /><br />This is useful if there are two very similar keywords used - they can be merged into one keyword and so increase the ammount of keyword convergence between stories.";



	$tip_comments_viewSpam		=	"Show only message that the software thinks is spam";

	$tip_comments_viewNotSpam	=	"Show only messages that the software thinks are real (not spam) messages";

	$tip_comments_viewAll		=	"Show all messages";

	$tip_comments_perArticle	=	"Causes the messages to be shown grouped together per article, so you can more easily see an entire discussion";

	$tip_comment_dateOrder		=	"Show the messages in the order they were posted, so you can more easily see the new message that may need dealing with.";

	$tip_comments_deleteComment	=	"Completely remove the comment from the database.<br /><br /><b>Note : </b>Cannot be undone";

	$tip_comments_notSpam		=	"Mark this comment as not being a spam message.<br /><br />This will cause it to be visible to the public on the story that it was posted on.";

	$tip_comments_IsSpam		=	"Mark this comment as being a spam comment.<br /><br />This will cause it to stop being visible to the public.";

?>