<?php
/*
 * 		PROCESS SCHMOVIE EDIT SUBMISSION
 */



/*
 * 	====================================================================
 *
 *		 				PREPARE AND TEST DATA
 *
 *	====================================================================
 */


//	Prepare Data
$post_data			=	$_POST;			//	Local copy of POST superglobal
$expected_data		=	array(			//	Associative array of which fields are expected which datatype they are...

		'name' =>	'text',
		'headline'	=>	'text',
		'body_text' =>	'text',
		'blurb'	=>	'text',
		'category' =>	'int',
		'date'	=> 	'date',
		'image_small'	=>	'text',
		'image_large'	=>	'text',
		'image_vsmall'	=>	'text',
		'live' 	=> 'int'

);

/*
 * 	Any errors noticed get entered here. If this has any entires then we will call
 * 	back the edit script and not do any SQL here.
 */
$errors		=	array();








/*
 *	Cycle through each expected datatype and validate and sanitise it
 */
$data		=	array();
foreach ($expected_data	as	$datum => $type)	{

		//	Test that this value exists in the POST data...
		if (!isset($post_data[$datum]) && $datum != 'date')	{

			//	Raise an error...
			$errors[$datum]		=	ucwords($datum).' is not set';
			continue;

		}

		//	Make a local copy of the POST data value...
		if ($type != 'date')	{
			$value 		=	$post_data[$datum];
		}

		//	Test and validate depending on the datatype...
		switch ($type)	{


			case 'text':

				//	Is the var a string?
				if (!is_string($value))	{

					//	Raise an error...
					$errors[$datum]		=	ucwords($datum).' is empty';
					continue;

				}

				//	Is the string empty?
				if (trim($value) == '')	{

					//	Raise an error...
					$errors[$datum]		=	ucwords($datum).' is empty';
					continue;

				}

				$data[$datum]	=	cleaninput($value);
				break;


			case 'int':

				//	Is the var a number?
				if (!is_numeric($value))	{

					//	Raise an error...
					$errors[$datum]		=	ucwords($datum).' is not a number';
					continue;

				}

				$data[$datum]	=	cleaninput($value);
				break;


			case 'date':

				//	Test the date elements
				if (	!isset($post_data[$datum.'_day'])	||
						!isset($post_data[$datum.'_month'])	||
						!isset($post_data[$datum.'_year'])	)	{

							$errors[$datum]		=	'1 - There is a problem with '.ucwords($datum);
							continue;

						}

				//	Test the date elements
				if (	$post_data[$datum.'_day'] == ''		||
						$post_data[$datum.'_month'] == ''	||
						$post_data[$datum.'_year'] == ''	)	{

							$errors[$datum]		=	'2 - There is a problem with '.ucwords($datum);
							continue;

						}

				//	Concatenate date
				$date	=	$post_data[$datum.'_year'].'-'.$post_data[$datum.'_month'].'-'.$post_data[$datum.'_day'];

				// Final validity test
				if (strlen($date) != 10)	{

					$errors[$datum]		=	'3 - There is a problem with '.ucwords($datum);
					continue;

				}

				$data[$datum]	=	$date;
				break;

		}

}








/*
 * 	====================================================================
 *
 *		 			TEST FOR AND HANDLE ERRORS
 *
 *	====================================================================
 */
if (count($errors) > 0)	{

	require "schmovies_edit_schmovie.php";
	$schmovie_updated		=	true;

}







/*
 * 	====================================================================
 *
 *				 			DO THE SQL STUFF
 *
 *	====================================================================
 */

//	Do the SQl Update
else	{

	$conn		=	mysqlConnect();

	//	Are we adding a new record?
	if ($schmovie_id == 0)	{

		//	Test for duplications...
		$sql		=	" 	SELECT id FROM schmovies
							WHERE	name = '{$data['name']}'
						";
		$rs			=	mysqlQuery($sql, $conn);
		if (mysqlNumRows($rs, $conn) != 0)	{

			echo "<h3 style='color:red'>SchMOVIE Item '{$data['name']}' Already Exists in the Database</h3>";

		}

		//	If there are no duplications carry on...
		else	{

			$sql		=	"	INSERT INTO schmovies ";

			$fields		=	array();
			$values		=	array();

			foreach ($data as $field => $value)	{

				$fields[]		=	$field;
				$values[]		=	"'".$value."'";

			}

			$sql			.=	" ( ".implode(", ", $fields).' ) VALUES ( '.implode(", ", $values)." ) ";
			$rs				=	mysqlQuery($sql, $conn);
			$schmovie_id	=	mysqlGetInsertId($conn);

			echo "<h3 style='color:red'>SchMOVIE Item '{$data['name']}' Added</h3>";

		}

	}

	//	Or are we updating an existing record?
	else	{

		$sql		=	"	UPDATE schmovies SET ";
		$fields		=	array();
		foreach ($data		as	$field => $value)	{

			$fields[]		=	$field." = '".$value."'";

		}

		$sql		.=		implode(", ", $fields)." WHERE id = $schmovie_id ";
		$rs			=		mysqlQuery($sql, $conn);

		echo "<h3 style='color:red'>SchMOVIE Item '{$data['name']}' Updated</h3>";

	}



}











?>