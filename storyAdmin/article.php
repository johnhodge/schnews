<?php
/* MAIN ARTICLE VISUALISER

AUTHOR: ANDREW WINTERBOTTOM support@tendrousbeastie.com www.tendrousbeastie.com
LICENSING: GPL V2.
COMPATIBILITY: PHP4+ MYSQL 4+ XHTML

*/


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';



/* DATA PROCESSING SECTION */

// GET ARTICLE ID
if (isset($_GET['article']) && is_numeric($_GET['article']))	{
	
	$articleId	=	$_GET['article'];
		
}

if (!isset($articleId))	{
	if (DEBUG)	echo "<br>article.php :: data processing :: articleId not set ;; exiting silently <br>";
	exit;
}


require '../storyAdmin/storyCreationFunctions.php';
$article	=	 makeFullArticle($articleId);

if (file_exists('temp.php'))	unlink ('temp.php');
file_put_contents('temp.php', $article);

$link = dirname($_SERVER['REQUEST_URI']).'/temp.php';

//echo $_SERVER['REQUEST_URI']."<br />";
//echo $link;

header("Location: $link");



exit;

?>












