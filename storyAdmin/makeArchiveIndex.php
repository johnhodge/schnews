<?php
/* ARCHIVE INDEX VISUALISER

AUTHOR: ANDREW WINTERBOTTOM support@tendrousbeastie.com www.tendrousbeastie.com
LICENSING: GPL V2.
COMPATIBILITY: PHP4+ MYSQL 4+ XHTML

*/


/* GET GLOBAL CONFIG FILE AND FUNCTIONS */
require_once '../storyAdmin/functions.php';

/* PAGE SPECIFIC CONSTANTS - THESE CAN BE USED TO OVERRIDE THE GLOBAL CONFIG ARS IF REQUIRED */
// define('DEBUG', 1);



/* DATA PROCESSING SECTION */

// GET ISSUE ID
if (isset($_GET['issue']) && is_numeric($_GET['issue']))	{
	
	$issueId	=	$_GET['issue'];
		
}

if (!isset($issueId))	{
	if (DEBUG)	echo "<br>makeArchiveIndex.php :: data processing :: issueId not set ;; exiting silently <br>";
	exit;
}


require '../storyAdmin/storyCreationFunctions.php';


$homepage 	= 	makeArchiveIndex($issueId);



if (file_exists('archiveIndex_temp.php'))	unlink ('archiveIndex_temp.php');
file_put_contents('archiveIndex_temp.php', $homepage);

$link = dirname($_SERVER['REQUEST_URI']).'/archiveIndex_temp.php';

header("Location: $link");

exit;

?>
