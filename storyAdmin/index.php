<?php
session_start();
require_once 'functions.php';
require_once 'config.php';
require 'js_tooltip/popupText.php';
if (file_exists('../index_temp.php')) unlink ('../index_temp.php');
if (file_exists('temp.php')) unlink ('temp.php');
if (file_exists('archiveIndex_temp.php')) unlink ('archiveIndex_temp.php');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>SchNEWS Story/Edition Editor</title>
	<link href="styles.css" rel="stylesheet" type="text/css" />
	<?php

		if (isset($_GET['schmovies']) && $_GET['schmovies'] == 1)	{

			?>

				<link href="../schmovies.css" rel="stylesheet" type="text/css" />

			<?php

		}

	?>
</head>

<body>
<script type="text/javascript" src="../javascript/jquery.js"></script>
<script type="text/javascript" src="delete.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript" src="js_tooltip/wz_tooltip.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>


	<?php


	/* IF NO LOGIN SESSION VAR IS SET THEN SET IF TO FALSE (I.E. IF WE HAVE NO IDEA WHAT THE USERS STATUS IS THEN THEY
	SHOULD NOT BE LOGGED IN */
	if (!isset($_SESSION[SES.'loggedin']) || isset($_POST['logout']))		$_SESSION[SES.'loggedin']	=	false;
	if (!isset($_SESSION[SES.'loggedin']) || isset($_POST['logout']))		$_SESSION[SES.'loggedin']	=	false;







	/* =======================================================================
					TEST LOGIN */


	/* POST>LOGIN IS DETECT WHEN THE USER HAS CLICKED THE LOGIN BUTTON */
	if (isset($_POST['login']))	{

		/* TEST THE USERNAME AND PASSWORD INPUTS. IF THEY ARE ACCEPTABLE, STICK THEM IN VARS.
		ELSE MAKE A ERROR ARRAY ELEMENT FOR THEM */
		if (isset($_POST['username']) && $_POST['username'] != '')		$username		=	$_POST['username'];
			else 		$errors['username']		=	"No Username Set";
		if (isset($_POST['password']) && $_POST['password'] != '')		$password		=	$_POST['password'];
			else 		$errors['password']		=	"No Password Set";

		/* $ERRORS IS ONLY SET IF THE USERNAME OR THE PASSWORD HAD PROBLEMS.
		IF THEY DIDN'T THEN WE CAN TEST THEM TO SET IF THEY ARE THE CORRECT PHRASES */
		if (!isset($errors))	{

				if ($user_id	=	test_login_attempt($username, $password))	{

						$_SESSION[SES.'loggedin']	=	true;
						$user_data	=	get_user($user_id);
						$_SESSION[SES.'userid']		=	$user_id;
						$_SESSION[SES.'userlevel']	=	$user_data['level'];

				}

				/* IF THEY DONT MATCH UP THEN WE MAKE ANOTHER ERRORS */
				if (!$_SESSION[SES.'loggedin'])	$errors['login']	=	"Either your Username or Password were incorrect";
			}

	}







	/* ==================================================================
				LOGIN FORM */

	/* IF SESSION>LOGGEDIN ISN'T TRUE THEN THE USER ISN'T LOGGEDIN, AND SHOULD BE SHOWN THE LOGIN FORM */
	if (!$_SESSION[SES.'loggedin'])	{

			?>

			<h1>SchNEWS Edition and Story Editor</h1>
			<br /><br />
			<p><b>Need to login first...</b></p>
			<br />

			<?php /* IF A LOGIN ATTEMPT WAS PREVIOUSLY DETECTED, BUT DIDN'T SUCCEED THEN WE MAY HAVE SOME ERRORS
			HERE THAT NEED OUTPUTING */ ?>
			<form action="index.php" method="post">
			<table>

				<?php	if (isset($errors['login']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['login']."</div></td></tr>"; ?>

				<?php	if (isset($errors['username']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['username']."</div></td></tr>"; ?>
				<tr><td>Username :: </td><td><input type="text" name="username" /></td></tr>

				<?php	if (isset($errors['password']))	echo "<tr><td colspan='2'><div class=\"errorMessage\">".$errors['password']."</div></td></tr>"; ?>
				<tr><td>Password :: </td><td><input type="password" name="password" /></td></tr>
				<tr><td colspan='2' align="right"><div align="right"><input type="submit" name="login" value="Login" /></div></td></tr>
			</table>

			</form>

	<?php








	/* ==================================================================================
				LOGGED IN - DO STUFF HERE */

	}	else 	{

		$user_data	=	get_user($_SESSION[SES.'userid']);
		$mappings	=	get_user_types();
		?>
		<h1>Logged in as  <?php echo $user_data['name'].' ['.$mappings[$user_data['level']].']'; ?></h1>
		<p>----------------------------------------------------------------</p>


		<?php

			/*
			 * 	==================================================================
			 * 								FLOW CONTROL
			 * 	==================================================================
			 */

			//	Work out the flow mode based on the $_GET vars...
			$tab_mode	=	'issues';
			if (isset($_GET['features']) && $_GET['features'] == 1)	{

				$tab_mode	=	'features';

			}	elseif (isset($_GET['schmovies']) && $_GET['schmovies'] == 1)	{

				$tab_mode	=	'schmovies';

			}	elseif (isset($_GET['diy']) && $_GET['diy'] == 1)	{

				$tab_mode	=	'diy';

			}

			//	If you are low level user then you'll be force into the features sections
			if ($_SESSION[SES.'userlevel'] == 0)	$tab_mode 	=	'features';


			//	Show the features/issues tab...
			if ($_SESSION[SES.'userlevel'] > 0)	{

					echo make_features_tab($tab_mode);

			}


			//	====================================================================================
			//	Test the $_GET vars and load the correct script in depending on what we find...

			//	--------------------------------
			//	EDIT USERS
			if (isset($_GET['edit_users']) && $_GET['edit_users'] == 1)	{

					require 'edit_users.php';

			}

			//	--------------------------------
			//	FEATURES
			elseif ($tab_mode == 'features')	{

				//	EDIT FEATURE ARTICLE
				if (isset($_GET['edit_article']) && is_numeric($_GET['edit_article']))	{

						$edit_article	=	$_GET['edit_article'];
						require 'edit_feature.php';

				}
				//	EDIT FEATURE CATEGORIES
				elseif (isset($_GET['edit_categories']) && $_GET['edit_categories'] == 1) {

						require 'edit_feature_categories.php';

				}
				//	LIST OF FEATURES
				else {

						require 'features.php';

				}

			//	---------------------------------
			//	SCHMOVIES
			}	elseif ($tab_mode == 'schmovies')	{

					require 'schmovies.php';


			//	---------------------------------
			//	DIY GUIDE
			}	elseif ($tab_mode	==	'diy')	{

					require 'diy_guide.php';

			}

			//	---------------------------------
			//	ISSUES

			//	EDIT ISSUE ARTICLE
			elseif (isset($_GET['editArticle']) && is_numeric($_GET['editArticle'])
				&& isset($_GET['issue']) && is_numeric($_GET['issue']))	{

					$issue			=	$_GET['issue'];
					$articleId		=	$_GET['editArticle'];
					require "editArticle.php";

			}
			//	UPDATE ISSUE ARTICLE
			elseif (isset($_GET['updateArticle']) && is_numeric($_GET['updateArticle'])
					&& isset($_GET['issue']) && is_numeric($_GET['issue']))	{

					$issue			=	$_GET['issue'];
					$articleId		=	$_GET['updateArticle'];
					if (isset($_POST['submit']))	require "updateArticle.php";
					elseif (isset($_POST['delete']))	require "deleteArticle.php";
					else	require "editionList.php";

			}
			//	EDIT ISSUE
			elseif (isset($_GET['editIssue']) && is_numeric($_GET['editIssue']))	{

					$issueId			=	$_GET['editIssue'];
					require "editIssue.php";

			}
			//	UPDATE ISSUE
			elseif (isset($_GET['updateIssue']) && is_numeric($_GET['updateIssue']))	{

					$issueId			=	$_GET['updateIssue'];
					if (isset($_POST['submit']))	require "updateIssue.php";
					elseif (isset($_POST['delete']))	require "deleteIssue.php";
					else	require "editionList.php";

			}
			// KEYWORDS LIST
			elseif (isset($_GET['keywordsList']) && $_GET['keywordsList'] == 1)	{

					require 'keywordList.php';

			}
			//	SETTINGS
			elseif (isset($_GET['settings']) && $_GET['settings'] == 1)	{

					require 'settings.php';

			}
			//	SHOW COMMENTS
			elseif (isset($_GET['comments']) && $_GET['comments'] == 1)	{

					require 'comments.php';

			}
			//	CREATE FULL ISSUE ON SERVER
			elseif (isset($_GET['createFullEdition']) && is_numeric($_GET['createFullEdition']))	{

					$stage		=	$_GET['createFullEdition'];
					require 'createIssue.php';

			}
			// 	SHOW ISSUE LIST...
			else 	require "editionList.php";
		?>





		<p>----------------------------------------------------------------</p>

		<?php
		if ($_SESSION[SES.'userlevel'] > 1)	{

		?>
		<a href='index.php?edit_users=1' target="_blank">Edit Users</a>

		<p>----------------------------------------------------------------</p>
		<?php } ?>

		<form action="index.php" method="POST">
			<input type="submit" name="logout" value="Logout" />
		</form>

		<?php
	}


?>
</body>
</html>
