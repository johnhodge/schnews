<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Checks to see whether a login has been attempted via the POST superglobal, and process the login if so
 *
 */




/*
 * Test for a login attempt
 */
if (isset($_POST['username']) && isset($_POST['password']))
{

	debug("Login detected");

	//	If the login is detected from an already logged in user then we have to assume a security breach
	if (LOGIN_is_user_logged_in() && ( isset($_SESSION['logged_in_username'.SESSION_APPEND]) && $_POST['username'] != $_SESSION['logged_in_username'.SESSION_APPEND]) && !isset($_POST['name']))
	{

		debug ("Login by already logged in user");

		//	Log the secutiry breah to the main, login and security logs as a security entry
		$log_message 	=	'Login detected from already logged in user. Current login = '.$_SESSION['logged_in_username'.SESSION_APPEND].'. New login = '.$_POST['username'];
		LOG_record_entry($log_message, 'security', 1);


		//	Display message of warning to user
		$output		=	array();
		$ouptut[]	=	"<h1>It looks like you are trying to login while already logged in</h1>";
		$output[]	=	"<p><b>This is considered a security breach, and to be safe you have been logged out of the system</b></p>";
		$output[]	=	'';
		$output[]	=	"<p>This incident has been logged and reported</p>";
		echo implode("\t\r\n\t", $output);


		//	Log the user out
		LOGIN_log_user_out();


		//	End the page
		unset($log);
		require "inc/html_footer.php";

	}


	$login_result	=	LOGIN_test_login($_POST['username'], $_POST['password']);

	if (is_array($login_result))
	{

		debug ("Login failed");
		$login_errors = $login_result;

	}
	else
	{

		debug ("Login Succeeded. ID = ".$login_result);
		$user = new user($login_result);
		$user->log_user_in();
	}

}

?>