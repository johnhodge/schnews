<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *  Gives the user the form to reset their password
 *
 */



/*
 * 	There is no real circumstance under which a user should be logged in at this point
 */
LOGIN_log_user_out();


/*
 * 	Prepare intro text
 */
if ( $action == 'set_ask' )
{

	$title		=	"Set the Password for Your New Account";
	$intro		=	"A new account has been set for you in our lovely little system.<br /><br />Before you can login you must create a password for your account:";
	$button		=	"Create Password";


}
else
{

	$title		=	"Set a New Password for Your Account";
	$intro		=	"Either you or an admin has requested that you reset your account password<br /><br />Please do so below: ";
	$button		=	"Reset Password";

}



$output		=	array( "\r\n\r\n\r\n");


$output[]	=	"<!-- BEGIN PASSWORD RE/SET -->";
$output[]	=	"<div class='password_reset_title'>";
$output[]	=	strtoupper( $title );
$output[]	=	"</div>";
$output[]	=	"\r\n\r\n<br /><br />\r\n\r\n";
$output[]	=	"<div class='password_reset_subtitle'>";
$output[]	=	$intro;
$output[]	=	"</dvi>";
$output[]	=	"\r\n\r\n<br /><br />\r\n\r\n";


$output[]	=	"\r\n\r\n<!-- BEGIN RESET FORM -->";
$url		=	URL_make_url_from_get( URL_get_GET_vars() );
$output[]	=	"<form action='$url&do=1' method='POST'>";
$output[]	=	"<div class='password_reset_form'>";
$output[]	=	"<table align='center'>";


if ( isset( $errors ) )
{

	foreach ( $errors as $error )
	{

		$output[]	=	"\r\n\r\n<!-- BEGIN ERROR MESSAGE -->";
		$output[]	=	"<tr>";
		$output[]	=	"<td colspan='2' class='error_message'>";
		$output[]	=	$error;
		$output[]	=	"</td>";
		$output[]	=	"</tr>";
		$output[]	=	"<!-- END ERROR MESSAGE -->";

	}

	$output[]	=	"\r\n<tr>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"</tr>";

}


if ( isset( $_GET[ 'password_reset' ] ) )
{

	//	OLD PASSWORD
	$output[]	=	"\r\n\r\n<tr>";
	$output[]	=	"<td class='settings_user_table_cell' align='right'>Current Password: </td>";
	$output[]	=	"<td class='settings_user_table_cell_no_border' align='left'>";
	$output[]	=	"<input type='password' name='old_password' />";
	$output[]	=	"</td>";
	$output[]	=	"</tr>";


	$output[]	=	"\r\n<tr>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"</tr>";

}


//	NEW PASSWORD
$output[]	=	"\r\n\r\n<tr>";
$output[]	=	"<td class='settings_user_table_cell' align='right'>New Password: </td>";
$output[]	=	"<td class='settings_user_table_cell_no_border' align='left'>";
$output[]	=	"<input type='password' name='password' />";
$output[]	=	"</td>";
$output[]	=	"</tr>";


$output[]	=	"\r\n<tr>";
$output[]	=	"<td>&nbsp;</td>";
$output[]	=	"</tr>";


//	PASSWORD VERIFICATION
$output[]	=	"\r\n\r\n<tr>";
$output[]	=	"<td class='settings_user_table_cell' align='right'>Password Verification: </td>";
$output[]	=	"<td class='settings_user_table_cell_no_border' align='left'>";
$output[]	=	"<input type='password' name='password_verify' />";
$output[]	=	"</td>";
$output[]	=	"</tr>";


$output[]	=	"\r\n<tr>";
$output[]	=	"<td>&nbsp;</td>";
$output[]	=	"</tr>";


// 	SUBMIT BUTTON
$output[]	=	"\r\n\r\n<tr>";
$output[]	=	"<td colspan='2' align='right'>";
$output[]	=	"<input type='submit' value='$button' />";
$output[]	=	"</td>";
$output[]	=	"</tr>";



$output[]	=	"</table>";
$output[]	=	"</div>";
$output[]	=	"<!-- END RESET FORM -->";



/*
 * 	Display Account Info
 */
$output[]	=	"\r\n\r\n<br /><br />\r\n\r\n";
$output[]	=	"<div class='password_reset_account_info'><u>Your Account Info Is</u></div>";
$output[]	=	"\r\n\r\n<br />\r\n\r\n";
$output[]	=	"<table align='center'>";

//	Name
$output[]	=	"<tr><td align='right'><b>Real Name : </b></td><td align='left'>&nbsp;&nbsp;&nbsp;" . $user->get_name() . "</td></tr>";

$output[]	=	"<tr><td>&nbsp;</td></tr>";

//	Username
$output[]	=	"<tr><td align='right'><b>Login Username : </b></td><td align='left'>&nbsp;&nbsp;&nbsp;" . $user->get_username() . "</td></tr>";

$output[]	=	"<tr><td>&nbsp;</td></tr>";


//	Email
$output[]	=	"<tr><td align='right'><b>Email Address : </b></td><td align='left'>&nbsp;&nbsp;&nbsp;<a href='mailto:" . $user->get_email() . "'>" . $user->get_email() . "</a></td></tr>";
$output[]	=	"</table>";
$output[]	=	"<!-- END PASSWORD RE/SET -->";



echo implode( "\r\n", $output );



