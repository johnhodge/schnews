<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *  Tests a submitted password reset
 *
 */



$errors		=	array();



/*
 * 	Test current password
 */
if ( isset( $_GET[ 'password_reset' ] ) )
{

	$user 	=	new user( $_GET[ 'password_reset' ] );
	if ( trim( $_POST[ 'old_password' ] ) == '' )
	{

		$errors[]	=	"Existing Password not set";

	}
	elseif ( !isset( $_POST['old_password'] ) || md5( $_POST[ 'old_password' ] ) != $user->get_password_hash() )
	{

		$errors[]	=	"The Existing Password Entered is Wrong";

	}

}




/*
 * 	Test password
 */
if ( isset( $_POST[ 'password' ] ) && trim( $_POST[ 'password' ] != '' ) )
{

	$password	=	$_POST[ 'password' ];

}
else
{

	$errors[]	=	"Password not set";

}



/*
 * 	Test password verification
 */
if ( isset( $_POST[ 'password_verify' ] ) && trim( $_POST[ 'password_verify' ] != '' ) )
{

	$password_verify	=	$_POST[ 'password_verify' ];

}
else
{

	$errors[]	=	"Password Verification not set";

}



/*
 * 	Test Verification Match
 */
if ( isset( $password ) && isset( $password_verify ) && $password != $password_verify )
{

	$errors[]	=	"Password verification does not match";

}



/*
 * 	If there are any errors then call the form again and exit
 */
if ( count( $errors ) != 0 )
{

	require "scripts/password_reset_ask.php";
	require "inc/html_footer.php";

}



