<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script should be called by every page in the storyAdmin system.
 *
 * 	It testsn whether the user is logged in and calls the login form if not.
 * 	If the user is logged in it sets the appropriate constants for the other pages to use.
 *
 */




//	Test whether the user is currently logged in...
if (!LOGIN_is_user_logged_in())
{


	//	Draw the login form
	require "inc/login_form.php";


	//	End the page
	unset($log);
	require "inc/html_footer.php";

}




