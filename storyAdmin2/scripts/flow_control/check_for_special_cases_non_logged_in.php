<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Check for any execptional cases outside of the normal flow control (e.g. password reset)
 *
 * 	NOTE ***IMPORTANT*** :: Do not do anything here that should require a login
 *
 */


/*
 * 	DETERMINE THE REQUIRED ACTION
 */

/*
 * 	PASSWORD SET
 *
 * 	User clicks on a link in an email notifying them that their account has been created
 */
 if ( isset( $_GET[ 'password_set' ] ) && CORE_is_number( $_GET[ 'password_set' ] ) )
 {

 	$user		=	new user ( $_GET[ 'password_set' ] );
	if ( !isset( $_GET[ 'hash' ] ) || !$user->check_reset_hash( $_GET[ 'hash' ] ) )
	{

		$action 	=	'no';

	}
	elseif ( isset( $_GET[ 'do' ] ) && $_GET[ 'do' ] == '1' )
	{

		$action		=	'set_do';

	}
	else
	{

		$action		=	'set_ask';

	}

 }



 /*
 * 	PASSWORD RESET
 *
 * 	User clicks on a link in an email notifying them that they need to reset their password
 */
 if ( isset( $_GET[ 'password_reset' ] ) && CORE_is_number( $_GET[ 'password_reset' ] ) )
 {

 	$user		=	new user ( $_GET[ 'password_reset' ] );
	if ( !isset( $_GET[ 'hash' ] ) || !$user->check_reset_hash( $_GET[ 'hash' ] ) )
	{

		$action 	=	'no';

	}
	elseif ( isset( $_GET[ 'do' ] ) && $_GET[ 'do' ] == '1' )
	{

		$action		=	'reset_do';

	}
	else
	{

		$action		=	'reset_ask';

	}

 }



 /*
  * 	PERFROM ACTION
  */
if ( isset( $action ) )
{

	echo "\r\n\r\n<div class='section_container'>";
	echo "\r\n<br /><br /><br /><br />";

	switch ( $action )
	{

		case 'no':
			require "scripts/security_rejection.php";
			break;

		case 'set_ask':
			require "scripts/password_reset_ask.php";
			break;

		case 'reset_ask':
			require "scripts/password_reset_ask.php";
			break;

		case 'set_do':
			require "scripts/password_reset_do.php";
			break;

		case 'reset_do':
			require "scripts/password_reset_do.php";
			break;

	}
	echo "</div>";

	require "inc/html_footer.php";

}




