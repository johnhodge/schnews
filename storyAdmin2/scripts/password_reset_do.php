<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *  Re/sets the user password
 *
 */



$_SESSION['logged_in_name'.SESSION_APPEND]		=	"Not Set";



/*
 * 	Test that the password and verification are OK
 */
require "scripts/test_password_submission.php";



/*
 * 	Make sure we have a valid user id
 */
if ( isset( $_GET[ 'password_reset' ] ) && CORE_is_number( $_GET[ 'password_reset' ] ) )
{

	$id		=	$_GET[ 'password_reset' ];
	$mode	=	'reset';

}
if ( isset( $_GET[ 'password_set' ] ) && CORE_is_number( $_GET[ 'password_set' ] ) )
{

 	$id		=	$_GET[ 'password_set' ];
 	$mode	=	'set';

}



/*
 * 	Make a new user object and the update the password
 */
$user		=	new user( $id );
$user->set_password_hash( $password );
$user->update_user_password();
$user->remvoe_reset_hash();



/*
 * 	Add a log entry
 */
if ( $mode == 'reset' )
{

	$message		=	"User has reset their password. User = '" . $user->get_name() . "'";
	$mail_message_a	=	"User " . $user->get_name() . " has reset their password";
	$mail_message_u	=	"Hello " . $user->get_name() . ",\r\n\r\n\r\nYou (I hope) have reset your password.\r\n\r\n\r\nSchNEWS";
	$notice 		=	"You Have Reset Your Password.<br /><br />You've been emailed to confirm this";

}
else
{

	$message		=	"New user has set their password. User = '" . $user->get_name() . "'";
	$mail_message_a	=	"New User " . $user->get_name() . " has set their password";
	$mail_message_u	=	"Hello " . $user->get_name() . ",\r\n\r\n\r\nYou (I hope) have created your password. You can now login to the SchNEWS system.\r\n\r\nClick below to get going:\r\n\r\n" . SYSTEM_SITE_ROOT . "\r\n\r\n\r\nSchNEWS";
	$notice 		=	"You Have Created Your Password.<br /><br />You've been emailed to confirm this";

}
LOG_record_entry( $message );



$subject		=	"SchNEWS StoryAdmin :: Your Password Updated";
$mail			=	new email( $mail_message_u, $subject, $user->get_email() );
$mail->send();
$subject		=	"SchNEWS StoryAdmin :: User Password Updated";
$mail			=	new email( $mail_message_a, $subject, SYSTEM_ADMIN_EMAIL );
$mail->send();



$output			=	array();
$output[]		=	"\r\n\r\n\r\n";
$output[]		=	"\r\n\r\n<br /><br />\r\n\r\n";
$output[]		=	"<!-- BEGIN PASSWORD CHANGE NOTICE -->";
$output[]		=	"<div class='big_notice_message'>\r\n\t$notice\r\n</div>";
$output[]		=	"\r\n\r\n<br /><br /><br /><br />\r\n\r\n";
$output[]		=	"<div>";
$output[]		=	"Click here to login to your account: ";
$output[]		=	"<a href='" . SYSTEM_SITE_ROOT . "'>" . SYSTEM_SITE_ROOT . "</a>";
$output[]		=	"</div>";
$output[]		=	"<!-- END PASSWORD CHANGE NOTICE -->";
$output[]		=	"\r\n\r\n";

echo implode ( "\r\n", $output );











