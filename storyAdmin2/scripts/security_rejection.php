<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *  Displays a message telling the user a security objection has occured, and spams everyone and everything with warnings.
 *
 */



/*
 * 	Draw stuff to screen, giving the user a false sense of reassurance
 */
$output		=	array();
$output[]	=	"<div class='big_notice_message'>";
$output[]	=	"A Security Violation Has Occured";
$output[]	=	"</div>";
$output[]	= 	"<br /><br />";
$output[]	=	"<div class='notice_message'>";
$output[]	=	"It probably wasn't your fault (unless it was, in which case boy are you in trouble), but I can't let you continue.";
$output[]	=	"<br /><br />";
$output[]	=	"If you want to moan to someone then try <a href='mailto:webmaster@schnews.org.uk'>webmaster@schnews.org.uk</a> or <a href='mailto:" . SYSTEM_ADMIN_EMAIL . "'>" . SYSTEM_ADMIN_EMAIL . "</a>";
$output[]	=	"</div>";

echo implode( "\r\n", $output );



/*
 * 	Security log entry, as the incorrect hash is a risk of URL hacking
 */
LOG_record_entry( "Password reset attempted with incorrect or no hash", 'security', 2 );



/*
 * 	Email system admin, as incorrect hash is URL hacking risk
 */
$subject 	=	"SchNEWS StoryAdmin :: Password Reset Security Violation";
$message	=	"Password reset attempted with incorrect hash value.\r\n\r\nCheck the security log";
$email		=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
$email->send();
