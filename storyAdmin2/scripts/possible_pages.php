<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Work ot which pages are available to the user, based on their privilage status
 */



// Work out which pages to include
switch ($_SESSION['logged_in'.SESSION_APPEND])
{

	// Play it safe and default to a level 1 user (i.e. no access to settings
	default:
		$possible_pages		=	array(

			'stories',
			'issues',
			'keywords'

		);
		break;

	case 2:
		$possible_pages		=	array(

			'stories',
			'issues',
			'keywords',
			'homepage',
			'comments',
			'settings'

		);
		break;

}



