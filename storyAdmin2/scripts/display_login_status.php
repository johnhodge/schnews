<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	14th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Display info to the user about their current login status
 */



// If the user is not logged in...
if (!LOGIN_is_user_logged_in())
{

	if (isset($_GET['logout']))
	{

		echo "<div style='color: blue'>You have successfully logged out of the system</div>";

	}
	else
	{

		echo "You are not logged in";

	}

}
else
{

	$user		=	new user($_SESSION['logged_in_id'.SESSION_APPEND]);
	$datetime	=	new date_time($_SESSION['logged_in_last_login'.SESSION_APPEND]);

	$status_line		=	'';
	switch ( $user->get_status() )
	{
		
		case '1':
			$status_line	=	"&nbsp;&nbsp;<a href='help/?show=Users' target='_BLANK'>[ Guest User ]</a>";
			break;
			
		case '2':
			$status_line	=	"&nbsp;&nbsp;<a href='help/?show=Users' target='_BLANK'>[ Admin ]</a>";
			break;
			
	}

	$output		=	array();
	$output[]	=	"\r\n\r\n<!-- BEGIN LOGIN INFO BOX -->";
	$output[]	=	"You&nbsp;are&nbsp;logged&nbsp;in&nbsp;as&nbsp;<b>".$_SESSION['logged_in_name'.SESSION_APPEND]."</b>" . $status_line;
	$output[]	=	"<br />";
	$output[]	=	"<b>Last Login:</b> ".$datetime->make_human_date().', '.$datetime->make_human_time();
	$output[]	=	"<br />";
	$output[]	=	"<form method='POST' action='index.php?logout=1'>";
	$output[]	=	"<br />";
	$output[]	=	"<input type='submit' value='Logout' />";
	$output[]	=	"</form>";
	$output[]	=	"<!-- END LOGIN INFO BOX -->\r\n\r\n";

	echo implode("\r\n", $output);

}