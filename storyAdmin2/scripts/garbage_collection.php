<?php

	/*
	 * Run the garbage collector.
	 * 
	 * If this proves to expensive to run each page load then add a
	 * rand(0 , x) hit so it only runs every x page loads
	 */
	GC_collect_garbage();
	
?>