<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	20th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Acts as a wrapper for the info logging classes
 */



/*
 * 	A wrapper for the logging class
 * 
 * 	Causes the message to be logged to the main.log plus an optional extra
 * 	specific log file.
 * 
 * 	@param $message string			|| The message
 * 	@param $type string				|| Options = 'security', 'error'
 * 	@param $status int				|| 0 = information / 1 = security / 2 = error
 * 	@param $email bool				|| Whether to email the message to the designated admin (if specified in the LOG/CONF file)
 */
function LOG_record_entry($message, $type = false, $status = 0, $email = false)
{
	
	// Test inputs
	if (!is_string($message) || trim($message) == '') return false;
	if (!is_string($type) || trim($type) == '') $type = false;
	if ($status != 0 && $status != 1 && $status != 2) $status = 0;
	if (!is_bool($email)) $email = false;
	
	
	//	Open the main log.
	//		Everything gets logged to the main log, plus optionally also gets logged
	//		to another specific log.
	$log	=	new log_entry(LOGFILE_PATH.'main.log');
	$log->add_log_entry($message, $status, $email);
	unset($log);
	
	switch ($type)
	{
		
		case "security":
			$log	=	new log_entry(LOGFILE_PATH.'security.log');
			break;

		case "error":
			$log	=	new log_entry(LOGFILE_PATH.'error.log');
			break;
			
		case "comments":
			$log	=	new log_entry(LOGFILE_PATH.'comments.log');
			break;
		
	}
	
	if (isset($log))
	{
		
		$log->add_log_entry($message, $status, $email);
		unset($log);
	
	}

	return true;
	
}


