<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	24th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Controls formatting for the internal use of the SA system
 */



function FORMAT_make_story_for_list( $story, $read_only = false )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_object( $story ) ) 
	{
		
		debug( "FORMAT_make_story_for_list() called without an object input", __FILE__, __LINE__ );
		return false;
		
	}
	if ( !is_bool( $read_only ) ) $read_only 	=	false;
	
	
	
	/*
	 * Prepare some data
	 */
	$user	=	new user ( $story->get_author() );
	$date	=	new date_time($story->get_date_added() );
	$issue	=	new issue( $story->get_issue() );
	
	
	
	
	
	
	/*
	 * 	Work out if we need use a coloured container
	 */
	$container_style	=	'';
	if ( $story->get_live() == 0 )
	{

		$container_style	=	" style='border: 2px solid red' ";
		
	}
	if ( $story->get_sticky() == 1 )
	{
		
		$container_style	=	" style='border: 2px solid blue' ";
		
	}
	
	
	
	
	
	$info_bar		=	FORMAT_make_infoBar( $story );
	
	
	
	
	
	
	
	
	/*
	 * If it is read_only then don't display the links
	 */
	if ( $read_only )
	{
		
		$checkout_link 	=	'';
		$vote_link		=	'';
		
	}
	
	
	/*
	 * Prepare output data
	 */
	$output		=	array();
	$output[]	=	"<!-- BEGIN FORMATTED STORY SUMMARY -->";
	$output[]	=	"<div class='stories_list_container' $container_style >";
	$output[]	=	"<div class='stories_list_headline'>";
	$output[]	=	$story->get_headline();
	
	if ( !$read_only )
	{
		$output[]	=	FORMAT_make_edit_links_for_list_story( $story, $_SESSION['logged_in'.SESSION_APPEND] );
	}
	
	$output[]	=	"</div>";	
	
	$output[]	=	"<div class='jq_summary_container' id='jq_summary_container'>";
	
	$output[]	=	"<div class='stories_list_summary'>" . $story->get_summary() . "</div>";

	$output[]	=	"</div>";	
	
	$output[]	=	"<div class='stories_list_info_line'>";
	$output[]	=	$info_bar;
	$output[]	=	"</div>";
	
	

	
	
	$output[]	=	"</div>";
	
	$ouptut[]	=	"<!-- END FORMATTING STORY SUMMARY -->";
	
	return implode( "\r\n", $output );
	
}



function FORMAT_make_edit_links_for_list_story( $story, $user_status = 1 )
{

	//	Test inputs
	if ( !is_object( $story ) ) return false;
	if ( !CORE_is_number( $user_status ) ) $user_status = 1;

	
	
	/*
	 * Make voting link
	 */
	$vote_link		=	"<span class='stories_list_vote_link'><a href='?page=stories&vote=" . $story->get_id() . "'> [ VOTE FOR STORY ] </a></span>";
	if ( $story->has_user_voted_for_Story() )
	{
		
		$vote_link		=	"<span class='stories_list_vote_link'><a href='?page=stories&unvote=" . $story->get_id() . "'> [ REMOVE YOUR VOTE ] </a></span>";
		
	}
	if ( $story->get_checked_out() )
	{
		
		$vote_link		=	'';
		
	}
	if ( $story->get_author() == $_SESSION['logged_in_id'.SESSION_APPEND] )
	{
		
		$vote_link		=	"<span class='stories_list_vote_link'>You Cannot Vote for Your Own Story</span>";
		
	}
	
	
	
	/*
	 * Test for checkout
	 */
	$checkout_link		=		"<span class='stories_list_edit_story_link'><a href='?page=stories&preview=". $story->get_id() . "'>[ EDIT ]</a></span>"; 
	if ( $story->get_checked_out() )
	{
		
		if ( $story->get_checked_out() == $_SESSION['logged_in_id'.SESSION_APPEND] )
		{

			$checkout_link	=	"<span class='stories_list_checked_out'>You have the story locked. <a href='?pages=stories&edit_story=" . $story->get_id() . "'>Click Here</a> to continue editing.</span>";
			
		}
		else
		{
		
			$ch_user	=	new user ( $story->get_checked_out() );
			$ch_date	=	new date_time( $story->get_checkout_last_activity() );
			$checkout_link	=	"<span class='stories_list_checked_out'>Story is <a href='help?show=Story_Locking' target='_BLANK'>Locked</a> for Editing by <b>" . $ch_user->get_name() . "</b>. The last activity was " . $ch_date->make_human_date() . ' @ ' . $ch_date->make_human_time() . "</span>"; 
		
		}
		
	}
	
	
	
	/*
	 * 	Put together a different links bar depending on the user's status
	 */
	switch ( $user_status )
	{

		/*
		 * This shouldn't ever happen
		 */
		case '0':
			return '';
			break;
			
			
		/*
		 * 	A User/Guest level 2 user
		 */	
		case '1':
			
			/*
			 * 	A level 1 user should should only be able to edit their own stories
			 */
			if ( $story->get_author() != $_SESSION['logged_in_id'.SESSION_APPEND] )
			{
				
				$checkout_link 	=	"<span class='stories_list_checked_out'>You Can't Edit Stories You Didn't Write - <a href='?page=stories&preview=". $story->get_id() . "'> [ View Read Only ] </a></span>";
				
			}
			return $checkout_link . $vote_link;
			break;
			
			
		/*
		 * 	An Admin/Editor level 2 user
		 */
		case '2':
			return $checkout_link . $vote_link;		
			break;
		
	}

}





function FORMAT_make_revision_summary( $id, $read_only = false )
{

	/*
	 * Test inputs
	 */	
	if ( !CORE_is_number( $id ) ) 
	{
		$message	=	"FORMAT_make_revision_summary called with non-numeric ID";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2);
				
	}
	
	if ( !is_bool( $read_only ) ) $read_only 	=	false;
	
	
	
	/*
	 * 	Do DB query
	 */
	$mysql		=	new mysql_connection();
	$sql		=	" 	SELECT * 
						FROM revisions
						WHERE id = " . $id;
	$mysql->query( $sql );
	
	
	
	/*
	 * There sould only be one result
	 */
	if ( $mysql->get_num_rows() != 1 )
	{
		
		$message 	=	"FORMAT_make_revision_summary did not yield 1 result";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2);
		return false;
		
	}
	
	

	/*
	 * Begin output
	 */
	$rev		=	$mysql->get_row();
	$output		=	array();
	$output[]	=	"<!-- BEGIN REVISION SUMMARY -->";
	$output[]	=	"<div class='revision_summary_container'>";
	
	
	/*
	 * 	Generate info line
	 */
	$datetime		=	new date_time( $rev['date_saved'] );
	$author			=	new user( $rev['author'] );
	$info_line		=	"Added on <b>" . $datetime->make_human_date() . "</b> at <b>" . $datetime->make_human_time() . "</b> by <b>" . $author->get_name() . "</b>";
	$info_line		.=	"&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;";
	$info_line		.=	REVISIONS_work_out_word_count( $rev['content'] ) . " words";
	
	
	$output[]		=	"<div class='revision_summary_info_line'>";
	$output[]		=	$info_line;
	$output[]		=	"</div>";
	
	
	/*
	 * Main content
	 */
	$output[]		=	"<div class='revision_summary_content'>";
	$output[]		=	$rev['content'];
	$output[]		=	"</div>";
	
	
	
	
	
	
	/*
	 * Internal Comment
	 */
	if ( $rev['internal_comment'] != '' )
	{
		
		$output[]	=	"<div class='revision_summary_internal_comment'>";
		$output[]	=	"<div class='revision_summary_internal_comment_title'>Internal Comment</div>";
		$output[]	=	nl2br( $rev['internal_comment'] );
		$output[]	=	"</div>";
		
	}
	
	
	/*
	 * External Comment
	 */
	if ( $rev['external_comment'] != '' )
	{
		
		$output[]	=	"<div class='revision_summary_external_comment'>";
		$output[]	=	"<div class='revision_summary_external_comment_title'>External Comment</div>";
		$output[]	=	nl2br( $rev['external_comment'] );
		$output[]	=	"</div>";
		
	}
	
	
	
	/*
	 *	Revert Link 
	 */
	if ( !$read_only )
	{
		$output[]		=	"<div class='revision_summary_revert_link'>";
		$output[]		=	"<a href='?page=stories&edit_story=" . $rev['story_id'] . "&revert=" . $id . "&form_page=revision' onclick='return show_revision_revert_warning()'>Revert to This Text</a>";
		$output[]		=	"</div>";
	}
	
	$output[]		=	"</div>";
	$output[]		=	"<!-- END REVISION SUMMARY -->";
	return 	implode ("\r\n", $output );
	
}





function FORMAT_make_story_preview( $story )
{

	//	Test inputs
	if ( !is_object( $story ) ) return false;
	
	$output		=	array();
	$output[]	=	"<!-- BEGIN STORY PREVIEW -->";
	$output[]	=	"<div class='story_preview_container'>";
	$output[]	=	"<div class='story_preview_headline'>" . $story->get_headline() . "</div>";
	$output[]	=	"<div class='story_preview_info_line'>";
	
	$author		=	new user( $story->get_author() );
	$date		=	new date_time( $story->get_date_added() );
	
	$output[]	=	"Author: <b>" . $author->get_name() . "</b>";
	$output[]	=	"&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;";
	$output[]	=	"Added on <b>" . $date->make_human_date() . "</b>";
	
	$output[]	=	"</div>";
	
	$output[]	=	"<div class='story_preview_content'>";
	
	$revision 	=	$story->get_latest_revision();
	$output[]	=	$revision->get_content();
	$output[]	=	"</div>";
	$output[]	=	"</div>";
	$output[]	=	"<!-- END STORY PREVIEW -->";
	
	return implode ( "\r\n", $output );
	
}





function FORMAT_make_left_bar_story_summary( $story )
{
	
	//	Test Inputs
	if ( !is_object( $story ) ) return false;	
	
	$output		=	array();
	
	$output[]	=	"<!-- BEGIN STORY LEFT BAR PREVIEW -->";
	$output[]	=	"<div class='story_preview_left_container'>";
	$output[]	=	"<div class='story_preview_left_headline'>" . $story->get_headline() . "</div>";
	$output[]	=	"<div class='story_preview_left_summary'>" . $story->get_summary() . "</div>";
	$output[]	=	"</div>";
	$output[]	=	"<!-- END STORY LEFT BAR PREVIEW -->";
	
	return implode ( "\r\n", $output );
}






function FORMAT_make_infoBar( $story )
{
	
	$user	=	new user ( $story->get_author() );
	$date	=	new date_time($story->get_date_added() );
	$issue	=	new issue( $story->get_issue() );
	
	
	/*
	 * 	Generate Info Bar
	 */
	$spacer		=	"&nbsp;&nbsp;&nbsp;&nbsp;";
	$output		=	array();
	
	$output[]	=	"Added by <b>" . $user->get_name() . "</b>";
	$output[]	=	$spacer . " | " . $spacer;
	
	$output[]	=	"Added on <b>" . $date->make_human_date() . "</b>";
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	$output[]	=	'<b>' . $story->get_hits() . '</b> hits ';
	
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	$output[]	=	"<b>" . REVISIONS_get_number_of_revisions( $story->get_id() ) . "</b> revisions";
	
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	$output[]	=	"<b>" . FORMAT_get_number_of_comments( $story->get_id() ) . "</b> comments";
	
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	if ( !secret_story_ballot )
	{
		
		$output[]	=	"<a href='?page=stories&show_votes=" . $story->get_id() . "'>";
		
	}
	$output[]	=	"<b>" . $story->get_number_of_votes() . "</b> votes total";
	$output[]	=	" [ <b>" . $story->get_number_of_admin_votes() . "</b> admin votes ] ";
	if ( !secret_story_ballot )
	{
		
		$output[]	=	"</a>";
		
	}
	
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	$output[]	=	"www.schnews.org.uk/n/a=" . $story->get_id();
	
	$output[]	=	$spacer . $spacer . ' | ' . $spacer . $spacer;
	
	$output[]	=	"Issue " . $issue->get_number();
	
	$info_bar	=	implode( "\r\n", $output );	
	
	return $info_bar;	
	
}




function FORMAT_get_number_of_comments( $id )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	" 	SELECT id
					FROM comments
					WHERE
						story = $id
					AND deleted <> 1
					AND spam_score < " . spam_points_quarantine;
	$mysql->query( $sql );
	
	if ( !CORE_is_number( $mysql->get_num_rows() ) ) return '0';
	
	return $mysql->get_num_rows();	
	
}






