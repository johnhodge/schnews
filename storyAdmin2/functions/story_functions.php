<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Functions for manipulating stories
 * 
 */



/*
 * 	Finds the ID of the most recent revision for a given story
 * 
 * 	@param id [INT]			//	The MySQL ID of the story
 * 	@return int				//	The MySQL ID of the latest revision
 */
function STORIES_get_latest_revision( $id )
{
	//	Test inputs
	if (!CORE_is_number( $id ) || $id == 0 )
	{
		return false;
	}
	
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT id 
						FROM revisions
						WHERE story_id = " . $id . "
						ORDER BY date_saved DESC
						LIMIT 0, 1 ";
	$mysql->query( $sql );
	
	
	/*
	 * There should be 1 and only 1 result
	 */
	if ( $mysql->get_num_rows() != 1 )
	{

		$message	=	"SQL query in STORIES_get_latest_revision did not yield 1 result. Story id = $id";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	$res		=	$mysql->get_row();
	return $res[ 'id' ];
	
}




/*
 * 	Load a conf file containing an array of the possible on screen locations.
 * 
 * 	@return array 
 */
function STORIES_get_possible_on_screen_positions()
{
	
		require "conf/possible_on_screen_positions.php";
		return $possible_on_screen_positions;
	
}



/*
 * 	Load a conf file containing an array of the possible on screen sizes.
 * 
 * 	@return array 
 */
function STORIES_get_possible_on_screen_sizes()
{
	
		require "conf/possible_on_screen_positions.php";
		return $possible_on_screen_sizes;
	
}




/*
 * Test whether a story has received a sufficient number of votes, both in total and
 * from admins
 * 
 * @param story [INT]			//	The story's SQL id
 * @return bool  
 */
function STORIES_has_story_received_enough_votes( $story )
{
	
	// Test Inputs
	if ( !CORE_is_number( $story ) ) return false;
	
	$story	=	new story( $story );
	
	
	
	/*
	 * 	Get number of total votes
	 */
	$total_votes	=	$story->get_number_of_votes();
	
	
	
	/*
	 * Get number of admin votes
	 */
	$admin_votes	=	$story->get_number_of_admin_votes();
	
	
	
	/* 
	 * Test the number of votes
	 */
	if (	$total_votes >= number_story_votes_to_live
		AND $admin_votes >=	number_admin_votes_to_live )
		{
			return true;
		}
	else
		{
			return false;
		}
		
}







/*
 * 	Generates a PHP file for the story
 */
function STORIES_make_story_file( $id )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $id ) )
	{
		
		$message	= 	"Input id is not a number, in STORIES_make_story_file()";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 1 );
		return false;
		
	}
	
	
	
	/*
	 * 	Make a new object for the story
	 */
	$story	=	new story( $id );
	
	
	
	/*
	 * 	Work out the target path for the story
	 */
	$filename	=	STORIES_make_story_link( $story->get_headline() );		
	$path		=	SITE_STORY_LOC . $filename;
	if ( file_exists( $path ) )
	{
		
		return $path;
		
	}
	mkdir( $path );
	$path		.=	"/index.php";
	
	
	
	
	/*
	 * 	Load and process the page template
	 */
	$template	=	file_get_contents( "inc/story_template" );
	$template	=	str_replace( "***ID***", $id, $template );
	
	
	
	/*
	 * 	Make the story
	 */
	if ( !file_put_contents( $path, $template ) )
	{

		$message	= 	"Could not write to path, in STORIES_make_story_file(). Path = $path";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 1 );
		return false;
		
	}
	
	
	
	return true;
	
}





function STORIES_make_story_link( $link )
{
	
	/*
	 * 	Test inputs
	 */	
	if( !is_string( $link ) || empty( $link ) ) return false;
	
	
	
	/*
	 * 	Change spaces to hyphens
	 */
	$hyphens	=	array( ' ', '_');
	$link		=	str_replace( $hyphens, '-', $link );
	
	
	
	/*
	 * 	Remove quote marks
	 */
	$removes	=	array( '"', "'", ':', ';', '<', '>', '?' );
	$link		=	str_replace( $removes, '', $link );
	
	

	/*
	 * 	Replace ampersands
	 */
	$link		=	str_replace( "&", "and", $link );
	
	
	return $link;
	
}








