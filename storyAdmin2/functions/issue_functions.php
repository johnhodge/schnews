<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	27th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Basic functions for manipulating issues
 */



/*
 * 	Gets the MySQL ID of the database issue record with the highest number
 * 
 * 	@return int
 */
function ISSUES_get_newest_issue()
{
	
	/*
	 * 	Select the ID of the issue record with the greatest number
	 */
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT id 
						FROM issues
						ORDER BY number DESC
						LIMIT 0, 1";
	$mysql->query( $sql );
	
	
	/*
	 * 	There should only be one result
	 */
	if ( $mysql->get_num_rows() != 1 )
	{

		$message		=	"There were <> 1 results return in SQL by get_newest_issue()";
		debug( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * Return the ID
	 */
	$res		=	$mysql->get_row();
	$id			=	$res[ 'id' ];
	unset( $mysql );
	return $id;
	
}



/*
 * 	Determines the number of stories associated with a given issue
 * 
 * 	@param $id int					//	MySQL ID of the issue
 * 	@param $only_public bool		//	Only count stories that are live to the public, or count all stories (even those not yet live)
 * 	@return int 
 */
function ISSUES_get_num_stories_for_issue( $id, $only_public = true )
{
	
	//	Test inputs
	if ( !CORE_is_number( $id ) ) return false;
	if ( !is_bool ($only_public ) ) $only_public = true;
	

	$sql_add	=	'';
	if ( $only_public )
	{
		
		$sql_add 	=	" AND live = 1 and hide = 0 ";
		
	}
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT id
						FROM stories
						WHERE issue = " . $id . $sql_add;
	$mysql->query( $sql );
	
	return $mysql->get_num_rows();
	
}





/*
 * 	Generates an email body, suitable for sending to the plain text
 * 	mailing list upon completion of an issue.
 * 
 * 	@param int $issue_id
 * 	@return string
 */
function ISSUES_make_plain_text_list_email_body( $issue_id )
{

	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $issue_id ) ) return false;
	
	
	
	/*
	 * 	Load email template
	 */
	$template	=	file_get_contents( "email_templates/plain_text_mailing_list_issue" );

	
	
	/*
	 * Get latest Wake Up and Disclaimer text
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT wake_up_text, disclaimer
					FROM homepage_misc_fields
					ORDER BY date DESC
					LIMIT 0, 1 ";
	$mysql->query( $sql );
	$res	=	$mysql->get_row();
	
	$wake_up		=	$res['wake_up_text'];
	$disclaimer		=	$res['disclaimer'];
	
	
	
	/*
	 * 	Get the issue and stories
	 */
	$issue		=	new issue( $issue_id );
	$stories	=	ISSUES_get_stories_for_mailing_list( $issue_id );
	
	
	
	/*
	 * 	Prepare the issue date
	 */
	if ( $issue->get_date_published() != '0000-00-00 00:00:00' )
	{
		$date	=	new date_time( $issue->get_date_published() );
	}
	else
	{
		$date	=	new date_time( DATETIME_make_sql_datetime() );
	}
	
	
	
	/*
	 * 	Fill the template with data
	 */
	$template	=	str_replace( "***WAKEUP_TEXT***", $wake_up, $template );
	$template	=	str_replace( "***DISCLAIMER***", $disclaimer, $template );
	$template	=	str_replace( "***ISSUE_NUMBER***", $issue->get_number(), $template );
	$template	=	str_replace( "***PUBLISHED_DATE***", $date->make_human_date(), $template );
	$template	=	str_replace( "***STORIES***", $stories, $template );
	
	$template 	=	wordwrap( $template, 80 );
	
	return $template;
	
}






function ISSUES_make_plain_text_list_email_subject( $issue_id )
{

	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $issue_id ) ) return false;
	
	
	
	$issue	=	new issue( $issue_id );
	
	if ( $issue->get_date_published() != '0000-00-00 00:00:00' )
	{
		$date	=	new date_time( $issue->get_date_published() );
	}
	else
	{
		$date	=	new date_time( DATETIME_make_sql_datetime() );
	}
	
	$subject	=	"SchNEWS " . $issue->get_number();
	$subject	.=	", " . $date->make_human_date();
	$subject	.=	" - PLAIN TEXT";
	
	return $subject;

}



/*
 * 	Generates the list of stories for including in the plain text
 * 	mailing list email
 * 
 * 	@param int $issue_id
 * 	$return string
 */
function ISSUES_get_stories_for_mailing_list( $issue_id )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !CORE_is_number( $issue_id ) ) return false;

	$mysql	=	new mysql_connection();
	
	
	
	/*
	 * 	Get any sticky stories first
	 */
	$ids	=	array();
	$sql	=	"	SELECT id 
					FROM stories
					
					WHERE 
						live = 1
					AND	hide <> 1
					AND sticky = 1
					AND deleted <> 1
					AND issue = $issue_id
					
					ORDER BY 
						date_live DESC
					";
	$mysql->query( $sql );
	
		
	/*
	 * 	Add them to an array
	 */
	while( $res		=	$mysql->get_row() )
	{

		$ids[]	=	$res[ 'id' ];
		
	}
	
	
	
	
	/*
	 * 	Grab IDs of non-sticky stories from database
	 */
	$sql	=	"	SELECT id 
					FROM stories
					
					WHERE 
						live = 1
					AND	hide <> 1
					AND sticky = 0
					AND deleted <> 1
					aNd issue = $issue_id
					
					ORDER BY 
						date_live DESC
						
					";
	$mysql->query( $sql );
	
	
	/*
	 * 	Add them to an array
	 */
	while( $res		=	$mysql->get_row() )
	{
		
		$ids[]	=	$res[ 'id' ];
		
	}
	
	
	
	/*
	 * 	Put the story content together in an array
	 */
	$stories	=	array();
	foreach ( $ids as $id )
	{
	
		$story	=	new story ( $id );
		$temp	=	strtoupper( $story->get_headline() );
		if ( $story->get_subheadline() != '' )
		{
		
			$temp	.=	"\r\n\r\n" . $story->get_subheadline();
		
		}
		
		
		/*
		 * 	Tidy up the content to remove HTML code
		 */
		$revision	=	$story->get_latest_revision();
		$revision	=	$revision->get_content();
		$revision	=	str_replace( array( "\t", "&#8239;" ), "", $revision );
		$revision	=	str_replace( array( "&lsquo;", "&rsquo;" ), "'", $revision );
		$revision	=	str_replace( array( "&rdquo;", "&ldquo;" ), '"', $revision );
		$revision	=	str_replace( "&ndash;", "-", $revision );
		$revision	=	html_entity_decode( $revision, ENT_QUOTES );
		$revision	=	htmlspecialchars_decode( $revision, ENT_QUOTES );
		
		$temp	.=	"\r\n\r\n" . strip_tags( $revision );
		
		$link	=	STORIES_make_story_link( $story->get_headline() );
		$link	=	"http://www.schnews.org.uk/stories/" . $link;
		$temp	.=	"\r\n\r\n" . $link;
		
		$stories[]	=	$temp;
		
	}
	
	
	
	/*
	 * 	Put it together and end
	 */
	$divider	=	"\r\n\r\n\r\n========================================\r\n\r\n\r\n";
	return implode( $divider, $stories );
	
}




