<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	11th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Functions for manipulating keywords
 */



/*
 * 	Returns an alphabeticsed array list of the keyword names and IDs
 */
function KEYWORDS_get_all_keywords()
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id, keyword
					FROM keywords
					ORDER BY keyword ASC ";
	$mysql->query( $sql );
	
	if ( $mysql->get_num_rows() == 0 ) return false;
	
	$output		=	array();
	while ( $res	=	$mysql->get_row() )
	{

		$output[ $res['id'] ] 	=	$res['keyword'];
		
	}
	
	return $output;
	
}




/*
 * Returns the IDs of all keywords that have been selected for a given story
 */
function KEYWORDS_get_keywords_for_story( $id )
{
	
	/*
	 * Test inputs
	 */	
	if ( !CORE_is_number( $id ) ) return false;
	
	
	
	/*
	 * Query Database
	 */
	$mysql		=	new mysql_connection();
	$sql		=	" 	SELECT keyword
						FROM keyword_mappings
						WHERE story = " . $id;
	$mysql->query( $sql );
	
	
	
	if ( $mysql->get_num_rows() == 0 ) return array();
	
	
	
	$output	=	array();
	while ( $res	=	$mysql->get_row() )
	{
		
		$output[]	=	$res['keyword'];
		
	}
	
	return $output;
	
}




/*
 * Removes all keywords for a give story.
 */
function KEYWORDS_remove_keywords_from_story( $id )
{

	/*
	 * Test inputs
	 */	
	if ( !CORE_is_number( $id ) ) return false;
	
	
	
	/*
	 * Query Database
	 */
	$mysql		=	new mysql_connection();
	$sql		=	"	DELETE FROM keyword_mappings
						WHERE story = " . $id;
	$mysql->query( $sql );
	
	return true;
	
}





/*
 *	Adds the list of keywords to the given story. 
 */
function KEYWORDS_add_keyword_selection( $keywords, $story_id )
{
	
	/*
	 * Test inputs
	 */
	if ( !is_array( $keywords ) || count( $keywords ) == 0 ) return false;
	if ( !CORE_is_number( $story_id ) ) return false;
	
	if ( DEBUG )	var_dump( $keywords );
	
	
	
	/*
	 *	Cycle through each keyword, adding it to the database 
	 */
	$mysql	=	new mysql_connection();
	foreach ( $keywords as $keyword )
	{

		//if ( !CORE_is_number( $keyword ) ) continue;
		
		$sql	=	"	INSERT INTO keyword_mappings
						( keyword, story )
						VALUES
						( $keyword, $story_id )";
		$mysql->query( $sql );
		
	}
	
	return true;
	
}