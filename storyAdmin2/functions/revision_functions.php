<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Functions for manipulating revisions
 * 
 */






/*
 * 	Write a temp text file containing the revision text.
 * 
 * 	@param string $text				
 * 	@param int $story_id
 * 	@param int $user_id
 * 	@return bool
 */
function REVISIONS_store_temp_revision_text( $text, $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !is_string( $text ) ) 
	{

		$message	=	"Input text not full sting in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * 	Write file to filesystem
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'revisions' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	$content	=	DATETIME_make_sql_datetime() . "\r\n" . $text;
	if (!file_put_contents( $filename, $content ) )
	{
		
		$message 	=	"Could not write temp revision file. Filename = $filename";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		
	}
	
	return true;
	
}










function REVISIONS_get_temp_revision( $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * 	Make sure the file exists
	 */
	if ( !REVISIONS_does_temp_revision_exist($story_id, $user_id ) )
	{
		
		return false;
		
	}
	
	
	
	/*
	 * 	Get the file
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'revisions' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	$file		=	file_get_contents( $filename );
	
	$bits		=	explode ( "\r\n", $file );
	$datetime	=	$bits[ 0 ];
	$text		=	str_replace( $datetime . "\r\n", '', $file );

	return array( $datetime, $text );	
	
}










function REVISIONS_does_temp_revision_exist( $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * 	Test for file existence
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'revisions' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	if ( file_exists( $filename ) )
	{
		
		return true;
		
	}
	else
	{
		
		$message		=	"Temp revision file does not exist. U = $user_id, S = $story_id.";
		debug ( $message, __FILE__, __LINE__ );
		return false;
		
	}
	
}











function REVISIONS_store_temp_internal_comments( $int, $ext, $story_id, $user_id )
{
	
/*
	 * 	Test inputs
	 */	
	if ( !is_string( $int ) ) 
	{

		$message	=	"Input int not sting in REVISIONS_store_temp_internal_comments()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !is_string( $ext ) ) 
	{

		$message	=	"Input ext not sting in REVISIONS_store_temp_internal_comments()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}

	
	
	/*
	 * 	Write to filesystem
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'internal_comments' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	$div		=	"<***DIVISION MARKER***>";
	$content	=	DATETIME_make_sql_datetime().$div.$int.$div.$ext;
	if (!file_put_contents( $filename, $content ) )
	{
		
		$message 	=	"Could not write temp internal_comments file. Filename = $filename";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		
	}
	
	return true;
	
}












function REVISIONS_does_temp_internal_comment_exist( $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * 	Test for file existence
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'internal_comments' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	if ( file_exists( $filename ) )
	{
		
		return true;
		
	}
	else
	{
		
		$message		=	"Temp internal_comments file does not exist. U = $user_id, S = $story_id.";
		debug ( $message, __FILE__, __LINE__ );
		return false;
		
	}
	
}










function REVISIONS_get_temp_internal_comments( $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * 	Make sure the file exists
	 */
	if ( !REVISIONS_does_temp_internal_comment_exist($story_id, $user_id ) )
	{
		
		return false;
		
	}
	
	
	
	/*
	 * 	Get the file
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'internal_comments' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	$file		=	file_get_contents( $filename );
	
	$bits		=	explode ( "<***DIVISION MARKER***>", $file );

	return $bits;	
	
}











/*
 * 	Removes the temp files for a given user and story combo
 */
function REVISIONS_remove_temp_files( $story_id, $user_id )
{
	
	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	if ( !CORE_is_number( $user_id ) )
	{
		
		$message	=	"Input user_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	
	/*
	 * Remove internal comments temp file
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'internal_comments' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	if ( file_exists( $filename))
	{
		
		unlink( $filename );
		
	}
	
	
	
/*
	 * Remove internal comments temp file
	 */
	$filename	=	SYSTEM_TEMP_DIR . 'revisions' . DIRECTORY_SEPARATOR . $story_id . '_' . $user_id;
	if ( file_exists( $filename))
	{
		
		unlink( $filename );
		
	}
	
	return true;
	
}
	










function REVISIONS_get_number_of_revisions( $story_id )
{

	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in STORIES_store_temp_revision_text()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT id
						FROM revisions
						WHERE story_id = $story_id ";
	$mysql->query( $sql );
	return $mysql->get_num_rows();

};

function REVISIONS_get_number_of_revision_authors( $story_id )
{

	/*
	 * 	Test inputs
	 */	
	if ( !CORE_is_number( $story_id ) )
	{
		
		$message	=	"Input story_id is not a number in REVISIONS_get_number_of_revision_authors()";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message, 'error', 2 );
		return false;
		
	}
	
	
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT DISTINCT author
						FROM revisions
						WHERE story_id = " . $story_id;
	$mysql->query( $sql );
	return $mysql->get_num_rows();

}






function REVISIONS_work_out_word_count( $text )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_string( $text ) ) return false;
	
	
	$text		=	str_replace( array( "\r", "\n" ), ' ', $text );
	$text		=	str_replace( "  ", " ", $text );
	
	$words		=	explode( " ", $text );
	
	foreach ( $words as $key => $value )
	{
		
		if ( trim( $value ) == '' )
		{
			unset ( $words[ $key ] );
		}
		
	}
	
	return count( $words );	
	
}


	



	
