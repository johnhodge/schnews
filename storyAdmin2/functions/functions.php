<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	11th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script controls all the function files.
 *
 * 	It basically loads the other function scripts.
 */



/*
 * 	Get Functions
 */
require "functions/login_functions.php";
require "functions/logging_functions.php";
require "functions/settings_functions.php";
require "functions/story_functions.php";
require "functions/issue_functions.php";
require "functions/revision_functions.php";
require "functions/garbage_collection_functions.php";
require "functions/internal_formatting_functions.php";
require "functions/keywords_functions.php";
require "functions/date_time_functions.php";



/*
 * 	Get Classes
 */
require "classes/user_class.php";
require "classes/revision_class.php";
require "classes/story_class.php";
require "classes/issue_class.php";
require "classes/keyword_class.php";
require "classes/comments_class.php";
require "classes/homepage_misc_fields.php";

