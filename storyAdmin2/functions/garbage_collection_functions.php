<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	31th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Functions for tidying up stray temp files (and incedentelly testing checkout timeouts)
 */




function GC_collect_garbage()
{
	
	/*
	 * 	READ ME
	 * 	11-01-12 
	 * 	Andrew Winterbottom 
	 * 	support@andrewwinterbottom.com
	 * 
	 * 	There is a bug somewhere in this set of functions, that only seems
	 * 	to manifest when run on Knightsbridge's server.
	 * 
	 * 	Until this is fixed this funtion is disabled - it will just return
	 * 	null right away.
	 */
	
	
	
	
	
	debug ( "GC Started", __FILE__, __LINE__ );
	
	if ( !isset( $_SESSION['logged_in'.SESSION_APPEND] ) ) return false;
	if ( !defined( 'checkout_timeout' ) ) return false;
	
	
	/*
	 * 	Select all currently checked-out stories
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT id
					FROM stories
					WHERE checked_out <> 0 ";
	$mysql->query( $sql );
	
	debug ( "There are " . $mysql->get_num_rows() . " stories checked out", __FILE__, __LINE__ );
	
	
	
	/*
	 *	Test each story to see if the timeout is exceeded 
	 */
	while ( $res 	=	$mysql->get_row() )
	{

		$story	=	new story( $res['id'] );
		if ( GC_is_checkout_timedout( $story ) )
		{
			
			GC_do_emergency_checkin( $story->get_id() );
			
		}
		
	}
		
	
}






function GC_is_checkout_timedout( $story )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_object( $story ) ) return false;
	
	

	/*
	 * 	Work out how old the checkout is
	 */
	$co_datetime	=	new date_time( $story->get_checkout_last_activity() );
	$now_datetime	=	new date_time();
	$diff			=	$now_datetime->get_timestamp() - $co_datetime->get_timestamp();
	
	
	
	/*
	 * 	Test whether it is too old
	 */
	$timeout		=	checkout_timeout * 60;
	if ( $diff > $timeout )
	{
		
		$message	=	"CO timeout exceeded for story id " . $story->get_id();
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message );
		return true;
		
	}
	else
	{
		
		return false;
		
	}
	
}





function GC_do_emergency_checkin( $story_id, $force_discard = false )
{
	
	//	Test inputs
	if ( !CORE_is_number( $story_id ) ) return false;
	if ( !is_bool( $force_discard ) ) $force_discard = false;
		
	
	
	/*
	 * New story object
	 */
	$story		=	new story( $story_id );
	$user		=	new user( $story->get_checked_out() );		
	
	
	/*
	 * Commit the changes or not
	 */
	$commit		=	false;
	if ( checkout_timeout_save && !$force_discard )
	{
		$commit		=	true;
	}
	
	
	
	/*
	 * 	Save the revision
	 */
	if ( $commit )
	{

		/*
		 * Get the revision data
		 */
		$rev_data	=	REVISIONS_get_temp_revision( $story->get_id(), $story->get_checked_out() );
		$com_data	=	REVISIONS_get_temp_internal_comments( $story->get_id(), $story->get_checked_out() );		
		
		/*
		 * Add data to object
		 */
		$revision	=	new revision();
		$revision->set_story_id( $story_id );
		$revision->set_content( $rev_data[1] );
		$revision->set_author( $story->get_checked_out() );
		$revision->set_internal_comment( $com_data[0] );
		$revision->set_external_comment( $com_data[1] );
		$revision->set_date_saved( DATETIME_make_sql_datetime( ) );
		$revision->set_clean_checkout( false );
		$revision->set_word_count( REVISIONS_work_out_word_count( $rev_data[1] ) );
		
		/*
		 * Add to database
		 */
		$revision->add_revision_to_database();
		
	}

	
	/*
	 * Tidy up database and file system
	 */
	REVISIONS_remove_temp_files( $story_id, $story->get_checked_out() );
	$story->checkin( false, true );
	
	
	
	/*
	 * Logging an debugging
	 */
	$message 	=	"Emergency checkin performed on story '" . $story->get_headline() . "'. User '" . $user->get_name() . "' was responsible.";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message );
	
	
	
	/*
	 * 	Email to Mailing List
	 */
	$subject 	=	"SchNEWS StoryAdmin :: An Emergency Story Checkin has Occurred";
	$message	=	"An emergency checkin of the story '" . $story->get_headline() . "' has occurred, as no activity has occure for more than " . checkout_timeout . " minutes.

The user resonsible is " . $user->get_name() . ", who can be contacted at " . $user->get_email();
	
	if ( $commit )
	{
		
		$message 	.=	"\r\n\r\nThe changes to the story have been saved";
		
	}
	else 
	{

		$message	.=	"\r\n\r\nThe changes have been discarded";
		
	}
	$email		=	new email( $message, $subject, internal_mailing_list_address );
	$email->send();
	
	
	
	/*
	 * Email the revision author
	 */
	$subject 	=	"SchNEWS StoryAdmin :: A Story You Were Editing Has Been Unlocked Due to Your Inactivity";
	$message	=	"Hello " . $user->get_name() . ",
	
You have been editing the story '" . $story->get_headline() . "', and had the story locked for editing, but have not yet saved your changes.

However, I have seen no activity on your part for at least " . checkout_timeout . " minutes, and so I have to assume you are no longer editing the story.

For this reason the story has been automatically unlocked, so other users can access it.";
	if ( $commit )
	{
		
		$message	.=	"\r\n\r\nYour changes have been saved.";
	
	}
	else
	{
		
		$message	.=	"\r\n\r\nYour changes have been discarded, in accordance with the system policy.";
	}
	$message	.=	"\r\n\r\n\r\nSchNEWS AI";
	$email		=	new email( $message, $subject, $user->get_email() );
	$email->send();

	return true;

}

