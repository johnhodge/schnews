<?php


function DATE_turn_sql_to_text( $datetime )
{
	
	$date	=	explode( ' ', $datetime );
	$date	=	$date[0];
	$bits	=	explode( '-', $date );
	
	if ( count( $bits) != 3 ) return;
	
	//if ( strlen( $bits[2] ) == 1 ) $bits[2] 	=	'0' . $bits[2];
	//if ( strlen( $bits[1] ) == 1 ) $bits[1] 	=	'0' . $bits[1];
	
	$date	=	 $bits[2] . '-' . $bits[1] . '-' . $bits[0];
	
	return $date;
}


function DATE_turn_text_to_sql( $date )
{
	
	$bits	=	explode( "-", trim( $date ) );
	
	if( count( $bits ) != 3 )	return false;

	$datetime	=	 $bits[2] . '-' . $bits[1] . '-' . $bits[0] . ' 00:00:00';
	$date	=	new date_time( $datetime );
	if ( $date->is_sql_datetime( $datetime ) )
	{

		return $datetime;
		
	}
	
}