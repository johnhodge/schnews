<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script has a few basic login functions.
 */




/*
 * 	Tests whether the user is currently logged in or not
 *
 * 	@return: bool
 */
function LOGIN_is_user_logged_in()
{

	if (!isset($_SESSION['logged_in'.SESSION_APPEND]) || $_SESSION['logged_in'.SESSION_APPEND] == 0)
	{
		return false;
	}
	elseif ($_SESSION['logged_in'.SESSION_APPEND] > 0)
	{
		return true;
	}

	return false;

}



/*
 * 	Log the current user out.
 *
 * 	Doesn't matter if they weren't logged in to begin with
 */
function LOGIN_log_user_out()
{

	if (isset($_SESSION['logged_in_name'.SESSION_APPEND]))
	{
		$message	=	"User '".$_SESSION['logged_in_name'.SESSION_APPEND]."' has been logged out";
	}
	else
	{
		$message	=	"Unknown user has been logged out. Most likely a previous logout was F5 refreshed by the user";
	}
	LOG_record_entry($message);

	
	
	/*
	 * Remove user login session data
	 */
	unset($_SESSION['logged_in'.SESSION_APPEND]);
	unset($_SESSION['logged_in_name'.SESSION_APPEND]);
	unset($_SESSION['logged_in_id'.SESSION_APPEND]);
	unset($_SESSION['logged_in_username'.SESSION_APPEND]);
	unset($_SESSION['logged_in_last_login'.SESSION_APPEND]);
	
	
	
	/*
	 * Remove messaging session data
	 */
	unset( $_SESSION['header_message_temp'] );
	unset( $_SESSION['header_message_perm'] );

	return true;

}



/*
 * 	Tests whether a given username and password combo constitute a successful login
 *
 * 	@param username			string
 * 	@param password			string
 *
 * 	@return 				bool
 */
function LOGIN_test_login($username, $password)
{

	LOG_record_entry("Login attempted, username = '$username'" );

	$errors		=	array();

	//	Test inputs
	if (!is_string($username) || trim($username) == '')
	{

		$errors[]	=	"Username is emtpy";

	}
	if (!is_string($password) || trim($password) == '')
	{

		$errors[]	=	"Password is emtpy";

	}


	//	If there are any error then bail
	if (count($errors) != 0)
	{

		return $errors;

	}


	//	Prepare SQL
	$mysql			=	new mysql_connection();
	$username		=	$mysql->clean_string($username);
	$password_hash	=	md5($password);

	$sql		=	" 	SELECT id
						FROM users
						WHERE username = '$username' AND password = '$password_hash' ";
	$mysql->query($sql);


	if ($mysql->get_num_rows() == 0)
	{
		debug ($sql);
		$errors[]	=	"Either the Username or the Password were wrong";
		return $errors;
	}


	$result 	=	$mysql->get_row();
	return $result['id'];

}



/*
 * 	Removes the specified user from the database.
 */
function LOGIN_delete_user( $user_id )
{

	//	Test inputs
	if ( !CORE_is_number( $user_id ) || $user_id == 0 )
	{

		debug ( "Delete user attempt with empty or false User_id" );
		return false;

	}



	//	Instantiate new user
	$user 		=	new user ( $user_id );
	debug ( "Deleting user " . $user->get_name() );



	LOG_record_entry( "User being deleted, username = " . $user->get_name() . ". Deleted by " . $_SESSION['logged_in_name'.SESSION_APPEND] , 'security' );



	//	delete user from DB
	$mysql		=	new mysql_connection();
	$sql		=	"	SELECT id
						FROM users
						WHERE id = $user_id ";
	$mysql->query( $sql );
	if ( $mysql->get_num_rows() != 1 )
	{

		$message 	=	" User being deleted does not exist in database || Userid = " . $user_id . " || User doing deleting = " . $_SESSION['logged_in_name'.SESSION_APPEND];
		debug ( $message, __FILE__, __LINE );

		LOG_record_entry( $message, 'security', 1 );

	}


	$sql		=	" 	DELETE FROM users
						WHERE id = $user_id ";
	$mysql->query( $sql );

	return true;

}



/*
 * 	Tests whether the name or username are taken by another user, other than that user specified
 * 	by the id.
 *
 * 	Used to make sure a give user is not renamed in such a way as to clash with another user.
 */
function LOGIN_does_user_exist_already($username, $name, $id)
{

	//	Test inputs
	if ( !is_string( $username ) || trim( $username ) == '' ) return false;
	if ( !is_string( $name ) || trim( $name ) == '' ) return false;
	if ( !CORE_is_number( $id ) ) return false;

 	$mysql	=	new mysql_connection();

 	//	Clean inputs
 	$name		=	$mysql->clean_string( $name );
 	$username	=	$mysql->clean_string( $username );

 	$sql		=	" 	SELECT id
 						FROM users
 						WHERE
 							(
 									name = '$name'
 								OR	username = '$username'
 							)
 							AND
 							id <> $id ";
 	$mysql->query( $sql );
 	if ( $mysql->get_num_rows() != 0 )
 	{

 		LOG_record_entry( "Attempt to edit/add user with already existing name or username. Name = '$name'. Username = '$username'." );
 		return true;

 	}

 	unset( $mysql );
 	return false;

}

?>