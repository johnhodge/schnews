<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 17-06-11
 *
 */


/*
 * 	This file contains functions for processing and interpreting URLs and Mailto links
 */




/*
 * 	Converts the $_GET superglobal into a normal array
 *
 * 	@return array
 */
function URL_get_GET_vars()
{

	$output		=	array();

	foreach ($_GET as $key => $value)
	{

		$output[$key]	=	$value;

	}

	return $output;

}



/*
 * Turns the $_GET superglobal (or any other assicative array into a URL string - i.e. ?key1=value1&key2=value2
 *
 * @param $get array
 * @return string
 */
function URL_make_url_from_get($get)
{

	//	Test inputs
	if (!is_array($get) || count($get) == 0)	return false;

	$pairs	=	array();
	foreach ($get as $key => $value)
	{
		$pairs[]	=	$key.'='.$value;
	}
	return '?'.implode('&', $pairs);

}



/*
 * 	Tets whether the input is a valid email addy
 *
 * 	@param email string
 * 	@return bool
 */
function URL_is_email($email)
{

	//	Test inputs
	if (!is_string($email) || trim($email) == '') return false;



	//	Does it contain only on @ glyph
	$temp		=	str_replace('@', '', $email);
	if (strlen($temp) != (strlen($email) - 1))
	{

		debug ("Email '$email' does not contain only 1 @ glyph", __FILE__ , __LINE__);
		return false;

	}



	//	Does it contain at least one . full stop glypgh
	$temp		=	str_replace('.', '', $email);
	if (strlen($temp) == strlen($email))
	{

		debug ("Email '$email' does not contain any . glyphs", __FILE__ , __LINE__);
		return false;

	}



	return true;


}





/*
 * 	Converts the input string to an HTTP link, with an optional TARGET parameter
 * 
 * @param $url string
 * @param $display_text string 
 * @param $target string
 * @return string
 */
function URL_make_http_link( $url, $display_text = '', $target = '' )
{
	
	/*
	 * Test inputs
	 */
	if ( !is_string( $url ) || trim( $url ) == '' ) return false;
	if ( $target != "_BLANK" ) $target = '';	
	if ( !is_string( $display_text ) ) $display_text = '';
	
	
	if ( $target != '' )
	{
		$target	=	" target=\"$target\" ";	
	}
	if ( $display_text == '' )
	{
		$display_text	=	$url;
	}
	$link		=	"<a href='$url' $target >$display_text</a>";	
	
	
	
	return $link;
	
}





/*
 * 	Converts the input address to a mailto link
 * 
 * 	@param $addr string
 * 	@param $display_text string
 * 	@return string
 */
function URL_make_mailto_link( $addr, $display_text = '' )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_string( $addr ) || trim( $addr ) == '' ) return false;
	if ( !is_string( $display_text ) ) $display_text = '';
	
	
	
	if ( $display_text == '' )
	{
		$display_text 	=	$addr;	
	}
	$link		=	"<a href='mailto:$addr'>$display_text</a>";
	
	
	
	return $link;

}