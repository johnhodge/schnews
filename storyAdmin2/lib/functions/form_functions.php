<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 29-10-11
 *
 */


/*
 * 	This file contains fucntions that manipulate data fro HTML forms
 */



function FORM_form_encode( $string )
{
	
	/*
	 * 	Test inputs
	 */
	if ( !is_string( $string ) ) return false;
	
	
	$string		=	str_replace( "'", "&apos;", $string );
	$string 	=	str_replace( '"', "&quot;", $string );
	
	return $string;	
	
}
