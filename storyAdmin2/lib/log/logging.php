<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-05-11
 *
 */

class log_entry
{
	protected $log_file_path;
	protected $log_file_handle;
	protected $most_recent_log_entry;

	function __construct($log_file_path = false)
	{
		//	Work out what the path to the log file is....
		if (is_string($log_file_path) && trim($log_file_path) != '')
		{
			$this->log_file_path		=	$log_file_path;
		}
		elseif (LOGFILE_PATH != false && trim(LOGFILE_PATH) != '')
		{
			$this->log_file_path 		= 	LOGFILE_PATH;
		}
		else
		{
			$this->log_file_path 		=	LIB_ROOT.DIRECTORY_SEPARATOR.'log'.DIRECTORY_SEPARATOR.'log.log';
		}

		/*
		 * Open the log file
		 *
		 * 	This is abstracted to a separate method to aid inheretence for oterh class. This class can then be used as a parent template
		 */
		$this->open_log_file();

	}



	/*
	 * Simply opens a handle to the log file
	 *
	 * 	Abstracted to a separate method to aid inheretence of future classes
	 */
	protected function open_log_file()
	{
		$this->log_file_handle 	=	fopen($this->log_file_path, 'a');
	}



	/*
	 * Adds an entry to the log file.
	 *
	 * @param string $message			- The log entry to add
	 * @param int $status				- 0 = information / 1 = security / 2 = error
	 * @param bool $email				- Whether to email the message to the designated admin (if specified in the LOG/CONF file)
	 */
	public function add_log_entry($message, $status = 0, $email = false)
	{
		// Test inputs
		if (!is_string($message) || trim($message) == '') return false;
		if ($status != 0 && $status != 1 && $status != 2) $status = 0;
		if (!is_bool($email)) $email = false;

		$this->most_recent_log_entry	=	$this->make_log_entry($message, $status);

		if (!fwrite($this->log_file_handle, $this->most_recent_log_entry))
		{
			debug("Couldn't write log entry \"$message\"", __FILE__, __LINE__);
			return false;
		}

		if ($email)
		{
			if (!$this->send_email())
			{
				debug("Couldn't send loggin email", __FILE__, __LINE__);
			}
		}

	}








	/*
	 * Generates the error message string
	 *
	 * 	Abstracted to a separate method to aid inheretence
	 *
	 * @param string $message			- The log entry to add
	 * @param int $status				- 0 = information / 1 = security / 2 = error
	 */
	protected function make_log_entry($message, $status)
	{
		$date	= 	DATETIME_make_sql_datetime();
		$ip 	=	$_SERVER['REMOTE_ADDR'];
		if (LOGFILE_HASH_IP)
		{
			$ip 	=	md5($ip);
		}
		$uri 	=	$_SERVER['REQUEST_URI'];
		$message 	=	str_replace(array("\r", "\n", ";"), " ", htmlentities($message));
		switch ($status)
		{
			default:
				$status = 'Information';
				break;

			case 1:
				$status = 'Security';
				break;

			case 2:
				$status = 'Error';
				break;
		}

		return $date.";".$ip.";".$status.";".$uri.";".$message."\r\n";
	}








	protected function send_email()
	{
		if (LOGFILE_EMAIL_ADDRESS == '' || !LOGFILE_EMAIL_ADDRESS) return false;

		$lines	=	explode("\t;\t", $this->most_recent_log_entry);
		$message = implode("\r\n\r\n", $lines);
		$headers = 		'From: '.LOGFILE_SEND_ADDRESS . "\r\n" .
    					'Reply-To: '.LOGFILE_SEND_ADDRESS . "\r\n" .
    					'X-Mailer: PHP/' . phpversion();


		if (!mail(LOGFILE_EMAIL_ADDRESS, SITE_NAME.' Log Entry Notification', $message, $headers))
		{
			return false;
		}
		return true;
	}

}







