<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 16-06-11
 *
 */



class date_time
{

	protected $timestamp;


	function __construct($timestamp = false)
	{

		if ($timestamp === false)
		{
			$this->timestamp 	=	date('U');
			return true;
		}

		if ($this->is_sql_datetime($timestamp))
		{
			$this->timestamp = $this->get_timestamp_from_datetime($timestamp);
		}

	//	End of constructor
	}



	/*
	 * 	Test whether a string is an SQL datetime
	 *
	 * 	@param datetime			string
	 *
	 * 	@return bool
	 */
	public function is_sql_datetime($datetime)
	{

		//	Test inputs
		if (!is_string($datetime) || trim($datetime) == '') return false;



		//	Test that there are 2 hyphens
		$datetime_processed 	=	str_replace("-", '', $datetime);
		if (strlen($datetime) != (strlen($datetime_processed) + 2) )
		{
			debug ("Datetime does not contain 2 hyphens. Datetime = '".$datetime."'", __FILE__, __LINE__);
			return false;
		}



		//	Test that there are 2 colons
		$datetime_processed 	=	str_replace(":", '', $datetime);
		if (strlen($datetime) != (strlen($datetime_processed) + 2) )
		{
			debug ("Datetime does not contain 2 colons. Datetime = '".$datetime."'", __FILE__, __LINE__);
			return false;
		}


		// Test that there are 14 numbers
		$datetime_processed 	=	str_replace(array('-', ':', ' '), '', $datetime);
		if (strlen($datetime_processed) != 14)
		{
			debug("There are not 14 numeric digits in the datetime. Datetime = '".$datetime."'", __FILE__, __LINE__);
			return false;
		}

		return true;

	//	End of is_sql_datetime function
	}



	/*
	 * 	Converts an SQL datetime to a Unix timestamp
	 *
	 * 	@param datetime string
	 *
	 * 	@return int
	 */
	public function get_timestamp_from_datetime($datetime)
	{

		//	Test inputs
		if (!is_string($datetime) || trim($datetime) == '' || !$this->is_sql_datetime($datetime)) return false;



		//	Split the datetime into date and time components
		$bits		=	explode(" ", $datetime);
		$date		=	$bits[0];
		$time		=	$bits[1];



		//	Process Date
		$bits		=	explode("-", $date);
		$year		=	$bits[0];
		$month		=	$bits[1];
		$day		=	$bits[2];



		//	Process Time
		$bits		=	explode(':', $time);
		$hour		=	$bits[0];
		$minute		=	$bits[1];
		$seconds	=	$bits[2];



		//	Make timstamp
		$timestamp	=	mktime($hour, $minute, $seconds, $month, $day, $year);
		return $timestamp;

	//	End of get_timestamp_from_datetime
	}



	/*
	 * 	Create a human readable date string
	 *
	 * 	@param mode string				:: Options are 'words' = 1st January 2011, 'numbers' = 01-01-2011, 'words_short' = 1 Jan 2011
	 * 	@param format string			:: Options are 'uk' = 21-05-2011, 'us' = 05-21-2011
	 */
	public function make_human_date($mode = 'words', $format = 'uk')
	{
		switch ($format)
		{

			//	UK Format
			default:
				switch ($mode)
				{

					//	Full text - i.e. 1st January 2011
					default:
						return date("jS F Y", $this->timestamp);
						break;

					//	Numbers only - i.e. 01-01-2011
					case 'numbers':
						return date("d-m-Y", $this->timestamp);
						break;

					//	Short textual - i.e. 1 Jan 2011
					case 'words_short':
						return date("j M Y", $this->timestamp);
						break;

				}


			//	US Format
			case 'us':
				switch ($mode)
				{

					//	Full text - i.e. 1st January 2011
					default:
						return date("jS F Y", $this->timestamp);
						break;

					//	Numbers only - i.e. 01-01-2011
					case 'numbers':
						return date("m-d-Y", $this->timestamp);
						break;

					//	Short textual - i.e. 1 Jan 2011
					case 'words_short':
						return date("j M Y", $this->timestamp);
						break;

				}

		}

	//	End of make_human_date function
	}



	/*
	 * 	Create a human readable time string
	 *
	 * 	@param format string			:: Options are '24' = 14:33, '12' = 2:33 PM
	 */
	public function make_human_time($format = '12')
	{

		switch ($format)
		{

			//	12 Hour format
			default:
				return date("g:i A", $this->timestamp);
				break;

			//	24 Hour format
			case '24':
				return date("H:i", $this->timestamp);
				break;

		}

	//	End of make_human_time function
	}
	
	
	
	
	
	
	
	
	
	
	/*
	 * ========================================================
	 * 						GETTERS
	 */
	public function get_timestamp()
	{
		return $this->timestamp;
	}
	
	


// End of Class
}
















