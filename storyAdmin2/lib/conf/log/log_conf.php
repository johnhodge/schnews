<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-05-11
 *
 */


/*
 * Log file path
 *  	Leave this empty/false to use the default (aw_lib/log/log.log)
 *  	This is an absolute path on the server's filesystem, or it can be relative to the script that has called the library
 *
 *  	NOTE: It is recommended to set this path as a non-public path - i.e. a path neither served by the webserver or with full 777 mods on the server (it is possible that SQL data could end up in here if you're not careful about what you log)
 *  	NOTE: Take care to escape any backslashes on a Windows path (e.g. it should be c:\\path)
 */
define ("LOGFILE_PATH", 'logs'.DIRECTORY_SEPARATOR);


/*
 * Store IP addresses as an MD5 hash?
 */
define ("LOGFILE_HASH_IP", true);


/*
 * Where to send emails in case an log entyr is deemed necessary to notifyu by email (via the EMAIL param of the ADD_LOG_ENTRY method)
 */
//define ("LOGFILE_EMAIL_ADDRESS", 'support@andrewwinterbottom.com');
define ("LOGFILE_EMAIL_ADDRESS", 'rollacoast@gmail.com');


/*
 * Which address to use to send the logfile emails from.
 *
 * Can be left blank (however it looks untidy, and may cause the emails to be spam filtered).
 */
define ('LOGFILE_SEND_ADDRESS', 'no-reply@schnews.org.uk');
