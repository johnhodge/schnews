<?php
/*
 * Basic Function Framework
 * (c) Andrew Winterbottom 2011
 *
 * @auth Andrew Winterbottom
 * @support support@andrewwinterbottom.com
 * @license GPL v2
 * @last_mod 13-08-11
 *
 */




/*
 * 	Default From address
 * 		To be used if non is specified when isntantiating an email_conf object
 */
define ( 'EMAIL_DEFAULT_FROM', 'no-reply@schnews.org.uk' );