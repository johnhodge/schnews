<h1>Story Locking</h1>
<p>When you elect to edit a story it will become locked. This means that other <a href='?show=Users'>users</a> will see that you are editing the story and will be unable to edit it themselves until you are finished</p>
<p>While you have the story locked you have control of it, blocking everyone else.</p>
<p>Once you have finished editing the story, and you have saved your changes, it is unlocked, at which point other people see your changes and they can edit it themsevles.</p>
<br /><br />
<p><b>NOTE:</b> There is a timeout on the locking process. If you have a story locked and you show no activity for <b><?php echo checkout_timeout; ?></b> minutes, then you will be kicked out, the story unlocked, and you'll be logged out.</p>
<p>This is necessary in case a user locks a story, and never finished editing it</p>