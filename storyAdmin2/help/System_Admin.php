<?php 

	require "../conf/conf.php";

?>

<h1>System Admin</h1>
<p>The System Admin is the person who administers this software, who monitors any security issues, or follows up any bugs or errors. The System Admin is not involved in any issues around the editorial process ro the content of stories.</p>
<p>The current System Admin is <b><?php echo SYSTEM_ADMIN_NAME; ?></b> who can be contacted at <?php echo "<a href='mailto:" . SYSTEM_ADMIN_EMAIL . "'><b>" . SYSTEM_ADMIN_EMAIL . "</b></a>"; ?></p>