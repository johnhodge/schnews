<h1>RSS (Really Simple Syndication)</h1>

<p>A way of distributing news or notices automatically using an XML feed.</p>
<p>The public subsribe to the RSS feed via a browser or email client, and whenever the news feed is updated the subscriber is automatically notified of the news.</p>
<p>Each SchNEWS story is added to our RSS feed, so subscribers will get notified as soon as the story is live.</p>
<p>When adding a story you will be asked for a small bit of text to include in the RSS entry (the story title and publication date will be enter into the RSS feed automatically). This would typically be a description of the story, about a paragraph in length, designed to entice the viewer to come the site and read the full story.</p>