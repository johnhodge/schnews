<h1>Revisions of Stories</h1>
<p>When you edit a story your changes are stored as a new revision. Every time a story is edited a new revision is stored (when it is <a href='?show=Story_Locking'>unlocked</a>).</p>
<p>This means that the system has a revison history for the story, and can for example revert back to a previous revision if new changes are deemed problematic.</p>