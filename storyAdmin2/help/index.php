<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Help System index
 *
 */

require "../functions/settings_functions.php";
require "../lib/aw_lib.php";
SETTINGS_load_story_settings();


/*
 * 	Test which mode we're in
 */
$mode 	=	'index';
if( isset( $_GET['show'] ) && trim( $_GET['show'] ) != '' )
{
	
	$mode	=	'show';
	$show	=	$_GET['show'];
	
}




/*
 * Drawn HTML Headers
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>SchNEWS StoryAdmin Help System</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
</head>
<body>
<?php 



switch ( $mode )
{
	
	default:
	
		/*
		 * 	Get list of entries by scanning filesystem
		 */
		$dir_handle		=	opendir( "./" );
		$entries		=	array();
		while ( $filename = readdir( $dir_handle ) )
		{
			
			if ( $filename == '.' || $filename == '..' || $filename == 'index.php' ) continue;
			$entries[]	=	str_replace( '.php', '', $filename );			
			
		}
		
		?>
		<h1> SchNEWS StoryAdmin Help Index</h1>
		<h2>List of Entries:</h2>
		<ul>
			<?php
			
			sort( $entries );
			foreach ( $entries as $entry )
			{
				
				echo "<li><a href='?show=$entry'>" . str_replace( "_", " ", $entry ) . "</a></li>";
				
			}
			
			?>
		</ul>
		<?php
		break;		
		
case 'show':
		if ( file_exists( $show . '.php' ) )
		{
			
			require $show . '.php';
			
		}
		
		echo "\r\n\r\n\r\n<br />\r\n<br />\r\n<br />\r\n<a href='index.php'>Back to Help Index</a>";

}
?>

</body>
</html>
