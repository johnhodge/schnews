<h1>Live Story Status</h1>
<p>Only <b>Live</b> stories are visible to the public on the site. If a story is not <b>Live</b> it will not be public at all.</p>
<p>In order for a story to be made live it must be <a href='?show=Voting_for_Stories'>voted</a> for by at least <?php echo number_story_votes_to_live; ?> <a href='?show=Users'>users</a>.</p>