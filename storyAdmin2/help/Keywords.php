<h1>Keywords</h1>
<p>Each story can be optionally assigned any number of keywords, from a pre-ordaned list.</p>
<p>This allows story about a particular subject to be grouped together. Viewers can see all stories for a given keyword (or combination thereof), and stories can link to other stories of a similar subject</p>
<p>Changes and additions to the keyword list will be subject to the vote or veto of other <a href='?show=Users'>users</a>.</p>