<h1>Users</h1>
<p>A user is someone who has login access to this StoryAdmin system.</p>
<p>You are a user.</p>
<p>As far as this system is concerned a user is <b><u>NOT</u></b> a member of the public viewing the site. A user is an editor with ligin access who can edit stories - someone who uses this system</p>

<br /><br />

<p>A User can come in two types:</p>

<h2>Admin / Editor</h2>
<p>An Admin can edit all stories, including other people's. All stories will require at least 1 vote from an Admin to be made <a href='?Live_Story_Status'>live</a>. Only Admins can edit <a href='?Live_Story_Status'>live</a> stories</p>
<p>Admins can modify other User's account info, and can modify the settings for this editing software.</p>

<h2>Guest / Basic User</h2>
<p>A Basic User can add new stories, and can edit their own stories.</p>
<p>Their stories will need at least on Admin vote before they can go <a href='?Live_Story_Status'>live</a>, so Basic User's stories cannot be made visible to the public without some level of admin involvement.</p>
<p>Basic Users can vote for any stories (except their own).</p>