<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	21st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	A class to manipulate a revision
 * 
 */


class revision
{
	
	//	SQL Data
	protected $id;
	protected $story_id;
	protected $revision;
	
	//	Content
	protected $content;
	protected $author;
	
	//	Meta Data
	protected $date_saved;
	protected $clean_checkout;
	protected $word_count;
	
	//	Comments
	protected $internal_comment;
	protected $external_comment;
	
	
	
	function __construct( $id = false )
	{
		
		debug ( "New Revision object instantiated. ID = $id", __FILE__, __LINE__ );
		
		//	Test inputs
		if (!CORE_is_number( $id ) ) $id = false;
		$this->id = $id;
		
		
		
		if ( !$id )
		{
			
			$this->make_new_revision();
			
		}
		else
		{

			if ( !$this->get_revision_data() )
			{
				
				return false;
			
			}
		
		}
		
	}
	
	
	
	protected function get_revision_data()
	{
		
		//	Test assumptions
		if ( !CORE_is_number( $this->id ) )
		{
			
			/*
			 * If ID is not a number by this stage then we have a serious error
			 */
			$message	=	"story->get_revision_data() called without revision->id being properly set.";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT * FROM revisions WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		/*
		 * 	Make sure only 1 result was returned
		 */
		if ( $mysql->get_num_rows() != 1 && $this->id != '0')
		{

			/*
			 * 	If there is not 1 result then we have a pretty serious programming error or a hack attampt
			 */
			$message 	=	"storyadmin2 SQL query in revisions->get_story_data() did not yield 1 result. Query = " . $sql;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		/*
		 * 	Populate Local Properties
		 */
		$rev						=	$mysql->get_row();
		$this->revision				=	$rev;
		
		$this->story_id				=	$rev[ 'story_id' ];
		$this->content				=	$rev[ 'content' ];
		$this->author				=	$rev[ 'author' ] ;
		$this->date_saved			=	$rev[ 'date_saved' ];
		$this->clean_checkout		=	$rev[ 'clean_checkout' ];
		$this->word_count			=	$rev[ 'word_count' ];
		$this->internal_comment		=	$rev[ 'internal_comment' ];
		$this->external_comment		=	$rev[ 'external_comment' ];
		
		return true;
		
	}
	
	
	
	protected function make_new_revision()
	{
		
		/*
		 * 	Populate Local Properties
		 */
		$this->revision				=	array();
		
		$this->story_id				=	0;
		$this->content				=	'';
		$this->author				=	0;
		$this->date_saved			=	'00-00-00 00:00:00';
		$this->clean_checkout		=	1;
		$this->word_count			=	0;
		$this->internal_comment		=	'';
		$this->external_comment		=	'';
		
		return true;
		
	}
	
	
	
	
	public function add_revision_to_database()
	{

		debug ( "Add_Revision_To_database() started", __FILE__, __LINE__ );
		
		$mysql		=	new mysql_connection();
		
		
		
		/*
		 * Generate SQL string
		 */
		$values		=	array();
		$fields		=	array(
			'story_id',
			'content',
			'author',
			'date_saved',
			'clean_checkout',
			'word_count',
			'internal_comment',
			'external_comment'
		);
		
		foreach ( $fields as $field )
		{

			$values[]	=	"'" . $mysql->clean_string( $this->$field ) . "'";
			
		}
		
		$fields		=	implode( ", ", $fields );
		$values		=	implode( ", ", $values );
		
		$sql		=	" 	INSERT INTO revisions
								( $fields )
							VALUES
								( $values ) ";
		
		
		
		/*
		 * Do the SQL query
		 */
		$mysql->query( $sql );
		$this->id		=	$mysql->get_insert_id();
		
		
		
		/*
		 * Do some logging
		 */
		$message		=	"New revision added for story " . $this->id;
		debug ( $message . " :: $sql", __FILE__, __LINE__ );
		LOG_record_entry( $message );		
		
		return true;
		
	}
	
	
	
	public function update_revision_in_database()
	{
		
		$mysql 	=	new mysql_connection();
		
		
		
		/*
		 * 	Generate SQL string
		 */
		$output		=	array();
		$fields		=	array(
			'story_id',
			'content',
			'author',
			'date_saved',
			'clean_checkout',
			'word_count',
			'internal_comment',
			'external_comment'
		);
		
		foreach ( $fields as $field )
		{
			
			$output		=	$field . " = '" . $mysql->clean_string( $this->$field ) . "' ";
			
		}
		$output		=	implode( ",", $output );
		
		$sql		=	" 	UPDATE revisions
							SET
								$output
							WHERE
								id = " . $this->id;
							
		
	
		/*
		 * 	Do SQl query
		 */
		$mysql->query ( $sql );
		
		
		
		/*
		 * 	Do some logging
		 */
		$message		=	"Revision updated for story " . $this->id;
		debug ( $message . " :: $sql", __FILE__, __LINE__ );
		LOG_record_entry( $message );		
		
		return true;
		
		
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * ======================================================
	 * 				GETTERS
	 */
	public function get_id()
	{
		return $this->id;
	}
	public function get_story_id()
	{
		return $this->story_id;
	}
	public function get_content()
	{
		return $this->content;		
	}
	public function get_author()
	{
		return $this->author;
	}
	public function get_date_saved()
	{
		return $this->date_saved;
	}
	public function get_clean_checkout()
	{
		return $this->clean_checkout;
	}
	public function get_word_count()
	{
		return $this->word_count;
	}
	public function get_internal_comment()
	{
		return $this->internal_comment;
	}
	public function get_external_comment()
	{
		return $this->external_comment;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * ======================================================
	 * 				SETTERS
	 */
	public function set_story_id( $id )
	{

		if ( !CORE_is_number( $id ) ) return false;
		
		$this->story_id 	=	$id;
		
	}
	
	public function set_author( $id )
	{

		if ( !CORE_is_number( $id ) ) return false;
		
		$this->author 	=	$id;
		
	}
	
	public function set_content( $text )
	{
		
		if ( !is_string( $text ) || trim( $text ) == '' ) return false;
		
		$this->content		=	$text;
		
	}
	
	public function set_date_saved ( $date )
	{
		
		$datetime	=	new date_time();
		if (!$datetime->is_sql_datetime( $date ) ) return false;
		
		$this->date_saved		=	$date;
		
	}
	
	public function set_clean_checkout( $check )
	{
		
		if ( $check != 0 && $check != 1 ) return false;
		
		$this->clean_checkout	=	$check;
		
	}
	
	public function set_internal_comment ( $text )
	{
		
		if ( !is_string( $text ) ) return false;
		
		$this->internal_comment 	=	$text;
		
	}
	
	public function set_external_comment( $text )
	{
		
		if ( !is_string( $text ) ) return false;
		
		$this->external_comment		=	$text;
		
	}
	
	public function set_word_count ( $wc )
	{
		
		//if ( !CORE_is_number( $wc ) ) return false;

		$this->word_count		=	$wc;
		
	}
	
	
	
	
	
	
	
	
}







