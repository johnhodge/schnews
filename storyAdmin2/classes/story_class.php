<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	3rd September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	A class to manipulate a story
 *
 */



class story
{
	//	SQL data
	protected $id;
	protected $story;

	//	Story Content
	protected $issue;
	protected $headline;
	protected $subheadline;
	protected $summary;
	protected $rss;
	protected $twitter;
	protected $author;
	
	protected $graphic_sm;
	protected $graphic_lg;
	protected $graphic_vlg;
	
	protected $additional_graphic_1;
	protected $additional_graphic_2;
	protected $additional_graphic_3;
	protected $additional_graphic_4;
	
	protected $additional_graphic_caption_1;
	protected $additional_graphic_caption_2;
	protected $additional_graphic_caption_3;
	protected $additional_graphic_caption_4;
	
	
	protected $latest_revision; // This should be an object of a revision

	//	Story Meta-Data
	protected $date_added;
	protected $live;
	protected $date_live;
	protected $position;
	protected $size;
	protected $sticky;
	protected $hide;
	protected $deleted;
	protected $hits;

	//	Comments
	protected $hide_comments;
	protected $lock_comments;

	// Checkout
	protected $checked_out;
	protected $checkout_last_activity;
	protected $checkout_change_request;


	/*
	 * 	Populates the object with the initial data, or an empty set if a new story.
	 *
	 * 	@param id [INT]			//	The story id in the database. Set to 0 or false or leave empty for a new story
	 */
	function __construct( $id = false )
	{

		debug( "New Story Instantiated. ID = $id", __FILE__, __LINE__ );

		//	Test inputs
		if (!CORE_is_number( $id ) ) $id = false;
		$this->id		=	$id;

		/*
		 * Depending on the ID either get the story data or populate with empty data
		 */
		if (!$id)
		{

			$this->make_new_story();

		}
		else
		{

			if ( !$this->get_story_data() )
			{

				return false;

			}

		}

		return true;

	}



	/*
	 * 	Grabs the story data from the DB and populates the local properties
	 */
	protected function get_story_data()
	{

		//	Test inputs
		if (!CORE_is_number( $this->id ) )
		{

			/*
			 * If ID is not a number by this stage then we have a serious error
			 */
			$message	=	"story->get_story_data() called without story->id being properly set.";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;

		}

		/*
		 * 	Do the database query to get the data
		 */
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT * FROM stories WHERE id = " . $this->id;
		$mysql->query( $sql );

		/*
		 * 	Make sure only 1 result was returned
		 */
		if ( $mysql->get_num_rows() != 1 && $this->id != 0)
		{

			/*
			 * 	If there is not 1 result then we have a pretty serious programming error or a hack attampt
			 */
			$message 	=	"SQL query in story->get_story_data() did not yield 1 result. Query = " . $sql;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;

		}

		$story	=	$mysql->get_row();
		$this->story 	=	$story;

		//	Populate local data
		$this->issue			=	$story[ 'issue' ];
		$this->headline			=	$story[ 'headline' ];
		$this->subheadline		=	$story[ 'subheadline' ];
		$this->summary			=	$story[ 'summary' ];
		$this->rss				=	$story[ 'rss' ];
		$this->twitter			=	$story[ 'twitter' ];
		$this->author			=	$story[ 'author' ];
		$this->graphic_sm		=	$story[ 'graphic_sm' ];
		$this->graphic_lg		=	$story[ 'graphic_lg' ];
		$this->graphic_vlg		=	$story[ 'graphic_vlg' ];
		$this->date_added		=	$story[ 'date_added' ];
		$this->live				=	$story[ 'live' ];
		$this->date_live		=	$story[ 'date_live' ];
		$this->position			=	$story[ 'position' ];
		$this->size				=	$story[ 'size' ];
		$this->sticky			=	$story[ 'sticky' ];
		$this->hide				=	$story[ 'hide' ];
		$this->deleted			=	$story[ 'deleted' ];
		$this->hits				=	$story[ 'hits' ];
		$this->hide_comments	=	$story[ 'hide_comments' ];
		$this->lock_comments	=	$story[ 'lock_comments' ];
		$this->checked_out		=	$story[ 'checked_out' ];
		$this->checkout_last_activity	=	$story[ 'checkout_last_activity' ];
		$this->checkout_change_request 	= 	$story[ 'checkout_change_request' ];
		$this->additional_graphic_1		=	$story['additional_graphic_1'];
		$this->additional_graphic_2		=	$story['additional_graphic_2'];
		$this->additional_graphic_3		=	$story['additional_graphic_3'];
		$this->additional_graphic_4		=	$story['additional_graphic_4'];
		
		$this->additional_graphic_caption_1		=	$story['additional_graphic_caption_1'];
		$this->additional_graphic_caption_2		=	$story['additional_graphic_caption_2'];
		$this->additional_graphic_caption_3		=	$story['additional_graphic_caption_3'];
		$this->additional_graphic_caption_4		=	$story['additional_graphic_caption_4'];

		/*
		 * Get the latest revision
		 */
		$rev_id		=	STORIES_get_latest_revision( $this->id );
		$this->latest_revision	=	new revision( $rev_id );

		return true;

	}



	/*
	 * 	Fills the local properties with empty data
	 */
	protected function make_new_story()
	{

		$this->story 			=	array();
		
		$issue					=	new issue( ISSUES_get_newest_issue() );
		$this->issue			=	$issue->get_id();

		$this->headline			=	'';
		$this->subheadline		=	'';
		$this->summary			=	'';
		$this->rss				=	'';
		$this->twitter			=	'';
		$this->author			=	0;
		$this->graphic_sm		=	'';
		$this->graphic_lg		=	'';
		$this->graphic_vlg		=	'';
		$this->date_added		=	DATETIME_make_sql_datetime();
		$this->live				=	0;
		$this->date_live		=	'00-00-00 00:00:00';
		$this->position			=	'1';
		$this->size				=	'2';
		$this->sticky			=	0;
		$this->hide				=	0;
		$this->deleted			=	0;
		$this->hits				=	0;
		$this->hide_comments	=	0;
		$this->lock_comments	=	0;
		$this->checked_out		=	0;
		$this->checkout_last_activity	=	'00-00-00 00:00:00';
		$this->checkout_change_request 	=	0;
		$this->latest_revision	=	false;

	}



	/*
	 * 	Commits changes to the database
	 */
	public function update_story_in_database()
	{

		if ( $this->id == 0 )
		{

			if ( !$this->commit_add_story() )
			{

				return false;

			}

		}
		else
		{

			if ( !$this->commit_update_story() )
			{

				return false;

			}

		}

		return true;

	}



	protected function commit_add_story()
	{
		/*
		 * Once the story has been added it stores the headline is a cookie. If the page is then refresh the 
		 * cookie will match the incoming headline, and we'll know not to add the story again.
		 */
		if ( isset( $_SESSION[ 'story_just_added' . SESSION_APPEND ] ) && $_SESSION[ 'story_just_added' . SESSION_APPEND ] == $this->headline )
		{
			
			$message	=	"Adding story failed due to SESSION story_just_added being triggered. \$story->commit_add_story()";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 1 );
			return false;
			
		}
		
		
		
		/*
		 * Insert into database
		 */
		$mysql		=	new mysql_connection();
		$sql		=	"	INSERT INTO stories
								(
									issue,
									headline,
									subheadline,
									summary,
									rss,
									twitter,
									author,
									date_added,
									date_live,
									position,
									size,
									sticky,
									hide,
									deleted,
									live,
									lock_comments,
									hide_comments,
									hits,
									checked_out,
									checkout_last_activity,
									checkout_change_request
								)
								VALUES
								(
									'" . $mysql->clean_string( $this->issue ) . "',
									'" . $mysql->clean_string( $this->headline ) . "',
									'" . $mysql->clean_string( $this->subheadline ) . "',
									'" . $mysql->clean_string( $this->summary ) . "',
									'" . $mysql->clean_string( $this->rss ) . "',
									'" . $mysql->clean_string( $this->twitter ) . "',
									'" . $mysql->clean_string( $this->author ) . "',
									'" . $mysql->clean_string( $this->date_added ) . "',
									'" . $mysql->clean_string( $this->date_live ) . "',
									'" . $mysql->clean_string( $this->position ) . "',
									'" . $mysql->clean_string( $this->size ) . "' ,
									'" . $mysql->clean_string( $this->sticky ) . "',
									'" . $mysql->clean_string( $this->hide ) . "',
									'" . $mysql->clean_string( $this->deleted ) . "',
									'" . $mysql->clean_string( $this->live ) . "',
									'" . $mysql->clean_string( $this->lock_comments ) . "',
									'" . $mysql->clean_string( $this->hide_comments ) . "',
									'" . $mysql->clean_string( $this->hits ) . "',
									'" . $mysql->clean_string( $this->checked_out ) . "',
									'" . $mysql->clean_string( $this->checkout_last_activity ) . "',
									'" . $mysql->clean_string( $this->checkout_change_request ) . "'
								)
						";
		$mysql->query( $sql );
		
		//	Get the new ID
		$this->id 	=	$mysql->get_insert_id();

		
		/*
		 *	Make some records of the event 
		 */
		debug ( $sql, __FILE__, __LINE__ );
		LOG_record_entry( "New Story Added. Headline = '" . $this->headline . "'. Added by " . $_SESSION['logged_in_name'.SESSION_APPEND] );
		
		
		
		/*
		 * Store a session var of the headline, so that if the page is refreshed we can detect
		 * it and avoid duplicating the record.
		 */
		$_SESSION[ 'story_just_added' . SESSION_APPEND ]	=	$this->headline;
		
		return true;

	}
	
	
	
	protected function commit_update_story()
	{
		
			debug ( "Beginning DB Update of Story", __FILE__, __LINE__ );
			
			$mysql	=	new mysql_connection();

			/*
			 * 	Make value = key pairs
			 */
			$fields		=	array(
									'issue',
									'headline',
									'subheadline',
									'summary',
									'rss',
									'twitter',
									'author',
									'graphic_sm',
									'graphic_lg',
									'graphic_vlg',
									'date_added',
									'date_live',
									'position',
									'size',
									'sticky',
									'hide',
									'deleted',
									'live',
									'lock_comments',
									'hide_comments',
									'hits',
									'checked_out',
									'checkout_last_activity',
									'checkout_change_request'
			);
			
			$output		=	array();
			foreach ( $fields as $field )
			{
				
				$output[]		=	" $field = '" . $mysql->clean_string( $this->$field ) . "' ";
				
			}
			$values		=	implode( ", ", $output );
			
			
			
			/*
			 *	Prepare SQL 
			 */
			$sql	=	"	UPDATE stories
							SET
								$values
							WHERE 
								id = " . $this->id;
			$mysql->query( $sql );
			debug ( $sql );
			
			
			
			/*
			 * 	Record some log entries
			 */
			$message		=	"Story updated. Story ID = " . $this->id . ". User = " . $_SESSION['logged_in_name'.SESSION_APPEND];
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message );
			
			
			return true;
			
	}



	/*
	 * 	Updates the database to set a story as checked out by a given user
	 * 
	 * 	@param int $user_id				//	The MySQL id of the user doing the checkout
	 * 	@return bool
	 */
	public function checkout( $user_id )
	{
		
		// Test inputs
		if ( !CORE_is_number( $user_id ) || !$user_id ) 
		{

			debug( "User ID not valid", __FILE__, __LINE__ );
			return false;
			
		}
		
		$this->checked_out				=	$user_id;
		
		
		//	Update checked_out in DB
		$mysql		=	new mysql_connection();
		$sql		=	"	UPDATE stories
							SET 
								checked_out = '" . $this->checked_out . "'
							WHERE
								id = " . $this->id;
		$mysql->query( $sql );
		
		$this->update_checkout_last_activity();
		
		
		/*
		 * Make some records of events
		 */
		$message 	=	"Story ID " . $this->id . " checked out by " . $this->checked_out;
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message );
		
		return true;
		
	}
	
	
	
	
		
	
	
	
	/*
	 * 	Updates the database to reflect that the user who has checked out a story 
	 * 	was last active now.
	 * 
	 * 	@return bool
	 */
	protected function update_checkout_last_activity()
	{
		
		if ( $this->checked_out == 0 || $this->checked_out == '' || $this->checked_out != $_SESSION['logged_in_id'.SESSION_APPEND] )	return false;
		
		$this->checkout_last_activity	=	DATETIME_make_sql_datetime();
		
		$mysql 	=	new mysql_connection();
		$sql	=	"	UPDATE stories
						SET 
							checkout_last_activity = '" . $this->checkout_last_activity . "'
						WHERE
							id = " . $this->id;
		$mysql->query( $sql );
		
		unset( $mysql );
		
		return true;
		
	}

	
	
	/*
	 * 	Marks a story in the database as checked i (i.e. not checked out)
	 * 
	 * 	@param bool $clean			//	A clean checkout is where the user smoothly saves their work and exits volentuerily
	 * 									An unclean checkout is where there is too much in activity and they are timed out.
	 * 	@param bool silent			//	Supress messages to screen or not
	 * 	@return bool
	 */
	public function checkin( $clean = true, $silent = false )
	{
		
		//	Test inputs
		if ( !is_bool( $clean ) ) 	$clean 	=	 true;
		if ( !is_bool( $silent ) ) 	$silent	=	false;
		
		
		
		/*
		 * 	Update database
		 */
		$this->checked_out 	=	0;
		$this->checkout_last_activity	=	DATETIME_make_sql_datetime();
		$this->checkout_change_request	=	0;
		
		if ( $clean )
		{
			
			$clean_line		=	" clean_checkin = 1 ";
			
		}
		else
		{
			
			$clean_line		=	" clean_checkin = 0 ";
			
		}
		
		$mysql	=	new mysql_connection();
		$sql	=	" 	UPDATE stories
						SET
							checked_out = 0,
							checkout_last_activity = '" . $this->checkout_last_activity. "',
							checkout_change_request = 0,
							$clean_line
						WHERE
							id = " . $this->id;
		$mysql->query( $sql );
		
		
		/*
		 * 	Make a record of events
		 */
		if ( !$silent )
		{
			$message 	=	"Story " . $this->id . " has been checked in." . $clean_line;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message );
			$_SESSION['header_message_perm'] = '';
			debug( "HeaderNoticePerm unset", __FILE__, __LINE__ );
			$_SESSION['header_message_temp']	=	"You have saved the changes to the story";
		}
				
	}

	
	
	
	
	public function has_user_voted_for_story()
	{
		
		/*
		 * Do DB Query
		 */
		$mysql		=	new mysql_connection();
		$sql		=	" 	SELECT id
							FROM story_votes
							WHERE 
								story	=	" . $this->id . "
							AND user	=	" . $_SESSION['logged_in_id'.SESSION_APPEND];
		$mysql->query( $sql );		
		
		
		if ( $mysql->get_num_rows() == 1 )
		{
			return true;
		}
		elseif ( $mysql->get_num_rows() == 0 )
		{
			return false;
		}
		else
		{
			$message	=	"More than 1 result returnedin DB query in Story->has_user_voted_for_story()";
			debug( $message, __FILE__, __LINE__);
			LOG_record_entry( $message, 'error', 2 );
			return false;
		}
		
	}

	
	
	
	
	/*
	 * 	Test to see if a vote from the current user would put this story
	 * 	above the threshold for the story going live.
	 * 
	 * 	Note, this will not test to see if the story is already live, it
	 * 	only compares the number of votes with the settings requirements.
	 */
	public function would_vote_make_live()
	{
		
		/*
		 * 	If the story is already live then there is no point in continuing
		 */
		if ( $this->get_live() == 1 )
		{
			
			return false;
			
		}
		
		/*
		 * 	Test to see if the total number of votes would be reached...
		 */
		$total_votes_new	=	$this->get_number_of_votes() + 1;
		if ( $total_votes_new < number_story_votes_to_live )
		{
			
			return false;	
			
		}
		
		
		
		/*
		 * 	If we got this far then we have enough total votes, so we 
		 * 	need to test the admin votes.
		 */
		if ( $_SESSION['logged_in'.SESSION_APPEND]	==	2 )
		{
			
			$total_admin_votes_new	=	$this->get_number_of_admin_votes() + 1;
			
		}
		else
		{
			
			$total_admin_votes_new	=	$this->get_number_of_admin_votes();
			
		}
		if ( $total_admin_votes_new < number_admin_votes_to_live )
		{

			return false;
			
		}

		
		
		/*
		 * 	If we have got this far then it must be true that a vote from
		 * 	this user would tip the story live...
		 */
		return true;
		
	}
	
	
	
	
	
	
	
	/*
	 * 	Adds a user's vote to this story.
	 * 
	 * 	It will also test to see whether this vote puts the story over 
	 * 	the vote threshold, and will call the ake_story_live() member if
	 * 	so.
	 */
	
	/*
	 * 	DEVELOPER NOTE: This needs more debuggig, logging and general 
	 * 	error handling
	 */
	public function vote_for_story( $user_id )
	{
		
		/*
		 * Test inputs
		 */
		if ( !CORE_is_number( $user_id ) )	return false;
		
		
		
		/*
		 * 	Add vote to database
		 */
		$user		=	new user( $user_id );
		
		$mysql		=	new mysql_connection();
		$sql		=	" 	INSERT INTO story_votes
							( story, user, user_status )
							VALUES
							( " . $this->id . ", " . $user_id . ", " . $user->get_status() . " ) ";
		$mysql->query( $sql );
		

		
		/*
		 * Test to see if this vote has caused the story to go live
		 */
		if ( STORIES_has_story_received_enough_votes( $this->id ) )
		{
			
			$this->make_story_live();
			
		}
		
		
		return true;
		
	}
	
	
	
	public function remove_vote_for_story( $user_id )
	{
		
		/*
		 * Test inputs
		 */
		if ( !CORE_is_number( $user_id ) )	return false;
		
		
		$mysql	=	new mysql_connection();
		
		
		/*
		 * Make sure that 1, and only 1, vote exists for this story / user combo
		 */
		$sql	=	"	SELECT id
						FROM story_votes
						WHERE
							story = " . $this->id . "
						AND user = " . $user_id;
		$mysql->query( $sql );
		if ( $mysql->get_num_rows() != 1 )
		{

			$message	=	"Not 1 result return from SQL in story->remove_vote_from_story()";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		/*
		 * Assuming that we only have 1 record at this stage, delete it.
		 */
		$sql	=	" 	DELETE FROM story_votes
						WHERE
							story = " . $this->id . "
						AND user = " . $user_id;
		$mysql->query( $sql );
	
		return true;
		
	}
	
	
	
	/*
	 * 	Find how many votes in total there are for the story
	 */
	public function get_number_of_votes()
	{
		
		$mysql		=	new mysql_connection();
		$sql		=	" 	SELECT id
							FROM story_votes
							WHERE story = " . $this->id;
		$mysql->query( $sql );
		
		return $mysql->get_num_rows();
		
	}
	
	
	
	/*
	 * 	Find how many votes from Admins there are for the story
	 */
	public function get_number_of_admin_votes()
	{
		
		$mysql		=	new mysql_connection();
		$sql		=	" 	SELECT id
							FROM story_votes
							WHERE 	story = " . $this->id . "
							AND		user_status = 2";
		$mysql->query( $sql );
		
		return $mysql->get_num_rows();
		
	}
	
	
	
	
	/*
	 * 	Sets the story as live (visible to the public) in both the 
	 * 	database and the filesystem.
	 * 
	 * 	This will not test the number of vote cast, it assumes that you
	 * 	have done so already. It is protected for this reason.
	 * 
	 * 	Make sure to only call it if the number of votes are sufficient.
	 * 
	 * 	This function is called by $this->vote_for_story(), which does the vote
	 * 	testing.
	 */
	protected function make_story_live()
	{

		/*
		 * Update the database
		 */
		$this->set_live( 1 );
		$this->set_date_live( DATETIME_make_sql_datetime() );
		$this->update_story_in_database();
		
		
		
		/*
		 * Send messages
		 */
		$message	=	"Story '" . $this->headline . "' has been made live";
		LOG_record_entry( $message );
		debug ( $message, __FILE__, __LINE__ );
		
		
				/*
				 * Send email to author only
				 */
				$author		=	new user( $this->author );
				$send_addr	=	$author->get_email();

				$subject	=	"SchNEWS StoryAdmin :: Your Story Has Been Voted Live.";
				$message	=	file_get_contents( "email_templates/story_live_author_only" );	

				$message	=	str_replace( "***AUTHOR_NAME***", $author->get_name(), $message );
				$message	=	str_replace( "***HEADLINE***", $this->headline, $message );
				
			
		
		/*
		 * 	Send the message
		 */
		$mail	=	new email( $message, $subject, $send_addr );
		$mail->send();
		
		
		
		/*
		 * 	Generate the file in the file system.
		 */
		STORIES_make_story_file( $this->id );
		
		
		return true;
		
	}
	
	
	
	/*
	 * 	==================================================
	 * 					     GETTERS
	 */
	public function get_id()
	{
		$this->update_checkout_last_activity();
		return $this->id;
	}
	public function get_issue()
	{
		$this->update_checkout_last_activity();
		return $this->issue;
	}
	public function get_headline()
	{
		$this->update_checkout_last_activity();
		return $this->headline;
	}
	public function get_subheadline()
	{
		$this->update_checkout_last_activity();
		return $this->subheadline;
	}
	public function get_summary()
	{
		$this->update_checkout_last_activity();
		return $this->summary;
	}
	public function get_rss()
	{
		$this->update_checkout_last_activity();
		return $this->rss;
	}
	public function get_twitter()
	{
		$this->update_checkout_last_activity();
		return $this->twitter;
	}
	public function get_author()
	{
		$this->update_checkout_last_activity();
		return $this->author;
	}
	public function get_date_added()
	{
		$this->update_checkout_last_activity();
		return $this->date_added;
	}
	public function get_date_live()
	{
		$this->update_checkout_last_activity();
		return $this->date_live;
	}
	public function get_position()
	{
		$this->update_checkout_last_activity();
		return $this->position;
	}
	public function get_size()
	{
		$this->update_checkout_last_activity();
		return $this->size;
	}
	public function get_sticky()
	{
		$this->update_checkout_last_activity();
		return $this->sticky;
	}	
	public function get_hide()
	{
		$this->update_checkout_last_activity();
		return $this->hide;
	}
	public function get_deleted()
	{
		$this->update_checkout_last_activity();
		return $this->deleted;
	}
	public function get_live()
	{
		$this->update_checkout_last_activity();
		return $this->live;
	}
	public function get_lock_comments()
	{
		$this->update_checkout_last_activity();
		return $this->hide_comments;
	}
	public function get_hits()
	{
		$this->update_checkout_last_activity();
		return $this->hits;
	}
	public function get_checked_out()
	{
		$this->update_checkout_last_activity();
		return $this->checked_out;
	}
	public function get_checkout_last_activity()
	{
		$this->update_checkout_last_activity();
		return $this->checkout_last_activity;
	}
	public function get_checkout_change_request()
	{
		$this->update_checkout_last_activity();
		return $this->checkout_change_request;
	}
	public function get_latest_revision()
	{
		$this->update_checkout_last_activity();
		return $this->latest_revision;
	}
	public function get_graphic_sm()
	{
		$this->update_checkout_last_activity();
		return $this->graphic_sm;
	}
	public function get_graphic_lg()
	{
		$this->update_checkout_last_activity();
		return $this->graphic_lg;
	}
	public function get_graphic_vlg()
	{
		$this->update_checkout_last_activity();
		return $this->graphic_vlg;
	}
	public function get_additional_graphics_1()
	{
		
		return $this->additional_graphic_1;
		
	}
	public function get_additional_graphics_2()
	{
		
		return $this->additional_graphic_2;
		
	}
	public function get_additional_graphics_3()
	{
		
		return $this->additional_graphic_3;
		
	}
	public function get_additional_graphics_4()
	{
		
		return $this->additional_graphic_4;
		
	}
	
	public function get_additional_graphics_caption_1()
	{
		
		return $this->additional_graphic_caption_1;
		
	}
	public function get_additional_graphics_caption_2()
	{
		
		return $this->additional_graphic_caption_2;
		
	}
	public function get_additional_graphics_caption_3()
	{
		
		return $this->additional_graphic_caption_3;
		
	}
	public function get_additional_graphics_caption_4()
	{
		
		return $this->additional_graphic_caption_4;
		
	}
	
	
	
	


	/*
	 * 	==================================================
	 * 					     SETTERS
	 */
	public function set_headline( $headline )
	{
		$this->update_checkout_last_activity();

		if ( is_string( $headline ) && trim( $headline ) != '' )
		{

			$this->headline		=	$headline;
			return true;

		}
		else
		{

			return false;

		}

	}
	
	public function set_subheadline( $headline )
	{
		$this->update_checkout_last_activity();

		if ( is_string( $headline ) && trim( $headline ) != '' )
		{

			$this->subheadline		=	$headline;
			return true;

		}
		else
		{

			return false;

		}

	}

	public function set_summary( $summary )
	{
		$this->update_checkout_last_activity();

		if ( is_string( $summary ) && trim( $summary ) != '' )
		{

			$this->summary		=	$summary;
			return true;
		}
		else
		{

			return false;

		}

	}
	
	public function set_rss( $rss )
	{
		
		$this->update_checkout_last_activity();
				
		if ( is_string( $rss ) )
		{
			
			$this->rss		=	$rss;
			return true;
			
		}
		else
		{
			
			return false;
		
		}
		
	}
	
	public function set_twitter( $twitter )
	{
		
		$this->update_checkout_last_activity();
		
		if ( is_string( $twitter ) )
		{
			
			$this->twitter = $twitter;
			return true;
			
		}
		else
		{
			
			return false;
			
		}
		
	}
	
	public function set_author( $author )
	{
		
		if ( !CORE_is_number( $author) ) return false;
		
		$this->author	=	$author;
		
	}
	
	public function set_sticky( $sticky )
	{

		if ( $sticky != 1 && $sticky != 0 ) return false;
		
		$this->sticky 	=	$sticky; 
		
	}
	
	public function set_hide ( $hide )
	{
		
		if ( $hide != 0 && $hide != 1) return false;
		
		$this->hide		=	$hide;
		
	}
	public function set_deleted( $deleted )
	{
		
		if ( $deleted != 0 && $deleted != 1 ) return false;
		
		$this->deleted	=	$deleted;
		
	}
	
	public function set_position( $position )
	{

		if ( !CORE_is_number( $position ) ) return false;
		
		$this->position	=	$position;
		
	}
	
	public function set_size( $size )
	{

		if ( !CORE_is_number( $size ) ) return false;
		
		$this->size		=	$size;
		
	}

	public function set_latest_revision( $revision )
	{
		
		if ( !is_object( $revision ) ) return false;
		
		$this->latest_revision  	=	$revision;
		
	}
	
	public function set_live( $live )
	{
		if ( $live != 1 && $live != 0 ) return false;
		
		$this->live		=	$live;
	}
	
	public function set_date_live( $date )
	{

		if ( !is_string( $date ) || strlen( $date ) != 19 ) return false;
		
		$this->date_live	=	$date;
		
	}
	
	public function set_graphic_sm( $gfx )
	{

		if ( !is_string( $gfx ) || empty( $gfx ) ) return false;
		
		$this->graphic_sm 	=	$gfx;
		
	}
	
	public function set_graphic_lg( $gfx )
	{

		if ( !is_string( $gfx ) || empty( $gfx ) ) return false;
		
		$this->graphic_lg 	=	$gfx;
		
	}
	
	public function set_graphic_vlg( $gfx )
	{

		if ( !is_string( $gfx ) || empty( $gfx ) ) return false;
		
		$this->graphic_vlg 	=	$gfx;
		
	}
	
}