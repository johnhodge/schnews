<?php



class homepage_misc_fields
{
	
	protected $wake_up_text;
	protected $disclaimer;
	protected $date;
	protected $user;
	
	
	
	function __construct()
	{
		
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT *
						FROM homepage_misc_fields
						ORDER BY id DESC
						LIMIT 0, 1 ";
		$mysql->query( $sql );

		$res	=	$mysql->get_row();
		
		$this->wake_up_text					=	$res['wake_up_text'];
		$this->disclaimer					=	$res['disclaimer'];
		$this->date							=	$res['date'];
		$this->user							=	$res['user'];
		
		return true;
		
	}

	
	
	public function update_fields()
	{
		
		$this->date		=	DATETIME_make_sql_datetime();
		$this->user		=	$_SESSION['logged_in_id'.SESSION_APPEND];
		
		$mysql	=	new mysql_connection();
		$sql	=	"	INSERT INTO homepage_misc_fields
						(
							wake_up_text,
							disclaimer,
							date,
							user
						)
						VALUES
						(
							'" . $mysql->clean_string( $this->wake_up_text ) . "',
							'" . $mysql->clean_string( $this->disclaimer ) . "',
							'" . $mysql->clean_string( $this->date ) . "',
							'" . $mysql->clean_string( $this->user ) . "'
						)";
		$mysql->query( $sql );
							
		return true;		
		
	}
	
	
	
	
	
	
	
	/*
	 * 	GETTERS
	 */
	

	/**
	 * @return the $wake_up_text
	 */
	public function get_wake_up_text() {
		return $this->wake_up_text;
	}

	/**
	 * @return the $disclaimer
	 */
	public function get_disclaimer() {
		return $this->disclaimer;
	}

	/**
	 * @return the $date
	 */
	public function get_date() {
		return $this->date;
	}

	/**
	 * @return the $user
	 */
	public function get_user() {
		return $this->user;
	}

	
	
	
	
	
	
	
	/*
	 * 		SETTERS
	 */
	

	/**
	 * @param $wake_up_text the $wake_up_text to set
	 */
	public function set_wake_up_text($wake_up_text) {
		$this->wake_up_text = $wake_up_text;
	}

	/**
	 * @param $disclaimer the $disclaimer to set
	 */
	public function set_disclaimer($disclaimer) {
		$this->disclaimer = $disclaimer;
	}


	
}