<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	A class to cover user manipulation
 *
 */



class user
{

	protected $id;
	protected $name;
	protected $username;
	protected $email;
	protected $password_hash;
	protected $status;
	protected $last_login;


	function __construct($id = false)
	{
		//	Test inputs
		if (!is_numeric($id) && $id != false) $id = false;
		if ($id == 0) $id = false;

		$this->id 	=	$id;

		debug ("User ID = ".$this->id, __FILE__, __LINE__);

		$this->get_user();

	}



	private function get_user()
	{

		if ($this->id == false) return true;

		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT *
						FROM users
						WHERE id = ".$this->id;
		$mysql->query($sql);
		debug($sql);

		//	Make sure there is only 1 result, and log an error if that's not the case
		if ($mysql->get_num_rows() != 1)
		{
			debug ("Not 1 result from get user", __FILE__, __LINE__);
			if ($mysql->get_num_rows() == 0)
			{
				LOG_record_entry("Attempt to get user that does not exist. ID = ".$this->id, false, 2);
			}
			if ($mysql->get_num_rows() > 1)
			{
				LOG_record_entry("Attempt to get user resulted in more than 1 user. ID = ".$this->id, false, 2);
			}
			return false;
		}


		$user					=	$mysql->get_row();
		$this->name				=	$user['name'];
		$this->username			=	$user['username'];
		$this->email			=	$user['email'];
		$this->password_hash	=	$user['password'];
		$this->status			=	$user['status'];
		$this->last_login		=	$user['date_last_login'];

		debug ("User status = ".$user['status']);

		unset($mysql);
		return true;

	}



	/*
	 * 	Set the session variabl;es to log the user in, plus update the DB with the last login
	 */
	public function log_user_in()
	{

		debug ("Status = ".$this->status, __FILE__, __LINE__);

		if ($this->id == 0 || $this->status == false)
		{

			$error 		=	"Attempt to login user when user id is not set";
			debug($error, __FILE__, __LINE__);
			LOG_record_entry($error, 'security', 1);

			return false;

		}



		//	Make sure that the user isn't already logged in (i.e. have they just logged in and then refreshed the page
		if (isset($_SESSION['logged_in_username'.SESSION_APPEND]) && $_SESSION['logged_in_username'.SESSION_APPEND] == $this->username)
		{
			return false;
		}



		//	Update last login info
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT date_this_login
							FROM users
							WHERE id = ".$this->id;
		$mysql->query($sql);
		$result 	=	$mysql->get_row();
		$last_login	=	$result['date_this_login'];

		$sql		=	" 	UPDATE users
							SET 	date_last_login = '$last_login',
									date_this_login = '".DATETIME_make_sql_datetime()."'
							WHERE id = ".$this->id;
		$mysql->query($sql);




		$_SESSION['logged_in'.SESSION_APPEND]				=	$this->status;
		$_SESSION['logged_in_name'.SESSION_APPEND] 			= 	$this->name;
		$_SESSION['logged_in_email'.SESSION_APPEND] 		= 	$this->email;
		$_SESSION['logged_in_id'.SESSION_APPEND] 			= 	$this->id;
		$_SESSION['logged_in_username'.SESSION_APPEND] 		= 	$this->username;
		$_SESSION['logged_in_last_login'.SESSION_APPEND] 	= 	$this->last_login;

		LOG_record_entry("User '".$this->name."' logged in");
		
		GC_collect_garbage();

		return true;

	}



	/*
	 * 	Add a New User
	 *
	 * 	It will test that the user does not already exist and then add to the database.
	 * 	The password will be stored as an MD5 hash
	 *
	 * 	@param name 		string		The name of the new user
	 * 	@param username		string		The username of the new user
	 * 	@param password		string		The password of the new user
	 *
	 * 	@return 			int/bool	0 = failed / 1 = suceeded / 2 = user already exists
	 */
	public function add_user($name, $username, $email)
	{



		//	Test inputs
		$errors = 0;
		if (!is_string($name) || trim($name) =='')
		{
			$errors++;
			LOG_record_entry('New user addition attempted with no name set', false, 2);
		}
		if (!is_string($username) || trim($username) =='')
		{
			$errors++;
			LOG_record_entry('New user addition attempted with no username set', false, 2);
		}
		if (!is_string($email) || trim($email) =='')
		{
			$errors++;
			LOG_record_entry('New user addition attempted with no email set', false, 2);
		}

		if ($errors > 0)
		{
			return false;
		}


		//	Test that user does not already exist
		if ($this->does_user_exist($name, $username))
		{
			LOG_record_entry("Attempt to add a user that already exists", false, 0);
			return 2;
		}


		//	Process and create some data
		$mysql		=	new mysql_connection();

		$this->name			=	$name;
		$this->username		=	$username;
		$this->email		=	$email;
		$this->status		=	1;
		$this->locked		=	0;


		$name		=	$mysql->clean_string($name);
		$username 	=	$mysql->clean_string($username);
		$email		=	$mysql->clean_string($email);
		$status = 1;
		$locked = 0;



		//	Insert into database
		$sql	=	"	INSERT INTO users
							(name, username, email, status, locked)
							VALUES
							('$name', '$username', '$email', $status, $locked)
					";
		if (!$mysql->query($sql))
		{

			LOG_record_entry("SQL failed when adding new user (name = '$name' / username = '$username')", 'security', 2);
			debug("SQL failed when adding new user (name = '$name' / username = '$username')");
			return false;
		}

		LOG_record_entry("New user added. Name = '$name', Username = '$username'", false, 0);
		$this->id		=	$mysql->get_insert_id();
		debug ( "New ID = " . $this->id );
		return true;

	}



	/*
	 * 	Updates the user based on the current object properties
	 */
	public function update_user()
	{

		$mysql		=	new mysql_connection();

		//	Clean Inputs
		$name		=	$mysql->clean_string( $this->name );
		$username	=	$mysql->clean_string( $this->username );
		$email		=	$mysql->clean_string( $this->email );
		$status		=	$mysql->clean_string( $this->status );

		$sql		=	"	UPDATE users
							SET
								name 		= '$name',
								username	= '$username',
								email		= '$email',
								status		= '$status'
							WHERE id = " . $this->id;
		$mysql->query($sql);

		LOG_record_entry( "User updated. Name = '$name', Username = '$username'.", 'security');

		return true;

	}



	/*
	 * 	Updates the user's password hash, based on the current object property
	 */
	public function update_user_password()
	{

		$mysql		=	new mysql_connection();

		$password	=	$mysql->clean_string( $this->password_hash );

		$sql		=	"	UPDATE users
							SET	password = '$password'
							WHERE id = " . $this->id;
		$mysql->query( $sql );

		LOG_record_entry( "Password updated for user " . $this->get_name() . ". Update by " . $_SESSION['logged_in_name'.SESSION_APPEND], 'security' );

	}



	/*
	 * 	Blanks the user's password to an emtpy string
	 */
	public function remove_password()
	{

		debug ( "Removing password for user " . $this->get_name(), __FILE__, __LINE__ );
		$mysql		=	new mysql_connection();
		$sql		=	"	UPDATE users
							SET password = ''
							WHERE id = " . $this->id;
		$mysql->query( $sql );

		LOG_record_entry( "Password removed for user " . $this->get_name() . ". Done by ". $_SESSION['logged_in_name'.SESSION_APPEND], 'security', 1);

		return true;

	}



	public function update_reset_hash()
	{

		$mysql		=	new mysql_connection();
		$name		=	$mysql->clean_string( substr( md5( DATETIME_make_sql_datetime() ), 0, 16 ) );
		$sql		=	"	UPDATE users
							SET	pr_link_hash = '$name'
							WHERE id = " . $this->id;
		$mysql->query( $sql );

		debug ( "Hash = $name", __FILE__, __LINE__ );

		return $name;

	}



	public function check_reset_hash( $hash )
	{

		// Test inputs
		if ( !is_string( $hash ) || trim( $hash ) == '' )
		{

			debug ("Hash is not full string", __FILE__, __LINE__ );
			return false;

		}

		debug ( "Input hash = '$hash'", __FILE__, __LINE__ );

		$mysql 		=	new mysql_connection();
		$hash		=	$mysql->clean_string( $hash );
		$sql		=	" 	SELECT id
							FROM users
							WHERE pr_link_hash = '$hash' ";
		$mysql->query( $sql );
		debug ($sql);

		if ( $mysql->get_num_rows() != 1 )
		{

			debug ( "Not 1 result from SQL hash check", __FILE__, __LINE__ );
			return false;

		}

		$res		=	$mysql->get_row();
		if ( $res['id'] != $this->id )
		{

			debug( "SQL result does not match object ID", __FILE__, __LINE__ );
			return false;

		}

		return true;

	}



	public function remove_reset_hash()
	{

		$mysql 		=	new mysql_connection();
		$sql		=	" 	UPDATE users
							SET pr_link_hash = ''
							WHERE id = " . $this->id;
		$mysql->query($sql);
		return true;

	}



	/*
	 * 	Does the name or username already exist in the database
	 *
	 * 	Used to make sure a new user does not clash with an existing user
	 */
	public function does_user_exist($name, $username)
	{

		//	Test inputs
		if (!is_string($name) || trim($name) == '') return false;
		if (!is_string($username) || trim($username) == '') return false;

		$mysql 		= 	new mysql_connection();
		$name 		= 	$mysql->clean_string($name);
		$username	=	$mysql->clean_string($username);

		$sql		=	"	SELECT id
							FROM users
							WHERE name = '$name' OR username = '$username'";
		$mysql->query($sql);

		if ($mysql->get_num_rows() != 0)
		{
			return true;
		}
		return false;

	}



	/*
	 * 	Generic Getters
	 */

	//	Id
	public function get_id()
	{

		return $this->id;

	}

	//	Name
	public function get_name()
	{
		if (is_string($this->name))
		{
			return $this->name;
		}
		else
		{
			return false;
		}
	}

	//	Username
	public function get_username()
	{
		if (is_string($this->username))
		{
			return $this->username;
		}
		else
		{
			return false;
		}
	}

	//	Email
	public function get_email()
	{
		if (is_string($this->email))
		{
			return $this->email;
		}
		else
		{
			return false;
		}
	}

	//	Password
	public function get_password_hash()
	{
		if ( is_string( $this->password_hash ) )
		{
			return $this->password_hash;
		}
		else
		{
			return false;
		}
	}

	//	Status
	public function get_status()
	{
		if (is_string($this->status))
		{
			return $this->status;
		}
		else
		{
			return false;
		}
	}

	//	Last Login
	public function get_last_login()
	{
		if (is_string($this->last_login))
		{
			return $this->last_login;
		}
		else
		{
			return false;
		}
	}




	/*
	 * 	Generic Setters
	 */


	//	Name
	public function set_name($name)
	{

		if (is_string($name))
		{

			$this->name = $name;

		}
		else
		{

			return false;

		}

	}

	//	Username
	public function set_username($username)
	{

		if (is_string($username))
		{

			$this->username = $username;

		}
		else
		{

			return false;

		}

	}

	//	Email
	public function set_email($email)
	{

		if (is_string($email))
		{

			$this->email = $email;

		}
		else
		{

			return false;

		}

	}

	//	Password_hash
	public function set_password_hash($password)
	{

		if (is_string($password) && trim($password) != '')
		{

			$this->password_hash = md5($password);

		}
		else
		{

			return false;

		}

	}

	//	Status
	public function set_status($status)
	{

		if (is_string($status))
		{

			$this->status = $status;

		}
		else
		{

			return false;

		}

	}


}