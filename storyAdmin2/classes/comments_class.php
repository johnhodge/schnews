<?php


class comment
{
	
	protected $id;
	protected $story_id;
	
	protected $author;
	protected $content;
	protected $date;
	
	protected $deleted;
	protected $deleted_by;
	
	protected $spam;
	protected $spam_score;
	protected $spam_reasons;
	
	function __construct( $id = false )
	{
		
		/*
		 * 	Test inputs
		 */
		if ( !CORE_is_number( $id ) ) $id = false;
		
		$this->id	=	$id;
		
		
		/*
		 * Determie if we're adding a new comments or editing an existing one...
		 */
		if ( $this->id == false )
		{
			
			$this->create_new_comment();
			
		}
		else
		{
		
			$this->get_comment_from_db();
		
		}
		
		return true;
		
	}
	

	
	protected function create_new_comment()
	{
		
		$this->story_id		=	false;
		$this->author		=	'';
		$this->content		=	'';
		$this->date			=	DATETIME_make_sql_datetime();
		$this->ip_hash		=	md5( $_SERVER['REMOTE_ADDR'] );
		
		$this->spam			=	false;
		$this->spam_score	=	0;
		$this->spam_reasons	=	'';
		
		$this->deleted		=	0;
		$this->deleted_by	=	0;
		
		return true;
		
	}
	
	
	
	protected function get_comment_from_db()
	{
		
		/*
		 * 	Get data from DB
		 */
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT *
						FROM comments
						WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		
		
		/*
		 * 	There should only be one result
		 */
		if ( $mysql->get_num_rows() != 1 ) return false;
		
		
		
		$res				=	$mysql->get_row();
		$this->story_id		=	$res['story'];
		$this->author		=	$res['author'];
		$this->content		=	$res['comment'];
		$this->date			=	$res['date'];
		$this->deleted		=	$res['deleted'];
		$this->deleted_by	=	$res['deleted_by'];
		$this->spam_score	=	$res['spam_score'];
		$this->spam_reasons	=	$res['spam_reasons'];
		
	}
	
	
	
	
	
	
	public function update_db()
	{
		
		if ( $this->id == false )
		{
			
			if ( $this->add_to_datebase() ) return true;
			
		}
		else
		{
			
			if ( $this->update_in_database() ) return true;
			
		}
		
		return false;
		
	}
	
	
	
	
	
	
	protected function add_to_database()
	{
		
		if ( $this->story_id == false ) return false;
		if ( $this->content == '' ) return false;
		
		$mysql	=	new mysql_connection();
		$sql	=	" 	INSERT INTO comments
							(
								story,
								comment,
								author,
								date,
								deleted,
								deleted_by,
								spam_score,
								spam_reasons
							)
						VALUES
							(
								" . $this->story_id . ",
								'" . $mysql->clean_string( $this->content ) . "',
								'" . $mysql->clean_string( $this->author ) . "',
								'" . $this->date . "',
								0,
								0,
								" . $this->spam_score . ",
								'" . $this->spam_reasons . "
							)";
		$mysql->query( $sql );

		if ( !$this->id = $mysql->get_insert_id() )
		{
			
			return false;
			
		}
		
		return true;
		
	}
	
	
	
	
	protected function update_in_database()
	{
		
		
		/*
		 * 	Test properties
		 */
		if ( $this->story_id == false ) return false;
		if ( $this->content == '' ) return false;
		
		
		
		/*
		 * 	Update DB
		 */
		$mysql	=	new mysql_connection();
		$sql	=	"	UPDATE comments
						SET
							story		=	" . $mysql->clean_string( $this->story_id ) . ",
							author		=	'" . $mysql->clean_string( $this->author ) . "',
							comment		=	'" . $mysql->clean_string( $this->content ) . "',
							date		=	'" . $mysql->clean_string( $this->date ) . "',
							deleted		=	" . $mysql->clean_string( $this->deleted ) . ",
							deleted_by	=	" . $mysql->clean_string( $this->deleted_by ) . ",
							spam_score	=	" . $mysql->clean_string( $this->spam_score ) . ",
							spam_reasons	=	'" . $mysql->clean_string( $this->spam_reasons ) . "'
						WHERE id = " . $this->id;
							
		$mysql->query( $sql );
		
		return true;
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * 	GETTERS AND SETTERS
	 */
	
	/**
	 * @return the $id
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return the $story_id
	 */
	public function getStory_id() {
		return $this->story_id;
	}

	/**
	 * @return the $author
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * @return the $content
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * @return the $date
	 */
	public function getDate() {
		return $this->date;
	}

	/**
	 * @return the $deleted
	 */
	public function getDeleted() {
		return $this->deleted;
	}

	/**
	 * @return the $deleted_by
	 */
	public function getDeleted_by() {
		return $this->deleted_by;
	}

	/**
	 * @return the $spam
	 */
	public function getSpam() {
		return $this->spam;
	}

	/**
	 * @return the $spam_score
	 */
	public function getSpam_score() {
		return $this->spam_score;
	}

	/**
	 * @return the $spam_reasons
	 */
	public function getSpam_reasons() {
		return $this->spam_reasons;
	}

	/**
	 * @param $story_id the $story_id to set
	 */
	public function setStory_id($story_id) {
		$this->story_id = $story_id;
	}

	/**
	 * @param $author the $author to set
	 */
	public function setAuthor($author) {
		$this->author = $author;
	}

	/**
	 * @param $content the $content to set
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * @param $date the $date to set
	 */
	public function setDate($date) {
		$this->date = $date;
	}

	/**
	 * @param $deleted the $deleted to set
	 */
	public function setDeleted($deleted) {
		$this->deleted = $deleted;
	}

	/**
	 * @param $deleted_by the $deleted_by to set
	 */
	public function setDeleted_by($deleted_by) {
		$this->deleted_by = $deleted_by;
	}

	/**
	 * @param $spam the $spam to set
	 */
	public function setSpam($spam) {
		$this->spam = $spam;
	}

	/**
	 * @param $spam_score the $spam_score to set
	 */
	public function setSpam_score($spam_score) {
		$this->spam_score = $spam_score;
	}

	/**
	 * @param $spam_reasons the $spam_reasons to set
	 */
	public function setSpam_reasons($spam_reasons) {
		$this->spam_reasons = $spam_reasons;
	}

	
	
	
	
	
	
	
	
}