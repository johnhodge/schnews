<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script defines a class for manipulating issues in the database
 */



class issue
{
	protected $id;
	protected $issue;
	
	protected $new;
	
	protected $number;
	protected $name;
	protected $date_added;
	protected $date_published;
	protected $published;
	
	
	
	function __construct( $id = false )
	{
		
		debug ( "New Issue object instantiated. ID = $id", __FILE__, __LINE__ );

		//	Test inputs
		if ( !CORE_is_number( $id ) ) $id =	false;
		$this->id 	=	$id;
		
		
		
		if ( !$id )
		{
			
			$this->make_new_issue();
			$this->new		=	true;
			
		}
		else
		{

			if ( !$this->get_issue_data() )
			{
				
				return false;
			
			}
			$this->new 	=	false;
		
		}
	
	}
	
	
	
	protected function get_issue_data()
	{
		
		//	Test assumptions
		if ( !CORE_is_number( $this->id ) )
		{
			
			/*
			 * If ID is not a number by this stage then we have a serious error
			 */
			$message	=	"issue->get_issue_data() called without issue->id being properly set.";
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		$mysql		=	new mysql_connection();
		$sql		=	"	SELECT * FROM issues WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		/*
		 * 	Make sure only 1 result was returned
		 */
		if ( $mysql->get_num_rows() != 1 && $this->id != 0)
		{

			/*
			 * 	If there is not 1 result then we have a pretty serious programming error or a hack attampt
			 */
			$message 	=	"SQL query in issue->get_story_data() did not yield 1 result. Query = " . $sql;
			debug ( $message, __FILE__, __LINE__ );
			LOG_record_entry( $message, 'error', 2 );
			return false;
			
		}
		
		
		/*
		 * 	Populate Local Properties
		 */
		$issue						=	$mysql->get_row();
		$this->issue				=	$issue;
		
		$this->number				=	$issue['number'];
		$this->name					=	$issue['name'];
		$this->date_added			=	$issue['date_added'];
		$this->date_published		=	$issue['date_published'];
		$this->published			=	$issue['published'];
		
		return true;
		
	}
	
	
	
	protected function make_new_issue()
	{

		$this->issue		=	array();
		
		$this->number				=	0;
		$this->name					=	'';
		$this->date_added			=	DATETIME_make_sql_datetime();
		$this->date_published		=	'00-00-00 00:00:00';
		$this->published			=	0;
		
		return true;
		
	}
	
	
	
	
	public function increment_hits()
	{
		
		$this->set_hits( $this->get_hits() + 1 );
		
	}
	
	
	
	protected function add_new_issue_to_db()
	{
		
		$mysql		=	new mysql_connection();
		$sql		=	"	INSERT INTO issues
							( date_added )
							VALUES
							( '" . $this->date_added . "') ";
		$mysql->query( $sql );
		
		$this->id	=	$mysql->get_insert_id();
		
		$message	=	"New Issue added to database. Id = " . $this->id;
		debug ( $message, __FILE__, __LINE__ );
		
		return true;
		
	}
	
	
	public function update_issue_in_db()
	{
		
		/*
		 * If this is a new issue it will need adding to the DB and an SQL id generating.
		 */
		if ( $this->new )
		{
			
			$this->add_new_issue_to_db();
			
		}
		
		
		
		// Test properties
		/*
		if( !CORE_is_number( $this->id ) )	
		{
			debug( "ID not valid for update_issue_in_db(). Id = '" . $this->id . "'. ID type = " . gettype($this->id), __FILE__, __LINE__ );
			return false;
		}
		*/
		if ( !$this->is_current_data_valid() ) 				
		{
			debug ( "Data not valid for update_issue_in_db()", __FILE__, __LINE__ );
			return false;
		}
		
		
		
		/*
		 * 	Update Database
		 */
		$mysql		=	new mysql_connection();
		$sql		=	"	UPDATE issues
							SET
								number = " . $this->number . ",
								name = '" . $mysql->clean_string( $this->name ) . "',
								date_added = '" . $mysql->clean_string( $this->date_added ) . "',
								date_published = '" . $mysql->clean_string( $this->date_published ) . "',
								published = " . $this->published . "
							WHERE 
								id = " . $this->id;
		$mysql->query( $sql );
		
		
		
		/*
		 * 	Logging
		 */
		$message 	=	"Issue updated. Issue ID = " . $this->id . ". Updated by " . $_SESSION['logged_in_name'.SESSION_APPEND];
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message );
		
		
		
		return true;		
		
		
	}
	
	
	
	/*
	 * 	Test the current object data set to determine whether it represents a valid issue.
	 */
	protected function is_current_data_valid()
	{
		
		$this->debug_issue();
		
		if ( !CORE_is_number( $this->number ) ) 			
		{

			debug ( "Issue data not valide. Problem = 'number'. Number = " . $this->number );
			return false;
			
		}
		
		if ( !is_string( $this->name ) && $this->name != '' ) 
		{

			debug ( "Issue data not valide. Problem = 'name'" );
			return false;
			
		}
		
		$datetime		=	new date_time();
		if ( !$datetime->is_sql_datetime( $this->date_added ) )	
		{

			debug ( "Issue data not valide. Problem = 'date_added'" );
			return false;
			
		}
		
		if ( !$datetime->is_sql_datetime( $this->date_published ) && $this->date_published != '00-00-00 00:00:00' && $this->date_published != '' ) 	
		{

			debug ( "Issue data not valide. Problem = 'date_published'" );
			return false;
			
		}
		
		if ( !CORE_is_number( $this->published ) ) 	
		{

			debug ( "Issue data not valide. Problem = 'published'" );
			return false;
			
		}

		return true;	
		
	}
	
	
	
	protected function debug_issue()
	{
		
		if ( !DEBUG )	return false;
		
		$output		=	array();
		$output[]	=	"<div class='notice_message'>";
		$output[]	=	"<b>Debugging Issue</b>";
		$output[]	=	"Number = " . $this->number;	
		$output[]	=	"Name = " . $this->name;
		$output[]	=	"Date Added = " . $this->date_added;
		$output[]	=	"Date Published = " . $this->date_published;
		$output[]	= 	"Published = " . $this->published;
		$output[]	=	"</div>";
		
		echo implode( "<br />\r\n", $output );		
		
	}
	
	

	
	
	
	
	/*
	 * 	===================================================================
	 * 
	 * 			GETTERS
	 */
	public function get_issue()
	{
		
		return $this->issue;
		
	}
	public function get_id()
	{
		
		return $this->id;
		
	}
	public function get_name()
	{
		
		return $this->name;
		
	}
	public function get_number()
	{
		
		return $this->number;
		
	}
	public function get_date_added()
	{
		
		return $this->date_added;
		
	}
	public function get_date_published()
	{
		
		return $this->date_published;
		
	}
	public function get_published()
	{
		
		return $this->published;
		
	}
	
	
	
	
	
	
	
	/*
	 * 	===================================================================
	 * 
	 * 			SETTERS
	 */
	public function set_number( $number )
	{
	
		//	Test inputs
		if ( $number == '' ) 				return false;
		//if ( !CORE_is_number( $number ) ) 	return false;	
		
		$this->number	=	$number;
		return true;
		
	}
	public function set_name( $name )
	{
	
		//	Test inputs
		if ( !is_string( $name ) ) 			return false;	
		
		$this->name		=	$name;
		return true;
		
	}
	public function set_date_added( $date )
	{
		
		// Test inputs
		if ( $date == '' )				return false;
		
		$datetime	=	new date_time( $date );
		if ( $datetime->is_sql_datetime( $date ) )
		{
			
			$this->date_added	=	$date;
			return true;
				
		}
		else
		{
			
			return false;
			
		}
		
	}
	public function set_date_published( $date )
	{
		
		// Test inputs
		if ( $date == '' )				return false;
		
		$datetime	=	new date_time( $date );
		if ( $datetime->is_sql_datetime( $date ) )
		{
			
			$this->date_published	=	$date;
			return true;
				
		}
		else
		{
			
			return false;
			
		}
		
	}
	public function set_published( $user )
	{
	
		//	Test inputs
		if ( $user == '' ) 				return false;
		if ( !CORE_is_number( $user ) ) 	return false;	
		
		$this->published	=	$user;
		return true;
		
	}
	
	
	
	
}