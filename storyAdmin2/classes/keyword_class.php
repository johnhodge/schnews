<?php


class keyword
{
	protected $id;
	protected $keyword;
	
	
	function __construct( $id = 0 )
	{
		
		//	Test inputs
		if ( !CORE_is_number( $id ) )
		{
			return false;
		}		
		$this->id 	=	$id;
		
		if ( !$this->get_keyword() )
		{
			return false;
		}
		
		return true;
		
	}
	
	protected function get_keyword()
	{
		
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT keyword
						FROM keywords
						WHERE id = " . $this->id;
		$mysql->query( $sql );
		
		if ( $mysql->get_num_rows() != 1 )
		{
			return false;
		}
		
		$keyword		=	$mysql->get_row();
		$this->keyword	=	$keyword['keyword'];
		return true;
		
	}
	
	
	
	public function get_number_of_stories_for_keyword()
	{
		
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT id 
						FROM keyword_mappings
						WHERE keyword = " . $this->id;
		$mysql->query( $sql );
		return $mysql->get_num_rows();
		
	}
	
	
	
	public function record_new_keyword ( $new_keyword )
	{
		
		/*
		 * Test input
		 */
		if ( !is_string( $new_keyword ) || trim( $new_keyword ) == '' ) return false;
		
		
		
		/*
		 * Insert into database
		 */
		$mysql	=	new mysql_connection();
		$sql	=	"	INSERT INTO keywords
						(
							keyword
						)
						VALUES
						(
							'" . $mysql->clean_string( $new_keyword ) . "'
						)	";
		$mysql->query( $sql );
		
		
		
		/*
		 * Do some logging
		 */
		$message	=	"Keyword addition entered into database";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message );
		
		return true;
		
	}
	
	
	public function record_change( $new_keyword )
	{

		//	Test inputs
		if ( !is_string( $new_keyword ) || trim( $new_keyword ) == '' ) return false;
		
		
		$mysql	=	new mysql_connection();
		$sql	=	"	UPDATE keywords
						SET
							keyword = '" . $mysql->clean_string( $new_keyword ) . "'
						WHERE
							id = " . $this->id;
		$mysql->query( $sql );
		
		/*
		 * Do some logging
		 */
		$message	=	"Keyword modification entered into database";
		debug ( $message, __FILE__, __LINE__ );
		LOG_record_entry( $message );
		
		return true;		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * ===================================================
	 * 					GETTERS
	 */
	public function get_id()
	{
		return $this->id;
	}
	public function get_name()
	{
	
		return $this->keyword;
	
	}
	
}