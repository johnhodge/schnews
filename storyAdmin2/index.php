<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This is the main index file of the system
 *
 * 	Its purpose is:
 *
 *  	1) Flow control - all pages are called by this one
 *  	2) Central plpace for all includes and constant definitions
 *  	3) Simplify login handling - individual pages don't need to worry about logins becuase it is handled by this page for them
 *  	4) All other pages can refernce their file system paths in reference to this one, so they don't need to wory where they are in the file system heirarchy
 *
 */

session_start();









/*
 * 	============================================================================================================================================================
 *
 * 		INTRODUCTION
 *
 * 		Here we load any includes/requires, and then draw the HTML headers.
 *
 * 		Also part of this section si the login script. This check to see if the user is logged and draw the login form if not (also handles the login process if a successful login is conducted).
 * 		The login script is here because it is essentially a static process. As far as this page is concerned it just calls one script, with no parameters, so it may as well be part of the headers.
 *
 * 	============================================================================================================================================================
 */

//	Get other required PHP files
require "conf/conf.php";
require "lib/aw_lib.php";
require "functions/functions.php";



//	Load the general story settings
SETTINGS_load_story_settings();


//	Draw the HTML header
require "inc/html_header.php";



//	Run the Garbage Collector
require "scripts/garbage_collection.php";



//	Do we need to send a summary email report
require"inc/summary_email.php";





/*
 * 	============================================================================================================================================================
 *
 * 		MAIN FLOW CONTROL
 *
 * 		This section will detect which page has been called via the GET superglobal, and then laod the appropriate page
 *
 * 	============================================================================================================================================================
 */



//	Check for special cases
require "scripts/flow_control/check_for_special_cases_non_logged_in.php";



//	Make sure the user is logged in
require "scripts/login.php";






/*
 * 	Determine which page we're on
 */
$page	=	'stories';
require "scripts/possible_pages.php";
if (isset($_GET['page']) && in_array($_GET['page'], $possible_pages))
{
	$page = $_GET['page'];
}

define('PAGE', $page);



//	Call the Nav Bar
require "inc/main_nav_bar.php";



switch (PAGE)
{
	default:
		require "pages/stories/stories.php";
		break;

	case 'issues':
		require "pages/issues/issues.php";
		break;
		
	case 'keywords':
		require "pages/keywords/keywords.php";
		break;

	case 'homepage':
		require "pages/homepage/homepage.php";
		break;
		
	case 'comments':
		require "pages/comments/comments.php";
		break;
		
	case 'settings':
		require "pages/settings/settings.php";
		break;
}






/*
 * 	============================================================================================================================================================
 *
 * 		END PAGE AND FOOTERS
 *
 * 		This will close any resources, log anything that needs loging, and draw the HTML footers.
 *
 * 	============================================================================================================================================================
 */
//	Draw the HTML footer
require "inc/html_footer.php";



