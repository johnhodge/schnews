<?php
/*
 *	NOTE: THIS FEATURE IS NOT YET IMPLEMENT
 *
 * 	This is a hook so that other CSS sets could be loaded in depending on what it in the 
 * 	$css_mode var.
 * 
 * 	Default is 'display'
 * 
 * 	Others could be 'print' or 'mobile' etc.
 */
$allowed_css_modes		=	array( 'display' );
if ( !isset( $css_mode ) )
{
	
	$css_mode = 'display';
	
}


$output		=	array("\r\n\r\n<!-- BEGIN " . strtoupper( $css_mode ) . " CSS -->" );
if( is_dir( 'css/' . $css_mode ) && $dir_handle		=	opendir( 'css/' . $css_mode ) )
{

	while ( $filename =  readdir( $dir_handle ) )
	{
		
		if ( $filename == '.' || $filename == '..' ) continue; 	//	Exclude file system entities
		
		$output[]		=	"<link rel=\"stylesheet\" href=\"css/$css_mode/$filename\" type=\"text/css\" />";

	}	
	
}

sort( $output );
$output[]		=	"<!-- END " . strtoupper( $css_mode ) . " CSS -->\r\n\r\n";
echo implode( "\r\n", $output );
