<div class='section_container'>

<div class='big_error_message'>

<b>Something has gone horribly wrong.</b>
<br /><br />
Everybody has been emailed who should be, and a general panic will now ensue.

</div>

</div>

<?php

	LOG_record_entry( "error.php displayed", 'error', 2, true );
	LOGIN_log_user_out();
	require "inc/html_footer.php";

?>