<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Display the login form to the user
 */
?>




<?php

//	Get the GET vars so we can include them in the target array
$get	=	URL_get_GET_vars();
if (isset($get['logout']))
{
	unset ($get['logout']);
}
$url_append 	=	URL_make_url_from_get($get);

?>



<!-- BEGIN LOGIN FORM -->
<div class='login_form'>

	<b>You Must Login:</b>
	<br />
	<br />
	<?php

		//	If there are any login errors from a failed login attempt then show them
		if (isset($login_errors))
		{

			foreach ($login_errors as $error)
			{

				echo "\r\n<div class='error_message'>\r\n".$error."\r\n</div>\r\n";

			}
			echo "\r\n<br />\r\n";

		}


	?>
	<form method='POST' action='index.php<?php echo $url_append; ?>'>

		<table>
			<!-- USERNAME -->
			<tr>
				<td class='login_table_cell'>
					Username:
				</td>
				<td class='login_table_cell'>
					<input type='text' name='username' class='login_textbox' />
				</td>
			</tr>

			<!-- PASSWORD -->
			<tr>
				<td class='login_table_cell'>
					Password:
				</td>
				<td class='login_table_cell'>
					<input type='password' name='password' class='login_textbox' />
				</td>
			</tr>

			<!-- SUBMIT BUTTON -->
			<tr>
				<td>
				</td>
				<td class='login_table_cell'>
					<input type='submit' value='Login' class='login_submit' />
				</td>
			</tr>
		</table>

	</form>

</div>
<!-- END LOGIN FORM -->


