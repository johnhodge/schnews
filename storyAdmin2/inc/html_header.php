<?php
if (!LOGIN_is_user_logged_in())
{

	// If the user is not logged in then log a few things...
	LOG_record_entry("New user connection - login form presented" );

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<!-- BEGIN HTML HEADER -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>SchNEWS - StoryAdmin Editorial System</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	
	<?php 
	
	/*
	 * 	Load in the set of stylesheets.
	 * 
	 * 	Defaults to display
	 */
	require "inc/load_css.php";
	
	?>
	
	<script language="JavaScript"  type="text/javascript" src='js/jquery.js'></script>
	<script language="JavaScript"  type="text/javascript" src='js/jquery_functions.js'></script>
	<script language="JavaScript"  type="text/javascript" src='js/popups.js'></script>
	<script language="JavaScript"  type="text/javascript" src='js/functions.js'></script>
	<script type="text/javascript" language='JavaScript' src="ckeditor/ckeditor.js"></script>
</head>
<!-- END HTMl HEADER -->

<!-- BEGIN HTML BODY -->
<body>

	<noscript>
		<div class='header_warning'>
			Javascript not detected - without Javascript enabled some features might not work properly
		</div>	
	</noscript>
	
	<?php 
	
	if ( isset( $_SESSION['header_message_perm'] ) && $_SESSION['header_message_perm'] != '' )
	{
		
		echo "<div class='header_notice'>" . $_SESSION['header_message_perm'] . '</div>';
		
	}
	
	if ( isset( $_SESSION['header_message_temp'] ) )
	{
		
		echo "<div class='header_notice'>" . $_SESSION['header_message_temp'] . '</div>';
		unset( $_SESSION['header_message_temp'] );
		
	}
	
	?>
	

	<?php

		require "scripts/test_for_login.php";
		require "scripts/test_for_logout.php";


	?>


	<table width='100%'>
		<tr>
			<td>
				<img src='../images_main/schnews.gif' style='padding:0px; margin: 0px'/>
			</td>
			<td>
				<div class='header_login_status'>
					<?php
						require "scripts/display_login_status.php";
					?>

				</div>
			</td>
		</tr>
	</table>

