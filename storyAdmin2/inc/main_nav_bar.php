<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Simple navigation bar for the top of the pages
 */





/*
 * Prepare Javascript
 */
$js		=	"";
if ( isset( $_GET['edit_story'] ) && CORE_is_number( $_GET['edit_story'] ) )
{
	
	$js	=	" onclick='return show_save_warning()' ";
	
}




//	Begin output buffer
$output		=	array();

$output[]	=	"\t<!-- BEGIN MAIN NAVBAR -->";
$output[]	=	"<table class='navbar_container'>";
$output[]	=	"<tr>";



/*
 * $possible_pages is defined in /scripts/possible_pages
 */
foreach ($possible_pages as $possible_page)
{
	$class = 'navbar_entry';
	if ($possible_page == PAGE)
	{
		$class	=	'navbar_entry_selected';
	}

	$output[]	=	"\t<td>";
	$output[]	=	"\t<a href='index.php?page=$possible_page' $js >";
	$output[]	=	"\t<div class='$class'>";
	$output[]	=	"\t".strtoupper($possible_page);
	$output[]	=	"\t</div>";
	$output[]	=	"\t</a>";
	$output[]	=	"\t</td>";
}

$output[]	=	"</tr>";
$output[]	=	"</table>";
$output[]	=	"<!-- END MAIN NAVBAR -->";
$output[]	=	"\r\n\r\n";

echo implode("\r\n\t", $output);