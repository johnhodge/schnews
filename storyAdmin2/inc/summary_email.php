<?php

$week	=	3600 * 24 * 7;

$mysql	=	new mysql_connection();
$sql	=	"	SELECT id
				FROM email_reports
				WHERE email_sent > '" . DATETIME_make_sql_datetime( date( "U" ) - $week ) . "'";
$mysql->query( $sql );

if ( $mysql->get_num_rows() == 0 )
{

	debug ( "SENDING REPORT EMAIL", __FILE__, __LINE__ );
	require "inc/make_summary_email.php";	
	
}
else
{
	
	debug ( "NOT SENDING REPORT EMAIL", __FILE__, __LINE__ );
	
}

