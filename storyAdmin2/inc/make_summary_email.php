<?php

/*
 *	Show a summary of each story 
 */

$mysql	=	new mysql_connection();
$sql	=	"	SELECT id
				
				FROM stories
				
				WHERE
					live = 1
				AND hide <> 1
				AND deleted <> 1 
				
				ORDER BY 
					date_live DESC
					
				LIMIT 0, 20
				";
$mysql->query( $sql );


$output	=	array();
while( $res	=	$mysql->get_row() )
{
	
	$story	=	new story( $res['id'] );
	$text	=	$story->get_headline() . " \t\t :: " . $story->get_hits() . ' hits - ' . FORMAT_get_number_of_comments( $res['id'] ) . ' comments.';
	
	$user	=	new user( $story->get_author() );
	$date	=	new date_time( $story->get_date_live() );
	$text 	.=	"\r\n\tAdded by " . $user->get_name() . ". Voted live on " . $date->make_human_date();
	
	$output[]	=	$text;
	
}
$output	=	implode("\r\n\r\n", $output );


$stories	=	"Information about the most recent 20 stories:\r\n\r\n\r\n" . $output . "\r\n\r\n\r\n\r\n";











/*
 * 	Make a report about any non-live stories
 */
$sql	=	"	SELECT id
				
				FROM stories
				
				WHERE 
					live <> 1
				AND deleted <> 1
				
				ORDER BY
					date_added DESC
					
				LIMIT 0, 20
					
			";
$mysql->query( $sql );


$temp	=	array();
while( $res	=	$mysql->get_row() )
{
	
	$story	=	new story( $res['id'] );
	$text	=	$story->get_headline();
	
	$user	=	new user( $story->get_author() );
	$date	=	new date_time( $story->get_date_added() );
	$text	.=	"\r\n\tAdded by " . $user->get_name() . ". Added on " . $date->make_human_date();
	
	$temp[]	=	$text;	
	
}
$temp	=	implode( "\r\n\r\n", $temp );

$nonlive	=	"The following stories are in the system, but are not live as they have insufficient votes:\r\n\r\n\r\n" . $temp . "\r\n\r\n\r\n\r\n";









/*
 * Stories with comments
 */
$sql	=	"	SELECT id
				
				FROM stories
				
				WHERE
					live = 1
				AND hide <> 1
				AND deleted <> 1 
				
				ORDER BY 
					date_live DESC
					
				LIMIT 0, 20
				";
$mysql->query( $sql );


$ids	=	array();
while( $res	=	$mysql->get_row() )
{

	$ids[]	=	$res['id'];
	
}


$temp	=	array();
foreach( $ids as $id )
{
	
	if ( FORMAT_get_number_of_comments( $id ) > 2 )
	{
		
		$story	=	new story( $id );
		$text	=	$story->get_headline() . "\r\n\r\n";
		
		$sql	=	"	SELECT id
						FROM comments
						WHERE 
							story = " . $id . "
						AND deleted <> 1
						AND	spam_score < " . spam_points_quarantine . "
						ORDER BY date ASC ";
		$mysql->query( $sql );
		
		while ( $res = $mysql->get_row() )
		{
			
			$comment 	=	new comment( $res['id'] );
			$date		=	new date_time( $comment->getDate() );
			$text		.=	"Added by " . $comment->getAuthor() . " on " . $date->make_human_date() . "\r\n";
			$text		.=	$comment->getContent() . "\r\n\r\n";
			
		}
		
		$temp[]	=	$text;
		
	}
	
}
$comments 	=	implode( "\r\n\r\n\r\n", $temp );


$comments	=	"The following stories have received multiple comments...\r\n\r\n\r\n" . $comments;











/*
 * 	Compile the full message body
 */
$body	=	"Hello,


Here is a report about the recent stories in the system....\r\n\r\n\r\n\r\n";
$body	.=	$stories . "\r\n\r\n------------------------------\r\n\r\n\r\n" . $nonlive . "\r\n\r\n------------------------------\r\n\r\n\r\n" . $comments;






if ( DEBUG )
{
	
	echo nl2br( $body );
	exit;
	
}






/*
 * 	Send the email
 */
$subject	=	"SchNEWS StoryAdmin :: Semi-Regular Report";
$mail		=	new email( $body, $subject, internal_mailing_list_address );
$mail->send();






/*
 * 	Update the timestamp in the database
 */
$sql	=	"
				INSERT INTO email_reports
				(
					email_sent
				)
				VALUES
				(
					'" . DATETIME_make_sql_datetime() . "'
				)
			";
$mysql->query( $sql );




