
<form method='post' action='?page=comments&type=<?php echo strtolower( $type ); ?>' >

<div class='COMMENTS_list_submit_buttons'>




<?php 
	
	/*
	 * 	Draw the "Mark as Spam" and "Delete Selected" buttons...
	 */
	require "pages/comments/draw_buttons.php";

?>


<?php 
$search 	=	'';
if ( isset( $_POST['comments_search'] ) )
if ( $_SESSION['comments_search'] )
{
	
	$search 	=	$_SESSION['comments_search'];
	
}
?>




<table class='COMMENTS_list_table' align='center'>
	<tr>
		<td style='padding-right: 8px'><b style='color: blue' id='jq_select_all'>Select&nbsp;All</b></td>
		<td>&nbsp;</td>
		<td class='COMMENTS_list_header'>Comment</td>
		<td>&nbsp;</td>
		<td class='COMMENTS_list_header'>Story</td>
		<td>&nbsp;</td>
		<td class='COMMENTS_list_header'>Date</td>
		<td>&nbsp;</td>
		<td class='COMMENTS_list_header'>Status</td>
		<td>&nbsp;</td>
		<td class='COMMENTS_list_header'>Spam Score</td>	
	</tr>
	
	<tr>
		<td>&nbsp;</td>
	</tr>

	<?php 
	
	foreach ( $ids as $id )
	{
		
		$comment	=	new comment( $id );
		$story		=	new story( $comment->getStory_id() );
		$date		=	new date_time( $comment->getDate() );
		
		$class		=	"COMMENTS_list_cell";
		if ( $comment->getSpam() == 1 )
		{
			
			$class 	=	"COMMENTS_list_cell_spam";
			
		}
				
		?>

		<tr>
		
			<td align='center'><input type='checkbox' name='comment_<?php echo $id; ?>' class='jq_tick_all_checkbox' /></td>
		
			<td>&nbsp;</td>
			
			<!-- Comment -->
			<td class='<?php echo $class; ?>'>
			
				<?php echo htmlentities( $comment->getContent() ); ?>
			
			</td>	
			
			<td>&nbsp;</td>
			
			<!-- Story -->
			<td class='<?php echo $class; ?>'>
			
				<?php echo htmlentities( $story->get_headline() ); ?>
			
			</td>
			
			<td>&nbsp;</td>
			
			<!-- Date -->
			<td class='<?php echo $class; ?>'>
			
				<?php echo $date->make_human_date(); ?>
			
			</td>
			
			<td>&nbsp;</td>
			
			<!-- Status -->
			<td class='<?php echo $class; ?>'>
			
				<?php 

				$status	=	"Live";
				if ( $comment->getSpam()	== 	1 ) $status	=	"Spam";
				if ( $comment->getDeleted()	==	1 ) $status = 	"Deleted";
				echo $status;
				
				?>
			
			</td>
			
			<td>&nbsp;</td>
			
			<!-- Spam Score -->
			<td class='<?php echo $class; ?>'>
			
				<?php 

					if ( $comment->getSpam_score() > 0 )
					{

						echo htmlentities( $comment->getSpam_score() . ' - [ ' . $comment->getSpam_reasons() . ' ] ' ) ;
						
						
					}
					else
					{
						
						echo "&nbsp;";
						
					}
				
				?>
			
			</td>
		
		
		</tr>
		
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		
		<?php 
		
	}
	
	?>

</table>
</form>