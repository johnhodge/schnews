<?php


/*
 * 	This page runs an SQL command to delete all Spam comments.
*
* 	It will delete from the database, display a notice on screen and then pass control back to the calling page.
*/


//	Delete comments
$mysql	=	new mysql_connection();
$sql	=	"	DELETE FROM comments
				WHERE deleted = 1 ";
$mysql->query( $sql );


//	Delete comments hashes
$mysql	=	new mysql_connection();
$sql	=	"	DELETE * FROM comments_ip_hashes";
$mysql->query( $sql );



$output		=	"All Deleted Comments have been permanently removed.";