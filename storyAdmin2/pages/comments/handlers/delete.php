<?php

$count		=	0;


foreach ( $ids as $id )
{
	
	if ( isset( $_POST[ "comment_" . $id ] ) )
	{
		
		$comment		=	new comment( $id );
		$comment->setDeleted( 1 );
		$comment->update_db();

		$count++;
		
	}
	
}



if ( $count > 0 )
{
	
	$message 	=	"$count comment messages deleted, by " . $_SESSION['logged_in_name'.SESSION_APPEND] ;
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, "comments" );
	
}