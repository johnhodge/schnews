<?php

$spam_score	=	spam_points_delete - spam_points_quarantine;
$count		=	0;


foreach ( $ids as $id )
{
	
	if ( isset( $_POST[ "comment_" . $id ] ) )
	{

		$comment		=	new comment( $id );
		$new_score 		=	$comment->getSpam_score() + $spam_score;
		$comment->setSpam_score( $new_score );
		
		$spam_reason	=	array();
		if ( $comment->getSpam_reasons() != '' )
		{
			
			$spam_reason[] 	=	$comment->getSpam_reasons();
			
		}
		$spam_reason[]	=	"$spam_score - Moderated by User";
		$comment->setSpam_reasons( implode( ", ", $spam_reason ) );

		$comment->update_db();

		$count++;
		
	}
	
}



if ( $count > 0 )
{
	
	$message 	=	"$count comment messages marked as spam, by " . $_SESSION['logged_in_name'.SESSION_APPEND] ;
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, "comments" );
	
}