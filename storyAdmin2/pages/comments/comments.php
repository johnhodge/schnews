<?php

require "pages/comments/navbar.php";

if ( isset( $_GET['type'] ) && $_GET['type'] == 'keywords' )
{
	
	require "pages/comments/keywords/keywords.php";
		
}
else
{
	
	require "pages/comments/get_sql_ids.php";
	require "pages/comments/handlers/handlers.php";
	require "pages/comments/display_list.php";

}