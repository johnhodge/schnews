<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th October 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Draws a list of all the issues
 *
 */



/*
 * Generate a list of the all the issues ids o e displayed...
 */ 
$mysql	=	new mysql_connection();
$sql	=	"	SELECT id 
				FROM issues
				ORDER BY date_added DESC ";
$mysql->query( $sql );

$ids	=	array();
while ( $res  =  $mysql->get_row() )
{
		
	$ids[]	=	$res['id'];
	
}




/*
 * Draw the table headers...
 */
?>


<!-- BEGIN LIST OF ISSUES -->

<table align='center' >
	
	<!-- HEADERS -->
	<tr>
		<td class='issues_list_issues_header'>Issue Number</td>
		<td>&nbsp;</td>
		<td class='issues_list_issues_header'>Date Added</td>
		<td>&nbsp;</td>
		<td class='issues_list_issues_header'>Published</td>
		<td>&nbsp;</td>
		<td class='issues_list_issues_header'># of Stories</td>
	</tr>

	<?php 
	/*
	 * Draw each issue entry as a table row
	 */
	foreach ( $ids as $id )
	{
		
		$issue			=	new issue( $id );
		$num_stories	=	ISSUES_get_num_stories_for_issue( $id );
		$date_added		=	new date_time( $issue->get_date_added() );
		if ( $issue->get_published() )
		{
			$date		=	new date_time( $issue->get_date_published() );
			$published	=	"Yes [ " . $date->make_human_date() . " ] ";
		}		
		else
		{
			$published	=	"<b>No</b>";
		}
		?>
			<tr>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td class='issues_list_issues_cell'><?php echo $issue->get_number(); ?></td>
				<td>&nbsp;</td>
				<td class='issues_list_issues_cell'><?php echo $date_added->make_human_date(); ?></td>
				<td>&nbsp;</td>
				<td class='issues_list_issues_cell'><?php echo $published; ?></td>
				<td>&nbsp;</td>
				<td class='issues_list_issues_cell'><?php echo $num_stories; ?></td>
				
				<?php 
				if ( $_SESSION['logged_in_id'.SESSION_APPEND] == 2 )
				{
				?>
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td><a href='?page=issues&edit_issue=<?php echo $id; ?>'> [ EDIT ] </a></td>
				<?php 
				}
				?>
			</tr>
		
		<?php 
		
	}
	
	?>

</table>

<!-- END LIST OF ISSUES -->

