<?php


/*
 *	TEST PDF 
 */
$issue	=	new issue( $issue_id );

if ( $_FILES['pdf']['error'] > 0 )
{
	
	$pdf_errors[]	=	"Error Uploading PDF";
	
}
else
{
	
	$target		=	"../issues/pdfs/" . $issue->get_number() . ".pdf";
	
	if ( !move_uploaded_file( $_FILES['pdf']['tmp_name'] , $target ) )
	{
		
		$pdf_errors[]	=	"Error Processing Uploaded PDF File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$pdf_errors[]	=	"Error setting permissions for new PDF file";
	
	}

	$pdf_errors	=	"New PDF Uploaded";
	
}

require "pages/issues/edit_issue.php";

