<?php

$issue		=	new issue( $_GET['publish_issue'] );
$current_issue_number		=	$issue->get_number();
$new_issue_number			=	$current_issue_number + 1;



/*
 * 	Send Email to PT Mailing List
 */
$email_body		=	ISSUES_make_plain_text_list_email_body( $issue->get_id() );
$email_subject	=	ISSUES_make_plain_text_list_email_subject( $issue->get_id() );
$mail			=	new email( $email_body, $email_subject, plain_text_mailing_list );
$mail->send();



/*
 *	Update the current issue 
 */
$issue->set_published( "1" );
$issue->set_date_published( DATETIME_make_sql_datetime() );
$issue->update_issue_in_db();



/*
 * Make a new issue
 */
$new_issue	=	new issue();
$new_issue->set_date_added( DATETIME_make_sql_datetime() );
$new_issue->set_number( $new_issue_number );
$new_issue->update_issue_in_db(); 



/*
 * Do some logging
 */
$message	=	"Issue $current_issue_number has been published. New issue $new_issue_number has been created. This was done by user " . $_SESSION['logged_in_name'.SESSION_APPEND];
debug( $message, __FILE__, __LINE__ );
LOG_record_entry( $message );


