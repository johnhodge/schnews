<?php

if ( email_list_on_issue_publish )
		{
			
			$subject	=	"SchNEWS StoryAdmin :: Issue $current_issue_number Complete";
			$message	=	file_get_contents( "email_templates/issue_published_mailing_list" );
			$message	=	str_replace( "***OLD_ISSUE_NUMBER***", $current_issue_number, $message );
			$message	=	str_replace( "***NEW_ISSUE_NUMBER***", $new_issue_number, $message );
			$message	=	str_replace( "***ISSUE_NUM_STORIES***", ISSUES_get_num_stories_for_issue( $issue->get_id() ), $message );
			$message	=	str_replace( "***USER***", $_SESSION['logged_in_name'.SESSION_APPEND], $message );
			$mail		=	new email( $message, $subject, internal_mailing_list_address );
			$mail->send();
			
			echo nl2br( $message );
			
			?>
			
			<div class='explanatory_text_large'>
				The internal mailing list has been notified.
			</div>
				
			<br /><br />
			
			<?php 
		}
		else
		{
			
			?>
			
				No email notices have been sent.
			
			<?php 
		}
		
	
		

	
	
	
