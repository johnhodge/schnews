<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th October 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Flow control for issue editing
 *
 */
?>



<!-- BEGIN ISSUES SECTION -->

<div class='section_container'>

<div class='section_title'>
ISSUES
</div>

<?php


/*
 * 	Test the GET vars to see which page should be loaded.
 */
$page		=	'list_issues';



/*
 * 	Test for Edit Issue
 */
if ( isset( $_GET['edit_issue'] ) && CORE_is_number( $_GET['edit_issue'] ) )
{
	
	$issue_id		=	$_GET['edit_issue'];
	$page			=	'edit_issue';
	
}




if ( isset( $_GET['upload_pdf'] ) && CORE_is_number( $_GET['upload_pdf'] ) )
{
	
	$issue_id 	=	$_GET['upload_pdf'];
	$page		=	'upload_pdf';
	
}


/*
 * 	Test for Rename Issue
 */
if ( isset( $_GET['rename_issue'] ) && CORE_is_number( $_GET['rename_issue'] )  )
{
	
	$issue_id		=	$_GET['rename_issue'];

	if( isset( $_POST[ 'submit' ] ) )
	{
		
		$page 	=	'do_rename_issue';
		
	}
	else
	{
	
		$page			=	'rename_issue';
		
	}
	
}



/*
 * 	Test for publish issue
 */
if ( isset( $_GET['publish_issue'] ) && CORE_is_number( $_GET['publish_issue']) )
{
	
	$page 	=	'publish';
	
}





/*
 * 	Load in the relevant page...
 */
switch ( $page )
{
	default:
		require "pages/issues/list_issues.php";
		break;
		
	case 'edit_issue':
		require "pages/issues/edit_issue.php";
		break;
		
	case 'rename_issue':
		require "pages/issues/rename_issue.php";
		break;
		
	case 'do_rename_issue':
		require "pages/issues/do_rename_issue.php";
		break;

	case 'publish':
		require "pages/issues/publish.php";
		break;
	
	case 'upload_pdf':
		require "pages/issues/upload_pdf.php";
		break;
			
}



?>

</div>

<!--  END STORIES SECTION -->


