<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Draws the form to edit an issue details...
 *
 */




/*
 * 	There is no way that this wold not be set unless someone is calling the script
 * 	direct rather than clicking through the correct links.
 * 
 * 	(If that is the case though it will fail with a fatal error as CORE_is_number wont be a recognised function)
 */
if ( !isset( $issue_id ) || !CORE_is_number( $issue_id ) )
{
	
	$message	=	"issue_id not set. Should not be possible. Someone is calling the script pages/issues/edit_issue.php who should not be doing so.";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'security', 1 );
	
	require "inc/html_footer.php";
	exit;
	
}



/*
 * 	Only an admin should see this page
 */
if ( $_SESSION['logged_in'.SESSION_APPEND] != 2 )
{
	
	echo "Sorry, You Should Not Be Able to View This Page";
	$message = "Non-Admin accessed Edit Issue Page. User ID = " . $_SESSION['logged_in_id'.SESSION_APPEND];
	LOG_record_entry( $message, 'security', 1 );
	debug ( $message, __FILE__, __LINE__ );
	require "inc/html_footer.php";
	exit;	
	
	
}

/*
 * 	Prepare some date for display later.
 */


//	Instantiate new issue object
$issue		=	new issue( $issue_id );


//	Make a page title
$title		=	"Editing Issue " . $issue->get_number();
if ( $issue->get_name() != '' )
{
	
	$title 	.=	" <br />(" . $issue->get_name() . ")";
	
}
$title		=	strtoupper( $title );


//	Add the dateline
$date		=	new date_time( $issue->get_date_added() );
$title		.=	"\r\n\r\n<br /><br /><span class='issues_edit__issue_dateline'>\r\nAdded on <b>" . $date->make_human_date() . "</b>\r\n</span>\r\n\r\n";


//	Explanatory line showing how many stories there are for the issue...
$num_line	=	"There are " . ISSUES_get_num_stories_for_issue( $issue->get_id() ) . " stories for this issue.";


//	Instruction on how an issue is published
if ( $issue->get_published() )
{
	
	$date				=	new date_time( $issue->get_date_published() );
	$published_line 	=	"<b style='font-size: 13pt'>This Issue was Published on " . $date->make_human_date() . '</b>';
	
}
else
{
	
	$published_line		=	"The Issue is <b><u>NOT</u></b> yet published. This means that stories are still being added to it, and it has yet to be compiled as a finished issue or printed PDF.
	<br /><br />You can published this issue using the links below. This will cause the issue to be marked as completed, and various emails will be sent as a result.
	<br /><br />After you do this a new issue will be created and any new stories will be added to that.";
	
}




/*
 * Display the explanatory text...
 */
?>
<!-- BEGIN EDIT ISSUE -->
<div class='section_subtitle'>
	<?php echo $title; ?>
	
</div>

<div class='explanatory_text'>
	<?php echo $num_line; ?>
	<br /><br />
	<?php echo $published_line; ?>
	
	<?php 
		/*
		 * 	If we have successfully renamed the issue...
		 */
		if ( isset( $rename_message ) )
		{
			
			?>
				
				<br /><br /><br />
				<div class='big_notice_message'>
					<?php echo $rename_message; ?>
				</div>
			
			<?php 
			
		}
	
	?>
	
</div>




<?php 
/*
 *	Draw the PDF form 
 */
?>
<div class='issues_pdf_form'>
	<b>Upload PDF for Issue</b>
	<form method='POST' action='?page=issues&upload_pdf=<?php echo $issue->get_id(); ?>' enctype="multipart/form-data">
		<input type='file' name='pdf' />
		<input type='submit' value='Upload PDF' />
	</form>
	
	<?php 
	
		$path	=	"../issues/pdfs/" . $issue->get_number() . ".pdf";
		if ( file_exists( $path ) )
		{
			
			?>

				<br />There is a PDF currently uploaded, <a href='<?php echo $path; ?>'>here</a>
			
			<?php 			
		}
	
	?>
</div>





<?php
/*
 * Draw the publish link if necessary
 */ 
if ( $issue->get_published() )
{


}
else
{
	?>	

	<div class='issues_publish_issue_button'>
		<a href='?page=issues&publish_issue=<?php echo $issue->get_id(); ?>' class='issues_publish_issue_button_text' onclick='return show_publish_issue_warning()' >
			This Issue Is Still Being Worked On - CLICK HERE to Publish It
		</a>	
	</div>

	<?php 
}	




/*
 * Draw the list of stories for this issue
 */
?>

<!-- BEGIN EDIT ISSUE -->


<!-- BEGIN ISSUE SECTION -->


<?php 
/*
 * 	End page
 */
require "inc/html_footer.php";
exit;

?>