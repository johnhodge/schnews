<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Draws the short form to rename an issue...
 *
 */




/*
 * 	There is no way that this wold not be set unless someone is calling the script
 * 	direct rather than clicking through the correct links.
 * 
 * 	(If that is the case though it will fail with a fatal error as CORE_is_number wont be a recognised function)
 */
if ( !isset( $issue_id ) || !CORE_is_number( $issue_id ) )
{
	
	$message	=	"issue_id not set. Should not be possible. Someone is calling the script pages/issues/rename_issue.php who should not be doing so.";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'security', 1 );
	
	require "inc/html_footer.php";
	exit;
	
}




/*
 * 	Prepare some date for display later.
 */


//	Instantiate new issue object
$issue		=	new issue( $issue_id );


//	Make a page title
$title		=	"Renaming Issue " . $issue->get_number();
$title		=	strtoupper( $title );







/*
 * Display the explanatory text...
 */
?>
<!-- BEGIN EDIT ISSUE -->
<div class='section_subtitle'>
	<?php echo $title; ?>
</div>

<div class='explanatory_text'>
	This will simply rename the (optional) issue name. 
	<br /><br />
	This will not affect any of the stories for this issue. The name is just used when displaying a compiled version of the issue.
	<br /><br />
	This is optional - if you don't want there to be a name for this issue then leave the text box empty / blank.
</div>



<!-- BEGIN RENAME FORM -->
<div class='issues_rename_issue_form_container'>

	
	<?php 
	
		/*
		 *	If a previous attempt to rename failed then there will be errors... 
		 */
		if ( isset($errors) && is_array( $errors ) )
		{

			foreach ($errors as $error )
			{
				
				?>
				
					<div class='error_message'>
						<?php echo $error; ?>
					</div>
				
				<?php 
				
			}
			
		}
		
	?>
	

	Current Name ::
	<?php 
		
		if( $issue->get_name() == '' )
		{
			
			$name 	=	"<b> [ NONE SPECIFIED ] </b>";
			
		}
		else
		{
			
			$name	=	"<b> " . $issue->get_name() . " </b>";
		}
		echo $name;
	
	?>
	
	<br /><br />
	
	<form method='POST' action='?page=issues&rename_issue=<?php echo $issue->get_id(); ?>'>

		New Name :: 
		
		<input type='text' style='width: 300px' name='name' value="<?php echo htmlentities( $issue->get_name() ); ?>" />
		&nbsp;&nbsp;
		<input type='submit' name='submit' value='Rename' />	
	
	</form>
	
	<form method='POST' action='?page=issues'>
	
	<div class='issues_rename_issue_cancel_button'>
		
		<input type='submit' name='cancel' value='Cancel, Back to List of Issues' />
		
	</div>

	</form>

</div>
<!-- END RENAME FORM -->



