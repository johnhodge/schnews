<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th October 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Publishs an issue
 *
 */



/*
 * Test inputs
 */
if ( !isset( $_GET['publish_issue'] ) || !CORE_is_number( $_GET['publish_issue'] ) )
{
	
	$message	=	"issues/publish.php called without valid input vars";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $messgae, 'error', 2 );
	
	require "inc/html_footer.php";
	exit;
	
}



/*
 * 	Publish the issue
 */
require "pages/issues/publish/publish_issue.php";



/*
 * Output something to the user
 */
?>



<!--  BEGIN PUBLISH ISSUE -->

<div class='section_subtitle' style='height: 400px'>
	
	New Issue Added

</div>

	
</div>

<div class='explanatory_text'>
	
	<div class='explanatory_text_large'>
	
		ISSUE <?php echo $current_issue_number; ?> HAS BEEN PUBLISHED.
		<br /><br />
		A NEW ISSUE <?php echo $new_issue_number; ?> HAS BEEN CREATED
	
	</div>
	
	<br /><br />
	All new stories will from now on be added to issue <?php echo $new_issue_number; ?>.
		
	<br /><br />

	<?php 

		/*
		 * 	Do we need to send an email to the mailing list...
		 */
		require "pages/issues/publish/send_mailing_list_email.php";
	
	?>
	
	<a href='?page=issues&rename_issue=<?php echo $new_issue->get_id(); ?>'><b>Click Here</b></a> to give the new issue a name (optional)
	
	<br /><br />
	
	or <a href='?page=issues'><b>Click Here</b></a> to go back to the issues list.

</div>

<!-- END PUBLISH ISSUE -->


