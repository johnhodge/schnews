<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Processes an attempt to rename an issues, as submitted by rename_issue.php...
 *
 */




/*
 * 	There is no way that this wold not be set unless someone is calling the script
 * 	direct rather than clicking through the correct links.
 * 
 * 	(If that is the case though it will fail with a fatal error as CORE_is_number wont be a recognised function)
 */
if ( !isset( $issue_id ) || !CORE_is_number( $issue_id ) )
{
	
	$message	=	"issue_id not set. Should not be possible. Someone is calling the script pages/issues/do_rename_issue.php who should not be doing so.";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'security', 1 );
	
	require "inc/html_footer.php";
	exit;
	
}



/*
 * 	Make sure we have a working $name var
 */
if ( !isset( $_POST['name'] ) )
{
	
	$errors[]		=	"Name not properly set";
	require "pages/issues/rename_issue.php";
	require "inc/html_footer.php";
	exit;
	
}
else
{
	
	$name		=	$_POST['name'];
	
}




/*
 * 	Instantiate and manipulate object
 */
$issue		=	new issue( $issue_id );
$old_name	=	$issue->get_name();
$issue->set_name( $name );
$issue->update_issue_in_db();

if ( $name == '' )
{
	
	$rename_message		=	"Issue " . $issue->get_number() . " has had its name removed";	
	
}
elseif ( $old_name == '' )
{
	
	$rename_message		=	"Issue " . $issue->get_number() . " has had a name added";
	
}
elseif ( $name != $old_name )
{

	$rename_message		=	"Issue has been renamed from '<b>" . htmlentities( $old_name ) . "</b>' to '<b>" . htmlentities( $name ) . "</b>'";
	
}

if ( isset( $rename_message ) )
{

	LOG_record_entry( $rename_message . ". ID = " . $issue_id );
	
}




/*
 * 	Call back the issue summary form
 */
require "pages/issues/edit_issue.php";
require "inc/html_footer.php";
exit;

