<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th October 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script controls homepage settings, editable by admins.
 *
 * 	It basically handles flow control
 */



unset( $output );


/*
 * 	ADMIN ONLY
 * 	Test for admin login status.
 */
if ( $_SESSION['logged_in'.SESSION_APPEND] != 2 )
{

	require "pages/settings/homepage/user_not_allowed.php";

}



/*
 * 	If a submission is detected
 */
if ( isset( $_POST[ 'submit' ] ) && $_POST[ 'submit' ] == "Update" )
{

	require "pages/settings/homepage/test_submission.php";

}



/*
 * 	If a submission has been tested then see what happens next...
 */
if ( isset( $output ) && is_array( $output ) && count( $output ) > 0 && count ( $errors ) == 0 )
{

	require "pages/settings/homepage/do_update.php";

}







/*
 * 	DRAW FORM
 * 	Draw the settings form
 */
require "pages/settings/homepage/draw_form.php";



/*
 * END PAGE
 * Draw the HTML footer and end the page
 */
require "inc/html_footer.php";