<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	21th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Updates the database with the new settings info
 *
 */



/*
 * 	Generate an SQL string for the update, containing all the setting = value pairs
 */
$update_string 	=	array();
foreach ( $output as $field => $value )
{

	$update_string[]	=	" $field = '".$value."' ";

}
$update_string	=	implode ( ", ", $update_string );




/*
 * 	Open a MySQL connection and update the database
 */
$mysql		=	new mysql_connection();
$sql		=	"	UPDATE homepage_settings
					SET	$update_string
					";
$mysql->query( $sql );




/*
 * 	Do logging
 */
$message 	=	"Homepage Settings have been updated by " . $_SESSION['logged_in_name'.SESSION_APPEND];
$message	.=	". The new settings are: " . SETTINGS_get_homepage_settings_as_csv();
LOG_record_entry( $message, 'security', 1 );



/*
 * 	And email the system admin
 */
$subject		=	"SchNEWS StoryAdmin :: The Homepage Settings Have Been Changed";
$message		=	"The Homepage Settings have been updated by " . $_SESSION['logged_in_name'.SESSION_APPEND] . "

The new settings are:

" . SETTINGS_get_story_settings_as_text();
$email			=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
$email->send();



/*
 * 	Display notices to screen
 */
?>
<!-- BEGIN SETTINGS UPDATE NOTICE -->

<div class='big_notice_message'>
The Homepage Settings Have Been Updated
</div>
<div class='notice_message'>
A security log entry has been made and the System Admin has been emailed.
</div>

<!-- END SETTINGS UPDATE NOTICE -->

<?php 

/*
 * 	And tidy up before we leave the page
 */
unset($mysql);
unset ($email);

?>

