<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	20th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Tells the user that they are not allowed to view this page, logs a security log event, and closes the page.
 *
 */


//	Begin output buffer
	$output		=	array();
	$output[]	=	"<div class='section_container'>";



	//	Page title
	$output[]	=	"<div class='section_title'>";
	$output[]	=	strtoupper('You Are Not Entitle to See This Page');
	$output[]	=	'<br /><br />';
	$output[]	=	'This incident has been logged and reported';
	$output[]	=	"</div>";



	//	Flush output buffer
	$output[]	=	"</div>";
	echo implode("\r\n\t", $output);



	//	Log the incident
	LOG_record_entry( "User tried to access story settings when less than status 2. Username = '".$_SESSION['logged_in_name'.SESSION_APPEND]."'", 'security', 2 );



	//	Call the footer and end the page
	require "inc/html_footer.php";