<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This file acts as a flow control for the settings section.
 *
 * 	All it really does is work out which settings page is required and load it.
 *
 */



/*
 * 	ONLY ADMINS SHOULD BE ALLOWED TO SEE THIS PAGE
 */
$user 	=	 new user($_SESSION['logged_in_id'.SESSION_APPEND]);
if ($user->get_status() < 2)
{

	require "pages/settings/user_not_allowed.php";

}



/*
 *	Work out which settings page we are calling
 */
$settings_page 	=	'stories';
$possible_settings_pages		=	array(

	'stories',
	'homepage',
	'utilities',
	'users'

);
if (isset($_GET['settings_page']) && in_array($_GET['settings_page'], $possible_settings_pages))
{
	$settings_page = $_GET['settings_page'];
}
define('SETTINGS_PAGE', $settings_page);







//	Begin output buffer
$output		=	array();
$output[]	=	"<div class='section_container'>";



//	Page title
$output[]	=	"<div class='section_title'>";
$output[]	=	'SETTINGS'.' - '.strtoupper(SETTINGS_PAGE);
$output[]	=	"</div>";



//	Draw the setting NavBar
require ("pages/settings/navbar.php");



//	Flush output buffer
echo implode("\r\n\t", $output);


//	Call the particular page, depending on what is selected in the setting NavBar
switch (SETTINGS_PAGE)
{
	case 'stories':
		require 'pages/settings/stories/stories.php';
		break;
		
	case 'homepage':
		require "pages/settings/homepage/homepage.php";
		break;
		
	case 'utilities':
		require "pages/settings/utilities/utilities.php";
		break;

	case 'users':
		require 'pages/settings/users/users.php';
		break;
}



//	Flush output buffer
$output		=	array();
$output[]	=	"</div>";
echo implode("\r\n\t", $output);

