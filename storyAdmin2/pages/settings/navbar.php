<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Draw the navigation bar for the settings section
 */



//	Begin output buffer

$output[]	=	"\t<!-- BEGIN SETTINGS NAVBAR -->";
$output[]	=	"<table align='center' class='settings_navbar_container'>";
$output[]	=	"<tr>";

foreach ($possible_settings_pages as $possible_page)
{

	$class = 'settings_navbar_entry';
	if ($possible_page == SETTINGS_PAGE)
	{
		$class	=	'settings_navbar_entry_selected';
	}

	$output[]	=	"\t<td>";
	$output[]	=	"\t<a href='index.php?page=settings&settings_page=$possible_page'>";
	$output[]	=	"\t<div class='$class'>";
	$output[]	=	"\t".strtoupper($possible_page);
	$output[]	=	"\t</div>";
	$output[]	=	"\t</a>";
	$output[]	=	"\t</td>";


}

$output[]	=	"</tr>";
$output[]	=	"</table>";
$output[]	=	"<!-- END SETTINGS NAVBAR -->";



