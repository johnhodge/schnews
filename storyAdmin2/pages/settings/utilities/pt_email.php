<?php


/*
 * 	Generate the Email Body
 */
$mysql		=	new mysql_connection();
$sql		=	" 	SELECT id
					FROM issues
					WHERE 
						published = 1
					AND	date_published IS NOT NULL
					ORDER BY date_published DESC
					LIMIT 0,1 ";
$mysql->query( $sql );
$res 	=	$mysql->get_row();
$pt_email	=	ISSUES_make_plain_text_list_email_body( $res['id'] );
$pt_subject	=	ISSUES_make_plain_text_list_email_subject( $res['id'] );


/*
 * 	Output to screen
 */
echo "<div style='clear:both; font-family: 'courier', monospace'><b>Subject :: " . $pt_subject . "</b><br /><br /><br />" . nl2br( $pt_email ) . "</div>";



if ( isset( $_GET['send'] ) && $_GET['send'] == 'yes' )
{
	
	$mail	=	new email( $pt_email, $pt_subject, plain_text_mailing_list );
	$mail->send();	
	
}

