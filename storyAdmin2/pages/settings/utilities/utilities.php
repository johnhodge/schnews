<?php 


if ( isset( $_GET['action'] ) && !empty( $_GET['action'] ) )
{
	
	switch ( $_GET['action'] )
	{
		
		case 'pt_email':
			require "pages/settings/utilities/pt_email.php";
			require "inc/html_footer.php";
			exit;
			
		case 'export_stories':
			require "pages/settings/utilities/export_stories.php";
			require "inc/html_footer.php";
			exit;
		
	}
	
}


?>






<div style='clear: both; text-align: center; font-size: 13pt'>

<p>
	<a href='?page=settings&settings_page=utilities&action=pt_email&send=no'>
		Create Plain Text Email for Latest Published Issue	
	</a>
</p>


<p>
	<a href='?page=settings&settings_page=utilities&action=pt_email&send=yes'>
		Create and Send Plain Text Email for Latest Published Issue	
	</a>
</p>


<p>
	<a href='?page=settings&settings_page=utilities&action=export_stories&display=yes'>
		Export a CSV File of the Story List Info	
	</a>
</p>


</div>