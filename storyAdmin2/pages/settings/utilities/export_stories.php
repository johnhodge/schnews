<?php


$mysql		=	new mysql_connection();
$sql		=	"	SELECT 
							stories.headline,
							stories.date_live,
							stories.hits,
							issues.number
					FROM	stories 
					
					LEFT JOIN issues
					ON stories.issue = issues.id
					
					WHERE 
							stories.live = 1
						AND	stories.hide <> 1
						AND stories.deleted <> 1
						
					ORDER BY date_live DESC
					";
$mysql->query( $sql );



$output 	=	array();
$output[]	=	"<table style='margin: 25px; clear: both'>";
	$output[]	=	"<tr>";
	
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb; font-weight: bold'>Headline</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb; font-weight: bold'>Date Published</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb; font-weight: bold'>Hits</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb; font-weight: bold'>Issue </td>";

	$output[]	=	"</tr>";

$csv		=	array();


while ( $res	=	$mysql->get_row() )
{
	
	$output[]	=	"<tr>";
	
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb'>" . $res['headline'] . "</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb'>" . $res['date_live'] . "</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb'>" . $res['hits'] . "</td>";
	$output[]	=	"<td style='padding: 4px; border: 1px solid #bbbbbb'>" . $res['number'] . "</td>";

	$output[]	=	"</tr>";
	
	$line		=	array();
	$line[]		=	'"' . $res['headline'] . '"';
	$line[]		=	$res['date_live'];
	$line[]		=	$res['hits'];
	$line[]		=	$res['number'];
	
	$csv[]		=	implode( ", ", $line );
	
}

$output[]	=	"</table>";






if ( isset( $_GET['display'] ) && $_GET['display'] == 'yes' )
{
	
	$csv = implode( "\r\n", $csv );
	file_put_contents( "downloads/storyAdmin.csv", $csv );
	echo "<br /><br /><a href='downloads/storyAdmin.csv'>Download as CSV</a>";	
	
}


echo implode( "\r\n", $output );








