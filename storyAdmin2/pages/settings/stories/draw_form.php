<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	20th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Draws the settings edit form
 */



/*
 * 	DRAW TITLE
 */
?>

<!-- BEGIN SETTINGS TITLE -->

<br /><br />

<div class='section_subtitle'>Edit Story Editor Settings<br /><br /></div>

<div class='explanatory_text'>

These settings control various aspects of how stories are handled and displayed.
<br /><br />
Changing them may change the appearance and behavious of current live stories as well as new ones.
<br /><br />
All changes will be logged and the System Admin notified.

</div>

<br /><br />

<!-- END SETTINGS TITLE -->

<?php

$settings 			=	SETTINGS_get_story_settings_as_array();
$settings_desc		=	SETTINGS_get_story_settings_descriptions();
$settings_types		=	SETTINGS_get_story_settings_types();


/*
 * Begin and output buffer for the form
 */
$output	=	array();
$output[]		=	"<!-- BEGIN STORY SETTINGS EDIT FORM -->";

$output[]		=	"<form method='POST' action='?page=settings&settings_page=stories'>";
$output[]		=	"<table>";



if (isset( $errors ) && count( $errors ) > 0 )
{

	$output[]	=	"<!-- ERROR MESSAGES -->";
	$output[]	=	"<tr><td class='error_message' colspan='3'>";
	$output[]	=	"<b>There Were Errors</b><br /><br/>See the red messages below";
	$output[]	=	"</td></tr>";


}

$output[]		=	"<tr><td colspan='3' align='right'>";
$output[]		=	"<input type='submit' name='submit' value='Update' />";
$output[]		=	"</td></tr>";

$output[]	=	"<tr><td>&nbsp;</td></tr>";

foreach ( $settings as $setting => $value )
{

	if ( isset( $errors[ $setting ] ) )
	{

		$output[]	=	"<!-- BEGIN ERROR MESSAGE -->";
		$output[]	=	"<tr><td class='error_message' colspan='3'>";
		$output[]	=	$errors[ $setting ];
		$output[]	=	"</td></tr>";
		$output[]	=	"<!-- END ERROR MESSAGE -->";

	}


	$output[]	=	"<tr>";
	$output[]	=	"\t<td style='padding: 20px; margin: 5px; border: 1px solid #999999'>";
	$output[]	=	"\t<div class='section_subtitle'>$setting</div>";
	if ( isset( $settings_desc[ $setting ] ) )
	{
		$output[]	=	"\t<div class='explanatory_text'>" . $settings_desc[ $setting ] . '</div>';
	}
	$output[]	=	"</td>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"<td style='padding: 15px; margin: 5px; border: 1px solid #999999'>";
	switch ( $settings_types[ $setting ] )
	{
		case "int":
			$output[]	=	"<i>Integer</i><br /><br /><input type='text' name='$setting' value='$value' />";
			break;

		case "float":
			$output[]	=	"<i>Decimal Number</i><br /><br /><input type='text' name='$setting' value='$value' />";
			break;

		case "text":
			$output[]	=	"<i>Text</i><br /><br /><input type='text' name='$setting' value='$value' style='width: 300px' />";
			break;

		case "bool":
			$output[]	=	"<i>Yes / No</i><br /><br /><select name='$setting'>";
			if ( $value == '1' )
			{
				$select0 	=	'';
				$select1 	=	' selected ';
			}
			else
			{
				$select0	=	' selected ';
				$select1	=	'';
			}
			$output[]	=	"\t<option value='0' $select0 >No</option>";
			$output[]	=	"\t<option value='1' $select1 >Yes</option>";
			$output[]	=	"</select>";
	}
	$output[]	=	"</td>";
	$output[]	=	"</tr>";

	$output[]	=	"<tr><td>&nbsp;</td></tr>";
}

$output[]		=	"<tr><td colspan='3' align='right'>";
$output[]		=	"<input type='submit' name='submit' value='Update' />";
$output[]		=	"</td></tr>";

$output[]		=	"</table>";
$output[]		=	"</form>";

$output[]		=	"<!-- END STORY SETTINGS EDIT FORM -->";



/*
 * Flush the output buffer
 */
echo implode( "\r\n", $output );

