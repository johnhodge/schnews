<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	20th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Test the POST data for a settings  update
 *
 */



/*
 * 	Get the list of setting names and datetypes
 */
$settings		=	SETTINGS_get_story_settings_types();



/*
 * 	Test for errors
 */
$errors		=	array();	//	Any errors are stored in here, where the key = settings name
$output		=	array();	//	Any setting values are stored here, where the key = setting name



/*
 * 	Cycle through each setting in the POST var and evaluate it depending on its type
 */
foreach ( $settings as $setting => $type )
{

	//	Make sure it exists
	if (!isset( $_POST[ $setting ] ) )
	{

		$errors[ $setting ] = "<b>$setting</b> not set";

	}

	switch ( $type )
	{
		
		//	INTEGER
		case 'int':
			if ( !CORE_is_number( $_POST[ $setting ] ) )
			{

				$errors[ $setting ] 	=	"<b>$setting</b> is not a number";

			}
			break;
			
		//	TEXT
		case 'text':
			if ( !is_string( $_POST[ $setting ] ) ) 
			{
				
				$errors[ $setting ] 	=	"<b>$setting</b> is not a string";

			}
			break;
		
		//	FLOATING POINT NUMBER
		case 'float':
			if ( !CORE_is_number( $_POST[ $setting ] ) )
			{

				$errors[ $setting ] 	=	"<b>$setting</b> is not a number";

			}
			break;

		//	BOOLEAN BINARY
		case 'bool':
			if ( $_POST[ $setting ] != '0' && $_POST[ $setting ] != '1' )
			{

				$errors[ $setting ] 	=	"<b>$setting</b> is not Yes / No";

			}
			break;

		//	TEXT STRING
		case 'string':
			if ( !is_string( $_POST[ $setting ] ) )
			{

				$errors[ $setting ] 	=	"<b>$setting</b> is not a full text string";

			}
			break;

	}

	/*
	 * 	If and only if there are no errors then stick the value in the output buffer
	 */
	if ( count( $errors ) == 0 )
	{

		$output[ $setting ] 	=	$_POST[ $setting ];

	}

}

