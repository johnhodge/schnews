<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	6th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Handles the addition of editing of a user, calling other scripts
 * 	as necessary
 *
 */



debug("User Update detected", __FILE__, __LINE__);

$update_id 		=	$_GET['user_updated'];

// If $update_id is zero then we are adding a new user, if a valid positive int then we are editing an existing one
if ($update_id == 0)
{

	require "pages/settings/users/process_user_add.php";

}
else
{

	require "pages/settings/users/process_user_edit.php";

}
	
$user_id 	=	$update_id;

//	Load the suer edit page
require "pages/settings/users/edit_user.php";

//	Draw the HTML footer
require "inc/html_footer.php";