<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	14th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Send the email to the user informing them of their password reset.
 *
 */



/*
 * 	Unique hash for URL verification
 */
$hash			=	$user->update_reset_hash( );



/*
 * 	Email to user
 */
$link			=	SYSTEM_SITE_ROOT . "?password_reset=" . $user->get_id() . "&hash=" . $hash;
$subject		=	"SchNEWS StoryAdmin :: You Need to Reset Your Password";
$message		=	"Hello,


An admin has requested that you reset your password. Until you do so you will be unable to login to the SchNEWS Story Editor system.

Reseting your password is simple, just click on the link below:

$link



Thanks,

SchNEWS AI";
$mail		=	new email( $message, $subject, $user->get_email() );
$mail->send();



/*
 * 	Mail to admin
 */
$subject		=	"SchNEWS StoryAdmin :: User Password Reset Requested";
$message		=	"The user " . $user->get_name() . " has been required to reset their password.

The request was made by " . $_SESSION['logged_in_name'.SESSION_APPEND];
$mail			=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
$mail->send();



/*
 * 	Log entry
 */
LOG_record_entry( "Password reset requested for " . $user->get_name() . ". Request made by " . $_SESSION['logged_in_name'.SESSION_APPEND], 'security' );

