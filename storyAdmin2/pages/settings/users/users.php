<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	13th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Displays and edits the user account info
 *
 */



/*
 * 	Include the processor file, which will test the $_GET and $_POST vars and call any additional/alternative scripts as necessary.
 */
require "pages/settings/users/users_process_get_and_post.php";



//	Create an SQL connection and resource containing the user info
$mysql		=	new mysql_connection();
$sql		=	" SELECT * FROM users ORDER BY name ASC ";
$mysql->query($sql);

?>

<br />
<div class='section_subtitle'>List of Users...</div>

<table class='settings_user_table'>
	<tr>
		<th class='settings_user_table_header'>
			Name
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Username
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Email
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Last Login
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Admin Status
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Edit User?
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Delete User?
		</th>
		
		<td>&nbsp;</td>
		
		<th class='settings_user_table_header'>
			Reset Password?
		</th>
	</tr>

	<?php

		$statii		=	array(0 => 'Locked', 1 => 'User', 2 => 'Admin');

		//	Cycle through each SQl entry and draw a row for it
		while ($res		=	$mysql->get_row())
		{
			$output		=	array();
			$output[]	=	'';
			$output[]	=	"<!-- BEGIN ENTRY FOR USER ".strtoupper($res['name'])." -->";
			
			$output[]	=	"<tr><td style='height: 6px'></td></tr>";
			
			$output[]	=	"<tr>";
			$output[]	=	"\t<td class='settings_user_table_cell'>".$res['name'].'</td>';
			$output[]	=	"<td>&nbsp;</td>";
			$output[]	=	"\t<td class='settings_user_table_cell'>".$res['username'].'</td>';
			$output[]	=	"<td>&nbsp;</td>";
			$output[]	=	"\t<td class='settings_user_table_cell'>".$res['email'].'</td>';
			$output[]	=	"<td>&nbsp;</td>";



			// Format datetime for last login
			$datetime	=	new date_time($res['date_last_login']);
			$date		=	$datetime->make_human_date();
			$time		=	$datetime->make_human_time();
			$datetime	=	$date . ", " . $time;
			
			if ( $res['date_last_login'] == '' )
			{
				
				$datetime	=	" <b>[ NEVER LOGGED IN ]</b> ";
				
			}
			$output[]	=	"\t<td class='settings_user_table_cell'>" . $datetime .'</td>';
			$output[]	=	"<td>&nbsp;</td>";



			$output[]	=	"\t<td class='settings_user_table_cell'>".$statii[$res['status']]."\t</td>";
			$output[]	=	"<td>&nbsp;</td>";



			$output[]	=	"\t<td class='settings_user_table_cell'><a href='index.php?page=settings&settings_page=users&edit_user=".$res['id']."'>Edit User</a></td>";
			$output[]	=	"<td>&nbsp;</td>";
			$output[]	=	"\t<td class='settings_user_table_cell'><a href='index.php?page=settings&settings_page=users&delete_user=".$res['id']."'>Delete User</a></td>";
			$output[]	=	"<td>&nbsp;</td>";
			$output[]	=	"\t<td class='settings_user_table_cell'><a href='index.php?page=settings&settings_page=users&pw_reset=".$res['id']."'>Ask User to Reset Password</a></td>";
			$output[]	=	"</tr>";
			$output[]	=	"<!-- END ENTRY FOR USER ".strtoupper($res['name'])." -->";
			$output[]	=	'';
			echo implode("\r\n\t", $output);
		}

	?>

</table>

<div>
	<a href='index.php?page=settings&settings_page=users&edit_user=0'>Add New User</a>
</div>


