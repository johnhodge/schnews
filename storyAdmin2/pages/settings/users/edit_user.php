<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	29th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Changes the account detail for this user
 */



//	Instantiate a new user based on the GET id var
$user	=	new user($user_id);



//	Prepare a few variables for later use:
if ($user_id == 0)
{

	$title		=	'Adding New User';
	$submit		=	'Add User';
	$intro		=	"Enter the basic user info here. This will create the user in the system.<br /><br />The user will be emailed to inform them of their new account, and to ask them to create a password<br /><br />They will not be able to log in until they have done this.<br /><br />";

}
else
{

	$title		=	"Editing User '".$user->get_name()."'";
	$submit		=	'Update user';
	$password_1	=	'This will take you to a new page where you can change this users password.';
	$intro		=	'';
}



//	If an update/addition attempt has been made then we should update the user data...
if (isset($name))
{

	$user->set_name($name);

}
if (isset($username))
{

	$user->set_username($username);

}
if (isset($status))
{

	$user->set_status($status);

}
if (isset($email))
{
	$user->set_email($email);
}


?>



<br />
<div class='section_subtitle'><?php echo $title; ?></div>
<div class='explanatory_text'><?php echo $intro; ?></div>


<!-- BEGIN USER EDIT FORM/TABLE -->
<?php
$statii		=	array(0 => 'Locked', 1 => 'User', 2 => 'Admin');

//	Create select box for Admin Status
$select 		=	array();
$select[]		=	"\r\n\t\t\t<select name='status'>";
foreach ($statii as $key => $value)
{
	$selected 	=	'';
	if ($user->get_status() == $key) $selected = ' selected ';
	$select[]		=	"\t\t\t<option value='$key' $selected >$value</option>";
}
$select[]		=	"\t\t</select>\r\n\t";
$select		=	implode("\r\n\t", $select);



//	If there are any errors detected from a failed user add/edit process then display them
if (isset($errors))
{

	$output		=	array();
	foreach ($errors as $error)
	{

		$output[]		=	"\t<div class='error_message'>".$error.'</div>';

	}
	echo implode("\t\r\n", $output);


}


?>
<form method='POST' action='index.php?page=settings&settings_page=users&user_updated=<?php echo $user_id; ?>'>
<table class='settings_user_table' align='center'>

	<?php
		if ( isset( $errors[ 'name' ] ) )
		{

			echo "\r\n<tr>\r\n\t<td colspan='2' class='error_message'>" . $errors[ 'name' ] . "</td>\r\n</tr>\r\n";

		}
	?>
	<tr>
		<!-- NAME -->
		<td class='settings_user_table_cell' align='right'>
			Name
		</td>
		<td class='settings_user_table_cell_no_border'>
			<input type='text' name='name' value='<?php echo $user->get_name(); ?>' />
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>


	<?php
		if ( isset( $errors[ 'username' ] ) )
		{

			echo "\r\n<tr>\r\n\t<td colspan='2' class='error_message'>" . $errors[ 'username' ] . "</td>\r\n</tr>\r\n";

		}
	?>
	<tr>
		<!-- USERNAME -->
		<td class='settings_user_table_cell' align='right'>
			Username
		</td>
		<td class='settings_user_table_cell_no_border'>
			<input type='text' name='username' value='<?php echo $user->get_username(); ?>' />
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>


	<?php
		if ( isset( $errors[ 'email' ] ) )
		{

			echo "\r\n<tr>\r\n\t<td colspan='2' class='error_message'>" . $errors[ 'email' ] . "</td>\r\n</tr>\r\n";

		}
	?>
	<tr>
		<!-- EMAIL -->
		<td class='settings_user_table_cell' align='right'>
			Email Address
		</td>
		<td class='settings_user_table_cell_no_border'>
			<input type='text' name='email' value='<?php echo $user->get_email(); ?>' />
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>


	<?php
	if ( $user_id != 0 )
	{
	?>
	<tr>
		<!-- PASSWORD -->
		<td class='settings_user_table_cell' align='right'>
			Password
		</td>
		<td class='settings_user_table_cell_no_border'>
			<a href='index.php?page=settings&settings_page=users&change_password=<?php echo $user_id; ?>'>Click Here to Change Password</a>
		</td>
		<td style='padding-left: 20px'>
			<i><?php echo $password_1; ?></i>
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>


	<?php
		if ( isset( $errors[ 'status' ] ) )
		{

			echo "\r\n<tr>\r\n\t<td colspan='2' class='error_message'>" . $errors[ 'status' ] . "</td>\r\n</tr>\r\n";

		}
	?>
	<tr>
		<!-- ADMIN LEVEL -->
		<td class='settings_user_table_cell' align='right'>
			Admin Level
		</td>
		<td class='settings_user_table_cell_no_border'>
			<?php echo $select; ?>
		</td>
	</tr>

	<tr>
		<td>&nbsp;</td>
	</tr>

	<?php
	}
	?>



	<tr>
		<!-- SUBMIT AND CANCEL BUTTONS -->
		<td align='left'>
			<input type='submit' name='cancel' value='Cancel' />
		</td>
		<td align='right'>
			<input type='submit' name='submit' value='<?php echo $submit; ?>' />
		</td>
	</tr>


</table>
</form>
<!-- END USER EDIT FORM/TABLE -->