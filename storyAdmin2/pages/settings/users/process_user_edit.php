<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Processes an attempt to edit a user
 *
 */



//	Test submitted data
//		If any are missing it will generate an $errors array
require "pages/settings/users/user_edit/test_required_data.php";



//	Only continue processing if there are no errors...
if ( count( $errors ) == 0 )
{
	
	//	Create a new user object...
	$user	=	new user( $update_id );
	
	//	Make sure the name and/or username being hanged do not already belong
	//	to someone else in the database...
	if ( LOGIN_does_user_exist_already( $username, $name, $update_id ) )
	{
		
			$errors[]		=	"Either the Name or the Username is in use by another user";
		
	}
	else
	{

		//	Update the data in the database and draw a notice to screen...
		$user->set_name( $name );
		$user->set_username( $username );
		$user->set_email( $email );
		$user->set_status( $status );
		$user->update_user();
		
		echo "<div class='big_notice_message'>User $name Updated</div>";
		
	}
	unset( $user );	
}
