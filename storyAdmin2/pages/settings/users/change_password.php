<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	6th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Handles the changing of a users password
 *
 */



//	Test if a password update has been submitted
if 	( isset( $_POST[ 'password' ] ) && isset( $_POST[ 'password_verify' ] )	&& isset( $_GET[ 'change_password' ] ) )
{

	require "pages/settings/users/test_password_update.php";

	//	Only if there are no errors do we continue
	if ( count( $errors ) == 0 )
	{

			$id			=		$_GET[ 'change_password' ];
			$user		=		new user( $id );
			$user->set_password_hash( $_POST['password'] );
			$user->update_user_password();

			echo "\r\n<br /><br /><div class='big_notice_message' style='text-align: center'>\r\n\tPassword Updated for User '" . $user->get_name() . "'<br \><br \>User and System Admin have been notified by email and this has been logged\r\n</div>";
			echo "\r\n\r\n<br \>\r\n<br />\r\n<br />\r\n";
			echo "\r\n<div>\r\n\t<a href='?page=settings&settings_page=users&edit_user=$id'>Back to User Info</a>\r\n</div>";

			//	Draw the HTML footer, which will end the page
			require "inc/html_footer.php";

	}

}



// An explanation to the user about what the password verification represents
$verify_explanation 	=	"It is necessary to type the password twice in order to make sure that no typos are introduces when it is entered.
							<br /><br />If a typo is entered in either box then they wont match and we know there's a problem";

$page_explanation		=	"<b>NOTE: Use of this password reset is not recommended. You should ask the user to reset their own pass, which can be done by clicking here.</b>
							<br /><br />
							You can change the user's password to anything you want.
							<br /><br />
							However, doing so will cause a notification email to be sent the user (and a copy to the system admin) notifying them of the change, and that it was you who did it.
							<br /><br />
							It will also cause an entry to be added to the security log.
							";


//	Store local copy of user id and a local user object
$id		=	$_GET[ 'change_password' ];
$user	=	new user( $id );


//	Begin output buffer
$output 		=	array();

$output[]		=	"\r\n\r\n<!-- BEGIN CHANGE PASSWORD FORM -->";
$output[]		=	"<div class='section_subtitle'>Changing Password for '" . $user->get_name() . "'</div>";

$output[]		=	"<div style='padding: 15px'>" . $page_explanation . "</div>";

$output[]		=	"<form method='POST' action='index.php" . URL_make_url_from_get( URL_get_GET_vars() ) . "'>";
$output[]		=	"<table>";

if ( isset( $errors[ 'mismatch' ] ) )
{

	$output[]	=	"<!-- ERROR -->";
	$output[]	=	"<tr><td class='error_message' colspan='2'>" . $errors[ 'mismatch' ] . "</td></tr>";

}



$output[]		=	"<!-- BEGIN PASSOWRD INPUT -->";
if ( isset( $errors[ 'password' ] ) )
{

	$output[]	=	"<!-- ERROR -->";
	$output[]	=	"<tr><td class='error_message' colspan='2'>" . $errors[ 'password' ] . "</td></tr>";

}
$output[]		=	"<tr>";
$output[]		=	"<td class='settings_user_table_cell' align='right'>";
$output[]		=	"\tNew Password: ";
$output[]		=	"</td>";
$output[]		=	"<td class='settings_user_table_cell_no_border' align='left'>";
$output[]		=	"\t<input type='password' name='password' />";
$output[]		=	"</td>";
$output[]		=	"</tr>";
$output[]		=	"<!-- END PASSWORD INPUT -->";


$output[]		=	"<!-- SPACER -->";
$output[]		=	"<tr>";
$output[]		=	"<td>&nbsp;</td>";
$output[]		=	"</tr>";


$output[]		=	"<!-- BEGIN PASSOWRD VERIFY INPUT -->";
if ( isset( $errors[ 'password_verify' ] ) )
{

	$output[]	=	"<!-- ERROR -->";
	$output[]	=	"<tr><td class='error_message'>" . $errors[ 'password_verify' ] . "</td></tr>";

}
$output[]		=	"<tr>";
$output[]		=	"<td class='settings_user_table_cell' align='right'>";
$output[]		=	"\tVerify New Password: ";
$output[]		=	"</td>";
$output[]		=	"<td class='settings_user_table_cell_no_border' align='left'>";
$output[]		=	"\t<input type='password' name='password_verify' />";
$output[]		=	"</td>";
$output[]		=	"<td class='explanatory_text'>" . $verify_explanation . "</td>";
$output[]		=	"</tr>";
$output[]		=	"<!-- END PASSWORD VERIFY INPUT -->";


$output[]		=	"<!-- SPACER -->";
$output[]		=	"<tr>";
$output[]		=	"<td>&nbsp;</td>";
$output[]		=	"</tr>";


$output[]		=	"<!-- BEGIN SUBMIT / CANCEL BUTTONS -->";
$output[]		=	"<tr>";
$output[]		=	"<td align='left'>";
$output[]		=	"<input type='submit' name='cancel' value='Cancel' />";
$output[]		=	"</td>";
$output[]		=	"<td align='right'>";
$output[]		=	"<input type='submit' name='submit' value='Change Password' />";
$output[]		=	"</td>";
$output[]		=	"</tr>";
$output[]		=	"<!-- END SUBMIT / CANCEL BUTTONS -->";


$output[]		=	"</table>";
$output[]		=	"</form>";
$output[]		=	"<!-- END CHANGE PASSWORD FORM -->";



//	Flush the output buffer to screen
echo implode( "\r\n", $output );



//	Log entry
LOG_record_entry( "Change Password form for " . $user->get_name() . " displayed to " . $_SESSION['logged_in_name'.SESSION_APPEND], 'security', 0 );



//	Draw the HTML footer, which will end the page
require "inc/html_footer.php";
