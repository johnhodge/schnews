<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	29th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Processes an attempt to add a user
 *
 */



debug("Beginning Add New User script");



//	Begin an error buffer
$errors		=	array();



//	Try to put the POST data in local vars, and generate errors on failure...
$var_names	=	array('name', 'username', 'email');
foreach ($var_names as $var_name)
{

	if ( isset($_POST[$var_name]) && trim($_POST[$var_name]) != '')
	{

		$$var_name		=	$_POST[$var_name];

	}
	else
	{

		$errors[]			=	strtoupper($var_name.' IS NOT SET');

	}

}



//	If there are any error then call back the edit form which will display them...
if ( count($errors) > 0)
{
	$user_id = 0;
	require "pages/settings/users/edit_user.php";

	$message	=	"User addition attempted, failed with errors: ".implode(", ", $errors);
	LOG_record_entry( $message );

	//	Draw the HTML footer
	require "inc/html_footer.php";

}



//	If there are any error then call back the edit form which will display them...
if ( count($errors) > 0)
{
	$user_id = 0;
	require "pages/settings/users/edit_user.php";

	$message	=	"User addition attempted, failed with errors: ".implode(", ", $errors);
	LOG_record_entry( $message );

	//	Draw the HTML footer
	require "inc/html_footer.php";

}



//	Test that the email is valid
if (!URL_is_email($email))
{

	$errors[]	=	strtoupper("Email address '$email' is not valid");

}



//	If there are any errors then call back the edit form which will display them...
if ( count($errors) > 0)
{

	$user_id = 0;
	require "pages/settings/users/edit_user.php";

	$message	=	"User addition attempted, failed with errors: ".implode(", ", $errors);
	LOG_record_entry( $message );

	//	Draw the HTML footer
	require "inc/html_footer.php";

}



// All details at this stage are correct so let's add a user...
$message	=	"Adding New User:: name = $name, Username = $username, email = $email";
LOG_record_entry( $message );

$user	=	new user();

if ( $user->does_user_exist( $name, $username ) )
{
	$errors[]	=	"Name and/or Username already exist";
	$user_id = 0;
	require "pages/settings/users/edit_user.php";

	$message	=	"User addition attempted, failed with errors: ".implode(", ", $errors);
	LOG_record_entry( $message );

	//	Draw the HTML footer
	require "inc/html_footer.php";

}



$user->add_user($name, $username, $email);

//	Send an email to the user asking them to create a password
$pr_hash	=	$user->update_reset_hash();
$link		=	SYSTEM_SITE_ROOT . "index.php?password_set=" . $user->get_id() . "&hash=$pr_hash";
$subject 	=	"SchNEWS StoryAdmin :: A New Account Has Been Created For You";
$message	=	"Hello,


SchNEWS website drone here. An account has been setup for you on the " . SYSTEM_SITE_ROOT . " system.

Your username is: $username

Before you can login you will need to create a password for your account. Click the link below to do so:

$link

If you don't set a password within 7 days very bad things will happen to you.


SchNEWs AI Core";
$mail		=	new email( $message, $subject, $email );
$mail->send();
$mail		=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
$mail->send();



$output		=	array();
$output[]	=	"<div class='settings_user_add_notice'>";
$output[]	=	"<div class='big_notice_message'>";
$output[]	=	"New User '$name' Has Been Added";
$output[]	=	"</div>";
$output[]	=	"<br />";
$output[]	=	"<div class='notice_message'>";
$output[]	=	"They have been emailed a notice, requesting them to create a password. Until they do this they cannot login.";
$output[]	=	"</div>";
$output[]	=	"<br /><br /><br /><br />";
$output[]	=	"<div>";
$output[]	=	"<a href='?page=settings&settings_page=users'>Back to User List</a>";
$output[]	=	"</div>";
$output[]	=	"</div>";
echo implode( "\r\n", $output );
require "inc/html_footer.php";


