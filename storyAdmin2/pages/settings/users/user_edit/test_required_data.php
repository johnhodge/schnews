<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Makes sure that all the data required by a user edit or addition is present 
 * 	and correct.
 *
 */



//	These are the fields to be checked...
$fields		=	array(	'name', 'username', 'email', 'status' );
$errors		=	array();

foreach ( $fields as $field )
{
	
	if ( !isset( $_POST[ $field ] ) || trim( $_POST[ $field ] ) == '' )
	{
		
		$errors[ $field ] 	=	strtoupper( $field ) . ' not properly set.';
		$$field			 	=	'';
		
	}
	else
	{
		
		$$field 			=	$_POST[ $field ];
		
	}
	
}

