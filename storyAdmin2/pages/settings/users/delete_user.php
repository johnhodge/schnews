<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	29th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Questions and processes the deletion of a user
 *
 */


/*
 * 	STEP 1 - Confirm with Admin
 *
 *	If the delete has been initiated and so the user needs warning of the effects...
 */
if ( isset ( $delete_id ) )
{

	debug("Delete of User requested", __FILE__, __LINE__);

	$user		=	new user($delete_id);

	LOG_record_entry( "User deletion detected, drawing onscreen warnings. " . "User doing deleting = " . $_SESSION['logged_in_name'.SESSION_APPEND] . ". User being deleted = ".$user->get_name() );


	//	Draw a notice stating that the user will be deleted and give the user an OK / CANCEL option
	$output		=	array('<br />');
	$output[]	=	"<div class='settings_user_delete_user_container'>";
	$output[]	=	"<div class='settings_user_delete_user_title'>Are You Sure You Want to Delete User '".$user->get_name()."'</div>";
	$output[]	=	"<br />";
	$output[]	=	"<div>The user and the System Admin will be notified via email that their acount has been removed.<br /><br />Any votes on stories that they have made will be maintained.</div>";


	$output[]	=	"<form method='POST' action='index.php?page=settings&settings_page=users&do_delete=$delete_id'>";
	$output[]	=	"<br /><br />";
	$output[]	=	"<input type='submit' name='yes' value='Yes, Delete User' />";
	$output[]	=	"<br /><br />";
	$output[]	=	"<input type='submit' name='no' value='No, Do Not' />";
	$output[]	=	"</form>";
	$output[]	=	"</div>";



	echo implode("\t\r\n", $output);

}



/*
 * 	STEP 2 - Do the Deletion
 *
 * 	The admin has confirmed the deletion, so remove from the database, inform the user and the System Admin and tidy up
 */
if ( isset ( $do_delete) )
{

	$user	=	new user( $do_delete );

	if (!LOGIN_delete_user( $do_delete ))
	{

		unset ( $do_delete );
		$message	=	"Attempt to delete user '" . $user->get_name() . " failed.\r\n\r\nAttempt was made by " . $_SESSION['logged_in_name'.SESSION_APPEND] . "\r\n\r\nCheck the security log for details";
		$subject	=	"SchNEWS StoryAdmin :: User Delete Failed";
		$email		=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
		$email->send();

	}
	else
	{

		$message	=	"User " . $user->get_name() . " has been deleted.\r\n\r\nThe delete was done by " . $_SESSION['logged_in_name'.SESSION_APPEND] . "\r\n\r\nCheck the security log for details";
		$subject 	=	"SchNEWS StoryAdmin :: User Deleted";
		$email		=	new email( $message, $subject, SYSTEM_ADMIN_EMAIL );
		$email->send();

		$message 	=	"Hello " . $user->get_name() . ",\r\n\r\n\r\nI am the friendly SchNEWS Website AI. Your account on the SchNEWS online story editor has been deleted.\r\n\r\nFear not dear polemicist, your stories will remain, as will your votes on other storyies publication.\r\n\r\nIf this situation makes you angry, nervous or confused then have a shout at webmaster@schnews.org.uk or " . SYSTEM_ADMIN_EMAIL . "\r\n\r\n\r\nSchNEWS AI Core";
		$subject 	=	"SchNEWS StoryAdmin :: Your Account is No More";
		$email 		=	new email( $message, $subject, $user->get_email() );
		$email->send();

		echo "\r\n<div class='big_notice_message'>\r\n\tUser '" . $user->get_name() . "' has been Deleted\r\n</div>";
		echo "\r\n\r\n<div class='notice_message'>\r\n\tTheir stories and votes will remain, only their account has been removed.\r\n</div>";

	}

}






