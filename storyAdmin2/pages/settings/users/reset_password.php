<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	14th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Confirms and carries a password reset, emailing the user to request them to update their password.
 *
 */



/*
 * 	Create new user
 */
$user_id	=	$_GET[ 'pw_reset' ];
$user		=	new user( $user_id );



/*
 * 	Are we asking for confirmation or actually doing
 */
if ( isset( $_POST[ 'confirm' ] ) )
{

	//	Call a script to actually do the password reset work
	require "pages/settings/users/reset_password_do.php";

	?>

	<div class='settings_user_delete_user_container'>
	<div class='settings_user_delete_user_title'>Password Reset Email Sent to '<?php echo $user->get_name(); ?>'</div>

		<br /><br />
		The user has been emailed ( <?php echo $user->get_email(); ?> ), telling them that they need to reset their password.
		<br /><br />
		Until they do so they will be unable to log in.
		<br /><br />
		The System Admin has also been emailed, and a security log entry recorded.
		<br /><br /><br /><br />
		<a href='?page=settings&settings_page=users'>Back to Users List</a>

	</div>

	<?php

}
else
{

	?>

	<div class='settings_user_delete_user_container'>
	<div class='settings_user_delete_user_title'>Request Password Reset for '<?php echo $user->get_name(); ?>'</div>

		<br /><br />
		An email will be sent to the user ( <?php echo $user->get_email(); ?> ) informing them that their account has been locked until they reset their password.
		<br /><br />
		The email will contain a link, which will take them to the password reset page.
		<br /><br />
		An email will also be sent to the System Admin, and a security log entry made.

	<div style='padding: 15px'>

		<form method='POST' action='<?php echo URL_make_url_from_get( URL_get_GET_vars() ); ?>'>

			<input type='submit' name='confirm' value='Send Reset Email' />
			
			<br /><br /><br />
			
			<input type='submit' name='cancel' value='Cancel' />

		</form>

	</div>


	</div>

	<?php

}


