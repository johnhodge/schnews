<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	14th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Test the GET and POST vars to see if any user modifications, additions or deletions are required.
 *
 */



/*
 * 	EDIT USER
 *
 * 	If the 'Edit User' or 'Add New User' buttons were clicked...
 */
if (isset($_GET['edit_user']) && CORE_is_number($_GET['edit_user']) && !isset( $_POST ['cancel']))
{
	$user_id = $_GET['edit_user'];

	//	Load the suer edit page
	require "pages/settings/users/edit_user.php";

	//	Draw the HTML footer, which will end the page
	require "inc/html_footer.php";

}



/*
 *	EDIT USER PROCESSOR
 *
 *	If a user update has been submitted
 */
if (isset($_GET['user_updated']) && CORE_is_number($_GET['user_updated']) && !isset($_POST['cancel']))
{

	require "pages/settings/users/add_edit_user_handler.php";

}






/*
 * 	PASSWORD CHANGE
 *
 *	If the link to request a password change has been clicked
 */
if ( isset( $_GET[ 'change_password' ] ) && CORE_is_number( $_GET[ 'change_password' ] ) && !isset( $_POST[ 'cancel' ] ) )
{

	require "pages/settings/users/change_password.php";

}







/*
 *	DELETE USER
 *
 *	If a user deletion has been submitted
 */
if ( isset($_GET['delete_user']) && CORE_is_number($_GET['delete_user']) && !isset( $_POST[ 'no' ] ) )
{

	debug("User Delete Detected", __FILE__, __LINE__);

	$delete_id		=	$_GET['delete_user'];

	require "pages/settings/users/delete_user.php";


	//	Draw the HTML footer
	require "inc/html_footer.php";

}



/*
 *	DELETE USER CONFIRM
 *
 *	If a user deletion confirmation has been detected
 */
if ( isset( $_GET[ 'do_delete' ]) && CORE_is_number( $_GET[ 'do_delete' ]) && !isset( $_POST[ 'no' ] ) )
{

	debug ( "Confirmation of user deletion detected", __FILE__, __LINE__ );
	$do_delete		=	$_GET[ 'do_delete' ];

	require "pages/settings/users/delete_user.php";

}







/*
 * 	USER PASSWORD RESET
 *
 * 	Show the request page to ask a user to reset their password
 */
if ( isset( $_GET[ 'pw_reset' ] ) && CORE_is_number( $_GET[ 'pw_reset' ] ) && !isset( $_POST[ 'cancel' ] ) )
{

	debug ( "Beginning Password Reset Request Form", __FILE__, __LINE__ );
	require "pages/settings/users/reset_password.php";

	require "inc/html_footer.php";

}

?>