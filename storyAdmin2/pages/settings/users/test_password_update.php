<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	6th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Tests that the password and password verify are acceptable
 *
 */



//	Make local copies
$password			=	$_POST[ 'password' ];
$password_verify	= 	$_POST[	'password_verify' ];


//	Any errors get added to the $errors array.
//	Further proessing only occurs if the $errors array is empty
$errors		=	array();
if ( trim( $password ) == '' )
{

	$errors[ 'password' ]		=	"Password empty";
	
}

if ( trim( $password_verify ) == '' )
{
	
	$errors[ 'password_verify' ]	=	"Password verification empty";
	
}

if ( $password != $password_verify && count( $errors ) == 0 )
{
	
	$errors[ 'mismatch' ] 	=	"The verifcation does not match the password";
	
}
