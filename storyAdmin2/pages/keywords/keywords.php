<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Flow control for keywords handling
 *
 */



$page		=	'list';


/*
 * 	Test for edit
 */
if ( isset( $_GET['edit'] ) && CORE_is_number( $_GET['edit'] ) )
{
	
	$page	=	'edit';
	
	if ( isset( $_POST['keyword_change_submit'] ) && isset( $_POST['keyword'] ) )
	{
		
		$page	=	'edit_process';
		
	}
	
}


if ( 	isset( $_GET['add'] ) && $_GET['add'] == 0 
		&& 
		isset( $_POST['keyword'] ) && trim( $_POST['keyword'] ) != '' )
{
	
	$page	=	'add_process';
	
}



if ( isset( $_GET['delete'] ) && CORE_is_number( $_GET['delete'] ) )
{
	
	$page	=	"delete_process";
	
}



/*
 * Determine which page to load
 */
switch ( $page )
{
	
	default:
		require "pages/keywords/list/list.php";
		break;
		
	case 'edit':
		require "pages/keywords/edit/edit.php";
		break;
	
	case 'edit_process':
		require "pages/keywords/edit/edit_process.php";
		break;
		
	case 'add_process':
		require "pages/keywords/add/add_process.php";
		break;
		
	case "delete_process":
		require "pages/keywords/delete/delete_process.php";
		break;
		
}