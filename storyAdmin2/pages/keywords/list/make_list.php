<?php

/*
 * Get list of keyword IDs
 */
$mysql		=	new mysql_connection();
$sql		=	" 	SELECT id 
					FROM keywords
					ORDER BY keyword ASC ";
$mysql->query( $sql );



/*
 * Cycle through the keywords adding each as a row to the table
 */
$output		=	array();
$output[]	=	"<div class='keyword_list_container'>";
$output[]	=	"<table align='center'>";
$output[]	=	"<tr>";
$output[]	=	"<td class='keyword_table_header'>Category</td>";

$output[]	=	"<td>&nbsp;</td>";

$output[]	=	"<td class='keyword_table_header'>Number of Stories</td>";
$output[]	=	"</tr>";


while ( $id 	=	$mysql->get_row() )
{
	
	$keyword	=	new keyword( $id['id'] );
	
	$output[]	=	"<tr><td>&nbsp;</td></tr>";
	
	$output[]	=	"<tr>";
	$output[]	=	"<td class='keyword_table_value'>" . $keyword->get_name() . "</td>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"<td class='keyword_table_value'>" . $keyword->get_number_of_stories_for_keyword() . " stories</td>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"<td class='keyword_table_value'><a href='?page=keywords&edit=" . $keyword->get_id() . "'>Edit</a></td>";
	$output[]	=	"</tr>";
}

$output[]	=	"</table>";
$output[]	=	"</div>";

echo implode( "\r\n", $output );



?>