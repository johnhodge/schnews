<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Lists the keywords and other basic info about them
 *
 */


require "pages/keywords/list/title_and_intro.php";
require "pages/keywords/list/make_list.php";
require "pages/keywords/list/add_form.php";

