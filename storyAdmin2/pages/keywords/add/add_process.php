<?php


/*
 * Test inputs
 */
if ( isset( $_POST['keyword'] ) )
{
	
	$keyword		=	new keyword();
	$new_keyword 	=	$_POST['keyword'];
	
}
else
{
	
	$message = "Add Process called without either keyword or new keyword set";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/keywords/list/list.php";
	require "inc/html_footer.php";
	exit;
	
}



/*
 * 	UPDATE THE DATABASE
 */
$hash 	=	$keyword->record_new_keyword( $new_keyword );



/*
 * SEND THE EMAILS
 */
require "pages/keywords/add/send_email.php";



/*
 * DISPLAY NOTIFICATIONSTO SCREEN
 */
require "pages/keywords/add/display.php";