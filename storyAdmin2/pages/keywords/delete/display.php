<!-- BEGIN KEYWORD DELETE NOTIFICATION -->

<div class='explanatory_text'>

	<br /><br />

	
	<div class='explanatory_text_large'>
		KEYWORD DELETED
	</div>

	<br /><br />
	
	<div>
	
		The keywords '<b><?php echo htmlentities( $keyword->get_name() ); ?></b>' has been deleted.
		<br /><br />
		The <b><?php echo internal_mailing_list_address; ?></b> mailing list has been emailed about the change.
		<br /><br />
		<br /><br />
		<a href='index.php?page=keywords'>Back to Keywords List</a>
	
	</div>

</div>

<!-- END KEYWORD DELETE NOTIFICATION -->