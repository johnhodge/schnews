<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	4th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Allows a user to request a change to a keyword
 *
 */


/*
 * Test GET vars
 */
if ( !isset( $_GET['edit'] ) || !CORE_is_number( $_GET['edit'] ) )
{
	$message = "Keyword Edit called without working ID";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/keyords/list/list.php";
	require "inc/html_footer.php";
	exit;
}




$keyword		=	new keyword( $_GET['edit'] );




?>



<div class='explanatory_text'>

	<br /><br />
	
	<div class='explanatory_text_large'>
		EDITING KEYWORD '<?php echo $keyword->get_name(); ?>'
	</div>

	<br /><br />
	
	<div>
		
		You can change the name of '<?php echo $keyword->get_name(); ?>'.
		
		<br /><br />
		
		Or you can delete it. If you delete it any stories that where assocaited with that keyword will remain.
		
	</div>
</div>



<div class='keyword_change_form_container'>

	<form method='POST' action='?page=keywords&edit=<?php echo $keyword->get_id(); ?>'>
	
		<b>New Name :: </b> 
		<input type='text' style='width: 75%' name='keyword' value='<?php echo htmlentities( $keyword->get_name() ); ?>' />
		<br /><br />
		<input type='submit' name='keyword_change_submit' value='Change Keyword' />
	
	</form>
	
	<br /><br />
		
	<a href='?page=keywords'>Cancel, Back to Keyword List</a>

</div>




<div class='keyword_change_form_container'>

	<form method='POST' action='?page=keywords&delete=<?php echo $keyword->get_id(); ?>'>
	
		<input type='submit' name='keyword_change_submit' value='Delete It' />
	
	</form>
	
</div>
