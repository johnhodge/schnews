<!-- BEGIN KEYWORD CHANGE NOTIFICATION -->

<div class='explanatory_text'>

	<br /><br />

	
	<div class='explanatory_text_large'>
		KEYWORD CHANGE REQUEST RECEIVED
	</div>

	<br /><br />
	
	<div>
	
		You have requested to change the keyword '<b><?php echo htmlentities( $keyword->get_name() ); ?></b>' to '<b><?php echo htmlentities( $new_keyword ); ?></b>'
		<br /><br />
		The <b><?php echo internal_mailing_list_address; ?></b> mailing list has been emailed about the change.
		<br /><br />
		<br /><br />
		<a href='index.php?page=keywords'>Back to Keywords List</a>
	</div>

</div>

<!-- END KEYWORD CHANGE NOTIFICATION -->