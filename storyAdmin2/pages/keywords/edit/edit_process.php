<?php


/*
 * Test inputs
 */
if ( isset( $_GET['edit'] ) && CORE_is_number( $_GET['edit'] ) )
{
	
	$keyword	=	new keyword( $_GET['edit'] );
	
}

if ( isset( $_POST['keyword'] ) )
{
	
	$new_keyword	=	$_POST['keyword'];
	
}

if ( !isset( $keyword ) || !isset( $new_keyword ) )
{
	
	$message = "Edit Process called without either keyword or new keyword set";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/keywords/list/list.php";
	require "inc/html_footer.php";
	exit;
	
}



/*
 * Update database with the change request
 */
$keyword->record_change( $new_keyword );




/*
 * 	SEND EMAIL
 */
require "pages/keywords/edit/send_email.php";



/*
 * 	OUTPUT TO SCREEN
 */
require "pages/keywords/edit/display.php";
