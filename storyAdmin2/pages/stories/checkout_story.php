<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Checks the story out and passes control to the edit_story script
 */




/*
 * 	Make sure we have a valid story and user id
 */
if ( isset( $_GET['edit_story'] ) && CORE_is_number( $_GET['edit_story'] ) )
{
	
	$story_id		=	$_GET['edit_story'];
	
}
else
{
	
	$message		=	"Checkout script called without valid story ID";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	//	Get out of te page
	require "inc/html_footer.php";
	exit;
	
}

if ( isset( $_GET['checkout'] ) && CORE_is_number( $_GET['checkout'] ) )
{
	
	$user_id		=	$_GET['checkout'];
	
}
else
{
	
	$message		=	"Checkout script called without valid user ID";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	//	Get out of te page
	require "inc/html_footer.php";
	exit;
	
}



/*
 * 	Check the story out in the database
 */
$story		=	new story( $story_id );
$story->checkout( $user_id );
REVISIONS_store_temp_revision_text( "[New Story]", $story_id, $user_id );
debug( "HeaderNoticePerm set", __FILE__, __LINE__ );
$_SESSION['header_message_perm']	=	"You have a story locked for editing";



// 	Pass control to the edit story form.
require "pages/stories/edit_story/edit_story.php";
require "inc/html_footer.php";


?>


