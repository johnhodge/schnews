<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	31st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Checks the story backi to the database, issues emails to mailing lists, and does garbage collection
 */



/*
 * Test inputs
 */
if ( isset( $_GET['checkin'] ) && CORE_is_number( $_GET['checkin'] ) )
{
	
	$story_id		=	$_GET['checkin'];
	$story			=	new story( $story_id );
	
}
else
{
	
	$message		=	"Checkin form called with invalid or no GET checkin var";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'security', 1 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
	
}




/*
 * 	Get Temp Story
 */
if ( !$temp		=	REVISIONS_get_temp_revision( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] ) )
{
	
	$message		=	" Attempt to get temp story for checkin that doesn't exist. S = " . $story->get_id() . ", U = " . $_SESSION['logged_in_id'.SESSION_APPEND];
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
	
}
$revision_date		=	new date_time( $temp[0] );
$revision_text		=	$temp[1];
if ( $revision_text == '[New Story]' ) 
{
	debug( "Revision_text = [New Story]", __FILE__, __LINE__ );
	
	if ( $rev_id 	=	STORIES_get_latest_revision( $story->get_id() ) )
	{
		
		debug( "Determined Rev_id", __FILE__, __LINE__ );
		
		$temp_rev		=	new revision( $rev_id );
		$revision_text	=	$temp_rev->get_content();
		
	}
	else
	{
		
		$revision_text = '';
		
	}
	
	
}



/*
 * 	Get Temp Comments
 */
if ( $temp		=	REVISIONS_get_temp_internal_comments( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] ) )
{
	
	$comments_date		=	new date_time( $temp[0] );
	$comments_internal	=	$temp[1];
	$comments_external	=	$temp[2];
	
}
else
{
	
	debug ( "Temp comments don't exist. Using empty values", __FILE__, __LINE__ );
	$comments_date	=	'00-00-00 00:00:00';
	$comments_internal	=	'';
	$comments_external 	=	'';
	
}






/*
 * Update Database
 */
$revision		=	new revision();
$revision->set_story_id( $story->get_id() );
$revision->set_author( $_SESSION['logged_in_id'.SESSION_APPEND] );
$revision->set_content( $revision_text );
$revision->set_date_saved( DATETIME_make_sql_datetime() );
$revision->set_internal_comment( $comments_internal );
$revision->set_external_comment( $comments_external );
$revision->set_word_count( REVISIONS_work_out_word_count( $revision_text ) );



/*
 * 	If the new revision is different that the most recent one
 */
$latest_revision	=	new revision( STORIES_get_latest_revision( $story->get_id() ) );

if ( 		$revision_text != $latest_revision->get_content() 
		||	$comments_internal != '' 
		||	$comments_external != '' 	
	)
{
	debug ( "Updating Revision", __FILE__, __LINE__ );
	$revision->add_revision_to_database();
}







/*
 * 	Check Story In
 */
$story->checkin();
$_SESSION['header_message_perm'] = '';
debug( "HeaderNoticePerm unset", __FILE__, __LINE__ );




/*
 * Test for sending emails
 */
if ( email_list_on_edit )
{
	
	require "pages/stories/edit_story/send_emails.php";
	
}






/*
 * Do some logging and email notices
 */
$message		=		"Story " . $story->get_headline() . " successfully checked in. User = " . $_SESSION['logged_in_name'.SESSION_APPEND];
debug ( $message, __FILE__, __LINE__ );
LOG_record_entry( $message );





/*
 * Garbage Collection
 */
REVISIONS_remove_temp_files( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] );





/*
 * And Draw to Screen
 */
?>


<div class='section_subtitle'>
	
	STORY SAVED AND UNLOCKED
	<br />
</div>
 
<div class='explanatory_text'>

	<div class='explanatory_text_large'>
	
		You Have Saved and <a href='help?show=Story_Locking' target='_BLANK'>Unlocked</a> the Story
	
	</div>
	
	<br /><br />
	
	<div>
	
		It is now accessible to other <a href='help?show=Users' target='_BLANK'>users</a>, who can now come in and edit it themselves. They can also now <a href='help?show=Voting_for_Stories' target='_BLANK'>vote</a> for it.
	
	</div>
	
	
	<?php 
	if ( email_list_on_edit )
	{
	?>
	<br /><br />
	
	<div style='color: red'>
	
		The Mailing List has been emailed about the change.
	
	</div>
	
	<br /><br />
	<?php } ?>
	
	
</div>

<div class='checkin_confirm'>
	
	<form method='POST' action='?page=stories'>
	
		<input type='submit' value='Back to List of Stories' />
	
	</form>

</div>
 