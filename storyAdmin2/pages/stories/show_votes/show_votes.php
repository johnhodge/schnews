<?php 

	$story	=	new story( $_GET['show_votes'] );
	$author	=	new user( $story->get_author() );

	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT user
					FROM story_votes
					WHERE story = " . $story->get_id();
	$mysql->query( $sql );
	
?>


<br />
<div class='explanatory_text'>

	<div class='explanatory_text_large' style='text-align: right' >

		The Following User/s have Voted for This Story
		
		<br /><br />
		
		Story :: <?php echo strtoupper( $story->get_headline() ); ?>
	
		<br /><br />
		
		Author :: <?php echo $author->get_name(); ?>
		
	</div>
	
	<?php 
	
	if ( $mysql->get_num_rows() == 0 )
	{
		
		echo "<b>No Users Have Voted For This Story</b>";
		
	}
	else
	{
		
		while( $res	=	$mysql->get_row() )
		{
			
			$user	=	new user( $res['user'] );
			echo "<div style='font-size: 12pt; width: 50%; margin: 3px 25% 3px 25%; padding: 5px; border: 1px solid #dddddd'>";
			echo $user->get_name();
			
			if ( $user->get_status() == '2' )
			{
				
				echo " <b>[ Admin ]</b> ";
				
			}
			echo "</div>";
			
		}
		
	}
	
	?>
	
	
	
</div>
