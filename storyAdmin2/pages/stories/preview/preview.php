<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	25th September 2011
 *
 * 	========================================================
 * 
 */


/*
 * Test inputs
 */
 if ( !isset( $_GET['preview'] ) || !CORE_is_number( $_GET['preview'] ) )
 {
 	
 	$message = "preview/preview.php called without workng GET vars";
 	debug( $message, __FILE__, __LINE__ );
 	LOG_record_entry( $message, 'error', 2 );
 	
 	require "inc/html_footer.php";
 	exit;
 	
 }
 else
 {
 	
 	$story		=	new story ( $_GET['preview'] );
 	
 }
 
 
 
 
 /*
  * Determine read/write mode
  */
 $read_only		=	true;
 if ( $_SESSION['logged_in'.SESSION_APPEND] == 2 || $_SESSION['logged_in_id'.SESSION_APPEND] == $story->get_author() )
 {
 	
 	$read_only	=	false;
 	
 }
 
 
 
 
 /*
  *	Generate Edit Bar 
  */
 if ( $read_only )
 {
 
 	$edit_bar		=	"You Can Only Edit Stories You Wrote. You Didn't Write This Story";
 
 }
 else
 {
 	
 	$edit_bar		=	"<a href='?page=stories&begin_edit=" . $story->get_id() . "'> [ CONTINUE TO EDIT THIS STORY ] </a>";
 	
 }
 
 
 
 
 /*
  * Display the Edit Bar
  */
 ?>
 
 <!--  BEGIN PREVIEW EDIT BAR -->
 <div class='stories_preview_edit_bar_container'>
 	<span class='stories_preview_edit_bar'><?php echo $edit_bar; ?></span>
 	<br /><br />
 	<a href='?pages=stories'>Back to List of Stories</a>
 </div>
 <!--  END PREVIEW EDIT BAR -->
 
 
 
 
 <?php 
 /*
  * Display the Story Preview
  */
 ?>
 
 <!-- BEGIN STORY PREVIEW -->
 <div class='stories_preview_preview_container'>
 
 <?php 
 
 	echo FORMAT_make_story_preview( $story );
 
 ?>
 
 </div>
 <!-- END STORY PREVIEW -->
 
 
 
 