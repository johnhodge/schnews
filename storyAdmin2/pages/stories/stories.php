<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	25th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This file acts as a flow control for the stories section.
 *
 * 	All it really does is work out which stories page is required and load it.
 *
 */
?>



<!-- BEGIN STORIES SECTION -->

<div class='section_container'>

<div class='section_title'>
STORIES
</div>

<?php



	/*
	 * FLOW CONROL
	 */

	$page		=	'main_list';



	/*
	 * 	Test the GET vars to determine which page should be shown.
	 */
	
	/*
	 * 	Edit Issue
	 */
	if ( isset( $_GET['edit_issue'] ) && CORE_is_number( $_GET['edit_issue'] ) )
	{
		
		$page	=	'edit_issue';
		
	}

	

	/*
	 * 	Add New Story
	 */
	if ( isset( $_GET[ 'edit_story' ] ) )
	{

		$edit_story		=	$_GET[ 'edit_story' ];
		if ( $edit_story == 0 )
		{

			$page 	=	"add_new_story";

		}
		elseif ( CORE_is_number( $edit_story ) && $edit_story != 0 )
		{

			$page 	=	"edit_story";

		}

		/*
		 * 	If STEP is set the user has accepted the conditions and we are into actually doing the add...
		 */
		if ( isset( $_GET[ 'step' ] ) && CORE_is_number( $_GET[ 'step' ] ) )
		{

			switch ( $_GET[ 'step' ] )
			{

				case '1':
					$page	=	'add_new_story_step_1';
					break;

				case '2':
					if ( isset( $_POST[ 'headline' ] ) && trim( $_POST[ 'headline' ] ) )
					{

						$page	=	"add_new_story_step_2";

					}
					else
					{

						$error	=	"The Headline was not set properly. Please try again";
						$page	=	'add_new_story_step_1';

					}

			}

		}


	}
	
	
	
	/*
	 * 	Checkout Story
	 */
	if ( 	isset( $_GET['checkout'] ) && CORE_is_number( $_GET['checkout'] )
		&&	isset( $_GET['edit_story'] ) && CORE_is_number( $_GET['edit_story'] ) )
	{
		
		$page	=	"checkout";
		
	}
	
	
	
	/*
	 * 	Checkin Story
	 */
	if ( isset( $_GET['checkin'] ) && CORE_is_number( $_GET['checkin'] ) )
	{
		
		$page		=	'checkin';
		
	}
	
	
	
	/*
	 * 	Edit Story
	 */
	if ( isset( $_GET['begin_edit'] ) && CORE_is_number( $_GET['begin_edit'] ) )
	{
		
		$page		=	'begin_edit';
		
	}
	
	
	
	
	
	/*
	 * 	Preview Story
	 */
	if ( isset( $_GET['preview'] ) && CORE_is_number( $_GET['preview'] ) )
	{
		
		$page		=	"preview";
		
	}
	
	
	
	
	/*
	 * Vote for Story
	 */
	if ( isset( $_GET['vote'] ) && CORE_is_number( $_GET['vote'] ) )
	{
		
		$page		=	'vote_step_1';
		
	}
	
	if ( isset( $_GET['vote_confirm'] ) && CORE_is_number( $_GET['vote_confirm'] ) && !isset( $_POST['cancel'] ) )
	{
		
		$page		=	'vote_step_2';
		
	}
	
	
	
	/*
	 * Remove Vote for Story
	 */
	if ( isset( $_GET['unvote'] ) && CORE_is_number( $_GET['unvote'] ) )
	{
		
		$page		=	'unvote_step_1';
		
		if ( isset( $_POST['unvote_confirm'] ) )
		{
			
			$page		=	'unvote_step_2';
			
		}
		
	}
	

	if ( isset( $_GET['show_votes'] ) && CORE_is_number( $_GET['show_votes'] ) && !secret_story_ballot )
	{
		
		$page	=	'show_votes';
		
	}

	
	

	/*
	 * Depending of the $PAGE var load in the relevant page
	 */
	switch ( $page )
	{

		default:
			require "pages/stories/main_list/main_list.php";
			break;
			
		case 'edit_story':
			require "pages/stories/edit_story/edit_story.php";
			break;

		case 'add_new_story':
			require "pages/stories/add_new_story/add_new_story.php";
			break;

		case 'add_new_story_step_1':
			require "pages/stories/add_new_story/add_new_story_step_1.php";
			break;

		case 'add_new_story_step_2':
			require "pages/stories/add_new_story/add_new_story_step_2.php";
			break;
			
		case 'edit_issue':
			require "pages/issues/issues.php";
			break;
			
		case 'checkout':
			require "pages/stories/checkout_story.php";
			break;
			
		case 'checkin':
			require "pages/stories/checkin_story.php";
			break;
			
		case 'begin_edit':
			require "pages/stories/edit_story/begin_edit.php";
			break;

		case 'vote_step_1':
			require 'pages/stories/voting/vote/vote_for_step_1.php';
			break;
			
		case 'vote_step_2':
			require 'pages/stories/voting/vote/vote_for_step_2.php';
			break;
			
		case 'unvote_step_1':
			require 'pages/stories/voting/unvote/unvote_step_1.php';
			break;
			
		case 'unvote_step_2':
			require 'pages/stories/voting/unvote/unvote_step_2.php';
			break;
		
		case 'preview':
			require 'pages/stories/preview/preview.php';
			break;
			
		case 'show_votes':
			require "pages/stories/show_votes/show_votes.php";
			break;

	}

?>

</div>

<!--  END STORIES SECTION -->


