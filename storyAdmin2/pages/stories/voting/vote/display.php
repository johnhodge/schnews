<div class='section_subtitle' style='padding-bottom: 150px'>
	<br /><br />
</div>



<div class='voting_container'>

	<div class='voting_title'>
		You Have Voted for the Story
		<br />
		<br />
		"<i><?php echo $story->get_headline(); ?></i>"
	</div>
	
	
	
	
	<div class='voting_instructions'>
	
		<br /><br />
		
		Your vote has been recorded.
				
		<?php 
		
		if ( email_author_on_vote )
		{
			
			$author 	=	new user ( $story->get_author() );
			?>
			
			<br /><br />The author of the story ( <?php echo $author->get_name(); ?> ) has been emailed to inform them that a vote has been received by their story.
			
			<?php 

			if ( secret_story_ballot ) 
			{
				
				?>
				
					 Your identity has <b><u>NOT</u></b> been revealed.

				<?php
			
			}
			
		}
		
		?>
		
	</div>

	
	<div class='voting_form'>
	
		<form method='POST' action='?page=stories' >
			
			<input type='submit' name='confirm' value='Back to the List of Stories' />
		
		</form>
	
	</div>
	
</div>