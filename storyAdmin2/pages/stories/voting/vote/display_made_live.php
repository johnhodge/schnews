<div class='section_subtitle' style='padding-bottom: 150px'>
	<br /><br />
</div>



<div class='voting_container'>

	<div class='voting_title'>
		You Have Voted for the Story
		<br />
		<br />
		"<i><?php echo $story->get_headline(); ?></i>"
	</div>
	
	
	
	
	<div class='voting_instructions'>
	
		<br /><br />
		
		Your vote has been recorded.
				
				
		<div style='text-align: center; font-size: 13pt; color: #ff2222; font-weight:bold; padding-top: 25px'>

			NOTE: THE STORY IS NOW LIVE TO THE PUBLIC
			
			<br /><br />
			
			With your vote added the story now has enough votes to be made visible to the general public.				
				
		</div>
		
		<?php 
		if ( email_list_on_live )
		{
		?>
		<br /><br />
		<div>
		
			The Mailing List has been emailed to notify them that the story is now live.
		
		</div>
		<?php } ?>
		
		
				
		<?php 
		
		if ( email_author_on_vote )
		{
			
			$author 	=	new user ( $story->get_author() );
			?>
			
			<br /><br />The author of the story ( <?php echo $author->get_name(); ?> ) has been emailed to inform them that a vote has been received by their story.
			
			<?php 

			if ( secret_story_ballot ) 
			{
				
				?>
				
					 Your identity has <b><u>NOT</u></b> been revealed.

				<?php
			
			}
			
		}
		
		?>
		
	</div>

	
	<div class='voting_form'>
	
		<form method='POST' action='?page=stories' >
			
			<input type='submit' name='confirm' value='Back to the List of Stories' />
		
		</form>
	
	</div>
	
</div>