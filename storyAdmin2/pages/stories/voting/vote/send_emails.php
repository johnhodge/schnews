<?php 
/*
 * Process Emails
 */
$user		=	new user( $_SESSION['logged_in_id'.SESSION_APPEND] );
$author		=	new user( $story->get_author() );
$message	=	file_get_contents( "email_templates/story_vote_user");
$message	=	str_replace( "***NAME***", $user->get_name(), $message );
$message 	=	str_replace( "***AUTHOR***", $author->get_name(), $message );
$message	=	str_replace( "***HEADLINE***", $story->get_headline(), $message );
if ( secret_story_ballot )
{
	
	$message	=	str_replace( "***HAS/NOT***", 'HAS NOT', $message );
	
}	
else
{
	
	$message	=	str_replace( "***HAS/NOT***", 'HAS', $message );
	
}


$subject	=	"SchNEWS StoryAdmin :: You Have Voted For a Story";

$email		=	new email( $message, $subject, $user->get_email() );
$email->send();




if ( email_author_on_vote )
{
	
	$subject =	"SchNEWS StoryAdmin :: Your Story Has Received a Vote";
	$message	=	file_get_contents( "email_templates/story_vote_author");
	$message 	=	str_replace( "***AUTHOR***", $author->get_name(), $message );
	$message	=	str_replace( "***HEADLINE***", $story->get_headline(), $message );
	if ( secret_story_ballot )
	{
		
		$message	=	str_replace( "***USER***", 'not shown for privacy reasons', $message );
		
	}	
	else
	{
		
		$message	=	str_replace( "***USER***", $user->get_name(), $message );
		
	}
	
		$email		=	new email( $message, $subject, $author->get_email() );
		$email->send();
		
	}
	
	
if ( email_list_on_live )
{
	
	$subject 	=	"SchNEWS StoryAdmin :: Story '" . $story->get_headline() . "' has been voted live.";
	$message	=	file_get_contents( "email_templates/story_voted_live" );
	$message	=	str_replace( "***HEADLINE***", $story->get_headline(), $message );
	$message	=	str_replace( "***CONTENT***", html_entity_decode( strip_tags( $story->get_latest_revision()->get_content() ) ), $message ) ;
	
	if ( secret_story_ballot )
	{
		
		$message	=	str_replace( "***USER***", 'not shown for privacy reasons', $message );
		
	}	
	else
	{
		
		$message	=	str_replace( "***USER***", $user->get_name(), $message );
		
	}
	
		$email		=	new email( $message, $subject, internal_mailing_list_address );
		$email->send();
		
		echo nl2br( $message );
		
	
	
}