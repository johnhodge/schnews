<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This file acts as a flow control for the stories section.
 *
 * 	All it really does is work out which stories page is required and load it.
 *
 */


/*
 * Make sure we have a valid story ID and can instantiate an object
 */
if ( !isset( $_GET['vote_confirm'] ) || !CORE_is_number( $_GET['vote_confirm'] ) )
{
	
	$message	=	"Vote_for_step_2.php called without valid vote ID";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
	
}

$story	=	new story( $_GET['vote_confirm'] );



/*
 * Add the vote to the database
 */
if ( $story->has_user_voted_for_story() )
{
	$message	=	"Vote_for_step_2.php - User has already voted for this story";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
}


$made_live	=	false;
if ( $story->would_vote_make_live() )
{
	
	$made_live	=	true;
	
}

$story->vote_for_story( $_SESSION['logged_in_id'.SESSION_APPEND] );



/*
 * SEND EMAILS
 */
require "pages/stories/voting/vote/send_emails.php";



/*
 * GENERATE A HEADER NOTICE
 */
$_SESSION['header_message_temp']	=	"You Have Voted For a Story";





/*
 * DISPLAY TO SCREEN
 */
if ( $made_live )
{
	
	require "pages/stories/voting/vote/display_made_live.php";	
	
}
else
{
	
	require "pages/stories/voting/vote/display.php";
	
}




?>



