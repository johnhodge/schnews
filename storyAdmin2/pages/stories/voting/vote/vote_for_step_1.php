<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This file acts as a flow control for the stories section.
 *
 * 	All it really does is work out which stories page is required and load it.
 *
 */


/*
 * Make sure we have a valid story ID and can instantiate an object
 */
if ( !isset( $_GET['vote'] ) || !CORE_is_number( $_GET['vote'] ) )
{
	
	$message	=	"Vote_for_step_1.php called without valid vote ID";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
	
}

$story	=	new story( $_GET['vote'] );

?>



<div class='section_subtitle' style='text-align: center; padding-bottom: 200px'>
		<b><u>VOTING FOR STORY</u></b>
		<br />
		"<i><?php echo $story->get_headline(); ?></i>"
</div>


<div class='voting_container'>

	
	
	
	
	
	<div class='voting_instructions'>
	
		Here you can <a href='help?show=Voting_for_stories' target='_BLANK'>vote</a> for this story. 
		
		Once a story receives <b><?php echo number_story_votes_to_live; ?></b> votes ( of which <b><?php echo number_admin_votes_to_live; ?></b> must be from Admins ) it will be made <a href='help?show=Live_Story_Status' target='_BLANK'>live</a> and displayed to the public.
		
		<br /><br />
		
		This story currently has <b><?php echo $story->get_number_of_votes(); ?></b> votes, of which <b><?php echo $story->get_number_of_admin_votes(); ?></b> are from Admins.
		
		<?php 
		
		if ( email_author_on_vote )
		{
			
			$author 	=	new user ( $story->get_author() );
			?>
			
			<br /><br />The author of the story ( <?php echo $author->get_name(); ?> ) will be emailed to inform them that a vote has been received by their story.
			
			<?php 

			if ( secret_story_ballot ) 
			{
				
				?>
				
					 Your identity will <b><u>NOT</u></b> be revealed.

				<?php
			
			}
			
		}
		
		if ( $_SESSION['logged_in'.SESSION_APPEND] == 1 )
		{
			
			?>
			
			<br /><br />
			The story cannot be made live until at least <?php echo number_admin_votes_to_live; ?> Admins vote for it.
			
			<?php 
						
		}
		
		?>
		
		
		<br /><br /><br />
		Do you want to continue?
		
		
		
		<?php 
		/*
		 * 	Test to see if this vote would send the story live....
		 */
		if ( $story->would_vote_make_live() )
		{
			
			?>
				<div style='text-align: center; font-size: 13pt; color: #ff2222; font-weight:bold; padding-top: 25px'>
				
					NOTE: THIS VOTE WILL MAKE THE STORY LIVE TO THE PUBLIC
					<br /><br />
					This story requires only 1 more vote before it is live. Yours will be that vote.
				
				</div>	
			<?php 
			
		}
		
		
		/*
		 * 	If the story is already live
		 */
		if ( $story->get_live() == 1 )
		{
			
			?>
				<div style='text-align: center; font-size: 13pt; color: #ff2222; font-weight:bold; padding-top: 25px'>
				
					THIS STORY IS ALREADY LIVE
					<br /><br />
					Your vote will not affect the status of the story. But please don't let this stop you voting, you vote is still useful.
				
				</div>	
			<?php 
			
		}
		
		?>
		
		
	</div>

	
	<div class='voting_form'>
	
		<form method='POST' action='?page=stories&vote_confirm=<?php echo $story->get_id(); ?>' >
			
			<table width='100%'>
				<tr>
					<td align='left'>
						<input type='submit' name='confirm' value='Yes, Confirm my Vote' />
					</td>
					<td align='right'>
						<a href='?page=stories'>No, Take me Back to the Story List</a>
					</td>
				</tr>
			</table>
		
		</form>
	
	</div>

	<br />


	<?php 
	
	if ( $_SESSION['logged_in'.SESSION_APPEND] == 1 )
	{
	
	?>	
	<div class='voting_edit_story_link'>
	
		<a href='?page=stories&begin_edit=<?php echo $story->get_id(); ?>'>Edit Story</a>
	
	</div>
	<?php 
	}
	?>
	

	<div class='voting_story_summary'>
	
		<?php 
			echo FORMAT_make_story_for_list( $story, true );
		?>
	
	</div>
	
	<div class='voting_revision_summary'>
	
		<?php 
			echo FORMAT_make_revision_summary( STORIES_get_latest_revision( $story->get_id() ), true );	
		?>
	
	</div>
	
	
	<div class='voting_edit_story_link'>
	
		<a href='?page=stories&begin_edit=<?php echo $story->get_id(); ?>'>Edit Story</a>
	
	</div>

</div>