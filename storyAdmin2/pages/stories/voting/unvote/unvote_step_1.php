<?php 

	/*
	 * PREPARE SOME DATA
	 */
	$story		=	new story( $_GET['unvote'] );
	$num_votes	=	$story->get_number_of_votes();
	$author		=	new user( $story->get_author() );
	
?>


<!-- BEGIN UNVOTE NOTICE -->

<div class='section_subtitle' style='text-align: center; padding-bottom: 100px'>

	<u><b>STORY</b></u>
	<br />
	<?php echo htmlentities( $story->get_headline() ); ?>

</div>


<div class='eplanatory_text'>

	<div class='explanatory_text_large'>
	
		THIS WILL REMOVE YOUR VOTE FROM THE STORY
	
	</div>
	
	<?php 
		
		if ( $num_votes == number_story_votes_to_live )
		{
			
			require "pages/stories/voting/unvote/text/unlive.php";
			
		}
		else
		{
			
			require "pages/stories/voting/unvote/text/unvote.php";
			
		}
		
		
	
	?>
	
	<div class='voting_unvote_form'>
	
		<form method='POST' action='?page=stories&unvote=<?php echo $story->get_id(); ?>'>
		
			<input type='submit' name='unvote_confirm' value='Yes, Continue to Remove my Vote' />
		
		</form>
	
		<br /><br />
		
		<div style='text-align: right'>
			<a href='page=stories'>Cancel, Back to the List of Stories</a>
		</div>
		
	</div>	
	


</div>

<!-- END UNVOTE NOTICE -->