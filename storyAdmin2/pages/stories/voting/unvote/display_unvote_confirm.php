<div class='section_subtitle' style='padding-bottom: 150px'>
	<br /><br />
</div>


<div class='explanatory_text'>

	<div class='explanatory_text_large'>
	
		YOU HAVE REMOVED YOUR VOTE
	
	</div>
	
	<br /><br />
	
	<div style='text-align: center'>
	
		You have successfully removed your vote for the story '<i><?php echo htmlentities( $story->get_headline() ); ?></i>'
	
		<br /><br />
		
		The author ( <b><?php echo htmlentities( $author->get_name() ); ?></b> ) has been notified by email.
	
		<br /><br /><br /><br />
		
		<a href='?page=stories'><b>Back to the List of Stories</b></a>
	
	</div>

</div>