<div style='text-align: center'>

<br /><br />
There are currently <?php echo $num_votes; ?> votes for this story, including yours.
<br /><br />
<?php 

	if ( $num_votes > number_story_votes_to_live )
	{
		
		?>

			As there are currently more votes than are required to keep the story live, removing your vote will not affect the story's status. It will stay live to the public.
		
		<?php 
		
	}
	else
	{
		
		?>

			As there are currently fewer votes than are required to for the story to be live, removing your vote will not affect the story's status. It will remain hidden to the public until it receives more votes.
		
		<?php 
		
	}

?>
<br /><br />
The story's author ( <?php echo $author->get_name(); ?> ) will be emailed to notify them that your vote has been removed.

<?php 

if ( secret_story_ballot )
{
	
	?>
	
		As voting is anonymous, they will not be told that it is you who has removed the vote.
	
	<?php 
}
else
{
	
	?>
	
		Your identity will be revealed as the person who has removed the vote.
	
	<?php 
	
}
?>

<br /><br /><br />

Do you want to continue?

</div>