<?php 

	/*
	 * PREPARE SOME DATA
	 */
	$story		=	new story( $_GET['unvote'] );
	$num_votes	=	$story->get_number_of_votes();
	$author		=	new user( $story->get_author() );
	
	
	/*
	 * Test whether removing the vote will kill the story
	 */
	$dead 	=	false;
	if ( $story->get_number_of_votes() == number_story_votes_to_live )	$dead	=	true;
	

	
	/*
	 * 	REMOVE VOTE FROM DATABASE
	 */
	$story->remove_vote_for_story( $_SESSION['logged_in_id'.SESSION_APPEND] );
	
	
	
	/*
	 * We need to do different things here depending on whether we just killed
	 * the story or not.
	 */
	if ( $dead )
	{
		
		//	DO SOMETHING HERE
		
	}
	else
	{
		
		/*
		 * 	SEND EMAILS
		 */
		require "pages/stories/voting/unvote/send_unvote_emails.php";
		
		$_SESSION['header_message_temp']	=	"You Have Removed Your Vote From a Story";
		
		require "pages/stories/voting/unvote/display_unvote_confirm.php";
	}
	
	
	