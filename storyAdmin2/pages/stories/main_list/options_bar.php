<?php 

	$filter = '';
	if ( isset( $_SESSION['main_list_filter'] ) )
	{
		
		$filter	=	$_SESSION['main_list_filter'];
		
	}
	
	$users = '';
	if ( isset( $_SESSION['main_list_users' ] ) ) 
	{
		
		$users	 =	$_SESSION['main_list_users'];
		
	}

?>


<div class='stories_list_options_bar_container'>

	<form method='POST' action='?page=stories'>
		
		<select name='main_list_filter'>
			<option value='all' <?php if ( $filter == 'all' ) echo " selected "; ?> >Show All Stories</option>
			<option value='sticky' <?php if ( $filter == 'sticky' ) echo " selected "; ?> >Show Only Sticky</option>
			<option value='live' <?php if ( $filter == 'live' ) echo " selected "; ?> >Show Only Live</option>
			<option value='not_live' <?php if ( $filter == 'not_live' ) echo " selected "; ?> >Show Not Live</option>
			<option value='del' <?php if ( $filter == 'del' ) echo " selected "; ?> >Show Deleted Only</option>
			
		</select>
		
		
		<select name='main_list_users'>
			<option value='all' <?php if ( $users == 'all' ) echo " selected "; ?> >Show All Users</option>
			<option value='admin' <?php if ( $users == 'admin' ) echo " selected "; ?> >Show Only Admin's Stories</option>
			<option value='user' <?php if ( $users == 'user' ) echo " selected "; ?> >Show Only User's Stories</option>
		</select>
		

		<?php 
		
			require "pages/stories/main_list/issue_select.php";
		
		?>

		<input type='submit' value='Filter List' />	
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	|
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	<span style='color: red; border: 1px solid red; padding: 3px'><b>Red</b></span> = not yet live, 
	<span style='color: blue; border: 1px solid blue; padding: 3px'><b>Blue</b></span> = sticky 
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	|
	
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	
	<b>Showing <?php echo $number_of_stories; ?> Stories</b>
	
	</form>	
	
</div>