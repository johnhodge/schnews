<?php


/*
 * 	Test for POST data
 */
if ( isset( $_POST['main_list_filter'] ) )
{
	
	$_SESSION['main_list_filter'] = $_POST['main_list_filter'];
	
}

if ( isset( $_POST['main_list_users'] ) )
{
	
	$_SESSION['main_list_users'] = $_POST['main_list_users'];
	
}



/*
 * 	Test if a filter has been submitted
 */
$filter = '';
if ( isset( $_SESSION['main_list_filter'] ) )
{
		
	$filter	=	$_SESSION['main_list_filter'];
		
}



/*
 * 	Has a user filter been submitted
 */
$users = '';
if ( isset( $_SESSION['main_list_users'] ) )
{
		
	$users	=	$_SESSION['main_list_users'];
		
}



/*
 *	Has a issue filter been submitted 
 */
$issue	=	'all';
if ( isset( $_POST['issue'] ) && CORE_is_number( $_POST['issue'] ) )
{
	
	$issue	=	$_POST['issue'];
	
}





/*
 * Work out the WHERE line for the SQL string
 */
$filter_where	=	' WHERE deleted <> 1 ';
switch ( $filter )
{
	case "sticky":
		$filter_where	=	" WHERE deleted <> 1 AND live = 1 AND sticky = 1";
		break;
		
	case "live":
		$filter_where	=	' WHERE deleted <> 1 AND live = 1';
		break;
	
	case "not_live":
		$filter_where	=	' WHERE deleted <> 1 AND live <> 1';
		break;
		
	case "del":
		$filter_where	=	" WHERE deleted = 1 ";
		break;
		
}	



if ( $issue != 'all' && CORE_is_number( $issue ) )
{
	
	$filter_where	.=	" AND issue = $issue ";
	
}




if ( $users == 'admin' || $users == 'user' )
{
	
	$filter_where	.=	STORY_LIST_get_user_where( $users );
	
}




/*
 * Grab a list of Story IDs to show
 */
 $mysql		=	new mysql_connection();
 $sql		=	" 	SELECT id
 					FROM stories
 					$filter_where
 					ORDER BY date_added DESC
 					LIMIT 0, 100 ";
 $mysql->query( $sql );
 $number_of_stories	=	$mysql->get_num_rows();

 

 
 
 
 
 
 
 
 
 function STORY_LIST_get_user_where( $users )
 {
 	
		$user_ids		=	array();
		$mysql	=	new mysql_connection();

		if ( $users	==	'admin' )
		{
			$type	=	'2';
		}
		else
		{
			$type	=	'1';
		}
		
		
		//	Select all users of a given type/status
		$sql	=	"	SELECT id
						FROM users
						WHERE status = $type ";
		$mysql->query( $sql );
		
		
		//	If there are no applicable users then return an empty string
		if ( $mysql->get_num_rows() == 0 )
		{
			return '';
		}
		
		
		//	Create a comma separated list of the user ids
		while ( $res = $mysql->get_row() )
		{
			
			$user_ids[]	=	$res['id'];
				
		}
		$user_ids		=	implode( ", ", $user_ids );
		

		//	Return it in SQL format
		return " AND author IN ( $user_ids ) ";
		
 }