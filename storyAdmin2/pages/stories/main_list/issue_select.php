<?php

$issue	=	'all';
if ( isset( $_POST['issue'] ) )	$issue	=	$_POST['issue']; 


/*
 * 	Select all issues order by number (descending)
 */
$mysql		=	new mysql_connection();
$sql		=	"	SELECT id, number
					FROM issues
					ORDER BY number DESC ";
$mysql->query( $sql );

?>


<select name='issue'>
	<option value='all' <?php if ( $issue == 'all' ) echo ' selected'; ?> >
		Show All Issues
	</option>
	
	
	<?php 
	
		while ( $res 	=	$mysql->get_row() ) 
		{
			
			echo "<option value='" . $res['id'] . "' ";
			if ( $issue == $res['id'] ) echo " selected ";
			echo " '> Show Issue " . $res['number'] . "</option>";			
			
		}
	
	?>
	

</select>


