
<!-- BEGIN MAIN STORY LIST LINKS BAR -->

<table style='width:100%'>
<tr>


	<!-- BEGIN ADD NEW STORY -->
	<td style='width: 50%'>
		<div class='stories_add_new_story'>
			<a href='?page=stories&edit_story=0'><span class='big_link'>Add New Story</span></a>
		</div>	
	</td>
	<!-- END ADD NEW STORY -->

	
	
	<!-- BEGIN EDIT ISSUE -->
	<td style='width: 50%'>
		<div class='stories_edit_issue_button'>
			<?php 
				$issue 	=	new issue( ISSUES_get_newest_issue() );
				$number	=	$issue->get_number();
				$id		=	$issue->get_id();
			?>
			<span class='stories_edit_issue_current_issue'>
				The Current Issue is <b><?php echo $number; ?></b>			
			</span>
			
			
			<?php 
				if ( $_SESSION['logged_in'.SESSION_APPEND] == 2 )
				{
				?>
					
					&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;	
					<a href='?page=issues&edit_issue=<?php echo $id; ?>'><span class='big_link'>Click Here</span></a> to Edit the Issue Details
		
				<?php 
				}
				?>
		</div>	
	</td>
	<!-- END EDIT ISSUE -->


</tr>
</table>


<!-- END MAIN STORY LIST LINKS BAR -->