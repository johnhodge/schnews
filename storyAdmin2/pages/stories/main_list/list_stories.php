<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	31st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Draws the list of stories
 */


/*
 * 	Make the SQL string
 */
require "pages/stories/main_list/make_sql.php";



 /*
  *	Format the stories in HTML 
  */
 $stories		=	array();
 while ( $res	=	$mysql->get_row() )
 {

 	$story		=	new story( $res['id'] );
 	$stories[]	=	FORMAT_make_story_for_list( $story );
 	
 }
 
 
 
 /*
  * 	Display stories to screen
  */
require "pages/stories/main_list/toggle_bar.php";
require "pages/stories/main_list/options_bar.php";
?>

<br /><br /> 
 
 <?php  
 foreach ( $stories as $story )
 {
 	
 	echo $story;
 	
 }