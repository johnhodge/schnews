<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	16th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Draws the main list of stories
 *
 */



echo "\r\n<!-- BEGIN MAIN LIST OF STORIES -->";



/*
 * 	The section's title and introduction
 */
 require 'pages/stories/main_list/title_and_intro.php';
 
 
 
 /*
  * The options links at the top of the page
  */
 require 'pages/stories/main_list/links_bar.php';
 
 
 
 /*
  * Draw the list of stories
  */
 require "pages/stories/main_list/list_stories.php";
 
 
 
 echo "\r\n<!-- END MAIN LIST OF STORIES -->";