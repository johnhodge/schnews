<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	30th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	
 */
?>

	<!-- BEGIN CHECKOUT NOTICE -->
	<div class='checkout_notice_box'>
		
		<div class='explanatory_text_large'>
			You Have The Story Locked
		</div>
		
		<p>
			No One Else Can Edit It While it is <a href='help?show=Story_Locking' target='_BLANK'>locked</a>
		</p>
		
		<div class='checkin_button'>
		
			<form method='POST' action='?page=stories&checkin=<?php echo $story->get_id(); ?>'>
			
				<span class='checkin_notice'>Once you have finished editing you must unlock the story:</span>
				<input type='submit' name='checkin_submit' value='Finish Editing. Unlock Story' />
			
			</form>
			
		</div>
		
	</div>
	<!-- END CHECKOUT NOTICE -->