<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Processes a submission for the revision of a story
 */



/*
 * 	Test data
 */

$errors		=	array();

//	Test headline
if ( isset( $_POST['headline'] ) && trim( $_POST['headline'] ) != '' )
{
	
		$headline	=	$_POST['headline'];
	
}
elseif ( !isset( $_POST['headline'] ) )
{
	
		$errors[]	=	"Headline not set";
	
}
elseif ( trim( $_POST['headline'] ) == '' )
{
	
		$errors[]	=	"Headline is empty";

}


//	Test summary
if ( isset( $_POST['summary'] ) && trim( $_POST['summary'] ) != '' )
{
	
		$summary	=	$_POST['summary'];
	
}
elseif ( !isset( $_POST['summary'] ) )
{
	
		$errors[]	=	"Summary not set";
	
}
elseif ( trim( $_POST['summary'] ) == '' )
{
	
		$errors[]	=	"Summary is empty";

}



//	Test Sub-Headline
if ( isset( $_POST['subheadline'] ) && trim( $_POST['subheadline'] ) != '' )
{
	
		$subheadline	=	$_POST['subheadline'];
	
}
else
{
	
		$subheadline	=	'';

}



if ( isset( $_POST['date_live'] ) && $date_live = DATE_turn_text_to_sql( $_POST['date_live'] ) )
{
	
	debug ( "Date Live = $date_live" );
	
}
else
{	
	debug ( "Date Live not set" );
}


//	Test RSS
if ( isset( $_POST['rss'] ) && trim( $_POST['rss'] ) != '' )
{
	
		$rss	=	$_POST['rss'];
	
}
else
{
	
		$rss	=	'';

}



// 	Test Twitter
if ( isset( $_POST['twitter'] ) && trim( $_POST['twitter'] ) )
{
	
	if ( strlen( $_POST['twitter'] ) <= 160 )
	{
		
		$twitter	=	$_POST['twitter'];
		
	}
	else
	{

		$errors[]	=	"Twitter text was greater than 160 characters";
		
	}
	
}	
else
{
	
		$twitter 	=	'';
		
}



/*
 * 	Update Story object
 */
if ( count( $errors ) == 0 )
{
	
	if ( isset( $date_live ) )
	{
		
		$story->set_date_live( $date_live );
		
	}

	$old	=	$story->get_headline();
	$story->set_headline( $headline );
	$story->set_subheadline( $subheadline );
	$story->set_summary( $summary );
	$story->set_rss( $rss );
	$story->set_twitter( $twitter );
	$story->update_story_in_database();
	STORIES_update_file_name( $old, $headline, $story->get_id() );
	
	require "pages/stories/edit_story/form_processors/keywords.php";
	
}






function STORIES_update_file_name( $old, $new, $id )
{
	
	/*
	 * Test inputs
	 */
	if ( !is_string( $old ) || empty( $old ) ) return false;
	if ( !is_string( $new ) || empty( $new ) ) return false;
	if ( !CORE_is_number( $id ) ) return false;	
	

	debug ( "Testing if " . SITE_STORY_LOC . STORIES_make_story_link( $old ) . " exist" );
	if ( file_exists( SITE_STORY_LOC . STORIES_make_story_link( $old ) ) )
	{

		STORIES_replace_files( $old, $id );
		
	}
	
	return;
	
}


function STORIES_replace_files( $old, $id )
{
	
	$path	=	SITE_STORY_LOC . STORIES_make_story_link( $old );
	@unlink( $path . "/index.php" );
	rmdir( $path );
	STORIES_make_story_file( $id );	
	
}