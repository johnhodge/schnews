<?php


/*
 * 	Set delete flag to true in database
 */
$story->set_deleted( 1 );
$story->update_story_in_database();
$story->checkin( true, true );
REVISIONS_remove_temp_files( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] );
$_SESSION['header_message_temp']	=	"You Have Deleted a Story";
unset( $_SESSION['header_message_perm'] );
?>


<!-- BEGIN DELETE FORM -->
<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Story '<?php echo $story->get_headline(); ?>' Has Been Deleted
	</div>
	
	<div>
	
		The story has been removed from the system and will no longer
		be visible to the public or accessible to other users of this 
		system.
		
		<br /><br />
		
		<br /><br />
		
		<a href='?page=stories'>Back to the List of Stories</a>
			
	</div>
	
</div>
<!-- END DELETE FORM -->


<?php 

require "inc/html_footer.php";
exit;
 	
?>