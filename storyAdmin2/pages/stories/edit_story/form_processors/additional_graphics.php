<?php

/*
 *	TEST ADDITIONAL GRAPHIC 1
 */
if ( $_FILES['additional_graphic_1']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Additional Graphic 1";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['additional_graphic_1']['name'];
	
	if ( !move_uploaded_file( $_FILES['additional_graphic_1']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Additional Graphic 1 File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Additional Graphic 1 file";
	
	}
	
	$additional_graphic_1		=	$_FILES['additional_graphic_1']['name'];
	
}


/*
 *	TEST ADDITIONAL GRAPHIC 2
 */
if ( $_FILES['additional_graphic_2']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Additional Graphic 2";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['additional_graphic_2']['name'];
	
	if ( !move_uploaded_file( $_FILES['additional_graphic_2']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Additional Graphic 2 File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Additional Graphic 2 file";
	
	}
	
	$additional_graphic_2		=	$_FILES['additional_graphic_2']['name'];
	
}


/*
 *	TEST ADDITIONAL GRAPHIC 3
 */
if ( $_FILES['additional_graphic_3']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Additional Graphic 3";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['additional_graphic_3']['name'];
	
	if ( !move_uploaded_file( $_FILES['additional_graphic_3']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Additional Graphic 3 File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Additional Graphic 3 file";
	
	}
	
	$additional_graphic_3		=	$_FILES['additional_graphic_3']['name'];
	
}



/*
 *	TEST ADDITIONAL GRAPHIC 4
 */
if ( $_FILES['additional_graphic_4']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Additional Graphic 4";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['additional_graphic_4']['name'];
	
	if ( !move_uploaded_file( $_FILES['additional_graphic_4']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Additional Graphic 4 File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Additional Graphic 4 file";
	
	}
	
	$additional_graphic_4		=	$_FILES['additional_graphic_4']['name'];
	
}