<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Processes a submission for the revision of a story
 */



/*
 * 	Test data
 */

$errors		=	array();



//	Test sticky
if ( isset( $_POST['sticky'] ) )
{
	
	$sticky 	=	1;
	
}
else
{
	
	$sticky 	=	0;

}



//	Test hide
if ( isset( $_POST['hide'] ) )
{
	
	$hide		=	1;
	
}
else
{
	
	$hide		=	0;
	
}






//	Test Size
if ( isset( $_POST['size'] ) && CORE_is_number( $_POST['size'] ) )
{
	
	$size		=	$_POST['size'];

}
elseif ( !isset( $_POST['size'] ) )
{
	
	$errors[]	=	"Size not set";
	
}
elseif( !CORE_is_number( $_POST['size'] ) )
{
	
	$errors[]	=	"Size should be a number.";		
	
}






/*
 * 	GRAPHICS
 */

/*
 *	TEST SMALL GRAPHIC 
 */
if ( $_FILES['graphic_sm']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Small Graphic";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['graphic_sm']['name'];
	
	if ( !move_uploaded_file( $_FILES['graphic_sm']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Small Graphic File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Small Graphics file";
	
	}
	
	
	
	$graphic_sm		=	$_FILES['graphic_sm']['name'];
	
}



/*
 *	TEST LARGE GRAPHIC 
 */
if ( $_FILES['graphic_lg']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Large Graphic";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['graphic_lg']['name'];
	
	if ( !move_uploaded_file( $_FILES['graphic_lg']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Large Graphic File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Large Graphics file";
	
	}
	
	$graphic_lg		=	$_FILES['graphic_lg']['name'];
	
}


/*
 *	TEST VERY LARGE GRAPHIC 
 */
if ( $_FILES['graphic_vlg']['error'] > 0 )
{
	
	//$errors[]	=	"Error Uploading Large Graphic";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['graphic_vlg']['name'];
	
	if ( !move_uploaded_file( $_FILES['graphic_vlg']['tmp_name'] , $target ) )
	{
		
		$errors[]	=	"Error Processing Uploaded Very Large Graphic File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$errors[]	=	"Error setting permissions for new Very Large Graphics file";
	
	}
	
	$graphic_vlg		=	$_FILES['graphic_vlg']['name'];
	
}




/*
 * 	Test additional graphics
 */
require "pages/stories/edit_story/form_processors/additional_graphics.php";
require "pages/stories/edit_story/form_processors/additional_graphics_captions.php";


/*
 * 	Update Database
 */
if ( count( $errors ) == 0 )
{

	/*
	 * 	Do update for additional graphics
	 */
	require "pages/stories/edit_story/form_processors/add_additional_graphics.php";
	require "pages/stories/edit_story/form_processors/add_additional_graphics_captions.php";
	
	
	$story->set_sticky( $sticky );
	$story->set_hide( $hide );
	$story->set_size( $size );
	if ( isset( $graphic_sm ) ) 	$story->set_graphic_sm( $graphic_sm );
	if ( isset( $graphic_lg ) )		$story->set_graphic_lg( $graphic_lg );
	if ( isset( $graphic_vlg ) )	$story->set_graphic_vlg( $graphic_vlg );
	$story->update_story_in_database();
	
	
	/*
	 * 	Re-load story
	 */
	$story 	=	new story( $story->get_id() );
	
	
}