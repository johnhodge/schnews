<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Processes a submission for the internal of a story
 */



/*
 * 	Test data
 */

$errors		=	array();

if ( isset( $_POST['internal_comment'] ) && trim( $_POST['internal_comment'] ) != '' )
{
	
	$internal_comments		=	$_POST['internal_comment'];
	
}
else
{
	
	$internal_comments		=	' ';

}
if ( isset( $_POST['external_comment'] ) && trim( $_POST['external_comment'] ) != '' )
{
	
	$external_comments		=	$_POST['external_comment'];
	
}
else
{
	
	$external_comments		=	' ';
	
}



/*
 * 	Write temp file
 */
if ( count( $errors ) == 0 )
{
	
		REVISIONS_store_temp_internal_comments( $internal_comments, $external_comments, $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] );
	
}

