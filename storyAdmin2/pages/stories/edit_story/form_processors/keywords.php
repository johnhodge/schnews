<?php

/*
 * GET DATA FROM DB
 */
$keywords_all		=	KEYWORDS_get_all_keywords();

KEYWORDS_remove_keywords_from_story( $story->get_id() );


$keywords_selected		=	array();
foreach ( $keywords_all as $id => $keyword )
{
	
	if ( isset( $_POST['keyword_' . $id] ) )
	{

		$keywords_selected[]	=	$id;
		
	}
	
}

debug ("Number of keywords selected = " . count( $keywords_selected ), __FILE__, __LINE__ );

if ( count( $keywords_selected ) > 0 )
{
	
	KEYWORDS_add_keyword_selection( $keywords_selected, $story->get_id() );
	
}