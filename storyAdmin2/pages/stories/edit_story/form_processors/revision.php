<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Processes a submission for the revision of a story
 */



/*
 * 	Test data
 */

$errors		=	array();

if ( isset( $_POST['revision'] ) && trim( $_POST['revision'] ) != '' )
{
	
	$revision 	=	$_POST['revision'];
	
}
else
{
	
	$errors[]		=	"Revision Text not Set";
	
}




/*
 * 	Write temp file
 */
if ( count( $errors ) == 0 )
{
	
	$revision	=	strip_tags( $revision, '<b><i><em><strong><u><a><li><ul><ol><br><br /><p>' );
	
	REVISIONS_store_temp_revision_text( $revision, $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] );
	
}



?>