<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	31st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Flow control for the basic edit story form
 */



/*
 * 	Make sure we have a valid Edit_story var
 */
 if ( !isset( $_GET['edit_story'] ) || !CORE_is_number( $_GET['edit_story'] ) || $_GET['edit_story'] == 0 )
 {
 	
 	$message		=	"Edit Story form called without valid Edit_story var";
 	debug ( $message, __FILE__, __LINE__ );
 	LOG_record_entry( $message, 'error', 2 );
 	
 	require "pages/stories/main_list/main_list.php";
 	
 }
 else
 {
 	
 	$story 	=	new story( $_GET['edit_story'] );
 	
 }
 
 
 
 
 
 /*
  * 	TEST FOR SUBMISSION
  * 
  * 	Has an edit form been submitted?
  */
 
 //	Test for revision submit
 if ( isset( $_POST['revision_submit'] ) && $_POST['revision_submit'] == 'Save Changes' )
 {
 	
 	require "pages/stories/edit_story/form_processors/revision.php";

 }
 
 //	Test for summary submit
 if ( isset( $_POST['summary_submit'] ) && $_POST['summary_submit'] == 'Save Changes' )
 {
 	
 	require "pages/stories/edit_story/form_processors/summary.php";
 	
 }
 
 //	Test for position submit
 if ( isset( $_POST['position_submit'] ) && $_POST['position_submit'] == 'Save Changes' )
 {
 	
 	require "pages/stories/edit_story/form_processors/position.php";
 	
 }

 //	Test for delete submission
 if ( isset( $_GET['delete_story'] ) && $_GET['delete_story'] == 1 )
 {

 	require "pages/stories/edit_story/form_processors/delete.php";
 	
 }
 
 //	Test for comments submit
 if ( isset( $_POST['comments_submit'] ) && $_POST['comments_submit'] == 'Save Changes' )
 {
 	
 	require "pages/stories/edit_story/form_processors/comments.php";
 	
 }
 
 
 
 /*
  * 	Make sure the current user has the story checked out
  */
 if ( $_SESSION['logged_in_id'.SESSION_APPEND] != $story->get_checked_out() )
 {
 	
 	/*
 	 * Make some log entries
 	 */
 	$message 	=	"Attempt to show edit user forms while story not checked out by user";
 	debug ( $message, __FILE__, __LINE__ );
 	LOG_record_entry( $message, 'error', 2 );
 	
 	
 	/*
 	 * 	Show error page and close down
 	 */
 	require "pages/stories/edit_story/checkout_error.php";
 	require "inc/html_footer.php";
 	exit;
 	
 }



?>
 
 
 
 
<!-- BEGIN EDIT STORY FORM  -->
<?php 
 /*
 * 	Determine which form to load
 */
$form_page		=	'summary';
if ( isset( $_GET['form_page'] ) )
{	
	$form_page		=	$_GET['form_page'];
}


switch ( $form_page )
{
 	
 	default:
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/summary.html" );
 		break;
 		
 	case "revision":
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/revision.html" );
 		break;
 		
 	case "position":
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/position.html" );
 		break;
 		
 	case "comments":
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/comments.html" );
 		break;

 	case "delete":
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/delete.html" );
 		break;
 		
 	case "public_comments":
 		$explanatory_text	=	file_get_contents( "pages/stories/edit_story/explanations/public_comments.html" );
 		break;
 		
}

?>

 
<div class='explanatory_text'>

	<div style='text-align: right'>
		<span id='jq_checkout_box_explain_controller' class='jq_checkout_box'>
			Click Here to Show Instructions
		</span>
	</div>

	<div id='jq_checkout_box_explain_text' class='jq_checkout_box_hidden_text' style='text-align: right'>
		<?php echo $explanatory_text; ?>
	</div>
	
	<?php require "pages/stories/edit_story/checkout_box.php"; ?>
	
</div>
 
 





 
<?php 
/*
 * 	Determine which form to load
 */
$form_page		=	'summary';
if ( isset( $_GET['form_page'] ) )
{	
	$form_page		=	$_GET['form_page'];
}



/*
 * 	Call the navigation bar
 */
require "pages/stories/edit_story/edit_story_nav_bar.php";



/*
 * Load the relevant form sub-page
 */
switch ( $form_page )
{
 	
 	default:
 		require "pages/stories/edit_story/form_pages/summary_info.php";
 		break;
 		
 	case "revision":
 		require "pages/stories/edit_story/form_pages/revision.php";
 		break;
 		
 	case "position":
 		require "pages/stories/edit_story/form_pages/position.php";
 		break;
 		
 	case "comments":
 		require "pages/stories/edit_story/form_pages/comments.php";
 		break;
 		
 	case "delete":
 		require "pages/stories/edit_story/form_pages/delete.php";
 		break;
 		
 	case "public_comments":
 		require "pages/stories/edit_story/form_pages/public_comments.php";
 		break;
 		
 	
}

?>

<!-- END EDIT STORY FORM  -->
 
 
 