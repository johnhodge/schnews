<?php

	$subject 	=	"SchNEWS StoryAdmin :: Story '" . $story->get_headline() . "' has been modified.";
	$message	=	file_get_contents( "email_templates/story_changed" );
	$message	=	str_replace( "***HEADLINE***", $story->get_headline(), $message );
	$message	=	str_replace( "***CONTENT***", html_entity_decode( strip_tags( $story->get_latest_revision()->get_content() ) ), $message ) ;
	$message	=	str_replace( "***USER***", $user->get_name(), $message );
		
	
	$email		=	new email( $message, $subject, internal_mailing_list_address );
	$email->send();
		
