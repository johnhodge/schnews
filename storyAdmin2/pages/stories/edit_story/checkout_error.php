<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Displays a generic error message to the user in the event that they try to edit a
 *	story they don't have checked out
 */

LOGIN_log_user_out();

?>


<!-- BEGIN CHECKOUT ERROR MESSAGE -->

<div class='section_subtitle'>
	CHECKOUT ERROR
</div>

<div class='explanatory_text'>

	<div class='big_error_message'>
		ERROR: Attempt to Edit Story Without Checkout First
	</div>
	
	<br /><br />
	<div class='error_message'>
		Let's admit it, something has gone wrong, and I don't know what. It looks like you've tried to edit a story without having it checked out. Which means you're either trying to subvert my sophisticated security systems, or more like there is a bug in my code somewhere.
	</div>

</div>

<div class='stories_checkout_error_logout_notice'>
	You Have Been Logged Out. Sorry.
</div>

<!-- END CHECKOUT ERROR MESSAGE -->


