<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	31th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Allows a user to checkout and edit a story
 */

if ( isset( $_GET['begin_edit'] ) && CORE_is_number( $_GET['begin_edit'] ) )
{
	
	$story	=	new story( $_GET['begin_edit'] );
	
}
else
{
	
	$message 	=	"Begin Edit called with no working story ID";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	
	require "pages/stories/main_list/main_list.php";
	require "inc/html_footer.php";
	exit;
	
}


?>


<!-- BEGIN CHECKOUT TITLE AND INTRO -->

<br />
<div class='section_subtitle' style='text-align: center'>

	<?php 
	
		echo FORMAT_make_left_bar_story_summary( $story ); 
	
	?>
	
</div>

<div class='explanatory_text'>

	<div class='explanatory_text_large'>
	
		THIS WILL LOCK THE STORY
		<br /><br />
		Other Users Will Be Unable to Edit It While You Have It Locked.
	
	</div>
	
	<br /><br />
	
	<div>
	
		You can keep the story <a href='help?show=Story_Locking' target='_BLANK'>locked</a> for as long as you want while you edit it. Once you save your changes and unlocked the story other <a href='help?show=Users' target='_BLANK'>users</a> will see you changes and be able to edit it again themselves.
		<br /><br />
		If you leave without saving your changes, then after <?php echo checkout_timeout; ?> minutes the story will be unlocked and 
		
		<?php 
		
			switch ( checkout_timeout_save )
			{
				
				case 0:
					echo " your changes will <b><u>NOT</u></b> be saved, they will be discarded.";
					break;
					
				case 1:
					echo " your changes will be saved.";
					break;
				
			}
		
		?>
	
	</div>
	
</div>

<br />

<div class='begin_edit_prompt'>

Continue and Edit the Story?

	<div class='begin_edit_prompt_form'>
	
		<form action='?pages=stories&edit_story=<?php echo $story->get_id(); ?>&checkout=<?php echo $_SESSION['logged_in_id'.SESSION_APPEND]; ?>' method='POST' >
		
			<input type='submit' value='Yes, Edit Story' />
			
		</form>
	
	</div>
	
	<div class='begin_edit_prompt_cancel_link'>
		<a href='?page=stories'>Cancel, Back to List of Stories</a>
	</div>

</div>

<!-- END CHECKOUT TITLE AND INTRO -->


