<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Edit story for for the content revision info
 */




/*
 * If a request has been submitted to revert to a previous version...
 */
if ( isset( $_GET['revert'] ) && CORE_is_number( $_GET['revert'] ) )
{
	
	$message 	=	"Reverting to previous revision of story. Story ID = " . $story->get_id() . ", Revision ID = " . $_GET['revert'];
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message );
	
	$revision	=	new revision( $_GET['revert'] );
	REVISIONS_store_temp_revision_text( $revision->get_content(), $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND]);
	
}




/*
 * 	Prepare some data
 */
$revision_text 	=	"";
if ( $rev 	=	REVISIONS_get_temp_revision( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] ) )
{
	
	$revision_text	=	$rev[ 1 ];
	if ( $revision_text == '[New Story]' ) 
	{
		
		$revision_text 	=	'';
		
	}
	
}	


if ( is_object( $story->get_latest_revision() ) && $revision_text == '' )
{
	
	debug ( "Getting latest revision text from story object", __FILE__, __LINE__ );
	$rev			=	$story->get_latest_revision();
	$revision_text	=	$rev->get_content();
	
}



?>



<!-- BEGIN EDIT STORY SUMMARY -->

<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Main Story Content
	</div>
	
	<?php 
	
		if ( isset( $errors ) )
		{
			
			if ( count( $errors ) == 0 )
			{
				
				?>
				
					<div class='big_notice_message'>
						Text Updated. It will not be fully saved until you <a class='big_notice_message' href='help?show=Story_Locking' target='_BLANK'>unlock</a> the story.
					</div>
					<br />
				
				<?php 
			}
			else
			{
				
				foreach ( $errors as $error )
				{

					echo "\r\n<div class='error_message'>" . $error . "</div>";
					
				}
				echo "\r\n<br />";
				
			}
				
			
		}
	
	?>

	<form method='POST' action='?edit_story=<?php echo $story->get_id(); ?>&form_page=revision'>
	
		<table style='width:100%'>
		
			<!-- BEGIN MAIN CONTENT EDIT BOX -->
			<tr>
				
				<td class='stories_edit_story_summary_value'>
				
					<textarea name='revision' id='ckeditor_1' class='ckeditor_1' onkeyup="cnt(this,document.myform.c)"><?php echo $revision_text; ?></textarea>
					
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ckeditor_1',
						    {
						        toolbar : 	[
							   				['Bold', 'Italic', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink'],
											],
						        uiColor : '#bbbbbb',
						        height: '750px',
						        width: '650px',
						        extraPlugins: 'wordcount'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
				
				</td>

			</tr>
			<!-- END MAIN CONTENT EDIT BOX -->
			
			
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			
			
			
			<!-- BEGIN SUBMIT BUTTON -->
			<tr>
				
				<td align='right'>
					
					<input type='submit' name='revision_submit' value='Save Changes' onclick='return show_save_notice()' />
					
				</td>
			
			</tr>
			<!-- END SUBMIT BUTTON -->
			
		</table>
		
	</form>
	
</div>

<!-- END EDIT STORY SUMMARY -->


<?php
/*
 * Call revision info
 */ 
require "pages/stories/edit_story/form_pages/revision_info.php";
		
			
						