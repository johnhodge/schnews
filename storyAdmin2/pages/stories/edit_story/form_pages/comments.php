<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Edit story for for the internal comments
 */


/*
 * 	Prepare some data
 */
$internal_comment	=	'';
$external_comment	=	'';
if ( $com 	=	REVISIONS_get_temp_internal_comments( $story->get_id(), $_SESSION['logged_in_id'.SESSION_APPEND] ) )
{

	$internal_comment		=	$com[1];
	$external_comment		=	$com[2];
		
}



?>


<!-- BEGIN EDIT INTERNAL COMMENTS -->
<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Story Internal Comments
	</div>
	
	<?php 
	
		if ( isset( $errors ) )
		{
			
			if ( count( $errors ) == 0 )
			{
				
				?>
				
					<div class='big_notice_message'>
						Story Comments Updated. It will not be fully saved until you <a class='big_notice_message' href='help?show=Story_Locking' target='_BLANK'>unlock</a> the story.
					</div>
					<br />
				
				<?php 
			}
			else
			{
				
				foreach ( $errors as $error )
				{

					echo "\r\n<div class='error_message'>" . $error . "</div>";
					
				}
				echo "\r\n<br />";
				
			}
				
			
		}
	
	?>

	<form method='POST' action='?edit_story=<?php echo $story->get_id(); ?>&form_page=comments'>
	
		<table style='width:100%'>
			
			
			<!-- BEGIN INTERNAL COMMENT -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					Internal Comment:
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						This is visible only to internal users of this system.
						<br /><br/>
						It should be used to explain your changes or edits to other users. 
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<textarea name='internal_comment' class='stories_edit_story_comments_textarea'><?php echo $internal_comment; ?></textarea>
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END INTERNAL COMMENT -->
			
			
			
			
			
			<!-- BEGIN EXTERNAL COMMENT -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					External Comment:
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						This will be attached to the end of a story and shown to the public.
						<br /><br/>
						It should be used if there is a change made to the story that the public should know about (e.g. correction of a factual error)
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<textarea name='external_comment' class='stories_edit_story_comments_textarea'><?php echo $external_comment; ?></textarea>
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END EXTERNAL COMMENT -->
			
			
			
		
		
		
			<!-- BEGIN SUBMIT BUTTON -->
			<tr>
				
				<td colspan='3' align='right'>
					
					<input type='submit' name='comments_submit' value='Save Changes' onclick='return show_save_notice()' />
					
				</td>
			
			</tr>
			<!-- END SUBMIT BUTTON -->
		
		</table>
	
	</form>

</div>

<!-- END EDIT INTERNAL COMMENTS -->