<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	11th September 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Edit story for for the summary info
 */
?>


<!-- BEGIN EDIT STORY SUMMARY -->
<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Story Summary Info
	</div>
	
	<?php 
	
		if ( isset( $errors ) )
		{
			
			if ( count( $errors ) == 0 )
			{
				
				?>
				
					<div class='big_notice_message'>
						Story Updated. It will not be fully saved until you <a class='big_notice_message' href='help?show=Story_Locking' target='_BLANK'>unlock</a> the story.
					</div>
					<br />
				
				<?php 
			}
			else
			{
				
				foreach ( $errors as $error )
				{

					echo "\r\n<div class='big_error_message'>" . $error . "</div>";
					
				}
				echo "\r\n<br />";
				
			}
				
			
		}
	
	?>

	<form method='POST' action='?edit_story=<?php echo $story->get_id(); ?>'>
	
		<table style='width:100%'>
			
			
			<!-- BEGIN HEADLINE -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					Headline:
					<br />
					<div class='stories_edit_story_summary_label_required'>
						* required
					</div>
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						[ You can use <b>&lt;i&gt;</b>, <b>&lt;u&gt;</b> and <b>&lt;b&gt;</b> HTML tags ]
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<input type='text' name='headline' value="<?php echo htmlentities( $story->get_headline() ); ?>" class='stories_edit_story_summary_form_entry'/>
				
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END HEADLINE -->
			
			
			<!-- BEGIN SUB-HEADLINE -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					Sub-Headline:
					<br />
					<div class='stories_edit_story_summary_label_optional'>
						* optional
					</div>
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						[ You can use <b>&lt;i&gt;</b>, <b>&lt;u&gt;</b> and <b>&lt;b&gt;</b> HTML tags ]
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<input type='text' name='subheadline' value="<?php echo htmlentities( $story->get_subheadline() ); ?>" class='stories_edit_story_summary_form_entry'/>
				
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END SUB-HEADLINE -->
			
			
			
			<!-- BEGIN SUMMARY -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					Summary:
					<br />
					<div class='stories_edit_story_summary_label_required'>
						* required
					</div>
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						[ You can use <b>&lt;i&gt;</b>, <b>&lt;u&gt;</b> and <b>&lt;b&gt;</b> HTML tags ]
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<textarea name='summary' class='ckeditor_1' id='ckeditor_1' ><?php echo htmlentities( $story->get_summary() ); ?></textarea>
				
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ckeditor_1',
						    {
						        toolbar : 	[
							   				['Bold', 'Italic'],
											],
						        uiColor : '#999999',
						        height: '75px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
					
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END SUMMARY -->
			
			
			
			<!-- BEGIN RSS -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					RSS:
					<br />
					<div class='stories_edit_story_summary_label_optional'>
						* optional
					</div>
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						[ Plain Text Only - Any HTML tags or line breaks will be removed ]
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value'>
				
					<textarea name='rss' class='stories_edit_story_summary_form_entry'><?php echo htmlentities( $story->get_rss() ); ?></textarea>
				
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END RSS -->
			
			
			
			
			<!-- BEGIN TWITTER -->
			<tr>
				
				<td class='stories_edit_story_summary_label'>
					
					Twitter:
					<br />
					<div class='stories_edit_story_summary_label_optional'>
						* optional
					</div>
					<br /><br />
					<div class='stories_edit_story_summary_label_info'>
						[ Plain Text Only - Any HTML tags or line breaks will be removed ] - Make sure to use hashtags
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_summary_value' valign='middle'>
				<textarea 	name='twitter' 
							class='stories_edit_story_summary_form_entry' 
							maxlength='160'
							onKeyDown="CountLeft( this.form.twitter, this.form.left, 160 );"
							onKeyUp="CountLeft( this.form.twitter, this.form.left, 160 );"		
					/><?php echo htmlentities( $story->get_twitter() ); ?></textarea> 
					
					
					<input readonly type="text" name="left" size=3 maxlength=3 value="160"> 
					characters left
				
				</td>
			
			</tr>
			
			
			<?php 
			if ( $story->get_live() == 1 )
			{

				?>
				
				<tr>
				<td>&nbsp;</td>			
				</tr>
			
				<!-- BEGIN DATE LIVE -->
				<tr>
					
					<td class='stories_edit_story_summary_label'>
						
						Date Published:
						<br />
						<div class='stories_edit_story_summary_label_optional'>
							* dd-mm-yyyy
						</div>
					
					</td>
					
					<td>&nbsp;</td>
					
					<td class='stories_edit_story_summary_value' valign='middle'>
					
						<input type='text' name='date_live' value='<?php echo DATE_turn_sql_to_text( $story->get_date_live() ); ?>' class='stories_edit_story_summary_form_entry' />
					
					</td>
				
				</tr>
				
			<?php 
			
			}
			
			?>
			
			<tr>
				<td>&nbsp;</td>			
			</tr>
			<!--  END RSS -->
		
		
			<tr>
			<td colspan='3' class='stories_edit_keyword_title'>
				Keywords
			</td>
			</tr>
			
			<tr>
			<td colspan='3' align='center'>
			<?php 
			/*
			 * Include keywords form
			 */
			require "pages/stories/edit_story/form_pages/keywords.php";
			?>
			</td>
			</tr>
		
		
			<!-- BEGIN SUBMIT BUTTON -->
			<tr>
				
				<td colspan='3' align='right'>
					
					<input type='submit' name='summary_submit' value='Save Changes' onclick='return show_save_notice()' />
					
				</td>
			
			</tr>
			<!-- END SUBMIT BUTTON -->
		
		</table>
	
	</form>

</div>

<!-- END EDIT STORY SUMMARY -->