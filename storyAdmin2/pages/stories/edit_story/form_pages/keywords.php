<?php

/*
 * GET DATA FROM DB
 */
$keywords_all		=	KEYWORDS_get_all_keywords();
$keywords_selected	=	KEYWORDS_get_keywords_for_story( $story->get_id() );



/*
 * LOCAL CONFIG
 */
define ( 'NUM_COLUMNS', 3 );





$output		=	array();
$output[]	=	"<!-- BEGIN KEYWORDS FORM -->";
$output[]	=	"<table align='center' class='stories_edit_keywords_table'>";
$output[]	=	"<tr>";

$column		=	1;
foreach ( $keywords_all as $id => $keyword )
{
	
	$checked		=	'';
	if ( in_array( $id, $keywords_selected ) )
	{
		
		$checked 	=	' checked ';
		
	}
	
	
	if ( $column > NUM_COLUMNS )
	{

		$output[]	=	"</tr><tr><td>&nbsp;</td></tr><tr>";
		$column 	=	1;
		
	}
	
	$output[]	=	"<td class='stories_edit_keywords_cell' align='right'>" . htmlentities( $keyword ) . "</td>";
	$output[]	=	"<td>&nbsp;</td>";
	$output[]	=	"<td class='stories_edit_keywords_cell' align='center'><input type='checkbox' name='keyword_" . $id . "' " . $checked . " /></td>";
	$output[]	=	"<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>";
	
	$column++;
	
}

$output[]	=	"</table>";
$output[]	=	"<!-- END KEYWORDS FORM -->";


echo implode( "\r\n", $output );