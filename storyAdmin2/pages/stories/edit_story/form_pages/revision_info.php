<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Displays into about previous revisions
 */


/*
 * Make sure we have a valid story object set
 */
if ( !isset( $story ) || !is_object( $story ) )
{
	
	$message 	=	"Revision info script called without story object set";
	debug ( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 2 );
	require "inc/html_footer.php";
	exit;	

}



?>


<!--  BEGIN REVISION INFO -->

<div class='revision_info_container'>


<?php 
$mysql		=	new mysql_connection();
$sql		=	"	SELECT id
					FROM revisions
					WHERE story_id = " . $story->get_id() . "
					ORDER BY date_saved DESC ";
$mysql->query( $sql );
$ids		=	array();
if ( $mysql->get_num_rows() > 0 )
{
	
	while ( $res		=	$mysql->get_row() )
	{

		$ids[]		=	$res['id'];
	
	}
	
}



/*
 * 	If there are no previous revisions then there's no point continuing 
 */
if ( count ( $ids ) == 0 )
{
	
	?>
		<div class='revision_info'>
		
			There are no previous revisions to this story
		
		</div>
		
	<?php

	require "inc/html_footer.php";
	exit;
	
}

?>




<?php 
/*
 * How many revisions are there
 */
?>
<div class='revision_info'>
	There are <?php echo count ( $ids ); ?> revisions to this story, by <?php echo REVISIONS_get_number_of_revision_authors( $story->get_id() ); ?> users.
</div>



<?php 

foreach ( $ids as $id )
{
	
	echo FORMAT_make_revision_summary( $id );
	
}

?>


</div>

<!--  END REVISION INFO -->