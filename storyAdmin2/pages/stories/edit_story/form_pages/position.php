<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 *	Edit story for for the positional info
 */
?>


<!-- BEGIN POSITION FORM -->
<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Layout Info
	</div>
	
	<?php 
	
		if ( isset( $errors ) )
		{
			
			if ( count( $errors ) == 0 )
			{
				
				?>
				
					<div class='big_notice_message'>
						Story Updated. It will not be fully saved until you <a class='big_notice_message' href='help?show=Story_Locking' target='_BLANK'>unlock</a> the story.

					</div>
					<br />
				
				<?php 
			}
			else
			{
				
				foreach ( $errors as $error )
				{

					echo "\r\n<div class='error_message'>" . $error . "</div>";
					
				}
				echo "\r\n<br />";
				
			}
				
			
		}
	
	?>

	<form method='POST' action='?edit_story=<?php echo $story->get_id(); ?>&form_page=position' enctype="multipart/form-data">
	
		<table style='width:100%'>
		
			<!-- BEGIN STICKY -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Sticky?
					<br /><br />
					<div class='stories_edit_story_layout_label_info'>
						If a story is sticky, it stays at the top of the homepage even though other stories may be never.
						<br /><br />
						This is useful for really important stories that have a long term relevance, or maybe for stories that have been recently updated.
						<br /><br />
						<b>NOTE: </b> If there are several sticky stories then they will be shown in date order. Use this function sparingly.
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
				
					<?php 

						$checked	=	'';
						if ( $story->get_sticky() )
						{
							$checked 	=	'checked';
						}
					
					?>
					<input type='checkbox' name='sticky' <?php echo $checked; ?> />
					 
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!--  END STICKY -->
			
			
			
			<!-- BEGIN HIDE -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Hide Story?
					<br /><br />
					<div class='stories_edit_story_layout_label_info'>
						This will stop a story from being displayed to the public.
						<br /><br />
						It will still exist in the system, and can still be edited, but will not be shown to anyone public.
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
				
					<?php 

						$checked	=	'';
						if ( $story->get_hide() )
						{
							$checked 	=	'checked';
						}
					
					?>
					<input type='checkbox' name='hide' <?php echo $checked; ?> />
					 
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!--  END HIDE -->
			
			
			
			
		
			
			
			
			<!-- BEGIN SIZE -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Size on Screen?
					<br /><br />
					<div class='stories_edit_story_layout_label_info'>
						How large and prominent should the story appear on the homepage?
						<br /><br />
						e.g. lead stories should be large, 2 paragraph updates should be small
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
				
					<?php 
						
						$sizes 		=	STORIES_get_possible_on_screen_sizes();
						
						echo	"\r\n<select name='size'>\r\n";
						foreach ( $sizes as $key => $value )
						{
							
							$selected 	=	'';
							if ( $story->get_size() == $key ) $selected	=	' selected ';
							echo "\r\n<option value= '$key' $selected >" . $value . "</option>"; 
							
						}
						echo "\r\n</selected>";
					
					?>
					 
				</td>
			
			</tr>
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			
			<!--  END SIZE -->
		
		
		
		
			<!-- BEGIN GRAPHIC SM -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Graphic (Small)
					
					<br /><br />
					
					<div class='stories_edit_story_layout_label_info'>
						This will appear with this summary on the homepage
						<br /><br />
						Please, please make sure it is as close to 140 x 90 pixels as possible.
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
		
					<input type='file' name='graphic_sm' />
		
					<?php 
					
						$gfx	=	$story->get_graphic_sm();
						if ( $gfx != '' )
						{
							
							echo "<img src='../images/" . $gfx . "' width='100px' height='75px' align='right' />";
							
						}
					
					?>
					
				</td>
				
			</tr>
			
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!-- END GRAPHICS SM -->
			
			
			
			
			
			<!-- BEGIN GRAPHIC LG -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Graphic (Large)
					
					<br /><br />
					
					<div class='stories_edit_story_layout_label_info'>
						This will be shown along with the story when the story is displayed.
						<br /><br />
						Please, please make sure it is as close to 250 x 175 pixels as possible.
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
		
					<input type='file' name='graphic_lg' />
		
					<?php 
					
						$gfx	=	$story->get_graphic_lg();
						if ( $gfx != '' )
						{
							
							echo "<img src='../images/" . $gfx . "' width='250px' height='175px' align='right' />";
							
						}
					
					?>
					
				</td>
				
			</tr>
			
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!-- END GRAPHICS LG -->
			
			
			
			
			
			
			<!-- BEGIN GRAPHIC VLG -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Graphic (Very Large)
					
					<br /><br />
					
					<div class='stories_edit_story_layout_label_info'>
						This will be shown on its own page when the smaller image is clicked on
						<br /><br />
						It can be any size you want.
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
		
					<input type='file' name='graphic_vlg' />
		
					<?php 
					
						$gfx	=	$story->get_graphic_vlg();
						if ( $gfx != '' )
						{
							
							echo "<img src='../images/" . $gfx . "' width='250px' height='175px' align='right' />";
							
						}
					
					?>
					
				</td>
				
			</tr>
			
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!-- END GRAPHICS LG -->
			
			
			
			
			<!-- BEGIN ADDITIONAL GRAPHICS -->
			<tr>
				
				<td class='stories_edit_story_layout_label'>
					
					Additional Graphics
					
					<br /><br />
					
					<div class='stories_edit_story_layout_label_info'>
						These will be displayed at the end of a story.
						<br /><br />
						They can vary in size, but please try to make them something like 500 px wide, otheriwse it will look a bit odd.
						<br /><br />
						Please DO NOT exceed 700px wide.	
					 
					</div>
				
				</td>
				
				<td>&nbsp;</td>
				
				<td class='stories_edit_story_layout_value'>
		
					<b>Graphic 1 :: </b> <input type='file' name='additional_graphic_1' />
					<br /><br />
					<b>Caption 1 :: </b> <input type='input' name='additional_graphic_caption_1' value='<?php echo $story->get_additional_graphics_caption_1(); ?>' style='width:400px' />
					
					<?php 
					
						if ( $story->get_additional_graphics_1() != '' )
						{
							
							echo "<br /><br /><img src='" . SITE_IMAGE_URL . $story->get_additional_graphics_1() . "' />";
							
						}
						echo "<div style='border-bottom: 1px dotted #999999'>&nbsp;</div>";
						
					?>
					
					<br /><br />
					
					<b>Graphic 2 ::</b> <input type='file' name='additional_graphic_2' />
					<br /><br />
					<b>Caption 2 :: </b> <input type='input' name='additional_graphic_caption_2' value='<?php echo $story->get_additional_graphics_caption_2(); ?>' style='width:400px' />
					
					<?php 
					
						if ( $story->get_additional_graphics_2() != '' )
						{
							
							echo "<br /><br /><img src='" . SITE_IMAGE_URL . $story->get_additional_graphics_2() . "' />";
							
						}
						echo "<div style='border-bottom: 1px dotted #999999'>&nbsp;</div>";
					
					?>
					
					<br /><br />
					
					<b>Graphic 3 ::</b> <input type='file' name='additional_graphic_3' />
					<br /><br />
					<b>Caption 3 :: </b> <input type='input' name='additional_graphic_caption_3' value='<?php echo $story->get_additional_graphics_caption_3(); ?>' style='width:400px' />
					
					<?php 
					
						if ( $story->get_additional_graphics_3() != '' )
						{
							
							echo "<br /><br /><img src='" . SITE_IMAGE_URL . $story->get_additional_graphics_3() . "' />";
							
						}
						echo "<div style='border-bottom: 1px dotted #999999'>&nbsp;</div>";
					
					?>
					
					<br /><br />
					
					<b>Graphic 4 ::</b> <input type='file' name='additional_graphic_4' />
					<br /><br />
					<b>Caption 4 :: </b> <input type='input' name='additional_graphic_caption_4' value='<?php echo $story->get_additional_graphics_caption_4(); ?>' style='width:400px' />
					
					<?php 
					
						if ( $story->get_additional_graphics_4() != '' )
						{
							
							echo "<br /><br /><img src='" . SITE_IMAGE_URL . $story->get_additional_graphics_4() . "' />";
							
						}
					
					?>
					
				</td>
				
			</tr>
			
			<tr>
				<td>&nbsp;<br /><br /></td>			
			</tr>
			<!-- END ADDITIONAL GRAPHICS -->
			
			
			
		
			<!-- BEGIN SUBMIT BUTTON -->
			<tr>
				
				<td colspan='3' align='right'>
					
					<input type='submit' name='position_submit' value='Save Changes' onclick='return show_save_notice()' />
					
				</td>
			
			</tr>
			<!-- END SUBMIT BUTTON -->
		
		</table>
	
	</form>

</div>

<!-- END POSITION FORM -->