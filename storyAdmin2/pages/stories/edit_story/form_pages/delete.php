
<!-- BEGIN DELETE FORM -->
<div class='stories_edit_story_form_summary'>

	<div class='stories_edit_story_summary_title'>
		Delete Story
	</div>
	
	<div>
	
		This will delete the story from the system, so it will 
		no longer be visible to the public or to other users of
		this software.
		
		<br /><br />
		Everyone will be notified that this has happened (mailing
		list, System Admin, security logs, etc.) and you will be 
		identified as the person who did it.
		
		<br /><br />
		
		<br /><br />
		
		<div class='stories_edit_story_summary_title'>
			Do you want to continue?
		</div>
		
		<div>
			<form method='POST' action='?page=stories&edit_story=<?php echo $story->get_id(); ?>&delete_story=1'></div>
			
				<input type='submit' value='Yes, Delete Story' />
			
			</form>
		</div>

		<div style='text-align:right'>			
			<a href='?page=stories&edit_story=<?php echo $story->get_id(); ?>'>No, go back and edit the story</a>
		</div>
			
	</div>
	
</div>
<!-- END DELETE FORM -->


<?php 

require "inc/html_footer.php";
exit;
 	
?>