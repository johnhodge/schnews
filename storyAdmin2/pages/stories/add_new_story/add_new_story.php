<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	21st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Asks the user if they are sure they want to add the story, and informs them of what happens next.
 *
 */

unset( $_SESSION[ 'story_just_added' . SESSION_APPEND ] );
?>

<!-- BEGIN NEW STORY INSTRUCTION -->

<div class='instruction_text_whole_page'>

	<div class='instruction_text_whole_page_title'>
		YOU ARE ABOUT TO ADD A NEW STORY
		<br /><br />
		The Following Will Now Happen:
		
		<div class='instruction_text_whole_page_title_help_warning'>
			Any of the links below will take you to the Help system to explain the terms
		</div>
	</div>

	<div>

		<br /><br />

		<p>
			<b>1) The Story is Created and You Will Enter a Headline.</b><br />
			At the very least a headline is needed so the story can appear in the main list. Once you have added the headline you will be taken to the page where you can enter the rest of the story.
			<br /><br />
			<?php 
				$issue = new issue( ISSUES_get_newest_issue() );
			?>
			The story will be added as part of issue <b><?php echo $issue->get_number(); ?></b>
		</p>

		<br /><br />

		<p>
			<b>2) Edit the Story</b><br />
			You will be asked to enter the story text, and its other details (<a href='help?show=Keywords' target='_BLANK'>keywords</a>, <a href='help/?show=RSS' target='_BLANK'>RSS</a> text, summary, position on screen, etc.). At this point the story's existence will show up to other <a href='help/?show=Users' target='_BLANK'>users</a> and it will be <a href='help?show=Story_Locking' target='_BLANK'>locked</a> (i.e. other <a href='help/?show=Users' target='_BLANK'>users</a> will see that you are editing it and will be unable to edit it themselves until you are finished).
		</p>

		<br /><br />

		<p>
			<b>3) Sent to the Mailing List</b><br />
			Once you are complete, the story will be <a href='help?show=Story_Locking' target='_BLANK'>un-locked</a> and all your changes saved. The story will be sent to the mailing list and other <a href='help/?show=Users' target='_BLANK'>users</a> will be invited to come in and edit the story and/or <a href='help/?show=Voting_for_Stories' target='_BLANK'>vote</a> for it.
			<br /><br />
			Once the story has received at least <a href='help?show=Voting_for_Stories' target='_BLANK'><b><?php echo number_story_votes_to_live; ?></b></a> votes it will go live and be visible to the public.
		</p>

	</div>

	<br /><br /><br />

	<table width='100%'>
		<tr>
			<td align='left'>
				<form method='POST' action='?page=stories'>
					<input type='submit' name='cancel' value="No, I don't want to proceed, take me back to the story list." />
				</form>
			</td>
			<td align='right'>
				<form method='POST' action='?page=stories&edit_story=0&step=1'>
					<input type='submit' name='proceed' value="Yes, add the story and let me edit it." />
				</form>
			</td>
		</tr>
	</table>

</div>


<!-- END NEW STORY INSTRUCTION -->
