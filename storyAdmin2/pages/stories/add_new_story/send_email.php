<?php

$now  	=	new date_time( $story->get_date_added() );
$body	=	"Hello,


A new story has been added, by " . $_SESSION['logged_in_name'.SESSION_APPEND] . ".

The working title is '" . $story->get_headline() . ".

It was added on " . $now->make_human_date() . " at " . $now->make_human_time() . ".


SchNEWS AI";

$subject =	"SchNEWS StoryAdmin :: A New Story Has Been Added - " . $story->get_headline();

$mail	=	new email( $body, $subject, internal_mailing_list_address );
$mail->send();