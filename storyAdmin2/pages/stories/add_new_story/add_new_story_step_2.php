<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	21st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Adds the new story in the database and call the edit form
 *
 */



/*
 * Make a local copy of the Headline var
 */
$headline 	=	$_POST[ 'headline' ];



/*
 * 	Test if the summary was set and make a local copy if so
 */
if ( isset( $_POST[ 'summary' ] ) && trim( $_POST[ 'summary' ] ) )
{

	$summary = $_POST[ 'summary' ];

}



/*
 * 	Make a new story object, with empty data
 */
$story		=	new story();		//	This is only in memory at this stage. Nothing has been made in the database.



/*
 * 	Update the headline and summary properties in the story object
 */
$story->set_headline( $headline );
if ( isset( $summary ) )
{

	$story->set_summary( $summary );

}
$story->set_author( $_SESSION['logged_in_id'.SESSION_APPEND]);
$story->update_story_in_database();
$id 	=	$story->get_id();




/*
 * Send email notice to list
 */
if ( email_list_on_add )
{

	require "pages/stories/add_new_story/send_email.php";

}
?>




<div class='section_subtitle'>
	NEW STORY ADDED
</div>

<div class='explanatory_text'>

	<div class='big_notice_message'>
	
		New Story Added for Issue <?php echo $story->get_issue(); ?>
	
	</div>
	
	<br /><br />
	Your new story is now in the system.
	<br /><br >
	You will now be able to enter the story itself, as well as editing the peripheral information (e.g. <a href='help?show=Keywords' target='_BLANK'>RSS</a> text, <a href='help?show=Keywords' target='_BLANK'>keywords</a>, comments, etc.)

	<?php 
	if ( email_list_on_add )
	{
	?>
	
	<br /><br />
	<div style='color:red'>
	
		The Mailing List has been emailed a notification about the new story
	
	</div>
	<br /><br />
	<?php } ?>
	
	
</div>


<div class='stories_add_new_story_instruction'>

	<div class='stories_add_new_story_title'>
		Headline
	</div>
	<div class='stories_add_new_story_value'>
		<b><?php echo $story->get_headline(); ?></b>
	</div>
	<div class='stories_add_new_story_title'>
		Summary
	</div>
	<div class='stories_add_new_story_value'>
		<?php 
			if ( $story->get_summary() == '' )
			{
				echo "<span style='color: #aaaaaa'>[ Not Specified ]</span>";
			}
			else
			{
				echo $story->get_summary();
			}
		?>		

	</div>
	
	<div class='stories_add_new_story_continue_form'>
		
		<div style='text-align: left'>
			Once you click continue below you will be able to enter the story's content.
			<br /><br />
			The story will not be finished fully until you <a href='help?show=Story_Locking' target='_BLANK'>un-lock</a> it, using the button in the top right. This will close your editing session and allow other <a href='help?show=Users' target='_BLANK'>users</a> access to the story.
			<br /><br />
			When you <a href='help?show=Story_Locking' target='_BLANK'>un-lock</a> the story an email will be sent to the mailing list showing them the story and inviting them to come and <a href='help?show=Voting_for_Stories' target='_BLANK'>vote</a> for it. 		
		</div>
		
		<br /><br />
		
		<form method='POST' action='?page=stories&edit_story=<?php echo $story->get_id(); ?>&&checkout=<?php echo $_SESSION['logged_in_id'.SESSION_APPEND]; ?>&form_page=revision'>
		
			<input type='submit' name='add_story_checkout_submit' value="Continue to the next step, enter the story's content..." />
		
		</form>
	
	</div>

</div>





