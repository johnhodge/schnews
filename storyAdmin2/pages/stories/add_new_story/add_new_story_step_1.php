<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	21st August 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Asks the user if they are sure they want to add the story, and informs them of what happens next.
 *
 */

?>

<!-- BEGIN ADD NEW USER STEP 1 -->

<div class='section_subtitle'>
	Enter Headline for New Story
</div>

<br /><br />

<div class='explanatory_text'>
	<p>
		You will be able to edit this later on, so don't worry to much about getting it spot on now.
		<br /><br />
		However, other people will see this even though you haven't finished editing the story, so try to make it reasonably close to accurate for their sake.
	</p>
	<div class='explanatory_text_large' style='color: black'>
		<?php 
			
			$issue	=	new issue( ISSUES_get_newest_issue() );
			echo "<br />This story will be added as part of Issue <u>" . $issue->get_number() . "</u>";

		?>
	</div>
</div>

<br /><br />


<div style='clear:both'>

<form method='POST' action='?page=stories&edit_story=0&step=2'>
<table style='width: 100%'>


	<?php

		if ( isset( $error ) )
		{

			?>

				<tr>
					<td colspan='3' align='center'>

						<div class='error_message'>
							<?php echo $error; ?>
						</div>

					</td>
				</tr>

			<?php

		}

	?>


	<!-- BEGIN HEADLINE -->
	<tr>
		<td class='table_form_left'>
			Headline: 
			<br />
			<span style='color:blue'>[<i>Required</i>]</span>
			<br /><br />
			<i style='font-weight:normal'>[ You can use <b>&lt;i&gt;</b>, <b>&lt;u&gt;</b> and <b>&lt;b&gt;</b> HTML tags ]</i>
		</td>
		<td>
			&nbsp;
		</td>
		<td class='table_form_right'>
			<input type='text' name='headline' value='' style='width: 95%' />
		</td>
	</tr>
	<!-- END HEADLINE -->

	<tr>
		<td colspan='3' align='right'>
			<br />
			<input type='submit' name='submit' value='Add Story' />
		</td>
	</tr>



	<tr>
		<td colspan='3'>

			<br /><br />

			<div>
				<b>Optional</b><br />
				You can add a summary. This might be helpful to other users who will see this story while you are editing it...
			</div>

			<br /><br />

		</td>
	</tr>

	<!-- BEGIN SUMMARY -->
	<tr>
		<td class='table_form_left'>
			Summary:
			<br /><br />
			<i style='font-weight:normal'>[ You can use &lt;i&gt;, &lt;u&gt; and &lt;b&gt; HTML tags ]</i>
		</td>
		<td>
			&nbsp;
		</td>
		<td class='table_form_right'>
			<textarea name='summary' style='width: 95%; height: 125px'></textarea>
		</td>
	</tr>
	<!-- END SUMMARY -->

	<tr>
		<td colspan='3' align='right'>
			<br />
			<input type='submit' name='submit' value='Add Story' />
		</td>
	</tr>



</table>
</form>



<form method='POST' action='?page=stories'>
	<div>
		<br /><br /><br />
		<input type='submit' name='cancel' value="Cancel, don't save anything, and go back to the story list" />
	</div>
</form>

</div>



<!-- END ADD NEW USER STEP 1 -->


