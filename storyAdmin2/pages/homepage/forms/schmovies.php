

<div style='text-align: right'>

	<a id='HOMEPAGE_JQ_schmovies_link' class='homepage_show_hide_link' >Show</a>

</div>

<div class='homepage_form_title'>
	SchMOVIES Items on the Homepage
</div>

<?php 
	
	/*
	 * 	Display any notices and messages
	 */
	if ( isset( $schmovies_errors ) && count( $schmovies_errors ) > 0 )
	{
		
		foreach ( $schmovies_errors as $error )
		{
			
			echo "<div class='error_message'>" . $error . "</div>";
			
		}
		
	}
	
	
	if ( isset( $schmovies_message ) )
	{
		
		echo "<div class='notice_message'>$schmovies_message</div>";
		
	}
	
	?>
	
	
<div id='HOMEPAGE_JQ_schmovies' class='HOMEPAGE_JQ'>

	
	
	
	
	<table style='width: 100%'>
	
	<!-- SPACER -->
		<tr>
			<td>
				<br />
			</td>
		</tr>
					
		<tr>
					
			<td class='homepage_form_item_container'>
				
				<div style='font-size: 13pt; font-weight: bold'>
					Add New SchMOVIE Item
				</div>
				
				<form method='POST' action='?page=homepage&update_schmovie=0'>
						
					<b>Title: </b><br />
					<input type='text' name='title' value='' style='width: 75%' />
								
					<br /><br />
							
					<b>Summary: </b><br />
					<textarea name='summary' id='ck_sm_summary_0' ></textarea>
					
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ck_sm_summary_0',
						    {
						        toolbar : 	[
							   					['Bold', 'Italic'],
											],
						        uiColor : '#999999',
								width: '75%',
						        height: '80px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
					
						
					<br /><br />
								
					<b>Link: </b> (Optional)<br />
					<input type='text' name='link' value='' style='width: 75%' />
					
					<br /><br />
					
					<b>Date of Event (dd-mm-yyyy)</b><br />
					<input type='text' name='date' value='' style='width: 75%' />
					
					<br /><br />
								
					<input type='submit' name='schmovie_edit_submit' value='Add New SchMOVIE' />
							
				</form>
				
			</td>				
					
		</tr>	
		
		<tr>
			<td>
				
				<br /><br />
				
				<div style='font-size: 13pt; font-weight: bold'>
					Edit Existing SchMOVIES Items
				</div>
					
			</td>
		</tr>
		<?php 
		
		
			/*
			 * 	Grab the data from the DB
			 */
			$mysql	=	new mysql_connection();
			$sql	=	"	SELECT * 
							FROM schmovies
							ORDER BY date_added DESC
							LIMIT 0, " . number_schmovies_items_to_show;
			$mysql->query( $sql );
			
			
			while ( $res = $mysql->get_row() )
			{
	
				?>
				
					<!-- SPACER -->
					<tr>
						<td>
							<br />
						</td>
					</tr>
					
					<tr>
					
						<td class='homepage_form_item_container'>
						
							<form method='POST' action='?page=homepage&update_schmovie=<?php echo $res['id']; ?>'>
							
								<b>Title: </b><br />
								<input type='text' name='title' value='<?php echo stripslashes( $res['title'] );?>' style='width: 75%' />
								
								<br /><br />
								
								<b>Summary: </b><br />
								<textarea name='summary' id='ck_sm_summary_<?php echo $res['id']; ?>' ><?php echo stripslashes( $res['summary'] ); ?></textarea>
							
								<!-- BEGIN JS CALL TO CKEDITOR -->
								<script type='text/javascript'>
								
										CKEDITOR.replace( 'ck_sm_summary_<?php echo $res['id']; ?>',
									    {
									        toolbar : 	[
										   					['Bold', 'Italic'],
														],
									        uiColor : '#999999',
											width: '75%',
									        height: '80px'
									    });
								
								</script>
								<!-- END JS CALL TO CKEDITOR -->
								
								
								<br /><br />
								
								<b>Link: </b> (Optional)<br />
								<input type='text' name='link' value='<?php echo stripslashes( $res['link'] ); ?>' style='width: 75%' />
								
								<br /><br />
					
								<b>Date of Event (dd-mm-yyyy) </b><br />
								<input type='text' name='date' value='<?php echo DATE_turn_sql_to_text( $res['date_event'] ); ?>' style='width: 75%' />
								<br /><br />
								
								<input type='submit' name='schmovie_edit_submit' value='Update SchMOVIE' />
							
								<br /><br />
								
								<div style='text-align:right'>
									<a href='?page=homepage&delete_schmovie=<?php echo $res['id']; ?>'>Delete SchMOVIE Item</a>
								</div>
								
							</form>
						
						</td>				
					
					</tr>
				
				<?php
				 
			}
		
		?>
		
	</table>
	
</div>