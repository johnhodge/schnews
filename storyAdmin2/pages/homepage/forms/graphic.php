
<div style='text-align: right'>

	<a id='HOMEPAGE_JQ_graphics_link' class='homepage_show_hide_link' >Show</a>

</div>



<div class='homepage_form_title'>
	Main Graphics
</div>



<?php 
/*
 * 	Display any notices and messages
 */
if ( isset( $graphics_errors ) && count( $graphics_errors ) > 0 )
{
		
	foreach ( $graphics_errors as $error )
	{
			
		echo "<div class='error_message'>" . $error . "</div>";
			
	}
		
}

if ( isset( $graphics_message ) )
	{
		
		echo "<div class='notice_message'>$graphics_message</div>";
		
	}

?>



<div id='HOMEPAGE_JQ_graphics' class='HOMEPAGE_JQ'>

	<table width='100%'>
	<tr>
	<td class='homepage_form_item_container'>
	
		<div style='color: #666666'>
		
			<b>Add A New Set of Main Graphics</b>
			
			<br /><br />
	
			If you do, then your new graphics will replace the ones already 
			on the homepage...
					
		</div>
		
		<br /><br />
		
		<form method='POST' action='?page=homepage&change_graphics=0' enctype="multipart/form-data">
		
			<b>Graphic (Small): </b> Please, please, please make sure that this is exactly 264 px wide, and very close to 175px pixels high <br />
			<input type='file' name='graphic_sm' id='graphic_sm' style='width: 50%' />
			
			<br /><br />	
		
			<b>Graphic (Large): </b><br />
			<input type='file' name='graphic_lg' id='graphic_lg' style='width: 50%' />
			
			<br /><br />
			
			<b>Comment: </b> (Optional) <br />
			<textarea name='comment' style='width: 75%; height: 80px'></textarea>
			
			<br /><br />
			
			<input type='submit' name='graphics_submit' value='Upload New Graphics' />
			
		</form>
		
	</td>
	</tr>
		
		
	<!-- SPACER -->	
	<tr>
		<td>
			<br /><br />		
		</td>
	</tr>
	

	<tr>
		<td>
		
			<div style='color: #666666'>
		
				<b>Previous Main Graphics...</b>
			
			</div>
					
		</td>
	</tr>


	
	<?php 
	
		$mysql	=	new mysql_connection();
		$sql	=	"	SELECT graphic_sm, comment, date
						FROM homepage_graphics
						ORDER BY date DESC
						LIMIT 0, 100 ";
		$mysql->query( $sql );
		
		while ( $res	=	$mysql->get_row() )
		{
		?>
		
			
			<!--  SPACER -->			
			<tr>
				<td>
					&nbsp;
				</td>
			</tr>		
		
			<tr>
				<td class='homepage_form_item_container'>
			
					<table width='100%'>
						<tr>
						
							<td>
							
								<img src='<?php echo SITE_IMAGE_URL . $res['graphic_sm']; ?>' style='border: 2px solid black' />
							
							</td>
							<td valign='top' width='75%' style='padding-left: 30px'>
							
								<?php 
								
									$date		=	new date_time( $res['date'] );
																	
								?>
								
								<div style='font-weight: bold; color: #666666; text-align: right; padding: 10px'><?php echo $date->make_human_date(); ?></div>
							
								<?php echo nl2br( htmlentities( $res['comment'] ) ); ?>
							
							</td>
						
						</tr>					
					</table>
			
				</td>
			</tr>
		
		
		<?php 
		}
		?>


	</table>
			
</div>	



