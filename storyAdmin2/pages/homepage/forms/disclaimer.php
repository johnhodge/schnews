<?php 

	$data		=	new homepage_misc_fields();

?>



<div style='text-align: right'>

	<a id='HOMEPAGE_JQ_disclaimer_link' class='homepage_show_hide_link' >Show</a>

</div>



<div class='homepage_form_title'>
	Disclaimer
</div>



<?php 

if ( isset( $disclaimer_message ) )
	{
		
		echo "<div class='notice_message'>$disclaimer_message</div>";
		
	}

?>



<div id='HOMEPAGE_JQ_disclaimer' class='HOMEPAGE_JQ'>

	<br /><br />

	<form method='POST' action='?page=homepage&change_disclaimer=true'>
	
		<input type='text' name='disclaimer' value='<?php echo htmlentities( $data->get_disclaimer(), ENT_QUOTES ); ?>' style='width: 75%' />
		
		<br /><br />
		
		<input type='submit' name='disclaimer_submit' value='Update Disclaimer Text' />
	
	</form>
	
</div>