
<div style='text-align: right'>

	<a id='HOMEPAGE_JQ_and_finally_link' class='homepage_show_hide_link' >Show</a>

</div>



<div class='homepage_form_title'>
	And Finally... Text
</div>



<?php 
/*
 * 	Display any notices and messages
 */
if ( isset( $and_finally_errors ) && count( $and_finally_errors ) > 0 )
{
		
	foreach ( $and_finally_errors as $error )
	{
			
		echo "<div class='error_message'>" . $error . "</div>";
			
	}
		
}

if ( isset( $and_finally_message ) )
	{
		
		echo "<div class='notice_message'>$and_finally_message</div>";
		
	}

?>



<div id='HOMEPAGE_JQ_and_finally' class='HOMEPAGE_JQ'>

	<table width='100%'>
	<tr>
	<td class='homepage_form_item_container'>
	
		<div style='color: #666666'>
		
			<b>Add New And Finally... Text</b>
			
			<br /><br />
	
			If you do, then your new text will replace what is there already 
			on the homepage...
					
		</div>
		
	
	
		<form method='POST' action='?page=homepage&change_and_finally=0'>
		
			<b>Title: </b><br />
			<input type='text' name='title' value='' style='width: 75%' />
			
			<br /><br />
			
			<b>Summary: </b><br />
			<textarea name='summary' id='ck_af_summary_0' ></textarea>
			
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ck_af_summary_0',
						    {
						        toolbar : 	[
							   					['Bold', 'Italic'],
											],
						        uiColor : '#999999',
								width: '75%',
						        height: '80px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
					
					
			<br /><br />
			
			<b>Content: </b><br />
			<textarea name='content' id='ck_af_content_0' ></textarea>
			
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ck_af_content_0',
						    {
						        toolbar : 	[
							   					['Bold', 'Italic', 'Underline', '-', 'Link', 'Unlink'],
											],
						        uiColor : '#999999',
								width: '75%',
						        height: '200px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
			<br /><br />
			
			<input type='submit' name='and_finally_submit' value='Add New And Finally... Text' />
		
		</form>
	
		</td>
	</tr>
	
	
	
	<!-- SPACER -->	
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	
	<tr>
		<td>

			<div style='color: #666666'>
		
				<b>Edit Existing Entries...</b>		
		
			</div>
			
		</td>	
	</tr>
	
	<!-- SPACER -->	
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>


	
	<?php 
	/*
	 * 	Load the 10 most recent Crap Arrests entries from the DB
	 */
	$mysql	=	new mysql_connection();
	$sql	=	"	SELECT *
					FROM homepage_and_finally
					ORDER BY date DESC
					LIMIT 0, 10 ";
	$mysql->query( $sql );	
	
	while( $res		=	$mysql->get_row() )
	{ 
	?>
	
		<!-- SPACER -->	
		<tr>
			<td>
				&nbsp;
			</td>
		</tr>
			
	
		<tr>
		
			<td class='homepage_form_item_container'>
		
				<form method='POST' action='?page=homepage&change_and_finally=<?php echo $res['id']; ?>'>
		
					<b>Title: </b><br />
					<input type='text' name='title' value='<?php echo htmlentities( $res['title'] ); ?>' style='width: 75%' />
					
					<br /><br />
					
					<b>Summary: </b><br />
					<textarea name='summary' id='ck_af_summary_<?php echo $res['id']; ?>' ><?php echo $res['summary']; ?></textarea>
					
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ck_af_summary_<?php echo $res['id']; ?>',
						    {
						        toolbar : 	[
							   					['Bold', 'Italic'],
											],
						        uiColor : '#999999',
								width: '75%',
						        height: '80px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
					
					
					<br /><br />
					
					<b>Content: </b><br />
					<textarea name='content' id='ck_af_content_<?php echo $res['id']; ?>' ><?php echo htmlentities( $res['content'] ); ?></textarea>
					
					<!-- BEGIN JS CALL TO CKEDITOR -->
					<script type='text/javascript'>
					
							CKEDITOR.replace( 'ck_af_content_<?php echo $res['id']; ?>',
						    {
						        toolbar : 	[
												['Bold', 'Italic', 'Underline', '-', 'Link', 'Unlink'],
											],
						        uiColor : '#999999',
								width: '75%',
						        height: '200px'
						    });
					
					</script>
					<!-- END JS CALL TO CKEDITOR -->
				
					
					<br /><br />
					
					<input type='submit' name='and_finally_submit' value='Update And Finally... Text' />
					
					<div style='text-align: right'>
						<a href='?page=homepage&delete_and_finally=<?php echo $res['id']; ?>'>Delete</a>
					</div>
				
				</form>
		
			</td>
		
		</tr>
		
	<?php 
	}
	?>
	
	</table>
	
</div>