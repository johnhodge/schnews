<?php


if ( !isset( $_GET['update_schmovie'] ) || !CORE_is_number( $_GET['update_schmovie'] ) )
{
	
	$message 	=	"Add SchMOVIES script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
}




/*
 * 	Test and validate the input data
 */
require "pages/homepage/handlers/schmovies/validate.php";



if ( count( $schmovies_errors ) == 0 )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	" 	INSERT INTO schmovies
					( 
						title, 
						summary, 
						link, 
						date_added,
						date_event
					)
					VALUES
					(
						'" . $mysql->clean_string( $data['title'] ) . "',					
						'" . $mysql->clean_string( $data['summary'] ) . "',					
						'" . $mysql->clean_string( $data['link'] ) . "',
						'" . DATETIME_make_sql_datetime() . "',	
						'" . $mysql->clean_string( $data['date_event'] ) . "'
						)";	
	$mysql->query( $sql );	
	
	$schmovies_message	=	"New SchMOVIES Item Added";

}