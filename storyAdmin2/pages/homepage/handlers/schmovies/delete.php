<?php


if ( !isset( $_GET['delete_schmovie'] ) || !CORE_is_number( $_GET['delete_schmovie'] ) )
{
	
	$message 	=	"Edit SchMOVIES script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
	
}

$id		=	$_GET['delete_schmovie'];


$mysql	=	new mysql_connection();
$sql	=	"	DELETE FROM schmovies
				WHERE id = $id ";
$mysql->query( $sql );

$schmovies_message	=	"SchMOVIE Item Deleted";