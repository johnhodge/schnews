<?php


if ( !isset( $_GET['update_schmovie'] ) || !CORE_is_number( $_GET['update_schmovie'] ) )
{
	
	$message 	=	"Add SchMOVIES script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
}

$id		=	$_GET['update_schmovie'];


/*
 * 	Test and validate the input data
 */
require "pages/homepage/handlers/schmovies/validate.php";



if ( count( $schmovies_errors ) == 0 )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	UPDATE schmovies
					SET 
						title			=	'" . $mysql->clean_string( $data['title'] ) . "', 
						summary			=	'" . $mysql->clean_string( $data['summary'] ) . "', 
						link			=	'" . $mysql->clean_string( $data['link'] ) . "',
						date_event 		=	'" . $mysql->clean_string( $data['date_event'] ) . "'
					WHERE
						id = $id";
	$mysql->query( $sql );	
	
	$schmovies_message	=	"SchMOVIES Item Updated";

}