<?php

$data					=	array();
$schmovies_errors		=	array();

if ( isset( $_POST['title'] ) && trim( $_POST['title'] ) != '' )
{
	
	$data['title'] 	=	$_POST['title'];
	
}
else
{
	
	$schmovies_errors[]	=	'Title not set';
	
}

if ( isset( $_POST['summary'] ) && trim( $_POST['summary'] ) != '' )
{
	
	$data['summary']	=	$_POST['summary'];
	
}
else
{
	
	$schmovies_errors[]	=	"Summary not set";
	
}

if ( isset( $_POST['link'] ) && trim( $_POST['link'] ) != '' )
{
	
	$data['link'] 	=	$_POST['link'];
	
}
else
{
	
	$data['link']	=	'';
	
}


if ( isset( $_POST['date'] ) && trim( $_POST['date'] ) != '' && DATE_turn_text_to_sql( $_POST['date'] ) != false  )
{
	
	$data['date_event']	=	DATE_turn_text_to_sql( $_POST['date'] );
	
}
else
{
	
	$schmovies_errors[]	=	"Date not correctly set";
	
}