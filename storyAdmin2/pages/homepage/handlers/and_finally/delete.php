<?php


if ( !isset( $_GET['delete_and_finally'] ) || !CORE_is_number( $_GET['delete_and_finally'] ) )
{
	
	$message 	=	"Delete And Finally script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
}


$mysql	=	new mysql_connection();
$sql	=	"	DELETE FROM homepage_and_finally
				WHERE id = " . $mysql->clean_string( $_GET['delete_and_finally'] );
$mysql->query( $sql );


$and_finally_message	=	"And Finally Item Deleted";