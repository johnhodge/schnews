<?php


if ( !isset( $_GET['change_and_finally'] ) || !CORE_is_number( $_GET['change_and_finally'] ) )
{
	
	$message 	=	"Add And Finally script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
}




/*
 * 	Test and validate the input data
 */
require "pages/homepage/handlers/and_finally/validate.php";



if ( count( $and_finally_errors ) == 0 )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	"	UPDATE homepage_and_finally
					SET
						title	=	'" . $mysql->clean_string( $data['title'] ) . "',
						summary	=	'" . $mysql->clean_string( $data['summary'] ) . "',
						content	=	'" . $mysql->clean_string( $data['content'] ) . "'
					WHERE 
						id = " . $_GET['change_and_finally'];	
	$mysql->query( $sql );	
	
	$and_finally_message	=	"And Finally Item Updated";

}