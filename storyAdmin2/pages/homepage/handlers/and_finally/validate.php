<?php

$data					=	array();
$and_finally_errors		=	array();



if ( isset( $_POST['title'] ) )
{
	
	$data['title']	=	$_POST['title'];
	
}
else
{

	$and_finally_errors[]	=	"Title Not Set";
	
}

if ( isset( $_POST['summary'] ) )
{
	
	$data['summary']	=	$_POST['summary'];
	
}
else
{
	
	$and_finally_errors[]	=	"Summary Not Set";
	
}

if ( isset( $_POST['content'] ) )
{
	
	$data['content'] 	=	$_POST['content'];
		
}
else
{

	$and_finally_errors[]	=	"Content Not Set";
	
}
