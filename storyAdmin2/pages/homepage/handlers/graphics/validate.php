<?php
$data				=	array();
$graphics_errors	=	array();



/*
 *	TEST SMALL GRAPHIC 
 */
if ( $_FILES['graphic_sm']['error'] > 0 )
{
	
	$graphics_errors[]	=	"Error Uploading Small Graphic";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['graphic_sm']['name'];
	
	if ( !move_uploaded_file( $_FILES['graphic_sm']['tmp_name'] , $target ) )
	{
		
		$graphics_errors[]	=	"Error Processing Uploaded Small Graphic File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$graphics_errors[]	=	"Error setting permissions for new Small Graphics file";
	
	}
	
	
	
	$data['graphic_sm']		=	$_FILES['graphic_sm']['name'];
	
}



/*
 *	TEST LARGE GRAPHIC 
 */
if ( $_FILES['graphic_lg']['error'] > 0 )
{
	
	$graphics_errors[]	=	"Error Uploading Large Graphic";
	
}
else
{
	
	$target		=	SITE_IMAGE_DIR . $_FILES['graphic_lg']['name'];
	
	if ( !move_uploaded_file( $_FILES['graphic_lg']['tmp_name'] , $target ) )
	{
		
		$graphics_errors[]	=	"Error Processing Uploaded Large Graphic File";
		
	}
	elseif ( !chmod( $target, 0666 ) )
	{
	
		$graphics_errors[]	=	"Error setting permissions for new Large Graphics file";
	
	}
	
	$data['graphic_lg']		=	$_FILES['graphic_lg']['name'];
	
}




if ( isset( $_POST['comment'] ) )
{
	
	$data['comment'] 	=	$_POST['comment'];
	
}
else
{
	
	$data['comment']	=	'';
	
}

