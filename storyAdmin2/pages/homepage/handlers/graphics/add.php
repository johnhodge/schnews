<?php


if ( !isset( $_GET['change_graphics'] ) || !CORE_is_number( $_GET['change_graphics'] ) )
{
	
	$message 	=	"Add Graphics script called with no valid GET var";
	debug( $message, __FILE__, __LINE__ );
	LOG_record_entry( $message, 'error', 1 );
	
	require 'inc/html_footer.php';
	exit;
	
}




/*
 * 	Test and validate the input data
 */
require "pages/homepage/handlers/graphics/validate.php";




if ( count( $graphics_errors ) == 0 )
{
	
	$mysql	=	new mysql_connection();
	$sql	=	" 	INSERT INTO homepage_graphics
					( 
						graphic_sm,
						graphic_lg,
						comment,
						user,
						date
					)
					VALUES
					(
						'" . $mysql->clean_string( $data['graphic_sm'] ) . "',
						'" . $mysql->clean_string( $data['graphic_lg'] ) . "',
						'" . $mysql->clean_string( $data['comment'] ) . "',
						'" . $_SESSION['logged_in_id'.SESSION_APPEND] . "',				
						'" . DATETIME_make_sql_datetime() . "'					
					)";
	$mysql->query( $sql );

	$graphics_message		=	"New Main Graphics Added";
	
}