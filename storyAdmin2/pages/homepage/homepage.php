<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	28th August 2011
 *
 */


/*
 * The the homepage settings
 */
SETTINGS_load_homepage_settings();






/*
 * 	TEST FOR UPDATES AND ADDITIONS
 */



/*
 * 	SCHMOVIES
 */
if ( isset( $_GET['update_schmovie'] ) )
{
	
	/*
	 * If we're adding a new item
	 */
	if ( $_GET['update_schmovie'] == 0 )
	{
		
		require "pages/homepage/handlers/schmovies/add.php";
			
	}
	elseif ( CORE_is_number( $_GET['update_schmovie'] ) )
	{
		
		require "pages/homepage/handlers/schmovies/edit.php";
	}
	
}

if ( isset( $_GET['delete_schmovie'] ) && CORE_is_number( $_GET['delete_schmovie'] ) )
{

	require "pages/homepage/handlers/schmovies/delete.php";
	
}




/*
 * 	CRAP ARREST
 */
if ( isset( $_GET['change_crap_arrest'] ) && CORE_is_number( $_GET['change_crap_arrest'] ) )
{

	if ( $_GET['change_crap_arrest'] == 0 )
	{

		require "pages/homepage/handlers/crap_arrest/add.php";
		
	}
	else
	{
	
		require "pages/homepage/handlers/crap_arrest/edit.php";
	
	}
	
}
if ( isset( $_GET['delete_crap_arrest'] ) && CORE_is_number( $_GET['delete_crap_arrest'] ) && $_GET['delete_crap_arrest'] != 0  )
{

		require "pages/homepage/handlers/crap_arrest/delete.php";

}





/*
 * 	AND FINALLY
 */
/*
 * 	CRAP ARREST
 */
if ( isset( $_GET['change_and_finally'] ) && CORE_is_number( $_GET['change_and_finally'] ) )
{

	if ( $_GET['change_and_finally'] == 0 )
	{

		require "pages/homepage/handlers/and_finally/add.php";
		
	}
	else
	{
	
		require "pages/homepage/handlers/and_finally/edit.php";
	
	}
	
}
if ( isset( $_GET['delete_and_finally'] ) && CORE_is_number( $_GET['delete_and_finally'] ) && $_GET['delete_and_finally'] != 0  )
{

		require "pages/homepage/handlers/and_finally/delete.php";

}


/*
 * 	DOSCLAIMER
 */
if ( isset( $_GET['change_disclaimer'] ) && $_GET['change_disclaimer'] == true )
{
	
	require "pages/homepage/handlers/disclaimer/edit.php";
	
}




/*
 * 	AND FINALLY
 */
if ( isset( $_GET['change_wake_up'] ) && $_GET['change_wake_up'] == true )
{
	
	require "pages/homepage/handlers/wake_up/edit.php";
	
}




/*
 * 	MAIN GRAPHICS
 */
if ( isset( $_GET['change_graphics'] ) && $_GET['change_graphics'] == 0 )
{
	
	require "pages/homepage/handlers/graphics/add.php";
	
}








?>


<!-- BEGIN INTRO TEXT -->

<div class='homepage_instructions'>

	Use the Show / Hide buttons on the right to activate the editors

</div>

<!-- END INTRO TEXT -->




<!-- BEGIN SCHMOVIES EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/schmovies.php"; ?>
</div>

<!--  END SCHMOVIES EDITOR -->



<!-- BEGIN CRAP ARREST EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/crap_arrest.php"; ?>
</div>

<!--  END CRAP ARREST EDITOR -->



<!-- BEGIN AND FINALLY EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/and_finally.php"; ?>
</div>

<!--  END AND FINALLY EDITOR -->



<!-- BEGIN DISCLAIMER EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/disclaimer.php"; ?>
</div>

<!--  END DISCLAIMER EDITOR -->




<!-- BEGIN WAKE UP EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/wake_up_text.php"; ?>
</div>

<!--  END WAKE UP EDITOR -->




<!-- BEGIN MAIN GRAPHIC EDITOR -->

<div class='homepage_form_container'>
<?php require "pages/homepage/forms/graphic.php"; ?>
</div>

<!--  END MAIN GRAPHIC EDITOR -->


