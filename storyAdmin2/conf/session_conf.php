<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	Configuration data for the PHP Sessions and Cookies
 */



/*
 *	SESSION APPEND
 *
 * 	This string will be appended to the end of all session variable names.
 *	It makes determining the name of the session var just a little harder.
 *
 * 	Recommended to leave it to the default, which is an MD5 of the user IP. This has two benefits:
 *
 * 		1) It is unique to each user (or at least each user's network)
 * 		2) It will log a user out if their IP changes
 */
if (!defined('SESSION_APPEND'))
{
	define ('SESSION_APPEND', md5($_SERVER['REMOTE_ADDR']));
}