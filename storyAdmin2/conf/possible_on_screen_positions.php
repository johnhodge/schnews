<?php
	$possible_on_screen_positions		=	array(
	
			'1'	=> 'Left',
			'2' => 'Middle',
			'3' => 'Right'
	
	);
	
	$possible_on_screen_sizes			=	array(
	
			'1' => 'Small',
			'2' => 'Medium',
			'3' => 'Large'
	
	);