<?php
/*
 * 	ABOUT THIS SOFTWARE
 *
 * 	StoryAdmin2 - Online Editorial CMS System for SchNEWS.ORG.UK
 * 	Copyright (c) 2011 SchNEWS
 *
 * 	This software is copyleft - GPL v2 license applies (http://www.gnu.org/licenses/gpl-2.0.html)
 *
 * 	@author: 	Andrew Winterbottom
 * 	@support:	support@andrewwinterbottom.com webmaster@schnews.org.uk
 * 	@license:	GPL v2
 * 	@last_mod:	15th June 2011
 *
 * 	========================================================
 *
 * 	ABOUT THIS FILE
 *
 * 	This script controls all the configuration files.
 *
 * 	It basically loads the other config scripts, and contains a couple of global settings
 */




/*
 * 	Override deault MySQL settings.
 * 
 * 	Might be useful for testing or migrating to another server.
 */
if ( file_exists( 'conf/mysql_conf.php' ) )
{
	
	require "conf/mysql_conf.php";
	
}



/*
 * 	Overrides for session handling
 */
if ( file_exists( 'conf/session_conf.php' ) )
{
	
	require "conf/session_conf.php";
	
}



/*
 * SYSTEM ADMIN
 *
 * Receives various sytem wide email notices and security info
 */
define ( 'SYSTEM_ADMIN_NAME', 'Andrew Winterbottom' );
define ( 'SYSTEM_ADMIN_EMAIL', 'support@andrewwinterbottom.com' );






/*
 * 	SITE ROOT
 *
 * 	Where is the index.php for this site located, as an absolute URL
 */
define ( 'SYSTEM_SITE_ROOT', 'http://www.schnews.org.uk/storyAdmin2/');




/*
 *  IMAGE DIR
 *  
 *  WHere to upload the graphics file to...
 */
 
//	CAIN
define ( 'SITE_IMAGE_DIR', "/home/schnews/www.schnews.org.uk/images/" );
define ( 'SITE_IMAGE_URL', "http://www.schnews.org.uk/images/" );




/*
 * 	TEMP DIR
 * 
 * 	Where should temporary files be saved (e.g. in progress revisions, etc.)
 * 
 * 	WARNING: This should be slash terminated.
 * 
 */
define ( 'SYSTEM_TEMP_DIR', 'temp'.DIRECTORY_SEPARATOR );




/*
 * 	STORY LOC
 * 
 * 	Where are the stories located? (probably /stories
 */
define ( 'SITE_STORY_LOC', "../stories/" );