function show_save_warning()
{
	
	return confirm("This will move away from this page.\r\n\r\nWARNING: Any unsaved changes will be lost. Make sure you have saved.\r\n\r\nOK to Proceed?");
	
}


function show_save_notice()
{
	
	return alert("This will save your changes to this page.\r\n\r\nYou will still have the story locked and you can still continue to edit all aspects of the story" );
	
}


function show_revision_revert_warning()
{
	
	return confirm( "WARNING\r\n\r\nThis will replace the current revision with this text.\r\n\r\nThe current text will not be saved\r\n\r\nLike all changes to thestories content, this will not be saved and fialised until you unlocked the story and save your changes.\r\n\r\n\r\nAre you sure you want to proceed?" );
	
}


function show_publish_issue_warning()
{
	
	return confirm( "WARNING\r\n\r\nThis will close the current issue ad start a new one.\n\r\nAll new stories will be added to the new issue.\r\n\r\n\r\nAre you sure you want to proceed?" );
	
}

