<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Merchandise - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
						 <!--#include virtual="../inc/bannerGraphic_merchandise.php"-->
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarMain.php"-->

</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
		 <!--#include virtual="../inc/navBar_merchandise.php"-->

	<table border="0" cellpadding="0" cellspacing="0">
      <!--  Internal Links -->
      <tr>
        <td colspan="2" height="138" valign="top">
        
<p style="font-size: small;"><em>Don't order anything on this site - but if you really want something email <a href="mailto:schnews@riseup.net">schnews@riseup.net</a> you never know. </em></p>
		
          <p><b><font face="Arial, Helvetica, sans-serif" size="2">NEW - Out June
            2011 on DVD - Raiders From The Lost Archive 2009/10 (Vol 2) </font></b></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="#raiders2">Raiders
            From The Lost Archive Vol 2</a> - Part two of SchMOVIES 2008-11 collection
            - £6 incl p&amp;p<a href="#rftv"></a><a href="#raiders"><br>
            Raiders From The Lost Archive Vol 1</a> - Part one of SchMOVIES 2008-11
            collection - £6 incl p&amp;p<a href="#rftv"><br>
            Reports From The Verge</a> - The Smash EDO/ITT Anthology 2005-2009
            - £6 incl p&amp;p <br>
            <a href="#uncertified">Uncertified </a>- The SchMOVIES DVD Collection
            2008 - £6 incl p&amp;p<br>
            <a href="#verge">On The Verge -</a> The Smash EDO campaign film -
            £6 incl p&amp;p<br>
            <a href="#take3">Take Three</a> - SchMOVIES Collection 2007 - £6
            incl p&amp;p<br>
            <a href="#v">V For Video Activist</a> - SchMOVIES Collection 2006
            - £6 incl p&amp;p <br>
            <a href="#2005">SchMOVIES DVD Collection 2005</a> - £6 incl
            p&amp;p<br>
            <a href="#at10">SchNEWS At Ten - The Movie</a> - a decade of party
            &amp; protest - £5 incl p&amp;p<br>
            <a href="#sj">Casualisation Kills</a> - The Simon Jones Memorial Campaign
            Film<br>
            <a href="#gc">Guerillavision Compilation</a> - Rattle In Seattle,
            Capitals Ills, Crowd Bites Wolf</b></font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="221" valign="top">
          <table align="right" height="93" width="51">
            <tr>
              <td height="98">
                <div align="center"><font size="4"><b><a href="../schmovies/images/raiders2-1000.jpg" target="_blank"><img alt="Raiders From The Lost Archive Vol 2" border="0" height="282" src="../schmovies/images/raiders2-200.jpg" style="margin-left: 15px" width="200"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
                  <font size="1">click here for larger image </font></a></font></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="raiders2"></a></font></b></font></b></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif">RAIDERS
            OF THE LOST ARCHIVE (Vol 2) - OUT NOW<br>
            </font></b></font><font face="Arial, Helvetica, sans-serif"><b>Part
            two of the SchMOVIES collection 2008-2011</b></font></p>
          <p>This second volume of the Raiders compilation includes
            the final films and footage which had been confiscated for up to eighteen
            months by police following the raid of a SchMOVIES film-maker in June
            2009.</p>
          <p>Presented here, amongst others, is the film that instigated the raid
            in the first place, Batons nBombs, as well
            as others covering the dramatic conclusion to the EDO Decommissioners
            trial, the rise and fall of the much-loved Lewes Road Community Garden
            in Brighton, several recent actions from UK Uncut and more. Once again
            we have three SchMUSIC vids as well - big thanks to all the bands
            who contributed. </p>
          <p><b>Available Now - £6 incl P&amp;P</b></p>
          <p>
           </p>
		  
    </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Raiders of the Lost Archive - Vol 1 -->
      <tr>
        <td colspan="2" height="221" valign="top">
          <table align="right" height="93" width="51">
            <tr>
              <td height="98">
                <div align="center"><font size="4"><b><a href="../schmovies/images/raiders-1000.jpg" target="_blank"><img alt="Raiders From The Lost Archive Vol 1" border="0" height="284" src="../schmovies/images/raiders-200.jpg" style="margin-left: 15px" width="200"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
                  <font size="1">click here for larger image </font></a></font></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="raiders"></a></font></b></font></b></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif">RAIDERS
            OF THE LOST ARCHIVE (Vol 1)<br>
            </font></b></font><font face="Arial, Helvetica, sans-serif"><b>Part
            one of the SchMOVIES collection 2008-2011</b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This DVD features
            a number of films which were held by Sussex police for over a year
            following the raid and confiscation of all SchMOVIES equipment during
            an intelligence gathering operation in June 2009 related to the <a href="http://www.smashedo.org.uk" target="_blank">Smash
            EDO</a> campaign.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">The films in this
            collection include demonstrations against the Israeli bombing of Gaza
            in early 2009, the <a href="../archive/news729.htm">EDO
            Decommissioners trial</a>, Smash EDO protests and more direct action
            on the south coast... PLUS music clips of some of the bands who have
            performed at recent SchNEWS/SchMOVIES benefit gigs including Brighton&#39;s
            finest and perennial festival fave &#39;Tragic Roundabout&#39;.</font></p>
          <p><b>Available Now:</b></p>
          <p>
          </p>
          <p>Or send cheques for the above amount made to &#39;Justice&#39; to the address
            at the bottom of this page...</p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Reports from the Verge -->
      <tr>
        <td colspan="2" height="101" valign="top">
          <table align="right" height="93" width="51">
            <tr>
              <td height="98">
                <div align="center"><font size="4"><b><a href="../schmovies/images/rftv-1000.jpg" target="_blank"><img alt="Reports From The Verge" border="0" height="283" src="../schmovies/images/rftv-200.jpg" style="margin-left: 15px" width="200"></a></b></font><font face="Arial, Helvetica, sans-serif" size="2"><a href="images/batonsnbombs-900.jpg" target="_blank"><br>
                  <font size="1">click here for larger image </font></a></font></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="rftv"></a></font></b></font></b></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif">REPORTS
            FROM THE VERGE<br>
            </font></b></font><font face="Arial, Helvetica, sans-serif"><b><font size="2">Smash
            EDO/ITT Anthology 2005-2009</font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">A new collection
            of twelve SchMOVIES covering the Smash EDO/ITT&#39;s campaign efforts
            to shut down the Brighton based bomb factory since the company sought
            its draconian injunction against protesters in 2005. This series of
            films illustrate what lengths EDO and Sussex Police will go to stop
            the campaign and how Smash EDO hit back.</font></p>
          <font face="Arial, Helvetica, sans-serif" size="2">
          <input name="cmd" type="hidden" value="_xclick">
          <input name="business" type="hidden" value="paypal@schnews.org.uk">
          <input name="undefined_quantity" type="hidden" value="1">
          <input name="item_name" type="hidden" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009">
          <input name="item_number" type="hidden" value="REPORTS FROM THE VERGE - Smash EDO/ITT Anthology 2005-2009">
          <input name="amount" type="hidden" value="6.00">
          <input name="no_shipping" type="hidden" value="2">
          <input name="return" type="hidden" value="http://www.schnews.org.uk/extras/complete.htm">
          <input name="cancel_return" type="hidden" value="http://www.schnews.org.uk/extras/cancel.htm">
          <input name="currency_code" type="hidden" value="GBP">
          <input name="lc" type="hidden" value="GB">
          <input name="bn" type="hidden" value="PP-BuyNowBF">
          </font>
          <table>
            <tr>
              <td><font face="Arial, Helvetica, sans-serif" size="2">
                <input alt="Make payments with PayPal - it&#39;s fast, free and secure!" border="0" name="submit2" src="https://www.paypal.com/en_US/i/btn/x-click-but23.gif" type="image">
                </font></td>
              <td align="left"><font face="Arial, Helvetica, sans-serif" size="2">£6
                per copy (inc. P&amp;P) <br>
                (via Paypal) </font></td>
            </tr>
          </table>
          <font face="Arial, Helvetica, sans-serif" size="2"><img alt="" border="0" height="1" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1">
          </font> 
          
          Or send cheques for the above amount made to &#39;Justice&#39; to the address
          at the bottom of this page... </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Uncertified - SchMOVIES DVD Collection 2008 -->
      <tr>
        <td colspan="2" height="189" valign="top">
          <table align="right" height="209" width="204">
            <tr>
              <td height="262">
                <div align="center"><a href="../schmovies/images/uncertified-600.jpg" target="_blank"><img alt="Uncertified - SchMOVIES 2008" border="0" src="../schmovies/images/uncertified-200.jpg" style="margin-left: 15px" width="200"><br>
                  <font size="1">click here for larger image</font> </a></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="uncertified"></a>UNCERTIFIED</font></b></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif"><font size="2"><br>
             SchMOVIES DVD Collection 2008</font></font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Films on this
            DVD include...</b> The saga of <b>On The verge</b>  the film
            they tried to ban, the Newhaven anti-incinerator campaign, Forgive
            us our trespasses - as squatters take over an abandoned Brighton church,
            Titnore Woods update, protests against BNP festival and more... To
            view some of these films <A HREF="../schmovies/index.php">click here</a></font></p>
          <p>
          </p>
          
          <p></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="http://enr.clearerchannel.org/media/schnews/uncertified-trailer.mpg" target="_blank">*
            Click here</a> to download trailer </b>- (39 sec, mpg format, 4.9
            meg, Jan 2009).</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>* To download
            &#39;Uncertified&#39; on bit torrent</b> (900meg) <a href="http://onebigtorrent.org/torrents/5221/UNCERTIFIED" target="_blank">click
            here</a></font><font face="Arial, Helvetica, sans-serif"> </font><font face="Arial, Helvetica, sans-serif" size="2">
            </font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!--  On The Verge -->
      <tr>
        <td colspan="2" height="282" valign="top">
          <table align="right" height="209" width="204">
            <tr>
              <td height="262">
                <div align="center"><a href="../schmovies/images/on-the-verge-800.jpg" target="_blank"><img alt="On The Verge - the Smash EDO Campaign film produced by SchMOVIES" border="0" height="281" src="../schmovies/images/on-the-verge-200.jpg" style="margin-left: 15px" width="200"><br>
                  <font size="1">click here for larger image</font> </a></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="verge"></a></font></b></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif">ON
            THE VERGE <font size="2"><br>
             The Smash EDO Campaign Film</font></font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Get the film
            the police tried to ban!</b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This 90 minute
            film tells the story of the four year campaign in Brighton to close
            down EDO-MBM, a local factory producing weapons parts for the US weapons
            corporation EDO Corp - and used in Iraq, Palestine, Afghanistan and
            elsewhere. Using activist, police and CCTV footage, as well as interviews
            with those involved, it is the story of a resilient and successful
            direct action in the face of the police and private security protecting
            the company.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">Order yours now
            for £6 including postage - use the link below to order a copy
            via paypal, or send cheque to &#39;Justice&#39; to the address below (with
            profits going to Smash EDO).</font></p>
          
          <p><font face="Arial, Helvetica, sans-serif" size="2">For more information
            about the film <a href="../schmovies/index-on-the-verge.htm">click
            here</a></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="http://enr.clearerchannel.org/media/schnews/on-the-verge.wmv" target="_blank"><b>Click
            here</b></a><b> to download trailer</b> - (1.30mins, wmv format, 2.5meg)
            </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>* To download
            &#39;On The Verge&#39; on bit torrent</b> (900meg) <a href="../schmovies/on-the-verge-schmovies-2008.mpg.torrent" target="_blank">click
            here</a></font><font face="Arial, Helvetica, sans-serif"> </font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Take Three - SchMOVIES Collection DVD 2007 -->
      <tr>
        <td colspan="2" height="146" valign="top">
          <table align="right" height="209" width="196">
            <tr>
              <td height="262">
                <div align="center"><a href="../schmovies/images/take-three-800.jpg" target="_blank"><img alt="Take Three - SchMOVIES DVD Collection 2007" border="0" src="../schmovies/take-three-200.jpg" style="margin-left: 15px"><br>
                  <font size="1">click here for larger image</font> </a></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="take3"></a></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif">TAKE
            THREE  SchMOVIES Collection DVD 2007</font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This is the third
            SchMOVIES DVD collection featuring short direct action films produced
            by SchMOVIES in 2007, covering Hill Of Tara Protests, Smash EDO, Naked
            Bike Ride, The No Borders Camp at Gatwick, Class War plus many others.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Get a copy
            now... </b></font></p>
          
          
          <font face="Arial, Helvetica, sans-serif" size="2">or, send a cheque
          written out to &#39;Justice&#39; to the address at the bottom of the page...<br>
          <br>
          Please make sure to enter your shipping address on paypal, as it doesn&#39;t
          give us this information automatically. </font>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="http://enr.clearerchannel.org/media/schnews/take-three-trailer.mpg" target="_blank"><b>Click
            here</b></a><b> to download the trailer</b> - (1.30mins, MPEG1 format,
            16.2meg, Dec 2007</font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- V for Video Activist -->
      <tr>
        <td colspan="2" height="183" valign="top">
          <table align="right" height="163" width="179">
            <tr>
              <td>
                <div align="center"><a href="../schmovies/v-for-video-activist-700.jpg" target="_blank"><img border="0" height="198" src="../schmovies/v-for-video-activist-200.jpg" style="margin-left: 15px" width="200"><br>
                  <font size="1">click here for larger image</font></a></div>
              </td>
            </tr>
          </table>
          <p><font size="4"><b><font face="Arial, Helvetica, sans-serif">V For
            Video Activist</font><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="v"></a></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif"><br>
            </font></b></font><font face="Arial, Helvetica, sans-serif"><b><font size="2">SchMOVIES
            DVD Collection 2006</font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">The pick of the
            SchMOVIES produced in 2006, covering events across the UK such as
            the Smash EDO campaign, protest camps at Titnore Woods and Shepton
            Mallet, the Camp For Climate Action, anti-war demos at Parliament
            Square in the face of SOPCA laws, community resistance to the M74
            motorway in Glasgow, and more... One theme that runs through these
            films is the blatant use of a &#39;political police force&#39; to protect
            corporations and war criminals rather than uphold the law. But despite
            this, people are prepared to take direct action and not let a police
            state goosestep in. Remember... People should not be afraid of their
            governments. Governments should be afraid of their people.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Out NOW - £6
            (including P&amp;P) - which comes with free copy of the SchMOVIES
            2005 DVD! (If you already have one, and don&#39;t want the 2005 DVD let
            us know when you order). Contact the SchNEWS Office 01273 685913 or
            <a href="mailto:schnews@brighton.co.uk">email</a></b></font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- SchMOVIES DVD 2005 -->
      <tr>
        <td colspan="2" height="230" valign="top">
          <table align="right" height="135" width="183">
            <tr>
              <td>
                <div align="center"><a href="../schmovies/images/schmovies-dvd-2005-800.png" target="_blank"><img alt="SchMOVIES DVD Collection 2005" border="0" height="201" src="../schmovies/images/schmovies-dvd-2005-200.png" style="margin-left: 15px" width="200"><br>
                  <font size="1">click here for larger image</font></a></div>
              </td>
            </tr>
          </table>
          <h4 align="left"><b><font face="Arial, Helvetica, sans-serif" size="4">SchMOVIES
            DVD - 2005</font><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="2005"></a></font></b></font></b></font></b></font></b></font></b></h4>
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><strong>AVAILABLE
            NOW</strong> - £6 (including P&amp;P) Contact the SchNEWS Office
            01273 685913 or <a href="mailto:schnews@brighton.co.uk">email</a>
            </font>
          </p><p align="left"><font face="Arial, Helvetica, sans-serif" size="2">An
            eclectic collection spanning the G8 protests in Scotland this year,
            the SMASH EDO campaign (with Mark Thomas), the CRE8 Summit and MAD
            PRIDE to name a few. These are high resolution screening copies of
            some of the best SchMOVIES of 2005 (including some which not on this
            website) There are plenty of extras which also include a SchNEWS at
            DSEI special. </font>
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- SchNEWs at Ten -->
      <tr>
        <td colspan="2" height="181" valign="top">
          <table align="right" height="209" width="185">
            <tr>
              <td height="290">
                <div align="center"><a href="../schmovies/images/schnews-at-ten-800.jpg" target="_blank"><img alt="SchNEWS At Ten - The Movie" border="0" height="281" src="../schmovies/images/schnews-at-ten-200.jpg" style="margin-left: 15px" width="200"><br>
                  <font size="1">click here for larger image</font> </a></div>
              </td>
            </tr>
          </table>
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"></font><font size="4">SchNEWS
            At Ten - The Movie<b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="at10"></a></font></b></font></b></font></b></font></b><br>
            <font size="3">- A Decade Of Party &amp; Protest</font></font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">The final, final
            cut of the rollocking 74 minute journey through ten years of party
            and protest. This film was produced in the same crowded office at
            the same time as its companion book &#39;SchNEWS At Ten&#39; - for more about
            the book <a href="merchandise.html">click here</a></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="../schmovies/sch-at-10-movie.htm">For
            more information click here</a></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="schnews-at-ten-trailer.mov">Click
            here to download the trailer</a> </b>(1min 20, 4meg, quicktime format)</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Order yours
            now - </b>only a fiver including p&amp;p (a colour printed cover and
            plastic case but it&#39;s a burnt blank dvd). Send a cheque written to
            &#39;Justice&#39; to the address below. Paypal available soon.</font></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Casualisation Kills -->
      <tr>
        <td colspan="2" height="163" valign="top">
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="4"><img align="right" alt="Casualisation Kills - the story of the Simon Jones Memorial Campaign" border="1" height="277" src="../images_merchandise/simon.jpg" style="margin-left: 15px" width="201">Casualisation
            Kills</font><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="sj"></a></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif" size="2"><br>
            <font size="3">(Simon Jones 2002 update)</font></font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">People like Simon
            Jones get killed at work all the time and nothing is done about it,
            but not this time. Family and friends of a young man killed at work
            demand justice and an end to casualisation of labour. The campaign
            has since ensured a historic prosecution for corporate manslaughter.
            The video tells the story of the campain and shows how family and
            friends of Simon worked with direct action activists and trade unionists.
            <a href="http://www.cultureshop.org/details.php?code=SJC">www.cultureshop.org/details.php?code=SJC</a></font><b></b></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" height="2" valign="top">
          <hr>
        </td>
      </tr>
      <!-- Guerillavision Compilation -->
      <tr>
        <td colspan="2" height="136" valign="top">
          <p><font face="Arial, Helvetica, sans-serif" size="-1"><b><font face="Arial, Helvetica, sans-serif" size="4"><img align="right" alt="Guerillavision Compilation" height="176" src="../images_merchandise/guerillas.gif" style="margin-left: 15px" width="300">Guerillavision
            Compilation</font><font size="4"><b><font size="4"><b><font size="4"><b><font size="4"><b><font face="Arial, Helvetica, sans-serif"><a name="gc"></a></font></b></font></b></font></b></font></b></font><font face="Arial, Helvetica, sans-serif" size="4">
            <br>
            <font size="3">- Rattle in Seattle, Capitals Ill, Crowd Bites Wolf</font></font></b></font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="-1">Since Seattle
            Guerillavision have been documenting the campaigns against globalisation
            from behind the barricades. These are stylistic hard hitting films
            (Big Rattle in Seattle, Capitals Ill and Crowd Bites wolf) which pull
            no punches. But be careful, watching all three together will have
            you reaching for a rock and heading for the nearest symbol of capitalist
            oppression.<br>
            <br>
            Available from: <a href="http://www.cultureshop.org/details.php?code=GV3">www.cultureshop.org/details.php?code=GV3</a></font>
          </p>
        </td>
      </tr>
      <tr>
        <td bgcolor="#000000" colspan="2" height="2" valign="top"><img height="10" src="../images_main/spacer_black.gif" width="10"></td>
      </tr>
    </table>
</div>
<div class="mainBar_right">

		<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>


</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
