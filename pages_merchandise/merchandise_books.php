<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Merchandise - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise_distro_list.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
						 <!--#include virtual="../inc/bannerGraphic_merchandise.php"-->
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarMain.php"-->

</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">

		 <!--#include virtual="../inc/navBar_merchandise.php"-->
	
<p style="font-size: small;"><em>Don't order anything on this site - but if you really want something email <a href="mailto:schnews@riseup.net">schnews@riseup.net</a> you never know. </em></p>
	
	<table>
      <tr>
        <td colspan="4" height="144" valign="top"> 
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b><a href="#schnewsigd">SchNEWS 
            In Graphic Detail</a>  ONLY A FIVER plus £1.50 UK p&amp;p<a href="#schnewsat10"><br>
            SchNEWS At Ten</a> ONLY £5 including UK p&amp;p<br>
            <a href="#PDR">Peace de Resistance</a> <a href="#PDR">(Issues 351-401)</a> 
            £3 incl p&amp;p <a href="#sotw"><br>
            SchNEWS Of The World (Issues 301-350)</a> £3 incl p&amp;p <br>
            <a href="#book_one">SchNEWS Yearbook 2001 with SQUALL (Issues 251-300)</a> 
            £3 incl p&amp;p<br>
            <a href="#book_two">SchQUALL SchNEWS and SQUALL back to back (Issues 
            201-250)</a> SOLD OUT!<br>
            <a href="#book_three">SchNEWS Survival Handbook (Issues 151-200)</a> 
            SOLD OUT<br>
            <a href="#book_four">SchNEWSannual (Issues 101-150)</a> SOLD OUT<br>
            <a href="#book_five">SchNEWSround (Issues 51-100)</a> SOLD OUT<br>
            <a href="#book_six">SchNEWSreader (Issues 0-50)</a></b></font> <b><font face="Arial, Helvetica, sans-serif" size="2">SOLD 
            OUT<br>
            </font></b><font face="Arial, Helvetica, sans-serif" size="2">NOTE: 
            If you want a few different books email us and we&#39;ll do a deal... 
          </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><a href="#order">Ordering
            Information</a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SCHNEWS GRAPHICS BOOK -->
      <tr>
        <td colspan="4" height="2" valign="top"> 
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="schnewsigd"></a> 
            </b></font></b></font><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="3">SchNEWS In Graphic Detail</font></b></font></p>        </td>
        <td height="2" valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top"> 
          <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/in-graphic-detail-1000.jpg" target="_blank"><img alt="SchNEWS In Graphic Detail" border="0" height="280" src="../images_merchandise/in-graphic-detail-200.jpg" width="200"></a></p>
            <p><b><font face="Arial, Helvetica, sans-serif" size="4">OUT NOW -
              only a fiver!</font></b></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">A paperback-sized, 
            full-colour book of political graphics and satirical material produced 
            by SchNEWS in the past decade. Covering issues from climate change 
            to vivisection, police repression to resource wars, these graphics 
            manage to raise a grin out of some deadly serious matters, all the 
            while never watering down the uncompromising message of SchNEWS. This 
            is a diverse range of images both in form and content with detourned 
            or subverted images, fake ads, mixed metaphors, wafer-thin gags and 
            more. To view many of them <a href="../satire">click 
            here</a> </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 978-09529748-9-5<br>
            96 pages, 120 x 168mm, published June 2011<br>
            For sales, reviews or other info email <a href="mailto:books@schnews.org.uk">books@schnews.org.uk</a>
            </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>The book is
            just £5 plus £1.50 p&amp;p in UK</b> </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">We can receive 
            payment by cheque payable to &#39;Justice&#39;. (Address at bottom of page)</font></p>
          <div style="padding: 10px; margin: 10px; border-top: 2px solid #666666; margin-top: 20px">

          <br>
          
          <br>
          
		  </div>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWS at Ten -->
      <tr>
        <td colspan="3" height="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="schnewsat10"></a>
          </b></font></b></font><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="3">SchNEWS
          At Ten</font></b></font></td>
        <td height="4" valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
        	 <div align="center" style="float: right; padding: 10px; margin: 10px">
        	<p align="center"><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="3"><a href="../images_merchandise/s-at-10-lg.jpg" target="_blank"><img alt="SchNEWS At Ten book cover" border="0" src="../images_merchandise/s-at-10-200.jpg"></a></font></b></font></b></font></p>
          <p align="center"><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="2"><b><font face="Arial, Helvetica, sans-serif" size="4"><a href="../extras/at10/distrolist.htm">Buy
            this book in these bookshops</a></font></b></font></b></font></p>
           </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">It all started
            in a squatted courthouse in Brighton back in 1994 with people wearing
            silly wigs reading out the alternative news. Ever since then SchNEWS
            has been publishing continuous weekly newsheets full of radical get-off-your-arse
            direct action politics from Britain and across the world. This book
            brings together some of the key stories of the decade from the big
            roads protests and anti-Criminal Justice Act movement in the nineties
            through to the global anti-capitalist movement and opposition to the
            war in Iraq. From Newbury to Seattle, Liverpool Dockers to the Zapatistas
            - with a lot of the less well known, but equally important moments
            in between. And not forgetting our infamous crap arrests of the week.</font>          </p>
          <p>Click <a href="bookreviews.html">here</a> to see some reviews of
            the book.</p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748
            8 6<br>
            </font><font face="Arial, Helvetica, sans-serif" size="2">320 pages,
            168 x 245mm, published October 2004</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">Order it direct
            from us now for £5 incl. UK p&amp;p - cheques payable to &#39;Justice&#39;
            or click below to order by credit card. Email us for postage prices
            for outside UK.</font></p>
          
          
                  </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- Reace de Resistance -->
      <tr>
        <td colspan="4" height="8" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="PDR"></a><font size="3">Peace
          de Resistance - Annual 2003</font><br>
          </b></font></td>
        <td height="8" valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
        <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/pdr-lg.jpg" target="_blank"><img align="middle" alt="SchNEWS - Peace De Resistance book cover" border="0" src="../images_merchandise/Peace de Resistance web large.jpg"></a><br>
              <font size="4"><b><font face="Arial, Helvetica, sans-serif">With
            Free CD-Rom!</font></b></font></p>
            <p><b><font face="Arial, Helvetica, sans-serif" size="4"><a href="../peacederesistance/distrolist.htm">Buy
              this book in these bookshops</a></font></b></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">&#39;As the Forces
            of Darkness gather for a new attack on Iraq, they&#39;re shaken by an
            unexpected explosion of resistance. US airbases are invaded; military
            convoys are blockaded; schoolkids stage mass walkouts from Manchester
            to Melbourne; roads are occupied, embassies besieged, and cities all
            over the world are brought to a standstill by the biggest mass demonstrations
            ever seen. Read about the efforts to sabotage the war machine that
            the corporate media ignored: not just the mass demos, but Tornado-trashing
            in Scotland, riots across the Middle East, and train blockades in
            Europe. Featuring SchNEWS 351-401, this book also covers many of the
            past year&#39;s other stories from the rebel frontlines, including GM
            crop-trashing, international activists in Palestine, and the 10th
            anniversary of Castlemorton. All that plus loads of new articles,
            photos, cartoons, subverts, satire, a comprehensive contacts list,
            and more&#39;</font> </p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>COMES WITH
            FREE CD-ROM by BEYOND TV featuring footage of many of the actions
            reported in the book, satire plus other digital resources </b></font>          </p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748
            7 8 <br>
            304 pages 168 x 245mm, published June 2003</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">£3.00 inc.
            p&amp;p within UK (including a free cd-rom) direct from us - see address
            below - cheques payable to JUSTICE<br>
            <font size="1">For international orders email <a href="mailto:schnews@riseup.net">schnews@brighton.co.uk</a>
            for prices.</font> </font></p>
          
          
          
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b>All
            the SchNEWS issues (351-400) in this book are online</b></font><font face="Arial, Helvetica, sans-serif" size="2">
            -<a href="../archive/index-351-401.htm"><b> click here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWs of the World -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="sotw"></a><font size="3">SchNEWS
          Of The World - </font>
          THE STORIES THAT SHOOK THE WORLD </b></font></td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
        	<div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><br>
              <a href="../images_merchandise/sotw-lg.jpg" target="_blank"><img alt="SchENWS Of The World book cover" border="0" src="../images_merchandise/sotw-200.jpg"></a></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">The temperature
            goes up a few notches as the earth&#39;s climate has its hottest year
            on record - and the corporate carve-up lifts its tempo in the paranoia
            and madness of September 11th. Behind the haze Argentina goes into
            meltdown. Israel reoccupies Palestine causing a second Intifada. Hundreds
            of thousands come out on the street in Genoa, Quebec, Gothenburg,
            Barcelona, Brussels against globalised institutions. Even larger numbers
            fight for their land and livelihood against neo-liberalism in South
            Africa, India, South America and the rest of the global south. Global
            and local - this book covers it all and more. Featuring issues 301-350
            of SchNEWS plus 200 pages of articles, cartoons, photos, graphics,
            satire, and a comprehensive contact database.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748
            6 X <br>
            300 pages, 168 x 245mm, published August 2002</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>Bargain </b>-
            £3.00 inc. p&amp;p within UK direct from us - see address below
            - cheques payable to JUSTICE<br>
            <font size="1">For international orders email <a href="mailto:schnews@riseup.net">schnews@brighton.co.uk</a>
            for prices.</font> </font></p>
          
          
          
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b>All
            the SchNEWS issues (301-350) in this book are online</b></font><font face="Arial, Helvetica, sans-serif" size="2">
            -<a href="../archive/index-301-350.htm"><b>click here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!--  SchNEWS SQUALL Yearbook 2001 -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_one"></a><font size="3">SchNEWS
          SQUALL Yearbook 2001</font>
          SchNEWS and SQUALL and much more....</b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
         <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/yb2001-lg.jpg" target="_blank"><img alt="SchNEWS Squall Yearbook 2001 cover" border="0" src="../images_merchandise/yb2001-200.jpg"></a></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">The Zapatistas
            march into Mexico City, thousands disrupt the World Bank meeting in
            Prague, Churchill gets an anarchist make-over: from Bognor to Bogota,
            Dudley to Delhi, and Kilburn to Melbourne, resistance has become as
            global as the institutions of capitalism. It&#39;s all here. Featuring
            SchNEWS 251-300, the best of Squall, plus articles from people at
            the frontline of struggles worldwide, and creating sustainable solutions
            to the corporate carve-up of the planet. All wrapped up with loadsa
            photos, cartoons, satirical graphics, subverts, and a comprehensive
            contacts database.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748
            4 3 <br>
            300 pages, 168 x 245mm, published June 2001</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>BARGAIN</b>
            - £3.00 inc. p&amp;p direct from us - see address below - cheques
            payable to JUSTICE</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">Or you can pay
            by credit card by clicking the button below. <br>
            <b>NOTE: </b>we&#39;ve had to add 30p to the price to cover PayPal charges
            so it&#39;s £3.30</font></p>
          
          
          
          <p align="left"><font face="Arial, Helvetica, sans-serif" size="2"><b>All
            the SchNEWS issues (251-300) in this book are online</b></font><font face="Arial, Helvetica, sans-serif" size="2">
            -<a href="../archive/index-251-300.htm"><b>click here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchQUALL -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_two"></a><font size="3">SchQUALL</font><br>
          SchNEWS and SQUALL Back to Back </b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
			 <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/schquall-lg.jpg" target="_blank"><img alt="SchQUALL Book Cover" border="0" src="../images_merchandise/schquall-200.jpg"></a></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">IT&#39;S ALL HERE:
            genetically modified organisms, animal rights, freemasons, June 18th
            &#39;99 international day of action against capitalism, Exodus Collective,
            Cuba, the November 30th &#39;99 Battle for Seattle, climate change, parties
            and festivals, indigenous peoples&#39; resistance to multinational corporations,
            the privatisation of everything in sight, crap arrests of the week,
            prisoner support and much morethe full lowdown on the direct
            action movement in Britain and abroad! The SchQUALL book is packed
            full of quality photos from top frontline photographers including
            Nick Cobbing, Ivan Coleman, and Ian Hunter, and crammed with cartoons
            by Pete Loveday, Polyp, Isy, Kate Evans and others - plus all the
            usual satirical humour, wry cynicism and a massive contacts database
            of over 700 grassroots organisations, campaign groups, websites, zines,
            people and places </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748-3-5<br>
            274 pages, 168 x 245mm, published June 2000</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>WE&#39;VE SOLD 
            OUT - SORRY </b>but you AK Press still have some copies - go to <a href="http://www.akuk.com">www.a</a><a href="http://www.akuk.com">kuk.com</a> 
            and search for &quot;Schquall&quot;</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>All the SchNEWS
            issues (201-250) in this book are online</b> -<a href="../archive/index-201-250.htm"><b>
            click here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWS Survival Handbook -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_three"></a><font size="3">SchNEWS
          Survival Handbook</font><br>
          PROTEST AND SURVIVE</b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" valign="top">
        	<div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/sh-lg.jpg" target="_blank"><img alt="SchNEWS Survival Handbook Cover" border="0" src="../images_merchandise/sh-200.jpg"></a></p>
            <p> </p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">Read all about
            it! Genetic crop sites get a good kicking; streets reclaimed all over
            the world; docks occupied in protest of death at work; protestors
            rude about multinational corporations&#39; plans for world domination...
            </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">SchNEWS gives
            you the news the mainstream media ignores. Tells you where to party
            and protest. Tries not to get all po-faced about what&#39;s going down
            in the world. </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This book features
            issues 151-200 plus cartoons, photos and other articles designed to
            help you survive into the new millenium. Not forgetting a comprehensive
            database of nearly five hundred grassroots organisations. </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">Tune in, turn
            on, fight back. </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 09529748-2-7<br>
            256 pages, 168 x 245mm, published May 1999</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>WE&#39;VE SOLD 
            OUT - SORRY </b>but you AK Press still have some copies - go to <a href="http://www.akuk.com">www.a</a><a href="http://www.akuk.com">kuk.com</a> 
            and search for &quot;Schnews&quot;</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>All the SchNEWS
            issues (151-200) in this book are online - </b><a href="../archive/index-151-200.htm"><b>click
            here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWS Annual -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_four"></a><font size="3">SchNEWS
          Annual</font></b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" height="389" valign="top"> 
          <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/annual-lg.jpg" target="_blank"><img alt="SchNEWS Annual book cover" border="0" src="../images_merchandise/annual-200.jpg"></a></p>
          </div>
          <font face="Arial, Helvetica, sans-serif" size="2">With free radioactive 
          cover... this is issues 101 to 150, which cover full-on anti-nuclear 
          demos in Germany, the massive Reclaim The Streets in Trafalgar Square, 
          and one bloke on Rockall... not to mention the fascinating saga of New 
          Labour breaking each and every promise they ever made. With extra articles, 
          original graphics and the 1997 update of the activist&#39;s database. </font> 
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN: 0-9529748-1-9</font><font face="Arial, Helvetica, sans-serif" size="2"><br>
            208 pages, 168 x 245 mm, launched 10th February 1998</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><font size="3">SOLD 
            OUT - </font></b>You&#39;ll be lucky to find this anywhere.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>All the SchNEWS
            issues (101-150) in this book are online</b> -<a href="../archive/index-101-150.htm"><b>click
            here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWSRound -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_five"></a><font size="3">SchNEWSround</font></b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" height="399" valign="top"> 
          <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/round-lg.jpg"><img alt="SchNEWSround book cover" border="0" src="../images_merchandise/round-200.jpg"></a></p>
          </div>
          <font face="Arial, Helvetica, sans-serif" size="2">Reclaim the streets... 
          Newbury... the Squatter&#39;s Estate Agency... as &#39;Direct Action&#39; became 
          media buzzwords, SchNEWS published the inside story from the activists 
          themselves! Issues 51-100 plus 60 pages of extra articles and photographs, 
          as well as an activists database.</font> 
          <p><font face="Arial, Helvetica, sans-serif" size="2">&#39;The national 
            protest newsletter... with more correspondents than <i>The Guardian</i>.&#39; 
            - <i>The Guardian</i> </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN: 0-9529748-0-0<br>
            200 pages, 168 x 245mm, published December 1996</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><font size="3">SOLD 
            OUT - </font></b>Good luck trying to find one.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>All the SchNEWS
            issues (51-100) in this book are online</b> -<a href="../archive/index-051-100.htm"><b>click
            here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <!-- SchNEWSReader -->
      <tr>
        <td colspan="4" valign="top"><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="book_six"></a><font size="3">SchNEWSreader</font></b></font></td>
        <td valign="top" width="403"> </td>
      </tr>
      <tr>
        <td colspan="4" height="340" valign="top"> 
          <div align="center" style="float: right; padding: 10px; margin: 10px">
            <a href="../images_merchandise/reader-lg.jpg" target="_blank"><img alt="SchNEWS Reader book cover" border="0" src="../images_merchandise/reader-200.jpg"></a> </div>
        <font face="Arial, Helvetica, sans-serif" size="2">Where
          it all began... this original SchNEWS book contains the single-sided
          pilot issue, and all the others from 1 to 50. </font>
          <p><font face="Arial, Helvetica, sans-serif" size="2">This was where
            &#39;Crap Arrest of the Week&#39; was first seen, and the issues feature the
            famous Criminal Justice Act Arrest-o-meter! In between the SchNEWS
            issues are 60 pages of cartoons by Kate Evans.</font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><font size="3">SOLD 
            OUT</font> -</b> Hens teeth rare.<br>
            </font></p>
          <p>202 pages, 168 x 245 mm, published December 1995 <br>
            (No ISBN number)</p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>All the SchNEWS
            issues (pilot - 50) in this book are online</b> - <a href="../archive/index-001-50.htm"><b>click
            here</b></a></font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <hr>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top">
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b><a name="order"></a>Ordering
            SchNEWS merchandise</b><br>
            Send cheques or postal orders payable to &#39;Justice?&#39; for the amount
            listed above to the address below. Please send UK currency if you
            can.</font></p>        </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top"> </td>
      </tr>
      <tr>
        <td colspan="4" height="2" valign="top"><img height="10" src="../images_main/spacer_black.gif" width="618"></td>
      </tr>
    </table>
</div>
<div class="mainBar_right">


		<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>



		 <!--#include virtual="../inc/books-distro-list.htm"-->
	
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
