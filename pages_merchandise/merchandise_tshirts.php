<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Merchandise - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			
				 <!--#include virtual="../inc/bannerGraphic_merchandise.php"-->
	
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">

			 <!--#include virtual="../inc/navBar.php"-->
	
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
	
			 <!--#include virtual="../inc/leftBarMain.php"-->
	
</div>
<div class="copyleftBar">
	
		 <!--#include virtual="../inc/copyLeftBar.php"-->
	
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">

	 <!--#include virtual="../inc/navBar_merchandise.php"-->

<p style="font-size: small;"><em>Don't order anything on this site - but if you really want something email <a href="mailto:schnews@riseup.net">schnews@riseup.net</a> you never know. </em></p>

<table width="100%">
              <tr>
                <td height="130" width="68%">
                

                
                  <h3 align="left">SchNEWS T-Shirts</h3>
                  <p align="left"><b><font face="Arial, Helvetica, sans-serif" size="2">You
                    may have been there, you may have done it, but have you bought
                    the t-shirt? SchNEWS T-Shirts are back...</font></b></p>
                  <p align="left"><font face="Arial, Helvetica, sans-serif" size="2">The
                    t-shirts are in<b> black</b>
                    or <b><font color="#006600">dark green</font></b>, in the
                    following sizes... </font></p>
                  <p align="left"><font face="Arial, Helvetica, sans-serif" size="2">Small,
                    </font><font face="Arial, Helvetica, sans-serif" size="2">Medium,
                    Large and Extra Large - £10 (+ £1.50 P&amp;P to
                    UK, £2 to Europe and £2.50 to the rest of the
                    world.) </font></p>
                </td>
                <td height="130" width="32%">
                	<img align="right" src="../images_merchandise/t-shirt-front-back.png" style="border: 2px solid #555555">
				</td>
              </tr>
            </table>
            <table align="center" border="1" cellpadding="2" cellspacing="2" height="201" width="76%">
              <tr>
                <td bgcolor="#003300" height="59" width="46%"><img alt="SchNEWS - a drop of truth in an ocean of bullshit" src="../images_merchandise/tshirtfrontsmall.jpg"></td>
                <td bgcolor="#000000" height="59" width="54%">
                  <div align="center"><img alt="SchNEWS T-Shirt back - Newbury Reunion Crane" height="161" src="../images_merchandise/tshirtbacksmall.gif" width="250"></div>
                </td>
              </tr>
              <tr>
                <td bgcolor="#000000" width="46%">
                  <div align="center"><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Front</font></b></div>
                </td>
                <td bgcolor="#000000" width="54%">
                  <div align="center"><b><font color="#FFFFFF" face="Arial, Helvetica, sans-serif">Back</font></b></div>
                </td>
              </tr>
            </table>
            <p align="left">Send a cheque written out to &#39;Justice&#39; to the address
              below...</p>
            <p align="left">Or, you can now pay by credit card via PayPal. Click
              the Buy Now button and follow the instructions on the PayPal page.
              <b>PLEASE COULD YOU SUPPLY YOUR ADDRESS AS PAYPAL DON&#39;T AUTOMATICALLY
              GIVE US YOUR DETAILS.</b> Thanks! </p>
            
            <br>
            
            <br>
            
			<br><br>
			<img height="10" src="../images_main/spacer_black.gif" width="100%">
</div>
<div class="mainBar_right">
		
		<div style="margin-left:7px"><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>
		
</div>
<div class="footer">
	
			 <!--#include virtual="../inc/footer.php"-->
	
</div>
</div>
</body>
</html>
