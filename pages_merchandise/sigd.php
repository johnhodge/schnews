<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Merchandise - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise.css" rel="stylesheet" type="text/css">
<link href="../old_css/merchandise_distro_list.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
						 <!--#include virtual="../inc/bannerGraphic_merchandise.php"-->
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarMain.php"-->

</div>
<div class="copyleftBar">
		 <!--#include virtual="../inc/copyLeftBar.php"-->

</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<?php
		//	Navigation bar for the Merchandise pages
		$merch_page = "books";
		 <!--#include virtual="../inc/navBar_merchandise.php"-->
	?>
	<table width="100%">
      <!-- SCHNEWS GRAPHICS BOOK -->
      <tr>
        <td colspan="1" height="9" valign="top">
        	<div style="margin-left: 10%; margin-right: 10%; text-align: center; font-size: 17pt; font-weight: bold; padding: 5px">
        		NEW - SchNEWS IN GRAPHIC DETAIL
        	</div>
        </td>
      </tr>
      <tr>
        <td colspan="1" height="183" valign="top">
          <div align="center" style="float: right; padding: 10px; margin: 10px">
            <p><a href="../images_merchandise/in-graphic-detail-1000.jpg"><img alt="SchNEWS In Graphic Detail" border="0" height="280" src="../images_merchandise/in-graphic-detail-200.jpg" width="200"></a></p>
            <p><b><font face="Arial, Helvetica, sans-serif" size="4">OUT NOW -
              only a fiver!</font></b></p>
          </div>
          <p><font face="Arial, Helvetica, sans-serif" size="2">SchNEWS has (finally)
            assembled this collection of its best graphics from the past decade,
            presented in a full colour pocket-sized book. Covering issues from
            climate change to vivisection, police repression to resource wars,
            these graphics manage to raise a grin out of some deadly serious matters,
            all the while never watering down the uncompromising anarcho-green
            message of SchNEWS. This is a diverse range of images both in form
            and content with detourned or subverted images, fake ads, mixed metaphors,
            wafer-thin gags and more. To view many of them <a href="../satire">click
            here</a> </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">ISBN 978-09529748-9-5<br>
            96 pages, 120 x 168mm, published May 2011<br>
            For sales, reviews or other info email <a href="mailto:books@schnews.org.uk">books@schnews.org.uk</a>
            </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2"><b>The book is
            just £5 plus £1.50 p&amp;p in UK</b> </font></p>
          <p><font face="Arial, Helvetica, sans-serif" size="2">We can receive
            payment by cheque payable to &#39;Justice&#39;. (Address at bottom of page)
            </font></p>
          <p>
          </p><div style="padding: 10px; margin: 10px; border-top: 2px solid #666666; margin-top: 20px">
            <p>The first 50 people to order <b>SchNEWS in Graphic Detail</b> will
              receive a free SchNEWS DVD - request from the list of <a href="www.schnews.org.uk/pages_merchandise/merchandise_video.php">SchMOVIES
              titles </a> or go for pot luck and we&#39;ll send you one...</p>
            
          <br>
          
          <br>
          
		<br><br>
          Remember, make sure to state which SchMOVIES DVD you want else we&#39;ll choose for you.
          <p>
          Also, please make sure you&#39;re address is given when you order, otherwise we can&#39;t post it to you.
         </p></div>
        </td>
      </tr>
      <tr>
        <td colspan="1" height="2" valign="top">
          <hr>
        </td>
      </tr>
    </table>
</div>
<div class="mainBar_right">
		<?php
		//	Latest Issue Summary
		 <!--#include virtual="../inc/books-distro-list.htm"-->
		?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
