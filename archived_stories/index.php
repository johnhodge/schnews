<?php
		
	/*
	 * 	Initial local config
	 */
	$css_to_load	=	array( 'stories', 'homepage', 'general' );



	/*
	 *	Include the main library
	 */
	require "../display/display.php";


	
	/*
	 *	Draw the HTML headers
	 */
	HTML_draw_html_header( $css_to_load );
	

	
	/*
	 * 	Draw the page header (title graphic, nav bar, etc.)
	 */
	HTML_draw_page_header();
	
	

	
	/*
	 * 		DO STUFF HERE
	 * 
	 */
	PAGES_diplay_previous_stories();

	
	
	
	/*
	 * 	Draw the page footer
	 */
	HTML_draw_page_footer();
	



	/*
	 * 	Call the HTML footer and close the page
	 */
	HTML_draw_html_footer();
	exit;

?>