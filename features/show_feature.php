<?php
	//	Make sure an article ID is in the URL/GET Header
	if (isset($_GET['feature']) && is_numeric($_GET['feature']))	{
			$id		=	$_GET['feature'];
	}	else {
			header("Location: http://www.schnews.org.uk");
			exit;
	}
	//	Grab our functions...
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
	//	Make sure the ID is clean - it will be used for DB action later...
	$id		=	cleaninput($id);
	//	Cache the uer name/id combos
	$authors		=	get_users(true);
	//	Make sure that this feature has been voted fo sufficiently...
	$hits			=	get_num_votes_for_features($id);
	//	Are we in preview mode, and if so does the hash in the URl match the hash in the DB for the story being previewed
	$test			=	false;
	if (isset($_GET['test']))	{
		$hash	=	get_feature_test_hash($id);
		if (urldecode($_GET['test']) ==	$hash)	{
			$test		=	true;
		}
	}
	if ($hits < NUM_FEATURE_VOTES && !$test)	{
		header("Location: http://www.schnews.org.uk");
		exit;
	}
	if (!$test)	{
		incrementFeatureHits($id);
	}
	//	Test for print friendly version
	$print	=	false;
	if (isset($_GET['print']) && $_GET['print'] == 1)	{
		$print		=	true;
	}
	if ($print)	{
		require	('show_feature_print.php');
		exit;
	}
	//	Get the banner info...
	$conn			=	mysqlConnect();
	$sql			=	"	SELECT banner_graphic, banner_graphic_alt, banner_graphic_link
							FROM issues
							ORDER BY issue DESC
							LIMIT 0,1	";
	$rs				=	mysqlQuery($sql, $conn);
	$banner			=	mysqlGetRow($rs);
	mysql_close($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Feature Articles - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="features.css" rel="stylesheet" type="text/css">
<link href="../issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
			<?php
				//	Include Banner Graphic
				 <!--#include virtual="../inc/bannerGraphic.php"-->
			?>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
		<?php
		//	Include Left Bar
		 <!--#include virtual="../inc/leftBarMain.php"-->
	?>
</div>
<div class="copyleftBar">
	<?php
		//	Include Copy Left Bar
		 <!--#include virtual="../inc/copyLeftBar.php"-->
	?>
</div>
<!-- ============================================================================================
                            MAIN TABLE CELL
============================================================================================ -->
<div class="mainBar">
	<?php
		//	Get all the data from this article from the DB...
		$conn		=	mysqlConnect();
		$sql		=	"	SELECT *
							FROM features
							WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);
		$feature		=	mysqlGetRow($rs);
		//	Get all the keywords...
		$sql		=	"	SELECT keyword
							FROM keywords
							WHERE feature_id = $id";
		$rs			=	mysql_query($sql, $conn);
		$keywords	=	array();
		while ($keyword		=	mysqlGetRow($rs))	{
			$keywords[]		=	"<a href='../keywordSearch/?keyword=".urlencode(stripslashes($keyword['keyword']))."'>".stripslashes($keyword['keyword']).'</a>';
		}
		$keywords	=	implode(', ', $keywords);
		//	Prepare our data
		$date		=	makeHumanDate($feature['date'], 'words');
		if ($feature['author'] != 0)	{
			$author		=	stripslashes($authors[$feature['author']]);
		}
		if (isset($author) && trim($author) == '') unset($author);
		$title		=	strtoupper(stripslashes($feature['title']));
		$title		=	str_replace('SCHNEWS', 'SchNEWS', $title);
		$subtitle	=	stripslashes($feature['subtitle']);
		$blurb		=	stripslashes($feature['blurb']);
		$body		=	scanForCustomTags(scanForLinks(stripslashes($feature['body'])));
		$img_small	=	stripslashes($feature['graphic_small']);
		$img_large	=	stripslashes($feature['graphic_large']);
		//	Begin our output
		$output		=	array();
		if (isset($author))	$authorPhrase		=	" - <b>Author</b>: $author";
		else $authorPhrase	=	'';
		$output[]	=	"<table width='100%'><tr><td valign='top'>";
		$output[]	=	"<div class='feature_back_link'>";
		$output[]	=	"<a href='index.php'>Back to Feature List</a>";
		$output[]	=	"</div>";
		$output[]	=	"</td><td valign='top'>";
		//	Print Friendly Link
		$output[]	=	"<div class='feature_print_friendly_link'>";
		$output[]	=	"<a href='show_feature.php?feature=$id&print=1' target='_BLANK'>Print Friendly Version</a>";
		$output[]	=	"</div>";
		$output[]	=	"<div class='feature_date_line'>".$date.$authorPhrase.'</div>';
		$output[]	=	"</td></tr></table>";
		$output[]	=	"<div class='feature_title'>";
		$output[]	=	$title;
		$output[]	=	'</div>';
		if ($subtitle != '')	{
				$output[]	=	"<div class='feature_subtitle'>";
				$output[]	=	$subtitle;
				$output[]	=	'</div>';
		}
		$output[]	=	"<div class='feature_blurb'>";
		$output[]	=	$blurb;
		$output[]	=	'</div>';
		$output[]	=	"<div class='feature_body'>";
		if ($img_small != '' )	{
			$output[]		=	"<a href='../images/$img_large' target='_BLANK'><img src='../images/$img_small' align='right' class='feature_preview_image' style='margin: 0px 0px 10px 10px' /></a>";
		}$output[]	=	$body;
		$output[]	=	'</div>';
		$output[]	=	"<div class='feature_keywords'>";
		$output[]	=	"<b>Keywords : </b>".$keywords;
		$output[]	=	'</div>';
		echo implode("\r\n", $output);
		mysql_close($conn);
		echo "<br />";
	?>
		<!-- Messages/Discussion -->
		<a name="comments"> </a>
		<div id="messagesBoard">
			<?php
				if (SHOWMESSAGES)	{
						addMessage($id, true);
						echo getMessages($id, true);
						echo showAddMessage($id, true);
				}
			?>
		</div>
</div>
<div class="mainBar_right">
	<div>
		<span style="font-size:14px; font-weight: bold">SchNEWS</span> - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and
		social issues, direct action protests and campaigns, both UK and abroad.<br>
		<br>
		See details of this week&#39;s issue below...
	</div>
	<?php
		//	Include Current Issue Summary
		 <!--#include virtual="../inc/currentIssueSummary.php"-->
	?>
	<div style="font-weight: bold; font-size: 13px; text-align: center; border-top: 1px solid #666666; border-bottom: 1px solid #666666; margin-bottom:8px; padding-bottom: 16px; margin-top: 28px; padding-top: 16px">
		OTHER RECENT FEATURE ARTICLES
	</div>
	<br>
	<?php
		$voted_features		=	get_voted_for_features();
		if ($voted_features 	==	''	)	{
			$where		=	'WHERE 1 = 1';
		}	else	{
			$where		=	" WHERE id IN ( $voted_features) ";
		}
		//	Get the several most recent entries from the database
		$conn	=	mysqlConnect();
		$sql	=	"	SELECT id, title, blurb, date
						FROM features
						$where
						AND id <> $id
						ORDER BY date DESC
						LIMIT 0, ".NUM_RIGHT_BAR_FEATURES;
		$rs		=	mysqlQuery($sql, $conn);
		$features		=	array();
		//	Cycle through the articles
		while ($feature		=	mysqlGetRow($rs))	{
				//	Prepare Data
				$this_id	=	$feature['id'];
				if ($id == $this_id)	continue;
				$title		=	stripslashes($feature['title']);
				$blurb		=	stripslashes($feature['blurb']);
				$date		=	makeHumanDate($feature['date'], 'words');
				$new		= 	is_date_x_days_old($feature['date'], DAYS_FEATURE_IS_NEW);
				$output		=	array();
				if ($new)	{
					$output[]	=	"<div style='padding: 10px; border: 2px solid #000000; margin: 0px; background-color: #ffffcc'>";
					$output[]	=	"<div style='color: #000000; font-size: 13px; font-weight: bold; text-align: center'>NEW</div>";
				}
				$output[]	=	"<div style='font-size: 11px; font-weight: bold; text-align: left'>";
				$output[]	=	"<a href='show_feature.php?feature=$this_id'>$title</a>";
				$output[]	=	"</div>";
				$output[]	=	"<div style='text-align: left; font-size: 10px'>";
				$output[]	=	"<i>$blurb</i>";
				$output[]	=	"</div>";
				$output[]	=	"<div style='text-align: right; font-size: 10px'>";
				$output[]	=	$date;
				$output[]	=	"</div>";
				if ($new)	{
					$output[]	=	'</div>';
				}
				$features[]	=	implode("\r\n", $output);
		}
		echo implode("<br /><br />", $features);
		mysql_close($conn);
	?>
</div>
<div class="footer">
			 <!--#include virtual="../inc/footer.php"-->

</div>
</div>
</body>
</html>
