<?php	require '../storyAdmin/functions.php';	 <!--#include virtual="../functions/functions.php"-->	//	Get the banner info...	$conn			=	mysqlConnect();	$sql			=	"	SELECT banner_graphic, banner_graphic_alt, banner_graphic_link							FROM issues							ORDER BY issue DESC							LIMIT 0,1	";	$rs				=	mysqlQuery($sql, $conn);	$banner			=	mysqlGetRow($rs);	mysql_close($conn);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Feature Articles - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="features.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript"><!--function MM_swapImgRestore() { //v3.0  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;}function MM_preloadImages() { //v3.0  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}}function MM_swapImage() { //v3.0  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}}function MM_findObj(n, d) { //v4.01  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);  if(!x && d.getElementById) x=d.getElementById(n); return x;}//--></script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,					&#39;../images_menu/about-over.gif&#39;,					&#39;../images_menu/contacts-over.gif&#39;,					&#39;../images_menu/guide-over.gif&#39;,					&#39;../images_menu/monopolise-over.gif&#39;,					&#39;../images_menu/prisoners-over.gif&#39;,					&#39;../images_menu/subscribe-over.gif&#39;,					&#39;images_main/donate-mo.gif&#39;,					&#39;../images_menu/schmovies-over.gif&#39;,					&#39;images_main/schmovies-button-over.png&#39;,					&#39;images_main/contacts-lhc-button-mo.gif&#39;,					&#39;images_main/book-reviews-button-mo.gif&#39;,					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">			
<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>		
			</div>	
			<?php				//	Include Banner Graphic				 <!--#include virtual="../inc/bannerGraphic.php"-->			?>
			</div>
			<!--	NAVIGATION BAR 	-->
			<div class="navBar">		<?php			//	Include Nav Bar			 <!--#include virtual="../inc/navBar.php"-->		?>
			</div>
			<!--	=========================================== -->
			<!--					LEFT BAR 	-->
			<!--	=========================================== -->
						<div class="leftBar">		<?php			//	Include Footer			 <!--#include virtual="../inc/leftBarMain.php"-->		?></div>
						<div class="copyleftBar">	<?php		//	Include Copy Left Bar		 <!--#include virtual="../inc/copyLeftBar.php"-->	?></div>
		<!-- ============================================================================================ 
		MAIN TABLE CELL
		============================================================================================ -->
		<div class="mainBar">	
		<div class="main_page_title" id="main_page_title">		Feature Articles	</div>		
		<div>		For those with an attention span of more than three minutes, here is a new collection of articles too long, analytical or high-brow to normally publish in the weekly newsheet.	</div>
		<div style="font-size: 8pt; padding-left:10px; margin-left: 40px; border-left: 1px solid #bbbbbb">		<p>We are open to submissions of longer articles - 1000-3000 words - which can explore topics ranging from geopolitical situations to biotechnology, renewable energy, economics, laws and legislation, and more to provide further background to the issues we cover on a weekly basis. Articles can be credited to an author, if s/he requests it. And we may request changes or suggest an edited version before publishing. Photos to accompany articles are welcome. Unless specified, all articles will be given &#39;copyleft&#39; status.	
		</p><p>		
		</p><p>While the initial set of articles in this column have previously appeared in SchNEWS, mostly this section will be web-only (though some may also appear in issues from time to time).		</p><p>	
		</p><p>If you would like to submit an article, email SchNEWS at <a href="mailto:schnews@riseup.net">mail@schnews.org.uk</a>	</p></div>	
		<div class="features_summary_list">		
		<?php			echo get_features_for_homepage(50, true, '', true);		?>	
		</div>
		</div>
		<!-- ============================================================================================   
		RIGHT BAR CELL
		============================================================================================ -->
		<div class="mainBar_right">
		<div>	
		<span style="font-size:14px; font-weight: bold">SchNEWS</span> - the free weekly direct action newsheet produced in Brighton, UK, since 1994 covering environmental and		social issues, direct action protests and campaigns, both UK and abroad.<br>	
		<br>		See details of this week&#39;s issue below...	</div>	<?php		//	Include Current Issue Summary		 <!--#include virtual="../inc/currentIssueSummary.php"-->	?></div>
		<div class="footer">		<?php			//	Include Footer			 <!--#include virtual="../inc/footer.php"-->		?></div>
		</div>
		</body>
		</html>