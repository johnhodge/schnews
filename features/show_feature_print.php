<?php 
//	Test inputs
if (!isset($print) || !$print || !isset($id))	{
	exit;
}
//	Get all the data from this article from the DB...
		$conn		=	mysqlConnect();
		$sql		=	"	SELECT *
							FROM features
							WHERE id = $id ";
		$rs			=	mysqlQuery($sql, $conn);
		$feature		=	mysqlGetRow($rs);
		//	Get all the keywords...
		$sql		=	"	SELECT keyword
							FROM keywords
							WHERE feature_id = $id";
		$rs			=	mysql_query($sql, $conn);
		$keywords	=	array();
		while ($keyword		=	mysqlGetRow($rs))	{
			$keywords[]		=	stripslashes($keyword['keyword']);
		}
		$keywords	=	implode(', ', $keywords);
		//	Prepare our data
		$date		=	makeHumanDate($feature['date'], 'words');
		if ($feature['author'] != 0)	{
			$author		=	stripslashes($authors[$feature['author']]);
		}
		if (isset($author) && trim($author) == '') unset($author);
		$title		=	strtoupper(stripslashes($feature['title']));
		$title		=	str_replace('SCHNEWS', 'SchNEWS', $title);
		$subtitle	=	stripslashes($feature['subtitle']);
		$blurb		=	stripslashes($feature['blurb']);
		$body		=	scanForCustomTags(scanForLinks(stripslashes($feature['body'])));
$output		=	array();
$output[]	=	"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html>
<head>
<title>SchNEWS Feature Articles - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<META name='description' content=\"Feature article by SchNEWS the free weekly direct action newsheet from Brighton, UK. Going back to issue 100, some available as PDF\" />
<META name='keywords' content=\"feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />
<META HTTP-EQUIV=\"Pragma\" CONTENT=\"no-cache\">
</head>
<html>";
$output[]	=	"<p>$date</p>";
if (isset($author))	{
	$output[]	=	"<p>Author: $author</p>";
}
$output[]	=	"<h1>$title</h1>";
if (isset($subtitle) && trim($subtitle) != '')	{
	$output[]	=	"<h2>$subtitle</h2>";
}	
if (isset($blurb) && trim($blurb) != '')	{
	$output[]	=	"<h3>$blurb</h3>";
}
$output[]		=	"<p>".nl2br(strip_tags(str_replace("<br />", "\r\n", $body), '<b><i>'))."</p>";
$output[]		=	"<p>__________________________________</p>";
$output[]		=	"<p>Keywords: $keywords</p>";
$output[]		=	"</body>";
$output[]		=	"</html>";
echo implode("\r\n", $output);
?>