<?php
	 <!--#include virtual="../storyAdmin/functions.php"-->
	 <!--#include virtual="../functions/functions.php"-->
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>SchNEWS Feature Articles - Direct action, demonstrations, protest, climate change, anti-war, G8</title>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="feature articles, pdf, SchNEWS, direct action, Brighton, information for action, wake up, anarchist, justice, anti-copyright, anti-capitalist, crap arrest, social and environmental change, IMF, WTO, World Bank, WEF, GATS, Cliamte Change, no borders, Simon Jones, protest, privatisation, neo-liberal, yearbook 2002, Kyoto protocol, climate change, global warming, global south, GMO, anti-war, permaculture, sustainable, Schengen, SIS, sustainability, reclaim the streets, RTS, food miles, copyleft, stopthewar, SHAC, Plan Colombia, Zapatistas, anti-roads, anti-nuclear, anti-war, stopthewar, Iraq sanctions squatting, subvertise, satire, alternative news, Hackney not 4 sale, www.schnews.org.uk, you make plans, we make history, Mapuche, Aventis, Bayerhazard, Noborder, Rising Tide, Carlo Guiliani, PGA, Monopolise Resistance, anti-terrorism, Afghanistan, Radical Routes, WEF, Palestine occupation, Indymedia, Women speak out, Titnore Woods, asylum seeker" name="keywords">
<meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type">
<meta content="no-cache" http-equiv="Pragma">
<link href="../old_css/schnews_new.css" rel="stylesheet" type="text/css">
<link href="features.css" rel="stylesheet" type="text/css">
<link href="../old_css/issue_summary.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
//-->
</script>
</head>
<body alink="#FF6600" leftmargin="0px" link="#FF6600" onload="MM_preloadImages(&#39;../images_menu/archive-over.gif&#39;,
					&#39;../images_menu/about-over.gif&#39;,
					&#39;../images_menu/contacts-over.gif&#39;,
					&#39;../images_menu/guide-over.gif&#39;,
					&#39;../images_menu/monopolise-over.gif&#39;,
					&#39;../images_menu/prisoners-over.gif&#39;,
					&#39;../images_menu/subscribe-over.gif&#39;,
					&#39;images_main/donate-mo.gif&#39;,
					&#39;../images_menu/schmovies-over.gif&#39;,
					&#39;images_main/schmovies-button-over.png&#39;,
					&#39;images_main/contacts-lhc-button-mo.gif&#39;,
					&#39;images_main/book-reviews-button-mo.gif&#39;,
					&#39;images_main/merchandise-button-mo.gif&#39;)" topmargin="0px" vlink="#FF6600">
<script language="javascript" src="../javascript/jquery.js" type="text/javascript"></script>
<script language="javascript" src="../javascript/indexMain.js" type="text/javascript"></script>
<div class="main_container">
<!--	PAGE TITLE	-->
<div class="pageHeader">
			<div class="schnewsLogo">
			<a href="../index.htm"><img alt="SchNEWS the free weekly direct action news sheet covering social and environmental
						issues in the UK and abroad including climate change, arms industry, ecological direct action and more..." border="0" height="90px" src="../images_main/schnews.gif" width="292px"></a>
			</div>
    <div class="bannerGraphic"> <a href="../pages_merchandise/merchandise_books.php"><img alt="SchNEWS In Graphic Detail - NEW BOOK OUT NOW" border="0" height="90" src="../images_main/in-graphic-detail-banner2.jpg" width="700"></a> </div>
</div>
<!--	NAVIGATION BAR 	-->
<div class="navBar">
			 <!--#include virtual="../inc/navBar.php"-->
</div>
<!--	=========================================== -->
<!--					LEFT BAR 	-->
<!--	=========================================== -->
<div class="leftBar">
			 <!--#include virtual="../inc/leftBarMain.php"-->

</div>
<!-- ============================================================================================
 MAIN TABLE CELL
 ============================================================================================ -->
 <div class="mainBar">	
 <div class="main_page_title" id="main_page_title">		Feature Articles	</div>	
 	<div>	<p>	For those with an attention span of more than three minutes, here is a collection of articles too long, analytical or high-brow to normally publish in the weekly newsheet.</p>	</div>
		<div style="font-size: 8pt; padding-left:10px; margin-left: 40px; border-left: 1px solid #bbbbbb">		
		<p>We are open to submissions of longer articles - 1000-3000 words - which can explore topics ranging from geopolitical situations to biotechnology, renewable energy, economics, laws and legislation, and more to provide further background to the issues we cover on a weekly basis. Articles can be credited to an author, if s/he requests it. And we may request changes or suggest an edited version before publishing. Photos to accompany articles are welcome. Unless specified, all articles will be given &#39;copyleft&#39; status.		</p>
		<p>		</p>

	
		</div>	
		
		
		<div class="features_summary_list">	
		
			
			<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=18">
<img align="right" class="feature_preview_image" src="../images/f-dragon-75.jpg" width="75px"></a>

<div style="font-weight: bold; font-size: 20px">
  <a href="dragon-on-a-ciggie.htm">Dragon On A Ciggie</a></div>
  
<div style="font-size: 11px; color: #666666; padding: 3px">23-03-2011</div>
<div><p><i>A brief history of the Dragon Festival and Cigarrones travellers site, southern Spain. </i></p></div>
<div class="feature_summary_blurb"><p>The Cigarrones travellers&#39; site is one of several communities which have sprung up near Orgiva in Andalucia, Spain, in recent decades. Coming to the southern tip of Europe to escape the repression against travellers in Britain and elsewhere, they have carved out a life of avin&#39; it autonomous anarchy - despite increasing attention from tinpot local authorities who act like Franco is still in. Since 1997 the site has held the annual Dragon Festival - now arguably one of the most significant free festivals in Europe - but this is also under attack. Here is a brief history written by a resident of Cigarrones:</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=17"><img align="right" class="feature_preview_image" src="../images/f-climate-wars-75.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px">
<a href="dyer-warnings.htm">Dyer Warnings</a></div>

<div style="font-size: 11px; color: #666666; padding: 3px">19-11-2010</div>
<div class="feature_summary_blurb"><p>SchNEWS interviews Canadian journalist and military historian Gwynne Dyer about the dire warnings for a post-climate change world in his book &#39;The Climate Wars&#39;</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=16"><img align="right" class="feature_preview_image" src="../images/f-atentado-ciudad-juarez-75.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="mexico-its-grim-up-north.htm">Mexico: It&#39;s Grim Up North</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">21-08-2010</div>
<div class="feature_summary_blurb"><p>Report from US-Mexico border about the narco wars whose connections go right up to the president</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=15"><img align="right" class="feature_preview_image" src="../images/f-honduras-2-75.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="honduras.htm">More Than Just A Movement: Resistance To The Coup In Honduras</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">06-07-2010</div>
<div class="feature_summary_blurb"><p>One year after the military coup in Honduras which ousted the leader and installed a neo-liberal cabal, grassroots groups across the country are aligning to create a popular movement.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=14"><img align="right" class="feature_preview_image" src="../images/f-stallman-75.png" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="direct-hacktion.htm">Direct Hacktion</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">05-05-2010</div>
<div class="feature_summary_blurb"><p>SchNEWS interviews Richard Stallman - hacker, founder of the Free Software movement and activist for digital-software-information freedom...</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=13"><img align="right" class="feature_preview_image" src="../images/708-medyan-150.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="eyewitness-afghanistan.htm">Eyewitness Afghanistan</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">10-02-2010</div>
<div class="feature_summary_blurb"><p>From Kemp Town to Kabul, as SchNEWS interviews Al Jazeera journalist Medyan Dairieh about his take on the war...</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=11"><img align="right" class="feature_preview_image" src="../images/f-cambodia-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="cambodia.htm">Mass Evictions In Cambodia </a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">31-01-2010 - <b>Author:</b> Ziggy Black</div>
<div class="feature_summary_blurb"><p>An eyewitness account from Phnom Penh, as Cambodia faces its largest forced displacements since the time of the Khmer Rouge.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=10"><img align="right" class="feature_preview_image" src="../images/three_house_sm.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="titnore-woods-is-on-alert.htm">Titnore Woods is on Alert</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">28-01-2010</div>
<div class="feature_summary_blurb"><p>The future of Titnore Woods is threatened as Tesco and Worthing Council gang up to build upon the ancient woodland...</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=1"><img align="right" class="feature_preview_image" src="../images/f-693-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="who-are-ya.htm">Who Are Ya?</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">02-10-2009</div>
<div class="feature_summary_blurb"><p>Who are the far-right English Defence League, and what are their strategies?</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=2"><img align="right" class="feature_preview_image" src="../images/f-686-natalia-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="rotten-to-the-caucases.htm">Rotten to the Caucases</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">07-08-2009</div>
<div class="feature_summary_blurb"><p>With the murder of Russian human rights activist Natalia Estemirova in Chechnya, we look at the Russian-backed despotic regime in Chechnya.</p>
</div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=3"><img align="right" class="feature_preview_image" src="../images/f-685-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="hippy-hippy-shakedown.htm">The Hippy, Hippy Shakedown</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">27-07-2009</div>
<div class="feature_summary_blurb"><p>At the last minute the Big Green Gathering festival in Somerset was pulled due to legal pressure - SchNEWS looks at the events and factors that led to this.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=4"><img align="right" class="feature_preview_image" src="../images/f-682-honduras-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="coup-blimey.htm">Coup Blimey</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">02-07-2009</div>
<div class="feature_summary_blurb"><p>The president of Honduras, Manuel Zelaya, is removed and sent into exile after a military coup d&#39;etat.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=5"><img align="right" class="feature_preview_image" src="../images/f-678-beanfield-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="convoy-polloi.htm">Convoy Polloi</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">05-06-2009</div>
<div class="feature_summary_blurb"><p>This year marks the 24th anniversary of the infamous police attack on travellers on their way to Stonehenge in an incident now known as the Battle Of The Beanfield.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=6"><img align="right" class="feature_preview_image" src="../images/f-670-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="in-the-brown-stuff.htm">In the Brown Stuff</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">27-03-2009</div>
<div class="feature_summary_blurb"><p>SchNEWS looks how deep the financial problems are for the banks and the British Govt, and how they won&#39;t learn from their errors.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=8"><img align="right" class="feature_preview_image" src="../images/f-664-omar-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="omar-deghayes.htm">Omar Deghayes Speaks to SchNEWS</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">30-01-2009</div>
<div class="feature_summary_blurb"><p>Given a more optimistic environment after Obama&#39;s announcement that he&#39;s going to close the  Guantanamo prison camp, SchNEWS interviews ex-detainee, Omar Deghayes, to gauge his reaction.</p> </div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=9"><img align="right" class="feature_preview_image" src="../images/f-661-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="eyewitness-rafah-gaza.htm">Eyewitness Rafah, Gaza</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">09-01-2009</div>
<div class="feature_summary_blurb"><p>Eyewitness accounts from British activists on the ground during the wanton attack on Gaza by Israel in January 2009.</p></div>
</td></tr></tbody></table>
</div>


<div class="feature_summary" style="float: none; clear: both">
<table width="100%"><tbody><tr><td>
<a href="../features/show_feature.php?feature=7"><img align="right" class="feature_preview_image" src="../images/f-658-100.jpg" width="75px"></a>
<div style="font-weight: bold; font-size: 20px"><a href="somali-argh.htm">Somali-argh</a></div>
<div style="font-size: 11px; color: #666666; padding: 3px">05-12-2008</div>
<div class="feature_summary_blurb"><p>Somali pirates roaming the Gulf Of Aden, hijacking - amongst other ships - a Saudi oil supertanker. How is it possible? What geo-political context is giving rise to these latter-day pirates?</p></div>
</td></tr></tbody></table>
</div>
	</div>
	</div>
	<!-- ============================================================================================   
 RIGHT BAR CELL
 ============================================================================================ -->
 		<div class="mainBar_right">	
		
		<div><a href="../archive/index.htm" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Issue Archive','','../images_main/issue-archive-mo-225.jpg',1)"><img src="../images_main/issue-archive-225.jpg" alt="SchNEWS Issue Archive" name="Issue Archive" width="225" height="180" border="0" id="Issue Archive" /></a></div>
<p style="font-size:smaller; margin-left:7px">All articles published by SchNEWS in its weekly newsheets 1994-2014.<br>
<strong>See <a href="../archive/index.htm">SchNEWS Issue Archive </a></strong></p>

		
	
 		</div>
<div class="footer">				 <!--#include virtual="../inc/footer.php"-->		</div>
		</div>
		</body>
		</html>
