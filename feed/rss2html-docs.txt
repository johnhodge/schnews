PHP Script Documentation v2.0

Contents
  Introduction
  Uses
  Templates
  Running from FeedForAll's server
  Running on local server
  Example websites
	Advanced Notes

Introduction

FeedForAll has made available a free PHP program called rss2html.php, which can be used by webmasters to automatically display RSS feeds on their website.  The resulting webpage will always show the RSS feed's most current data.  The advantage of using the rss2html.php script is that the resulting webpage's contents will always be up to date, and can be easily spidered by search engines.


Uses

- Create a webpage with an RSS feed's information
- Insert an RSS feed's information into a location in an existing webpage


Templates

FeedForAll's rss2html.php script uses templates to generate its output.  The templates allow you to easily control what the resulting webpage will look like.  A template can be used to generate a complete webpage, or a portion of a webpage like a table (which can then be included inside an existing webpage). The template's syntax is exactly the same as the syntax used in FeedForAll's export functions.

Templates are simply standard HTML documents, with special variables inserted where the RSS feed's data should appear.  Usually the easiest way to create a template is to create a sample webpage with some real data in it, and then replace the feed's data with template variables, or modify an existing webpage, by adding template variables.  Here is a list of the variables supported by FeedForAll's rss2html.php script:

~~~FeedTitle~~~  (The feed's title)
~~~FeedDescription~~~  (the feed's description)
~~~FeedContentEncoded~~~ (the feed's description from optional ContentEncoded field)
~~~FeedLink~~~  (the feed's link)
~~~FeedPubDate~~~  (the feed's pub date/time in RSS format)
~~~FeedPubLongDate~~~  (the feed's date in your Windows long date format)
~~~FeedPubShortDate~~~ (the feed's date in your Windows short date format)
~~~FeedPubLongTime~~~  (the feed's time in your Windows long time format)
~~~FeedPubShortTime~~~  (the feed's time in your Windows short time format)
~~~FeedImageUrl~~~  (the URL to the feed's image)
~~~FeedImageTitle~~~  (the title for the feed's image)
~~~FeedImageLink~~~  (the link associated with the feed's image)

~~~FeedMaxItems=X~~~ (the maximum number of items to appear on the webpage - replace 'X' with a number. The first 'X' number of items in the feed will be displayed. Using a negative number for 'X' will show the last 'X' items from a feed. 
~~~SortByPubDate~~~  (the items will be sorted by their PubDate, before they are displayed - newest first)
~~~NoFutureItems~~~  (items with PubDates that are in the future won't be displayed until that date/time arrives)

~~~BeginItemsRecord~~~  (repeat everything between this and the end marker, for each item)
~~~EndItemsRecord~~~  (repeat everything between this and the start marker, for each item)
~~~BeginAlternateItemsRecord~~~  (separates the ItemsRecord into alternating halves)

~~~ItemTitle~~~  (the item's title) 
~~~ItemDescription~~~  (the item's description)
~~~ItemContentEncoded~~~ (the item's description from optional ContentEncoded field)
~~~ItemLink~~~  (the item's link)
~~~ItemPubDate~~~  (the item's pub date/time in RSS format)
~~~ItemPubLongDate~~~  (the items's date in your Windows long date format)
~~~ItemPubShortDate~~~   (the item's date in your Windows short date format)
~~~ItemPubLongTime~~~  (the item's time in your Windows long time format)
~~~ItemPubShortTime~~~  (the item's time in your Windows short time format)
~~~ItemEnclosureUrl~~~  (the item's enclosure URL)
~~~ItemGuid~~~ (the item's GUID field value)

When the rss2html.php script runs, it will substitute information from the RSS feed into the location wherever a variable is found.  For example, every time ~~~FeedTitle~~~ is found in the template's text, the Title from the RSS feed will be substituted.

Special variables: 
The ~~~FeedMaxItems=X~~~ variable can be used to limit the number of items that will be inserted into the webpage.  Replace the 'X' with a number of feed items you would like displayed (no spaces).

The ~~~BeginItemsRecord~~~ is used to mark the beginning of the section in the template, that should be repeated for each item in the RSS feed.  A corresponding ~~~EndItemsRecord~~~ should be used to mark the end of the repeating section.  Everything invbetween these two variables will be repeated for every item in the feed.  You would typically place all your ~~~Item...~~~ variables in between the ~~~BeginItemsRecord~~~ and ~~~EndItemsRecord~~~ variables.  

The ~~~BeginAlternateItemsRecord~~~ tag is optional, and can be used in between the ~~~BeginItemsRecord~~~ and ~~~EndItemsRecord~~~ variables to split the section into two halves.  Everything between the ~~~BeginItemsRecord~~~ and the ~~~BeginAlternateItemsRecord~~~ will be used for odd numbered RSS feed items, and everything between the ~~~BeginAlternateItemsRecord~~~ and the ~~~EndItemsRecord~~~ will be used for even numbered RSS feed items.  This is typically used to give alternating feed items a slightly different look.

Example - template used to generate a complete HTML webpage:

<HTML>
    <HEAD>
        <TITLE>~~~FeedTitle~~~</TITLE>
    </HEAD>
    <BODY>
        <H1>~~~FeedTitle~~~</H1>
        <BR>
        <H2>~~~FeedDescription~~~</H2>
        <BR>
        <TABLE width="100%">
            ~~~BeginItemsRecord~~~
            <TR>
                <TD>
                    <B>~~~ItemTitle~~~</B>
                </TD>
            </TR>
            <TR>
                <TD>
                    ~~~ItemDescription~~~
                </TD>
            </TR>
            <TR>
                <TD>
                    <BR>
                </TD>
            </TR>
            ~~~EndItemsRecord~~~
        </TABLE>
    </BODY>
</HTML>


Example - template used to generate an HTML table, for including inside existing webpage*

<TABLE WIDTH="100%">
    ~~~BeginItemsRecord~~~
    <TR VALIGN="TOP">
        <TD> 
            <BR>
	    <FONT SIZE="-1">~~~ItemPubShortDate~~~ </FONT><BR>
            <B>~~~ItemTitle~~~</B><BR>
            ~~~ItemDescription~~~<BR>
            <BR>
            <A HREF="~~~ItemLink~~~">more info</A>
            <BR><BR> 
        </TD>
    </TR>
    ~~~EndItemsRecord~~~
</TABLE>

*Server-side includes, imbedded PHP code, and other inclusion methods are outside the scope of this document.


Running rss2html.php from FeedForAll's public server

If your webserver doesn't support PHP, then you can run the rss2html.php script on FeedForAll's public server instead.  All you need to do is setup a link on your website to access the script and resulting webpage.

Syntax: 

http://scripts.rss2html.com/public/rss2html.php?XMLFILE=<your-rss-feed>&TEMPLATE=<your-template>&MAXITEMS=<max-items>

The above URL should all be on one line, with no spaces.  

Replace '<your-rss-feed>' with the full URL to your RSS feed (i.e. http://www.mydomain.com/myfeed.xml)

Replace '<your-template>' with the full URL to the webpage template you want to use (i.e. http://www.mydomain.com/mytemplate.htm)

Replace '<max-items>' with the maximum number of items from your RSS feed you want included on the webpage.  The MAXITEMS variable is optional.  If it is not included, all the RSS feed items will be included.

Sample:

http://scripts.rss2html.com/public/rss2html.php?XMLFILE=http://www.feedforall.com/blog-feed.xml&TEMPLATE=http://www.feedforall.com/samplefeedtemplate.htm

The above URL should all be on one line, with no spaces.


Running rss2html.php from your own webserver

Probably the best way to use rss2html.php is to upload it to your own webserver.  This gives you two ways to access the script.  First, you can setup a link on your webpage to access the script and resulting webpage.

Syntax:
http://<your-domain>/rss2html.php?XMLFILE=<your-rss-feed>&TEMPLATE=<your-template>&MAXITEMS=<max-items>

The above URL should all be on one line, with no spaces.  

Replace '<your-domain>' with the domain of your website

Replace '<your-rss-feed>' with the full URL to your RSS feed (i.e. http://www.mydomain.com/myfeed.xml)

Replace '<your-template>' with the full URL to the webpage template you want to use (i.e. http://www.mydomain.com/mytemplate.htm)

Replace '<max-items>' with the maximum number of items from your RSS feed you want included on the webpage.  The MAXITEMS variable is optional.  If it is not included, all the RSS feed items will be included.

Sample:

http://feedforall.com/rss2html.php?XMLFILE=blog-feed.xml&TEMPLATE=samplefeedtemplate.htm&MAXITEMS=10


The second option is to edit the settings inside of the rss2html.php script itself, and then just access the script as if it were a regular webpage.

With a text editor open up the rss2html.php file. Look for the line near the beginning that looks like this:

$XMLfilename = "sample.xml";

Replace sample.xml with either the local path or full URL to your RSS feed file.

Next, find the line that looks like this:

$TEMPLATEfilename = "BasicWebpage.htm";

Replace BasicWebpage.htm with either the local path or full URL to your template file.

Finally, if you want to specify the maximum number of items to include from your feed, find the line that looks like this:

$FeedMaxItems = 10000;

Replace 10000 with the maximum number of items you want.

Save the file with a new name (what you want to call it on your website), but make sure to keep the .php extension.

For example, if you named it 'myfeed.php', you can now access it from your browser by typing: http://<your-domain.com>/myfeed.php

Sample:

http://www.feedforall.com/blog.php


Examples of the RSS to HTML PHP script in action:

http://www.feedforall.com/blog.htm (FeedForAll's main RSS feed, using the rss2html.php script)

http://www.feedforall.com/public/layouts.htm (lots of sample pages)

http://www.small-business-software-net (the main page is actually the rss2html.php script being run)

http://www.create-rss.com (the main page is actually the rss2html.php script being run)


Advanced Notes:

Version 2.0 and later of this script use the PHP curl function to open feeds and templates.  Earlier versions used fopen.  If you need to use fopen for any reason, there is a setting at the beginning of the script that can be changed, to force that behavior.

Sometimes the URL of a feed you want to use with rss2html.php contains illegal characters in it. For example, you can' t use spaces, question marks, amperstans, etc in an URL. The solution is to use encoded characters in their place. Here is a list of illegal characters and their equivalent encodings:

% -> %2525
? -> %3f
& -> %26
= -> %3d
+ -> %2b
<SPACE> -> %20

For example this feed's URL:

http://www.mynewssource.com/opensearch.php?query=pike+fishing&search=Search&type=search

Should look like this in a call to rss2html.php:

http://www.rss2html.com/public/rss2html.php?XMLFILE=http://www.mynewssource.com/opensearch.php%3fquery%3dpike%2bfishing%26search%3dSearch%26type%3dsearch

